USE [RieltorServiceDB]
GO
/****** Object:  Table [dbo].[House_Type]    Script Date: 09/24/2018 22:52:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[House_Type](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_House_Type] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Source]    Script Date: 09/24/2018 22:52:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Source](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Source] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [Name] ON [dbo].[Source] 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Region]    Script Date: 09/24/2018 22:52:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Region](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Region] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Obj_Type]    Script Date: 09/24/2018 22:52:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Obj_Type](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Obj_Type] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [Type] ON [dbo].[Obj_Type] 
(
	[Type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cost_Type]    Script Date: 09/24/2018 22:52:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cost_Type](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Cost_Type] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[City]    Script Date: 09/24/2018 22:52:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[City](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](300) NOT NULL,
	[Id_Region] [int] NOT NULL,
 CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [Name] ON [dbo].[City] 
(
	[Name] ASC,
	[Id_Region] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sorce_city]    Script Date: 09/24/2018 22:52:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sorce_city](
	[Id_Source] [int] NOT NULL,
	[Id_City] [int] NOT NULL,
	[Domain] [nvarchar](max) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Settlement]    Script Date: 09/24/2018 22:52:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settlement](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Id_City] [int] NOT NULL,
 CONSTRAINT [PK_SubCity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[CreateCity]    Script Date: 09/24/2018 22:52:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CreateCity]
	@id int,   
    @name nvarchar(Max),
    @regionName nvarchar(Max)
AS
BEGIN
	DECLARE @idRegion INT;
	SELECT @idRegion = Id 
                FROM [Region]
                WHERE Name = @regionName;                
	insert into City values (@id, @name, @idRegion);
END
GO
/****** Object:  Table [dbo].[District]    Script Date: 09/24/2018 22:52:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[District](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Id_City] [int] NOT NULL,
 CONSTRAINT [PK_District] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MicroDistrict]    Script Date: 09/24/2018 22:52:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MicroDistrict](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Id_District] [int] NULL,
	[Id_City] [int] NOT NULL,
 CONSTRAINT [PK_MicroDistrict] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[CreateRegionsAndCity]    Script Date: 09/24/2018 22:52:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CreateRegionsAndCity]
AS
BEGIN

INSERT INTO [Region] VALUES (1, 'Адыгея');
INSERT INTO [Region] VALUES (2, 'Алтай');
INSERT INTO [Region] VALUES (3, 'Алтайский край');
INSERT INTO [Region] VALUES (4, 'Амурская область');
INSERT INTO [Region] VALUES (5, 'Архангельская область');
INSERT INTO [Region] VALUES (6, 'Астраханская область');
INSERT INTO [Region] VALUES (7, 'Башкортостан');
INSERT INTO [Region] VALUES (8, 'Белгородская область');
INSERT INTO [Region] VALUES (9, 'Брянская область');
INSERT INTO [Region] VALUES (10, 'Бурятия');
INSERT INTO [Region] VALUES (11, 'Владимирская область');
INSERT INTO [Region] VALUES (12, 'Волгоградская область');
INSERT INTO [Region] VALUES (13, 'Вологодская область');
INSERT INTO [Region] VALUES (14, 'Воронежская область');
INSERT INTO [Region] VALUES (15, 'Дагестан');
INSERT INTO [Region] VALUES (16, 'Еврейская АО');
INSERT INTO [Region] VALUES (17, 'Забайкальский край');
INSERT INTO [Region] VALUES (18, 'Ивановская область');
INSERT INTO [Region] VALUES (19, 'Ингушетия');
INSERT INTO [Region] VALUES (20, 'Иркутская область');
INSERT INTO [Region] VALUES (21, 'Кабардино-Балкария');
INSERT INTO [Region] VALUES (22, 'Калининградская область');
INSERT INTO [Region] VALUES (23, 'Калмыкия');
INSERT INTO [Region] VALUES (24, 'Калужская область');
INSERT INTO [Region] VALUES (25, 'Камчатский край');
INSERT INTO [Region] VALUES (26, 'Карачаево-Черкесия');
INSERT INTO [Region] VALUES (27, 'Карелия');
INSERT INTO [Region] VALUES (28, 'Кемеровская область');
INSERT INTO [Region] VALUES (29, 'Кировская область');
INSERT INTO [Region] VALUES (30, 'Коми');
INSERT INTO [Region] VALUES (31, 'Костромская область');
INSERT INTO [Region] VALUES (32, 'Краснодарский край');
INSERT INTO [Region] VALUES (33, 'Красноярский край');
INSERT INTO [Region] VALUES (34, 'Курганская область');
INSERT INTO [Region] VALUES (35, 'Курская область');
INSERT INTO [Region] VALUES (36, 'Ленинградская область');
INSERT INTO [Region] VALUES (37, 'Липецкая область');
INSERT INTO [Region] VALUES (38, 'Магаданская область');
INSERT INTO [Region] VALUES (39, 'Марий Эл');
INSERT INTO [Region] VALUES (40, 'Мордовия');
INSERT INTO [Region] VALUES (41, 'Москва');
INSERT INTO [Region] VALUES (42, 'Московская область');
INSERT INTO [Region] VALUES (43, 'Мурманская область');
INSERT INTO [Region] VALUES (44, 'Ненецкий Авт. Окр.');
INSERT INTO [Region] VALUES (45, 'Нижегородская область');
INSERT INTO [Region] VALUES (46, 'Новгородская область');
INSERT INTO [Region] VALUES (47, 'Новосибирская область');
INSERT INTO [Region] VALUES (48, 'Омская область');
INSERT INTO [Region] VALUES (49, 'Оренбургская область');
INSERT INTO [Region] VALUES (50, 'Орловская область');
INSERT INTO [Region] VALUES (51, 'Пензенская область');
INSERT INTO [Region] VALUES (52, 'Пермский край');
INSERT INTO [Region] VALUES (53, 'Приморский край');
INSERT INTO [Region] VALUES (54, 'Псковская область');
INSERT INTO [Region] VALUES (55, 'Ростовская область');
INSERT INTO [Region] VALUES (56, 'Рязанская область');
INSERT INTO [Region] VALUES (57, 'Самарская область');
INSERT INTO [Region] VALUES (58, 'Санкт-Петербург');
INSERT INTO [Region] VALUES (59, 'Саратовская область');
INSERT INTO [Region] VALUES (60, 'Сахалинская область');
INSERT INTO [Region] VALUES (61, 'Свердловская область');
INSERT INTO [Region] VALUES (62, 'Северная Осетия - Алания');
INSERT INTO [Region] VALUES (63, 'Смоленская область');
INSERT INTO [Region] VALUES (64, 'Ставропольский край');
INSERT INTO [Region] VALUES (65, 'Тамбовская область');
INSERT INTO [Region] VALUES (66, 'Татарстан');
INSERT INTO [Region] VALUES (67, 'Тверская область');
INSERT INTO [Region] VALUES (68, 'Томская область');
INSERT INTO [Region] VALUES (69, 'Тульская область');
INSERT INTO [Region] VALUES (70, 'Тыва');
INSERT INTO [Region] VALUES (71, 'Тюменская область');
INSERT INTO [Region] VALUES (72, 'Удмуртия');
INSERT INTO [Region] VALUES (73, 'Ульяновская область');
INSERT INTO [Region] VALUES (74, 'Хабаровский край');
INSERT INTO [Region] VALUES (75, 'Хакасия');
INSERT INTO [Region] VALUES (76, 'Ханты-Мансийский авт. окр.');
INSERT INTO [Region] VALUES (77, 'Челябинская область');
INSERT INTO [Region] VALUES (78, 'Чечня');
INSERT INTO [Region] VALUES (79, 'Чувашия');
INSERT INTO [Region] VALUES (80, 'Чукотский авт. окр.');
INSERT INTO [Region] VALUES (81, 'Якутия');
INSERT INTO [Region] VALUES (82, 'Ямало-Ненецкий авт. окр.');
INSERT INTO [Region] VALUES (83, 'Ярославская область');


EXECUTE CreateCity 1, 'Адыгейск', 'Адыгея';
EXECUTE CreateCity 2, 'Майкоп', 'Адыгея';
EXECUTE CreateCity 3, 'Горно-Алтайск', 'Алтай';
EXECUTE CreateCity 4, 'Алейск', 'Алтайский край';
EXECUTE CreateCity 5, 'Барнаул', 'Алтайский край';
EXECUTE CreateCity 6, 'Белокуриха', 'Алтайский край';
EXECUTE CreateCity 7, 'Бийск', 'Алтайский край';
EXECUTE CreateCity 8, 'Горняк', 'Алтайский край';
EXECUTE CreateCity 9, 'Заринск', 'Алтайский край';
EXECUTE CreateCity 10, 'Змеиногорск', 'Алтайский край';
EXECUTE CreateCity 11, 'Камень-на-Оби', 'Алтайский край';
EXECUTE CreateCity 12, 'Новоалтайск', 'Алтайский край';
EXECUTE CreateCity 13, 'Рубцовск', 'Алтайский край';
EXECUTE CreateCity 14, 'Славгород', 'Алтайский край';
EXECUTE CreateCity 15, 'Яровое', 'Алтайский край';
EXECUTE CreateCity 16, 'Белогорск', 'Амурская область';
EXECUTE CreateCity 17, 'Благовещенск', 'Амурская область';
EXECUTE CreateCity 18, 'Завитинск', 'Амурская область';
EXECUTE CreateCity 19, 'Зея', 'Амурская область';
EXECUTE CreateCity 20, 'Райчихинск', 'Амурская область';
EXECUTE CreateCity 21, 'Свободный', 'Амурская область';
EXECUTE CreateCity 22, 'Сковородино', 'Амурская область';
EXECUTE CreateCity 23, 'Тында', 'Амурская область';
EXECUTE CreateCity 24, 'Шимановск', 'Амурская область';
EXECUTE CreateCity 25, 'Архангельск', 'Архангельская область';
EXECUTE CreateCity 26, 'Вельск', 'Архангельская область';
EXECUTE CreateCity 27, 'Каргополь', 'Архангельская область';
EXECUTE CreateCity 28, 'Коряжма', 'Архангельская область';
EXECUTE CreateCity 29, 'Котлас', 'Архангельская область';
EXECUTE CreateCity 30, 'Мезень', 'Архангельская область';
EXECUTE CreateCity 31, 'Мирный', 'Архангельская область';
EXECUTE CreateCity 32, 'Новодвинск', 'Архангельская область';
EXECUTE CreateCity 33, 'Няндома', 'Архангельская область';
EXECUTE CreateCity 34, 'Онега', 'Архангельская область';
EXECUTE CreateCity 35, 'Северодвинск', 'Архангельская область';
EXECUTE CreateCity 36, 'Сольвычегодск', 'Архангельская область';
EXECUTE CreateCity 37, 'Шенкурск', 'Архангельская область';
EXECUTE CreateCity 38, 'Астрахань', 'Астраханская область';
EXECUTE CreateCity 39, 'Ахтубинск', 'Астраханская область';
EXECUTE CreateCity 40, 'Знаменск', 'Астраханская область';
EXECUTE CreateCity 41, 'Камызяк', 'Астраханская область';
EXECUTE CreateCity 42, 'Нариманов', 'Астраханская область';
EXECUTE CreateCity 43, 'Харабали', 'Астраханская область';
EXECUTE CreateCity 44, 'Агидель', 'Башкортостан';
EXECUTE CreateCity 45, 'Баймак', 'Башкортостан';
EXECUTE CreateCity 46, 'Белебей', 'Башкортостан';
EXECUTE CreateCity 47, 'Белорецк', 'Башкортостан';
EXECUTE CreateCity 48, 'Бирск', 'Башкортостан';
EXECUTE CreateCity 49, 'Благовещенск', 'Башкортостан';
EXECUTE CreateCity 50, 'Давлеканово', 'Башкортостан';
EXECUTE CreateCity 51, 'Дюртюли', 'Башкортостан';
EXECUTE CreateCity 52, 'Ишимбай', 'Башкортостан';
EXECUTE CreateCity 53, 'Кумертау', 'Башкортостан';
EXECUTE CreateCity 54, 'Межгорье', 'Башкортостан';
EXECUTE CreateCity 55, 'Мелеуз', 'Башкортостан';
EXECUTE CreateCity 56, 'Нефтекамск', 'Башкортостан';
EXECUTE CreateCity 57, 'Октябрьский', 'Башкортостан';
EXECUTE CreateCity 58, 'Салават', 'Башкортостан';
EXECUTE CreateCity 59, 'Сибай', 'Башкортостан';
EXECUTE CreateCity 60, 'Стерлитамак', 'Башкортостан';
EXECUTE CreateCity 61, 'Туймазы', 'Башкортостан';
EXECUTE CreateCity 62, 'Уфа', 'Башкортостан';
EXECUTE CreateCity 63, 'Учалы', 'Башкортостан';
EXECUTE CreateCity 64, 'Янаул', 'Башкортостан';
EXECUTE CreateCity 65, 'Алексеевка', 'Белгородская область';
EXECUTE CreateCity 66, 'Белгород', 'Белгородская область';
EXECUTE CreateCity 67, 'Бирюч', 'Белгородская область';
EXECUTE CreateCity 68, 'Валуйки', 'Белгородская область';
EXECUTE CreateCity 69, 'Грайворон', 'Белгородская область';
EXECUTE CreateCity 70, 'Губкин', 'Белгородская область';
EXECUTE CreateCity 71, 'Короча', 'Белгородская область';
EXECUTE CreateCity 72, 'Новый Оскол', 'Белгородская область';
EXECUTE CreateCity 73, 'Старый Оскол', 'Белгородская область';
EXECUTE CreateCity 74, 'Строитель', 'Белгородская область';
EXECUTE CreateCity 75, 'Шебекино', 'Белгородская область';
EXECUTE CreateCity 76, 'Брянск', 'Брянская область';
EXECUTE CreateCity 77, 'Дятьково', 'Брянская область';
EXECUTE CreateCity 78, 'Жуковка', 'Брянская область';
EXECUTE CreateCity 79, 'Злынка', 'Брянская область';
EXECUTE CreateCity 80, 'Карачев', 'Брянская область';
EXECUTE CreateCity 81, 'Клинцы', 'Брянская область';
EXECUTE CreateCity 82, 'Мглин', 'Брянская область';
EXECUTE CreateCity 83, 'Новозыбков', 'Брянская область';
EXECUTE CreateCity 84, 'Почеп', 'Брянская область';
EXECUTE CreateCity 85, 'Севск', 'Брянская область';
EXECUTE CreateCity 86, 'Сельцо', 'Брянская область';
EXECUTE CreateCity 87, 'Стародуб', 'Брянская область';
EXECUTE CreateCity 88, 'Сураж', 'Брянская область';
EXECUTE CreateCity 89, 'Трубчевск', 'Брянская область';
EXECUTE CreateCity 90, 'Унеча', 'Брянская область';
EXECUTE CreateCity 91, 'Фокино', 'Брянская область';
EXECUTE CreateCity 92, 'Бабушкин', 'Бурятия';
EXECUTE CreateCity 93, 'Гусиноозёрск', 'Бурятия';
EXECUTE CreateCity 94, 'Закаменск', 'Бурятия';
EXECUTE CreateCity 95, 'Кяхта', 'Бурятия';
EXECUTE CreateCity 96, 'Северобайкальск', 'Бурятия';
EXECUTE CreateCity 97, 'Улан-Удэ', 'Бурятия';
EXECUTE CreateCity 98, 'Александров', 'Владимирская область';
EXECUTE CreateCity 99, 'Владимир', 'Владимирская область';
EXECUTE CreateCity 100, 'Вязники', 'Владимирская область';
EXECUTE CreateCity 101, 'Гороховец', 'Владимирская область';
EXECUTE CreateCity 102, 'Гусь-Хрустальный', 'Владимирская область';
EXECUTE CreateCity 103, 'Камешково', 'Владимирская область';
EXECUTE CreateCity 104, 'Карабаново', 'Владимирская область';
EXECUTE CreateCity 105, 'Киржач', 'Владимирская область';
EXECUTE CreateCity 106, 'Ковров', 'Владимирская область';
EXECUTE CreateCity 107, 'Кольчугино', 'Владимирская область';
EXECUTE CreateCity 108, 'Костерёво', 'Владимирская область';
EXECUTE CreateCity 109, 'Курлово', 'Владимирская область';
EXECUTE CreateCity 110, 'Лакинск', 'Владимирская область';
EXECUTE CreateCity 111, 'Меленки', 'Владимирская область';
EXECUTE CreateCity 112, 'Муром', 'Владимирская область';
EXECUTE CreateCity 113, 'Петушки', 'Владимирская область';
EXECUTE CreateCity 114, 'Покров', 'Владимирская область';
EXECUTE CreateCity 115, 'Радужный', 'Владимирская область';
EXECUTE CreateCity 116, 'Собинка', 'Владимирская область';
EXECUTE CreateCity 117, 'Струнино', 'Владимирская область';
EXECUTE CreateCity 118, 'Судогда', 'Владимирская область';
EXECUTE CreateCity 119, 'Суздаль', 'Владимирская область';
EXECUTE CreateCity 120, 'Юрьев-Польский', 'Владимирская область';
EXECUTE CreateCity 121, 'Волгоград', 'Волгоградская область';
EXECUTE CreateCity 122, 'Волжский', 'Волгоградская область';
EXECUTE CreateCity 123, 'Дубовка', 'Волгоградская область';
EXECUTE CreateCity 124, 'Жирновск', 'Волгоградская область';
EXECUTE CreateCity 125, 'Калач-на-Дону', 'Волгоградская область';
EXECUTE CreateCity 126, 'Камышин', 'Волгоградская область';
EXECUTE CreateCity 127, 'Котельниково', 'Волгоградская область';
EXECUTE CreateCity 128, 'Котово', 'Волгоградская область';
EXECUTE CreateCity 129, 'Краснослободск', 'Волгоградская область';
EXECUTE CreateCity 130, 'Ленинск', 'Волгоградская область';
EXECUTE CreateCity 131, 'Михайловка', 'Волгоградская область';
EXECUTE CreateCity 132, 'Николаевск', 'Волгоградская область';
EXECUTE CreateCity 133, 'Новоаннинский', 'Волгоградская область';
EXECUTE CreateCity 134, 'Палласовка', 'Волгоградская область';
EXECUTE CreateCity 135, 'Петров Вал', 'Волгоградская область';
EXECUTE CreateCity 136, 'Серафимович', 'Волгоградская область';
EXECUTE CreateCity 137, 'Суровикино', 'Волгоградская область';
EXECUTE CreateCity 138, 'Урюпинск', 'Волгоградская область';
EXECUTE CreateCity 139, 'Фролово', 'Волгоградская область';
EXECUTE CreateCity 140, 'Бабаево', 'Вологодская область';
EXECUTE CreateCity 141, 'Белозерск', 'Вологодская область';
EXECUTE CreateCity 142, 'Великий Устюг', 'Вологодская область';
EXECUTE CreateCity 143, 'Вологда', 'Вологодская область';
EXECUTE CreateCity 144, 'Вытегра', 'Вологодская область';
EXECUTE CreateCity 145, 'Грязовец', 'Вологодская область';
EXECUTE CreateCity 146, 'Кадников', 'Вологодская область';
EXECUTE CreateCity 147, 'Кириллов', 'Вологодская область';
EXECUTE CreateCity 148, 'Красавино', 'Вологодская область';
EXECUTE CreateCity 149, 'Никольск', 'Вологодская область';
EXECUTE CreateCity 150, 'Сокол', 'Вологодская область';
EXECUTE CreateCity 151, 'Тотьма', 'Вологодская область';
EXECUTE CreateCity 152, 'Устюжна', 'Вологодская область';
EXECUTE CreateCity 153, 'Харовск', 'Вологодская область';
EXECUTE CreateCity 154, 'Череповец', 'Вологодская область';
EXECUTE CreateCity 155, 'Бобров', 'Воронежская область';
EXECUTE CreateCity 156, 'Богучар', 'Воронежская область';
EXECUTE CreateCity 157, 'Борисоглебск', 'Воронежская область';
EXECUTE CreateCity 158, 'Бутурлиновка', 'Воронежская область';
EXECUTE CreateCity 159, 'Воронеж', 'Воронежская область';
EXECUTE CreateCity 160, 'Калач', 'Воронежская область';
EXECUTE CreateCity 161, 'Лиски', 'Воронежская область';
EXECUTE CreateCity 162, 'Нововоронеж', 'Воронежская область';
EXECUTE CreateCity 163, 'Новохопёрск', 'Воронежская область';
EXECUTE CreateCity 164, 'Острогожск', 'Воронежская область';
EXECUTE CreateCity 165, 'Павловск', 'Воронежская область';
EXECUTE CreateCity 166, 'Поворино', 'Воронежская область';
EXECUTE CreateCity 167, 'Россошь', 'Воронежская область';
EXECUTE CreateCity 168, 'Семилуки', 'Воронежская область';
EXECUTE CreateCity 169, 'Эртиль', 'Воронежская область';
EXECUTE CreateCity 170, 'Буйнакск', 'Дагестан';
EXECUTE CreateCity 171, 'Дагестанские Огни', 'Дагестан';
EXECUTE CreateCity 172, 'Дербент', 'Дагестан';
EXECUTE CreateCity 173, 'Избербаш', 'Дагестан';
EXECUTE CreateCity 174, 'Каспийск', 'Дагестан';
EXECUTE CreateCity 175, 'Кизилюрт', 'Дагестан';
EXECUTE CreateCity 176, 'Кизляр', 'Дагестан';
EXECUTE CreateCity 177, 'Махачкала', 'Дагестан';
EXECUTE CreateCity 178, 'Хасавюрт', 'Дагестан';
EXECUTE CreateCity 179, 'Южно-Сухокумск', 'Дагестан';
EXECUTE CreateCity 180, 'Биробиджан', 'Еврейская АО';
EXECUTE CreateCity 181, 'Облучье', 'Еврейская АО';
EXECUTE CreateCity 182, 'Балей', 'Забайкальский край';
EXECUTE CreateCity 183, 'Борзя', 'Забайкальский край';
EXECUTE CreateCity 184, 'Краснокаменск', 'Забайкальский край';
EXECUTE CreateCity 185, 'Могоча', 'Забайкальский край';
EXECUTE CreateCity 186, 'Нерчинск', 'Забайкальский край';
EXECUTE CreateCity 187, 'Петровск-Забайкальский', 'Забайкальский край';
EXECUTE CreateCity 188, 'Сретенск', 'Забайкальский край';
EXECUTE CreateCity 189, 'Хилок', 'Забайкальский край';
EXECUTE CreateCity 190, 'Чита', 'Забайкальский край';
EXECUTE CreateCity 191, 'Шилка', 'Забайкальский край';
EXECUTE CreateCity 192, 'Вичуга', 'Ивановская область';
EXECUTE CreateCity 193, 'Гаврилов Посад', 'Ивановская область';
EXECUTE CreateCity 194, 'Заволжск', 'Ивановская область';
EXECUTE CreateCity 195, 'Иваново', 'Ивановская область';
EXECUTE CreateCity 196, 'Кинешма', 'Ивановская область';
EXECUTE CreateCity 197, 'Комсомольск', 'Ивановская область';
EXECUTE CreateCity 198, 'Кохма', 'Ивановская область';
EXECUTE CreateCity 199, 'Наволоки', 'Ивановская область';
EXECUTE CreateCity 200, 'Плёс', 'Ивановская область';
EXECUTE CreateCity 201, 'Приволжск', 'Ивановская область';
EXECUTE CreateCity 202, 'Пучеж', 'Ивановская область';
EXECUTE CreateCity 203, 'Родники', 'Ивановская область';
EXECUTE CreateCity 204, 'Тейково', 'Ивановская область';
EXECUTE CreateCity 205, 'Фурманов', 'Ивановская область';
EXECUTE CreateCity 206, 'Шуя', 'Ивановская область';
EXECUTE CreateCity 207, 'Южа', 'Ивановская область';
EXECUTE CreateCity 208, 'Юрьевец', 'Ивановская область';
EXECUTE CreateCity 209, 'Карабулак', 'Ингушетия';
EXECUTE CreateCity 210, 'Магас', 'Ингушетия';
EXECUTE CreateCity 211, 'Малгобек', 'Ингушетия';
EXECUTE CreateCity 212, 'Назрань', 'Ингушетия';
EXECUTE CreateCity 213, 'Алзамай', 'Иркутская область';
EXECUTE CreateCity 214, 'Ангарск', 'Иркутская область';
EXECUTE CreateCity 215, 'Байкальск', 'Иркутская область';
EXECUTE CreateCity 216, 'Бирюсинск', 'Иркутская область';
EXECUTE CreateCity 217, 'Бодайбо', 'Иркутская область';
EXECUTE CreateCity 218, 'Братск', 'Иркутская область';
EXECUTE CreateCity 219, 'Вихоревка', 'Иркутская область';
EXECUTE CreateCity 220, 'Железногорск-Илимский', 'Иркутская область';
EXECUTE CreateCity 221, 'Зима', 'Иркутская область';
EXECUTE CreateCity 222, 'Иркутск', 'Иркутская область';
EXECUTE CreateCity 223, 'Киренск', 'Иркутская область';
EXECUTE CreateCity 224, 'Нижнеудинск', 'Иркутская область';
EXECUTE CreateCity 225, 'Саянск', 'Иркутская область';
EXECUTE CreateCity 226, 'Свирск', 'Иркутская область';
EXECUTE CreateCity 227, 'Слюдянка', 'Иркутская область';
EXECUTE CreateCity 228, 'Тайшет', 'Иркутская область';
EXECUTE CreateCity 229, 'Тулун', 'Иркутская область';
EXECUTE CreateCity 230, 'Усолье-Сибирское', 'Иркутская область';
EXECUTE CreateCity 231, 'Усть-Илимск', 'Иркутская область';
EXECUTE CreateCity 232, 'Усть-Кут', 'Иркутская область';
EXECUTE CreateCity 233, 'Черемхово', 'Иркутская область';
EXECUTE CreateCity 234, 'Шелехов', 'Иркутская область';
EXECUTE CreateCity 235, 'Баксан', 'Кабардино-Балкария';
EXECUTE CreateCity 236, 'Майский', 'Кабардино-Балкария';
EXECUTE CreateCity 237, 'Нальчик', 'Кабардино-Балкария';
EXECUTE CreateCity 238, 'Нарткала', 'Кабардино-Балкария';
EXECUTE CreateCity 239, 'Прохладный', 'Кабардино-Балкария';
EXECUTE CreateCity 240, 'Терек', 'Кабардино-Балкария';
EXECUTE CreateCity 241, 'Тырныауз', 'Кабардино-Балкария';
EXECUTE CreateCity 242, 'Чегем', 'Кабардино-Балкария';
EXECUTE CreateCity 243, 'Багратионовск', 'Калининградская область';
EXECUTE CreateCity 244, 'Балтийск', 'Калининградская область';
EXECUTE CreateCity 245, 'Гвардейск', 'Калининградская область';
EXECUTE CreateCity 246, 'Гурьевск', 'Калининградская область';
EXECUTE CreateCity 247, 'Гусев', 'Калининградская область';
EXECUTE CreateCity 248, 'Зеленоградск', 'Калининградская область';
EXECUTE CreateCity 249, 'Калининград', 'Калининградская область';
EXECUTE CreateCity 250, 'Краснознаменск', 'Калининградская область';
EXECUTE CreateCity 251, 'Ладушкин', 'Калининградская область';
EXECUTE CreateCity 252, 'Мамоново', 'Калининградская область';
EXECUTE CreateCity 253, 'Неман', 'Калининградская область';
EXECUTE CreateCity 254, 'Нестеров', 'Калининградская область';
EXECUTE CreateCity 255, 'Озёрск', 'Калининградская область';
EXECUTE CreateCity 256, 'Пионерский', 'Калининградская область';
EXECUTE CreateCity 257, 'Полесск', 'Калининградская область';
EXECUTE CreateCity 258, 'Правдинск', 'Калининградская область';
EXECUTE CreateCity 259, 'Светлогорск', 'Калининградская область';
EXECUTE CreateCity 260, 'Светлый', 'Калининградская область';
EXECUTE CreateCity 261, 'Славск', 'Калининградская область';
EXECUTE CreateCity 262, 'Советск', 'Калининградская область';
EXECUTE CreateCity 263, 'Черняховск', 'Калининградская область';
EXECUTE CreateCity 264, 'Приморск', 'Калининградская область';
EXECUTE CreateCity 265, 'Городовиковск', 'Калмыкия';
EXECUTE CreateCity 266, 'Лагань', 'Калмыкия';
EXECUTE CreateCity 267, 'Элиста', 'Калмыкия';
EXECUTE CreateCity 268, 'Балабаново', 'Калужская область';
EXECUTE CreateCity 269, 'Белоусово', 'Калужская область';
EXECUTE CreateCity 270, 'Боровск', 'Калужская область';
EXECUTE CreateCity 271, 'Ермолино', 'Калужская область';
EXECUTE CreateCity 272, 'Жиздра', 'Калужская область';
EXECUTE CreateCity 273, 'Жуков', 'Калужская область';
EXECUTE CreateCity 274, 'Калуга', 'Калужская область';
EXECUTE CreateCity 275, 'Киров', 'Калужская область';
EXECUTE CreateCity 276, 'Козельск', 'Калужская область';
EXECUTE CreateCity 277, 'Кондрово', 'Калужская область';
EXECUTE CreateCity 278, 'Кремёнки', 'Калужская область';
EXECUTE CreateCity 279, 'Людиново', 'Калужская область';
EXECUTE CreateCity 280, 'Малоярославец', 'Калужская область';
EXECUTE CreateCity 281, 'Медынь', 'Калужская область';
EXECUTE CreateCity 282, 'Мещовск', 'Калужская область';
EXECUTE CreateCity 283, 'Мосальск', 'Калужская область';
EXECUTE CreateCity 284, 'Обнинск', 'Калужская область';
EXECUTE CreateCity 285, 'Сосенский', 'Калужская область';
EXECUTE CreateCity 286, 'Спас-Деменск', 'Калужская область';
EXECUTE CreateCity 287, 'Сухиничи', 'Калужская область';
EXECUTE CreateCity 288, 'Таруса', 'Калужская область';
EXECUTE CreateCity 289, 'Юхнов', 'Калужская область';
EXECUTE CreateCity 290, 'Вилючинск', 'Камчатский край';
EXECUTE CreateCity 291, 'Елизово', 'Камчатский край';
EXECUTE CreateCity 292, 'Петропавловск-Камчатский', 'Камчатский край';
EXECUTE CreateCity 293, 'Карачаевск', 'Карачаево-Черкесия';
EXECUTE CreateCity 294, 'Теберда', 'Карачаево-Черкесия';
EXECUTE CreateCity 295, 'Усть-Джегута', 'Карачаево-Черкесия';
EXECUTE CreateCity 296, 'Черкесск', 'Карачаево-Черкесия';
EXECUTE CreateCity 297, 'Беломорск', 'Карелия';
EXECUTE CreateCity 298, 'Кемь', 'Карелия';
EXECUTE CreateCity 299, 'Кондопога', 'Карелия';
EXECUTE CreateCity 300, 'Костомукша', 'Карелия';
EXECUTE CreateCity 301, 'Лахденпохья', 'Карелия';
EXECUTE CreateCity 302, 'Медвежьегорск', 'Карелия';
EXECUTE CreateCity 303, 'Олонец', 'Карелия';
EXECUTE CreateCity 304, 'Петрозаводск', 'Карелия';
EXECUTE CreateCity 305, 'Питкяранта', 'Карелия';
EXECUTE CreateCity 306, 'Пудож', 'Карелия';
EXECUTE CreateCity 307, 'Сегежа', 'Карелия';
EXECUTE CreateCity 308, 'Сортавала', 'Карелия';
EXECUTE CreateCity 309, 'Суоярви', 'Карелия';
EXECUTE CreateCity 310, 'Анжеро-Судженск', 'Кемеровская область';
EXECUTE CreateCity 311, 'Белово', 'Кемеровская область';
EXECUTE CreateCity 312, 'Берёзовский', 'Кемеровская область';
EXECUTE CreateCity 313, 'Гурьевск', 'Кемеровская область';
EXECUTE CreateCity 314, 'Калтан', 'Кемеровская область';
EXECUTE CreateCity 315, 'Кемерово', 'Кемеровская область';
EXECUTE CreateCity 316, 'Киселёвск', 'Кемеровская область';
EXECUTE CreateCity 317, 'Ленинск-Кузнецкий', 'Кемеровская область';
EXECUTE CreateCity 318, 'Мариинск', 'Кемеровская область';
EXECUTE CreateCity 319, 'Междуреченск', 'Кемеровская область';
EXECUTE CreateCity 320, 'Мыски', 'Кемеровская область';
EXECUTE CreateCity 321, 'Новокузнецк', 'Кемеровская область';
EXECUTE CreateCity 322, 'Осинники', 'Кемеровская область';
EXECUTE CreateCity 323, 'Полысаево', 'Кемеровская область';
EXECUTE CreateCity 324, 'Прокопьевск', 'Кемеровская область';
EXECUTE CreateCity 325, 'Салаир', 'Кемеровская область';
EXECUTE CreateCity 326, 'Тайга', 'Кемеровская область';
EXECUTE CreateCity 327, 'Таштагол', 'Кемеровская область';
EXECUTE CreateCity 328, 'Топки', 'Кемеровская область';
EXECUTE CreateCity 329, 'Юрга', 'Кемеровская область';
EXECUTE CreateCity 330, 'Белая Холуница', 'Кировская область';
EXECUTE CreateCity 331, 'Вятские Поляны', 'Кировская область';
EXECUTE CreateCity 332, 'Зуевка', 'Кировская область';
EXECUTE CreateCity 333, 'Киров', 'Кировская область';
EXECUTE CreateCity 334, 'Кирово-Чепецк', 'Кировская область';
EXECUTE CreateCity 335, 'Кирс', 'Кировская область';
EXECUTE CreateCity 336, 'Котельнич', 'Кировская область';
EXECUTE CreateCity 337, 'Луза', 'Кировская область';
EXECUTE CreateCity 338, 'Малмыж', 'Кировская область';
EXECUTE CreateCity 339, 'Мураши', 'Кировская область';
EXECUTE CreateCity 340, 'Нолинск', 'Кировская область';
EXECUTE CreateCity 341, 'Омутнинск', 'Кировская область';
EXECUTE CreateCity 342, 'Орлов', 'Кировская область';
EXECUTE CreateCity 343, 'Слободской', 'Кировская область';
EXECUTE CreateCity 344, 'Советск', 'Кировская область';
EXECUTE CreateCity 345, 'Сосновка', 'Кировская область';
EXECUTE CreateCity 346, 'Уржум', 'Кировская область';
EXECUTE CreateCity 347, 'Яранск', 'Кировская область';
EXECUTE CreateCity 348, 'Воркута', 'Коми';
EXECUTE CreateCity 349, 'Вуктыл', 'Коми';
EXECUTE CreateCity 350, 'Емва', 'Коми';
EXECUTE CreateCity 351, 'Инта', 'Коми';
EXECUTE CreateCity 352, 'Микунь', 'Коми';
EXECUTE CreateCity 353, 'Печора', 'Коми';
EXECUTE CreateCity 354, 'Сосногорск', 'Коми';
EXECUTE CreateCity 355, 'Сыктывкар', 'Коми';
EXECUTE CreateCity 356, 'Усинск', 'Коми';
EXECUTE CreateCity 357, 'Ухта', 'Коми';
EXECUTE CreateCity 358, 'Буй', 'Костромская область';
EXECUTE CreateCity 359, 'Волгореченск', 'Костромская область';
EXECUTE CreateCity 360, 'Галич', 'Костромская область';
EXECUTE CreateCity 361, 'Кологрив', 'Костромская область';
EXECUTE CreateCity 362, 'Кострома', 'Костромская область';
EXECUTE CreateCity 363, 'Макарьев', 'Костромская область';
EXECUTE CreateCity 364, 'Мантурово', 'Костромская область';
EXECUTE CreateCity 365, 'Нерехта', 'Костромская область';
EXECUTE CreateCity 366, 'Нея', 'Костромская область';
EXECUTE CreateCity 367, 'Солигалич', 'Костромская область';
EXECUTE CreateCity 368, 'Чухлома', 'Костромская область';
EXECUTE CreateCity 369, 'Шарья', 'Костромская область';
EXECUTE CreateCity 370, 'Абинск', 'Краснодарский край';
EXECUTE CreateCity 371, 'Анапа', 'Краснодарский край';
EXECUTE CreateCity 372, 'Апшеронск', 'Краснодарский край';
EXECUTE CreateCity 373, 'Армавир', 'Краснодарский край';
EXECUTE CreateCity 374, 'Белореченск', 'Краснодарский край';
EXECUTE CreateCity 375, 'Геленджик', 'Краснодарский край';
EXECUTE CreateCity 376, 'Горячий Ключ', 'Краснодарский край';
EXECUTE CreateCity 377, 'Гулькевичи', 'Краснодарский край';
EXECUTE CreateCity 378, 'Ейск', 'Краснодарский край';
EXECUTE CreateCity 379, 'Кореновск', 'Краснодарский край';
EXECUTE CreateCity 380, 'Краснодар', 'Краснодарский край';
EXECUTE CreateCity 381, 'Кропоткин', 'Краснодарский край';
EXECUTE CreateCity 382, 'Крымск', 'Краснодарский край';
EXECUTE CreateCity 383, 'Курганинск', 'Краснодарский край';
EXECUTE CreateCity 384, 'Лабинск', 'Краснодарский край';
EXECUTE CreateCity 385, 'Новокубанск', 'Краснодарский край';
EXECUTE CreateCity 386, 'Новороссийск', 'Краснодарский край';
EXECUTE CreateCity 387, 'Приморско-Ахтарск', 'Краснодарский край';
EXECUTE CreateCity 388, 'Славянск-на-Кубани', 'Краснодарский край';
EXECUTE CreateCity 389, 'Сочи', 'Краснодарский край';
EXECUTE CreateCity 390, 'Темрюк', 'Краснодарский край';
EXECUTE CreateCity 391, 'Тимашёвск', 'Краснодарский край';
EXECUTE CreateCity 392, 'Тихорецк', 'Краснодарский край';
EXECUTE CreateCity 393, 'Туапсе', 'Краснодарский край';
EXECUTE CreateCity 394, 'Усть-Лабинск', 'Краснодарский край';
EXECUTE CreateCity 395, 'Хадыженск', 'Краснодарский край';
EXECUTE CreateCity 396, 'Артёмовск', 'Красноярский край';
EXECUTE CreateCity 397, 'Ачинск', 'Красноярский край';
EXECUTE CreateCity 398, 'Боготол', 'Красноярский край';
EXECUTE CreateCity 399, 'Бородино', 'Красноярский край';
EXECUTE CreateCity 400, 'Дивногорск', 'Красноярский край';
EXECUTE CreateCity 401, 'Дудинка', 'Красноярский край';
EXECUTE CreateCity 402, 'Енисейск', 'Красноярский край';
EXECUTE CreateCity 403, 'Железногорск', 'Красноярский край';
EXECUTE CreateCity 404, 'Заозёрный', 'Красноярский край';
EXECUTE CreateCity 405, 'Зеленогорск', 'Красноярский край';
EXECUTE CreateCity 406, 'Игарка', 'Красноярский край';
EXECUTE CreateCity 407, 'Иланский', 'Красноярский край';
EXECUTE CreateCity 408, 'Канск', 'Красноярский край';
EXECUTE CreateCity 409, 'Кодинск', 'Красноярский край';
EXECUTE CreateCity 410, 'Красноярск', 'Красноярский край';
EXECUTE CreateCity 411, 'Лесосибирск', 'Красноярский край';
EXECUTE CreateCity 412, 'Минусинск', 'Красноярский край';
EXECUTE CreateCity 413, 'Назарово', 'Красноярский край';
EXECUTE CreateCity 414, 'Норильск', 'Красноярский край';
EXECUTE CreateCity 415, 'Сосновоборск', 'Красноярский край';
EXECUTE CreateCity 416, 'Ужур', 'Красноярский край';
EXECUTE CreateCity 417, 'Уяр', 'Красноярский край';
EXECUTE CreateCity 418, 'Шарыпово', 'Красноярский край';
EXECUTE CreateCity 419, 'Далматово', 'Курганская область';
EXECUTE CreateCity 420, 'Катайск', 'Курганская область';
EXECUTE CreateCity 421, 'Курган', 'Курганская область';
EXECUTE CreateCity 422, 'Куртамыш', 'Курганская область';
EXECUTE CreateCity 423, 'Макушино', 'Курганская область';
EXECUTE CreateCity 424, 'Петухово', 'Курганская область';
EXECUTE CreateCity 425, 'Шадринск', 'Курганская область';
EXECUTE CreateCity 426, 'Шумиха', 'Курганская область';
EXECUTE CreateCity 427, 'Щучье', 'Курганская область';
EXECUTE CreateCity 428, 'Дмитриев', 'Курская область';
EXECUTE CreateCity 429, 'Железногорск', 'Курская область';
EXECUTE CreateCity 430, 'Курск', 'Курская область';
EXECUTE CreateCity 431, 'Курчатов', 'Курская область';
EXECUTE CreateCity 432, 'Льгов', 'Курская область';
EXECUTE CreateCity 433, 'Обоянь', 'Курская область';
EXECUTE CreateCity 434, 'Рыльск', 'Курская область';
EXECUTE CreateCity 435, 'Суджа', 'Курская область';
EXECUTE CreateCity 436, 'Фатеж', 'Курская область';
EXECUTE CreateCity 437, 'Щигры', 'Курская область';
EXECUTE CreateCity 438, 'Бокситогорск', 'Ленинградская область';
EXECUTE CreateCity 439, 'Волосово', 'Ленинградская область';
EXECUTE CreateCity 440, 'Волхов', 'Ленинградская область';
EXECUTE CreateCity 441, 'Всеволожск', 'Ленинградская область';
EXECUTE CreateCity 442, 'Выборг', 'Ленинградская область';
EXECUTE CreateCity 443, 'Высоцк', 'Ленинградская область';
EXECUTE CreateCity 444, 'Гатчина', 'Ленинградская область';
EXECUTE CreateCity 445, 'Ивангород', 'Ленинградская область';
EXECUTE CreateCity 446, 'Каменногорск', 'Ленинградская область';
EXECUTE CreateCity 447, 'Кингисепп', 'Ленинградская область';
EXECUTE CreateCity 448, 'Кириши', 'Ленинградская область';
EXECUTE CreateCity 449, 'Кировск', 'Ленинградская область';
EXECUTE CreateCity 450, 'Коммунар', 'Ленинградская область';
EXECUTE CreateCity 451, 'Лодейное Поле', 'Ленинградская область';
EXECUTE CreateCity 452, 'Луга', 'Ленинградская область';
EXECUTE CreateCity 453, 'Любань', 'Ленинградская область';
EXECUTE CreateCity 454, 'Никольское', 'Ленинградская область';
EXECUTE CreateCity 455, 'Новая Ладога', 'Ленинградская область';
EXECUTE CreateCity 456, 'Отрадное', 'Ленинградская область';
EXECUTE CreateCity 457, 'Пикалёво', 'Ленинградская область';
EXECUTE CreateCity 458, 'Подпорожье', 'Ленинградская область';
EXECUTE CreateCity 459, 'Приморск', 'Ленинградская область';
EXECUTE CreateCity 460, 'Приозерск', 'Ленинградская область';
EXECUTE CreateCity 461, 'Светогорск', 'Ленинградская область';
EXECUTE CreateCity 462, 'Сертолово', 'Ленинградская область';
EXECUTE CreateCity 463, 'Сланцы', 'Ленинградская область';
EXECUTE CreateCity 464, 'Сосновый Бор', 'Ленинградская область';
EXECUTE CreateCity 465, 'Сясьстрой', 'Ленинградская область';
EXECUTE CreateCity 466, 'Тихвин', 'Ленинградская область';
EXECUTE CreateCity 467, 'Тосно', 'Ленинградская область';
EXECUTE CreateCity 468, 'Шлиссельбург', 'Ленинградская область';
EXECUTE CreateCity 469, 'Грязи', 'Липецкая область';
EXECUTE CreateCity 470, 'Данков', 'Липецкая область';
EXECUTE CreateCity 471, 'Елец', 'Липецкая область';
EXECUTE CreateCity 472, 'Задонск', 'Липецкая область';
EXECUTE CreateCity 473, 'Лебедянь', 'Липецкая область';
EXECUTE CreateCity 474, 'Липецк', 'Липецкая область';
EXECUTE CreateCity 475, 'Усмань', 'Липецкая область';
EXECUTE CreateCity 476, 'Чаплыгин', 'Липецкая область';
EXECUTE CreateCity 477, 'Магадан', 'Магаданская область';
EXECUTE CreateCity 478, 'Сусуман', 'Магаданская область';
EXECUTE CreateCity 479, 'Волжск', 'Марий Эл';
EXECUTE CreateCity 480, 'Звенигово', 'Марий Эл';
EXECUTE CreateCity 481, 'Йошкар-Ола', 'Марий Эл';
EXECUTE CreateCity 482, 'Козьмодемьянск', 'Марий Эл';
EXECUTE CreateCity 483, 'Ардатов', 'Мордовия';
EXECUTE CreateCity 484, 'Инсар', 'Мордовия';
EXECUTE CreateCity 485, 'Ковылкино', 'Мордовия';
EXECUTE CreateCity 486, 'Краснослободск', 'Мордовия';
EXECUTE CreateCity 487, 'Рузаевка', 'Мордовия';
EXECUTE CreateCity 488, 'Саранск', 'Мордовия';
EXECUTE CreateCity 489, 'Темников', 'Мордовия';
EXECUTE CreateCity 490, 'Москва', 'Москва';
EXECUTE CreateCity 491, 'Апрелевка', 'Московская область';
EXECUTE CreateCity 492, 'Балашиха', 'Московская область';
EXECUTE CreateCity 493, 'Бронницы', 'Московская область';
EXECUTE CreateCity 494, 'Верея', 'Московская область';
EXECUTE CreateCity 495, 'Видное', 'Московская область';
EXECUTE CreateCity 496, 'Волоколамск', 'Московская область';
EXECUTE CreateCity 497, 'Воскресенск', 'Московская область';
EXECUTE CreateCity 498, 'Высоковск', 'Московская область';
EXECUTE CreateCity 499, 'Голицыно', 'Московская область';
EXECUTE CreateCity 500, 'Дедовск', 'Московская область';
EXECUTE CreateCity 501, 'Дзержинский', 'Московская область';
EXECUTE CreateCity 502, 'Дмитров', 'Московская область';
EXECUTE CreateCity 503, 'Долгопрудный', 'Московская область';
EXECUTE CreateCity 504, 'Домодедово', 'Московская область';
EXECUTE CreateCity 505, 'Дрезна', 'Московская область';
EXECUTE CreateCity 506, 'Дубна', 'Московская область';
EXECUTE CreateCity 507, 'Егорьевск', 'Московская область';
EXECUTE CreateCity 508, 'Железнодорожный', 'Московская область';
EXECUTE CreateCity 509, 'Жуковский', 'Московская область';
EXECUTE CreateCity 510, 'Зарайск', 'Московская область';
EXECUTE CreateCity 511, 'Звенигород', 'Московская область';
EXECUTE CreateCity 512, 'Ивантеевка', 'Московская область';
EXECUTE CreateCity 513, 'Истра', 'Московская область';
EXECUTE CreateCity 514, 'Кашира', 'Московская область';
EXECUTE CreateCity 515, 'Климовск', 'Московская область';
EXECUTE CreateCity 516, 'Клин', 'Московская область';
EXECUTE CreateCity 517, 'Коломна', 'Московская область';
EXECUTE CreateCity 518, 'Котельники', 'Московская область';
EXECUTE CreateCity 519, 'Королёв', 'Московская область';
EXECUTE CreateCity 520, 'Красноармейск', 'Московская область';
EXECUTE CreateCity 521, 'Красногорск', 'Московская область';
EXECUTE CreateCity 522, 'Краснозаводск', 'Московская область';
EXECUTE CreateCity 523, 'Краснознаменск', 'Московская область';
EXECUTE CreateCity 524, 'Кубинка', 'Московская область';
EXECUTE CreateCity 525, 'Куровское', 'Московская область';
EXECUTE CreateCity 526, 'Ликино-Дулёво', 'Московская область';
EXECUTE CreateCity 527, 'Лобня', 'Московская область';
EXECUTE CreateCity 528, 'Лосино-Петровский', 'Московская область';
EXECUTE CreateCity 529, 'Луховицы', 'Московская область';
EXECUTE CreateCity 530, 'Лыткарино', 'Московская область';
EXECUTE CreateCity 531, 'Люберцы', 'Московская область';
EXECUTE CreateCity 532, 'Можайск', 'Московская область';
EXECUTE CreateCity 533, 'Московский', 'Москва';
EXECUTE CreateCity 534, 'Мытищи', 'Московская область';
EXECUTE CreateCity 535, 'Наро-Фоминск', 'Московская область';
EXECUTE CreateCity 536, 'Ногинск', 'Московская область';
EXECUTE CreateCity 537, 'Одинцово', 'Московская область';
EXECUTE CreateCity 538, 'Ожерелье', 'Московская область';
EXECUTE CreateCity 539, 'Озёры', 'Московская область';
EXECUTE CreateCity 540, 'Орехово-Зуево', 'Московская область';
EXECUTE CreateCity 541, 'Павловский Посад', 'Московская область';
EXECUTE CreateCity 542, 'Пересвет', 'Московская область';
EXECUTE CreateCity 543, 'Подольск', 'Московская область';
EXECUTE CreateCity 544, 'Протвино', 'Московская область';
EXECUTE CreateCity 545, 'Пушкино', 'Московская область';
EXECUTE CreateCity 546, 'Пущино', 'Московская область';
EXECUTE CreateCity 547, 'Раменское', 'Московская область';
EXECUTE CreateCity 548, 'Реутов', 'Московская область';
EXECUTE CreateCity 549, 'Рошаль', 'Московская область';
EXECUTE CreateCity 550, 'Руза', 'Московская область';
EXECUTE CreateCity 551, 'Сергиев Посад', 'Московская область';
EXECUTE CreateCity 552, 'Серпухов', 'Московская область';
EXECUTE CreateCity 553, 'Солнечногорск', 'Московская область';
EXECUTE CreateCity 554, 'Старая Купавна', 'Московская область';
EXECUTE CreateCity 555, 'Ступино', 'Московская область';
EXECUTE CreateCity 556, 'Талдом', 'Московская область';
EXECUTE CreateCity 557, 'Троицк', 'Москва';
EXECUTE CreateCity 558, 'Фрязино', 'Московская область';
EXECUTE CreateCity 559, 'Химки', 'Московская область';
EXECUTE CreateCity 560, 'Хотьково', 'Московская область';
EXECUTE CreateCity 561, 'Черноголовка', 'Московская область';
EXECUTE CreateCity 562, 'Чехов', 'Московская область';
EXECUTE CreateCity 563, 'Шатура', 'Московская область';
EXECUTE CreateCity 564, 'Щёлково', 'Московская область';
EXECUTE CreateCity 565, 'Щербинка', 'Москва';
EXECUTE CreateCity 566, 'Электрогорск', 'Московская область';
EXECUTE CreateCity 567, 'Электросталь', 'Московская область';
EXECUTE CreateCity 568, 'Электроугли', 'Московская область';
EXECUTE CreateCity 569, 'Юбилейный', 'Московская область';
EXECUTE CreateCity 570, 'Яхрома', 'Московская область';
EXECUTE CreateCity 571, 'Апатиты', 'Мурманская область';
EXECUTE CreateCity 572, 'Гаджиево', 'Мурманская область';
EXECUTE CreateCity 573, 'Заозёрск', 'Мурманская область';
EXECUTE CreateCity 574, 'Заполярный', 'Мурманская область';
EXECUTE CreateCity 575, 'Кандалакша', 'Мурманская область';
EXECUTE CreateCity 576, 'Кировск', 'Мурманская область';
EXECUTE CreateCity 577, 'Ковдор', 'Мурманская область';
EXECUTE CreateCity 578, 'Кола', 'Мурманская область';
EXECUTE CreateCity 579, 'Мончегорск', 'Мурманская область';
EXECUTE CreateCity 580, 'Мурманск', 'Мурманская область';
EXECUTE CreateCity 581, 'Оленегорск', 'Мурманская область';
EXECUTE CreateCity 582, 'Островной', 'Мурманская область';
EXECUTE CreateCity 583, 'Полярные Зори', 'Мурманская область';
EXECUTE CreateCity 584, 'Полярный', 'Мурманская область';
EXECUTE CreateCity 585, 'Североморск', 'Мурманская область';
EXECUTE CreateCity 586, 'Снежногорск', 'Мурманская область';
EXECUTE CreateCity 587, 'Нарьян-Мар', 'Ненецкий Авт. Окр.';
EXECUTE CreateCity 588, 'Арзамас', 'Нижегородская область';
EXECUTE CreateCity 589, 'Балахна', 'Нижегородская область';
EXECUTE CreateCity 590, 'Богородск', 'Нижегородская область';
EXECUTE CreateCity 591, 'Бор', 'Нижегородская область';
EXECUTE CreateCity 592, 'Ветлуга', 'Нижегородская область';
EXECUTE CreateCity 593, 'Володарск', 'Нижегородская область';
EXECUTE CreateCity 594, 'Ворсма', 'Нижегородская область';
EXECUTE CreateCity 595, 'Выкса', 'Нижегородская область';
EXECUTE CreateCity 596, 'Горбатов', 'Нижегородская область';
EXECUTE CreateCity 597, 'Городец', 'Нижегородская область';
EXECUTE CreateCity 598, 'Дзержинск', 'Нижегородская область';
EXECUTE CreateCity 599, 'Заволжье', 'Нижегородская область';
EXECUTE CreateCity 600, 'Княгинино', 'Нижегородская область';
EXECUTE CreateCity 601, 'Кстово', 'Нижегородская область';
EXECUTE CreateCity 602, 'Кулебаки', 'Нижегородская область';
EXECUTE CreateCity 603, 'Лукоянов', 'Нижегородская область';
EXECUTE CreateCity 604, 'Лысково', 'Нижегородская область';
EXECUTE CreateCity 605, 'Навашино', 'Нижегородская область';
EXECUTE CreateCity 606, 'Нижний Новгород', 'Нижегородская область';
EXECUTE CreateCity 607, 'Павлово', 'Нижегородская область';
EXECUTE CreateCity 608, 'Первомайск', 'Нижегородская область';
EXECUTE CreateCity 609, 'Перевоз', 'Нижегородская область';
EXECUTE CreateCity 610, 'Саров', 'Нижегородская область';
EXECUTE CreateCity 611, 'Семёнов', 'Нижегородская область';
EXECUTE CreateCity 612, 'Сергач', 'Нижегородская область';
EXECUTE CreateCity 613, 'Урень', 'Нижегородская область';
EXECUTE CreateCity 614, 'Чкаловск', 'Нижегородская область';
EXECUTE CreateCity 615, 'Шахунья', 'Нижегородская область';
EXECUTE CreateCity 616, 'Боровичи', 'Новгородская область';
EXECUTE CreateCity 617, 'Валдай', 'Новгородская область';
EXECUTE CreateCity 618, 'Великий Новгород', 'Новгородская область';
EXECUTE CreateCity 619, 'Малая Вишера', 'Новгородская область';
EXECUTE CreateCity 620, 'Окуловка', 'Новгородская область';
EXECUTE CreateCity 621, 'Пестово', 'Новгородская область';
EXECUTE CreateCity 622, 'Сольцы', 'Новгородская область';
EXECUTE CreateCity 623, 'Старая Русса', 'Новгородская область';
EXECUTE CreateCity 624, 'Холм', 'Новгородская область';
EXECUTE CreateCity 625, 'Чудово', 'Новгородская область';
EXECUTE CreateCity 626, 'Барабинск', 'Новосибирская область';
EXECUTE CreateCity 627, 'Бердск', 'Новосибирская область';
EXECUTE CreateCity 628, 'Болотное', 'Новосибирская область';
EXECUTE CreateCity 629, 'Искитим', 'Новосибирская область';
EXECUTE CreateCity 630, 'Карасук', 'Новосибирская область';
EXECUTE CreateCity 631, 'Каргат', 'Новосибирская область';
EXECUTE CreateCity 632, 'Куйбышев', 'Новосибирская область';
EXECUTE CreateCity 633, 'Купино', 'Новосибирская область';
EXECUTE CreateCity 634, 'Новосибирск', 'Новосибирская область';
EXECUTE CreateCity 635, 'Обь', 'Новосибирская область';
EXECUTE CreateCity 636, 'Татарск', 'Новосибирская область';
EXECUTE CreateCity 637, 'Тогучин', 'Новосибирская область';
EXECUTE CreateCity 638, 'Черепаново', 'Новосибирская область';
EXECUTE CreateCity 639, 'Чулым', 'Новосибирская область';
EXECUTE CreateCity 640, 'Исилькуль', 'Омская область';
EXECUTE CreateCity 641, 'Калачинск', 'Омская область';
EXECUTE CreateCity 642, 'Называевск', 'Омская область';
EXECUTE CreateCity 643, 'Омск', 'Омская область';
EXECUTE CreateCity 644, 'Тара', 'Омская область';
EXECUTE CreateCity 645, 'Тюкалинск', 'Омская область';
EXECUTE CreateCity 646, 'Абдулино', 'Оренбургская область';
EXECUTE CreateCity 647, 'Бугуруслан', 'Оренбургская область';
EXECUTE CreateCity 648, 'Бузулук', 'Оренбургская область';
EXECUTE CreateCity 649, 'Гай', 'Оренбургская область';
EXECUTE CreateCity 650, 'Кувандык', 'Оренбургская область';
EXECUTE CreateCity 651, 'Медногорск', 'Оренбургская область';
EXECUTE CreateCity 652, 'Новотроицк', 'Оренбургская область';
EXECUTE CreateCity 653, 'Оренбург', 'Оренбургская область';
EXECUTE CreateCity 654, 'Орск', 'Оренбургская область';
EXECUTE CreateCity 655, 'Соль-Илецк', 'Оренбургская область';
EXECUTE CreateCity 656, 'Сорочинск', 'Оренбургская область';
EXECUTE CreateCity 657, 'Ясный', 'Оренбургская область';
EXECUTE CreateCity 658, 'Болхов', 'Орловская область';
EXECUTE CreateCity 659, 'Дмитровск', 'Орловская область';
EXECUTE CreateCity 660, 'Ливны', 'Орловская область';
EXECUTE CreateCity 661, 'Малоархангельск', 'Орловская область';
EXECUTE CreateCity 662, 'Мценск', 'Орловская область';
EXECUTE CreateCity 663, 'Новосиль', 'Орловская область';
EXECUTE CreateCity 664, 'Орёл', 'Орловская область';
EXECUTE CreateCity 665, 'Белинский', 'Пензенская область';
EXECUTE CreateCity 666, 'Городище', 'Пензенская область';
EXECUTE CreateCity 667, 'Заречный', 'Пензенская область';
EXECUTE CreateCity 668, 'Каменка', 'Пензенская область';
EXECUTE CreateCity 669, 'Кузнецк', 'Пензенская область';
EXECUTE CreateCity 670, 'Нижний Ломов', 'Пензенская область';
EXECUTE CreateCity 671, 'Никольск', 'Пензенская область';
EXECUTE CreateCity 672, 'Пенза', 'Пензенская область';
EXECUTE CreateCity 673, 'Сердобск', 'Пензенская область';
EXECUTE CreateCity 674, 'Спасск', 'Пензенская область';
EXECUTE CreateCity 675, 'Сурск', 'Пензенская область';
EXECUTE CreateCity 676, 'Александровск', 'Пермский край';
EXECUTE CreateCity 677, 'Березники', 'Пермский край';
EXECUTE CreateCity 678, 'Верещагино', 'Пермский край';
EXECUTE CreateCity 679, 'Горнозаводск', 'Пермский край';
EXECUTE CreateCity 680, 'Гремячинск', 'Пермский край';
EXECUTE CreateCity 681, 'Губаха', 'Пермский край';
EXECUTE CreateCity 682, 'Добрянка', 'Пермский край';
EXECUTE CreateCity 683, 'Кизел', 'Пермский край';
EXECUTE CreateCity 684, 'Красновишерск', 'Пермский край';
EXECUTE CreateCity 685, 'Краснокамск', 'Пермский край';
EXECUTE CreateCity 686, 'Кудымкар', 'Пермский край';
EXECUTE CreateCity 687, 'Кунгур', 'Пермский край';
EXECUTE CreateCity 688, 'Лысьва', 'Пермский край';
EXECUTE CreateCity 689, 'Нытва', 'Пермский край';
EXECUTE CreateCity 690, 'Оса', 'Пермский край';
EXECUTE CreateCity 691, 'Оханск', 'Пермский край';
EXECUTE CreateCity 692, 'Очёр', 'Пермский край';
EXECUTE CreateCity 693, 'Пермь', 'Пермский край';
EXECUTE CreateCity 694, 'Соликамск', 'Пермский край';
EXECUTE CreateCity 695, 'Усолье', 'Пермский край';
EXECUTE CreateCity 696, 'Чайковский', 'Пермский край';
EXECUTE CreateCity 697, 'Чердынь', 'Пермский край';
EXECUTE CreateCity 698, 'Чёрмоз', 'Пермский край';
EXECUTE CreateCity 699, 'Чернушка', 'Пермский край';
EXECUTE CreateCity 700, 'Чусовой', 'Пермский край';
EXECUTE CreateCity 701, 'Арсеньев', 'Приморский край';
EXECUTE CreateCity 702, 'Артём', 'Приморский край';
EXECUTE CreateCity 703, 'Большой Камень', 'Приморский край';
EXECUTE CreateCity 704, 'Владивосток', 'Приморский край';
EXECUTE CreateCity 705, 'Дальнегорск', 'Приморский край';
EXECUTE CreateCity 706, 'Дальнереченск', 'Приморский край';
EXECUTE CreateCity 707, 'Лесозаводск', 'Приморский край';
EXECUTE CreateCity 708, 'Находка', 'Приморский край';
EXECUTE CreateCity 709, 'Партизанск', 'Приморский край';
EXECUTE CreateCity 710, 'Спасск-Дальний', 'Приморский край';
EXECUTE CreateCity 711, 'Уссурийск', 'Приморский край';
EXECUTE CreateCity 712, 'Фокино', 'Приморский край';
EXECUTE CreateCity 713, 'Великие Луки', 'Псковская область';
EXECUTE CreateCity 714, 'Гдов', 'Псковская область';
EXECUTE CreateCity 715, 'Дно', 'Псковская область';
EXECUTE CreateCity 716, 'Невель', 'Псковская область';
EXECUTE CreateCity 717, 'Новоржев', 'Псковская область';
EXECUTE CreateCity 718, 'Новосокольники', 'Псковская область';
EXECUTE CreateCity 719, 'Опочка', 'Псковская область';
EXECUTE CreateCity 720, 'Остров', 'Псковская область';
EXECUTE CreateCity 721, 'Печоры', 'Псковская область';
EXECUTE CreateCity 722, 'Порхов', 'Псковская область';
EXECUTE CreateCity 723, 'Псков', 'Псковская область';
EXECUTE CreateCity 724, 'Пустошка', 'Псковская область';
EXECUTE CreateCity 725, 'Пыталово', 'Псковская область';
EXECUTE CreateCity 726, 'Себеж', 'Псковская область';
EXECUTE CreateCity 727, 'Азов', 'Ростовская область';
EXECUTE CreateCity 728, 'Аксай', 'Ростовская область';
EXECUTE CreateCity 729, 'Батайск', 'Ростовская область';
EXECUTE CreateCity 730, 'Белая Калитва', 'Ростовская область';
EXECUTE CreateCity 731, 'Волгодонск', 'Ростовская область';
EXECUTE CreateCity 732, 'Гуково', 'Ростовская область';
EXECUTE CreateCity 733, 'Донецк', 'Ростовская область';
EXECUTE CreateCity 734, 'Зверево', 'Ростовская область';
EXECUTE CreateCity 735, 'Зерноград', 'Ростовская область';
EXECUTE CreateCity 736, 'Каменск-Шахтинский', 'Ростовская область';
EXECUTE CreateCity 737, 'Константиновск', 'Ростовская область';
EXECUTE CreateCity 738, 'Красный Сулин', 'Ростовская область';
EXECUTE CreateCity 739, 'Миллерово', 'Ростовская область';
EXECUTE CreateCity 740, 'Морозовск', 'Ростовская область';
EXECUTE CreateCity 741, 'Новочеркасск', 'Ростовская область';
EXECUTE CreateCity 742, 'Новошахтинск', 'Ростовская область';
EXECUTE CreateCity 743, 'Пролетарск', 'Ростовская область';
EXECUTE CreateCity 744, 'Ростов-на-Дону', 'Ростовская область';
EXECUTE CreateCity 745, 'Сальск', 'Ростовская область';
EXECUTE CreateCity 746, 'Семикаракорск', 'Ростовская область';
EXECUTE CreateCity 747, 'Таганрог', 'Ростовская область';
EXECUTE CreateCity 748, 'Цимлянск', 'Ростовская область';
EXECUTE CreateCity 749, 'Шахты', 'Ростовская область';
EXECUTE CreateCity 750, 'Касимов', 'Рязанская область';
EXECUTE CreateCity 751, 'Кораблино', 'Рязанская область';
EXECUTE CreateCity 752, 'Михайлов', 'Рязанская область';
EXECUTE CreateCity 753, 'Новомичуринск', 'Рязанская область';
EXECUTE CreateCity 754, 'Рыбное', 'Рязанская область';
EXECUTE CreateCity 755, 'Ряжск', 'Рязанская область';
EXECUTE CreateCity 756, 'Рязань', 'Рязанская область';
EXECUTE CreateCity 757, 'Сасово', 'Рязанская область';
EXECUTE CreateCity 758, 'Скопин', 'Рязанская область';
EXECUTE CreateCity 759, 'Спас-Клепики', 'Рязанская область';
EXECUTE CreateCity 760, 'Спасск-Рязанский', 'Рязанская область';
EXECUTE CreateCity 761, 'Шацк', 'Рязанская область';
EXECUTE CreateCity 762, 'Жигулёвск', 'Самарская область';
EXECUTE CreateCity 763, 'Кинель', 'Самарская область';
EXECUTE CreateCity 764, 'Нефтегорск', 'Самарская область';
EXECUTE CreateCity 765, 'Новокуйбышевск', 'Самарская область';
EXECUTE CreateCity 766, 'Октябрьск', 'Самарская область';
EXECUTE CreateCity 767, 'Отрадный', 'Самарская область';
EXECUTE CreateCity 768, 'Похвистнево', 'Самарская область';
EXECUTE CreateCity 769, 'Самара', 'Самарская область';
EXECUTE CreateCity 770, 'Сызрань', 'Самарская область';
EXECUTE CreateCity 771, 'Тольятти', 'Самарская область';
EXECUTE CreateCity 772, 'Чапаевск', 'Самарская область';
EXECUTE CreateCity 773, 'Зеленогорск', 'Санкт-Петербург';
EXECUTE CreateCity 774, 'Колпино', 'Санкт-Петербург';
EXECUTE CreateCity 775, 'Красное Село', 'Санкт-Петербург';
EXECUTE CreateCity 776, 'Кронштадт', 'Санкт-Петербург';
EXECUTE CreateCity 777, 'Ломоносов', 'Санкт-Петербург';
EXECUTE CreateCity 778, 'Павловск', 'Санкт-Петербург';
EXECUTE CreateCity 779, 'Петергоф', 'Санкт-Петербург';
EXECUTE CreateCity 780, 'Пушкин', 'Санкт-Петербург';
EXECUTE CreateCity 781, 'Санкт-Петербург', 'Санкт-Петербург';
EXECUTE CreateCity 782, 'Сестрорецк', 'Санкт-Петербург';
EXECUTE CreateCity 783, 'Аркадак', 'Саратовская область';
EXECUTE CreateCity 784, 'Аткарск', 'Саратовская область';
EXECUTE CreateCity 785, 'Балаково', 'Саратовская область';
EXECUTE CreateCity 786, 'Балашов', 'Саратовская область';
EXECUTE CreateCity 787, 'Вольск', 'Саратовская область';
EXECUTE CreateCity 788, 'Ершов', 'Саратовская область';
EXECUTE CreateCity 789, 'Калининск', 'Саратовская область';
EXECUTE CreateCity 790, 'Красноармейск', 'Саратовская область';
EXECUTE CreateCity 791, 'Красный Кут', 'Саратовская область';
EXECUTE CreateCity 792, 'Маркс', 'Саратовская область';
EXECUTE CreateCity 793, 'Новоузенск', 'Саратовская область';
EXECUTE CreateCity 794, 'Петровск', 'Саратовская область';
EXECUTE CreateCity 795, 'Пугачёв', 'Саратовская область';
EXECUTE CreateCity 796, 'Ртищево', 'Саратовская область';
EXECUTE CreateCity 797, 'Саратов', 'Саратовская область';
EXECUTE CreateCity 798, 'Хвалынск', 'Саратовская область';
EXECUTE CreateCity 799, 'Шиханы', 'Саратовская область';
EXECUTE CreateCity 800, 'Энгельс', 'Саратовская область';
EXECUTE CreateCity 801, 'Александровск-Сахалинский', 'Сахалинская область';
EXECUTE CreateCity 802, 'Анива', 'Сахалинская область';
EXECUTE CreateCity 803, 'Долинск', 'Сахалинская область';
EXECUTE CreateCity 804, 'Корсаков', 'Сахалинская область';
EXECUTE CreateCity 805, 'Курильск', 'Сахалинская область';
EXECUTE CreateCity 806, 'Макаров', 'Сахалинская область';
EXECUTE CreateCity 807, 'Невельск', 'Сахалинская область';
EXECUTE CreateCity 808, 'Оха', 'Сахалинская область';
EXECUTE CreateCity 809, 'Поронайск', 'Сахалинская область';
EXECUTE CreateCity 810, 'Северо-Курильск', 'Сахалинская область';
EXECUTE CreateCity 811, 'Томари', 'Сахалинская область';
EXECUTE CreateCity 812, 'Углегорск', 'Сахалинская область';
EXECUTE CreateCity 813, 'Холмск', 'Сахалинская область';
EXECUTE CreateCity 814, 'Шахтёрск', 'Сахалинская область';
EXECUTE CreateCity 815, 'Южно-Сахалинск', 'Сахалинская область';
EXECUTE CreateCity 816, 'Алапаевск', 'Свердловская область';
EXECUTE CreateCity 817, 'Арамиль', 'Свердловская область';
EXECUTE CreateCity 818, 'Артёмовский', 'Свердловская область';
EXECUTE CreateCity 819, 'Асбест', 'Свердловская область';
EXECUTE CreateCity 820, 'Берёзовский', 'Свердловская область';
EXECUTE CreateCity 821, 'Богданович', 'Свердловская область';
EXECUTE CreateCity 822, 'Верхний Тагил', 'Свердловская область';
EXECUTE CreateCity 823, 'Верхняя Пышма', 'Свердловская область';
EXECUTE CreateCity 824, 'Верхняя Салда', 'Свердловская область';
EXECUTE CreateCity 825, 'Верхняя Тура', 'Свердловская область';
EXECUTE CreateCity 826, 'Верхотурье', 'Свердловская область';
EXECUTE CreateCity 827, 'Волчанск', 'Свердловская область';
EXECUTE CreateCity 828, 'Дегтярск', 'Свердловская область';
EXECUTE CreateCity 829, 'Екатеринбург', 'Свердловская область';
EXECUTE CreateCity 830, 'Заречный', 'Свердловская область';
EXECUTE CreateCity 831, 'Ивдель', 'Свердловская область';
EXECUTE CreateCity 832, 'Ирбит', 'Свердловская область';
EXECUTE CreateCity 833, 'Каменск-Уральский', 'Свердловская область';
EXECUTE CreateCity 834, 'Камышлов', 'Свердловская область';
EXECUTE CreateCity 835, 'Карпинск', 'Свердловская область';
EXECUTE CreateCity 836, 'Качканар', 'Свердловская область';
EXECUTE CreateCity 837, 'Кировград', 'Свердловская область';
EXECUTE CreateCity 838, 'Краснотурьинск', 'Свердловская область';
EXECUTE CreateCity 839, 'Красноуральск', 'Свердловская область';
EXECUTE CreateCity 840, 'Красноуфимск', 'Свердловская область';
EXECUTE CreateCity 841, 'Кушва', 'Свердловская область';
EXECUTE CreateCity 842, 'Лесной', 'Свердловская область';
EXECUTE CreateCity 843, 'Михайловск', 'Свердловская область';
EXECUTE CreateCity 844, 'Невьянск', 'Свердловская область';
EXECUTE CreateCity 845, 'Нижние Серги', 'Свердловская область';
EXECUTE CreateCity 846, 'Нижний Тагил', 'Свердловская область';
EXECUTE CreateCity 847, 'Нижняя Салда', 'Свердловская область';
EXECUTE CreateCity 848, 'Нижняя Тура', 'Свердловская область';
EXECUTE CreateCity 849, 'Новая Ляля', 'Свердловская область';
EXECUTE CreateCity 850, 'Новоуральск', 'Свердловская область';
EXECUTE CreateCity 851, 'Первоуральск', 'Свердловская область';
EXECUTE CreateCity 852, 'Полевской', 'Свердловская область';
EXECUTE CreateCity 853, 'Ревда', 'Свердловская область';
EXECUTE CreateCity 854, 'Реж', 'Свердловская область';
EXECUTE CreateCity 855, 'Североуральск', 'Свердловская область';
EXECUTE CreateCity 856, 'Серов', 'Свердловская область';
EXECUTE CreateCity 857, 'Среднеуральск', 'Свердловская область';
EXECUTE CreateCity 858, 'Сухой Лог', 'Свердловская область';
EXECUTE CreateCity 859, 'Сысерть', 'Свердловская область';
EXECUTE CreateCity 860, 'Тавда', 'Свердловская область';
EXECUTE CreateCity 861, 'Талица', 'Свердловская область';
EXECUTE CreateCity 862, 'Туринск', 'Свердловская область';
EXECUTE CreateCity 863, 'Алагир', 'Северная Осетия - Алания';
EXECUTE CreateCity 864, 'Ардон', 'Северная Осетия - Алания';
EXECUTE CreateCity 865, 'Беслан', 'Северная Осетия - Алания';
EXECUTE CreateCity 866, 'Владикавказ', 'Северная Осетия - Алания';
EXECUTE CreateCity 867, 'Дигора', 'Северная Осетия - Алания';
EXECUTE CreateCity 868, 'Моздок', 'Северная Осетия - Алания';
EXECUTE CreateCity 869, 'Велиж', 'Смоленская область';
EXECUTE CreateCity 870, 'Вязьма', 'Смоленская область';
EXECUTE CreateCity 871, 'Гагарин', 'Смоленская область';
EXECUTE CreateCity 872, 'Демидов', 'Смоленская область';
EXECUTE CreateCity 873, 'Десногорск', 'Смоленская область';
EXECUTE CreateCity 874, 'Дорогобуж', 'Смоленская область';
EXECUTE CreateCity 875, 'Духовщина', 'Смоленская область';
EXECUTE CreateCity 876, 'Ельня', 'Смоленская область';
EXECUTE CreateCity 877, 'Починок', 'Смоленская область';
EXECUTE CreateCity 878, 'Рославль', 'Смоленская область';
EXECUTE CreateCity 879, 'Рудня', 'Смоленская область';
EXECUTE CreateCity 880, 'Сафоново', 'Смоленская область';
EXECUTE CreateCity 881, 'Смоленск', 'Смоленская область';
EXECUTE CreateCity 882, 'Сычёвка', 'Смоленская область';
EXECUTE CreateCity 883, 'Ярцево', 'Смоленская область';
EXECUTE CreateCity 884, 'Благодарный', 'Ставропольский край';
EXECUTE CreateCity 885, 'Будённовск', 'Ставропольский край';
EXECUTE CreateCity 886, 'Георгиевск', 'Ставропольский край';
EXECUTE CreateCity 887, 'Ессентуки', 'Ставропольский край';
EXECUTE CreateCity 888, 'Железноводск', 'Ставропольский край';
EXECUTE CreateCity 889, 'Зеленокумск', 'Ставропольский край';
EXECUTE CreateCity 890, 'Изобильный', 'Ставропольский край';
EXECUTE CreateCity 891, 'Ипатово', 'Ставропольский край';
EXECUTE CreateCity 892, 'Кисловодск', 'Ставропольский край';
EXECUTE CreateCity 893, 'Лермонтов', 'Ставропольский край';
EXECUTE CreateCity 894, 'Минеральные Воды', 'Ставропольский край';
EXECUTE CreateCity 895, 'Михайловск', 'Ставропольский край';
EXECUTE CreateCity 896, 'Невинномысск', 'Ставропольский край';
EXECUTE CreateCity 897, 'Нефтекумск', 'Ставропольский край';
EXECUTE CreateCity 898, 'Новоалександровск', 'Ставропольский край';
EXECUTE CreateCity 899, 'Новопавловск', 'Ставропольский край';
EXECUTE CreateCity 900, 'Пятигорск', 'Ставропольский край';
EXECUTE CreateCity 901, 'Светлоград', 'Ставропольский край';
EXECUTE CreateCity 902, 'Ставрополь', 'Ставропольский край';
EXECUTE CreateCity 903, 'Жердевка', 'Тамбовская область';
EXECUTE CreateCity 904, 'Кирсанов', 'Тамбовская область';
EXECUTE CreateCity 905, 'Котовск', 'Тамбовская область';
EXECUTE CreateCity 906, 'Мичуринск', 'Тамбовская область';
EXECUTE CreateCity 907, 'Моршанск', 'Тамбовская область';
EXECUTE CreateCity 908, 'Рассказово', 'Тамбовская область';
EXECUTE CreateCity 909, 'Тамбов', 'Тамбовская область';
EXECUTE CreateCity 910, 'Уварово', 'Тамбовская область';
EXECUTE CreateCity 911, 'Агрыз', 'Татарстан';
EXECUTE CreateCity 912, 'Азнакаево', 'Татарстан';
EXECUTE CreateCity 913, 'Альметьевск', 'Татарстан';
EXECUTE CreateCity 914, 'Арск', 'Татарстан';
EXECUTE CreateCity 915, 'Бавлы', 'Татарстан';
EXECUTE CreateCity 916, 'Болгар', 'Татарстан';
EXECUTE CreateCity 917, 'Бугульма', 'Татарстан';
EXECUTE CreateCity 918, 'Буинск', 'Татарстан';
EXECUTE CreateCity 919, 'Елабуга', 'Татарстан';
EXECUTE CreateCity 920, 'Заинск', 'Татарстан';
EXECUTE CreateCity 921, 'Зеленодольск', 'Татарстан';
EXECUTE CreateCity 922, 'Казань', 'Татарстан';
EXECUTE CreateCity 923, 'Лаишево', 'Татарстан';
EXECUTE CreateCity 924, 'Лениногорск', 'Татарстан';
EXECUTE CreateCity 925, 'Мамадыш', 'Татарстан';
EXECUTE CreateCity 926, 'Менделеевск', 'Татарстан';
EXECUTE CreateCity 927, 'Мензелинск', 'Татарстан';
EXECUTE CreateCity 928, 'Набережные Челны', 'Татарстан';
EXECUTE CreateCity 929, 'Нижнекамск', 'Татарстан';
EXECUTE CreateCity 930, 'Нурлат', 'Татарстан';
EXECUTE CreateCity 931, 'Тетюши', 'Татарстан';
EXECUTE CreateCity 932, 'Чистополь', 'Татарстан';
EXECUTE CreateCity 933, 'Андреаполь', 'Тверская область';
EXECUTE CreateCity 934, 'Бежецк', 'Тверская область';
EXECUTE CreateCity 935, 'Белый', 'Тверская область';
EXECUTE CreateCity 936, 'Бологое', 'Тверская область';
EXECUTE CreateCity 937, 'Весьегонск', 'Тверская область';
EXECUTE CreateCity 938, 'Вышний Волочёк', 'Тверская область';
EXECUTE CreateCity 939, 'Западная Двина', 'Тверская область';
EXECUTE CreateCity 940, 'Зубцов', 'Тверская область';
EXECUTE CreateCity 941, 'Калязин', 'Тверская область';
EXECUTE CreateCity 942, 'Кашин', 'Тверская область';
EXECUTE CreateCity 943, 'Кимры', 'Тверская область';
EXECUTE CreateCity 944, 'Конаково', 'Тверская область';
EXECUTE CreateCity 945, 'Красный Холм', 'Тверская область';
EXECUTE CreateCity 946, 'Кувшиново', 'Тверская область';
EXECUTE CreateCity 947, 'Лихославль', 'Тверская область';
EXECUTE CreateCity 948, 'Нелидово', 'Тверская область';
EXECUTE CreateCity 949, 'Осташков', 'Тверская область';
EXECUTE CreateCity 950, 'Ржев', 'Тверская область';
EXECUTE CreateCity 951, 'Старица', 'Тверская область';
EXECUTE CreateCity 952, 'Тверь', 'Тверская область';
EXECUTE CreateCity 953, 'Торжок', 'Тверская область';
EXECUTE CreateCity 954, 'Торопец', 'Тверская область';
EXECUTE CreateCity 955, 'Удомля', 'Тверская область';
EXECUTE CreateCity 956, 'Асино', 'Томская область';
EXECUTE CreateCity 957, 'Кедровый', 'Томская область';
EXECUTE CreateCity 958, 'Колпашево', 'Томская область';
EXECUTE CreateCity 959, 'Северск', 'Томская область';
EXECUTE CreateCity 960, 'Стрежевой', 'Томская область';
EXECUTE CreateCity 961, 'Томск', 'Томская область';
EXECUTE CreateCity 962, 'Алексин', 'Тульская область';
EXECUTE CreateCity 963, 'Белёв', 'Тульская область';
EXECUTE CreateCity 964, 'Богородицк', 'Тульская область';
EXECUTE CreateCity 965, 'Болохово', 'Тульская область';
EXECUTE CreateCity 966, 'Венёв', 'Тульская область';
EXECUTE CreateCity 967, 'Донской', 'Тульская область';
EXECUTE CreateCity 968, 'Ефремов', 'Тульская область';
EXECUTE CreateCity 969, 'Кимовск', 'Тульская область';
EXECUTE CreateCity 970, 'Киреевск', 'Тульская область';
EXECUTE CreateCity 971, 'Липки', 'Тульская область';
EXECUTE CreateCity 972, 'Новомосковск', 'Тульская область';
EXECUTE CreateCity 973, 'Плавск', 'Тульская область';
EXECUTE CreateCity 974, 'Суворов', 'Тульская область';
EXECUTE CreateCity 975, 'Тула', 'Тульская область';
EXECUTE CreateCity 976, 'Узловая', 'Тульская область';
EXECUTE CreateCity 977, 'Чекалин', 'Тульская область';
EXECUTE CreateCity 978, 'Щёкино', 'Тульская область';
EXECUTE CreateCity 979, 'Ясногорск', 'Тульская область';
EXECUTE CreateCity 980, 'Советск', 'Тульская область';
EXECUTE CreateCity 981, 'Ак-Довурак', 'Тыва';
EXECUTE CreateCity 982, 'Кызыл', 'Тыва';
EXECUTE CreateCity 983, 'Туран', 'Тыва';
EXECUTE CreateCity 984, 'Чадан', 'Тыва';
EXECUTE CreateCity 985, 'Шагонар', 'Тыва';
EXECUTE CreateCity 986, 'Заводоуковск', 'Тюменская область';
EXECUTE CreateCity 987, 'Ишим', 'Тюменская область';
EXECUTE CreateCity 988, 'Тобольск', 'Тюменская область';
EXECUTE CreateCity 989, 'Тюмень', 'Тюменская область';
EXECUTE CreateCity 990, 'Ялуторовск', 'Тюменская область';
EXECUTE CreateCity 991, 'Воткинск', 'Удмуртия';
EXECUTE CreateCity 992, 'Глазов', 'Удмуртия';
EXECUTE CreateCity 993, 'Ижевск', 'Удмуртия';
EXECUTE CreateCity 994, 'Камбарка', 'Удмуртия';
EXECUTE CreateCity 995, 'Можга', 'Удмуртия';
EXECUTE CreateCity 996, 'Сарапул', 'Удмуртия';
EXECUTE CreateCity 997, 'Барыш', 'Ульяновская область';
EXECUTE CreateCity 998, 'Димитровград', 'Ульяновская область';
EXECUTE CreateCity 999, 'Инза', 'Ульяновская область';
EXECUTE CreateCity 1000, 'Новоульяновск', 'Ульяновская область';
EXECUTE CreateCity 1001, 'Сенгилей', 'Ульяновская область';
EXECUTE CreateCity 1002, 'Ульяновск', 'Ульяновская область';
EXECUTE CreateCity 1003, 'Амурск', 'Хабаровский край';
EXECUTE CreateCity 1004, 'Бикин', 'Хабаровский край';
EXECUTE CreateCity 1005, 'Вяземский', 'Хабаровский край';
EXECUTE CreateCity 1006, 'Комсомольск-на-Амуре', 'Хабаровский край';
EXECUTE CreateCity 1007, 'Николаевск-на-Амуре', 'Хабаровский край';
EXECUTE CreateCity 1008, 'Советская Гавань', 'Хабаровский край';
EXECUTE CreateCity 1009, 'Хабаровск', 'Хабаровский край';
EXECUTE CreateCity 1010, 'Абаза', 'Хакасия';
EXECUTE CreateCity 1011, 'Абакан', 'Хакасия';
EXECUTE CreateCity 1012, 'Саяногорск', 'Хакасия';
EXECUTE CreateCity 1013, 'Сорск', 'Хакасия';
EXECUTE CreateCity 1014, 'Черногорск', 'Хакасия';
EXECUTE CreateCity 1015, 'Белоярский', 'Ханты-Мансийский авт. окр.';
EXECUTE CreateCity 1016, 'Когалым', 'Ханты-Мансийский авт. окр.';
EXECUTE CreateCity 1017, 'Лангепас', 'Ханты-Мансийский авт. окр.';
EXECUTE CreateCity 1018, 'Лянтор', 'Ханты-Мансийский авт. окр.';
EXECUTE CreateCity 1019, 'Мегион', 'Ханты-Мансийский авт. окр.';
EXECUTE CreateCity 1020, 'Нефтеюганск', 'Ханты-Мансийский авт. окр.';
EXECUTE CreateCity 1021, 'Нижневартовск', 'Ханты-Мансийский авт. окр.';
EXECUTE CreateCity 1022, 'Нягань', 'Ханты-Мансийский авт. окр.';
EXECUTE CreateCity 1023, 'Покачи', 'Ханты-Мансийский авт. окр.';
EXECUTE CreateCity 1024, 'Пыть-Ях', 'Ханты-Мансийский авт. окр.';
EXECUTE CreateCity 1025, 'Радужный', 'Ханты-Мансийский авт. окр.';
EXECUTE CreateCity 1026, 'Советский', 'Ханты-Мансийский авт. окр.';
EXECUTE CreateCity 1027, 'Сургут', 'Ханты-Мансийский авт. окр.';
EXECUTE CreateCity 1028, 'Урай', 'Ханты-Мансийский авт. окр.';
EXECUTE CreateCity 1029, 'Ханты-Мансийск', 'Ханты-Мансийский авт. окр.';
EXECUTE CreateCity 1030, 'Югорск', 'Ханты-Мансийский авт. окр.';
EXECUTE CreateCity 1031, 'Аша', 'Челябинская область';
EXECUTE CreateCity 1032, 'Бакал', 'Челябинская область';
EXECUTE CreateCity 1033, 'Верхнеуральск', 'Челябинская область';
EXECUTE CreateCity 1034, 'Верхний Уфалей', 'Челябинская область';
EXECUTE CreateCity 1035, 'Еманжелинск', 'Челябинская область';
EXECUTE CreateCity 1036, 'Златоуст', 'Челябинская область';
EXECUTE CreateCity 1037, 'Карабаш', 'Челябинская область';
EXECUTE CreateCity 1038, 'Карталы', 'Челябинская область';
EXECUTE CreateCity 1039, 'Касли', 'Челябинская область';
EXECUTE CreateCity 1040, 'Катав-Ивановск', 'Челябинская область';
EXECUTE CreateCity 1041, 'Копейск', 'Челябинская область';
EXECUTE CreateCity 1042, 'Коркино', 'Челябинская область';
EXECUTE CreateCity 1043, 'Куса', 'Челябинская область';
EXECUTE CreateCity 1044, 'Кыштым', 'Челябинская область';
EXECUTE CreateCity 1045, 'Магнитогорск', 'Челябинская область';
EXECUTE CreateCity 1046, 'Миасс', 'Челябинская область';
EXECUTE CreateCity 1047, 'Миньяр', 'Челябинская область';
EXECUTE CreateCity 1048, 'Нязепетровск', 'Челябинская область';
EXECUTE CreateCity 1049, 'Озёрск', 'Челябинская область';
EXECUTE CreateCity 1050, 'Пласт', 'Челябинская область';
EXECUTE CreateCity 1051, 'Сатка', 'Челябинская область';
EXECUTE CreateCity 1052, 'Сим', 'Челябинская область';
EXECUTE CreateCity 1053, 'Снежинск', 'Челябинская область';
EXECUTE CreateCity 1054, 'Трёхгорный', 'Челябинская область';
EXECUTE CreateCity 1055, 'Троицк', 'Челябинская область';
EXECUTE CreateCity 1056, 'Усть-Катав', 'Челябинская область';
EXECUTE CreateCity 1057, 'Чебаркуль', 'Челябинская область';
EXECUTE CreateCity 1058, 'Челябинск', 'Челябинская область';
EXECUTE CreateCity 1059, 'Южноуральск', 'Челябинская область';
EXECUTE CreateCity 1060, 'Юрюзань', 'Челябинская область';
EXECUTE CreateCity 1061, 'Аргун', 'Чечня';
EXECUTE CreateCity 1062, 'Грозный', 'Чечня';
EXECUTE CreateCity 1063, 'Гудермес', 'Чечня';
EXECUTE CreateCity 1064, 'Урус-Мартан', 'Чечня';
EXECUTE CreateCity 1065, 'Шали', 'Чечня';
EXECUTE CreateCity 1066, 'Алатырь', 'Чувашия';
EXECUTE CreateCity 1067, 'Канаш', 'Чувашия';
EXECUTE CreateCity 1068, 'Козловка', 'Чувашия';
EXECUTE CreateCity 1069, 'Мариинский Посад', 'Чувашия';
EXECUTE CreateCity 1070, 'Новочебоксарск', 'Чувашия';
EXECUTE CreateCity 1071, 'Цивильск', 'Чувашия';
EXECUTE CreateCity 1072, 'Чебоксары', 'Чувашия';
EXECUTE CreateCity 1073, 'Шумерля', 'Чувашия';
EXECUTE CreateCity 1074, 'Ядрин', 'Чувашия';
EXECUTE CreateCity 1075, 'Анадырь', 'Чукотский авт. окр.';
EXECUTE CreateCity 1076, 'Билибино', 'Чукотский авт. окр.';
EXECUTE CreateCity 1077, 'Певек', 'Чукотский авт. окр.';
EXECUTE CreateCity 1078, 'Алдан', 'Якутия';
EXECUTE CreateCity 1079, 'Верхоянск', 'Якутия';
EXECUTE CreateCity 1080, 'Вилюйск', 'Якутия';
EXECUTE CreateCity 1081, 'Ленск', 'Якутия';
EXECUTE CreateCity 1082, 'Мирный', 'Якутия';
EXECUTE CreateCity 1083, 'Нерюнгри', 'Якутия';
EXECUTE CreateCity 1084, 'Нюрба', 'Якутия';
EXECUTE CreateCity 1085, 'Олёкминск', 'Якутия';
EXECUTE CreateCity 1086, 'Покровск', 'Якутия';
EXECUTE CreateCity 1087, 'Среднеколымск', 'Якутия';
EXECUTE CreateCity 1088, 'Томмот', 'Якутия';
EXECUTE CreateCity 1089, 'Удачный', 'Якутия';
EXECUTE CreateCity 1090, 'Якутск', 'Якутия';
EXECUTE CreateCity 1091, 'Губкинский', 'Ямало-Ненецкий авт. окр.';
EXECUTE CreateCity 1092, 'Лабытнанги', 'Ямало-Ненецкий авт. окр.';
EXECUTE CreateCity 1093, 'Муравленко', 'Ямало-Ненецкий авт. окр.';
EXECUTE CreateCity 1094, 'Надым', 'Ямало-Ненецкий авт. окр.';
EXECUTE CreateCity 1095, 'Новый Уренгой', 'Ямало-Ненецкий авт. окр.';
EXECUTE CreateCity 1096, 'Ноябрьск', 'Ямало-Ненецкий авт. окр.';
EXECUTE CreateCity 1097, 'Салехард', 'Ямало-Ненецкий авт. окр.';
EXECUTE CreateCity 1098, 'Тарко-Сале', 'Ямало-Ненецкий авт. окр.';
EXECUTE CreateCity 1099, 'Гаврилов-Ям', 'Ярославская область';
EXECUTE CreateCity 1100, 'Данилов', 'Ярославская область';
EXECUTE CreateCity 1101, 'Любим', 'Ярославская область';
EXECUTE CreateCity 1102, 'Мышкин', 'Ярославская область';
EXECUTE CreateCity 1103, 'Переславль-Залесский', 'Ярославская область';
EXECUTE CreateCity 1104, 'Пошехонье', 'Ярославская область';
EXECUTE CreateCity 1105, 'Ростов', 'Ярославская область';
EXECUTE CreateCity 1106, 'Рыбинск', 'Ярославская область';
EXECUTE CreateCity 1107, 'Тутаев', 'Ярославская область';
EXECUTE CreateCity 1108, 'Углич', 'Ярославская область';
EXECUTE CreateCity 1109, 'Ярославль', 'Ярославская область';

END
GO
/****** Object:  Table [dbo].[Adress]    Script Date: 09/24/2018 22:52:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Adress](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_City] [int] NOT NULL,
	[Street] [nvarchar](max) NULL,
	[House] [nvarchar](50) NULL,
	[Flat] [nvarchar](30) NULL,
	[Region] [nvarchar](max) NULL,
	[Geolocation] [nvarchar](max) NULL,
	[MaxFloor] [nchar](10) NULL,
	[Id_HouseType] [int] NULL,
	[Id_District] [int] NULL,
	[Id_MicroDistrict] [int] NULL,
	[Id_Settlement] [int] NULL,
 CONSTRAINT [PK_Adress] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [District] ON [dbo].[Adress] 
(
	[Id_District] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [MicroDistrict] ON [dbo].[Adress] 
(
	[Id_MicroDistrict] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Page]    Script Date: 09/24/2018 22:52:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Page](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_Source] [int] NOT NULL,
	[URL] [nvarchar](max) NOT NULL,
	[Date_Create] [datetime] NOT NULL,
	[Date_Publish] [datetime] NULL,
	[Date_LastRefresh] [datetime] NOT NULL,
	[IsDeleted] [bit] NULL,
	[Id_Adress] [int] NULL,
	[Cost] [decimal](18, 0) NULL,
	[Floor] [int] NULL,
	[Description] [nvarchar](max) NULL,
	[Id_Cost_Type] [int] NULL,
	[Id_Obj_Type] [int] NULL,
	[Area_Size] [float] NULL,
	[Cout_Rooms] [nvarchar](100) NULL,
	[Prepay] [decimal](18, 0) NULL,
	[IsAgency] [bit] NOT NULL,
	[IsDailyRent] [bit] NOT NULL,
	[PhoneNumber] [nvarchar](25) NOT NULL,
	[SearchIndex] [nvarchar](100) NULL,
	[RentType] [int] NOT NULL,
 CONSTRAINT [PK_Page] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Cost] ON [dbo].[Page] 
(
	[Cost] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Date] ON [dbo].[Page] 
(
	[Date_LastRefresh] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IsDailyRent] ON [dbo].[Page] 
(
	[IsDailyRent] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Old] ON [dbo].[Page] 
(
	[IsDeleted] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [RentType] ON [dbo].[Page] 
(
	[RentType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Rooms] ON [dbo].[Page] 
(
	[Cout_Rooms] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [SearchIndex] ON [dbo].[Page] 
(
	[SearchIndex] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Size] ON [dbo].[Page] 
(
	[Area_Size] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  View [dbo].[GetUndeletedPages]    Script Date: 09/24/2018 22:52:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[GetUndeletedPages]
AS
SELECT dbo.Page.Id, dbo.Page.URL, dbo.Source.Name AS SourceName, dbo.Page.Date_Create, dbo.Page.Date_Publish, DATEDIFF(minute, dbo.Page.Date_LastRefresh, 
               GETDATE()) AS minuteDiffLastRefresh
FROM  dbo.Page INNER JOIN
               dbo.Source ON dbo.Page.Id_Source = dbo.Source.Id
WHERE (dbo.Page.IsDeleted = 0)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Page"
            Begin Extent = 
               Top = 36
               Left = 485
               Bottom = 296
               Right = 680
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "Source"
            Begin Extent = 
               Top = 59
               Left = 176
               Bottom = 227
               Right = 366
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 2400
         Width = 3288
         Width = 3444
         Width = 1200
         Width = 1200
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1176
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1356
         SortOrder = 1416
         GroupBy = 1350
         Filter = 1356
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'GetUndeletedPages'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'GetUndeletedPages'
GO
/****** Object:  View [dbo].[GetPageView]    Script Date: 09/24/2018 22:52:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[GetPageView]
AS
SELECT TOP (100) PERCENT dbo.Source.Name AS Откуда, dbo.Page.Date_LastRefresh AS [Дата последнего обноления], 
               dbo.Page.Date_Publish AS [Дата публикации], dbo.Page.Cost AS Стоимость, dbo.Page.Description AS Описание, dbo.Page.Area_Size AS [Кв.м.], 
               dbo.Page.Cout_Rooms AS Комнат, dbo.Page.Prepay AS Депозит, dbo.Cost_Type.Type AS Оплата, CAST(dbo.Page.Floor AS varchar(5)) 
               + '/' + CAST(dbo.Adress.MaxFloor AS varchar(5)) AS Этаж, dbo.City.Name AS Город, dbo.Adress.Street AS Улица, dbo.House_Type.Type AS [Тип дома], 
               dbo.Page.IsAgency AS [Агенство?], dbo.Page.IsDailyRent AS [Посуточно?], dbo.Page.PhoneNumber AS Телефон
FROM  dbo.Adress INNER JOIN
               dbo.City ON dbo.Adress.Id_City = dbo.City.Id INNER JOIN
               dbo.Page ON dbo.Adress.Id = dbo.Page.Id_Adress INNER JOIN
               dbo.Obj_Type ON dbo.Page.Id_Obj_Type = dbo.Obj_Type.Id INNER JOIN
               dbo.Cost_Type ON dbo.Page.Id_Cost_Type = dbo.Cost_Type.Id INNER JOIN
               dbo.House_Type ON dbo.Adress.Id_HouseType = dbo.House_Type.Id INNER JOIN
               dbo.Source ON dbo.Page.Id_Source = dbo.Source.Id
WHERE (dbo.Page.IsDeleted = 0)
ORDER BY [Дата последнего обноления] DESC
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[37] 4[39] 2[12] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -120
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Adress"
            Begin Extent = 
               Top = 9
               Left = 30
               Bottom = 240
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "City"
            Begin Extent = 
               Top = 7
               Left = 286
               Bottom = 112
               Right = 476
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Page"
            Begin Extent = 
               Top = 7
               Left = 1238
               Bottom = 265
               Right = 1433
            End
            DisplayFlags = 280
            TopColumn = 10
         End
         Begin Table = "Obj_Type"
            Begin Extent = 
               Top = 7
               Left = 1000
               Bottom = 132
               Right = 1190
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Cost_Type"
            Begin Extent = 
               Top = 7
               Left = 524
               Bottom = 112
               Right = 714
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "House_Type"
            Begin Extent = 
               Top = 112
               Left = 286
               Bottom = 217
               Right = 476
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Source"
            Begin Extent = 
               Top = 127
               Left = 524
               Bottom = 232
               Right = 714
            End
            DisplayFlags = 28' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'GetPageView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'0
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 18
         Width = 284
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 2784
         Width = 1200
         Width = 1200
         Width = 2556
         Width = 1200
         Width = 1992
         Width = 1200
         Width = 1200
         Width = 1200
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 4536
         Alias = 2808
         Table = 1176
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1356
         SortOrder = 1416
         GroupBy = 1356
         Filter = 1356
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'GetPageView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'GetPageView'
GO
/****** Object:  View [dbo].[Dublicates]    Script Date: 09/24/2018 22:52:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Dublicates]
AS
SELECT TOP (100) PERCENT COUNT(URL) AS Expr1, DATEDIFF(second, MIN(Date_Create), MAX(Date_Create)) AS 'Duration', URL
FROM  dbo.Page
GROUP BY URL
HAVING (COUNT(URL) > 1)
ORDER BY Expr1 DESC, 'Duration' DESC
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Page"
            Begin Extent = 
               Top = 7
               Left = 48
               Bottom = 148
               Right = 259
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1176
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1356
         SortOrder = 1416
         GroupBy = 1350
         Filter = 1356
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Dublicates'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Dublicates'
GO
/****** Object:  Table [dbo].[Image]    Script Date: 09/24/2018 22:52:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Image](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_Page] [int] NOT NULL,
	[Path] [nvarchar](max) NOT NULL,
	[Exctension] [nvarchar](10) NULL,
 CONSTRAINT [PK_Image] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[AddedInHour]    Script Date: 09/24/2018 22:52:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[AddedInHour]
AS
SELECT TOP (100) PERCENT DATEDIFF(hour, Date_LastRefresh, GETDATE()) AS [За последние N часов], COUNT(Date_LastRefresh) 
               AS [Добавлено N объявлений]
FROM  dbo.Page
GROUP BY DATEDIFF(hour, Date_LastRefresh, GETDATE())
ORDER BY [За последние N часов]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Page"
            Begin Extent = 
               Top = 7
               Left = 48
               Bottom = 148
               Right = 259
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 2784
         Width = 3264
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1176
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1356
         SortOrder = 1416
         GroupBy = 1350
         Filter = 1356
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'AddedInHour'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'AddedInHour'
GO
/****** Object:  StoredProcedure [dbo].[ClearDB]    Script Date: 09/24/2018 22:52:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ClearDB] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
TRUNCATE TABLE [Image];  
DELETE FROM [Page];  
DELETE FROM [Adress];  
DELETE FROM [Cost_Type];  
DELETE FROM [Obj_Type];  
DELETE FROM [House_Type];  
DELETE FROM [MicroDistrict];  
DELETE FROM [District];  
DELETE FROM [City];  
DELETE FROM [Region];  
DELETE FROM [Source];  

INSERT INTO [Source] (Name) VALUES ('Mlsn');
INSERT INTO [Source] (Name) VALUES ('Avito');
INSERT INTO [Source] (Name) VALUES ('Cian');
INSERT INTO [Source] (Name) VALUES ('Yandex');
INSERT INTO [Source] (Name) VALUES ('NGS');
INSERT INTO [Source] (Name) VALUES ('IsRukVRuki');

EXECUTE CreateRegionsAndCity;
END
GO
/****** Object:  Default [DF_Page_IsDeleted]    Script Date: 09/24/2018 22:52:13 ******/
ALTER TABLE [dbo].[Page] ADD  CONSTRAINT [DF_Page_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF_Page_PhoneNumber]    Script Date: 09/24/2018 22:52:13 ******/
ALTER TABLE [dbo].[Page] ADD  CONSTRAINT [DF_Page_PhoneNumber]  DEFAULT (N'ToDo') FOR [PhoneNumber]
GO
/****** Object:  Default [DF_Page_RentType]    Script Date: 09/24/2018 22:52:13 ******/
ALTER TABLE [dbo].[Page] ADD  CONSTRAINT [DF_Page_RentType]  DEFAULT ((0)) FOR [RentType]
GO
/****** Object:  ForeignKey [FK_City_Region]    Script Date: 09/24/2018 22:52:12 ******/
ALTER TABLE [dbo].[City]  WITH CHECK ADD  CONSTRAINT [FK_City_Region] FOREIGN KEY([Id_Region])
REFERENCES [dbo].[Region] ([Id])
GO
ALTER TABLE [dbo].[City] CHECK CONSTRAINT [FK_City_Region]
GO
/****** Object:  ForeignKey [FK_Sorce_city_City]    Script Date: 09/24/2018 22:52:12 ******/
ALTER TABLE [dbo].[Sorce_city]  WITH CHECK ADD  CONSTRAINT [FK_Sorce_city_City] FOREIGN KEY([Id_City])
REFERENCES [dbo].[City] ([Id])
GO
ALTER TABLE [dbo].[Sorce_city] CHECK CONSTRAINT [FK_Sorce_city_City]
GO
/****** Object:  ForeignKey [FK_Sorce_city_Source]    Script Date: 09/24/2018 22:52:12 ******/
ALTER TABLE [dbo].[Sorce_city]  WITH CHECK ADD  CONSTRAINT [FK_Sorce_city_Source] FOREIGN KEY([Id_Source])
REFERENCES [dbo].[Source] ([Id])
GO
ALTER TABLE [dbo].[Sorce_city] CHECK CONSTRAINT [FK_Sorce_city_Source]
GO
/****** Object:  ForeignKey [FK_Settlement_City]    Script Date: 09/24/2018 22:52:12 ******/
ALTER TABLE [dbo].[Settlement]  WITH CHECK ADD  CONSTRAINT [FK_Settlement_City] FOREIGN KEY([Id_City])
REFERENCES [dbo].[City] ([Id])
GO
ALTER TABLE [dbo].[Settlement] CHECK CONSTRAINT [FK_Settlement_City]
GO
/****** Object:  ForeignKey [FK_District_City]    Script Date: 09/24/2018 22:52:13 ******/
ALTER TABLE [dbo].[District]  WITH CHECK ADD  CONSTRAINT [FK_District_City] FOREIGN KEY([Id_City])
REFERENCES [dbo].[City] ([Id])
GO
ALTER TABLE [dbo].[District] CHECK CONSTRAINT [FK_District_City]
GO
/****** Object:  ForeignKey [FK_MicroDistrict_City]    Script Date: 09/24/2018 22:52:13 ******/
ALTER TABLE [dbo].[MicroDistrict]  WITH CHECK ADD  CONSTRAINT [FK_MicroDistrict_City] FOREIGN KEY([Id_City])
REFERENCES [dbo].[City] ([Id])
GO
ALTER TABLE [dbo].[MicroDistrict] CHECK CONSTRAINT [FK_MicroDistrict_City]
GO
/****** Object:  ForeignKey [FK_MicroDistrict_District]    Script Date: 09/24/2018 22:52:13 ******/
ALTER TABLE [dbo].[MicroDistrict]  WITH CHECK ADD  CONSTRAINT [FK_MicroDistrict_District] FOREIGN KEY([Id_District])
REFERENCES [dbo].[District] ([Id])
GO
ALTER TABLE [dbo].[MicroDistrict] CHECK CONSTRAINT [FK_MicroDistrict_District]
GO
/****** Object:  ForeignKey [FK_Adress_City]    Script Date: 09/24/2018 22:52:13 ******/
ALTER TABLE [dbo].[Adress]  WITH CHECK ADD  CONSTRAINT [FK_Adress_City] FOREIGN KEY([Id_City])
REFERENCES [dbo].[City] ([Id])
GO
ALTER TABLE [dbo].[Adress] CHECK CONSTRAINT [FK_Adress_City]
GO
/****** Object:  ForeignKey [FK_Adress_District]    Script Date: 09/24/2018 22:52:13 ******/
ALTER TABLE [dbo].[Adress]  WITH CHECK ADD  CONSTRAINT [FK_Adress_District] FOREIGN KEY([Id_District])
REFERENCES [dbo].[District] ([Id])
GO
ALTER TABLE [dbo].[Adress] CHECK CONSTRAINT [FK_Adress_District]
GO
/****** Object:  ForeignKey [FK_Adress_House_Type]    Script Date: 09/24/2018 22:52:13 ******/
ALTER TABLE [dbo].[Adress]  WITH CHECK ADD  CONSTRAINT [FK_Adress_House_Type] FOREIGN KEY([Id_HouseType])
REFERENCES [dbo].[House_Type] ([Id])
GO
ALTER TABLE [dbo].[Adress] CHECK CONSTRAINT [FK_Adress_House_Type]
GO
/****** Object:  ForeignKey [FK_Adress_MicroDistrict]    Script Date: 09/24/2018 22:52:13 ******/
ALTER TABLE [dbo].[Adress]  WITH CHECK ADD  CONSTRAINT [FK_Adress_MicroDistrict] FOREIGN KEY([Id_MicroDistrict])
REFERENCES [dbo].[MicroDistrict] ([Id])
GO
ALTER TABLE [dbo].[Adress] CHECK CONSTRAINT [FK_Adress_MicroDistrict]
GO
/****** Object:  ForeignKey [FK_Adress_Settlement]    Script Date: 09/24/2018 22:52:13 ******/
ALTER TABLE [dbo].[Adress]  WITH CHECK ADD  CONSTRAINT [FK_Adress_Settlement] FOREIGN KEY([Id_Settlement])
REFERENCES [dbo].[Settlement] ([Id])
GO
ALTER TABLE [dbo].[Adress] CHECK CONSTRAINT [FK_Adress_Settlement]
GO
/****** Object:  ForeignKey [FK_Page_Adress]    Script Date: 09/24/2018 22:52:13 ******/
ALTER TABLE [dbo].[Page]  WITH CHECK ADD  CONSTRAINT [FK_Page_Adress] FOREIGN KEY([Id_Adress])
REFERENCES [dbo].[Adress] ([Id])
GO
ALTER TABLE [dbo].[Page] CHECK CONSTRAINT [FK_Page_Adress]
GO
/****** Object:  ForeignKey [FK_Page_Cost_Type]    Script Date: 09/24/2018 22:52:13 ******/
ALTER TABLE [dbo].[Page]  WITH CHECK ADD  CONSTRAINT [FK_Page_Cost_Type] FOREIGN KEY([Id_Cost_Type])
REFERENCES [dbo].[Cost_Type] ([Id])
GO
ALTER TABLE [dbo].[Page] CHECK CONSTRAINT [FK_Page_Cost_Type]
GO
/****** Object:  ForeignKey [FK_Page_Obj_Type]    Script Date: 09/24/2018 22:52:13 ******/
ALTER TABLE [dbo].[Page]  WITH CHECK ADD  CONSTRAINT [FK_Page_Obj_Type] FOREIGN KEY([Id_Obj_Type])
REFERENCES [dbo].[Obj_Type] ([Id])
GO
ALTER TABLE [dbo].[Page] CHECK CONSTRAINT [FK_Page_Obj_Type]
GO
/****** Object:  ForeignKey [FK_Page_Source]    Script Date: 09/24/2018 22:52:13 ******/
ALTER TABLE [dbo].[Page]  WITH CHECK ADD  CONSTRAINT [FK_Page_Source] FOREIGN KEY([Id_Source])
REFERENCES [dbo].[Source] ([Id])
GO
ALTER TABLE [dbo].[Page] CHECK CONSTRAINT [FK_Page_Source]
GO
/****** Object:  ForeignKey [FK_Image_Page]    Script Date: 09/24/2018 22:52:14 ******/
ALTER TABLE [dbo].[Image]  WITH CHECK ADD  CONSTRAINT [FK_Image_Page] FOREIGN KEY([Id_Page])
REFERENCES [dbo].[Page] ([Id])
GO
ALTER TABLE [dbo].[Image] CHECK CONSTRAINT [FK_Image_Page]
GO

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Core.Enum;
using CoreParser.Enum;
using Logger;

namespace CoreParser
{
    public static class DictionaryStorage
    {
        public static ConcurrentDictionary<ParsingPageType, string> BoardParsingSettings = new ConcurrentDictionary<ParsingPageType, string>();
        
        /// <summary>
        /// Список полей для парсинга
        /// </summary>
        public static ConcurrentDictionary<ParsingPageType, ConcurrentDictionary<PageField, string>> PageParsingSettings = new ConcurrentDictionary<ParsingPageType, ConcurrentDictionary<PageField, string>>();

        public static void Init(){}

        static DictionaryStorage()
        {
            try
            {
                BoardParsingSettings.TryAdd(ParsingPageType.Avito, "//a[contains(@class, 'item-description-title-link') or contains(@class,'description-title-link')]");
                BoardParsingSettings.TryAdd(ParsingPageType.Cian, "//a[contains(@class,'-header--')]");
                BoardParsingSettings.TryAdd(ParsingPageType.Mlsn, "//a[contains(@class,'underline') or contains(@class, 'location slim') or contains(@class, 'location')]");
                BoardParsingSettings.TryAdd(ParsingPageType.Yandex,"//a[@class='Link Link_js_inited Link_size_m Link_theme_islands SerpItemLink OffersSerpItem__link']");
                BoardParsingSettings.TryAdd(ParsingPageType.N1, "//a[@class='link']");//"//a[contains(@class, 'living-list-card-title__link') or contains(@class, 'commercial-list-card-title__link') or contains(@data-test,'offers-list-item-header')]");

                PageParsingSettings.TryAdd(ParsingPageType.Avito, new ConcurrentDictionary<PageField, string>());
                PageParsingSettings.TryAdd(ParsingPageType.Cian, new ConcurrentDictionary<PageField, string>());
                PageParsingSettings.TryAdd(ParsingPageType.Mlsn, new ConcurrentDictionary<PageField, string>());
                PageParsingSettings.TryAdd(ParsingPageType.Yandex, new ConcurrentDictionary<PageField, string>());
                PageParsingSettings.TryAdd(ParsingPageType.N1, new ConcurrentDictionary<PageField, string>());


                #region Init PageParsingSettings

                PageParsingSettings[ParsingPageType.Avito].TryAdd(PageField.Title, "//span[@class='title-info-title-text']");
                PageParsingSettings[ParsingPageType.Avito].TryAdd(PageField.Size, "//li[@class='item-params-list-item']");
                PageParsingSettings[ParsingPageType.Avito].TryAdd(PageField.XPathImage,"//div[@class='gallery-img-frame js-gallery-img-frame']");
                PageParsingSettings[ParsingPageType.Avito].TryAdd(PageField.HouseMaterial, "//li[@class='item-params-list-item']");
                PageParsingSettings[ParsingPageType.Avito].TryAdd(PageField.Floor, "//li[@class='item-params-list-item']");
                PageParsingSettings[ParsingPageType.Avito].TryAdd(PageField.MaxFloor, "//li[@class='item-params-list-item']");
                PageParsingSettings[ParsingPageType.Avito].TryAdd(PageField.Address, "//div[@class='item-map-location']");
                PageParsingSettings[ParsingPageType.Avito].TryAdd(PageField.Prepay, "//div[@class='item-price-sub-price']");
                PageParsingSettings[ParsingPageType.Avito].TryAdd(PageField.Cost, "//span[@class='js-item-price']");
                PageParsingSettings[ParsingPageType.Avito].TryAdd(PageField.Description, "//div[@itemprop='description']");
                PageParsingSettings[ParsingPageType.Avito].TryAdd(PageField.CostType, ""); //"//div[@class='item-price-sub-price']");
                PageParsingSettings[ParsingPageType.Avito].TryAdd(PageField.ObjType,"//a[@class='js-breadcrumbs-link js-breadcrumbs-link-interaction']");
                PageParsingSettings[ParsingPageType.Avito].TryAdd(PageField.DatePublish, "//div[@class='title-info-metadata-item' or @class='title-info-metadata-item-redesign']");
                PageParsingSettings[ParsingPageType.Avito].TryAdd(PageField.PhoneNumber, "//span[@class='item-phone-button-sub-text']");
                PageParsingSettings[ParsingPageType.Avito].TryAdd(PageField.RentType,"//a[@class='js-breadcrumbs-link js-breadcrumbs-link-interaction']");
                PageParsingSettings[ParsingPageType.Avito].TryAdd(PageField.GeoMetka,"//div[@class='b-search-map expanded item-map-wrapper js-item-map-wrapper']");
                PageParsingSettings[ParsingPageType.Avito].TryAdd(PageField.SellType, "//a[@class='js-breadcrumbs-link js-breadcrumbs-link-interaction']");
                PageParsingSettings[ParsingPageType.Avito].TryAdd(PageField.IsWithFurniture, "//");
                PageParsingSettings[ParsingPageType.Avito].TryAdd(PageField.IsAgency, "//div[contains(@class,'item-price-sub-price') or contains(@class, 'seller-info')]");

                PageParsingSettings[ParsingPageType.Cian].TryAdd(PageField.Title, "//title");
                PageParsingSettings[ParsingPageType.Cian].TryAdd(PageField.RentType, @"//span[contains(@itemprop,'itemListElement')]");
                PageParsingSettings[ParsingPageType.Cian].TryAdd(PageField.ObjType, @"//span[contains(@itemprop,'itemListElement')]");
                PageParsingSettings[ParsingPageType.Cian].TryAdd(PageField.Size, @"//div[contains(@class,'--info--')]");
                PageParsingSettings[ParsingPageType.Cian].TryAdd(PageField.XPathImage, @"//img[contains(@class, 'photo--')]");
                PageParsingSettings[ParsingPageType.Cian].TryAdd(PageField.HouseMaterial, @"//li[contains(@class,'--item--')] | //div[contains(@class,'--item--')]");
                PageParsingSettings[ParsingPageType.Cian].TryAdd(PageField.Floor, @"//li[contains(@class,'--item--')]");
                PageParsingSettings[ParsingPageType.Cian].TryAdd(PageField.MaxFloor, @"//li[contains(@class,'--item--')]");
                PageParsingSettings[ParsingPageType.Cian].TryAdd(PageField.Address, @"//address[contains(@class,'address--')]");
                PageParsingSettings[ParsingPageType.Cian].TryAdd(PageField.Prepay, @"//p[contains(@class, 'description--')]");
                PageParsingSettings[ParsingPageType.Cian].TryAdd(PageField.Cost, @"//span[contains(@itemprop, 'price')]");
                PageParsingSettings[ParsingPageType.Cian].TryAdd(PageField.CostType, @"//div[@class='more_price_rent--341D0']");
                PageParsingSettings[ParsingPageType.Cian].TryAdd(PageField.Description, @"//p[contains(@itemprop, 'description')]");
                PageParsingSettings[ParsingPageType.Cian].TryAdd(PageField.DatePublish, @"//div[contains(@class,'offer_card_page-top_left')]");
                PageParsingSettings[ParsingPageType.Cian].TryAdd(PageField.PhoneNumber, @"//div[@class='print_phones--2T2Pz']");
                PageParsingSettings[ParsingPageType.Cian].TryAdd(PageField.GeoMetka, "//");
                PageParsingSettings[ParsingPageType.Cian].TryAdd(PageField.SellType, @"//li[contains(@class,'--item--')]");
                PageParsingSettings[ParsingPageType.Cian].TryAdd(PageField.IsWithFurniture, @"//li[contains(@class,'--item--')]");
                PageParsingSettings[ParsingPageType.Cian].TryAdd(PageField.IsAgency, "//");

                PageParsingSettings[ParsingPageType.Mlsn].TryAdd(PageField.Title, "//title");
                PageParsingSettings[ParsingPageType.Mlsn].TryAdd(PageField.Size, "//h1[@class='Title__base fonts__mainFont']");
                PageParsingSettings[ParsingPageType.Mlsn].TryAdd(PageField.XPathImage, "//script");
                PageParsingSettings[ParsingPageType.Mlsn].TryAdd(PageField.HouseMaterial,"//td[@class='ParamsList__paramValue typography__body fonts__mainFont']");
                PageParsingSettings[ParsingPageType.Mlsn].TryAdd(PageField.Floor,"//td[@class='ParamsList__paramValue typography__body fonts__mainFont']");
                PageParsingSettings[ParsingPageType.Mlsn].TryAdd(PageField.MaxFloor,"//td[@class='ParamsList__paramValue typography__body fonts__mainFont']");
                PageParsingSettings[ParsingPageType.Mlsn].TryAdd(PageField.Address, "//h1[@class='Title__base fonts__mainFont']");
                PageParsingSettings[ParsingPageType.Mlsn].TryAdd(PageField.Prepay, "//div[@class='Flex__base']");
                PageParsingSettings[ParsingPageType.Mlsn].TryAdd(PageField.Cost, "//span[@class='Price__base']");
                PageParsingSettings[ParsingPageType.Mlsn].TryAdd(PageField.CostType, "//div[@class='Flex__base']");
                PageParsingSettings[ParsingPageType.Mlsn].TryAdd(PageField.Description, "//div[@itemprop='description']");
                PageParsingSettings[ParsingPageType.Mlsn].TryAdd(PageField.ObjType, "//div[@class='Breadcrumbs__base typography__bodySmall fonts__mainFont']");
                PageParsingSettings[ParsingPageType.Mlsn].TryAdd(PageField.DatePublish,"//div[@class='PropertyStats__propertyStatistics typography__bodySmall fonts__mainFont']");
                PageParsingSettings[ParsingPageType.Mlsn].TryAdd(PageField.PhoneNumber, "//title");
                PageParsingSettings[ParsingPageType.Mlsn].TryAdd(PageField.RentType,"//td[@class='ParamsList__paramValue typography__body fonts__mainFont']");
                PageParsingSettings[ParsingPageType.Mlsn].TryAdd(PageField.GeoMetka, "//title");
                PageParsingSettings[ParsingPageType.Mlsn].TryAdd(PageField.SellType, "//td[@class='ParamsList__paramValue typography__body fonts__mainFont']");
                PageParsingSettings[ParsingPageType.Mlsn].TryAdd(PageField.IsWithFurniture, "//td[@class='ParamsList__paramValue typography__body fonts__mainFont']");
                PageParsingSettings[ParsingPageType.Mlsn].TryAdd(PageField.IsAgency, "//div[contains(@class,'ContactsCard__contact')]");

                PageParsingSettings[ParsingPageType.Yandex].TryAdd(PageField.Title, "//title");
                PageParsingSettings[ParsingPageType.Yandex].TryAdd(PageField.Description, "//p[@class='OfferTextDescription__text']");
                PageParsingSettings[ParsingPageType.Yandex].TryAdd(PageField.Size, "//p[@class='ColumnsList__item']");
                PageParsingSettings[ParsingPageType.Yandex].TryAdd(PageField.XPathImage, "//a[contains(@class,'offer-card__photo')]");//"//div[contains(@class,'offer-card stat i-bem') or contains(@class,'offer-card__photos-wrapper')]");
                PageParsingSettings[ParsingPageType.Yandex].TryAdd(PageField.HouseMaterial, "//span[@class='BuildingInfoFeature']");
                PageParsingSettings[ParsingPageType.Yandex].TryAdd(PageField.Floor, "//p[@class='ColumnsList__item']");
                PageParsingSettings[ParsingPageType.Yandex].TryAdd(PageField.MaxFloor, "//p[@class='ColumnsList__item']");
                PageParsingSettings[ParsingPageType.Yandex].TryAdd(PageField.Prepay, "//div[@class='offer-card__terms']");
                PageParsingSettings[ParsingPageType.Yandex].TryAdd(PageField.Cost, "//h3[@class='OfferBaseInfo__price']");//"//div[contains(class,'Price Price_with-trend Price_interactive') or contains(class, 'Price OfferCard__title')]");
                PageParsingSettings[ParsingPageType.Yandex].TryAdd(PageField.CostType, "//div[@class='offer-card__terms']");
                PageParsingSettings[ParsingPageType.Yandex].TryAdd(PageField.ObjType, "//h1[@class='offer-card__header-text' or @class='OfferHeader__title']");
                PageParsingSettings[ParsingPageType.Yandex].TryAdd(PageField.DatePublish, "//div[contains(class,'OfferHeader__meta-info_capitalize')]");
                PageParsingSettings[ParsingPageType.Yandex].TryAdd(PageField.PhoneNumber, "//div[@class='']");
                PageParsingSettings[ParsingPageType.Yandex].TryAdd(PageField.RentType, "//p[@class='OfferBaseInfo__text-info']");
                PageParsingSettings[ParsingPageType.Yandex].TryAdd(PageField.GeoMetka, "//");
                PageParsingSettings[ParsingPageType.Yandex].TryAdd(PageField.Address, "//");
                PageParsingSettings[ParsingPageType.Yandex].TryAdd(PageField.SellType, "//div[@class='offer-card__building-type']");
                PageParsingSettings[ParsingPageType.Yandex].TryAdd(PageField.IsWithFurniture, "//div[@class='offer-card__feature offer-card__feature_name_room-furniture']");
                PageParsingSettings[ParsingPageType.Yandex].TryAdd(PageField.IsAgency, "//div[@class='offer-card__author-note']");

                PageParsingSettings[ParsingPageType.N1].TryAdd(PageField.Title, "//title");
                PageParsingSettings[ParsingPageType.N1].TryAdd(PageField.Description, "//div[@class='text']");
                PageParsingSettings[ParsingPageType.N1].TryAdd(PageField.Size, "//span[@data-test='offer-card-param-total-area']");
                PageParsingSettings[ParsingPageType.N1].TryAdd(PageField.XPathImage, "");
                PageParsingSettings[ParsingPageType.N1].TryAdd(PageField.HouseMaterial,"//span[@data-test='offer-card-param-house-material-type']");
                PageParsingSettings[ParsingPageType.N1].TryAdd(PageField.Floor, "//span[@data-test='offer-card-param-floor']");
                PageParsingSettings[ParsingPageType.N1].TryAdd(PageField.MaxFloor, "//span[@data-test='offer-card-param-floor']");
                PageParsingSettings[ParsingPageType.N1].TryAdd(PageField.Prepay, "");
                PageParsingSettings[ParsingPageType.N1].TryAdd(PageField.Cost, "//span[@data-test='offer-card-price']");//"contains(@class, 'card-living-content-price__val') or contains(@class, 'card-commercial-content-price__val')]");
                PageParsingSettings[ParsingPageType.N1].TryAdd(PageField.CostType, "");
                PageParsingSettings[ParsingPageType.N1].TryAdd(PageField.ObjType, "//h1[contains(@class, 'header__title')]");
                PageParsingSettings[ParsingPageType.N1].TryAdd(PageField.DatePublish, "//p[contains(@class, 'content__state')]");
                PageParsingSettings[ParsingPageType.N1].TryAdd(PageField.PhoneNumber, "//a[@class='offer-card-contacts-phones__phone']");
                PageParsingSettings[ParsingPageType.N1].TryAdd(PageField.RentType,"//span[@class='card-living-content-price__period']");
                PageParsingSettings[ParsingPageType.N1].TryAdd(PageField.GeoMetka, "//");
                PageParsingSettings[ParsingPageType.N1].TryAdd(PageField.Address, "//h1[contains(@class,'header__title')]");
                PageParsingSettings[ParsingPageType.N1].TryAdd(PageField.SellType, "//li[@class='breadcrumbs-list__item']");
                PageParsingSettings[ParsingPageType.N1].TryAdd(PageField.IsWithFurniture, "//li[@class='card-living-content-params-list__item']");
                PageParsingSettings[ParsingPageType.N1].TryAdd(PageField.IsAgency, "//div[@class='offer-card-contacts__person _type']");

                #endregion


                #region Проверка - все ли поля настроены для парсинга страниц?

                foreach (ParsingPageType value in System.Enum.GetValues(typeof(ParsingPageType)))
                {
                    if (value == ParsingPageType.Undefined)
                        continue;
                    CheckPageParsingSettings(value);
                }

                #endregion
            }
            catch (Exception ex)
            {
                Log.Fatal(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Проверка все ли поля настроены для парсинга страниц?
        /// </summary>
        /// <param name="parsingPageType"></param>
        private static void CheckPageParsingSettings(ParsingPageType parsingPageType)
        {
            var unsettedFields = "Не установленые поля: ";
            try
            {
                var countVar = System.Enum.GetValues(typeof(PageField)).Length;
                if (PageParsingSettings[parsingPageType].Count == countVar) return;

                var checkDictionary = new Dictionary<PageField, bool>(countVar);
                foreach (var value in System.Enum.GetValues(typeof(PageField)))
                {
                    checkDictionary.Add((PageField) value, false);
                }

                foreach (var pair in PageParsingSettings[parsingPageType])
                {
                    if (string.IsNullOrEmpty(pair.Value))
                        Log.Error(
                            "DictionaryStorage.PageParsingSettings[" + parsingPageType + "] не указан xPath для поля '" +
                            pair.Key + "'");
                    checkDictionary[pair.Key] = true;
                }

                foreach (var pair in checkDictionary)
                {
                    if (!checkDictionary[pair.Key])
                    {
                        unsettedFields += pair.Key + "; ";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }

            throw new Exception("У типа '" + parsingPageType + "' указаны не все поля для парсинга. " + unsettedFields);
        }


        private static readonly Dictionary<string, int> MonthNumbersDictionary = new Dictionary<string, int>()
        {
            {"Январь", 1},
            {"Января", 1},
            {"Январём", 1},
            {"Январем", 1},
            {"Январю", 1},
            {"Январе", 1},
            {"январь", 1},
            {"января", 1},
            {"январём", 1},
            {"январем", 1},
            {"январю", 1},
            {"январе", 1},

            {"Февраль", 2},
            {"Февраля", 2},
            {"Февралём", 2},
            {"Февралем", 2},
            {"Февралю", 2},
            {"Феврале", 2},
            {"февраль", 2},
            {"февраля", 2},
            {"февралём", 2},
            {"февралем", 2},
            {"февралю", 2},
            {"феврале", 2},

            {"Март", 3},
            {"Марта", 3},
            {"Марту", 3},
            {"Мартом", 3},
            {"Марте", 3},
            {"март", 3},
            {"марта", 3},
            {"марту", 3},
            {"мартом", 3},
            {"марте", 3},

            {"Апрель", 4},
            {"Апреля", 4},
            {"Апрелем", 4},
            {"Апрелю", 4},
            {"Апреле", 4},
            {"апрель", 4},
            {"апреля", 4},
            {"апрелем", 4},
            {"апрелю", 4},
            {"апреле", 4},

            {"Май", 5},
            {"Мая", 5},
            {"Маеи", 5},
            {"Маю", 5},
            {"Мае", 5},
            {"май", 5},
            {"мая", 5},
            {"маеи", 5},
            {"маю", 5},
            {"мае", 5},

            {"Июнь", 6},
            {"Июня", 6},
            {"Июнем", 6},
            {"Июню", 6},
            {"Июне", 6},
            {"июнь", 6},
            {"июня", 6},
            {"июнем", 6},
            {"июню", 6},
            {"июне", 6},

            {"Июль", 7},
            {"Июля", 7},
            {"Июлем", 7},
            {"Июлю", 7},
            {"Июле", 7},
            {"июль", 7},
            {"июля", 7},
            {"июлем", 7},
            {"июлю", 7},
            {"июле", 7},

            {"Август", 8},
            {"Августа", 8},
            {"Августу", 8},
            {"Августом", 8},
            {"Августе", 8},
            {"август", 8},
            {"августа", 8},
            {"августу", 8},
            {"августом", 8},
            {"августе", 8},

            {"Сентябрь", 9},
            {"Сентября", 9},
            {"Сентябрю", 9},
            {"Сентябрем", 9},
            {"Сентябре", 9},
            {"сентябрь", 9},
            {"сентября", 9},
            {"сентябрю", 9},
            {"сентябрем", 9},
            {"сентябре", 9},

            {"Октябрь", 10},
            {"Октября", 10},
            {"Октябрю", 10},
            {"Октябрем", 10},
            {"Октябре", 10},
            {"октябрь", 10},
            {"октября", 10},
            {"октябрю", 10},
            {"октябрем", 10},
            {"октябре", 10},

            {"Ноябрь", 11},
            {"Ноября", 11},
            {"Ноябрю", 11},
            {"Ноябрем", 11},
            {"Ноябре", 11},
            {"ноябрь", 11},
            {"ноября", 11},
            {"ноябрю", 11},
            {"ноябрем", 11},
            {"ноябре", 11},

            {"Декабрь", 12},
            {"Декабря", 12},
            {"Декабрю", 12},
            {"Декабрем", 12},
            {"Декабре", 12},
            {"декабрь", 12},
            {"декабря", 12},
            {"декабрю", 12},
            {"декабрем", 12},
            {"декабре", 12}
        };

        public static int GetMonthNumber(string monthString)
        {
            if (string.IsNullOrEmpty(monthString))
                return DateTime.Now.Month;
            monthString = monthString.Trim();
            if (string.IsNullOrEmpty(monthString))
                throw new ArgumentNullException("monthString");
            if (MonthNumbersDictionary.TryGetValue(monthString, out var number))
                return number;
            Log.Warn("Не удалось определить номер месяца: "+ monthString);
            return -1;
        }
    }
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Transactions;
using Core;
using Core.Enum;
using Core.Storage;
using CoreParser.DB.Models;
using CoreParser.Enum;
using CoreParser.Services.PropertyAnalizators;
using Logger;
using Adress = CoreParser.DB.Models.Adress;
using District = Core.Enteties.District;
using MicroDistrict = Core.Enteties.MicroDistrict;
using SellType = CoreParser.DB.Models.SellType;
using Source = CoreParser.DB.Models.Source;

namespace CoreParser.DB
{
    public static class DbObjectManager
    {
        private static ConcurrentDictionary<ParsingPageType, Source> _dbSources;
        private static ConcurrentDictionary<string, LocalEntities.City> _dbCities;
        private static ConcurrentDictionary<string, Cost_Type> _dbCostTypes;
        private static ConcurrentDictionary<EnumHouseMaterial, LocalEntities.HouseMaterial> _dbHouseMaterial;
        private static ConcurrentDictionary<string, Obj_Type> _dbObjTypes;
        private static ConcurrentDictionary<string, District> _dbDistricts;
        private static ConcurrentDictionary<string, MicroDistrict> _dbMicroDistricts;
        private static ConcurrentDictionary<string, SellType> _dbSellTypes;

        public static List<Source> Sources => _dbSources.Values.ToList();
        public static List<LocalEntities.City> Cities => _dbCities.Values.ToList();
        public static List<Cost_Type> CostTypes => _dbCostTypes.Values.ToList();
        public static List<LocalEntities.HouseMaterial> HouseMaterials => _dbHouseMaterial.Values.ToList();
        public static List<Obj_Type> ObjTypes => _dbObjTypes.Values.ToList();
        public static List<District> Districts => _dbDistricts.Values.ToList();
        public static List<MicroDistrict> MicroDistricts => _dbMicroDistricts.Values.ToList();
        public static List<SellType> SellTypes => _dbSellTypes.Values.ToList();

        internal static void Init()
        {
            var dbSources = new ConcurrentDictionary<ParsingPageType, DB.Models.Source>();
            var dbCities = new ConcurrentDictionary<string, LocalEntities.City>();
            var dbCostTypes = new ConcurrentDictionary<string, Cost_Type>();
            var dbHouseMaterial = new ConcurrentDictionary<EnumHouseMaterial, LocalEntities.HouseMaterial>();
            var dbObjTypes = new ConcurrentDictionary<string, Obj_Type>();
            var dbDistricts = new ConcurrentDictionary<string, District>();
            var dbMicroDistricts = new ConcurrentDictionary<string, MicroDistrict>();
            var dbSellTypes = new ConcurrentDictionary<string, SellType>();

            using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
            {
                using (var context = new RieltorServiceDBEntities())
                {
                    foreach (var entity in context.Sources.OrderBy(x => x.Name))
                    {
                        if (System.Enum.TryParse<ParsingPageType>(entity.Name, out var eResult))
                            dbSources.TryAdd(eResult, entity);
                    }

                    foreach (var entity in context.Cities.OrderBy(x => x.Id_Region).ThenBy(x => x.Name))
                        dbCities.TryAdd(entity.Name.ToLower(), new LocalEntities.City(entity));

                    foreach (var entity in context.Cost_Type)
                        dbCostTypes.TryAdd(entity.Type.ToLower(), entity);

                    foreach (var entity in context.HouseMaterials)
                    {
                        var item = new LocalEntities.HouseMaterial(entity);
                        dbHouseMaterial.TryAdd(item.Type, item);
                    }

                    foreach (var entity in context.Obj_Type)
                        dbObjTypes.TryAdd(entity.Type.ToLower(), entity);

                    foreach (var entity in context.Districts.OrderBy(x => x.Id_City))
                        dbDistricts.TryAdd(entity.Id_City + ":" + entity.Name.ToLower(),
                            new District(entity.Id, entity.Name, entity.Id_City, entity.City?.Name,
                                entity.GeoCoordinates));

                    foreach (var entity in context.MicroDistricts.OrderBy(x => x.Id_City))
                        dbMicroDistricts.TryAdd(entity.Id_City + ":" + entity.Name.ToLower(),
                            new MicroDistrict(entity.Id, entity.Name, entity.Id_District, entity.District?.Name,
                                entity.Id_City, entity.City?.Name, entity.GeoCoordinates));

                    foreach (var entity in context.SellTypes)
                        dbSellTypes.TryAdd(entity.Type.ToLower(), entity);

                    _dbSources = dbSources;
                    _dbCities = dbCities;
                    _dbCostTypes = dbCostTypes;
                    _dbHouseMaterial = dbHouseMaterial;
                    _dbObjTypes = dbObjTypes;
                    _dbDistricts = dbDistricts;
                    _dbMicroDistricts = dbMicroDistricts;
                    _dbSellTypes = dbSellTypes;
                    transaction.Complete();
                }
            }

            RegionsManager.DistrictManager = new DistrictManager();
            RegionsManager.Init(dbDistricts.Select(x => x.Value).ToList(),
                dbMicroDistricts.Select(x => x.Value).ToList());
        }

        #region Get Id or add new entity
        /// <summary>
        /// Получить ID типа объекта или Добавить новый тип источника (сайта) и получить его ID, если его еще нет
        /// </summary>
        /// <returns></returns>
        public static int GetIdOrAddNewPageSource(ParsingPageType parsingPageType)
        {
            try
            {
                if (_dbSources.TryGetValue(parsingPageType, out var value))
                {
                    return value.Id;
                }

                lock (_dbSources)
                {
                    if (_dbSources.TryGetValue(parsingPageType, out value))
                    {
                        return value.Id;
                    }
                    Log.Fatal("Добавление нового Source в БД: "+ parsingPageType.ToString());
                    var context = new RieltorServiceDBEntities();
                    var newEntity = new Source() { Name = parsingPageType.ToString() };
                    context.Sources.Add(newEntity);
                    context.SaveChanges();
                    if (System.Enum.TryParse<ParsingPageType>(newEntity.Name, out var eResult))
                        _dbSources.TryAdd(eResult, newEntity);
                    return newEntity.Id;
                }
            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
            }
            catch (Exception e)
            {
                Log.Exception((e.InnerException?.InnerException ?? e.InnerException) ?? e);
            }
            return -1;
        }

        /// <summary>
        /// Получить ID города или Добавить новый город и получить его ID, если его еще нет
        /// </summary>
        /// <returns></returns>
        public static int GetIdCity(string cityName)
        {
            try
            {
                var cityNameLower = cityName?.Replace(",", "").Trim().ToLower();
                if (string.IsNullOrEmpty(cityNameLower))
                    return -1;
                if (_dbCities.TryGetValue(cityNameLower, out var value))
                {
                    return value.Id;
                }
                lock (_dbCities)
                {
                    if (_dbCities.TryGetValue(cityNameLower, out value))
                    {
                        return value.Id;
                    }

                    using (var context = new RieltorServiceDBEntities())
                    {
                        var newEntity = new City() { Name = cityName };
                        context.Cities.Add(newEntity);
                        context.SaveChanges();
                        _dbCities.TryAdd(cityNameLower, new LocalEntities.City(newEntity));
                        return newEntity.Id;
                    }

                    return -1;
                }
            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
            }
            catch (Exception e)
            {
                Log.Exception((e.InnerException?.InnerException ?? e.InnerException) ?? e);
            }

            return -1;
        }
        public static string GetNameCity(int id)
        {
            var city = _dbCities.FirstOrDefault(x => x.Value.Id == id);
            return city.Value.Name;
        }

        /// <summary>
        /// Получить ID типа стоимости или Добавить новый и получить его ID, если его еще нет
        /// </summary>
        /// <returns></returns>
        public static int GetIdOrAddNewCostType(string costTypeName)
        {
            try
            {
                if (_dbCostTypes.TryGetValue(costTypeName.ToLower(), out var value))
                {
                    return value.Id;
                }

                lock (_dbCostTypes)
                {
                    if (_dbCostTypes.TryGetValue(costTypeName.ToLower(), out value))
                    {
                        return value.Id;
                    }

                    var context = new RieltorServiceDBEntities();

                    var newEntity = new Cost_Type() { Type = costTypeName };
                    context.Cost_Type.Add(newEntity);
                    context.SaveChanges();
                    _dbCostTypes.TryAdd(newEntity.Type.ToLower(), newEntity);
                    return newEntity.Id;
                }
            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
            }
            catch (Exception e)
            {
                Log.Exception((e.InnerException?.InnerException ?? e.InnerException) ?? e);
            }

            return -1;
        }

        /// <summary>
        /// Получить ID типа дома или Добавить новый и получить его ID, если его еще нет
        /// </summary>
        /// <returns></returns>
        public static int GetIdOrAddNewHouseMaterial(EnumHouseMaterial houseType)
        {
            try
            {
                if (houseType == EnumHouseMaterial.Undefined)
                    return -1;

                if (_dbHouseMaterial.TryGetValue(houseType, out var value))
                {
                    return value.Id ?? -1;
                }

                /*lock (DbHouseMaterial)
                {
                    if (DbHouseMaterial.TryGetValue(houseTypeName.ToLower(), out value))
                    {
                        return value.Id;
                    }

                    var context = new RieltorServiceDBEntities();
                    var newEntity = new HouseMaterial() { Type = houseTypeName };
                    context.HouseMaterials.Add(newEntity);
                    context.SaveChanges();
                    DbHouseMaterial.TryAdd(newEntity.Type.ToLower(), newEntity);

                    return newEntity.Id;
                }*/
            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
            }
            catch (Exception e)
            {
                Log.Exception((e.InnerException?.InnerException ?? e.InnerException) ?? e);
            }

            return -1;
        }

        /// <summary>
        /// Получить ID типа объявления или добавить новый и получить его ID, если его еще нет
        /// </summary>
        /// <returns></returns>
        public static int GetIdOrAddNewObjType(string objTypeName)
        {
            if (string.IsNullOrEmpty(objTypeName))
                return -1;
            try
            {
                if (_dbObjTypes.TryGetValue(objTypeName.ToLower(), out var value))
                {
                    return value.Id;
                }

                lock (_dbObjTypes)
                {
                    if (_dbObjTypes.TryGetValue(objTypeName.ToLower(), out value))
                    {
                        return value.Id;
                    }

                    var context = new RieltorServiceDBEntities();
                    var newEntity = new Obj_Type() { Type = objTypeName };
                    context.Obj_Type.Add(newEntity);
                    context.SaveChanges();

                    _dbObjTypes.TryAdd(newEntity.Type.ToLower(), newEntity);
                    return newEntity.Id;
                }

            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
            }
            catch (Exception e)
            {
                Log.Exception((e.InnerException?.InnerException ?? e.InnerException) ?? e);
            }

            return -1;
        }

        /// <summary>
        /// Получить ID района
        /// </summary>
        /// <returns></returns>
        public static int GetIdDistrict(string name, int idcity)
        {
            if (string.IsNullOrEmpty(name))
                return -1;
            try
            {
                if (_dbDistricts.TryGetValue(idcity+":"+name.ToLower(), out var value))
                {
                    return value.Id;
                }

                /*lock (DbDistricts)
                {
                    if (DbDistricts.TryGetValue(idcity + ":" + name.ToLower(), out value))
                    {
                        return value.Id;
                    }

                    var context = new RieltorServiceDBEntities();
                    var newEntity = new District() { Name = name, Id_City = idcity };
                    context.Districts.Add(newEntity);
                    context.SaveChanges();

                    DbDistricts.TryAdd(idcity + ":" + newEntity.Name.ToLower(), new LocalEntities.District(newEntity));
                    return newEntity.Id;
                }*/
            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
            }
            catch (Exception e)
            {
                Log.Exception((e.InnerException?.InnerException ?? e.InnerException) ?? e);
            }

            return -1;
        }

        /// <summary>
        /// Получить ID мкр
        /// </summary>
        /// <returns></returns>
        public static int GetIdMicroDistrict(string name, int idDistrict, int idCity)
        {
            if (string.IsNullOrEmpty(name))
                return -1;
            try
            {
                if (_dbMicroDistricts.TryGetValue(idCity+":"+name.ToLower(), out var value))
                {
                    return value.Id;
                }
                var context = new RieltorServiceDBEntities();

                /*lock (DbMicroDistricts)
                {
                    var newEntity = new MicroDistrict() { Name = name, Id_City = idCity };
                    if (idDistrict != -1)
                        newEntity.Id_District = idDistrict;
                    context.MicroDistricts.Add(newEntity);
                    context.SaveChanges();

                    DbMicroDistricts.TryAdd(idCity + ":" + newEntity.Name.ToLower(), new LocalEntities.MicroDistrict(newEntity));
                    return newEntity.Id;
                }*/
            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
            }
            catch (Exception e)
            {
                Log.Exception((e.InnerException?.InnerException ?? e.InnerException) ?? e);
            }

            return -1;
        }

        /// <summary>
        /// Проверит по координатам - это новый адрес или нет
        /// </summary>
        /// <param name="coord"></param>
        /// <param name="adress">Вернется объект, если такой адрес уже имеется</param>
        /// <returns></returns>
        public static bool TryFindAdress(GeoCoordinate coord, out Adress adress)
        {
            adress = null;
            if (coord == null || coord.IsEmpty)
                return false;
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))
                {
                    using (var context = new RieltorServiceDBEntities())
                    {
                        var coordinate = coord.ToString();
                        adress = context.Adresses.FirstOrDefault(x => x.Geolocation == coordinate);
                        transaction.Complete();
                        return adress != null;
                    }
                }
            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
            }
            catch (Exception e)
            {
                Log.Exception((e.InnerException?.InnerException ?? e.InnerException) ?? e);
            }

            return false;
        }

        public static int? GetIdSellType(Enum.SellType sellType)
        {
            if (sellType == Enum.SellType.Undefined)
                return null;
            if (_dbSellTypes.TryGetValue(sellType.ToString().ToLower(), out var value))
                return value.Id;

            try
            {
                var dbSellTypes = new ConcurrentDictionary<string, SellType>();
                using (var context = new RieltorServiceDBEntities())
                {
                    context.SellTypes.Add(new SellType() { Type = sellType.ToString() });
                    context.SaveChanges();
                    foreach (var entity in context.SellTypes)
                        dbSellTypes.TryAdd(entity.Type.ToLower(), entity);
                }
                _dbSellTypes = dbSellTypes;
                if (_dbSellTypes.TryGetValue(sellType.ToString().ToLower(), out value))
                    return value.Id;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "DbObjManager::GetIdSellType");
            }
            Log.Error("Не найден ID SellType: "+ sellType);
            return null;
        }

        #endregion

        public static void UpdateMicroDistrict(MicroDistrict mkr)
        {
            using (var context = new RieltorServiceDBEntities())
            {
                var oldEntity = context.MicroDistricts.FirstOrDefault(x => x.Id == mkr.Id);
                if (oldEntity == null)
                {
                    Log.Error("не могу обновить, тк не найден мкр "+ mkr.Id);
                    return;
                }

                oldEntity.Id_District = mkr.IdDistrict;
                context.MicroDistricts.AddOrUpdate(oldEntity);
                context.SaveChanges();
            }
        }
    }
}

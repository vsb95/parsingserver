using System;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Transactions;
using Core;
using Core.Enum;
using CoreParser.DB.Models;
using CoreParser.Enum;
using CoreParser.Extension;
using CoreParser.Pages;
using Logger;

namespace CoreParser.DB
{
    public static class DbManager
    {
        public const int DefaultRetryCount = 5;
        public const int DeadlockErrorNumber = 1205;
        public const int LockingErrorNumber = 1222;
        public const int UpdateConflictErrorNumber = 3960;

        
        public static void Init()
        {
            Log.Info("DbManager Initializing...");
            using (var dbContext = new RieltorServiceDBEntities())
            {
                dbContext.Database.CreateIfNotExists();
            }
            DbObjectManager.Init();
        }

        /// <summary>
        /// Получить страницу по id
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static AbstractAdvertisement GetAbstractAdvertisementById(int id)
        {
            var watch = new Stopwatch();
            watch.Start();
            AbstractAdvertisement abstractAdv = null;
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))
                {
                    using (var context = new RieltorServiceDBEntities())
                    {
                        var pages = context.Pages.Where(page => page.Id == id);

                        if (pages.Count() == 0)
                            return null;
                        if (pages.Count() > 1)
                        {
                            Log.Warn("Данному id соответствует более 1 страницы; id: " + id);
                            pages = pages.OrderBy(page => page.Date_LastRefresh);
                        }

                        abstractAdv = pages.FirstOrDefault()?.ToAbstractAdvertisement();
                        transaction.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true);
            }
            finally
            {
                if (abstractAdv != null)
                    Log.Trace("Поиск '" + id + "' занял " + Math.Truncate(watch.Elapsed.TotalMilliseconds) + " ms");
            }

            return abstractAdv;
        }

        /// <summary>
        /// Получить страницу по SearchIndex
        /// </summary>
        /// <param name="searchIndex"></param>
        /// <returns></returns>
        private static AbstractAdvertisement GetAbstractAdvertisementBySearchIndex(string searchIndex)
        {
            var watch = new Stopwatch();
            watch.Start();
            AbstractAdvertisement abstractAdv = null;
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                    {IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted}))
                {
                    using (var context = new RieltorServiceDBEntities())
                    {

                        var pages = context.Pages.Where(page => page.SearchIndex == searchIndex);

                        if (pages.Count() == 0)
                            return null;
                        if (pages.Count() > 1)
                        {
                            Log.Warn("Данному id соответствует более 1 страницы; searchIndex: " + searchIndex);
                            pages = pages.OrderBy(page => page.Date_LastRefresh);
                        }

                        abstractAdv = pages.FirstOrDefault()?.ToAbstractAdvertisement();
                        transaction.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true);
                if (ex.InnerException != null)
                {
                    Log.Exception(ex.InnerException, LogType.Error, true);
                    if (ex.InnerException.InnerException != null)
                        Log.Exception(ex.InnerException.InnerException, LogType.Error, true);
                }
            }
            finally
            {
                if (abstractAdv != null)
                    Log.Trace("Поиск '" + searchIndex + "' занял " + Math.Truncate(watch.Elapsed.TotalMilliseconds) +
                              " ms");
            }

            return abstractAdv;
        }

        /// <summary>
        /// Получить страницу по урлу
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static AbstractAdvertisement GetAbstractAdvertisementByUrl(string url)
        {
            var searchIndex = AbstractAdvertisement.GetSearchIndex(url);
            return string.IsNullOrEmpty(searchIndex) ? null : GetAbstractAdvertisementBySearchIndex(searchIndex);
        }

        /// <summary>
        /// Быстрая проверка на существование такой страницы
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static bool IsExistAdvertisementByUrl(string url)
        {
            var searchIndex = AbstractAdvertisement.GetSearchIndex(url);
            if (string.IsNullOrEmpty(searchIndex))
                return false;

            var watch = new Stopwatch();
            watch.Start();
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))
                {
                    using (var context = new RieltorServiceDBEntities())
                    {
                        var res = context.Pages.Any(page => page.SearchIndex == searchIndex);
                        transaction.Complete();
                        return res;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true);
                if (ex.InnerException != null)
                {
                    Log.Exception(ex.InnerException, LogType.Error, true);
                    if (ex.InnerException.InnerException != null)
                        Log.Exception(ex.InnerException.InnerException, LogType.Error, true);
                }
            }
            finally
            {
                Log.Trace("Поиск '" + searchIndex + "' занял " + Math.Truncate(watch.Elapsed.TotalMilliseconds) + " ms");
            }

            return false;
        }

        public static bool AddOrUpdatePage(AbstractAdvertisement abstractAdvertisement)
        {
            var trace = new StringBuilder();
            try
            {
                if (abstractAdvertisement == null)
                {
                    Log.Error("[DbManager::AddOrUpdatePage] Попытка добавить пустую страницу");
                    return false;
                }

                if (abstractAdvertisement.IsDeleted && abstractAdvertisement.Id == null)
                {
                    Log.Error("[DbManager::AddOrUpdatePage] Попытка добавить удаленную страницу");
                    return false;
                }

                //Если у нас млсн мсл, но почему то не агентсво - исправим это
                if (abstractAdvertisement.Type == ParsingPageType.Mlsn
                    && abstractAdvertisement.IsMls
                    && !abstractAdvertisement.IsAgency)
                {
                    abstractAdvertisement.IsAgency = true;
                }

                if (abstractAdvertisement.IsDeleted)
                    abstractAdvertisement.IdMasterPage = null;

                trace.AppendLine("start abstractAdvertisement.ToDbPage()");
                bool isNeedUpdateImages = false;
                var page = abstractAdvertisement.ToDbPage();
                if (page == null)
                    throw new Exception("Не удалось преобразовать abstractAdvertisement.ToDbPage(): " + abstractAdvertisement.Url);
                if (abstractAdvertisement.Id.HasValue)
                {
                    trace.AppendLine("UpdatePage id = " + abstractAdvertisement.Id);
                    Log.Trace("UpdatePage id = " + abstractAdvertisement.Id
                        +"\tLastRefreshDate: "+abstractAdvertisement.LastRefreshDateTime?.ToString("g"));
                    var originPage = GetAbstractAdvertisementById(abstractAdvertisement.Id.Value);
                    if (originPage.DownloadedImagesPathList?.Count !=
                        abstractAdvertisement.DownloadedImagesPathList?.Count)
                    {
                        trace.AppendLine("количество изображений не совпало");
                        isNeedUpdateImages = true;
                    }
                    else if(abstractAdvertisement.DownloadedImagesPathList != null)
                    {
                        foreach (var newImage in abstractAdvertisement.DownloadedImagesPathList)
                        {
                            if (originPage.DownloadedImagesPathList.Contains(newImage)) continue;
                            trace.AppendLine("нашли другое изображение: " + newImage);
                            isNeedUpdateImages = true;
                            break;
                        }
                    }
                }
                else
                {
                    trace.AppendLine("Try Add Page");
                    // Проверим, вдруг пытаемся добавить то что уже есть, но без id
                    if (IsExistAdvertisementByUrl(abstractAdvertisement.Url))
                    {
                        trace.Append(": ExistAdvertisementByUrl");
                        return false;
                    }

                    trace.Append("Add Page");
                    Log.Trace("AddPage");
                    page.Date_Create = DateTime.Now;
                }

                var attemptNumber = 1;
                while (true)
                {
                    try
                    {
                        var context = new RieltorServiceDBEntities();
                        {
                            using (var transaction = context.Database.BeginTransaction())
                            {
                                if (isNeedUpdateImages)
                                {
                                    trace.AppendLine("Update images: " + abstractAdvertisement.SearchIndex);
                                    var origPage = context.Pages.First(x => x.Id == page.Id);
                                    Log.Debug("Update images: " + abstractAdvertisement.SearchIndex 
                                             + " " + origPage.Images.Count + " -> " + page.Images.Count);

                                    if (origPage.Images != null && origPage.Images.Count > 0)
                                    {
                                        foreach (var image in origPage.Images.ToList())
                                            context.Images.Remove(image);
                                    }

                                    origPage.Images = page.Images;
                                    context.Pages.AddOrUpdate(origPage);
                                    context.SaveChanges();
                                }

                                trace.AppendLine("AddOrUpdate page");
                                context.Pages.AddOrUpdate(page);
                                context.Adresses.AddOrUpdate(page.Adress);
                                context.SaveChanges();

                                transaction.Commit();
                                trace.AppendLine("end transaction");

                                if (!abstractAdvertisement.Id.HasValue)
                                {
                                    trace.AppendLine("update id");
                                    abstractAdvertisement.Id = page?.Id;
                                    trace.AppendLine("update adress id");
                                    abstractAdvertisement.Adress.Id = page?.Id_Adress ?? 0;
                                }

                                if (abstractAdvertisement.Adress.Id == null || abstractAdvertisement.Adress.Id == 0)
                                {
                                    Log.PrintStackTrace(5);
                                    Log.Error("Объявление без Id_Adress: " + abstractAdvertisement.Url, true);
                                    Thread.Sleep(3000);
                                }

                                break;
                            }
                        }
                    }
                    catch(DbEntityValidationException vex)
                    {
                        // Retrieve the error messages as a list of strings.
                        var errorMessages = vex.EntityValidationErrors
                            .SelectMany(x => x.ValidationErrors)
                            .Select(x => x.ErrorMessage);

                        // Join the list to a single string.
                        var fullErrorMessage = string.Join("; ", errorMessages);

                        // Combine the original exception message with the new one.
                        var exceptionMessage = string.Concat(vex.Message, " The validation errors are: ", fullErrorMessage);

                        // Throw a new DbEntityValidationException with the improved exception message.
                        var newVex = new DbEntityValidationException(exceptionMessage, vex.EntityValidationErrors);
                        Log.Exception(newVex, LogType.Error, true); 
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException is SqlException sqlException)
                        {
                            // если отваливаемся по таймауту
                            if (sqlException.Number == -2)
                            {
                                if (attemptNumber == DefaultRetryCount + 1)
                                {
                                    Log.Exception(ex, LogType.Error, true);
                                    return false;
                                }

                                Thread.Sleep(SettingsManager.IsStartAsAPI ? 5000 : 15000);
                                attemptNumber++;
                            }
                            if (!sqlException.Errors.Cast<SqlError>().Any(error =>
                                (error.Number == DeadlockErrorNumber) ||
                                (error.Number == LockingErrorNumber) ||
                                (error.Number == UpdateConflictErrorNumber)))
                            {
                                Log.Exception(ex, LogType.Error, true);
                                return false;
                            }

                            if (attemptNumber == DefaultRetryCount + 1)
                            {
                                Log.Exception(ex, LogType.Error, true);
                                return false;
                            }

                            Thread.Sleep(15);
                            attemptNumber++;
                            continue;
                        }

                        Log.Exception(ex, LogType.Error, true, "[DbManager::AddOrUpdatePage]");
                        return false;
                    }
                }

                if (isNeedUpdateImages && abstractAdvertisement != null)
                {
                    trace.AppendLine("NeedUpdateImages");
                    var origin = abstractAdvertisement?.DownloadedImagesPathList?.Count;
                    var updated = GetAbstractAdvertisementById(abstractAdvertisement.Id.Value)?.DownloadedImagesPathList?.Count;

                    if (origin != null && origin > 0 && origin != updated)
                    {
                        Log.Error(
                            "разница в количестве после обновления: пытались = " + origin + "; в БД по факту = " + updated +
                            "; разница = " + (origin - updated) + "\n" +
                            abstractAdvertisement.Url, true);
                    }
                }

                trace.AppendLine("end");
                return true;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "[DbManager::AddOrUpdatePage-global]:\n" + trace);
            }
            return false;
        }

        
    }
}

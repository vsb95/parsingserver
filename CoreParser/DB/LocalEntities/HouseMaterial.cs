﻿using System;
using CoreParser.Enum;
using Newtonsoft.Json;

namespace CoreParser.DB.LocalEntities
{
    [JsonObject(Id = "house-material", MemberSerialization = MemberSerialization.OptIn)]
    public class HouseMaterial
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("name")] public string Name => JsonPrepare();
        public EnumHouseMaterial Type { get; set; } = EnumHouseMaterial.Undefined;

        public HouseMaterial()
        {

        }

        public HouseMaterial(DB.Models.HouseMaterial dbHouseMaterial)
        {
            if (dbHouseMaterial == null)
                throw new NullReferenceException("При создании объекта HouseMaterial передан null dbHouseMaterial");
            Id = dbHouseMaterial.Id;
            if (Type == EnumHouseMaterial.Undefined && System.Enum.TryParse(dbHouseMaterial.Type, out EnumHouseMaterial houseMaterial))
                Type = houseMaterial;
        }

        public HouseMaterial(EnumHouseMaterial type)
        {
            var id = DbObjectManager.GetIdOrAddNewHouseMaterial(type);
            if (id != -1)
            {
                Id = id;
            }

            Type = type;
        }

        public static bool TryParse(string type, out HouseMaterial result)
        {
            result = null;
            if (string.IsNullOrEmpty(type))
                return false;
            try
            {
                if (System.Enum.TryParse(type, out EnumHouseMaterial houseMaterial))
                {
                    result = new HouseMaterial(houseMaterial);
                    return true;
                }

                type = type?.ToLower()?.Trim();
                EnumHouseMaterial eType = EnumHouseMaterial.Undefined;
                if (type == "кирпич"
                    || type == "кирпичный"
                    || type == "кирпичное"
                    || type == "кирпично-насыпной")
                {
                    eType = EnumHouseMaterial.Кирпичный;
                }
                else if (type.Contains("брус")
                         || type.Contains("бревно")
                         || type.Contains("деревян")
                         || type.Contains("рубленый")
                         || type.Contains("дерев"))
                {
                    eType = EnumHouseMaterial.РубленыйБрусовой;
                }
                else if (type.Contains("панельный")
                         || type.Contains("сэндвич-панели")
                         || type.Contains("панель"))
                {
                    eType = EnumHouseMaterial.Панельный;
                }
                else if (type.Contains("кирпич - монолит")
                         || type.Contains("монолитно-кирпичный")
                         || type.Contains("кирпично-монолитное"))
                {
                    eType = EnumHouseMaterial.МонолитноКирпичный;
                }
                else if (type.Contains("монолит"))
                {
                    eType = EnumHouseMaterial.Монолитный;
                }
                else if (type.Contains("каркасно-насыпн"))
                {
                    eType = EnumHouseMaterial.КаркасноНасыпной;
                }
                else if (type.Contains("экспериментальные мате")
                         || type.Contains("блок")
                         || type.Contains("блочный")
                         || type.Contains("блочное")
                         || type.Contains("прочее")
                         || type.Contains("не указано")
                         || type.Contains("другое")
                         || type.Contains("металл")
                         || type.Contains("ж/б панели"))
                {
                    eType = EnumHouseMaterial.Прочее;
                }
                else
                {
                    eType = EnumHouseMaterial.Прочее;
                    Logger.Log.Error("Не распознан HouseMaterial: '" + type + "', Считаем что это прочее", true);
                }

                result = new HouseMaterial(eType);
                return true;
            }
            catch (Exception e)
            {
                Logger.Log.Exception(e);
            }

            return false;
        }

        public string JsonPrepare()
        {
            if (Type == EnumHouseMaterial.Undefined && System.Enum.TryParse(Name, out EnumHouseMaterial houseMaterial))
                Type = houseMaterial;
            switch (Type)
            {
                case EnumHouseMaterial.РубленыйБрусовой:
                    return "Рубленый/брусовой";
                case EnumHouseMaterial.КаркасноНасыпной:
                    return "Каркасно-насыпной";
                case EnumHouseMaterial.МонолитноКирпичный:
                    return "Монолитно-кирпичный";
                default:
                    return Type.ToString();
            }
        }
    }
}

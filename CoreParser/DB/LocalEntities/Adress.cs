﻿using System;
using Core;
using Logger;
using Newtonsoft.Json;

namespace CoreParser.DB.LocalEntities
{
    [JsonObject(Id = "adress", MemberSerialization = MemberSerialization.OptIn)]
    public class Adress
    {
        public int? Id { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("street")]
        public string Street { get; set; }
        [JsonProperty("house-number")]
        public string House { get; set; }
        [JsonProperty("flat")]
        public string Flat { get; set; }
        [JsonProperty("region")]
        public string Region { get; set; }

        /// <summary>
        /// Адрес в строковом представлении для поиска через геокодер
        /// </summary>
        internal string GeoSearchString { get; set; }

        /// <summary>
        /// Координата
        /// </summary>
        [JsonProperty("coord")]
        public GeoCoordinate GeoCoordinate { get; set; } = new GeoCoordinate();

        [JsonProperty("max-floor")]
        public int? MaxFloor { get; set; }
        [JsonProperty("house-material")]
        public HouseMaterial HouseMaterial { get; set; }
        
        [JsonProperty("district")]
        public string District { get; set; }
        [JsonProperty("micro-district")]
        public string MicroDistrict { get; set; }

        /// <summary>
        /// Это область?
        /// </summary>
        public bool ItIsSubCity { get; set; } = false;

        public int? IdCity { get; set; }
        public int? IdDistrict { get; set; }
        public int? IdMicroDistrict { get; set; }

        public Adress()
        {
            
        }

        /// <summary>
        /// Создать БД сущность из локального адреса
        /// </summary>
        public DB.Models.Adress ToDbAdress()
        {
            var result = new DB.Models.Adress
            {
                Id_City = IdCity ?? DbObjectManager.GetIdCity(City),
                Street = Street,
                House = House,
                Flat = Flat,
                Geolocation = GeoCoordinate?.ToString(),
                Region = Region,
                MaxFloor = MaxFloor
            };

            if (result.Id_City == -1)
            {
                result.Id_City = 0;
                Log.Error("Указан неизвестный город, либо он не определен: '" + City + "'", true);
            }

            if (Id != null && Id != 0)
            {
                result.Id = Id.Value;
            }

            if (HouseMaterial != null)
            {
                var idHouseMaterial = DbObjectManager.GetIdOrAddNewHouseMaterial(HouseMaterial.Type);
                if (idHouseMaterial != -1)
                    result.Id_HouseMaterial = idHouseMaterial;
            }
            
            if (IdDistrict == null)
                IdDistrict = DbObjectManager.GetIdDistrict(District, result.Id_City);
            if (IdDistrict != -1)
            {
                result.Id_District = IdDistrict;
            }

            if (IdMicroDistrict == null)
                IdMicroDistrict = DbObjectManager.GetIdMicroDistrict(MicroDistrict, IdDistrict ?? -1, result.Id_City);
            if (IdMicroDistrict != -1)
                result.Id_MicroDistrict = IdMicroDistrict;
            return result;
        }

        public override string ToString()
        {
            return string.Format("{0}, {1} ({2})", City, Street, GeoCoordinate);
        }
    }
}

﻿using Newtonsoft.Json;

namespace CoreParser.DB.LocalEntities
{
    [JsonObject(Id = "city", MemberSerialization = MemberSerialization.OptIn)]
    public class City
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("region-name")]
        public string RegionName { get; set; }

        public City()
        {

        }
        public City(Models.City dbCity)
        {
            Id = dbCity.Id;
            Name = dbCity.Name;
            RegionName = dbCity.Region.Name;
        }
    }
}

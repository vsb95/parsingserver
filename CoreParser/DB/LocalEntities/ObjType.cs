﻿using System;
using System.Linq;
using CoreParser.DB.Models;
using CoreParser.Enum;
using Newtonsoft.Json;

namespace CoreParser.DB.LocalEntities
{
    [JsonObject(Id = "obj-type", MemberSerialization = MemberSerialization.OptIn)]
    public class ObjType
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        public EnumObjType Type { get; set; } = EnumObjType.Undefined;

        public ObjType()
        {
            
        }

        public ObjType(Obj_Type objType)
        {
            Id = objType.Id;
            Name = objType.Type;
            if (Type == EnumObjType.Undefined && TryParse(Name, out var newObjType))
                Type = newObjType.Type;
        }

        public ObjType(EnumObjType type)
        {
            string typeString = "";
            switch (type)
            {
                case EnumObjType.Dacha:
                    typeString = "Дача";
                    break;
                case EnumObjType.Dom:
                    typeString = "Дом";
                    break;
                case EnumObjType.Kottedj:
                    typeString = "Коттедж";
                    break;
                case EnumObjType.Studia:
                    typeString = "Студия";
                    break;
                case EnumObjType.Komnata:
                    typeString = "Комната";
                    break;
                case EnumObjType.Kvartira1:
                    typeString = "1-комн";
                    break;
                case EnumObjType.Kvartira2:
                    typeString = "2-комн";
                    break;
                case EnumObjType.Kvartira3:
                    typeString = "3-комн";
                    break;
                case EnumObjType.Kvartira4:
                    typeString = "4-комн";
                    break;
                case EnumObjType.Kvartira5Plus:
                    typeString = "5+ комн";
                    break;
                case EnumObjType.Dolya:
                    typeString = "Доля в квартире";
                    break;
                case EnumObjType.Commercial:
                    typeString = "Коммерческая";
                    break;
                case EnumObjType.Uchastok:
                    typeString = "Участок";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
            var entity = DbObjectManager.ObjTypes.FirstOrDefault(x => x.Type == typeString);
            if (entity == null)
            {
                int id = DbObjectManager.GetIdOrAddNewObjType(typeString);
                entity = DbObjectManager.ObjTypes.FirstOrDefault(x => x.Type == typeString);
            }

            if (entity != null)
                Id = entity.Id;
            

            Name = typeString;
            Type = type;
        }

        public static bool TryParse(string type, out ObjType result)
        {
            // ' > Новостройки > Своб. планировка > Купить > Квартиры > Недвижимость > Все объявления в Омске'
            result = null;
            type = type?.ToLower()?.Trim();
            if (string.IsNullOrEmpty(type) 
                || type== "посуточно" 
                || type == "на длительный срок" 
                || type == "вторичка"
                || type == "аренда"
                || type == "новостройки"
                || type == "аренда квартир, комнат"
                || type == "главная"
                || type == "продажа"
                || type == "недвижимость"
                || type == "квартиры"
                || type.Contains("объявлени")
                || type.Contains("улиц")
                || type.Contains("недвижимость в ")
                || type.Contains("сдам")
                || type.Contains("ипотека")
                || type.Contains("купить")
                || type.Contains("снять")
                || type.Contains("животные")
                || type.Contains("продам"))
                return false;
            EnumObjType eType = EnumObjType.Undefined;
            try
            {
                if (type.Contains("дач"))
                {
                    eType = EnumObjType.Dacha;
                }
                else if(type.Contains("дом") 
                        || type.Contains("таунхаус") || type == "поселений (ижс)")
                {
                    eType = EnumObjType.Dom;
                }
                else if (type.Contains("1-к") 
                         || type.Contains("1 комн"))
                {
                    eType = EnumObjType.Kvartira1;
                }
                else if (type.Contains("2-к") 
                         || type.Contains("2 комн"))
                {
                    eType = EnumObjType.Kvartira2;
                }
                else if (type.Contains("3-к") 
                         || type.Contains("3 комн"))
                {
                    eType = EnumObjType.Kvartira3;
                }
                else if (type.Contains("4-к") 
                         || type.Contains("4 комн"))
                {
                    eType = EnumObjType.Kvartira4;
                }
                else if (type.Contains("-к") 
                        || type.Contains("комн.") 
                        || type.Contains("комнатные квартиры") 
                        || type.Contains("5+ "))
                {
                    eType = EnumObjType.Kvartira5Plus;
                }
                else if (type.Contains("студ")
                         || type.Contains("свободной планировкой")
                         || type.Contains("со свободной планировкой")
                         || type == "своб. планировка")
                {
                    eType = EnumObjType.Studia;
                }
                else if (type.Contains("участ"))
                {
                    eType = EnumObjType.Uchastok;
                }
                else if (type.Contains("коттедж"))
                {
                    eType = EnumObjType.Kottedj;
                }
                else if (type.Contains("комнат"))
                {
                    eType = EnumObjType.Komnata;
                }
                else if (type.Contains("доли в квартире") || type.Contains("доля в квартире"))
                {
                    eType = EnumObjType.Dolya;
                }
                else if (type.Contains("магазин") 
                         || type.Contains("офис") 
                         || type.Contains("помещени")
                         || type.Contains("площади")
                         || type.Contains("другое")
                         || type.Contains("коммерчес")
                         || type.Contains("сто")
                         || type.Contains("производств")
                         || type.Contains("сельхозназначения")
                         || type.Contains("досуг")
                         || type.Contains("готовый бизнес")
                         || type.Contains("здание")
                         || type.Contains("квартиры на первых этажах"))
                {
                    eType = EnumObjType.Commercial;
                }
                else if (type.Contains(""))
                {
                    eType = EnumObjType.Undefined;
                    Logger.Log.Fatal("Не распознан ObjType: '" + type + "'");
                    return false;
                }

                result = new ObjType(eType);
                return true;
            }
            catch (Exception e)
            {
                Logger.Log.Exception(e);
            }

            return false;
        }

        public string JsonPrepare()
        {
            if (Type == EnumObjType.Undefined && TryParse(Name, out var newObjType))
                Type = newObjType.Type;
            switch (Type)
            {
                case EnumObjType.Kvartira1:
                    return "1 комнатная квартира";
                case EnumObjType.Kvartira2:
                    return "2 комнатная квартира";
                case EnumObjType.Kvartira3:
                    return "3 комнатная квартира";
                case EnumObjType.Kvartira4:
                    return "4 комнатная квартира";
                case EnumObjType.Kvartira5Plus:
                    return "5+ комнатная квартира";
                default:
                    return Name;
            }
        }

        public override string ToString()
        {
            return Name + " : " + Id;
        }
    }
}

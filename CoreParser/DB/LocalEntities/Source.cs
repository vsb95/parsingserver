﻿using Core.Enum;
using CoreParser.Enum;
using Newtonsoft.Json;

namespace CoreParser.DB.LocalEntities
{
    [JsonObject(Id = "source", MemberSerialization = MemberSerialization.OptIn)]
    public class Source
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }

        public Source()
        {

        }
        public Source(DB.Models.Source dbSource, out bool isVisible)
        {
            isVisible = true;
            Id = dbSource.Id;
            if (!System.Enum.TryParse(dbSource.Name, out ParsingPageType pageType))
            {
                isVisible = false;
                Name = dbSource.Name;
                return;
            }

            switch (pageType)
            {
                case ParsingPageType.Cian:
                    isVisible = false;
                    Name = "Циан";
                    break;
                case ParsingPageType.Yandex:
                    Name = "Я.Недвижимость";
                    break;
                case ParsingPageType.N1:
                    Name = "N1";
                    break;
                case ParsingPageType.Avito:
                case ParsingPageType.Mlsn:
                    Name = dbSource.Name;
                    break;
                default:
                    isVisible = false;
                    break;
            }
        }
    }
}

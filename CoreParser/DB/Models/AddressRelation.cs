//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoreParser.DB.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AddressRelation
    {
        public long Id { get; set; }
        public int IdSource { get; set; }
        public string GeoSearchString { get; set; }
        public int IdAddress { get; set; }
    
        public virtual Adress Adress { get; set; }
        public virtual Source Source { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using CoreParser.DB.Models;
using CoreParser.Extension;
using CoreParser.Pages;
using Logger;

namespace CoreParser.DB.Managers
{
    public static class DbDebugManager
    {
        public static List<int> ForceUpdateSearchindex()
        {
            var listBadid = new List<int>();
            try
            {
                var regexStr = "(:" + string.Join(":|:", DbObjectManager.Sources.Select(x => x.Id)) + ":)";

                var regex = new Regex(regexStr, RegexOptions.Compiled);

                using (var context = new RieltorServiceDBEntities())
                {
                    int i = 0;
                    int maxcount = context.Pages.Count(x => x.IsDeleted == null || !x.IsDeleted.Value);
                    var percent = maxcount / 100;
                    foreach (var page in context.Pages.Where(x => x.IsDeleted == null || !x.IsDeleted.Value)
                        .OrderByDescending(x => x.Date_Create))
                    {
                        i++;
                        if (i % 5000 == 0)
                        {
                            Log.Debug("Проверено " + (i / percent) + "% (" + i + " из " + maxcount +
                                      "); Найдено плохих: " + listBadid.Count);
                        }

                        var newSearchIndex = AbstractAdvertisement.GetSearchIndex(page.URL);
                        if (page.SearchIndex == newSearchIndex)
                            continue;
                        Log.Debug(page.SearchIndex + " => " + newSearchIndex);
                        var oldSearchIndex = page.SearchIndex;
                        page.SearchIndex = newSearchIndex;
                        try
                        {
                            if (!DbManager.AddOrUpdatePage(page.ToAbstractAdvertisement()))
                            {
                                listBadid.Add(page.Id);
                                page.SearchIndex = oldSearchIndex;
                                page.IsDeleted = true;
                                page.IdMasterPage = null;
                                foreach (var slavePage in context.Pages.Where(x => x.IdMasterPage == page.Id))
                                {
                                    slavePage.IdMasterPage = null;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Exception(ex);
                            listBadid.Add(page.Id);
                        }
                    }
                    context.SaveChanges();
                }

                #region Поиск и удаление IsDeleted страниц
                Log.Info("Начинаем процесс удаления IsDeleted страниц");
                using (var context = new RieltorServiceDBEntities())
                {
                    var countDeleted = 0;
                    var deletedImages = context.Images.Where(x => x.Page.IsDeleted.HasValue && x.Page.IsDeleted.Value).Take(1000).ToList();
                    while (deletedImages.Count > 0)
                    {
                        foreach (var img in deletedImages)
                        {
                                context.Images.Remove(img);
                            countDeleted++;
                            if (countDeleted % 500 == 0)
                                Log.Debug("Удалено IsDeleted Изображений: " + countDeleted);
                        }
                        context.SaveChanges();
                        deletedImages = context.Images.Where(x => x.Page.IsDeleted.HasValue && x.Page.IsDeleted.Value).Take(1000).ToList();
                    }
                    context.SaveChanges();
                }
                using (var context = new RieltorServiceDBEntities())
                {
                    var countDeleted = 0;
                    var deletedPages = context.Pages.Where(x => x.IsDeleted.HasValue && x.IsDeleted.Value).Take(50).ToList();
                    while (deletedPages.Count > 0)
                    {
                        foreach (var page in deletedPages)
                        {
                            foreach (var slavePage in context.Pages.Where(x => x.IdMasterPage == page.Id))
                                slavePage.IdMasterPage = null;
                            context.Pages.Remove(page);
                            countDeleted++;
                            context.SaveChanges();
                            if (countDeleted % 100 == 0)
                                Log.Debug("Удалено IsDeleted страниц: " + countDeleted);
                        }
                        deletedPages = context.Pages.Where(x => x.IsDeleted.HasValue && x.IsDeleted.Value).Take(50).ToList();
                    }
                    context.SaveChanges();
                    Log.Info("Закончили удаление IsDeleted страниц: " + countDeleted);
                }
                #endregion

                #region Поиск и удаление адресов без привязки к страницам
                using (var context = new RieltorServiceDBEntities())
                {
                    var countDeleted = 0;
                    Log.Info("Начинаем процесс удаления адресов без страниц");
                    var adresses = context.Adresses.Where(x => x.Pages.Count == 0).Take(50).ToList();
                    while (adresses.Count > 0)
                    {
                        foreach (var adress in adresses)
                        {
                            context.Adresses.Remove(adress);
                            countDeleted++;
                            context.SaveChanges();
                            Log.Trace("Удалено адресов без привязки к страницам: " + countDeleted);
                            if (countDeleted % 100 == 0)
                                Log.Debug("Удалено адресов без привязки к страницам: " + countDeleted);
                        }
                        adresses = context.Adresses.Where(x => x.Pages.Count == 0).Take(50).ToList();
                    }
                    context.SaveChanges();
                    Log.Info("Закончили удаление адресов без привязки к страницам: " + countDeleted);
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "ForceUpdateSearchindex");
            }

            return listBadid;
        }
    }
}

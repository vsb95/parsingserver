﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Transactions;
using Core.Extension;
using CoreParser.DB.LocalEntities;
using CoreParser.DB.Models;
using CoreParser.Enum;
using CoreParser.Extension;
using CoreParser.Pages;
using Logger;

namespace CoreParser.DB.Managers
{
    public static class DbMergeManager
    {
        public static List<AbstractAdvertisement> GetNotMergedPages(int offset = 0)
        {
            var watch = new Stopwatch();
            watch.Start();
            var res = new List<AbstractAdvertisement>();
            using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
            {
                using (var context = new RieltorServiceDBEntities())
                {
                    var allowableObjTypeList = new List<int>
                    {
                        //new ObjType(EnumObjType.Commercial).Id,
                        new ObjType(EnumObjType.Komnata).Id,
                        new ObjType(EnumObjType.Kottedj).Id,
                        new ObjType(EnumObjType.Kvartira1).Id,
                        new ObjType(EnumObjType.Kvartira2).Id,
                        new ObjType(EnumObjType.Kvartira3).Id,
                        new ObjType(EnumObjType.Kvartira4).Id,
                        new ObjType(EnumObjType.Kvartira5Plus).Id,
                        new ObjType(EnumObjType.Studia).Id,
                    };
                    var idBadSellType = DbObjectManager.GetIdSellType(Enum.SellType.Новостройка);
                    var pages = context.Pages.Where(page => (page.IsDeleted == null || !page.IsDeleted.Value) && page.IdMasterPage == null && (page.Id_SellType == null || page.Id_SellType != idBadSellType));
                    pages = pages.Where(page => allowableObjTypeList.Contains(page.Id_Obj_Type.Value));
                    pages = pages.Where(page => page.Adress != null && !string.IsNullOrEmpty(page.Adress.Street));
#if DEBUG
                    var minMinutes = 10;
                    pages = pages.Where(page => DbFunctions.DiffMinutes(page.Date_LastRefresh, DateTime.Now) > minMinutes);
#else
                    var minHours = 2;
                    pages = pages.Where(page => DbFunctions.DiffHours(page.Date_LastRefresh, DateTime.Now) > minHours);
#endif
                    pages = pages.OrderBy(page => page.Date_LastRefresh);

                    var totalCountNonMerged = pages.Count();
                    Log.Debug("Найдено " + totalCountNonMerged + " объявлений на склейку; Отступ: " + offset);

                    foreach (var page in pages.Skip(offset).Take(200))
                    {
                        res.Add(page.ToAbstractAdvertisement());
                    }

                    transaction.Complete();
                }
            }

            Log.Debug("GetNotMergedPages sec = " + watch.GetTotalSeconds());
            return res;
        }

        public static List<AbstractAdvertisement> GetMergedPages(AbstractAdvertisement adv)
        {
            var result = new List<AbstractAdvertisement>();
            if (adv?.Id == null)
                return result;
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (var context = new RieltorServiceDBEntities())
                    {
                        foreach (var page in context.Pages.Where(x => x.IdMasterPage == adv.Id.Value))
                        {
                            result.Add(page.ToAbstractAdvertisement());
                        }
                    }
                    transaction.Complete();
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "Api::SearchController:Details:FindMerged");
            }

            return result;
        }

        /// <summary>
        /// Найдти и склеить объявления
        /// </summary>
        /// <param name="adv"></param>
        /// <returns>TRUE если объявление сохранено</returns>
        public static bool MergeAdvertisment(int id)
        {
            var adv = DbManager.GetAbstractAdvertisementById(id);
            if (adv?.Id == null)
            {
                Log.Error("У объявления не указан id. не могу склеить: " + adv?.SearchIndex);
                return false;
            }

            if (adv.IsDeleted)
            {
                Log.Warn("Объявление удалено. Не буду склеивать: " + adv?.SearchIndex);
                adv.IdMasterPage = null;
                return false;
            }

            if (string.IsNullOrEmpty(adv.Adress?.Street))
            {
                Log.Error("У объявления не указан адрес. не могу склеить: " + adv?.SearchIndex);
                return false;
            }


            switch (adv.RentType)
            {
                case RentType.Sell:
                    if (adv.ObjType.Type == EnumObjType.Commercial
                        || adv.ObjType.Type == EnumObjType.Kottedj
                        || adv.ObjType.Type == EnumObjType.Dacha
                        || adv.ObjType.Type == EnumObjType.Dolya
                        || adv.ObjType.Type == EnumObjType.Dom
                        || adv.ObjType.Type == EnumObjType.Undefined
                        || adv.ObjType.Type == EnumObjType.Uchastok
                        || adv.SellType == Enum.SellType.Новостройка)
                    {
                        adv.IdMasterPage = null;
                        return false;
                    }

                    break;
                case RentType.Rent:
                    if (adv.ObjType.Type == EnumObjType.Commercial
                        || adv.ObjType.Type == EnumObjType.Kottedj
                        || adv.ObjType.Type == EnumObjType.Dacha
                        || adv.ObjType.Type == EnumObjType.Dolya
                        || adv.ObjType.Type == EnumObjType.Dom
                        || adv.ObjType.Type == EnumObjType.Undefined
                        || adv.ObjType.Type == EnumObjType.Uchastok)
                    {
                        adv.IdMasterPage = null;
                        return false;
                    }

                    break;
                case RentType.Undefined:
                case RentType.DailyRent:
                    adv.IdMasterPage = null;
                    return false;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            try
            {
                var minSize = adv.Size.HasValue ? Math.Floor(adv.Size.Value) : 0;
                var search = new SearchFilter
                {
                    IdCity = adv.Adress.IdCity ?? -1,
                    SearchAdress = adv.Adress.Street,
                    MinFloor = adv.Floor ?? 0,
                    MaxFloor = adv.Floor ?? 0,
                    SizeMin = minSize,
                    SizeMax = minSize + 1,
                    RentType = (int) adv.RentType,
                    ObjTypes = new List<int> {adv.ObjType.Id},
                    Count = 50,
                    IsOnlyMerged = false
                };

                if (adv.RentType == RentType.Sell && adv.Cost.HasValue && adv.Cost.Value != -1)
                {
                    var costPercent = adv.Cost / 100 ?? 0;
                    var pagePercent = costPercent * 7;
                    search.CostMin = adv.Cost.Value - pagePercent-1;
                    search.CostMax = adv.Cost.Value + pagePercent+1;
                }

                var idSellType = DbObjectManager.GetIdSellType(adv.SellType);
                if (idSellType.HasValue)
                    search.SellTypes = new List<int> {idSellType.Value};

                var pages = DbSearchManager.SearchPages(search, out var totalCountByFilter);
                if (totalCountByFilter > 20)
                {
                    if (adv.RentType != RentType.Sell)
                    {
                        Log.Error(
                            "Подозрительно много объявлений на склейку для " + adv.SearchIndex + " : " +
                            totalCountByFilter + "\n" + string.Join("\r\n", pages.Select(x => x.Url)), true);
                        return false;
                    }
                }

                if (pages.Count == 0)
                {
                    if (adv.IsMasterMerge) return false;
                    adv.IdMasterPage = null;
                    Log.Debug("не найдено объявлений подходящих по фильтру");
                    if (!DbManager.AddOrUpdatePage(adv))
                        throw new Exception("Страница adv не сохранена");
                    return true;
                }


                // берем мастера
                var betterPage = pages.FirstOrDefault(x => x.IsMasterMerge) ?? adv;
                for (var i = pages.Count - 1; i >= 0; i--)
                {
                    var page = pages[i];
                    // Для продаж у нас цена не должна отличаться более чем на 7%
                    /*if (page.RentType == RentType.Sell && betterPage.Cost != page.Cost)
                    {
                        var costPercent = betterPage.Cost / 100 ?? 0;
                        var pagePercent = Math.Round(page.Cost / costPercent ?? 0, 2);
                        if (pagePercent < 93 || pagePercent > 107)
                        {
                            Console.WriteLine("DELETE pagePercent = " + pagePercent);
                            pages.RemoveAt(i);
                            // Если объявление было закреплено за нами, а мы его удалям - то переклеим его потом
                            if (!page.IsMasterMerge
                                && (betterPage.IsMasterMerge && betterPage.Id == page.IdMasterPage || adv.Id == page.IdMasterPage)
                                //&& pages.Any(x=>x.Id == page.IdMasterPage) 
                                && page.Id.HasValue)
                            {
                                page.IdMasterPage = null;
                                DbManager.AddOrUpdatePage(page);
                                Log.Debug("Оторвалось объявление: "+page.Id);
                                //remergeList.Add(page.Id.Value);
                            }
                            continue;
                        }
                        else
                        {
                            Console.WriteLine("skip pagePercent = " + pagePercent);
                        }
                    }
                    */
                    if (betterPage.IsAgency)
                    {
                        // Если собственник - то он в приоритете
                        if (!page.IsAgency)
                        {
                            betterPage = page;
                        }
                        // Иначе в приоритете наиболее свежее объявление
                        else if (betterPage.CreateDateTime < page.CreateDateTime)
                        {
                            betterPage = page;
                        }
                    }
                    // Если это 2 объявления от собственника, то лучший вариант - свежий
                    else if (!page.IsAgency && betterPage.CreateDateTime < page.CreateDateTime)
                    {
                        betterPage = page;
                    }
                }

                var remergeList = new List<AbstractAdvertisement>();
                if (adv.RentType == RentType.Sell)
                {
                    // Для продаж у нас цена не должна отличаться более чем на 7%
                    for (var i = pages.Count - 1; i >= 0; i--)
                    {
                        var page = pages[i];
                        if (betterPage.Cost == page.Cost) continue;

                        var costPercent = betterPage.Cost / 100 ?? 0;
                        var pagePercent = 100 - Math.Round(adv.Cost / costPercent ?? 0, 2);
                        if (Math.Abs(pagePercent) > 7)
                        {
                            //Console.WriteLine("DELETE pagePercent = " + pagePercent);
                            pages.RemoveAt(i);
                            // Если объявление было закреплено за нами, а мы его удалям - то переклеим его потом
                            if (page.IdMasterPage.HasValue && page.Id.HasValue
                                //&& betterPage.Id != page.IdMasterPage
                                //&& pages.Any(x=>x.Id == page.IdMasterPage) 
                            )
                            {
                                remergeList.Add(page);
                                Log.Debug("\t\tОторвалось объявление: " + page.Id);
                            }
                        }
                    }
                }

                foreach (var page in pages)
                {
                    var costPercent = betterPage.Cost / 100 ?? 0;
                    var pagePercent = 100 - Math.Round(page.Cost / costPercent ?? 0, 2);
                    if (adv.RentType == RentType.Sell && Math.Abs(pagePercent) > 7)
                    {
                        Log.Error("\t\tОторвалось объявление: page = "+ page.Id+", а пытались приклеить с разрывом в " + pagePercent + "%", true);
                        continue;
                    }
                    page.IdMasterPage = betterPage.Id;
                    if (!DbManager.AddOrUpdatePage(page))
                        throw new Exception("Страница " + page.Id + " не сохранена");
                }
                
                if (adv.Id != betterPage.Id && adv.IdMasterPage != betterPage.Id)
                {
                    var costPercent = betterPage.Cost / 100 ?? 0;
                    var pagePercent = 100-Math.Round(adv.Cost / costPercent ?? 0, 2);
                    if (Math.Abs(pagePercent) > 7)
                    {
                        Log.Debug("\t\tОторвалось объявление: adv, а пытались приклеить с разрывом в "+ pagePercent+"%");
                    }
                    
                    adv.IdMasterPage = betterPage.Id;
                    if (!DbManager.AddOrUpdatePage(adv))
                        throw new Exception("Страница adv не сохранена");
                    
                }
                

                betterPage.IdMasterPage = null;
                if (!DbManager.AddOrUpdatePage(betterPage))
                    throw new Exception("Страница betterPage не сохранена");
                foreach (var page in remergeList)
                {
                    if (!page.IdMasterPage.HasValue) continue;
                    // Если мы были приклеены к кому то из тех кто склеился с другим
                    if (pages.All(x => x.Id != page.IdMasterPage.Value)) continue;
                    //page.IdMasterPage = null;
                    //DbManager.AddOrUpdatePage(page);
                    //Log.Debug("Переклеиваем: " + page.Id);
                    //if (!MergeAdvertisment(page.Id.Value))
                    //  Log.Error("При переклейке оторванных страниц произошла ошибка: первоисточник = " + adv.Id +" оторванец = " + page.Id, true);
                }

                return true;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "Merger::AnalizeAndMerge:: " + adv.SearchIndex);
            }

            return false;
        }

        /// <summary>
        /// "Освободить" склеенных. Сбросить параметр IdMasterPage для связанных объявлений
        /// </summary>
        public static async void UnMasterPage(int id)
        {
            try
            {
                var slavedIdList = new List<int>();
                using (var context = new RieltorServiceDBEntities())
                {
                    foreach (var slavePage in context.Pages.Where(x => x.IdMasterPage == id))
                    {
                        slavedIdList.Add(slavePage.Id);
                        slavePage.IdMasterPage = null;
                    }
                   await context.SaveChangesAsync();
                }

                foreach (var slaveId in slavedIdList)
                {
                    if(MergeAdvertisment(slaveId))
                        break;
                    Log.Debug("не удалось склеить наследника: "+slaveId);
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "[DbMergeManager:UnMasteredPage] "+id);
            }
        }
    }
}

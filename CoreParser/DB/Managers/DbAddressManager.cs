﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Core;
using CoreParser.DB.Models;
using CoreParser.Extension;
using CoreParser.Pages;
using Logger;

namespace CoreParser.DB.Managers
{
    public static class DbAddressManager
    {
        /// <summary>
        /// Рекомендуется использовать GetAddressByGeoSearchString, тк точность поиска по координатам меньше
        /// </summary>
        /// <param name="coord"></param>
        /// <returns></returns>
        public static LocalEntities.Adress GetAddressByCoord(GeoCoordinate coord)
        {
            /*
             *  Тк в базе координаты хранятся в строковом представлении  - мы можем проверять координаты только по точному совпадению
             *  А это очень грустно, тк после 6 знака разговор идет уже о см, естественно ВЕРОЯТНОСТЬ ЕГО РЕЗИСТА КРАЙНЕ МАЛА
             */
            if (coord == null || coord.IsEmpty)
                return null;
            var watch = new Stopwatch();
            watch.Start();
            LocalEntities.Adress result = null;
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))
                {
                    using (var context = new RieltorServiceDBEntities())
                    {
                        var coordinates = coord.ToString();
                        var addresses = context.Adresses.Where(x => x.Geolocation == coordinates).AsEnumerable();

                        // if (addresses.Count() > 1) Log.Warn("Данному запросу соответствует более 1 адреса; запрос: " + coord.ToString());
                        

                        result = addresses.FirstOrDefault()?.ToLocalAdress();
                        transaction.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "[DbAddressManager::GetAddressByCoord]: " + coord);
            }
            finally
            {
                if (result != null)
                    Log.Trace("Поиск '" + coord + "' занял " + Math.Truncate(watch.Elapsed.TotalMilliseconds) + " ms");
            }

            return result;
        }

        /// <summary>
        /// Ищет точное совпадение уже такой geoSearchString, если найдет - вернет адрес
        /// </summary>
        /// <param name="geoSearchString"></param>
        /// <returns></returns>
        public static LocalEntities.Adress GetAddressByGeoSearchString(string geoSearchString)
        {
            if (string.IsNullOrEmpty(geoSearchString))
                return null;
            var watch = new Stopwatch();
            watch.Start();
            LocalEntities.Adress result = null;
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))
                {
                    using (var context = new RieltorServiceDBEntities())
                    {
                        var addresses =
                            context.AddressRelations
                                .Where(x => x.GeoSearchString.ToLower() == geoSearchString.ToLower()).Select(x => x.Adress);

                        if (addresses.Count() > 1)
                        {
                            Log.Error("Данному запросу соответствует " + addresses.Count() + " адреса; запрос: " + geoSearchString, true);
                            transaction.Complete();
                            return null;
                        }
                        

                        result = addresses.FirstOrDefault()?.ToLocalAdress();
                        transaction.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "[DbAddressManager::GetAddressByGeoSearchString]: " + geoSearchString);
            }
            finally
            {
                if (result != null)
                    Log.Trace("Поиск '" + geoSearchString + "' занял " +
                              Math.Truncate(watch.Elapsed.TotalMilliseconds) + " ms");
            }

            return result;
        }

        /// <summary>
        /// Записать (закэшировать) связь GeoSearchString -> Adress
        /// </summary>
        /// <param name="geoSearchString"></param>
        /// <param name="adv"></param>
        public static void AddRelationAddressGeoSearchString(string geoSearchString, AbstractAdvertisement adv)
        {
            if (adv?.Adress == null)
                throw new ArgumentNullException("не указан адрес");
            if (!adv.Adress.Id.HasValue)
            {
                adv.Adress.Id = AddOrUpdate(adv.Adress);
            }
            if (string.IsNullOrEmpty(geoSearchString))
                return;
            var watch = new Stopwatch();
            watch.Start();
            try
            {
                using (var context = new RieltorServiceDBEntities())
                {
                    using (var transaction = context.Database.BeginTransaction())
                    {
                        var idSource = DbObjectManager.GetIdOrAddNewPageSource(adv.Type);
                        var sameRelation = context.AddressRelations.FirstOrDefault(x => x.IdSource == idSource && x.IdAddress == adv.Adress.Id.Value);
                        if (sameRelation != null)
                        {
                            Log.Error(
                                "Дубликат адреса в AddressRelation: пытались добавить '" + geoSearchString +
                                "', когда уже есть '" + sameRelation.GeoSearchString + "' для источника " + adv.Type,
                                true);
                            return;
                        }
                        var relation = new AddressRelation
                        {
                            IdSource = idSource,
                            GeoSearchString = geoSearchString,
                            IdAddress = adv.Adress.Id.Value
                        };
                        context.AddressRelations.AddOrUpdate(relation);
                        context.SaveChanges();
                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "[DbAddressManager::AddRelationAddressGeoSearchString]: "+ geoSearchString);
            }
            finally
            {
                Log.Trace("Добавление '" + geoSearchString + "' заняло " +
                          Math.Truncate(watch.Elapsed.TotalMilliseconds) + " ms");
            }

        }

        public static int AddOrUpdate(LocalEntities.Adress adress)
        {
            if (adress == null)
                throw new ArgumentNullException("не указан адрес");
            var watch = new Stopwatch();
            watch.Start();
            int result = -1;
            try
            {
                using (var context = new RieltorServiceDBEntities())
                {
                    using (var transaction = context.Database.BeginTransaction())
                    {
                        var dbAdress = adress.ToDbAdress();
                        context.Adresses.AddOrUpdate(dbAdress);
                        context.SaveChanges();
                        transaction.Commit();
                        result = dbAdress.Id;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "[DbAddressManager::AddOrUpdate]: " + adress.Street);
            }
            finally
            {
                Log.Trace("Добавление '" + adress.Street + "' заняло " +
                          Math.Truncate(watch.Elapsed.TotalMilliseconds) + " ms");
            }

            return result;
        }
    }
}

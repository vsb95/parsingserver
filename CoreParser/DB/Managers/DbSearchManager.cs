﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Transactions;
using Core;
using Core.Enum;
using CoreParser.DB.LocalEntities;
using CoreParser.DB.Models;
using CoreParser.Enum;
using CoreParser.Extension;
using CoreParser.Pages;
using Logger;
using District = Core.Enteties.District;
using MicroDistrict = Core.Enteties.MicroDistrict;

namespace CoreParser.DB.Managers
{
    public static class DbSearchManager
    {
        /// <summary>
        /// Поиск объявлений
        /// </summary>
        public static List<AbstractAdvertisement> SearchPages(SearchFilter filter, out int totalCountByFilter)
        {
            totalCountByFilter = 0;
            var result = new List<AbstractAdvertisement>();
            try
            {
                if (SettingsManager.IsStartAsAPI)
                    Log.Debug(filter.ToString());
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                    {IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted}))
                {
                    using (var context = new RieltorServiceDBEntities())
                    {
                        var pages = context.Pages.Where(page => (page.IsDeleted == null || !page.IsDeleted.Value)
                                                                && page.Adress.Id_City == filter.IdCity);
                        if (filter.IsOnlyMerged)
                            pages = pages.Where(page => page.IdMasterPage == null);
                        if (filter.RentType != -1)
                        {
                            pages = pages.Where(page => page.RentType == filter.RentType);
                            if (filter.RentType == (int) RentType.Sell && filter.SellTypes != null &&
                                filter.SellTypes.Count != 0)
                                pages = pages.Where(page =>
                                    page.Id_SellType.HasValue && filter.SellTypes.Contains(page.Id_SellType.Value));
                        }

                        if (filter.SizeMin > 0)
                            pages = pages.Where(page => page.Area_Size.HasValue &&
                                                        page.Area_Size >= filter.SizeMin);
                        if (filter.SizeMax > 0)
                            pages = pages.Where(page => page.Area_Size.HasValue &&
                                                        page.Area_Size.Value <= filter.SizeMax);
                        if (filter.CostMin > 0)
                            pages = pages.Where(page => page.Cost.HasValue &&
                                                        page.Cost.Value >= filter.CostMin);
                        if (filter.CostMax > 0)
                            pages = pages.Where(page => page.Cost.HasValue && page.Cost.Value <= filter.CostMax);
                        if (filter.CostType != CostType.Undefined)
                            pages = pages.Where(page => page.Cost_Type.Type == filter.CostType.ToString());
                        if (filter.MinFloor != 0)
                            pages = pages.Where(page => page.Floor.HasValue &&
                                                        page.Floor.Value >= filter.MinFloor);
                        if (filter.MaxFloor != 0)
                            pages = pages.Where(page => page.Floor.HasValue &&
                                                        page.Floor.Value <= filter.MaxFloor);
                        if (filter.HouseMaterials != null && filter.HouseMaterials.Count != 0)
                        {
                            pages = pages.Where(page =>
                                page.Adress.Id_HouseMaterial.HasValue &&
                                filter.HouseMaterials.Contains(page.Adress.Id_HouseMaterial.Value));
                        }

                        if (filter.ObjTypes != null && filter.ObjTypes.Count != 0)
                        {
                            pages = pages.Where(page =>
                                page.Id_Obj_Type.HasValue && filter.ObjTypes.Contains(page.Id_Obj_Type.Value));
                        }

                        // Если указаны районы
                        if (filter.Districts != null && filter.Districts.Count != 0)
                        {
                            // Если есть еще и мкр
                            if (filter.MicroDistricts != null && filter.MicroDistricts.Count != 0)
                            {
                                pages = pages.Where(page =>
                                    page.Adress.Id_MicroDistrict.HasValue &&
                                    filter.MicroDistricts.Contains(page.Adress.Id_MicroDistrict.Value)
                                    || page.Adress.Id_District.HasValue &&
                                    filter.Districts.Contains(page.Adress.Id_District.Value));
                            }
                            // Если только районы
                            else
                            {
                                pages = pages.Where(page =>
                                    page.Adress.Id_District.HasValue &&
                                    filter.Districts.Contains(page.Adress.Id_District.Value));
                            }
                        }
                        // если указаны только мкр
                        else if (filter.MicroDistricts != null && filter.MicroDistricts.Count != 0)
                        {
                            pages = pages.Where(page =>
                                page.Adress.Id_MicroDistrict.HasValue &&
                                filter.MicroDistricts.Contains(page.Adress.Id_MicroDistrict.Value));
                        }


                        if (filter.IsWithFurniture.HasValue)
                            pages = pages.Where(page => page.IsWithFurniture == filter.IsWithFurniture.Value);

                        if (filter.IsOnlyWithPhoto)
                            pages = pages.Where(page => page.Images.Any());

                        if (filter.IsSelfmanNeeded && !filter.IsAgencyNeeded)
                            pages = pages.Where(page => !page.IsAgency);

                        if (!filter.IsSelfmanNeeded && filter.IsAgencyNeeded)
                            pages = pages.Where(page => page.IsAgency);

                        if (filter.IsNotLastFloor)
                            pages = pages.Where(page => page.Floor.HasValue && page.Floor.Value < page.Adress.MaxFloor);
                        
                        if (filter.Sources != null && filter.Sources.Count != 0 || filter.IsMls)
                        {
                            var idMlsn = DbObjectManager.Sources.FirstOrDefault(x => x.Name == ParsingPageType.Mlsn.ToString())
                                ?.Id;
                            if (idMlsn.HasValue)
                            {
                                // Если у нас указан isMls то делаем доп проверку на вкл млсн
                                if (filter.IsMls)
                                {
                                    if (!filter.Sources.Contains(idMlsn.Value))
                                    {
                                        pages = pages.Where(page => filter.Sources.Contains(page.Id_Source)
                                                                    || page.Id_Source == idMlsn.Value && page.IsMls);
                                    }
                                    else
                                    {
                                        pages = pages.Where(page => filter.Sources.Contains(page.Id_Source));
                                    }
                                }
                                else
                                {
                                    pages = filter.Sources.Contains(idMlsn.Value)
                                        ? pages.Where(page => filter.Sources.Contains(page.Id_Source) && !page.IsMls)
                                        : pages.Where(page => filter.Sources.Contains(page.Id_Source));
                                }
                            }
                            else
                            {
                                Log.Fatal("Поиск: не удалось определить id ParsingPageType.Mlsn");
                            }

                        }

                        // исключаем старые объявления (нужно для перезагрузки отчета по фильтру, чтобы не показывать старее)
                        if (filter.DateBegin != DateTime.MinValue)
                            pages = pages.Where(page =>
                                DbFunctions.TruncateTime(page.Date_Create) > filter.DateBegin.Date);
                        if (filter.DateEnd != DateTime.MinValue)
                            pages = pages.Where(
                                page => DbFunctions.TruncateTime(page.Date_Create) < filter.DateEnd.Date);

                        // исключаем некоторые объявления (нужно для перезагрузки отчета по фильтру, чтобы не показывать опять теже самые)
                        if (filter.ExcludeIdList != null && filter.ExcludeIdList.Count != 0)
                        {
                            pages = pages.Where(page => !filter.ExcludeIdList.Contains(page.Id));
                        }

                        if (!string.IsNullOrEmpty(filter.SearchPhrase))
                            pages = pages.Where(page => page.Description.ToLower().Contains(filter.SearchPhrase)
                                                        || page.Adress.Street.ToLower().Contains(filter.SearchPhrase)
                                                        || (page.Adress.District != null && page.Adress.District.Name
                                                                .ToLower().Contains(filter.SearchPhrase))
                                                        || (page.Adress.MicroDistrict != null && page.Adress
                                                                .MicroDistrict
                                                                .Name.ToLower().Contains(filter.SearchPhrase))
                                                        || (page.URL.ToLower().Contains(filter.SearchPhrase)));
                        if (!string.IsNullOrEmpty(filter.SearchAdress))
                        {
                            // Если указана улица, но без дома
                            if (string.IsNullOrEmpty(filter.House) && !string.IsNullOrEmpty(filter.Street))
                            {
                                pages = pages.Where(page => page.Adress.Street.Contains(filter.Street));
                            }
                            else
                            {
                                pages = pages.Where(page => filter.SearchAdress.Contains(page.Adress.Street));
                            }
                        }
                        switch (filter.OrderType)
                        {
                            case 1: // Сначала старые
                                pages = pages.OrderBy(page => page.Date_Publish);
                                break;
                            case 2: // Сначала дешевые
                                pages = pages.OrderBy(page => page.Cost);
                                break;
                            case 3: // Сначала дорогие
                                pages = pages.OrderByDescending(page => page.Cost);
                                break;
                            case 0:
                            default: // Сначала новые
                                pages = pages.OrderByDescending(page => page.Date_Publish);
                                break;
                        }

                        totalCountByFilter = pages.Count();
                        if (filter.IndexPage != 0)
                            pages = pages.Skip(filter.Count * filter.IndexPage);
                        pages = pages.Take(filter.Count);
                        foreach (var page in pages)
                        {
                            if (page == null)
                                continue;
                            var adv = page.ToAbstractAdvertisement();
                            result.Add(adv);
                        }
                        
                        transaction.Complete();
                    }
                }
            }
            catch (AggregateException ae)
            {
                ae.Handle((x) =>
                {
                    Log.Exception(x, LogType.Error, true);
                    return false;
                });
            }
            catch (Exception ex)
            {
                Log.Exception(ex.InnerException ?? ex, LogType.Error, true);
            }

            return result;
        }
        
        #region Public Get Init Search Collections

        public static List<LocalEntities.City> GetCities()
        {
            var result = new List<LocalEntities.City>();
            try
            {
                return DbObjectManager.Cities;
                /*foreach (var dbcity in DbObjectManager.Cities)
                {
                    var city = new LocalEntities.City(dbcity);
                    result.Add(city);
                }*/
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true);
            }

            return result;
        }

        public static List<KeyValue> GetObjTypes()
        {
            var result = new List<KeyValue>();
            try
            {
                foreach (var objType in DbObjectManager.ObjTypes.OrderBy(x => x.Type))
                {
                    if (objType.Type.Contains("Доля")) continue; // пока не нужны

                    var item = new KeyValue(objType.Id, objType.Type.Replace("-комн", "").Replace(" комн", ""));
                    result.Add(item);
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true);
            }

            return result;
        }

        public static List<District> GetDistricts(int idCity)
        {
            var result = new List<District>();
            try
            {
                if (idCity == -1)
                    return DbObjectManager.Districts;
                foreach (var entity in DbObjectManager.Districts.Where(x => x.IdCity == idCity))
                {
                    result.Add(entity);
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true);
            }

            return result;
        }

        public static List<KeyValue> GetSources()
        {
            var result = new List<KeyValue>();
            try
            {
                foreach (var entity in DbObjectManager.Sources)
                {
                    var item = new LocalEntities.Source(entity, out var isVisible);
                    if (isVisible)
                    {
                        var item2 = new KeyValue(entity.Id, entity.Name);
                        result.Add(item2);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true);
            }

            return result.OrderBy(x => x.Value.Length).ToList();
        }

        public static List<MicroDistrict> GetMicroDistricts(int idCity)
        {
            var result = new List<MicroDistrict>();
            try
            {
                if (idCity == -1)
                    return DbObjectManager.MicroDistricts;
                foreach (var entity in DbObjectManager.MicroDistricts.Where(x => x.IdCity == idCity))
                    result.Add(entity);
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true);
            }

            return result;
        }

        public static List<KeyValue> GetSellTypes()
        {
            var result = new List<KeyValue>();
            try
            {
                foreach (var entity in DbObjectManager.SellTypes)
                {
                    var item = new KeyValue(entity.Id, entity.Type);
                    result.Add(item);
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true);
            }

            return result;
        }

        public static List<KeyValue> GetHouseTypes()
        {
            var result = new List<KeyValue>();
            try
            {
                foreach (var entity in DbObjectManager.HouseMaterials)
                {
                    if (entity.Type == EnumHouseMaterial.Undefined || !entity.Id.HasValue)
                        continue;
                    var item = new KeyValue(entity.Id.Value, entity.Name);
                    result.Add(item);
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true);
            }

            return result;
        }

        #endregion

    }
}

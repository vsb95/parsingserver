﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Transactions;
using Core;
using Core.Enum;
using CoreParser.DB.Models;
using CoreParser.Enum;
using CoreParser.Extension;
using CoreParser.Pages;
using Logger;

namespace CoreParser.DB.Managers
{
   public static class DbActualizeManager
    {
        private static TimeSpan? _minTimeToReload;
        /// <summary>
        /// Получить список объявлений без определенного номера телефона
        /// </summary>
        /// <param name="isOnlySelfmanAdv">Только объявления от совственников</param>
        /// <returns></returns>
        public static List<AbstractAdvertisement> GetPagesWithoutPhone(bool isOnlySelfmanAdv = true)
        {

            var watch = new Stopwatch();
            watch.Start();
            var result = new List<AbstractAdvertisement>();
            try
            {
                if (_minTimeToReload == null)
                {
                    _minTimeToReload = new TimeSpan(0, 0, SettingsManager.Settings.ActualizerSettings.ActualizePageEveryMin, 0);
                }

                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (var context = new RieltorServiceDBEntities())
                    {

                        var pages = context.Pages.Where(page => (!page.IsDeleted.HasValue || page.IsDeleted == false) &&
                                                                (string.IsNullOrEmpty(page.PhoneNumber)
                                                                 || page.PhoneNumber == "Undefined"
                                                                 || page.PhoneNumber == "ToDo"
                                                                 || page.PhoneNumber.Contains("X")
                                                                 || !page.PhoneNumber.Contains("7") && !page.PhoneNumber.Contains("8") && !page.PhoneNumber.Contains("9")));
#if !DEBUG
                        if (isOnlySelfmanAdv)
                            pages = pages.Where(page => page.IsAgency == false);
#endif
                        pages = pages.OrderBy(page => page.Date_LastRefresh);
                        Log.Debug("Найдено " + pages.Count() + " объявлений без номера телефона");
                        foreach (var page in pages)
                        {
                            result.Add(page.ToAbstractAdvertisement());
                        }
                        transaction.Complete();
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }
            Log.Debug("GetPagesWithoutPhone sec = " + Math.Round(watch.Elapsed.TotalSeconds, 3));
            return result;
        }

        /// <summary>
        /// Получить список объявлений без мкр\района 
        /// </summary>
        /// <returns></returns>
        public static List<AbstractAdvertisement> GetPagesWithoutDistrict(int idCity, bool isWithoutDistrict,
            bool isWithoutMicroDistrict, int top = 500, List<int> types = null)
        {
            var watch = new Stopwatch();
            watch.Start();
            var result = new List<AbstractAdvertisement>();
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
                {
                    using (var context = new RieltorServiceDBEntities())
                    {

                        var pages = context.Pages.Where(page =>
                            (!page.IsDeleted.HasValue || !page.IsDeleted.Value) && page.Adress != null
                                                                                && (idCity == -1 ||
                                                                                    page.Adress.Id_City == idCity)
                                                                                && (!isWithoutDistrict &&
                                                                                    page.Adress.Id_District.HasValue
                                                                                    || isWithoutDistrict &&
                                                                                    !page.Adress.Id_District.HasValue)
                                                                                && (!isWithoutMicroDistrict &&
                                                                                    page.Adress.Id_MicroDistrict
                                                                                        .HasValue
                                                                                    || isWithoutMicroDistrict &&
                                                                                    !page.Adress.Id_MicroDistrict
                                                                                        .HasValue));
                        if (types != null && types.Count != 0)
                        {
                            pages = pages.Where(page =>
                                page.Id_Obj_Type.HasValue && types.Contains(page.Id_Obj_Type.Value));
                        }

                        Log.Debug(
                            "Найдено " + pages.Count() + " объявлений без привязки к области отдаем первые " + top);
                        pages = pages.Take(top);
                        foreach (var page in pages)
                        {
                            result.Add(page.ToAbstractAdvertisement());
                        }

                        transaction.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

            Log.Debug("GetPagesWithoutDistrict sec = " + Math.Round(watch.Elapsed.TotalSeconds, 3));
            return result;
        }

        public static List<AbstractAdvertisement> GetNotActualPages(out int totalCountNotActualized, int offset = 0,
            int take = 100, ParsingPageType parsingPageType = ParsingPageType.Undefined)
        {
            totalCountNotActualized = 0;
            var watch = new Stopwatch();
            watch.Start();
            var result = new List<AbstractAdvertisement>();
            try
            {
                if (_minTimeToReload == null)
                {
                    _minTimeToReload = new TimeSpan(0, 0, SettingsManager.Settings.ActualizerSettings.ActualizePageEveryMin, 0);
                }

                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
                {
                    using (var context = new RieltorServiceDBEntities())
                    {
                        var pages = context.Pages.Where(page => (page.IsDeleted == null || !page.IsDeleted.Value));

#if DEBUG
                        var minMinutes = 10;
                        pages = pages.Where(page => DbFunctions.DiffMinutes(page.Date_LastRefresh, DateTime.Now) > minMinutes || page.Id_Adress == null || page.Cost == null);
#else
                        var minHours = _minTimeToReload.Value.TotalHours;
                        pages = pages.Where(page => DbFunctions.DiffHours(page.Date_LastRefresh, DateTime.Now) > minHours || page.Id_Adress == null || page.Cost == null);
#endif
                        if (parsingPageType != ParsingPageType.Undefined)
                        {
                            var idType = DbObjectManager.Sources.FirstOrDefault(x => x.Name == parsingPageType.ToString())?.Id;
                            pages = pages.Where(page => page.Id_Source == idType);
                        }

                        pages = pages.Where(page => page.RentType == (int)RentType.Rent || page.RentType == (int)RentType.Sell).OrderBy(page => page.Date_LastRefresh);
                        //Log.Trace(pages.ToString()+"\n"+ minHours);
                        totalCountNotActualized = pages.Count();
                        Log.Debug("Найдено " + totalCountNotActualized + " объявлений на обновление, отступ (в очереди через браузер): " + offset);
                        if (totalCountNotActualized == offset + 1)
                            return result;
                        foreach (var page in pages.Skip(offset).Take(take))
                        {
                            result.Add(page.ToAbstractAdvertisement());
                        }
                        transaction.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

            Log.Debug("GetNotActualPages sec = " + Math.Round(watch.Elapsed.TotalSeconds, 3));
            return result;
        }


        public static void SavePhone(int pageId, string phone)
        {
            try
            {
                var page = DbManager.GetAbstractAdvertisementById(pageId);
                if (page?.Id == null)
                    throw new ArgumentOutOfRangeException(nameof(pageId));
                page.PhoneNumber = phone;
                DbManager.AddOrUpdatePage(page);
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }
        }

        public static void SavePhone(string pageUrl, string phone)
        {
            try
            {
                var page = DbManager.GetAbstractAdvertisementByUrl(pageUrl);
                if (page?.Id == null)
                    throw new ArgumentOutOfRangeException(nameof(pageUrl));
                page.PhoneNumber = phone;
                DbManager.AddOrUpdatePage(page);
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoreParser.Enum;
using Newtonsoft.Json;
using Logger;

namespace CoreParser.DB
{
    [JsonObject(Id = "SearchFilter", MemberSerialization = MemberSerialization.OptOut)]
    public class SearchFilter
    {
        #region Адрес

        public int IdCity { get; set; }
        public string Street { get; set; }
        public string House { get; set; }

        public string SearchAdress { get; set; }

        #endregion

        public double SizeMin { get; set; }
        public double SizeMax { get; set; }

        public int MinFloor { get; set; } = 0;
        public int MaxFloor { get; set; } = 0;

        public decimal CostMin { get; set; } 
        public decimal CostMax { get; set; } 

        public CostType CostType { get; set; } 
        
        public int RentType { get; set; }
        public List<int> SellTypes { get; set; }

        /// <summary>
        /// Список источников
        /// </summary>
        public List<int> Sources { get; set; }

        /// <summary>
        /// млсн мультилистенг?
        /// </summary>
        public bool IsMls { get; set; }

        /// <summary>
        /// Тип объявления(дача\коттедж)
        /// </summary>
        public List<int> ObjTypes { get; set; }

        /// <summary>
        /// Список районов\округов
        /// </summary>
        public List<int> Districts { get; set; } = new List<int>();

        /// <summary>
        /// Список микрорайонов
        /// </summary>
        public List<int> MicroDistricts { get; set; } = new List<int>();

        /// <summary>
        /// Тип дома при продаже (кирпич\панель\ итд)
        /// </summary>
        public List<int> HouseMaterials { get; set; }

        /// <summary>
        /// id-исключения. они не нужны в результатах
        /// </summary>
        public List<int> ExcludeIdList { get; set; }

        /// <summary>
        /// Количество на странице
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// № Страницы
        /// </summary>
        public int IndexPage { get; set; }

        /// <summary>
        /// Тип сортировки
        /// </summary>
        public int OrderType { get; set; } = 0;


        public bool? IsWithFurniture { get; set; }
        public bool IsOnlyWithPhoto { get; set; }

        /// <summary>
        /// Объявления от агентсв\риэлторов
        /// </summary>
        public bool IsAgencyNeeded { get; set; }

        /// <summary>
        /// Объявления от частников
        /// </summary>
        public bool IsSelfmanNeeded { get; set; }
        
        public string SearchPhrase { get; set; }
        public DateTime DateBegin { get; set; }

        /// <summary>
        /// По какое время
        /// Используется пока в дебаге
        /// </summary>
        public DateTime DateEnd { get; set; }

        public bool IsOnlyMerged { get; set; } = true;

        /// <summary>
        ///  не послежний этаж
        /// </summary>
        public bool IsNotLastFloor { get; set; }

        public SearchFilter()
        {

        }

        public override string ToString()
        {
            try
            {
                var json = JsonConvert.SerializeObject(this, Formatting.Indented);
                Log.Debug(json);
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Debug);
            }

            return null;
        }

        public int SerializedValue()
        {
            if (ExcludeIdList != null && ExcludeIdList.Count > 0)
                return GetHashCode();
            var hashCode = IdCity;
            hashCode = (hashCode * 397) ^ (Street?.Length ?? 1);
            hashCode = (hashCode * 397) ^ (House?.Length ?? 1);
            hashCode = (hashCode * 397) ^ (SearchAdress?.Length ?? 1);
            hashCode = (hashCode * 397) ^ (int)SizeMin;
            hashCode = (hashCode * 397) ^ (int)SizeMax;
            hashCode = (hashCode * 397) ^ MinFloor;
            hashCode = (hashCode * 397) ^ MaxFloor;
            hashCode = (hashCode * 397) ^ (int)CostMin;
            hashCode = (hashCode * 397) ^ (int)CostMax;
            hashCode = (hashCode * 397) ^ (int)CostType;
            hashCode = (hashCode * 397) ^ RentType;
            hashCode = (hashCode * 397) ^ (SellTypes?.Sum() ?? 1);
            hashCode = (hashCode * 397) ^ (Sources?.Sum() ?? 1);
            hashCode = (hashCode * 397) ^ (IsMls ? 2 : 1);
            hashCode = (hashCode * 397) ^ (ObjTypes?.Sum() ?? 1);
            hashCode = (hashCode * 397) ^ (Districts?.Sum() ?? 1);
            hashCode = (hashCode * 397) ^ (MicroDistricts?.Sum() ?? 1);
            hashCode = (hashCode * 397) ^ (HouseMaterials?.Sum() ?? 1);
            hashCode = (hashCode * 397) ^ Count;
            hashCode = (hashCode * 397) ^ IndexPage;
            hashCode = (hashCode * 397) ^ OrderType;
            hashCode = (hashCode * 397) ^ (IsWithFurniture.HasValue && IsWithFurniture.Value ? 5 : 1);
            hashCode = (hashCode * 397) ^ (IsOnlyWithPhoto ? 5 : 1);
            hashCode = (hashCode * 397) ^ (IsAgencyNeeded ? 5 : 1);
            hashCode = (hashCode * 397) ^ (IsSelfmanNeeded ?5:1);
            hashCode = (hashCode * 397) ^ (SearchPhrase?.Length ?? 1);
            hashCode = (hashCode * 397) ^ (IsOnlyMerged ? 5 : 1);
            hashCode = (hashCode * 397) ^ (IsNotLastFloor ? 5 : 1);
            return hashCode;
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = IdCity;
                hashCode = (hashCode * 397) ^ (Street != null ? Street.Length : 1);
                hashCode = (hashCode * 397) ^ (House != null ? House.GetHashCode() : 1);
                hashCode = (hashCode * 397) ^ (SearchAdress != null ? SearchAdress.GetHashCode() : 1);
                hashCode = (hashCode * 397) ^ SizeMin.GetHashCode();
                hashCode = (hashCode * 397) ^ SizeMax.GetHashCode();
                hashCode = (hashCode * 397) ^ MinFloor;
                hashCode = (hashCode * 397) ^ MaxFloor;
                hashCode = (hashCode * 397) ^ CostMin.GetHashCode();
                hashCode = (hashCode * 397) ^ CostMax.GetHashCode();
                hashCode = (hashCode * 397) ^ (int) CostType;
                hashCode = (hashCode * 397) ^ RentType;
                hashCode = (hashCode * 397) ^ (SellTypes != null ? SellTypes.GetHashCode() : 1);
                hashCode = (hashCode * 397) ^ (Sources != null ? Sources.GetHashCode() : 1);
                hashCode = (hashCode * 397) ^ IsMls.GetHashCode();
                hashCode = (hashCode * 397) ^ (ObjTypes != null ? ObjTypes.GetHashCode() : 1);
                hashCode = (hashCode * 397) ^ (Districts != null ? Districts.GetHashCode() : 1);
                hashCode = (hashCode * 397) ^ (MicroDistricts != null ? MicroDistricts.GetHashCode() : 1);
                hashCode = (hashCode * 397) ^ (HouseMaterials != null ? HouseMaterials.GetHashCode() : 1);
                hashCode = (hashCode * 397) ^ (ExcludeIdList != null ? ExcludeIdList.GetHashCode() : 1);
                hashCode = (hashCode * 397) ^ Count;
                hashCode = (hashCode * 397) ^ IndexPage;
                hashCode = (hashCode * 397) ^ OrderType;
                hashCode = (hashCode * 397) ^ IsWithFurniture.GetHashCode();
                hashCode = (hashCode * 397) ^ IsOnlyWithPhoto.GetHashCode();
                hashCode = (hashCode * 397) ^ IsAgencyNeeded.GetHashCode();
                hashCode = (hashCode * 397) ^ IsSelfmanNeeded.GetHashCode();
                hashCode = (hashCode * 397) ^ (SearchPhrase != null ? SearchPhrase.GetHashCode() : 1);
                hashCode = (hashCode * 397) ^ IsOnlyMerged.GetHashCode();
                hashCode = (hashCode * 397) ^ IsNotLastFloor.GetHashCode();
                return hashCode;
            }
        }
    }

   
}

﻿using System;

namespace CoreParser.Exceptions
{
    class CaptchaException:System.Exception
    {
        public string ProxyAdress { get; private set; }
        public CaptchaException(Uri proxyAdress = null)
        {
            ProxyAdress = proxyAdress?.ToString();
        }

        public CaptchaException(string message, Uri proxyAdress = null)
            : base(message)
        {
            ProxyAdress = proxyAdress?.ToString();
        }

        public CaptchaException(string message, System.Exception inner, Uri proxyAdress = null)
            : base(message, inner)
        {
            ProxyAdress = proxyAdress?.ToString();
        }
    }
}

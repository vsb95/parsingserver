﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreParser.Exceptions
{
    class CheckActuallyException : System.Exception
    {
        public string PageUrl { get; private set; }
        public CheckActuallyException(Uri pageUrl = null)
        {
            PageUrl = pageUrl?.ToString();
        }

        public CheckActuallyException(string message, Uri pageUrl = null)
            : base(message)
        {
            PageUrl = pageUrl?.ToString();
        }

        public CheckActuallyException(string message, System.Exception inner, Uri pageUrl = null)
            : base(message, inner)
        {
            PageUrl = pageUrl?.ToString();
        }
    }
}

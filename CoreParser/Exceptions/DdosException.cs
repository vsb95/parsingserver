﻿namespace CoreParser.Exceptions
{
    class DdosException:System.Exception
    {
        public DdosException()
        {
        }

        public DdosException(string message)
            : base(message)
        {
        }

        public DdosException(string message, System.Exception inner)
            : base(message, inner)
        {
        }
    }
}

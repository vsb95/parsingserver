﻿using System.Collections.Generic;
using System.Linq;
using CoreParser.DB.LocalEntities;
using Logger;
using Yandex.Geocoder;

namespace CoreParser.Extension
{
    public static class YandexResponseExtention
    {
        public static Adress UpdateLocalAdress(this YandexResponse resp, Adress adress)
        {
            if (adress == null)
                adress = new Adress();
            if (!string.IsNullOrEmpty(resp.DependentLocalityName))
                adress.MicroDistrict = resp.DependentLocalityName;

            switch (resp.Kind)
            {
                case Kind.None:
                    break;
                case Kind.house:
                    if (!string.IsNullOrEmpty(resp.Name))
                        adress.Street = resp.Name;
                    break;
                case Kind.street:
                    if (!string.IsNullOrEmpty(resp.Name))
                        adress.Street = resp.Name;
                    break;
                case Kind.district:
                    if (!string.IsNullOrEmpty(resp.Name) && adress.MicroDistrict != resp.Name)
                        adress.District = resp.Name;
                    break;
                case Kind.locality:
                    break;
                case Kind.entrance:
                    if (!string.IsNullOrEmpty(resp.Name))
                    {
                        var chunks = resp.Name.Split(',');
                        for (var i = 0; i < chunks.Length; i++)
                        {
                            if (!chunks[i].Contains("подъезд ")) continue;
                            adress.Street = string.Join("", chunks.Take(i).ToList());
                            break;
                        }
                    }
                    break;
                case Kind.metro:
                    if (!string.IsNullOrEmpty(resp.Name) && string.IsNullOrEmpty(adress.Street))
                        adress.Street = resp.Name;
                    break;
                default:
                    adress.Street = resp.Name;
                    Logger.Log.Warn("[YandexResponseExtention::UpdateAdress] Неизвестный Kind в YandexResponse: '" +
                                    resp.Kind + "'");
                    break;
            }

            if (!string.IsNullOrEmpty(resp.LocalityName))
            {
                var locality = resp.LocalityName.Replace("садовое некоммерческое товарищество", "СНТ")
                    .Replace("садоводческое некоммерческое товарищество", "СНТ").Replace("садовое товарищество", "СТ");
                var lowerLocalityName = locality.ToLower();
                if (string.IsNullOrEmpty(adress.City))
                    adress.City = locality;
                else if (lowerLocalityName != adress.City.ToLower())
                {
                    bool isFound = false;
                    foreach (var notCityName in LocalityNameNotCity)
                    {
                        if (!lowerLocalityName.Contains(notCityName.ToLower())) continue;
                        adress.ItIsSubCity = true;
                        isFound = true;
                        break;
                    }

                    if (!isFound)
                        Log.Error("Новый объект-поселок: '" + locality + "'", true);
                    adress.Street = locality + ", " + adress.Street;
                }
            }
            return adress;
        }

        private static readonly List<string> LocalityNameNotCity = new List<string>
        {
            "поселок",
            "посёлок",
            "село",
            "деревня",
            "ТЛПХ",
            "садовое некоммерческое товарищество",
            "садовое товарищество",
            "садовые участки",
            "садоводческое некоммерческое товарищество",
            "дачное некоммерческое товарищество",
            "СНТ",
            "СДТ",
            "Тюкалинск",
            "Называевск",
            "Калачинск",
            "Исилькуль",

            "Курган",
            "Киров",
            "Тамбов",
            "СТ "
        };
    }
}

﻿using System.Linq;
using Core;
using CoreParser.DB;
using CoreParser.DB.LocalEntities;

namespace CoreParser.Extension
{
    public static class DbAdressExtension
    {
        /// <summary>
        /// Создать локальный адрес из БД сущности
        /// </summary>
        /// <returns></returns>
        public static Adress ToLocalAdress(this DB.Models.Adress adress)
        {
            var result = new Adress();
            if (adress == null)
                return result;

            result.Id = adress.Id;
            result.IdCity = adress.City?.Id;
            result.City = adress.City?.Name;
            result.Street = adress.Street;
            result.IdDistrict = adress.District?.Id;
            result.District = adress.District?.Name;
            result.IdMicroDistrict = adress.MicroDistrict?.Id;
            result.MicroDistrict = adress.MicroDistrict?.Name;
            result.House = adress.House;
            result.Flat = adress.Flat;
            result.GeoCoordinate = GeoCoordinate.Deserialize(adress.Geolocation);
            result.Region = adress.Region;
            if (adress.Id_HouseMaterial.HasValue)
            {
                result.HouseMaterial = DbObjectManager.HouseMaterials.FirstOrDefault(x=>x.Id == adress.Id_HouseMaterial);
            }
            result.MaxFloor = adress.MaxFloor;

            return result;
        }
    }
}

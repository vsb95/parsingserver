﻿using System;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using Core.Enum;
using CoreParser.DB;
using CoreParser.DB.LocalEntities;
using CoreParser.DB.Models;
using CoreParser.Enum;
using CoreParser.Pages;
using Logger;

namespace CoreParser.Extension
{
    public static class DbPageExtension
    {
        private const int DeadlockErrorNumber = 1205;
        private const int LockingErrorNumber = 1222;
        private const int UpdateConflictErrorNumber = 3960;

        /// <summary>
        /// Создать абстрактную страницу из БД сущности
        /// </summary>
        /// <param name="dbPage"></param>
        /// <returns></returns>
        public static AbstractAdvertisement ToAbstractAdvertisement(this Page dbPage)
        {
            if (dbPage == null)
                throw new ArgumentNullException("dbPage");

            var attemptNumber = 1;
            while (true)
            {
                try
                {
                    //using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions{IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted}))
                    {
                        using (var context = new RieltorServiceDBEntities())
                        {
                            if (!AbstractAdvertisement.TryParse(null, dbPage.URL, out var abstractpage))
                            {
                                var sourceName = dbPage.Source?.Name;
                                if (dbPage.Source == null)
                                {
                                    var source = context.Sources.FirstOrDefault(x => x.Id == dbPage.Id_Source);
                                    if (source == null)
                                        throw new ArgumentException(
                                            "dbPage.Source не определен; idPage = " + dbPage.Id +
                                            "; Id_Source = " + dbPage.Id_Source);
                                    sourceName = source.Name;
                                }

                                if (!System.Enum.TryParse(sourceName, out ParsingPageType pageType))
                                    throw new ArgumentException(
                                        "Неизвестный тип страницы = '" + sourceName + "'");

                                switch (pageType)
                                {
                                    case ParsingPageType.Avito:
                                        abstractpage = new AvitoAdvertisement(dbPage.URL);
                                        break;
                                    case ParsingPageType.Mlsn:
                                        abstractpage = new MlsnAdvertisement(dbPage.URL);
                                        break;
                                    case ParsingPageType.Cian:
                                        abstractpage = new CianAdvertisement(dbPage.URL);
                                        break;
                                    case ParsingPageType.Yandex:
                                        abstractpage = new YandexAdvertisement(dbPage.URL);
                                        break;
                                    case ParsingPageType.N1:
                                        abstractpage = new N1Advertisement(dbPage.URL);
                                        break;
                                    default:
                                        throw new ArgumentOutOfRangeException(
                                            "Не валидный parsingPageType == " + pageType);
                                }
                            }

                            abstractpage.Id = dbPage.Id;
                            abstractpage.CreateDateTime = dbPage.Date_Create;
                            abstractpage.PublishDateTime = dbPage.Date_Publish;
                            abstractpage.LastRefreshDateTime = dbPage.Date_LastRefresh;
                            if (abstractpage.DownloadedImagesPathList == null ||
                                abstractpage.DownloadedImagesPathList.Count == 0)
                            {
                                var images = dbPage.Images.Where(x => x.Id_Page == dbPage.Id)
                                    .Select(image => image.Path).ToList();
                                abstractpage.DownloadedImagesPathList = images;
                            }

                            abstractpage.IsDeleted = dbPage.IsDeleted.HasValue && dbPage.IsDeleted.Value;
                            abstractpage.IsAgency = dbPage.IsAgency;
                            if (dbPage.IsDailyRent && dbPage.RentType == 0)
                                dbPage.RentType = (int) RentType.DailyRent;

                            abstractpage.RentType = (RentType) dbPage.RentType;
                            abstractpage.PhoneNumber = dbPage.PhoneNumber;
                            abstractpage.Cost = dbPage.Cost;
                            abstractpage.Floor = dbPage.Floor;
                            abstractpage.IsWithFurniture = dbPage.IsWithFurniture;
                            abstractpage.IdMasterPage = dbPage.IdMasterPage;
                            abstractpage.Size = dbPage.Area_Size;
                            abstractpage.SearchIndex = dbPage.SearchIndex;
                            abstractpage.IsFromOwnerSection = dbPage.IsFromOwnerSection;
                            abstractpage.Prepay = dbPage.Prepay;
                            abstractpage.IsMls = dbPage.IsMls;
                            abstractpage.IsForceIsAgency = dbPage.IsForceIsAgency;
                            abstractpage.Description = dbPage.Description;
                            if (dbPage.Id_SellType.HasValue)
                            {
                                if (System.Enum.TryParse(dbPage.SellType.Type, out Enum.SellType sellType))
                                {
                                    abstractpage.SellType = sellType;
                                }
                                else
                                {
                                    Log.Fatal("Не удалось определить тип продажи из БД в Enum: " +
                                              dbPage.SellType.Type);
                                }
                            }

                            if (dbPage.Id_Obj_Type != null)
                            {
                                var objType =
                                    DbObjectManager.ObjTypes.FirstOrDefault(x => x.Id == dbPage.Id_Obj_Type);
                                if (objType != null)
                                    abstractpage.ObjType = new ObjType(objType);
                                else
                                {
                                    Log.Fatal("Не удалось определить ObjType, Id: " + dbPage.Id_Obj_Type);
                                }
                            }

                            if (dbPage.Id_Adress != null)
                            {
                                try
                                {
                                    var adress = context.Adresses.FirstOrDefault(x => x.Id == dbPage.Id_Adress);
                                    abstractpage.Adress = adress?.ToLocalAdress();
                                }
                                catch (Exception ex)
                                {
                                    abstractpage.Adress = dbPage.Adress.ToLocalAdress();
                                }
                            }

                            if (dbPage.Id_Cost_Type == null) return abstractpage;

                            var costTypeName = dbPage.Cost_Type;
                                //context.Cost_Type.FirstOrDefault(x => x.Id == dbPage.Id_Cost_Type);
                            if (System.Enum.TryParse(costTypeName?.Type, out CostType costType))
                                abstractpage.CostType = costType;


                            //transaction.Complete();
                            return abstractpage;
                        }
                    }
                }
                catch (DbUpdateException updateEx)
                {
                    if (updateEx.InnerException is SqlException sqlException)
                        if (!sqlException.Errors.Cast<SqlError>().Any(error =>
                            (error.Number == DeadlockErrorNumber) ||
                            (error.Number == LockingErrorNumber) ||
                            (error.Number == UpdateConflictErrorNumber)))
                        {
                            Log.Exception(updateEx, LogType.Error, true);
                            break;
                        }
                        else if (attemptNumber == 4)
                        {
                            Log.Exception(updateEx, LogType.Error, true);
                            break;
                        }
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, LogType.Error, true);
                    if (attemptNumber == 4)
                        break;
                }

                attemptNumber++;
            }

            throw new Exception("Из-за блокировки не удалось достать объявление");
        }
    }
}

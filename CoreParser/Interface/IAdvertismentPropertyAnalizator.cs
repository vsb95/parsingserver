﻿using CoreParser.Enum;
using CoreParser.Pages;

namespace CoreParser.Interface
{
    internal interface IAdvertismentPropertyAnalizator
    {
        PageField PageField { get;}

        /// <summary>
        /// Определисть и установить свойство
        /// </summary>
        /// <param name="page"></param>
        void Identify(AbstractAdvertisement page);
    }
}

﻿using System;
using Core;
using Core.Enum;
using Core.Services;
using CoreParser.Enum;
using CoreParser.Services;
using CoreParser.Services.Downloaders;
using Logger;

namespace CoreParser.Interface
{
    public interface IDownloader
    {
        int TryDownloadCount { get; set; }
        string Download(string url, out string referedUrl, ParsingPageType parsingPageType);
        void SendDelayProxy();
    }

    public static class DownloaderFactory
    {
        /// <summary>
        /// Используется для поочередного запуска браузера через ProxyService или использования Headless-браузера
        /// </summary>
        public static bool Switcher = true;
        public static IDownloader CreateDownloader(ParsingPageType parsingPageType)
        {
            try
            {
                if (SettingsManager.Settings.ServerSettings.ParsingSettings.TryGetValue(parsingPageType, out var config))
                {
                    if (!config.IsDownloadThroughBrowser) return new HttpWebRequestDownloader();
                    Switcher = !Switcher;
                    //if (Switcher) return new WebBrowserDownloader();
                    // return new HeadlessBrowserDownloader();
                    return new WebBrowserDownloader();
                }
            }
            catch (Exception e)
            {
                Log.Exception(e, LogType.Error, true, "DownloaderFactory by ParsingPageType");
            }

            Log.Error("не удалось определить тип наследования IDownloader'a: [" + parsingPageType +"] будет скачан через браузер", true);
            return new WebBrowserDownloader();
        }
    }
}

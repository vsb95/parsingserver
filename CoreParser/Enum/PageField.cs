﻿namespace CoreParser.Enum
{
    public enum PageField
    {
        /// <summary>
        /// Заголовок
        /// </summary>
        Title,

        /// <summary>
        /// Квадратура
        /// </summary>
        Size,

        /// <summary>
        /// Материал дома (панельный\кирпич)
        /// </summary>
        HouseMaterial,

        /// <summary>
        /// Тип объявления (кв\коттедж\вещи и тд)
        /// </summary>
        ObjType,

        /// <summary>
        /// xPath для картинок
        /// </summary>
        XPathImage,

        /// <summary>
        /// Этаж
        /// </summary>
        Floor,

        /// <summary>
        /// N-этажный дом
        /// </summary>
        MaxFloor,
        
        /// <summary>
        /// Адрес
        /// </summary>
        Address,

        /// <summary>
        /// Залог
        /// </summary>
        Prepay,

        /// <summary>
        /// Стоимость
        /// </summary>
        Cost,

        /// <summary>
        /// Тип стоимости (+к.у\счетчики)
        /// </summary>
        CostType,

        /// <summary>
        /// Описание
        /// </summary>
        Description,

        /// <summary>
        /// Дата публикации
        /// </summary>
        DatePublish,

        /// <summary>
        /// Посуточная аренда?
        /// </summary>
        RentType,

        /// <summary>
        /// Номер телефона
        /// </summary>
        PhoneNumber,
        
        /// <summary>
        /// Координаты геометки
        /// </summary>
        GeoMetka,

        /// <summary>
        /// Тип продажи (Новостройка\Вторичка)
        /// </summary>
        SellType,

        /// <summary>
        /// С мебелью?
        /// </summary>
        IsWithFurniture,

        /// <summary>
        /// Проверка на агентство
        /// </summary>
        IsAgency,


    }
}

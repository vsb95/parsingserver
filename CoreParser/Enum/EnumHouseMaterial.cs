﻿namespace CoreParser.Enum
{
    public enum EnumHouseMaterial
    {
        Undefined,


        Кирпичный,
        Панельный,
        Монолитный,
        РубленыйБрусовой,
        КаркасноНасыпной,
        МонолитноКирпичный,
        Прочее,
    }
}

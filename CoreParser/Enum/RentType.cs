﻿namespace CoreParser.Enum
{
    public enum RentType
    {
        Undefined = 0,

        /// <summary>
        /// Продажа
        /// </summary>
        Sell = 1,

        /// <summary>
        /// Длительная аренда
        /// </summary>
        Rent = 2,

        /// <summary>
        /// Посуточная аренда
        /// </summary>
        DailyRent = 3
    }
}

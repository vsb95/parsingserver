﻿namespace CoreParser.Enum
{
    public enum CostType
    {
        Undefined,

        ВсеВключено,

        Счетчики,

        Коммуналка,

        КоммуналкаИСчетчики
    }
}

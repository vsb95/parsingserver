﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Core.Storage;
using CoreParser.DB;
using CoreParser.DB.Managers;
using Logger;

namespace CoreParser.Services
{
    /// <summary>
    /// Класс првязывающий мкр к районам по координатам
    /// </summary>
    public static class CoordAnalizer
    {
        public static void Analize(int cityId)
        {
            var districts = DbSearchManager.GetDistricts(cityId);
            var microDistricts = DbSearchManager.GetMicroDistricts(cityId);
            if (districts.Count == 0)
            {
                Log.Error("Не найдены районы для города "+ cityId);
                return;
            }
            if (microDistricts.Count == 0)
            {
                Log.Error("Не найдены микрорайоны для города " + cityId);
                return;
            }

            foreach (var microDistrict in microDistricts)
            {
                if(microDistrict.IdDistrict.HasValue)
                   continue; 
                var center = RegionsManager.GetCenter(microDistrict.GeoCoordinates);
                if (center == null)
                {
                    Log.Warn("Не найдена центральная координата у мкр id="+ microDistrict.Id);
                    continue;
                }

                var isFound = false;
                foreach (var district in districts)
                {
                    if (!RegionsManager.InPoligon(district.GeoCoordinates, center)) continue;
                    isFound = true;
                    microDistrict.IdDistrict = district.Id;
                    DbObjectManager.UpdateMicroDistrict(microDistrict);
                    break;
                }

                if (!isFound)
                {
                    Log.Error("Не найден район для мкр id=" + microDistrict.Id);
                }
            }
        }
    }
}

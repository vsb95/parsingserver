using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using Core;
using Core.Enum;
using CoreParser.DB;
using CoreParser.DB.Models;
using CoreParser.Enum;
using CoreParser.Extension;
using CoreParser.Pages;
using CoreParser.Pages.Board;
using CoreParser.Services.PropertyAnalizators;
using CoreParser.Services.Proxies;
using Logger;

namespace CoreParser.Services
{
    public static class PageManager
    {
        private static ConcurrentQueue<string> ImageOriginDirectoryQueue = new ConcurrentQueue<string>();

        private static Task DeleteOriginImages = new Task(() =>
        {
            while (true)
            {
                Thread.Sleep(10000);
                string originDirectory = null;
                while (ImageOriginDirectoryQueue.TryDequeue(out originDirectory))
                {
                    try
                    {
                        if (Directory.Exists(originDirectory))
                            Directory.Delete(originDirectory, true);
                        Thread.Sleep(1500);
                    }
                    catch (Exception ex)
                    {
                        Log.Exception(ex, LogType.Warn, false, "");
                    }
                }
            }
        });

        static PageManager()
        {
            DeleteOriginImages.Start();
        }

        public class PageManagerResult
        {
            public ConcurrentBag<string> UnParsedUrList { get; private set; } = new ConcurrentBag<string>();

            public ConcurrentBag<AbstractAdvertisement> UnParsedAdvertisements { get; private set; } =
                new ConcurrentBag<AbstractAdvertisement>();

            public ConcurrentBag<AbstractAdvertisement> Advertisements { get; private set; } =
                new ConcurrentBag<AbstractAdvertisement>();
        }

        /// <summary>
        /// Вернет массив [street, ImagesList]
        /// </summary>
        /// <param name="urls"></param>
        /// <returns></returns>
        public static ConcurrentDictionary<string, List<string>> GetImages(List<string> urls)
        {
            var result = new ConcurrentDictionary<string, List<string>>();
            try
            {
                var listUrl = urls.Where(url => !string.IsNullOrEmpty(url)).ToList();
                var advertisements = GetAdvertisementsByUrl(listUrl, false);

                var tasksList = new List<Task>();
                
                foreach (var page in advertisements.Advertisements)
                {
                    // Если мы уже закачивали фото
                    if (page.DownloadedImagesPathList != null 
                        && page.DownloadedImagesPathList.Count > 0)
                    {
                        var img = page.DownloadedImagesPathList.First();
                        if (!img.Contains("http") && File.Exists(img))
                        {
                            var key = string.IsNullOrEmpty(page.Adress.Street)? "Не определено": page.Adress.Street;
                            int i = 0;
                            while (!result.TryAdd(key, page.DownloadedImagesPathList))
                            {
                                i++;
                                key = (string.IsNullOrEmpty(page.Adress.Street) ? "Не определено" : page.Adress.Street) + "_" + i;
                            }
                        }
                        else
                        {
                            var imgManager = new ImageManager();
                            imgManager.DownloadImages(page);

                            if (page.DownloadedImagesPathList == null && page.DownloadedImagesPathList.Count == 0)
                                continue;

                            var key = string.IsNullOrEmpty(page.Adress.Street) ? "Не определено" : page.Adress.Street;
                            int i = 0;
                            while (!result.TryAdd(key, page.DownloadedImagesPathList))
                            {
                                i++;
                                key = (string.IsNullOrEmpty(page.Adress.Street) ? "Не определено" : page.Adress.Street) + "_" + i;
                            }
                            DbManager.AddOrUpdatePage(page);
                            //DbManager.UpdateImages(page);
                            //var зфпу = DbManager.GetAbstractAdvertisementById(page.Id.Value)?.DownloadedImagesPathList;
                        }
                        continue;
                    }

                    // Скачаем фоточки
                    var task = Task.Factory.StartNew(() =>
                    {
                        try
                        {
                            var imgManager = new ImageManager();
                            if (!page.Download())
                            {
                                Log.Error(
                                    "[PageManager::GetImages] не удалось скачать страницу при загрузке фото: " +
                                    page.Url, true);
                                return;
                            }

                            imgManager.Identify(page); // определим ссылки на фото
                            imgManager.DownloadImages(page);

                            if (page.DownloadedImagesPathList == null && page.DownloadedImagesPathList.Count == 0)
                                return;

                            var key = string.IsNullOrEmpty(page.Adress.Street) ? "Не определено" : page.Adress.Street;
                            int i = 0;
                            while (!result.TryAdd(key, page.DownloadedImagesPathList))
                            {
                                i++;
                                key = (string.IsNullOrEmpty(page.Adress.Street) ? "Не определено" : page.Adress.Street) + "_" + i;
                            }

                            DbManager.AddOrUpdatePage(page); 
                            // DbManager.UpdateImages(page);
                        }
                        catch (Exception e)
                        {
                            Log.Exception(e, LogType.Error, true, "Скачаем фоточки");
                        }
                    });
                    tasksList.Add(task);
                }

                // Теперь скачаем фоточки для объявлений, которые не относятся к недвижке
                foreach (var page in advertisements.UnParsedAdvertisements)
                {
                    var task = Task.Factory.StartNew(() =>
                    {
                        try
                        {
                            var imgManager = new ImageManager();
                            imgManager.Identify(page);
                            imgManager.DownloadImages(page);
                            if (page.DownloadedImagesPathList == null && page.DownloadedImagesPathList.Count == 0)
                                return;
                            var key = string.IsNullOrEmpty(page.Title) ? "Не определено" : page.Title;
                            int i = 0;
                            while (!result.TryAdd(key, page.DownloadedImagesPathList))
                            {
                                i++;
                                key = (string.IsNullOrEmpty(page.Title) ? "Не определено" : page.Title) + "_" + i;
                            }
                        }
                        catch (Exception e)
                        {
                            Log.Exception(e, LogType.Error, true,
                                "Теперь скачаем фоточки для объявлений, которые не относятся к недвижке");
                        }
                    });
                    tasksList.Add(task);
                }

                Task.WaitAll(tasksList.ToArray());

                // Теперь поставим в очередь на удаление оригинальных или непонятных картиночек
                Task.Factory.StartNew(() =>
                {
                    if (!SettingsManager.Settings.ServerSettings.IsSaveOriginImages)
                    {
                        Thread.Sleep(10000);
                        foreach (var adv in advertisements.Advertisements)
                            ImageOriginDirectoryQueue.Enqueue(adv.Directory + "\\Original");
                    }

                    if (advertisements.UnParsedAdvertisements.Count == 0)
                        return;
                    Thread.Sleep(60000);
                    foreach (var adv in advertisements.UnParsedAdvertisements)
                        ImageOriginDirectoryQueue.Enqueue(adv.Directory);
                });
            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

            return result;
        }

        public static PageManagerResult GetAdvertisementsByUrl(IEnumerable<string> urls, bool IsDownloadImages)
        {
            var result = new PageManagerResult();
            try
            {
                // уберем пустые и повторяющиеся ссылки
                var listUrl = urls.Where(url => !string.IsNullOrEmpty(url) && !url.Contains("garazh")).Distinct()
                    .Select(x => x.Replace("//m.avito.ru", "//www.avito.ru").Replace("/yaya/","/omsk/").Trim()).ToList();
                if (listUrl.Count == 0)
                    return result;
                #region Уберем закешированные страницы

                var cachedPages = GetCachedPages(ref listUrl);
                Log.Trace("Закешированных ссылок: " + cachedPages.Count + " осталось: " + listUrl.Count);
                foreach (var cachedPage in cachedPages)
                {
                    if (IsDownloadImages)
                    {
                        var images = GetImages(new List<string> {cachedPage.Url})?.Values.FirstOrDefault();
                        cachedPage.DownloadedImagesPathList = images;
                    }

                    result.Advertisements.Add(cachedPage);
                }

                if (listUrl.Count == 0)
                    return result;
                #endregion

                var maxCountPerIteration = ProxyManager.ProxyCount + 10;
                for (int i = 0; i < listUrl.Count; i += maxCountPerIteration)
                {
                    var links = listUrl.GetRange(i, Math.Min(listUrl.Count - i, maxCountPerIteration));
                    var tasks = new List<Task>();
                    var partResult = new PageManagerResult();
                    foreach (var link in links)
                    {
                        var task = Task.Factory.StartNew(() =>
                        {
                            try
                            {
                                if (string.IsNullOrEmpty(Thread.CurrentThread.Name))
                                    Thread.CurrentThread.Name = "PageManagerSubTask";
                                //var downloader = new HttpWebRequestDownloader();
                                var watch = new Stopwatch();
                                watch.Start();
                                var page = AbstractPage.DownloadPage(link, null);
                                if (page == null)
                                {
                                    result.UnParsedUrList.Add(link);
                                    Log.Warn("Undefined link: " + link);
                                    return; // Неверный урл
                                }

                                switch (page)
                                {
                                    case AbstractAdvertisement abstractAdvertisement:
                                        if (!abstractAdvertisement.IsCached && !abstractAdvertisement.Download() 
                                            || abstractAdvertisement.ObjType == null || abstractAdvertisement.ObjType.Type == EnumObjType.Undefined)
                                        {
                                            result.UnParsedUrList.Add(link);
                                            result.UnParsedAdvertisements.Add(abstractAdvertisement);
                                            Log.Warn("Unparsed link: " + link);
                                            return; // Неверный урл
                                        }
                                        else
                                        {
                                            DbManager.AddOrUpdatePage(abstractAdvertisement);
                                            partResult.Advertisements.Add(abstractAdvertisement);
                                        }

                                        break;
                                    case AbstractBoardPage abstractBoard:
                                        var boardUrls = FormatUrl(abstractBoard.UrlList);
                                        var res = GetAdvertisementsByUrl(boardUrls, IsDownloadImages);
                                        foreach (var advertisement in res.Advertisements)
                                        {
                                            partResult.Advertisements.Add(advertisement);
                                        }

                                        foreach (var url in res.UnParsedUrList)
                                        {
                                            result.UnParsedUrList.Add(url);
                                        }

                                        // если у нас определен раздел доски, то проверим все объявления, которые достали
                                        if (abstractBoard.IsFromOwnerSection.HasValue)
                                        {
                                            var agencyManager = new Agency2Manager();
                                            foreach (var adv in res.Advertisements)
                                            {
                                                // укажем у них разделы только если раздел не указан, 
                                                // или установлен как агентство, а мы хотим установить как частника
                                                if (adv.IsFromOwnerSection != null &&
                                                    (adv.IsFromOwnerSection.Value ||
                                                     !abstractBoard.IsFromOwnerSection.Value))
                                                    continue;
                                                Log.Debug("Обновляем раздел: " + adv.SearchIndex +
                                                          (abstractBoard.IsFromOwnerSection.Value
                                                              ? " частник"
                                                              : " агентство"));
                                                adv.IsFromOwnerSection = abstractBoard.IsFromOwnerSection;
                                                agencyManager.Identify(adv);
                                                DbManager.AddOrUpdatePage(adv);
                                            }
                                        }
                                        break;
                                    default:
                                        throw new Exception("page undefined Type: '" + page.GetType() + "'");
                                }

                                Log.Trace("page DOWNLOAD sec: " + Math.Round(watch.Elapsed.TotalSeconds, 3));

                            }
                            catch (Exception ex)
                            {
                                Log.Exception(ex, LogType.Fatal, true);
                                result.UnParsedUrList.Add(link);
                            }
                        });
                        tasks.Add(task);
                    }

                    Log.Trace("\twaiting tasks... count = " + tasks.Count);
                    Task.WaitAll(tasks.ToArray());
                    if (partResult.Advertisements.Count > 0)
                    {
                        foreach (var advertisement in partResult.Advertisements)
                        {
                            result.Advertisements.Add(advertisement);
                        }
                    }
                }

                if (IsDownloadImages)
                {
                    Log.Trace("\tГрузим фоточки");
                    GetImages(result.Advertisements.Select(x => x.Url).ToList());
                }
            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

            return result;
        }

        public static PageManagerResult GetAdvertisementsByUrl(string url, bool IsDownloadImages)
        {
            return GetAdvertisementsByUrl(new List<string> {url}, IsDownloadImages);
        }
        
        public static PageManagerResult GetAdvertisementsById(IEnumerable<int> idList, bool isCutImages)
        {
            var result = new PageManagerResult();
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))
                {
                    using (var context = new RieltorServiceDBEntities())
                    {
                        var tasksList = new List<Task>();
                        foreach (var id in idList)
                        {
                            var page = context.Pages.FirstOrDefault(x => x.Id == id)?.ToAbstractAdvertisement();
                            if (page == null)
                            {
                                result.UnParsedUrList.Add(id.ToString());
                                continue;
                            }

                            var task = Task.Factory.StartNew(() =>
                            {
                                // нет у нас такой страницы
                                if (page.DownloadedImagesPathList == null || page.DownloadedImagesPathList.Count <= 0)
                                {
                                    result.Advertisements.Add(page);
                                    return;
                                }

                                // фото уже скачаны
                                if (File.Exists(page.DownloadedImagesPathList.First()) || !isCutImages)
                                {
                                    result.Advertisements.Add(page);
                                    return;
                                }

                                var imgManager = new ImageManager();
                                imgManager.DownloadImages(page);

                                result.Advertisements.Add(page);

                                if (page.DownloadedImagesPathList == null && page.DownloadedImagesPathList.Count == 0)
                                    return;

                                DbManager.AddOrUpdatePage(page);
                            });
                            tasksList.Add(task);
                        }

                        Task.WaitAll(tasksList.ToArray());
                        transaction.Complete();
                    }
                }
            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

            return result;
        }

        private static ConcurrentBag<AbstractAdvertisement> GetCachedPages(ref List<string> urls)
        {
            var foundedUrlList = new ConcurrentBag<string>();
            var dbTasks = new List<Task>();
            var concurrentBag = new ConcurrentBag<AbstractAdvertisement>();
            try
            {
                foreach (var url in urls)
                {
                    if (!SettingsManager.IsStartAsAPI)
                    {
                        try
                        {
                            // Проверим, если это сслыка на борду, то нехрен ее искать в БД
                            var pageType = ParsingPageTypeIdentifier.Identify(url);
                            if (CityManager.GeneratedUrlList.TryGetValue(pageType, out var urlList) &&
                                urlList.Contains(url))
                            {
                                continue;
                            }
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            var cachedPage = DbManager.GetAbstractAdvertisementByUrl(url);
                            if (cachedPage != null)
                            {
                                cachedPage.IsDeleted = true;
                                DbManager.AddOrUpdatePage(cachedPage);
                            }

                            continue;
                        }
                    }

                    var task = Task.Factory.StartNew(() =>
                    {
                        try
                        {
                            var cachedPage = DbManager.GetAbstractAdvertisementByUrl(url);
                            if (cachedPage == null) return;
                            if (cachedPage.IsDeleted)
                            {
                                // Перепроверим и если не удалено, то обновим в бд
                                if (cachedPage.CheckActualy() && !cachedPage.IsDeleted)
                                    DbManager.AddOrUpdatePage(cachedPage);
                            }

                            concurrentBag.Add(cachedPage);
                            foundedUrlList.Add(url);
                        }
                        catch (Exception ex)
                        {
                            Log.Exception(ex, LogType.Error, true);
                        }
                    });
                    dbTasks.Add(task);
                }

                Task.WaitAll(dbTasks.ToArray());
                foreach (var foundedUrl in foundedUrlList)
                {
                    urls.Remove(foundedUrl);
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "PageManager:GetCachedPages");
            }
            Log.Trace(string.Format("[PageManager::GetCachedPages] Запросили {0} страниц, отдаем {1}", urls.Count, concurrentBag.Count));
            return concurrentBag;
        }
        
        private static List<string> FormatUrl(List<string> rawUrlList)
        {
            try
            {
                var result = new List<string>();

                foreach (var rawUrl in rawUrlList)
                {
                    var clearUrl = rawUrl.Replace("https:", "http:");
                    var indexOfArg = rawUrl.IndexOf("/?");
                    if (indexOfArg != -1)
                        clearUrl = rawUrl.Substring(0, indexOfArg);
                    clearUrl = clearUrl.Trim();
                    if (!clearUrl.EndsWith("/"))
                        clearUrl += "/";
                    result.Add(clearUrl);
                }

                return result;
            }
            catch (Exception ex)
            {
                Log.Exception(ex,LogType.Error,true, "PageManager::FormatUrl");
            }

            return null;
        }
    }
}

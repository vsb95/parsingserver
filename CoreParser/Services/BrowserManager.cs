﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Core;
using Core.Enteties;
using Core.Enum;
using CoreParser.Exceptions;
using CoreParser.Services.Proxies;
using Logger;
using Newtonsoft.Json;
using TaskExtensions = System.Data.Entity.SqlServer.Utilities.TaskExtensions;

namespace CoreParser.Services
{
    public static class BrowserManager
    {
        private static readonly Random _random = new Random();

        public static string GetNextBrowserAdress(bool? isOnlyPrivate = null, bool? isCanFree = null)
        {
            if (isOnlyPrivate == null)
                isOnlyPrivate = SettingsManager.Settings.ServerSettings.IsParsingServerUsingOnlySharedProxy;
            if (isCanFree == null)
                isCanFree = !SettingsManager.IsStartAsAPI;
            for (int i = 0; !ProxyManager.IsAliveService; i++)
            {
                Thread.Sleep(1000);

                // через 90 сек оповестим
                if (i > 90)
                {
                    i = 0;
                    Log.Fatal("Ожидаю запуска ProxyService по адресу: " +
                              SettingsManager.Settings.ServerSettings.ProxyServiceApiAdress);
                }

                Log.Warn("Ожидаю запуска ProxyService по адресу: " +
                         SettingsManager.Settings.ServerSettings.ProxyServiceApiAdress);
            }

            var url = SettingsManager.Settings.ServerSettings.ProxyServiceApiAdress + "api/browser";
            if (!SettingsManager.IsStartAsAPI)
            {
                if (isOnlyPrivate.Value)
                    url += "?isOnlyPrivate=true";
                else
                    url += "?isOnlyPrivate=false";
                if (isCanFree.Value)
                    url += "&isCanFree=true";
                else
                    url += "&isCanFree=false";
            }

            using (var client = new WebClient())
            {
                var adress = client.DownloadString(url);
                return adress.Replace("\"", "");
            }
        }

        public static string Download(string url, int triedCount = 0)
        {
            var requestUriString = SettingsManager.Settings.ServerSettings.ProxyServiceApiAdress + "api/browser";

            var values = new Dictionary<string, string>
            {
                {"url", url}
            };
            if (!SettingsManager.IsStartAsAPI)
            {
                values.Add("isOnlyPrivate",
                    SettingsManager.Settings.ServerSettings.IsParsingServerUsingOnlySharedProxy.ToString());
                values.Add("isCanFree", (!SettingsManager.IsStartAsAPI).ToString());
            }

            HttpClient client = null;
            FormUrlEncodedContent content = null;
            HttpResponseMessage response = null;
            try
            {
                client = new HttpClient {Timeout = new TimeSpan(0, 0, 250)};
                content = new FormUrlEncodedContent(values);

                var time = new Stopwatch();
                time.Start();

                response = client.PostAsync(requestUriString, content).Result;

                var responseString = response.Content.ReadAsStringAsync().Result;
                Log.Trace("\tBrowserManager::Download at " + Math.Round(time.Elapsed.TotalSeconds, 3) +
                          " sec;\n\tURL = " + url);

                if (string.IsNullOrEmpty(responseString))
                {
                    if (triedCount >= 3)
                        throw new Exception("Не удалось скачать тк браузер/прокси-сервис потухли");
                    triedCount++;
                    return Download(url, triedCount);
                }

                var message = JsonConvert.DeserializeObject<string>(responseString);
                while (message == "<AllIsBusy>")
                {
                    var ms = _random.Next(500, 10000);
                    Log.Info("Все браузеры заняты. Подождем " + ms + "ms");
                    Thread.Sleep(ms);
                    content?.Dispose();
                    response?.Dispose();
                    content = new FormUrlEncodedContent(values);
                    response = client.PostAsync(requestUriString, content).Result;
                    responseString = response.Content.ReadAsStringAsync().Result;
                    message = JsonConvert.DeserializeObject<string>(responseString);
                }

                if (message == "<head></head><body></body>")
                {
                    // не удалось скачать страницу через этот браузер
                    throw new WebException(message, null, WebExceptionStatus.UnknownError, null);
                }

                if (message == "<Timeout>")
                    throw new WebException(message, null, WebExceptionStatus.Timeout, null);
                var result = new BrowserResult(message);

                switch (result.StatusTypeResult)
                {
                    case StatusTypeResult.OK:
                        if (result.Result == "<Captcha>")
                            throw new DdosException("Закапчен браузер на прокси: " + result.Proxy + " для сайта " + url);
                        break;
                    case StatusTypeResult.EMPTY:
                        Log.Warn("WebBrowserDownloader: EMPTY " + result.Status + "\nURL: " + url);
                        break;
                    case StatusTypeResult.ERROR:
                        Log.Error("WebBrowserDownloader: ERROR " + result.Status + "\nURL: " + url, true);
                        switch (result.Status)
                        {
                            case "<Captcha>":
                                throw new DdosException("Закапчен браузер на прокси: " + result.Proxy + " для сайта " + url);
                            case "<404>":
                                throw new WebException(responseString, WebExceptionStatus.RequestCanceled);
                            case "<BadProxy>":
                                throw new WebException(responseString, WebExceptionStatus.RequestCanceled);
                            case "<Timeout>":
                                throw new WebException(responseString, WebExceptionStatus.RequestCanceled);
                            default:
                                throw new WebException("[" + result.Status + "]" + responseString,
                                    WebExceptionStatus.RequestCanceled);
                        }
                }

                return result.Result;
            }
            catch (AggregateException e)
            {
                Log.Exception(e, LogType.Error, true,
                    "Ошибка во время загрузки страницы через браузер");
            }
            finally
            {
                response?.Dispose();
                content?.Dispose();
                client?.Dispose();
            }

            return null;
        }

        private static async Task<string> CreateNewPrivateBrowser()
        {
            var requestUriString = SettingsManager.Settings.ServerSettings.ProxyServiceApiAdress + "api/browser";

            HttpClient client = null;
            HttpResponseMessage response = null;
            try
            {
                client = new HttpClient {Timeout = new TimeSpan(0, 0, 250)};
                response = await client.PutAsync(requestUriString, (FormUrlEncodedContent) null);
                var responseString = await response.Content.ReadAsStringAsync();
                var message = JsonConvert.DeserializeObject<string>(responseString);
                if (string.IsNullOrEmpty(message))
                    throw new Exception("Не удалось создать браузер");
                return message;
            }
            catch (AggregateException e)
            {
                Log.Exception(e.InnerException ?? e, LogType.Error, true,
                    "Ошибка во создания приватного браузера");
            }
            finally
            {
                response?.Dispose();
                client?.Dispose();
            }

            throw new Exception("Не удалось создать браузер");
        }

        public static async Task<List<string>> GetMlsList(int maxIndex)
        {
            var result = new List<string>();
            try
            {
                var browserAdress = await CreateNewPrivateBrowser();
                var time = new Stopwatch();
                time.Start();
                var url = browserAdress + "/api/mls/?maxBoardIndex=" + maxIndex;
                Log.Info("Делаем запрос к браузеру: " + url);

                var request = (HttpWebRequest) WebRequest.Create(url);
                request.Method = "GET";
                request.Timeout = 1 * 60 * 60 * 1000; // 1 час

                string responseStr = null;
                using (var response = (HttpWebResponse) await request.GetResponseAsync())
                {
                    var responseStream = response.GetResponseStream();
                    if (responseStream == null)
                    {
                        throw new NullReferenceException("Сервер не отвечает. Не можем отрыть соединение");
                    }

                    using (var stream = new StreamReader(responseStream))
                    {
                        responseStr = await stream.ReadToEndAsync();
                    }
                }

                if (string.IsNullOrEmpty(responseStr))
                    throw new Exception("Браузер не ответил нам при поиске млс");

                var dResult = new BrowserResult(responseStr);
                switch (dResult.StatusTypeResult)
                {
                    case StatusTypeResult.OK: break;
                    case StatusTypeResult.EMPTY:
                    case StatusTypeResult.ERROR:
                        throw new Exception("WebBrowserDownloader: " + dResult.StatusTypeResult + ": " +
                                            dResult.Status);
                }

                if (string.IsNullOrEmpty(dResult.Result)) return result;
                JsonSerializer _deserializer = new JsonSerializer {NullValueHandling = NullValueHandling.Ignore};

                try
                {
                    using (StringReader sr = new StringReader(dResult.Result))
                    {
                        using (var jsonReader = new JsonTextReader(sr))
                        {
                            _deserializer.Populate(jsonReader, result);
                        }
                    }
                }
                catch (JsonReaderException e)
                {
                    Log.Exception(e, LogType.Fatal, true);
                }

            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "Попытка скачать список объявлений в мультилистинге МЛСН");
            }

            return result;
        }

        [JsonObject(Id = "result", MemberSerialization = MemberSerialization.OptIn)]
        public class BrowserResult : DefaultResult
        {
            [JsonProperty("result")] public string Result { get; set; }

            [JsonProperty("proxy")] public string Proxy { get; set; }

            protected override void Deserialize(string input)
            {
                if (string.IsNullOrEmpty(input)) return;
                JsonSerializer _deserializer = new JsonSerializer {NullValueHandling = NullValueHandling.Ignore};
                try
                {
                    using (StringReader sr = new StringReader(input))
                    {
                        using (var jsonReader = new JsonTextReader(sr))
                        {
                            _deserializer.Populate(jsonReader, this);
                        }
                    }

                }
                catch (JsonReaderException e)
                {
                    Log.Exception(e, LogType.Fatal, true);
                }
            }

            public BrowserResult(string input) : base(input)
            {
            }
        }
    }


}

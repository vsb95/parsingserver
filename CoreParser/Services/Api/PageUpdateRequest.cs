﻿using System.IO;
using System.Text;
using CoreParser.Pages;
using Newtonsoft.Json;

namespace CoreParser.Services.Api
{
    /// <summary>
    /// запрос на обновление объявления
    /// </summary>
    public class PageUpdateRequest
    {
        private static readonly JsonSerializer Deserializer = new JsonSerializer { NullValueHandling = NullValueHandling.Ignore };

        public string Url { get; set; }

        public bool IsDeleted { get; set; }

        /// <summary>
        /// Обязательное поле, если IsDeleted != true
        /// </summary>
        public string Html { get; set; }

        public bool IsAgency { get; set; }

        public bool IsForceIsAgency { get; set; }
        public bool IsMls { get; set; }

        public PageUpdateRequest()
        {
            
        }

        public PageUpdateRequest(string json)
        {
            Deserialize(json);
        }

        public PageUpdateRequest(AbstractAdvertisement adv)
        {
            Url = adv.Url;
            IsDeleted = adv.IsDeleted;
            Html = adv.HtmlPage;
            IsAgency = adv.IsAgency;
            IsForceIsAgency = adv.IsForceIsAgency;
            IsMls = adv.IsMls;
        }

        public string Serialize()
        {
            var builder = new StringBuilder();
            builder.Append("Url=");
            builder.Append(Url);
            builder.Append("&IsDeleted=");
            builder.Append(IsDeleted.ToString());
            if (!IsDeleted && false) // ToDo обрезается html
            {
                builder.Append("&Html=");
                builder.Append(Html);
            }
            builder.Append("&IsAgency=");
            builder.Append(IsAgency.ToString());
            builder.Append("&IsForceIsAgency=");
            builder.Append(IsForceIsAgency.ToString());
            builder.Append("&IsMls=");
            builder.Append(IsMls.ToString());

            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public void Deserialize(string json)
        {
            if (string.IsNullOrEmpty(json)) return;
            using (StringReader sr = new StringReader(json))
            {
                Deserializer.Populate(new JsonTextReader(sr), this);
            }
        }
    }
}

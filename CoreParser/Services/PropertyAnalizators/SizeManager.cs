﻿using System;
using System.Linq;
using Core.Enum;
using CoreParser.Enum;
using CoreParser.Interface;
using CoreParser.Pages;
using Logger;

namespace CoreParser.Services.PropertyAnalizators
{
    internal class SizeManager : IAdvertismentPropertyAnalizator
    {
        public PageField PageField => PageField.Size;


        public void Identify(AbstractAdvertisement page)
        {
            if (!DictionaryStorage.PageParsingSettings.TryGetValue(page.Type, out var selectorDictionary))
                throw new Exception("Неизвестный тип страницы: " + page.Type);
            if (!selectorDictionary.TryGetValue(PageField, out var selector))
                throw new Exception("Не указан селектор [" + PageField + "] для страницы типа: " + page.Type);
            try
            {
                var nodes = page.TrySelectNodes(PageField, selector, out var isShouldContinue);

                if (nodes == null && !isShouldContinue)
                    return;

                var firstNodeInnerHtml = nodes != null && nodes.Count > 0
                    ? nodes[0].InnerHtml.Replace("&nbsp;", " ").Replace("\n", "").Trim()
                    : "";

                string sizeStr = null;
                switch (page.Type)
                {
                    case ParsingPageType.Avito:
                    {
                        if (nodes == null)
                            break;

                        foreach (var node in nodes)
                        {
                            var row = node.InnerText.Replace("&nbsp;", " ").Replace("\n \n ", "\n");
                            if (string.IsNullOrEmpty(row.Trim()))
                                continue;
                            var chunks = row.Trim().Split(':');
                            if (chunks.Length < 2)
                                break; //throw new Exception("Не удалось распознать значение в '" + row + "'");
                            switch (page.ObjType.Type)
                            {
                                case EnumObjType.Dacha:
                                case EnumObjType.Dom:
                                case EnumObjType.Kottedj:
                                    if (!chunks[0].ToLower().Contains("площадь дома")) continue;
                                    break;
                                case EnumObjType.Komnata:
                                    if (!chunks[0].ToLower().Contains("площадь комнаты")) continue;
                                    break;
                                case EnumObjType.Studia:
                                case EnumObjType.Kvartira1:
                                case EnumObjType.Kvartira2:
                                case EnumObjType.Kvartira3:
                                case EnumObjType.Kvartira4:
                                case EnumObjType.Kvartira5Plus:
                                    if (!chunks[0].ToLower().Contains("общая площадь")) continue;
                                    break;
                                case EnumObjType.Commercial:
                                case EnumObjType.Uchastok:
                                    if (!chunks[0].ToLower().Contains("площадь")) continue;
                                    break;
                                default:
                                    throw new ArgumentOutOfRangeException();
                            }

                            var preparedSize = chunks[1].Replace(" м²", "").Trim();
                            sizeStr = preparedSize.Replace(".", ",");
                                break;
                        }
                        break;
                    }
                    case ParsingPageType.Mlsn:
                    {
                        if (nodes != null && nodes.Count > 0)
                        {
                            //1-комнатная квартира, 31 м² ул Ишимская, 26 6 000 ₽/мес.+ КУ (3 000 ₽) + оплата эл. энергии
                            var t = nodes[0].InnerText.Remove(0, nodes[0].InnerText.IndexOf(',') + 2)
                                .Replace(".", ",").Split(' ');
                            if (double.TryParse(t[0], out var item))
                            {
                                sizeStr = t[0];
                            }
                            else if (nodes[0].InnerText.Contains("Комната"))
                            {
                                sizeStr = "0";
                            }
                        }

                        break;
                    }
                    case ParsingPageType.Cian:
                    {
                        if (nodes == null)
                            break;
                        foreach (var node in nodes)
                        {
                            var nd = node.InnerText;
                            if (!nd.Contains("м²") && !nd.Contains("Общая")) continue;
                            sizeStr = nodes[0].InnerText.Replace("м²", "").Replace("Общая", "");
                            break;
                        }

                        break;
                        }
                    case ParsingPageType.Yandex:
                    {
                        if (nodes == null)
                        {
                            nodes = page.TrySelectNodes(PageField, "//meta[@property='og:title']",
                                out isShouldContinue);

                            var title = nodes.FirstOrDefault()?.GetAttributeValue("content", "");
                            var chunks = title?.Trim().Split(' ');
                            if (chunks == null)
                            {
                                Log.Error("Не определена площадь: " + page.Url, true);
                                break;
                            }
                            foreach (var chunk in chunks)
                            {
                                if(!chunk.Contains("м²")) continue;
                                sizeStr = chunk.Replace("&nbsp;м²", "").Trim();
                                break;
                            }
                        }
                        else
                        {
                            foreach (var node in nodes)
                            {
                                var text = node.InnerText;
                                if (!text.Contains("общая площадь")) continue;
                                sizeStr = text
                                    .Split(new string[] { "&nbsp;" }, StringSplitOptions.RemoveEmptyEntries)
                                    .FirstOrDefault()?.Trim();
                                if (text.Contains("соток") && double.TryParse(sizeStr, out var result))
                                {
                                    result *= 100;
                                    page.Size = result;
                                    return;
                                }

                                if (!string.IsNullOrEmpty(sizeStr) && sizeStr.Length > 8)
                                {
                                    sizeStr = sizeStr.Split(' ').FirstOrDefault();
                                }
                                break;
                            }
                        }

                        break;
                    }
                    case ParsingPageType.N1:
                        if (nodes != null && nodes.Count >0)
                        {
                            if (nodes.Count == 1 && nodes[0].InnerText.Contains(" м2"))
                            {
                                var text = nodes[0].LastChild == null || nodes[0].LastChild.Name == "sup" ? nodes[0].InnerText : nodes[0].LastChild.InnerText;
                                sizeStr = text.Replace(" м2", "");
                                break;
                            }

                            foreach (var node in nodes)
                            {
                                if (!node.InnerText.ToLower().Contains("площадь")) continue;
                                var text = node.LastChild == null ? node.InnerText : node.LastChild.InnerText;
                                if (text.Contains("сот"))
                                {
                                    var prepared = text.Replace(" соток", "").Replace(" сотки", "").Replace(" сотка", "")
                                        .Replace(".", ",").Trim();
                                    if (double.TryParse(prepared, out var c))
                                    {
                                        page.Size = c * 100;
                                        return;
                                    }
                                    else
                                    {
                                        Log.Error(text + " -> " + prepared);
                                    }
                                }
                                else if (text.Contains(" м2"))
                                {
                                    sizeStr = text.Replace(" м2", "");
                                }

                                break;
                            }
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                sizeStr = sizeStr?.Replace("м²", "").Trim();
                if (double.TryParse(sizeStr, out var size))
                    page.Size = size;
                else if(page.ObjType.Type != EnumObjType.Komnata)
                    Log.Warn("[" + page.Type + "] Не удалось распознать площадь: " + page.Url);
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка парсинга параметра: " + PageField + " " + page.Url, true);
                Log.Exception(ex, LogType.Error, true);
                page.LogInFile("BadSize", ex.ToString());
            }
        }

    }
}

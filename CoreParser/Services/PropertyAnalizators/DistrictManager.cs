using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Core.Enteties;
using Core.Interface;
using Core.Services;
using Core.Storage;
using CoreParser.DB;
using CoreParser.DB.LocalEntities;
using CoreParser.Enum;
using CoreParser.Interface;
using CoreParser.Pages;
using Logger;

namespace CoreParser.Services.PropertyAnalizators
{
    public class DistrictManager : IAdvertismentPropertyAnalizator, IDistrictManager
    {
        public PageField PageField => PageField.Address;

        public void Identify(AbstractAdvertisement page)
        {
            if (page.Adress.GeoCoordinate == null || page.Adress.GeoCoordinate.IsEmpty)
            {
                if (page.ObjType.Type != EnumObjType.Dacha
                    && page.ObjType.Type != EnumObjType.Uchastok
                    && page.ObjType.Type != EnumObjType.Dom
                    && page.ObjType.Type != EnumObjType.Kottedj)
                {
                    Log.Error(
                        "Координата не указана. Не могу определить район/микрорайон: '" + page.Title+"' "+
                        (string.IsNullOrEmpty(page.Adress.Street)
                            ? string.IsNullOrEmpty(page.Adress.GeoSearchString) 
                                ? null 
                                : "Адреса нет, ное есть GeoSearchString: '" + page.Adress.GeoSearchString + "'\n"
                            : "Адрес есть: '" + page.Adress.Street + "'\n") + page.Url, false);
                    //page.LogInFile("BadGeocoord","Коррдинаты вообще нет");
                }
                return;
            }

            try
            {
                if (TryIdentifyDistrict(page.Adress.GeoCoordinate, out var district))
                {
                    page.Adress.District = district?.Name;
                    if (TryIdentifyMicroDistrict(page.Adress.GeoCoordinate, district, out var microDistrict) 
                        || TryIdentifyNearlyMicroDistrict(page.Adress.GeoCoordinate, district, out microDistrict, 5000))
                        page.Adress.MicroDistrict = microDistrict?.Name;
                }
                // Попробуем перевернуть координаты - яндекс может закосячить ((
                else
                {
                    var reverseCoord = new GeoCoordinate(page.Adress.GeoCoordinate.Lon, page.Adress.GeoCoordinate.Lat);
                    if (TryIdentifyDistrict(reverseCoord, out district))
                    {
                        Log.Warn("Перевернутая координата подошла:" + page.Adress + "\n" + page.Url);
                        page.Adress.GeoCoordinate = reverseCoord;
                        page.Adress.District = district?.Name;
                        if (TryIdentifyMicroDistrict(reverseCoord, district, out var microDistrict)
                            || TryIdentifyNearlyMicroDistrict(page.Adress.GeoCoordinate, district, out microDistrict, 5000))
                            page.Adress.MicroDistrict = microDistrict?.Name;
                    }
                    else
                    {
                        page.Adress.IdDistrict = null;
                        page.Adress.District = null;
                        page.Adress.IdMicroDistrict = null;
                        page.Adress.MicroDistrict = null;
                    }
                }

                // Для области можно не делать проверку. Есть район -  ну хорошо. Нет - ну и хрен с ним
                /* if (!page.Adress.ItIsSubCity && !string.IsNullOrEmpty(page.Adress.Street)
                    && page.ObjType.Type != EnumObjType.Uchastok
                    && page.ObjType.Type != EnumObjType.Dacha)
                {
                    if (page.Adress.District == null)
                        Log.Error("[" + page.ObjType.Type + "] не определен район: " + page.Adress + "\n" + page.Url, false, true);
                    //else if (page.Adress.MicroDistrict == null)
                    //    Log.Error("[" + page.ObjType.Type + "] Район распознан, а МКР нет: " + page.Adress + "\n" + page.Url, false, true);
                }*/
                
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка парсинга параметра: District " + page.Url);
                Log.Exception(ex, LogType.Error, true);
            }
        }

        public bool TryIdentifyDistrictAndMicroDistrict(GeoCoordinate geoCoordinate, out District district,
            out MicroDistrict microDistrict, out bool isNeedReverse)
        {
            isNeedReverse = false;
            district = null;
            microDistrict = null;
            if (TryIdentifyDistrict(geoCoordinate, out var res))
            {
                district = res;
                if (TryIdentifyMicroDistrict(geoCoordinate, district, out var resMicro)
                    || TryIdentifyNearlyMicroDistrict(geoCoordinate, district, out resMicro, 5000))
                    microDistrict = resMicro;
                return true;
            }
            // Попробуем перевернуть координаты - яндекс может закосячить ((
            var reverseCoord = new GeoCoordinate(geoCoordinate.Lon, geoCoordinate.Lat);
            if (TryIdentifyDistrict(reverseCoord, out res))
            {
                isNeedReverse = true;
                district = res;
                if (TryIdentifyMicroDistrict(reverseCoord, district, out var resMicro)
                    || TryIdentifyNearlyMicroDistrict(reverseCoord, district, out resMicro, 5000))
                    microDistrict = resMicro;
                return true;
            }

            return false;
        }

        private bool TryIdentifyDistrict(GeoCoordinate geoCoordinate, out District district)
        {
            district = null;
            foreach (var distr in DbObjectManager.Districts)
            {
                if (!RegionsManager.InPoligon(distr.GeoCoordinates, geoCoordinate)) continue;
                district = distr;
                return true;
            }

            return false;
        }

        private bool TryIdentifyMicroDistrict(GeoCoordinate geoCoordinate, District district,
            out MicroDistrict microDistrict)
        {
            microDistrict = null;
            if (district == null)
                return false;
            foreach (var mDistrict in district.GetMicroDistricts())
            {
                if (!RegionsManager.InPoligon(mDistrict.GeoCoordinates, geoCoordinate)) continue;
                microDistrict = mDistrict;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Определит ближайший мкр в выбранной границе 
        /// </summary>
        /// <param name="geoCoordinate"></param>
        /// <param name="district"></param>
        /// <param name="microDistrict"></param>
        /// <param name="maxRange">Максимальный радиус в метрах</param>
        /// <returns></returns>
        private bool TryIdentifyNearlyMicroDistrict(GeoCoordinate geoCoordinate, District district,
            out MicroDistrict microDistrict, double maxRange = 1000)
        {
            microDistrict = null;
            if (district == null)
                return false;

            var distanceDictionary = new Dictionary<MicroDistrict, double>();
            foreach (var mDistrict in district.GetMicroDistricts())
            {
                var centerMkr = mDistrict.GetCenter();
                distanceDictionary.Add(mDistrict, geoCoordinate.GetDistanceTo(centerMkr));
            }

            var pair = distanceDictionary.OrderBy(x => x.Value).FirstOrDefault();
            if (pair.Value > maxRange)
                return false;

            microDistrict = pair.Key;
            return true;
        }

    }
}

﻿using System;
using Core.Enum;
using CoreParser.Enum;
using CoreParser.Interface;
using CoreParser.Pages;
using Logger;

namespace CoreParser.Services.PropertyAnalizators
{
    internal class DescriptionManager : IAdvertismentPropertyAnalizator
    {
        public PageField PageField => PageField.Description;


        public void Identify(AbstractAdvertisement page)
        {
            if (!DictionaryStorage.PageParsingSettings.TryGetValue(page.Type, out var selectorDictionary))
                throw new Exception("Неизвестный тип страницы: " + page.Type);
            if (!selectorDictionary.TryGetValue(PageField, out var selector))
                throw new Exception("Не указан селектор [" + PageField + "] для страницы типа: " + page.Type);
            try
            {
                var nodes = page.TrySelectNodes(PageField, selector, out var isShouldContinue);

                if (nodes == null && !isShouldContinue)
                    return;

                var firstNodeInnerHtml = nodes != null && nodes.Count > 0
                    ? nodes[0].InnerHtml.Replace("&nbsp;", " ").Replace("\n", "").Replace("&quot;", "\"").Replace("«", "\"").Replace("»", "\"").Trim()
                    : "";

                switch (page.Type)
                {
                    case ParsingPageType.Avito:
                        page.Description = firstNodeInnerHtml;
                        break;
                    case ParsingPageType.Mlsn:
                        if (nodes != null && nodes.Count > 0)
                            page.Description = nodes[0].InnerText.Replace("показать полностью", "").Replace("&quot;", "\"").Replace("«", "\"").Replace("»", "\"");
                        break;
                    case ParsingPageType.Cian:
                        page.Description = firstNodeInnerHtml;
                        break;
                    case ParsingPageType.Yandex:
                        if (nodes != null && nodes.Count > 0)
                            page.Description = nodes[0].InnerText.Replace("&quot;", "\"").Replace("«", "\"").Replace("»", "\"");
                        //page.Description = firstNodeInnerHtml;
                        break;
                    case ParsingPageType.N1:
                        page.Description = firstNodeInnerHtml;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка парсинга параметра: " + PageField + " " + page.Url);
                Log.Exception(ex, LogType.Error, true);
            }
        }

    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Core.Enum;
using CoreParser.Enum;
using CoreParser.Interface;
using CoreParser.Pages;
using Logger;

namespace CoreParser.Services.PropertyAnalizators
{
    internal class AgencyManager : IAdvertismentPropertyAnalizator
    {
        public PageField PageField => PageField.IsAgency;


        public void Identify(AbstractAdvertisement page)
        {
            //if (page.IsAgency) return;
            if (!DictionaryStorage.PageParsingSettings.TryGetValue(page.Type, out var selectorDictionary))
                throw new Exception("Неизвестный тип страницы: " + page.Type);
            if (!selectorDictionary.TryGetValue(PageField, out var selector))
                throw new Exception("Не указан селектор [" + PageField + "] для страницы типа: " + page.Type);
            try
            {
                if(page.IsForceIsAgency)
                    return;
                if (page.RentType == RentType.Sell)
                {
                    var agencyNodes = page.TrySelectNodes(PageField, selector, out var isContinue);

                    if ((agencyNodes == null || agencyNodes.Count == 0) && !isContinue)
                        throw new Exception("Не распознан параметр IsAgency для страницы типа: " + page.Type + "\n\t" +
                                            page.Url);

                    switch (page.Type)
                    {
                        case ParsingPageType.Mlsn:
                        {
                            var subTitle = agencyNodes[0].InnerText.ToLower();
                            if (subTitle.Contains("риэлтор в агентстве")
                                || subTitle.Contains("специалист по недвижимости")
                                || subTitle.Contains("частный риэлтор")
                                || subTitle.Contains("представитель застройщика")
                                || subTitle.Contains("представитель строительной организации")
                                || subTitle.Contains("ипотечный брокер"))
                            {
                                page.IsAgency = true;
                            }
                            // На ключевики в описании не будем проверять. не агентства пойдут через форсирование
                            return;
                        }
                    }
                }
                var goodKeys = page.RentType == RentType.Sell ? SellSelfmanKeywords : RentSelfmanKeywords;
                var badKeys = page.RentType == RentType.Sell ? SellRieltorKeywords : RentRieltorKeywords;
                // Презумпция агентства - это 100% агентство, 
                // если не найдено "хороших" без "плохих" ключевиков

                page.IsAgency = true;
                var description = page.Description?.ToLower()?.Replace("один собственник", "")
                    .Replace("собственник один", "")
                    ?.Replace("1 собственник", "").Replace("собственник 1", "").Replace("омского союза риэлторов ", "");

                if (string.IsNullOrEmpty(description))
                    return;
                description = Regex.Replace(description, @"взросл(ый|ая) собственни(к|ца)", "", RegexOptions.IgnoreCase);
                description = Regex.Replace(description, @"собственн(ая|ость|ости)", "", RegexOptions.IgnoreCase);

                if (page.IsAgency && goodKeys.Any(keyword => description.Contains(keyword)))
                {
#if DEBUG
                    foreach (var selfmanKeyword in goodKeys)
                    {
                        if (description.Contains(selfmanKeyword))
                        {
                            Log.Debug("Собственник определен по ключевику '" + selfmanKeyword + "'; " + page.Url);
                        }
                    }
#endif
                    // Если нашли плохое слово
                    //          или нашли АН без "не беспокоить" и без "не звонить"
                    // и нет "комиссию платить не нужно"
                    // то это агенство
                    if ((badKeys.Any(keyword => description.Contains(keyword)) ||
                         Regex.IsMatch(description, @"(аген(т|)с(т|)во\sнедвижимости|(\,|\.|\s|^)(АН|А\.Н\.))\s", RegexOptions.IgnoreCase) 
                         && !description.Contains("не беспокоить") && !description.Contains("не звонить")) 
                        && !description.Contains("комиссию платить не нужно"))
                    {
#if DEBUG
                        foreach (var badKey in badKeys)
                        {
                            if (description.Contains(badKey))
                            {
                                Log.Debug("Риэтор определен по ключевику '" + badKey + "'; " + page.Url);
                            }
                        }
#endif
                        return;
                    }
                    else
                        page.IsAgency = false;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка парсинга параметра: " + PageField + " " + page.Url);
                Log.Exception(ex, LogType.Error, true);
            }
        }

        public static readonly List<string> RentSelfmanKeywords =
            new List<string>()
            {
                "хозяин",
                "не агенство",
                "не агентство",
                "не компания",
                "не риэлтор",
                "не риелтор",
                "без комис",
                "не посредник",
                "без посредников",
                "без дополнительных оплат",
                "без доп оплат",
                "без доп. оплат",

                "частник",
                "собствен",
                "напрямую",
                "агентствам не беспокоить",
                "агенствам не беспокоить",
                "агентства не беспокоить",
                "агенства не беспокоить"
            };

        public static readonly List<string> RentRieltorKeywords =
            new List<string>()
            {
                "компания \"",
                "компания «",

                "агенство недвижимости \"",
                "агентсво недвижимости \"",
                "агентство недвижимости \"",
                "агенство недвижимости «",
                "агентсво недвижимости «",
                "агентство недвижимости «",
                "по факту заселения",
                "сдает агентство",
                "сдает агенство",
                "сдается через агентство",
                "сдается через агенство",
                "с комис",
                "комиссия",
                "комиссию",
                "комиссионное воз",
                "отличный собствен",
                "отличная собствен",
                "отличные собствен",
                "прекрасная собствен",
                "прекрасный собствен",
                "прекрасные собствен",
                "прекрасный хозя",
                "прекрасная хозя",
                "прекрасные хозя",
                "отличные хозя",
                "отличный хозя",
                "отличная хозя",
                "собственнику",
                "собственику",
                "собственнице",
                "собственице",
                "собственики",
                "собственники",
                "собственность боле",
                "переездом собственников",
                "переезда собственников",
                "собственники переехали",
                "на руки собственнику",
                "на руки собственникам",
            };



        public static readonly List<string> SellRieltorKeywords = new List<string>
        {
            "компания \"",
            "компания «",

            "агенство недвижимости «",
            "агентсво недвижимости «",
            "агентство недвижимости «",
            "агенство недвижимости \"",
            "агентсво недвижимости \"",
            "агентство недвижимости \"",
            "через агентство",
            "через агенство",
            "с комис",
            "комиссия",
            "комиссию", //а если будет у собственника указано комиссию платить не нужно? – желательно тогда такую фразу в исключения добавить, что это собственник.
            "комиссионное воз",
            "АН ",
            "А.Н. ",
            "отличный собствен",
            "отличная собствен",
            "отличные собствен",
            "прекрасная собствен",
            "прекрасный собствен",
            "прекрасные собствен",
            "прекрасный хозя",
            "прекрасная хозя",
            "прекрасные хозя",
            "отличные хозя",
            "отличный хозя",
            "отличная хозя",
            "собственнику",
            "собственику",
            "собственнице",
            "собственице",
            "собственики",
            "собственники",
            "собственность боле",
            "помощь",
            "поможем",
            "окажем содействие",
            "посодействуем",
            "сопровождение",
            " займ ",
            " займ.",
            " займ,",
            "trade-in",
            "tradein",
            "trade in",
            "эксклюзив",
            "эксклюзивный договор",
            "проведем сделку",
            "номер в базе",
            "юридическое",
            "переездом собственников",
            "переезда собственников",
            "собственники переехали",
        };
        
        public static readonly List<string> SellSelfmanKeywords =
            new List<string>()
            {
                "хозяин",
                "хозяйка",
                "не агенство",
                "не агентство",
                "не компания",
                "не риэлтор",
                "не риелтор",
                "без комис",
                "не посредник",
                "без посредников",
                "без дополнительных оплат",
                "без доп оплат",
                "без доп. оплат",
                "частник",
                "собствен",
                "напрямую",
                "агентствам не беспокоить",
                "агенствам не беспокоить",
                "агентства не беспокоить",
                "агенства не беспокоить"
            };

    }
}

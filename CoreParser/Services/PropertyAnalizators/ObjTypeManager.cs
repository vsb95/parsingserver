﻿using System;
using Core.Enum;
using CoreParser.DB.LocalEntities;
using CoreParser.Enum;
using CoreParser.Interface;
using CoreParser.Pages;
using Logger;

namespace CoreParser.Services.PropertyAnalizators
{
    internal class ObjTypeManager : IAdvertismentPropertyAnalizator
    {
        public PageField PageField => PageField.ObjType;


        public void Identify(AbstractAdvertisement page)
        {
            if (!DictionaryStorage.PageParsingSettings.TryGetValue(page.Type, out var selectorDictionary))
                throw new Exception("Неизвестный тип страницы: " + page.Type);
            if (!selectorDictionary.TryGetValue(PageField, out var selector))
                throw new Exception("Не указан селектор [" + PageField + "] для страницы типа: " + page.Type);
            string bread = null;
            try
            {
                var nodes = page.TrySelectNodes(PageField, selector, out var isShouldContinue);

                if (nodes == null && !isShouldContinue)
                    return;

                var firstNodeInnerHtml = nodes != null && nodes.Count > 0
                    ? nodes[0].InnerHtml.Replace("&nbsp;", " ").Replace("\n", "").Trim()
                    : "";


                switch (page.Type)
                {
                    case ParsingPageType.Avito:
                    {
                        for (var i = nodes.Count - 1; i >= 0; i--)
                        {
                            var node = nodes[i]?.InnerText.Replace("&nbsp;","")?.Trim();
                            bread += " > " + node;
                            if (!ObjType.TryParse(node, out var type)) continue;
                            page.ObjType = type;
                            break;
                        }

                        break;
                    }
                    case ParsingPageType.Mlsn:
                    {
                        //nodes = nodes?[0].SelectNodes("//a[@class='Link__base']");
                        if (nodes != null && nodes.Count > 0)
                        {
                            var breadcrumbs = nodes[0].InnerText.Split('/');
                            for (var i = breadcrumbs.Length - 1; i >= 0; i--)
                            {
                                bread += " > " + breadcrumbs[i];
                                if (ObjType.TryParse(breadcrumbs[i], out var type))
                                {
                                    page.ObjType = type;
                                    break;
                                }
                            }
                        }

                        break;
                    }
                    case ParsingPageType.Cian:
                    {
                        if (nodes != null && nodes.Count != 0)
                        {
                            foreach (var node in nodes)
                            {
                                var breadcrumb = node.InnerText?.Replace("\\n","").Trim();
                                bread += " > " + breadcrumb;
                                    if (!ObjType.TryParse(breadcrumb, out var type)) continue;
                                page.ObjType = type;
                                break;
                            }
                        }
                            break;
                    }
                    case ParsingPageType.Yandex:
                    {
                        if (nodes != null && nodes.Count == 1)
                        {
                            bread = nodes[0].InnerText;
                            if (bread.Contains("комнаты в "))
                                bread = "комната";
                            if (ObjType.TryParse(bread, out var type))
                            {
                                page.ObjType = type;
                            }
                        }
                        break;
                    }
                    case ParsingPageType.N1:
                    {
                        if (!string.IsNullOrEmpty(firstNodeInnerHtml))
                        {
                            EnumObjType type = EnumObjType.Undefined;
                            firstNodeInnerHtml = firstNodeInnerHtml.ToLower();
                            if (firstNodeInnerHtml.IndexOf(',') != -1)
                            {
                                firstNodeInnerHtml =
                                    firstNodeInnerHtml.Substring(0, firstNodeInnerHtml.IndexOf(',') + 2);
                            }

                            firstNodeInnerHtml = firstNodeInnerHtml.Replace("продам", "").Replace("сдам", "");

                            bread = firstNodeInnerHtml;

                                if (firstNodeInnerHtml.Contains("1-к"))
                            {
                                type = EnumObjType.Kvartira1;


                                nodes = page.TrySelectNodes(null,
                                    "//span[@class='card-living-content-params-list__value']",
                                    out isShouldContinue);
                                if (nodes != null && nodes.Count > 0)
                                {
                                    foreach (var node in nodes)
                                    {
                                        if (!node.InnerText.Contains("студия")) continue;
                                        type = EnumObjType.Studia;
                                        break;
                                    }
                                }
                            }
                            else if (ObjType.TryParse(firstNodeInnerHtml, out var t))
                            {
                                page.ObjType = t;
                                break;
                            }
                            else if (firstNodeInnerHtml.Contains("дом") || firstNodeInnerHtml.Contains("таунхаус"))
                            {
                                type = EnumObjType.Dom;
                            }
                                else if (firstNodeInnerHtml.Contains("2-к"))
                            {
                                type = EnumObjType.Kvartira2;
                            }
                            else if (firstNodeInnerHtml.Contains("3-к"))
                            {
                                type = EnumObjType.Kvartira3;
                            }
                            else if (firstNodeInnerHtml.Contains("4-к"))
                            {
                                type = EnumObjType.Kvartira4;
                            }
                            else if (firstNodeInnerHtml.Contains("садовый участок") || firstNodeInnerHtml.Contains("дач"))
                            {
                                type = EnumObjType.Dacha;
                            }
                            else if (firstNodeInnerHtml.Contains("коттедж"))
                            {
                                type = EnumObjType.Kottedj;
                            }
                            else if (firstNodeInnerHtml.Contains("комнату"))
                            {
                                type = EnumObjType.Komnata;
                            }
                            else if (firstNodeInnerHtml.Contains("-к"))
                            {
                                type = EnumObjType.Kvartira5Plus;
                            }
                            else if(firstNodeInnerHtml.Contains("участ"))
                            {
                                type = EnumObjType.Uchastok;
                            }
                            else if (firstNodeInnerHtml.Contains("коммерчес"))
                            {
                                type = EnumObjType.Commercial;
                            }
                            else
                            {
                                type = EnumObjType.Commercial;
                            }
                            if(type == EnumObjType.Undefined)
                                break;
                                page.ObjType = new ObjType(type);
                        }

                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "Ошибка парсинга параметра: " + PageField + " " + page.Url);
            }
            if (page.ObjType == null || page.ObjType.Type == EnumObjType.Undefined)
            {
                Log.Fatal(
                    "[" + page.Type + "] Не удалось распознать ObjType: " + page.Url + "\n\tХлебушек: " +
                    (String.IsNullOrEmpty(bread) ? "нету его" : "'" + bread + "'"));
                throw new Exception("Не удалось распознать ссылку: "+page.Url);
            }
        }

    }
}

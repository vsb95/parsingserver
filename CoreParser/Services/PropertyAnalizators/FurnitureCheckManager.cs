﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Enum;
using CoreParser.Enum;
using CoreParser.Interface;
using CoreParser.Pages;
using Logger;

namespace CoreParser.Services.PropertyAnalizators
{
    /// <summary>
    /// Определение свойства "С мебелью"
    /// </summary>
    internal class FurnitureCheckManager : IAdvertismentPropertyAnalizator
    {
        public PageField PageField => PageField.IsWithFurniture;

        public void Identify(AbstractAdvertisement page)
        {
            if (page.RentType == RentType.Sell
                || page.ObjType.Type == EnumObjType.Uchastok)
            {
                page.IsWithFurniture = false;
                return;
            }
            if (!DictionaryStorage.PageParsingSettings.TryGetValue(page.Type, out var selectorDictionary))
                throw new Exception("Неизвестный тип страницы: " + page.Type);
            if (!selectorDictionary.TryGetValue(PageField, out var selector))
                throw new Exception("Не указан селектор [" + PageField + "] для страницы типа: " + page.Type);
            try
            {
                var description = page.Description?.ToLower();
                page.IsWithFurniture = !string.IsNullOrEmpty(description) &&
                                       !KeysList.Any(keyword => description.Contains(keyword));

                var nodes = page.TrySelectNodes(PageField, selector, out var isShouldContinue);
                if (nodes == null && !isShouldContinue)
                    return;
                var firstNodeInnerHtml = nodes != null && nodes.Count > 0
                    ? nodes[0].InnerHtml.Replace("&nbsp;", " ").Replace("\n", "").Trim()
                    : "";

                switch (page.Type)
                {
                    case ParsingPageType.Mlsn:
                        foreach (var node in nodes)
                        {
                            if (!node.ParentNode.InnerText.Contains("Мебель")) continue;
                            switch (node.InnerText)
                            {
                                case "по договоренности":
                                case "частично":
                                case "полностью":
                                    page.IsWithFurniture = true;
                                    break;
                                case "без мебели":
                                case "только кухня":
                                    page.IsWithFurniture = false;
                                    break;
                                default:
                                    throw new Exception("Не определено свойство \"С мебелью\\без\": " + node.InnerText);
                            }

                            break;
                        }

                        break;
                    case ParsingPageType.Cian:
                    {

                        break;
                    }
                    case ParsingPageType.Yandex:
                    {
                        if (nodes != null && nodes.Count == 1)
                        {
                            page.IsWithFurniture = nodes[0].InnerText.Contains("есть");
                        }

                        break;
                    }
                    case ParsingPageType.N1:
                        foreach (var node in nodes)
                        {
                            if (!node.InnerText.Contains("Мебель")) continue;
                            page.IsWithFurniture = node.InnerText.Contains("есть");

                            var t = node.InnerText;

                        }

                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка парсинга параметра: " + PageField + " " + page.Url);
                Log.Exception(ex, LogType.Error, true);
            }
        }

        /// <summary>
        /// Словарь ключевых слов, по которым можно понять, что мебели нет
        /// </summary>
        private static readonly List<string> KeysList = new List<string>
        {
            "пусто",
            "пустая",
            "без мебели",
            "без техники",
            "из мебели только",
            "только кухня"
        };
    }
}

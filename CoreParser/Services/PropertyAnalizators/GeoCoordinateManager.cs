using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Core;
using Core.Enum;
using Core.Extension;
using Core.Services.Geocoding.Sputnik;
using CoreParser.DB.Managers;
using CoreParser.Enum;
using CoreParser.Extension;
using CoreParser.Interface;
using CoreParser.Pages;
using CoreParser.Services.Proxies;
using Logger;
using Yandex.Geocoder;

namespace CoreParser.Services.PropertyAnalizators
{
    internal class GeoCoordinateManager : IAdvertismentPropertyAnalizator
    {
        public PageField PageField => PageField.GeoMetka;

        public void Identify(AbstractAdvertisement page)
        {
            if (!DictionaryStorage.PageParsingSettings.TryGetValue(page.Type, out var selectorDictionary))
                throw new Exception("Неизвестный тип страницы: " + page.Type);
            if (!selectorDictionary.TryGetValue(PageField, out var selector))
                throw new Exception("Не указан селектор [" + PageField + "] для страницы типа: " + page.Type);
            if (page.ObjType.Type == EnumObjType.Dacha)
            {
                if(string.IsNullOrEmpty(page.Adress.Street))
                    page.Adress.Street = page.Adress.GeoSearchString;
                return;
            }
            try
            {
                var nodes = page.TrySelectNodes(PageField, selector, out var isShouldContinue);

                if (nodes == null && !isShouldContinue)
                    return;

                string latStr = null, lonStr = null;
                switch (page.Type)
                {
                    case ParsingPageType.Avito:
                    {
                        if (nodes != null && nodes.Count == 1)
                        {
                            var node = nodes[0];
                            lonStr = node.GetAttributeValue("data-map-lon", "").Replace(".", ",");
                            latStr = node.GetAttributeValue("data-map-lat", "").Replace(".", ",");
                        }

                        break;
                    }
                    case ParsingPageType.Mlsn:
                    {
                        var indexOfLonloat = page.HtmlPage.LastIndexOf("lonLat", StringComparison.Ordinal);
                        if (indexOfLonloat == -1) break;

                        var indexOfOpen = page.HtmlPage.IndexOf("[", indexOfLonloat, StringComparison.Ordinal);
                        if (indexOfOpen == -1) break;

                        var indexOfClose = page.HtmlPage.IndexOf("]", indexOfOpen, StringComparison.Ordinal);
                        if (indexOfClose == -1) break;

                        var coordinates1 = page.HtmlPage.Substring(indexOfOpen + 1, indexOfClose - indexOfOpen - 1);
                        var coordinates = coordinates1?.Split(',');
                        if (coordinates.Length != 2)
                            break;
                        lonStr = coordinates[0].Replace(".", ",");
                        latStr = coordinates[1].Replace(".", ",");
                        break;
                    }
                    case ParsingPageType.Cian:
                    {
                        var location = page.HtmlPage.Split("\"coordinates\":{")?.LastOrDefault();
                        location = location?.Split("},")?.FirstOrDefault();
                        var coordinates = location?.Split(',');
                        if (coordinates != null && coordinates.Length == 2)
                        {
                            var value = coordinates[0].Contains("lng")
                                ? coordinates[0].Replace("\"lng\":", "")
                                : coordinates[1].Replace("\"lng\":", "");
                            lonStr = value.Replace(".", ",").Trim();

                            value = coordinates[0].Contains("lat")
                                ? coordinates[0].Replace("\"lat\":", "")
                                : coordinates[1].Replace("\"lat\":", "");
                            latStr = value.Replace(".", ",").Trim();
                        }

                        break;
                    }
                    case ParsingPageType.Yandex:
                    {
                        string location = null;
                        var split = page.HtmlPage.Split("CardShortcutsItem__bg\" style=\"background-image:url(");
                        if (split.Count == 2)
                        {
                            location = split[1].Substring(0, split[1].IndexOf(">", StringComparison.Ordinal)).Replace("\"");
                            var sp = location.Split("&amp;");
                            foreach (var s in sp)
                            {
                                if(!s.Contains("ll=")) continue;
                                var part = s.Split("%2C");
                                lonStr = part[0].Replace("ll=").Replace(".", ",").Trim();
                                latStr = part[1].Replace(".", ",").Trim();
                                    break;
                            }
                        }
                        if(string.IsNullOrEmpty(lonStr) || string.IsNullOrEmpty(latStr))
                            Log.Warn("Ошибка распознвавния координат у яндекса: '" + location + "'\n " + page.Url);
                        break;
                    }
                    case ParsingPageType.N1:
                    {
                        //page.LogInFile("GeoCoord");
                            var location = page.HtmlPage.Split("\"location\":{")?.LastOrDefault();
                        location = location?.Split("},")?.FirstOrDefault();
                        var coordinates = location?.Split(',');
                        if (coordinates != null && coordinates.Length == 2)
                        {
                            var value = coordinates[0].Contains("lon")
                                ? coordinates[0].Replace("\"lon\":", "")
                                : coordinates[1].Replace("\"lon\":", "");
                            lonStr = value.Replace(".", ",").Trim();

                            value = coordinates[0].Contains("lat")
                                ? coordinates[0].Replace("\"lat\":", "")
                                : coordinates[1].Replace("\"lat\":", "");
                            latStr = value.Replace(".", ",").Trim();
                        }

                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                var coordinate = new GeoCoordinate();
                if (double.TryParse(lonStr, out var lon) && double.TryParse(latStr, out var lat))
                {
                    coordinate.Lon = lon;
                    coordinate.Lat = lat;
                }

                // Если у нас уже есть геолокация, то значит проверяли
                if (page.Adress.GeoCoordinate != null && !page.Adress.GeoCoordinate.IsEmpty
                    && coordinate.IsEqualsPlace(page.Adress.GeoCoordinate)
                    || TryFindAdressByCoord(page, GeoCodeType.GeoSearchString) && page.Adress.GeoCoordinate != null)
                {
                    return;
                }

                if (coordinate.IsEmpty || IsCenterCity(coordinate))
                {
                    page.Adress.Street = page.Adress.GeoSearchString;
                    page.Adress.GeoCoordinate = null;
                    return;
                }

                page.Adress.GeoCoordinate = coordinate;

                if (!TryFindAdressByCoord(page, GeoCodeType.GeoCoordinate))
                {
                    page.Adress.GeoCoordinate = null;
                    if (page.ObjType.Type != EnumObjType.Dacha
                        && page.ObjType.Type != EnumObjType.Dom
                        && page.ObjType.Type != EnumObjType.Uchastok
                        && page.ObjType.Type != EnumObjType.Kottedj)
                        Log.Warn("[" + page.Type + "] [" + page.ObjType.Type + "] Не удалось определить GeoMetka: " + page.Url);
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true,
                    "[" + page.Type + "] [" + page.ObjType.Type + "] Ошибка парсинга параметра: " + PageField + " " + page.Url + "\nGeoSearchString: '" +
                    page.Adress.GeoSearchString + "'\nGeoCoordinate: '" + page.Adress.GeoCoordinate?.ToString() + "'");
                page.Adress.GeoCoordinate = null;
            }
            if(string.IsNullOrEmpty(page.Adress.Street))
                page.Adress.Street = page.Adress.GeoSearchString?.Replace(",", "")?.Trim();
        }
        
        private bool TryFindAdressByCoord(AbstractAdvertisement page, GeoCodeType type)
        {
            try
            {
                if (string.IsNullOrEmpty(page.Adress.City))
                    page.Adress.City = CityManager.GetCityNameByUrl(page.Url);
                string geocode = null;
                switch (type)
                {
                    case GeoCodeType.GeoCoordinate:
                        if (page.Adress.GeoCoordinate == null || page.Adress.GeoCoordinate.IsEmpty)
                        {
                            if (string.IsNullOrEmpty(page.Adress.GeoSearchString))
                                return false;
                                // ну принудительн опопробуем определить по улице
                            geocode = string.IsNullOrEmpty(page.Adress.City) || page.Adress.GeoSearchString.ToLower().Contains(page.Adress.City.ToLower())
                                ? page.Adress.GeoSearchString
                                : page.Adress.City + ", " + page.Adress.GeoSearchString;
                        }
                        else
                        {
                            geocode = page.Adress.GeoCoordinate?.ToString();
                        }
                        break;
                    case GeoCodeType.GeoSearchString:
                        if (string.IsNullOrEmpty(page.Adress.GeoSearchString))
                        {
                            if (!string.IsNullOrEmpty(page.Adress.City)
                                && !string.IsNullOrEmpty(page.Adress.Street))
                            {
                                page.Adress.GeoSearchString = page.Adress.City + ", " + page.Adress.Street;
                            }
                            else
                            {
                                if (page.ObjType.Type != EnumObjType.Dacha
                                    && page.ObjType.Type != EnumObjType.Dom
                                    && page.ObjType.Type != EnumObjType.Uchastok
                                    && page.ObjType.Type != EnumObjType.Kottedj)
                                    Log.Warn(
                                        "[TryFindAdressByStreet] [" + page.Type + "]: GeoSearchString не указан: " +
                                        page.Url);
                                return false;
                            }
                        }

                        page.Adress.GeoSearchString = string.IsNullOrEmpty(page.Adress.City)|| page.Adress.GeoSearchString.ToLower().Contains(page.Adress.City.ToLower())
                            ? page.Adress.GeoSearchString
                            : page.Adress.City + ", " + page.Adress.GeoSearchString;

                        // Если нет номера дома, то я даже не буду искать (если есть координата)
                        var chunks = page.Adress.GeoSearchString.Split(",");
                        if (string.IsNullOrEmpty(chunks?.LastOrDefault()?.FindDigit()))
                        {
                            var isHasCoord = page.Adress.GeoCoordinate != null && !page.Adress.GeoCoordinate.IsEmpty;
                            if (page.ObjType.Type != EnumObjType.Dacha
                                && page.ObjType.Type != EnumObjType.Dom
                                && page.ObjType.Type != EnumObjType.Uchastok
                                && page.ObjType.Type != EnumObjType.Kottedj)
                                Log.Warn("[TryFindAdressByCoord] [" + page.ObjType.Type +
                                         "] нет номера дома GeoSearchString: '" + page.Adress.GeoSearchString +
                                         "'\nURL: " + page.Url);
                            if (isHasCoord)
                                return false;
                        }

                        geocode = page.Adress.GeoSearchString;

                        if (string.IsNullOrEmpty(geocode))
                            geocode = page.Adress.GeoCoordinate?.ToString();

                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(type));
                }

                if (string.IsNullOrEmpty(geocode))
                    return false;

                if (IsCenterCity(page.Adress.GeoCoordinate) && type == GeoCodeType.GeoCoordinate)
                {
                    page.Adress.Street = page.Adress.GeoSearchString;
                    page.Adress.GeoCoordinate = null;
                    return true;
                }

                var cachedAddress = type == GeoCodeType.GeoSearchString
                    ?
                    DbAddressManager.GetAddressByGeoSearchString(geocode)
                    : type == GeoCodeType.GeoCoordinate
                        ? DbAddressManager.GetAddressByCoord(page.Adress.GeoCoordinate)
                        : throw new ArgumentOutOfRangeException("Неизвестный тип определения адреса");
                if (cachedAddress != null)
                {
                    Log.Trace("Определен закешированный адрес: '" + geocode + "' по " + type);
                    if (IsCenterCity(cachedAddress.GeoCoordinate))
                    {
                        page.Adress.Street = page.Adress.GeoSearchString;
                        page.Adress.GeoCoordinate = null;
                        return true;
                    }

                    page.Adress = cachedAddress;
                    return true;
                }

                Thread.Sleep(500);
                
                SputnikResponse response = null;
                if (type == GeoCodeType.GeoCoordinate)
                {
                    if (page.Adress?.GeoCoordinate == null)
                        return false;
                    response = SputnikGeocoder.FindAddress(page.Adress.GeoCoordinate.Lat, page.Adress.GeoCoordinate.Lon, ProxyManager.GetProxy(ParsingPageType.Undefined)?.ToWebProxy());
                }
                else
                {
                    response = SputnikGeocoder.FindCoord(geocode, ProxyManager.GetProxy(ParsingPageType.Undefined)?.ToWebProxy());
                }

                if (!string.IsNullOrEmpty(response?.City) && !string.IsNullOrEmpty(response.Street))
                {
                    // Проблема с дачами - вырезается СНТ
                    var house = response.House ?? page.Adress.GeoSearchString?.Replace(response.City).Replace(response.Street).Trim().Split(',').LastOrDefault();
                    //var p3 = p2?.Split(' ').LastOrDefault()?.Trim();
                    page.Adress.City = CityManager.GetCityNameByUrl(page.Url) ?? response.City;
                    page.Adress.Street = response.Street;
                    page.Adress.House = house?.Trim();
                    if (!string.IsNullOrEmpty(page.Adress.House) && page.Adress.House.Length > 10)
                    {
                        Log.Error("Error house: Too long; "+ page.Url
                                + "\nCutted house: " + page.Adress.House 
                                + "\nResponse house: " + response.House 
                                + "\nGeoSeaarchString house: " + page.Adress.GeoSearchString?.Replace(response.City).Replace(response.Street).Trim().Split(',').LastOrDefault()
                                + "\nGeoSeaarchString: " + page.Adress.GeoSearchString, true);
                        page.Adress.House = null;
                    }
                    page.Adress.GeoCoordinate = response.Coordinates?.FirstOrDefault();

                    if (type == GeoCodeType.GeoSearchString)
                        DbAddressManager.AddRelationAddressGeoSearchString(geocode, page);
                    return true;
                }
                return false;
                var resp = YandexGeocoder.FindAdress(geocode, Kind.house, ProxyManager.GetProxy(ParsingPageType.Yandex, false)?.ToWebProxy());
                resp?.UpdateLocalAdress(page.Adress);
                if (resp?.Point != null 
                    && (type != GeoCodeType.GeoCoordinate || page.Adress.GeoCoordinate == null || page.Adress.GeoCoordinate.IsEmpty))
                {
                    var point = new GeoCoordinate(resp.Point.Latitude, resp.Point.Longitude);
                    page.Adress.GeoCoordinate = point;
                    if (type == GeoCodeType.GeoSearchString)
                        DbAddressManager.AddRelationAddressGeoSearchString(geocode, page);
                }
                // ToDo Есть какая то бага с тем, что адреса кешируются не правильно
                // Пример - разные названия с одинаковой координатой + привязаны к одному адресу. И получается вааще не верно
                // пока что решается удалением лишних связей адресов
                /*
                DELETE FROM [dbo].[AddressRelation]
                      WHERE IdAddress In (SELECT IdAddress
		                FROM            dbo.AddressRelation
		                GROUP BY IdAddress
		                HAVING        (COUNT(Id) > 2)
		                ) 
                 
                DbAddressManager.AddRelationAddressGeoSearchString(geocode, page);
                 */

                if (IsCenterCity(page.Adress.GeoCoordinate))
                {
                    page.Adress.Street = page.Adress.GeoSearchString;
                    page.Adress.GeoCoordinate = null;
                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "TryFindAdressByCoord:: "+page.Url);
            }

            return page.Adress.GeoCoordinate != null && !page.Adress.GeoCoordinate.IsEmpty;
        }

        private enum GeoCodeType
        {
            GeoSearchString, GeoCoordinate
        }

        /// <summary>
        /// Если у нас эта координата является центром города - значит это объявление без адреса 
        /// </summary>
        /// <returns></returns>
        private static bool IsCenterCity(GeoCoordinate coordinate)
        {
            if (coordinate == null || coordinate.IsEmpty)
                return false;
            foreach (var centerCityCoordinate in CenterCityCoordinates)
            {
                if (!coordinate.IsEqualsPlace(centerCityCoordinate)) continue;
                Log.Debug("Указана координата в центре города");
                return true;
            }

            return false;

        }

        /// <summary>
        /// Список координат, которые без адреса
        /// </summary>
        private static List<GeoCoordinate> CenterCityCoordinates = new List<GeoCoordinate>()
        {
            // Омск
            new GeoCoordinate(54.989342, 73.368212), // avito
            //new GeoCoordinate(),


        };
    }
}

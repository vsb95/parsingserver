﻿using System;
using Core.Enum;
using CoreParser.Enum;
using CoreParser.Interface;
using CoreParser.Pages;
using CoreParser.Pages.Board;
using Logger;

namespace CoreParser.Services.PropertyAnalizators
{
    internal class IsDeletedManager : IAdvertismentPropertyAnalizator
    {
        public PageField PageField => PageField.IsAgency;


        public void Identify(AbstractAdvertisement page)
        {
            //if (page.IsDeleted) return;
            try
            {
                page.IsDeleted = false;
                // Если нас перебросило на доску - то объявление удалили
                if (AbstractBoardPage.TryParse(page.HtmlPage, page.Url, out var board))
                {
                    Log.Trace("Перебросили на доску - объявление удалено");
                    page.IsDeleted = true;
                    return;
                }

                switch (page.Type)
                {
                    case ParsingPageType.Avito:
                    {
                        if (!string.IsNullOrEmpty(page.Title) &&
                            page.Title.Contains("Объявление отклонено администрацией сайта")
                            || page.HtmlPage.Contains("По вашему запросу ничего не найдено")
                            || page.HtmlPage.Contains("Попробуйте сократить или задать запрос по-другому.")
                            || page.HtmlPage.Contains("Это объявление закрыто владельцем")
                            || page.HtmlPage.Contains("Срок размещения этого объявления истёк")
                            || page.HtmlPage.Contains("Ой! Такой страницы на нашем сайте нет")
                            || page.HtmlPage.Contains("Наверное, вы ошиблись при наборе адреса или перешли по неверной ссылке")
                            || page.HtmlPage.Length < 50 && page.HtmlPage.Contains("Ошибка ")) // тут какой то хэш
                            page.IsDeleted = true;
                        break;
                    }
                    case ParsingPageType.Mlsn:
                    {
                        if (string.IsNullOrEmpty(page.HtmlPage))
                            throw new Exception("не удается распознать IsDeleted тк старница не скачана: " + page.Url);
                        // Проверка - не помещено ли объявление в архив
                        page.IsDeleted =  page.HtmlPage.Contains("Объявление находится в архиве. Контакты продавца скрыты");
                        break;
                    }
                    case ParsingPageType.Cian:
                    {
                        if (!string.IsNullOrEmpty(page.Title) &&
                            page.HtmlPage.Contains("Контакты скрыты, объявление находится в архиве"))
                            page.IsDeleted = true;

                        break;
                    }
                    case ParsingPageType.Yandex:
                    {
                        if (!string.IsNullOrEmpty(page.HtmlPage) 
                            && (page.HtmlPage.Contains("Контакты скрыты")
                                || page.HtmlPage.Contains("Объявление устарело<")
                                || page.HtmlPage.Contains("Страница не найдена<")
                                || page.HtmlPage.Contains(">Страница не найдена")))
                            page.IsDeleted = true;
                        break;
                    }
                    case ParsingPageType.N1:
                    {
                        if (!string.IsNullOrEmpty(page.Title) && page.Title.Contains("Объявление отклонено администрацией сайта")
                            || page.HtmlPage.Contains("Контакты скрыты, объявление находится в архиве"))
                            page.IsDeleted = true;
                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка парсинга параметра: IsDeleted " + page.Url);
                Log.Exception(ex, LogType.Error, true);
            }
        }

    }
}

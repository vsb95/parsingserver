﻿using System;
using Core.Enum;
using CoreParser.Enum;
using CoreParser.Interface;
using CoreParser.Pages;
using Logger;

namespace CoreParser.Services.PropertyAnalizators
{
    internal class CostTypeManager : IAdvertismentPropertyAnalizator
    {
        public PageField PageField => PageField.CostType;


        public void Identify(AbstractAdvertisement page)
        {
            if(page.RentType != RentType.Rent
               || page.ObjType.Type == EnumObjType.Uchastok
               || page.ObjType.Type == EnumObjType.Dom
               || page.ObjType.Type == EnumObjType.Dacha
               || page.ObjType.Type == EnumObjType.Commercial)
                return;
            if (!DictionaryStorage.PageParsingSettings.TryGetValue(page.Type, out var selectorDictionary))
                throw new Exception("Неизвестный тип страницы: " + page.Type);
            if (!selectorDictionary.TryGetValue(PageField, out var selector))
                throw new Exception("Не указан селектор [" + PageField + "] для страницы типа: " + page.Type);
            try
            {
                var nodes = page.TrySelectNodes(PageField, selector, out var isShouldContinue);

                if (nodes == null && !isShouldContinue)
                    return;

                var firstNodeInnerHtml = nodes != null && nodes.Count > 0
                    ? nodes[0].InnerHtml.Replace("&nbsp;", " ").Replace("\n", "").Trim()
                    : "";


                switch (page.Type)
                {
                    case ParsingPageType.Avito:
                        page.CostType = GetCostType(page.Description);
                        break;
                    case ParsingPageType.Mlsn:
                    {
                        if (nodes != null && nodes.Count > 11)
                        {
                            //1-комнатная квартира, 31 м² ул Ишимская, 26 6 000 ₽/мес.+ КУ (3 000 ₽) + оплата эл. энергии
                            var preparedData = nodes[10].InnerText;
                            var indexofPlus = preparedData.IndexOf('+');
                            if (indexofPlus != -1)
                            {
                                preparedData = nodes[10].InnerText.Remove(0, indexofPlus);
                            }

                            if (preparedData.Contains("+ КУ"))
                            {
                                preparedData = preparedData.Replace("+ КУ", "+ К/У");
                            }

                            page.CostType = GetCostType(preparedData);
                        }

                        break;
                    }
                    case ParsingPageType.Cian:
                    {
                        if (nodes != null && nodes.Count != 0)
                        {
                            var preparedData = nodes[0].InnerText.Replace("₽", "");
                            var chunks = preparedData.Split('+');
                            // ToDo неправильно определяется CostType
                            //CostType = GetCostType(chunks.Length > 1 ? chunks[1] : chunks[0]);
                        }
                            break;
                    }
                    case ParsingPageType.Yandex:
                    {
                        if (!string.IsNullOrEmpty(firstNodeInnerHtml))
                        {
                            if (firstNodeInnerHtml.Contains("КУ не включены"))
                            {
                                page.CostType = CostType.Коммуналка;
                            }
                            else if (firstNodeInnerHtml.Contains("КУ включены"))
                            {
                                page.CostType = CostType.ВсеВключено;
                            }
                            else 
                            {
                                page.CostType = GetCostType(page.Description);
                            }
                        }

                        break;
                    }
                    case ParsingPageType.N1:
                        page.CostType = GetCostType(page.Description);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }


                if (page.CostType == CostType.Undefined)
                    Log.Warn("[" + page.Type + "] Не удалось распознать CostType: " + page.Url);
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка парсинга параметра: " + PageField + " " + page.Url);
                Log.Exception(ex, LogType.Error, true);
            }
        }


        private CostType GetCostType(string source)
        {
            if (string.IsNullOrEmpty(source))
                return CostType.Undefined;

            var result = CostType.ВсеВключено;
            if (string.IsNullOrEmpty(source))
                return result;
            var preparedData = source.ToLower().Replace("ё", "е").Replace("мм", "м").Replace(",", ".").Replace("(", "")
                .Replace(")", "").Replace(" ", "").Replace(" ", "").Replace("\\", "/");
            if (string.IsNullOrEmpty(preparedData))
                return result;

            if (preparedData == "ком.платеживключеныбезсчетчиков")
            {
                return CostType.ВсеВключено;
            }

            if (preparedData.Contains("ком.платежибезсчетчиков"))
            {
                return CostType.Коммуналка;
            }

            if (preparedData.Contains("к/у") || preparedData.Contains("+ку") || preparedData.Contains("+кварплат") ||
                preparedData.Contains("квитанции") ||
                preparedData.Contains("ком.услуги") || preparedData.Contains("ком.плат") ||
                preparedData.Contains("комунал"))
            {
                result = CostType.Коммуналка;
            }

            if (!preparedData.Contains("безсчетч") && (preparedData.Contains("четчик") ||
                                                       preparedData.Contains("эл.энергии") ||
                                                       preparedData.Contains("+свет")))
            {
                result = result == CostType.Коммуналка ? CostType.КоммуналкаИСчетчики : CostType.Счетчики;
                return result;
            }

            return result;
        }

    }
}

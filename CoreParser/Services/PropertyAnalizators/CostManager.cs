﻿using System;
using System.Globalization;
using System.Linq;
using Core.Enum;
using CoreParser.Enum;
using CoreParser.Interface;
using CoreParser.Pages;
using Core.Extension;
using Logger;

namespace CoreParser.Services.PropertyAnalizators
{
    internal class CostManager : IAdvertismentPropertyAnalizator
    {
        public PageField PageField => PageField.Cost;


        public void Identify(AbstractAdvertisement page)
        {
            if (!DictionaryStorage.PageParsingSettings.TryGetValue(page.Type, out var selectorDictionary))
                throw new Exception("Неизвестный тип страницы: " + page.Type);
            if (!selectorDictionary.TryGetValue(PageField, out var selector))
                throw new Exception("Не указан селектор [" + PageField + "] для страницы типа: " + page.Type);
            try
            {
                var nodes = page.TrySelectNodes(PageField, selector, out var isShouldContinue);

                if (nodes == null && !isShouldContinue)
                    return;
                
                var firstNodeInnerHtml = nodes != null && nodes.Count > 0
                    ? nodes[0].InnerHtml.Replace("&nbsp;", " ").Replace("\n", "").Trim()
                    : "";
                page.Cost = null;
                string costStr = null;
                switch (page.Type)
                {
                    case ParsingPageType.Avito:
                        if (nodes == null)
                        {
                            nodes = page.TrySelectNodes(null, "//div[@id='price-value']", out isShouldContinue);
                            firstNodeInnerHtml = nodes != null && nodes.Count > 0
                                ? nodes[0].InnerText.Replace("&nbsp;", " ").Replace("\n", "").Trim()
                                : "";
                        }
                       costStr = firstNodeInnerHtml;
                        if (costStr == "ценанеуказана" || costStr == "Цена не указана")
                            costStr = "договорная";
                        break;
                    case ParsingPageType.Mlsn:
                        if (nodes != null && nodes.Count > 0)
                            costStr = nodes[0].InnerText.Split('₽').FirstOrDefault();
                        break;
                    case ParsingPageType.Cian:
                        if (nodes != null && nodes.Count != 0)
                            costStr = nodes[0].InnerText.Replace("&nbsp;", "").Replace("₽", "").Replace("/мес.", "").Replace("/сут.", "");//.Split('₽').FirstOrDefault();
                        break;
                    case ParsingPageType.Yandex:
                        if (nodes != null)
                        {
                            if (nodes.Count == 1)
                            {
                                var text = "";
                                var node = nodes[0];
                                while (node.HasChildNodes)
                                {
                                    node = node.FirstChild;
                                }
                                firstNodeInnerHtml = node.InnerText.Replace("&nbsp;", "").Replace("<span class=\"price\">", "").Replace("₽", "").Split('<').FirstOrDefault();
                                if(string.IsNullOrEmpty(firstNodeInnerHtml))
                                    throw new Exception("не удалось распознать стоимость: '"+ nodes[0].InnerHtml + "'");
                                if (firstNodeInnerHtml.Contains("млн"))
                                {
                                    if (decimal.TryParse(firstNodeInnerHtml.Replace("млн", "").Replace(".", ",").Trim(),
                                        out var yaCost))
                                    {
                                        costStr = (yaCost * 1000000).ToString(CultureInfo.InvariantCulture).Replace(".", ",");
                                    }
                                    else
                                        costStr = firstNodeInnerHtml.Replace("в месяц", "");
                                }
                                else if (firstNodeInnerHtml.Contains("месяц"))
                                {
                                    costStr = firstNodeInnerHtml.Replace(" в месяц", "");
                                }
                                else
                                {
                                    costStr = firstNodeInnerHtml;
                                }
                            }
                            else
                            {
                                firstNodeInnerHtml = nodes[0].InnerText.Replace("₽", "").Replace("&nbsp;", "").Replace(" ", "").Replace("/", "");
                                if (firstNodeInnerHtml.Contains("млн"))
                                {
                                    if (decimal.TryParse(firstNodeInnerHtml.Replace("млн", "").Replace(".", ",").Trim(),
                                        out var yaCost))
                                    {
                                        costStr = (yaCost * 1000000).ToString(CultureInfo.InvariantCulture).Replace(".", ",");
                                    }
                                    else
                                        costStr = firstNodeInnerHtml.Replace("вмесяц", "");
                                }
                                else if (firstNodeInnerHtml.Contains("месяц"))
                                {
                                    costStr = firstNodeInnerHtml.Replace("вмесяц", "");
                                } else if (firstNodeInnerHtml.Contains("мес"))
                                {
                                    costStr = firstNodeInnerHtml.Replace("мес.", "");
                                }
                                else
                                {
                                    costStr = firstNodeInnerHtml;
                                }
                            }
                        }

                        costStr = costStr?.Replace(" ").Replace("<spanclass=\"price\">")
                            .Replace("</span>").Replace("<span>").Replace("<!--").Replace("-->").Replace("мес").Replace("/,").Replace("\\,").FindDigit();
                        break;
                    case ParsingPageType.N1:
                        if (nodes != null && nodes.Count == 1)
                            costStr = nodes[0].InnerText.Replace(" ", "").Trim();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                costStr = costStr?.Replace(".", ",").Replace("&nbsp", "").Replace(" ", "").Replace(";", "").Trim().ToLower();
                if (decimal.TryParse(costStr, out var cost))
                    page.Cost = cost;
                else if (page.ObjType.Type == EnumObjType.Commercial || costStr.Contains("договорная"))
                    page.Cost = -1;
                else
                {
                    Log.Fatal("[" + page.Type + "] Не удалось распознать стоимость: '" + costStr + "' " + page.Url);
                    page.LogInFile("BadCost", "costStr = '"+ costStr+"'");
                }

                //if (cost > 100000000){Log.Fatal("Стоимость больше 100млн? " + page.Url);} // Есть уроды которые ставят ценик в 999 млн
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка парсинга параметра: " + PageField + " " + page.Url);
                Log.Exception(ex, LogType.Error, true);
            }
        }
    }
}

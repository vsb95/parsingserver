using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Core.Enum;
using CoreParser.Enum;
using CoreParser.Interface;
using CoreParser.Pages;
using Logger;

namespace CoreParser.Services.PropertyAnalizators
{
    public class Agency2Manager : IAdvertismentPropertyAnalizator
    {
        public PageField PageField => PageField.IsAgency;

        /// <summary>
        /// Отладочная информация по последней идентификации
        /// </summary>
        public string DebugInfo { get; private set; }

        public void Identify(AbstractAdvertisement page)
        {
            if(page == null)
                return;
            DebugInfo = null;

            var debugInfoBuilder = new StringBuilder();
            // Основано на https://pp.userapi.com/c846123/v846123750/19252a/z5KMm6oQuPY.jpg
            debugInfoBuilder.AppendLine("Проверка " + page.Url);

            var description = page.Description?.Replace("<br>", "\r\n");
            string dbgDescription = "";
            try
            {
                if (string.IsNullOrEmpty(description))
                {
                    debugInfoBuilder.AppendLine("Отсутствует описание. Считаем по разделу. \r\nЕсли не определен, то агентство");
                    page.IsAgency = !page.IsFromOwnerSection ?? true;
                    return;
                }

                if (page.IsMls)
                {
                    debugInfoBuilder.AppendLine("Объявление из мультилистинга - там всегда только агентства");
                    page.IsAgency = true;
                    return;
                }

                var hardAgencyList = HardAgencyKeywordList;
                var lightAgencyList = LightAgencyKeywordList;

                var hardSelfList = HardSelfKeywordList;
                var lightSelfList = LightSelfKeywordList;
                
                // удаляем все слова-исключения
                foreach (var regex in ExcludeList)
                {
                    description = regex.Replace(description, "");
                }

                dbgDescription = description;

                // Если у страницы определен раздел
                if (page.IsFromOwnerSection != null)
                    //&& (page.Type == ParsingPageType.Mlsn || page.Type == ParsingPageType.Avito))
                {
                    debugInfoBuilder.AppendLine("У объявления определен раздел на источнике: " +
                                                (page.IsFromOwnerSection.Value
                                                    ? "частники\\без комиссии"
                                                    : "агентства"));
                    // Проверка на профиль агентства
                    var isAgencyProfile = IsAgencyProfile(page, debugInfoBuilder);
                    if (isAgencyProfile.HasValue && isAgencyProfile.Value)
                    {
                        page.IsAgency = true;
                        return;
                    }

                    // Если из раздела частники \ без комиссии
                    if (page.IsFromOwnerSection.Value)
                    {
                        // есть жесткие ключевики на агентства?
                        if (hardAgencyList.Any(regex => regex.IsMatch(description)))
                        {
#if true//DEBUG
                            foreach (var regex in hardAgencyList)
                            {
                                var match = regex.Match(description);
                                if (match.Success)
                                {
                                    debugInfoBuilder.AppendLine("нашли жесткий ключевик агентства: " + match.Value);
                                    dbgDescription = dbgDescription.Replace(match.Value, "<strong>" + match.Value + "</strong>");
                                }
                            }
#endif
                            page.IsAgency = true;
                            return;
                        }

                        debugInfoBuilder.AppendLine("Не нашли жестких ключевиков на агентства");
                        page.IsAgency = false;
                        return;
                    }
                    // Если из раздела агентства
                    else
                    {
                        // есть жесткие на частника?
                        if (hardSelfList.Any(regex => regex.IsMatch(description)))
                        {
#if true//DEBUG
                            foreach (var regex in hardSelfList)
                            {
                                var match = regex.Match(description);
                                if (match.Success)
                                {
                                    debugInfoBuilder.AppendLine("нашли жесткий ключевик собственника: " + match.Value);
                                    dbgDescription = dbgDescription.Replace(match.Value, "<strong>" + match.Value + "</strong>");
                                }
                            }
#endif
                            page.IsAgency = false;
                            return;
                        }
                        else
                        {
                            debugInfoBuilder.AppendLine("не нашли жестких на частника");
                        }

                        // есть обычный ключевик на частника?
                        if (lightSelfList.Any(regex => regex.IsMatch(description)))
                        {
#if true//DEBUG
                            foreach (var regex in lightSelfList)
                            {
                                var match = regex.Match(description);
                                if (match.Success)
                                {
                                    debugInfoBuilder.AppendLine("нашли обычный ключевик собственника: " + match.Value);
                                    dbgDescription = dbgDescription.Replace(match.Value, "<strong>" + match.Value + "</strong>");
                                }
                            }
#endif

                            // есть жесткие ключевики на агентства?
                            if (hardAgencyList.Any(regex => regex.IsMatch(description)))
                            {
#if true//DEBUG
                                foreach (var regex in hardAgencyList)
                                {
                                    var match = regex.Match(description);
                                    if (match.Success)
                                    {
                                        debugInfoBuilder.AppendLine("нашли жесткий ключевик агентства: " + match.Value);
                                        dbgDescription = dbgDescription.Replace(match.Value, "<strong>" + match.Value + "</strong>");
                                    }
                                }
#endif
                                page.IsAgency = true;
                                return;
                            }
                            else
                            {
                                debugInfoBuilder.AppendLine("не нашли жесткий на агентства");
                            }

                            // есть обычный ключевик на агентство?
                            if (lightAgencyList.Any(regex => regex.IsMatch(description)))
                            {
#if true//DEBUG
                                foreach (var regex in lightAgencyList)
                                {
                                    var match = regex.Match(description);
                                    if (match.Success)
                                    {
                                        debugInfoBuilder.AppendLine("нашли обычный ключевик агентства: " + match.Value);
                                        dbgDescription = dbgDescription.Replace(match.Value, "<strong>" + match.Value + "</strong>");
                                    }
                                }
#endif
                                page.IsAgency = true;
                                return;
                            }
                            else
                            {
                                debugInfoBuilder.AppendLine("не нашли обычный на агентства");
                            }

                            page.IsAgency = false;
                            return;
                        }

                        debugInfoBuilder.AppendLine("Не нашли обычных ключевиков на частника");
                        page.IsAgency = true;
                        return;
                    }
                }
                // Иначе только по ключевикам
                else
                {
                    debugInfoBuilder.AppendLine("У объявления не определн раздел на сайте источнике");

                    // есть жесткие ключевики на агентства?
                    if (hardAgencyList.Any(regex => regex.IsMatch(description)))
                    {
#if true//DEBUG
                        foreach (var regex in hardAgencyList)
                        {
                            var match = regex.Match(description);
                            if (match.Success)
                            {
                                debugInfoBuilder.AppendLine("нашли жесткий ключевик агентства: " + match.Value);
                                dbgDescription = dbgDescription.Replace(match.Value, "<strong>" + match.Value + "</strong>");
                            }
                        }
#endif
                        page.IsAgency = true;
                        return;
                    }
                    else
                    {
                        debugInfoBuilder.AppendLine("не нашли жесткий на агентства");
                    }

                    // есть жесткие на частника?
                    if (hardSelfList.Any(regex => regex.IsMatch(description)))
                    {
#if true//DEBUG
                        foreach (var regex in hardSelfList)
                        {
                            var match = regex.Match(description);
                            if (match.Success)
                            {
                                debugInfoBuilder.AppendLine("нашли жесткий ключевик собственника: " + match.Value);
                                dbgDescription = dbgDescription.Replace(match.Value, "<strong>" + match.Value + "</strong>");
                            }
                        }
#endif
                        page.IsAgency = false;
                        return;
                    }
                    else
                    {
                        debugInfoBuilder.AppendLine("не нашли жесткий на частника");
                    }

                    // есть обычный ключевик на частника?
                    if (lightSelfList.Any(regex => regex.IsMatch(description)))
                    {
#if true//DEBUG
                        foreach (var regex in lightSelfList)
                        {
                            var match = regex.Match(description);
                            if (match.Success)
                            {
                                debugInfoBuilder.AppendLine("нашли обычный ключевик собственника: " + match.Value);
                                dbgDescription = dbgDescription.Replace(match.Value, "<strong>" + match.Value + "</strong>");
                            }
                        }
#endif

                        // есть обычный ключевик на агентство?
                        if (lightAgencyList.Any(regex => regex.IsMatch(description)))
                        {
#if true//DEBUG
                            foreach (var regex in lightAgencyList)
                            {
                                var match = regex.Match(description);
                                if (match.Success)
                                {
                                    debugInfoBuilder.AppendLine("нашли обычный ключевик агентства: " + match.Value);
                                    dbgDescription = dbgDescription.Replace(match.Value, "<strong>" + match.Value + "</strong>");
                                }
                            }
#endif
                            page.IsAgency = true;
                            return;
                        }

                        debugInfoBuilder.AppendLine("Не найдено обычных ключевиков на агентсво");
                        page.IsAgency = false;
                        return;
                    }
                    
                    debugInfoBuilder.AppendLine("Не найдено обычных на собственника");
                    // иначе это агентство
                    page.IsAgency = true;
                    return;
                }
            }
            catch (Exception ex)
            {
                debugInfoBuilder.AppendLine("Ошибочка: "+ex.Message);
                Log.Exception(ex, LogType.Error, true, "Agency_v2_Manager: " + page.Url);
                page.IsAgency = true;
            }
            finally
            {
                debugInfoBuilder.AppendLine("Результат: " + (page.IsAgency ? "Агентство" : "Собственник"));
                DebugInfo = "Текст с вырезанными исключениями: \r\n" + dbgDescription + "\r\n\r\n" + debugInfoBuilder;
#if DEBUG
                //Log.Debug(DebugInfo);
#endif
            }
        }

        public static bool? CheckAgencyByText(string description, bool? isFromOwnerSection, out string debugInfo)
        {
            var page = new MlsnAdvertisement("");
            page.Description = description;
            page.IsFromOwnerSection = isFromOwnerSection;
            var analizator = new Agency2Manager();
            analizator.Identify(page);
            debugInfo = analizator.DebugInfo;
            return page.IsAgency;
        }
        /// <summary>
        /// Проверка профиля на агентство
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        private bool? IsAgencyProfile(AbstractAdvertisement page, StringBuilder debugInfoBuilder)
        {
            if (!DictionaryStorage.PageParsingSettings.TryGetValue(page.Type, out var selectorDictionary))
                throw new Exception("Неизвестный тип страницы: " + page.Type);
            if (!selectorDictionary.TryGetValue(PageField, out var selector))
                throw new Exception("Не указан селектор [" + PageField + "] для страницы типа: " + page.Type);
            // млсн
            switch (page.Type)
            {
                case ParsingPageType.Mlsn:
                    // продажа
                    if (page.RentType == RentType.Sell)
                    {
                        if (string.IsNullOrEmpty(page.HtmlPage))
                        {
                            debugInfoBuilder.AppendLine("МЛСН продажа - тут должно быть определение агентства по профилю");
                            debugInfoBuilder.AppendLine("Но на данный момент этого сделать нельзя, тк страница не скачана");
                            return null;
                        }

                        var agencyNodes = page.TrySelectNodes(PageField, selector, out var isContinue);

                        if ((agencyNodes == null || agencyNodes.Count == 0) && !isContinue)
                            throw new Exception("Не распознан параметр IsAgency для страницы типа: " + page.Type + "\n\t" + page.Url);

                        var subTitle = agencyNodes[0].InnerText.ToLower();
                        if (subTitle.Contains("риэлтор в агентстве")
                            || subTitle.Contains("специалист по недвижимости")
                            || subTitle.Contains("частный риэлтор")
                            || subTitle.Contains("представитель застройщика")
                            || subTitle.Contains("представитель строительной организации")
                            || subTitle.Contains("ипотечный брокер"))
                        {
#if true//DEBUG
                            debugInfoBuilder.AppendLine("МЛСН продажа - определили агентство по профилю");
#endif
                            page.IsForceIsAgency = true;
                            return true;
                        }
                        // На ключевики в описании не будем проверять. не агентства пойдут через форсирование
                        page.IsAgency = true;
                        return true;
                    }
                    // аренда в мультилистинге?
                    if (page.IsMls)
                    {
#if true//DEBUG
                        debugInfoBuilder.AppendLine("МЛСН аренда - объявление в мультилистинге");
#endif
                        page.IsAgency = true;
                        return true;
                    }

                    break;
            }

            return null;
        }

        private static readonly List<Regex> ExcludeList = new List<Regex>()
        {
            // HTML tags
            new Regex("(<.{0,7}>|</.{0,7}>)", RegexOptions.Compiled | RegexOptions.IgnoreCase),

            new Regex("од(ин|на|ни) собствен(н|)и(к|ца|ки)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("собствен(н|)и(к|ца|ки) од(ин|на|ни)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("1 собствен(н|)и(к|ца)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("собствен(н|)и(к|ца) 1", RegexOptions.Compiled | RegexOptions.IgnoreCase),

            new Regex("дв(а|ое) собствен(н|)ик(а|ов)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("собствен(н|)ик(а|ов) дв(а|ое)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("2 собствен(н|)ик(а|ов)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("собствен(н|)ик(а|ов) 2", RegexOptions.Compiled | RegexOptions.IgnoreCase),

            new Regex("од(ин|на|ни) хозя(ин|йка|ева)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("хозя(ин|йка|ева) од(ин|на|ни)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("1 хозя(ин|йка)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("хозя(ин|йка) 1", RegexOptions.Compiled | RegexOptions.IgnoreCase),

            new Regex("дв(а|ое) хозя(ев|инов|ева|ина)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("хозя(ев|инов|ева|ина) два", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("2 хозя(ев|инов|ева|ина)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("хозя(ев|инов|ева|ина) 2", RegexOptions.Compiled | RegexOptions.IgnoreCase),

            new Regex("количество собствен(н|)иков", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("взросл(ый|ая|ые) собствен(н|)и(к|ца|ки)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("собствен(н|)(ая|ость|ости)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("управляющая компания", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("собствен(н|)ое ТСЖ", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("нов(ому|ого|ый) собствен(н|)ик(у|а|)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("плачу комиссион(н|)ое вознаграждение", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("ГРАЖДАН", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("собствен(н|)ост(и|ь) боле", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("займет", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("эксклюзивн(ая|ый|ому) (мебель|дизайн|ремонт)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("эксклюзивность", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("оплата по факту заселения", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("(моют|первые|жилые|верхние|крайние) этажи", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("этажи (не звонить|не беспокоить)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("(вставка|приставка|кадастровый номер объекта)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("собствен(н|)ик(ов|а| владеет)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("новому хозяину", RegexOptions.Compiled | RegexOptions.IgnoreCase),
        };

        private static readonly List<Regex> HardAgencyKeywordList = new List<Regex>()
        {
            new Regex(@"(аген(т|)с(т|)во\sнедвижимости|(>|\,|\.|\s|^)(АН|А\.Н\.)|компания)\s(\w+|"".{0,30}""|«.{0,30}»)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("сертифицированное аген(т|)с(т|)во", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("ключи (на руках|в аген(т|)с(т|)ве)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("займ под материнский капитал", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("ипотека без первоначального взноса", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("преференции", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("помощь в (получении|оформлении) ипотеки", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("окажем содействие", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("номер в базе", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("полное (юридическое |)сопровождение", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("банк(ов|и) партнер(ов|ы)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("индивидуальный подход к каждому клиенту", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("омского союза риэлторов", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("trade(-| |)in", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("компания недвижимости", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("по факту заселения", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("на руки собственнику", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("порядочные собственники", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("(Миард|Авеста|Квартсервис|Этажи|Доверие|Галеон|Ледон|-риэлт|Центр недвижимости)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("Авеста(-| |)Риэлт", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("Аркада( Стиль|-Стиль|)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("ипотека бесплатно", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("Квартира от Застройщика без посредников", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("(ставка|партнер|номер объекта)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex("сдается через аген(т|)с(т|)во", RegexOptions.Compiled | RegexOptions.IgnoreCase)
        };

        private static readonly List<Regex> HardSelfKeywordList = new List<Regex>()
        {
            new Regex(@"Я(-| )(сам(a|) |)собствен(н|)и(к|ца)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"(ри(э|е)лтор(ам|ов)|аген(т|)с(т|)в(а|ам|о)|п(о|а)средникам) (просьба |)(не беспокоить|не звонить)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"(Продажа|Аренда|сдача) (напрямую |)от собствен(н|)и(ка|цы|ци|ков)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"(Эксклюзив не интересен|не нуждаюсь)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"прода(ем|ю) сам(и|а|)(\,|\.|\s|$)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"(\s(\,|\.)|^)Собстве(н|)ник(\!|\.|\,|$)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
        };

        private static readonly List<Regex> LightAgencyKeywordList = new List<Regex>()
        {
            new Regex(@"(аген(т|)с(т|)во\sнедвижимости)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"переезд(ом|а) собствен(н|)и(ка|цы|ци|ков)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"собствен(н|)и(к|ца|ки) (переехал(и|а|)|хочет|готов(ы|а|)|предусматривает)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"сдает аген(т|)с(т|)во", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"(\,|\.|\s|^|с )комис(c|)и(я|ю)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"(прекрас(т|)н(ый|ая|ые)|отличн(ый|ая|ые)|хорош(ий|ая|ие)) (собствен(н|)и(к|ца|ки)|хозя(ин|йка|ева))", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"собствен(н|)и(ку|це|ки)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"(помощь|поможем|посодействуем|юридическое|сопровождение|эксклюзив(\,|\.|\s))", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"(\,|\.|\s|^)займ(\,|\.|\s|$)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"(эксклюзивный договор|Не подошел этот вариант|только реальные объекты|проведем сделку)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"военная ипотека", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"(предлагаем вашему вниманию|Количество собствен(н|)иков)", RegexOptions.Compiled | RegexOptions.IgnoreCase)
        };
        
        private static readonly List<Regex> LightSelfKeywordList = new List<Regex>()
        {
            new Regex(@"Комиссию не беру", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"не (аген(т|)с(т|)во|компания|ри(э|е)лтор|посредник)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"без (комис(c|)|посредников|доп(олнительных|(\,|\.)|) оплат)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"(собствен(н|)и(к|ца)|частник|от хозяина|хозя(ин|йка))(\s\,|\.|$)", RegexOptions.Compiled | RegexOptions.IgnoreCase)
        };
    }
}

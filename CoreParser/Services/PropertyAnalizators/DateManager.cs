﻿using System;
using System.Linq;
using Core.Enum;
using Core.Extension;
using CoreParser.Enum;
using CoreParser.Interface;
using CoreParser.Pages;
using Logger;

namespace CoreParser.Services.PropertyAnalizators
{
    internal class DateManager : IAdvertismentPropertyAnalizator
    {
        public PageField PageField => PageField.DatePublish;

        public void Identify(AbstractAdvertisement page)
        {
            if (!DictionaryStorage.PageParsingSettings.TryGetValue(page.Type, out var selectorDictionary))
                throw new Exception("Неизвестный тип страницы: " + page.Type);
            if (!selectorDictionary.TryGetValue(PageField, out var selector))
                throw new Exception("Не указан селектор [" + PageField + "] для страницы типа: " + page.Type);
            try
            {
                var nodes = page.TrySelectNodes(PageField, selector, out var isShouldContinue);

                if (nodes == null && !isShouldContinue)
                    return;

                var firstNodeInnerHtml = nodes != null && nodes.Count > 0
                    ? nodes[0].InnerHtml.Replace("&nbsp;", " ").Replace("\n", "").Trim()
                    : "";

                string publishDateStr = null;
                switch (page.Type)
                {
                    case ParsingPageType.Avito:
                    {
                        var preparedData = "";
                        foreach (var node in nodes)
                        {
                            preparedData += node.InnerText.Replace("&nbsp;", " ");
                        }

                        preparedData = preparedData.Replace("\n \n ", "\n");
                        if (preparedData.Contains("сегодня") || preparedData.Contains("Сегодня"))
                        {
                            publishDateStr = DateTime.Now.ToString("G");
                            break;
                        }

                        if (preparedData.Contains("вчера") || preparedData.Contains("Вчера"))
                        {
                            publishDateStr = DateTime.Now.Subtract(new TimeSpan(1, 0, 0, 0)).ToString("G");
                            break;
                        }

                        var digits = preparedData.FindDigits(true);
                        if (digits[2].Contains("размещено"))
                        {
                            var preparedMonth = digits[4].Trim().Split(' ');
                            var month = preparedMonth[0];
                            publishDateStr = digits[3] + "." + DictionaryStorage.GetMonthNumber(month) + "." +
                                             DateTime.Now.Year;
                        }
                        if (digits[0].Contains("размещено"))
                        {
                            var preparedMonth = digits[2].Trim().Split(' ');
                            var month = preparedMonth[0];
                            publishDateStr = digits[1] + "." + DictionaryStorage.GetMonthNumber(month) + "." +
                                             DateTime.Now.Year;
                        }

                            break;
                    }
                    case ParsingPageType.Mlsn:
                    {
                        var node = nodes.Last();
                        var t = node.InnerText.Split(new[] {"Обновлено "}, StringSplitOptions.RemoveEmptyEntries);
                        publishDateStr = t[1];
                        break;
                    }
                    case ParsingPageType.Cian:
                    {
                        if (nodes != null && nodes.Count != 0)
                        {
                            var preparedData = nodes[0].InnerText.Split(',').FirstOrDefault();

                            if (preparedData.Contains("сегодня") || preparedData.Contains("Сегодня"))
                            {
                                publishDateStr = DateTime.Now.ToString("G");
                                break;
                            }

                            if (preparedData.Contains("вчера") || preparedData.Contains("Вчера"))
                            {
                                publishDateStr = DateTime.Now.Subtract(new TimeSpan(1, 0, 0, 0)).ToString("G");
                                break;
                            }

                            var t = preparedData.Split(',');
                            publishDateStr = t.FirstOrDefault();
                        }
                            break;
                    }
                    case ParsingPageType.Yandex:
                    {
                        if (!string.IsNullOrEmpty(firstNodeInnerHtml))
                        {
                            if (firstNodeInnerHtml.ToLower().Contains("минут"))
                            {
                                publishDateStr = DateTime.Now.Subtract(new TimeSpan(0, 1, 0, 0)).ToString("G");
                                break;
                            }

                            if (firstNodeInnerHtml.ToLower().Contains("сегодня"))
                            {
                                publishDateStr = DateTime.Now.ToString("G");
                                break;
                            }

                            if (firstNodeInnerHtml.ToLower().Contains("вчера"))
                            {
                                publishDateStr = DateTime.Now.Subtract(new TimeSpan(1, 0, 0, 0)).ToString("G");
                                break;
                            }

                            string day = null;
                            string month = null;
                            var chunks = firstNodeInnerHtml.Split(',');
                            if (chunks.Length == 2)
                            {
                                publishDateStr = chunks[0].Replace("Объявление обновлено", "").Trim();
                                //createDateStr = chunks[1].Replace("размещено", "").Trim();

                                day = publishDateStr.FindDigits(true).FirstOrDefault()?.Trim();
                            }
                            else if (chunks.Length == 1)
                            {
                                publishDateStr = chunks[0].Replace("Объявление размещено ", "").Trim();
                                if (DateTime.TryParse(publishDateStr, out var date))
                                    break;
                                
                                day = publishDateStr.FindDigits(true).FirstOrDefault()?.Trim();
                            }

                            month = publishDateStr?.Replace((DateTime.Now.Year - 1).ToString(), "")
                                .Replace(DateTime.Now.Year.ToString(), "").Replace(day, "").Trim();

                                if (month != null && month.Contains("час"))
                            {
                                if (int.TryParse(day, out var hours))
                                {
                                    var dt = DateTime.Now - new TimeSpan(hours, 0, 0);
                                    publishDateStr = dt.ToString("g");
                                    //if (chunks.Length == 1) createDateStr = publishDateStr;

                                }
                            }
                            else
                            {
                                publishDateStr = day + "." + DictionaryStorage.GetMonthNumber(month) + "." +
                                                 DateTime.Now.Year;
                                //if (chunks.Length == 1) createDateStr = publishDateStr;
                            }
                        }

                        break;
                    }
                    case ParsingPageType.N1:
                    {
                        if (!string.IsNullOrEmpty(firstNodeInnerHtml))
                        {
                            if (firstNodeInnerHtml.ToLower().Contains("сегодня"))
                            {
                                publishDateStr = DateTime.Now.ToString("G");
                                break;
                            }

                            if (firstNodeInnerHtml.ToLower().Contains("вчера"))
                            {
                                publishDateStr = DateTime.Now.Subtract(new TimeSpan(1, 0, 0, 0)).ToString("G");
                                break;
                            }
                            
                            var chunks = firstNodeInnerHtml.Split(',');
                            if (chunks.Length == 2)
                            {
                                publishDateStr = chunks[0].Replace("Обновлено ", "").Trim();
                                //createDateStr = chunks[1].Replace("  опубликовано ", "").Trim();
                            }
                            else if (chunks.Length == 1)
                            {
                                var datePublishStr = chunks[0].Replace("Объявление размещено ", "").Trim();

                                var day = datePublishStr.FindDigits(true).FirstOrDefault()?.Trim();
                                var month = datePublishStr.Replace(day, "").Trim();
                                //createDateStr = day + "." + DictionaryStorage.GetMonthNumber(month) + "." + DateTime.Now.Year;
                            }
                        }

                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                if (DateTime.TryParse(publishDateStr, out var datePublish))
                    page.PublishDateTime = datePublish;
                else
                    Log.Warn("[" + page.Type + "] Не смогли распарсить дату публикации: '" + publishDateStr + "' : " +
                             page.Url);
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка парсинга параметра: " + PageField + " " + page.Url);
                Log.Exception(ex, LogType.Error, true);
            }
        }
    }
}

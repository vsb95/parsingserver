﻿using System;
using CoreParser.Enum;
using CoreParser.Interface;
using CoreParser.Pages;
using Logger;

namespace CoreParser.Services.PropertyAnalizators
{
    internal class PhoneManager : IAdvertismentPropertyAnalizator
    {
        public PageField PageField => PageField.PhoneNumber;

        public void Identify(AbstractAdvertisement page)
        {
            if (!DictionaryStorage.PageParsingSettings.TryGetValue(page.Type, out var selectorDictionary))
                throw new Exception("Неизвестный тип страницы: " + page.Type);
            if (!selectorDictionary.TryGetValue(PageField, out var selector))
                throw new Exception("Не указан селектор [" + PageField + "] для страницы типа: " + page.Type);
            try
            {
                var nodes = page.TrySelectNodes(PageField, selector, out var isShouldContinue);

                if (nodes == null && !isShouldContinue)
                    return;

                var firstNodeInnerHtml = nodes != null && nodes.Count > 0
                    ? nodes[0].InnerHtml.Replace("&nbsp;", " ").Replace("\n", "").Trim()
                    : "";

                string phone = "Undefined"; // TODO PhoneNumber
                /*
                switch (page.Type)
                {
                    case ParsingPageType.Avito:
                    case ParsingPageType.Mlsn:
                        // Выкачивается через RabbitMQ
                        break;
                    case ParsingPageType.Cian:
                    {
                        if (nodes != null && nodes.Count != 0)
                        {
                            var tt = nodes[0].FirstChild?.Attributes["href"]?.Value;
                            phone = tt.Replace("tel:", "");
                        }
                            break;
                    }
                    case ParsingPageType.Yandex:
                    {
                    

                        var t = page.HtmlPage.Split(new string[] { "&quot;phonesPairs&quot;" }, StringSplitOptions.RemoveEmptyEntries)
                            .LastOrDefault()?.Replace(":[{&quot;temporary&quot;:&quot;", "");
                        var tt = t.Split(new string[] { "&quot;" }, StringSplitOptions.RemoveEmptyEntries);
                        phone = tt.FirstOrDefault();
                        if (phone.Length > 15)
                            phone = "Undefined";
                        break;
                    }
                    case ParsingPageType.N1:
                        if (nodes != null && nodes.Count == 1)
                            phone = nodes[0].GetAttributeValue("href", "Undefined").Replace("tel:", "");
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                */

                page.PhoneNumber = phone;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка парсинга параметра: " + PageField + " " + page.Url);
                Log.Exception(ex, LogType.Error, true);
            }
        }
    }
}

﻿using System;
using Core.Enum;
using CoreParser.Enum;
using CoreParser.Interface;
using CoreParser.Pages;
using Logger;

namespace CoreParser.Services.PropertyAnalizators
{
    internal class SellTypeManager : IAdvertismentPropertyAnalizator
    {
        public PageField PageField => PageField.SellType;

        public void Identify(AbstractAdvertisement page)
        {
            if (page == null || page.RentType != RentType.Sell)
                return;
            if (!DictionaryStorage.PageParsingSettings.TryGetValue(page.Type, out var selectorDictionary))
                throw new Exception("Неизвестный тип страницы: " + page.Type);
            if (!selectorDictionary.TryGetValue(PageField, out var selector))
                throw new Exception("Не указан селектор [" + PageField + "] для страницы типа: " + page.Type);
            try
            {

                var nodes = page.TrySelectNodes(PageField, selector, out var isShouldContinue);

                if (nodes == null && !isShouldContinue)
                    return;

                var firstNodeInnerHtml = nodes != null && nodes.Count > 0
                    ? nodes[0].InnerHtml.Replace("&nbsp;", " ").Replace("\n", "").Trim()
                    : "";

                SellType selltype = SellType.Undefined;
                switch (page.Type)
                {
                    case ParsingPageType.Avito:
                    {
                        if (nodes == null)
                            break;
                        for (var i = nodes.Count - 1; i >= 0; i--)
                        {
                            var breadcrumb = nodes[i].InnerText.Replace("&nbsp;", " ").Trim();
                            switch (breadcrumb)
                            {
                                case "Вторичка":
                                    selltype = SellType.Вторичка;
                                    break;
                                case "Новостройки":
                                    selltype = SellType.Новостройка;
                                    break;
                            }

                            if (selltype != SellType.Undefined)
                                break;
                        }

                        // У домов, дач и коттеджей нет такой характеристики
                        if (selltype == SellType.Undefined
                            && (page.ObjType.Type == EnumObjType.Dacha
                                || page.ObjType.Type == EnumObjType.Dom
                                || page.ObjType.Type == EnumObjType.Komnata
                                || page.ObjType.Type == EnumObjType.Kottedj
                                || page.ObjType.Type == EnumObjType.Commercial))
                            selltype = SellType.Вторичка;

                        break;
                    }
                    case ParsingPageType.Mlsn:
                    {
                        if (nodes == null)
                            break;
                        selltype = SellType.Вторичка;
                        foreach (var node in nodes)
                        {
                            var title = node.ParentNode.InnerText;
                            if (title.Contains("Новостройка"))
                            {
                                selltype = node.InnerText == "да" ? SellType.Новостройка : SellType.Вторичка;
                                break;
                            }

                            if (!title.Contains("Срок сдачи")) continue;
                            selltype = SellType.Новостройка;
                            break;
                        }

                        break;
                    }
                    case ParsingPageType.Cian:
                    {
                        foreach (var node in nodes)
                        {
                            if (!node.InnerText.Contains("Тип жилья")) continue;
                            var preparedData = node.InnerText.Replace("Тип жилья", "");
                            switch (preparedData)
                            {
                                case "Вторичка":
                                    selltype = SellType.Вторичка;
                                    break;
                                case "Новостройка":
                                    selltype = SellType.Новостройка;
                                    break;
                            }
                            break;
                        }
                        break;
                    }
                    case ParsingPageType.Yandex:
                    {
                        if (nodes == null)
                        {
                            selltype = SellType.Вторичка;
                        }
                        else
                        {
                            var node = nodes[0].InnerText;
                            switch (node)
                            {
                                case "новостройка":
                                    selltype = SellType.Новостройка;
                                    break;
                                default:
                                    selltype = SellType.Вторичка;
                                    Log.Error("Неизвестный тип продажи у яндекса: " + node + "; " + page.Url);
                                    break;
                            }
                        }

                        break;
                    }
                    case ParsingPageType.N1:
                    {
                        if (nodes == null)
                            break;
                        for (var i = nodes.Count - 1; i >= 0; i--)
                        {
                            var breadcrumb = nodes[i].InnerText.Replace("&nbsp;", " ").Trim();
                            switch (breadcrumb)
                            {
                                case "Вторичное жильё":
                                    selltype = SellType.Вторичка;
                                    break;
                                case "Новостройки":
                                    selltype = SellType.Новостройка;
                                    break;
                            }

                            if (selltype != SellType.Undefined)
                                break;
                        }

                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                if (selltype == SellType.Undefined
                    && (page.ObjType.Type == EnumObjType.Dacha
                        || page.ObjType.Type == EnumObjType.Uchastok
                        || page.ObjType.Type == EnumObjType.Dom
                        || page.ObjType.Type == EnumObjType.Komnata
                        || page.ObjType.Type == EnumObjType.Kottedj
                        || page.ObjType.Type == EnumObjType.Commercial))
                    selltype = SellType.Вторичка;

                if (selltype != SellType.Undefined)
                    page.SellType = selltype;
                else
                    Log.Fatal("[" + page.Type + "] Не удалось определить SellType: " + page.Url);
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка парсинга параметра: " + PageField + " " + page.Url);
                Log.Exception(ex, LogType.Error, true);
            }
        }
    }
}

﻿using System;
using System.Linq;
using Core.Enum;
using CoreParser.Enum;
using CoreParser.Interface;
using CoreParser.Pages;
using Logger;

namespace CoreParser.Services.PropertyAnalizators
{
    internal class RentTypeManager : IAdvertismentPropertyAnalizator
    {
        public PageField PageField => PageField.RentType;

        public void Identify(AbstractAdvertisement page)
        {
            if (!DictionaryStorage.PageParsingSettings.TryGetValue(page.Type, out var selectorDictionary))
                throw new Exception("Неизвестный тип страницы: " + page.Type);
            if (!selectorDictionary.TryGetValue(PageField, out var selector))
                throw new Exception("Не указан селектор [" + PageField + "] для страницы типа: " + page.Type);
            try
            {
                var nodes = page.TrySelectNodes(PageField, selector, out var isShouldContinue);

                if (nodes == null && !isShouldContinue)
                    return;

                var firstNodeInnerText = nodes != null && nodes.Count > 0
                    ? nodes[0].InnerText.Replace("&nbsp;", " ").Replace("\n", "").Trim().ToLower()
                    : "";

                switch (page.Type)
                {
                    case ParsingPageType.Avito:
                    {
                        foreach (var node in nodes)
                        {
                            var preparedNode = node.InnerText.Replace("&nbsp;", " ").Trim();
                            if (preparedNode.Contains("Посуточно"))
                            {
                                page.RentType = RentType.DailyRent;
                                break;
                            }

                            if (preparedNode.Contains("На длительный срок"))
                            {
                                page.RentType = RentType.Rent;
                                break;
                            }

                            if (preparedNode.Contains("Продам") 
                                || preparedNode.Contains("Покупка")
                                || preparedNode.Contains("Купить"))
                            {
                                page.RentType = RentType.Sell;
                                break;
                            }
                            
                        }

                        break;
                    }
                    case ParsingPageType.Mlsn:
                    {
                        if (page.Url.Contains("pokupka"))
                        {
                            page.RentType = RentType.Sell;
                        }

                        if (page.Url.Contains("arenda"))
                        {
                            if (page.Url.Contains("posutochnaja"))
                            {
                                page.RentType = RentType.DailyRent;
                                break;
                            }

                            page.RentType = RentType.Rent;
                            if (nodes == null)
                                break;
                            foreach (var node in nodes)
                            {
                                if (!node.ParentNode.InnerText.Contains("Период сдачи в аренду")) continue;
                                if (node.InnerText != "любой срок" && node.InnerText != "от 1 года и более")
                                {
                                    // Проверим по стоимости за период (месяц/сутки)
                                    if (selectorDictionary.TryGetValue(PageField.Cost, out selector))
                                    {
                                        var priceNode = page
                                            .TrySelectNodes(PageField.Cost, selector, out isShouldContinue)
                                            ?.FirstOrDefault();
                                        var type = priceNode?.InnerText?.Split('₽').LastOrDefault();
                                        if (type!= null && type.Contains("мес"))
                                        {
                                            page.RentType = RentType.Rent;
                                            break;
                                        }

                                        if (type != null && type.Contains("сут"))
                                        {
                                            page.RentType = RentType.DailyRent;
                                            break;
                                        }
                                    }

                                    // Определим по сумме
                                    page.RentType = RentType.DailyRent;
                                    if (node.InnerText == "не указано")
                                    {
                                        switch (page.ObjType.Type)
                                        {
                                            case EnumObjType.Dacha:
                                                page.RentType = page.Cost >= 6000 ? RentType.Rent : RentType.DailyRent;
                                                    break;
                                            case EnumObjType.Dom:
                                            case EnumObjType.Kottedj:
                                                page.RentType = page.Cost >= 6000 ? RentType.Rent : RentType.DailyRent;
                                                    break;
                                            case EnumObjType.Komnata:
                                                page.RentType = page.Cost >= 3500 ? RentType.Rent : RentType.DailyRent;
                                                    break;
                                            case EnumObjType.Studia:
                                            case EnumObjType.Kvartira1:
                                            case EnumObjType.Kvartira2:
                                                page.RentType = page.Cost >= 4000 ? RentType.Rent : RentType.DailyRent;
                                                    break;
                                            case EnumObjType.Kvartira3:
                                            case EnumObjType.Kvartira4:
                                            case EnumObjType.Kvartira5Plus:
                                                page.RentType = page.Cost >= 5000 ? RentType.Rent : RentType.DailyRent;
                                                    break;
                                            case EnumObjType.Dolya:
                                            case EnumObjType.Uchastok:
                                            case EnumObjType.Commercial:
                                                page.RentType = RentType.Rent;
                                                    break;
                                            default:
                                                throw new ArgumentOutOfRangeException();
                                        }

                                        Log.Error("MLSN не указана длительность аренды: " + page.Url +
                                                  " определили как " + page.RentType+". Просьба перепроверить", true);
                                    }
                                }

                                break;
                            }
                        }

                        if (page.RentType == RentType.DailyRent)
                        {
                            Log.Error("Кв определена как посуточная - перепроверь. " + page.Url);
                        }
                        break;
                    }
                    case ParsingPageType.Cian:
                    {
                        foreach (var node in nodes)
                        {
                            var breadcrumb = node.InnerText.ToLower();
                            switch (breadcrumb)
                            {
                                case "продажа":
                                    page.RentType = RentType.Sell;
                                    break;
                                case "аренда":
                                    page.RentType = RentType.Rent;
                                    break;
                                case "посуточно":
                                    page.RentType = RentType.DailyRent;
                                    break;

                            }
                            if (page.RentType != RentType.Undefined)
                                break;
                        }

                        break;
                    }
                    case ParsingPageType.Yandex:
                    {
                        if (!string.IsNullOrEmpty(firstNodeInnerText))
                        {
                            if (firstNodeInnerText.Contains("долгосрочная аренда"))
                            {
                                page.RentType = RentType.Rent;
                            }
                            else if (firstNodeInnerText.Contains("продажа"))
                            {
                                page.RentType = RentType.Sell;
                            }
                            else
                            {
                                page.RentType = RentType.DailyRent;
                                    if (selectorDictionary.TryGetValue(PageField.Cost, out selector))
                                    break;
                                nodes = page.TrySelectNodes(PageField.Cost, selector, out isShouldContinue);
                                var cost = nodes.FirstOrDefault()?.InnerText.ToLower();
                                if (cost != null && cost.Contains("месяц"))
                                    page.RentType = RentType.Rent;
                                    
                            }
                        }

                        break;
                    }
                    case ParsingPageType.N1:
                    {
                        if (!string.IsNullOrEmpty(firstNodeInnerText))
                        {
                            if (firstNodeInnerText.Contains("в месяц"))
                            {
                                page.RentType = RentType.Rent;
                            }
                            else if (firstNodeInnerText.Contains("в сутки"))
                            {
                                page.RentType = RentType.DailyRent;
                            }
                        }
                        else if (page.Title.ToLower().Contains("прода"))
                        {
                            page.RentType = RentType.Sell;
                        }
                        else if (page.Title.ToLower().Contains("аренда"))
                        {
                            page.RentType = RentType.Rent;
                        }

                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                if (page.RentType == RentType.Undefined)
                    Log.Error("[" + page.Type + "] Не удалось определить RentType: " +page.Url, true);

            }
            catch (Exception ex)
            {
                Log.Error("Ошибка парсинга параметра: " + PageField + " " + page.Url);
                Log.Exception(ex, LogType.Error, true);
            }
        }
    }
}

using System;
using System.Linq;
using Core.Enum;
using CoreParser.DB.LocalEntities;
using CoreParser.Enum;
using CoreParser.Interface;
using CoreParser.Pages;
using Core.Extension;
using Logger;

namespace CoreParser.Services.PropertyAnalizators
{
    internal class HouseMaterialManager : IAdvertismentPropertyAnalizator
    {
        public PageField PageField => PageField.HouseMaterial;

        public void Identify(AbstractAdvertisement page)
        {
            if (page.RentType != RentType.Sell 
                || page.ObjType.Type == EnumObjType.Commercial
                || page.ObjType.Type == EnumObjType.Uchastok)
                return;
            if (!DictionaryStorage.PageParsingSettings.TryGetValue(page.Type, out var selectorDictionary))
                throw new Exception("Неизвестный тип страницы: " + page.Type);
            if (!selectorDictionary.TryGetValue(PageField, out var selector))
                throw new Exception("Не указан селектор [" + PageField + "] для страницы типа: " + page.Type);
            try
            {
                var nodes = page.TrySelectNodes(PageField, selector, out var isShouldContinue);
                if (nodes == null && !isShouldContinue)
                    return;
                string housematerial = null;

                switch (page.Type)
                {
                    case ParsingPageType.Avito:
                    {
                        foreach (var node in nodes)
                        {
                            var row = node.InnerText.Replace("&nbsp;", " ").Replace("\n \n ", "\n");
                            if (string.IsNullOrEmpty(row.Trim()))
                                continue;
                            var chunks = row.Trim().Split(':');
                            if (chunks.Length < 2)
                                break;
                            if (chunks[0] != "Тип дома" && chunks[0] != "Материал стен") continue;
                            housematerial = chunks[1].Trim();
                            break;
                        }

                        break;
                    }
                    case ParsingPageType.Mlsn:
                    {
                        if (nodes == null)
                            break;
                        foreach (var node in nodes)
                        {
                            var title = node.ParentNode.InnerText;
                            if (!title.Contains("Материал дома")) continue;
                            housematerial = node.InnerText;
                            break;
                        }

                        break;
                    }
                    case ParsingPageType.Cian:
                    {
                        foreach (var node in nodes)
                        {
                            if (!node.InnerText.Contains("Тип дома") || !node.InnerText.Contains("Материалы стен")) continue;
                            var preparedData = node.InnerText.Replace("Тип дома", "").Replace("Материалы стен", "");
                            housematerial = preparedData;
                            break;
                        }

                        break;
                    }
                    case ParsingPageType.Yandex:
                    {
                        foreach (var node in nodes)
                        {
                            var text = node.InnerText;
                            if(!text.Contains("здание")) continue;

                            var houseType = text.Split("здание").FirstOrDefault();
                            housematerial = houseType;
                                break;
                        }

                        break;
                    }
                    case ParsingPageType.N1:
                    {
                        if (nodes == null)
                            break;
                        foreach (var node in nodes)
                        {
                            var title = node.ParentNode.InnerText;
                            if (title.StartsWith("Материал дома"))
                            {
                                housematerial = node.InnerText;
                                break;

                            }

                            if (node.InnerText.StartsWith("Материал дома"))
                            {
                                housematerial = node.InnerText.Replace("Материал дома", "");
                                break;
                            }
                        }

                        //page.Adress.HouseMaterial = nodes?.FirstOrDefault()?.InnerText;
                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                if (!string.IsNullOrEmpty(housematerial) && HouseMaterial.TryParse(housematerial, out var result))
                {
                    page.Adress.HouseMaterial = result;
                }
                else if(page.Type == ParsingPageType.N1 || page.Type == ParsingPageType.Yandex)
                    Log.Warn("[" + page.Type + "] Не удалось распознать тип дома '"+ housematerial+"': " + page.Url);
                else
                    Log.Error("[" + page.Type + "] Не удалось распознать тип дома'" + housematerial + "': " + page.Url, true);
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка парсинга параметра: " + PageField + " " + page.Url);
                Log.Exception(ex, LogType.Error, true);
            }
        }
    }
}

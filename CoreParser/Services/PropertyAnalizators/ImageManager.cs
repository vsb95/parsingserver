﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Core;
using Core.Enum;
using Core.Extension;
using Core.Services;
using CoreParser.Enum;
using CoreParser.Interface;
using CoreParser.Pages;
using CoreParser.Services.Downloaders;
using Logger;

namespace CoreParser.Services.PropertyAnalizators
{
    internal class ImageManager : IAdvertismentPropertyAnalizator
    {
        private ConcurrentBag<string> _downloadedImagesPathBag;
        private ConcurrentDictionary<string, int> ErrorTryingDownloadDictionary = new ConcurrentDictionary<string, int>();
        private int _topIndent = 0;
        private int _bottomIndent = 0;
        private string _pageUrl;
        public PageField PageField => PageField.XPathImage;

        public void Identify(AbstractAdvertisement page)
        {
            if (!DictionaryStorage.PageParsingSettings.TryGetValue(page.Type, out var selectorDictionary))
                throw new Exception("Неизвестный тип страницы: " + page.Type);
            if (!selectorDictionary.TryGetValue(PageField, out var selector))
                throw new Exception("Не указан селектор [" + PageField + "] для страницы типа: " + page.Type);
            try
            {
                // Если у нас уже есть скачанные изображения - то не надо обновлять
                if (page.DownloadedImagesPathList != null 
                    && page.DownloadedImagesPathList.Count > 0
                    && !page.DownloadedImagesPathList.First().Contains("http") 
                    && File.Exists(page.DownloadedImagesPathList.First()))
                    return;
                

                var nodes = page.TrySelectNodes(PageField, selector, out var isShouldContinue);

                if (nodes == null && !isShouldContinue)
                    return;
                
                var imagesUrlList = new List<string>();
                switch (page.Type)
                {
                    case ParsingPageType.Avito:
                    {
                        foreach (var node in nodes)
                        {
                            string urlImage = node.Attributes["data-url"].Value;
                            urlImage = "http:" + urlImage.Replace("640x480", "1280x960");
                            //string filename = urlImage.Substring(urlImage.LastIndexOf("/", StringComparison.Ordinal) + 1);
                            //urlImage = "https://66.img.avito.st/1280x960/" + filename;
                            imagesUrlList.Add(urlImage);
                        }

                        break;
                    }
                    case ParsingPageType.Mlsn:
                    {
                        foreach (var node in nodes)
                        {
                            var leftBorderCutted = node.InnerText.Split(new[] {"\"all\":"},
                                StringSplitOptions.None);
                            if (leftBorderCutted.Length < 2)
                                continue;
                            var photos = leftBorderCutted[1].Split(new[] {"\"count\":"},
                                StringSplitOptions.None);
                            var jsonUrls = photos[0].Split(new[] {"\"url\":"}, StringSplitOptions.None)
                                .ToList();
                            jsonUrls.RemoveAt(0);
                            foreach (string urlJson in jsonUrls)
                            {
                                var firstlevel = urlJson.Split(',');
                                var secondlevel = firstlevel[0].Split('"');
                                var item = secondlevel[1].Replace("{{ size }}", "large");
                                imagesUrlList.Add(item);
                            }
                        }

                        break;
                    }
                    case ParsingPageType.Cian:
                    {
                        var split = page.HtmlPage.Split(new[] {".jpg"}, StringSplitOptions.RemoveEmptyEntries);
                        for (var i = 0; i < split.Length - 1; i++)
                        {
                            var almostUrl = split[i];
                            var index = almostUrl.LastIndexOf('\"') + 1;
                            var length = almostUrl.Length - index;
                            var url = almostUrl.Substring(index, length);
                            url += ".jpg";
                            url = System.Text.RegularExpressions.Regex.Unescape(url).Replace("\\u002F","/");
                            if (url.Contains("image-temp/") || imagesUrlList.Contains(url) ||
                                !url.EndsWith("1.jpg"))
                                continue;
                            imagesUrlList.Add(url);
                        }

                        break;
                    }
                    case ParsingPageType.Yandex:
                    {
                        foreach (var node in nodes)
                        {
                            string url = "https:" + node.Attributes["href"].Value;
                            if (imagesUrlList.Contains(url))
                                continue;
                            imagesUrlList.Add(url);
                        }
                        break;
                    }
                    case ParsingPageType.N1:
                    {
                        var html = page.HtmlPage.Split("\"main_photo\":");
                        var htm = html[1].Split("\"video_link\":");
                        var ent = htm?.FirstOrDefault();
                        var tt = ent?.Split("\"1200x900p\":\"");
                        if (tt == null)
                            break;
                        for (var i = 1; i < tt.Count; i++)
                        {
                            var urlImage = tt[i].Split("\",").FirstOrDefault();
                            if (imagesUrlList.Contains(urlImage))
                                continue;
                            imagesUrlList.Add(urlImage);
                        }

                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                page.DownloadedImagesPathList = imagesUrlList;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка парсинга параметра: " + PageField + " " + page.Url);
                Log.Exception(ex, LogType.Error, true);
            }
        }


        public bool DownloadImages(AbstractAdvertisement page, string directoryTo = null)
        {
            // Метод не статический, тк используется коллекция задач

            if (page.DownloadedImagesPathList == null || page.DownloadedImagesPathList?.Count == 0)
            {
                Log.Trace("Не инициализирован список url изображений; Url: " + page.Url);
                return true;
            }

            _topIndent = page.TopIndent;
            _bottomIndent = page.BottomIndent;
            _pageUrl = page.Url;
            try
            {
                page.Directory = string.IsNullOrEmpty(directoryTo) ? Randomizer.CreateRandomFolder(page.Directory) : directoryTo;
                if (!Directory.Exists(page.Directory))
                    Directory.CreateDirectory(page.Directory);

                _downloadedImagesPathBag = new ConcurrentBag<string>();
                Task[] tasks = new Task[page.DownloadedImagesPathList.Count];
                //WebProxy proxy = null;//Proxy;
                for (var i = 0; i < page.DownloadedImagesPathList.Count; i++)
                {
                    var urlImage = page.DownloadedImagesPathList[i].Replace("https", "http");
                    if (!urlImage.Contains("http"))
                    {
                        Log.Debug("Попытка скачать скачанное изображение: {pageId: "+page.Id+", imageUrl: "+urlImage+"}");
                        continue;
                    }
                    var fileName = i + ".jpg";
                    try
                    {
                        if (urlImage.Contains(".png")) fileName = i + ".png";
                        else if (urlImage.Contains(".gif")) fileName = i + ".gif";
                        else if (urlImage.Contains(".webp")) fileName = i + ".webp";
                        else if (urlImage.Contains(".svg")) fileName = i + ".svg";
                        tasks[i] = Task.Factory.StartNew(() => { DowloadAndCropImage(page.Directory, urlImage, fileName); });
                        Thread.Sleep(50);
                    }
                    catch (Exception ex)
                    {
                        Log.Warn("Изображение по адресу \"" + urlImage + "\" не удалось скачать; Обьявление: " + page.Url+"\n Потому что: "+ex.Message);
                    }
                }
                Task.WaitAll(tasks);
                page.DownloadedImagesPathList = _downloadedImagesPathBag.ToList();
                return true;
            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

            return false;
        }

        protected void DowloadAndCropImage(string destination, string url, string filename = null)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(destination))
                    throw new ArgumentNullException("destination is empty");
                if (string.IsNullOrWhiteSpace(url))
                    throw new ArgumentNullException("URL is empty");

                DirectoryInfo dirInfo = new DirectoryInfo(destination + "\\Original");
                if (!dirInfo.Exists)
                {
                    dirInfo.Create();
                }

                if (SettingsManager.Settings.ServerSettings.IsConvertImagesToPng)
                {
                    DirectoryInfo dirInfo2 = new DirectoryInfo(destination + "\\PNG");
                    if (!dirInfo2.Exists)
                    {
                        dirInfo2.Create();
                    }
                }

                if (string.IsNullOrEmpty(filename))
                    filename = url.Substring(url.LastIndexOf("/", StringComparison.Ordinal) + 1);

                if (HttpWebRequestDownloader.DownloadFile(url, destination + "\\Original\\" + filename) <= 0)
                {
                    Log.Error("Не удалось скачать изображение через HttpWebRequestDownloader: " + url);
                    using (WebClient web = new WebClient())
                    {
                        web.DownloadFile(new Uri(url), destination + "\\Original\\" + filename);
                    }
                }

                var img = Image.FromFile(destination + "\\Original\\" + filename);

                //Если у нас высота картинки меньше чем 2 верхних и 2 нижних отступа - значит она ОООЧЕНЬ МАЛЕНЬКАЯ
                if (img.Height < _topIndent * 2 + _bottomIndent * 2)
                    return;

                Rectangle selectedArea = new Rectangle(0, _topIndent, img.Width, img.Height - _bottomIndent);
                var name = filename.LastIndexOf(".", StringComparison.Ordinal) == -1
                    ? filename
                    : filename.Substring(0, filename.LastIndexOf(".", StringComparison.Ordinal));
                var croppedImage = img.CropImage(selectedArea);

                string jpeg = destination + "\\" + name + ".jpg";

                croppedImage.Save(jpeg, ImageFormat.Jpeg);

                _downloadedImagesPathBag.Add(jpeg);
                if (SettingsManager.Settings.ServerSettings.IsConvertImagesToPng)
                {
                    string png = destination + "\\PNG\\" + name + ".png";
                    croppedImage.Save(png, ImageFormat.Png);
                    _downloadedImagesPathBag.Add(png);
                }
            }
            catch (WebException wex)
            {
                if (wex.Response != null && ((HttpWebResponse) wex.Response).StatusCode == HttpStatusCode.NotFound)
                {
                    Log.Info("Image Not Found (404). URL: " + url);
                    return;
                }

                if (wex.Message.Contains("истекло") || wex.Message.Contains("неожидано закрыто"))
                {
                    var errorCount = ErrorTryingDownloadDictionary.GetOrAdd(url, s => 0);
                    if (errorCount > 4)
                    {
                        Log.Fatal("Не скачано изображение: " + url + "\n\tиз: " + _pageUrl);
                        return;
                    }

                    Log.Error("Отвались по таймауту во время скачивания изображений");
                    ErrorTryingDownloadDictionary.TryUpdate(url, errorCount + 1, errorCount);
                    DowloadAndCropImage(destination, url, filename);
                }


                Log.Debug(wex + "\n" + wex.StackTrace);
                Log.Error(
                    "Image download Error: " + wex.Message + ";\nStatusCode = " +
                    (wex.Response as HttpWebResponse)?.StatusCode + ".\n\t" + wex.InnerException?.Message +
                    "\n\tURL: " + url);
            }
            catch (IOException ioex)
            {
                //ToDo Переписать нормально, не проверку на текст, а на тип исключения
                //if (ioex.Message.Contains("Удаленный хост принудительно разорвал существующее подключение"))
                //{
                var errorCount = ErrorTryingDownloadDictionary.GetOrAdd(url, s => 0);
                if (errorCount > 4)
                {
                    Log.Exception(ioex, LogType.Error, true);
                    Log.Fatal("Не скачано изображение: " + url + "\n\tиз: " + _pageUrl);
                    return;
                }

                ErrorTryingDownloadDictionary.TryUpdate(url, errorCount + 1, errorCount);
                Log.Error("Что то с транспортом. Попробуем еще раз: " + url);
                if (errorCount > 1)
                    Thread.Sleep(300);
                DowloadAndCropImage(destination, url, filename);
                //}
            }
            catch (Exception ex)
            {
                Log.Error("Была попытка скачать: " + url + "\n\tиз: " + _pageUrl);
                Log.Exception(ex, LogType.Error);
            }
        }

    }
}

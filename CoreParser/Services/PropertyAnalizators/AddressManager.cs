﻿using System;
using System.Linq;
using Core.Enum;
using Core.Extension;
using CoreParser.Enum;
using CoreParser.Interface;
using CoreParser.Pages;
using Logger;

namespace CoreParser.Services.PropertyAnalizators
{
    internal class AddressManager : IAdvertismentPropertyAnalizator
    {
        public PageField PageField => PageField.Address;


        public void Identify(AbstractAdvertisement page)
        {
            if (!DictionaryStorage.PageParsingSettings.TryGetValue(page.Type, out var selectorDictionary))
                throw new Exception("Неизвестный тип страницы: " + page.Type);
            if (!selectorDictionary.TryGetValue(PageField, out var selector))
                throw new Exception("Не указан селектор [" + PageField + "] для страницы типа: " + page.Type);
            try
            {
                var nodes = page.TrySelectNodes(PageField, selector, out var isShouldContinue);

                if (nodes == null && !isShouldContinue)
                    return;

                var firstNodeInnerHtml = nodes != null && nodes.Count > 0
                    ? nodes[0].InnerHtml.Replace("&nbsp;", " ").Replace("\n", "").Trim()
                    : "";

                switch (page.Type)
                {
                    case ParsingPageType.Avito:
                    {
                        if (nodes != null)
                            page.Adress.GeoSearchString = nodes[0].InnerText.Replace("\\n", "").Replace("\n", "")
                                .Replace("Адрес:", "").Replace("Скрыть карту", "").Trim();
                        break;
                    }
                    case ParsingPageType.Mlsn:
                    {
                        if (nodes != null)
                        {
                            if (string.IsNullOrEmpty(page.Adress.City))
                                page.Adress.City = CityManager.GetCityNameByUrl(page.Url);

                            var node = nodes[0].LastChild;
                            page.Adress.GeoSearchString = page.Adress.City + ", " + node?.InnerText;
                        }

                        break;
                    }
                    case ParsingPageType.Cian:
                    {
                        var subtitle = page.TrySelectNodes(null, "//span[contains(@itemprop,'name')]", out var n);
                        var geoAdr = subtitle?.LastOrDefault()?.GetAttributeValue("content", "");
                        if (nodes != null && nodes.Count != 0)
                        {
                            var t = nodes[0].InnerText.Replace("На карте", "").Split(',');
                            page.Adress.GeoSearchString = t[1].Trim() + t[3].Trim() + t[4];
                        }

                        break;
                    }
                    case ParsingPageType.Yandex:
                    {
                        page.HtmlPage = page.HtmlPage.Replace("&quot;", "\"");
                        if (!string.IsNullOrEmpty(firstNodeInnerHtml))
                        {
                            page.Adress.GeoSearchString = firstNodeInnerHtml;
                        }
                        else
                        {
                            var split = page.HtmlPage.Split("geocoderAddress");

                            if (split.Count == 1)
                            {
                                var collection = page.TrySelectNodes(null,
                                    "//h2[contains(@class,'offer-card__address') or contains(@class,'offer-card-address')]",
                                    out var s);
                                page.Adress.GeoSearchString = collection?.FirstOrDefault()?.InnerText;

                                if (!string.IsNullOrEmpty(page.Adress.GeoSearchString)) break;

                                var part = page.HtmlPage.Substring(
                                    page.HtmlPage.IndexOf("<div class=\"OfferHeader__address\">", StringComparison.Ordinal)+ "<div class=\"OfferHeader__address\">".Length);
                                page.Adress.GeoSearchString = part.Substring(0,part.IndexOf("</", StringComparison.Ordinal));
                                if (string.IsNullOrEmpty(page.Adress.GeoSearchString))
                                    // нет в странице таких полей
                                    Log.Error("нет данный о GeoSearchString (даже адрес не разобран)" + page.Url,
                                        true);
                                }
                            else
                            {
                                page.Adress.GeoSearchString = split.LastOrDefault()?.Replace("\":\"")?.Split('"')
                                    .FirstOrDefault();
                            }

                        }

                        break;
                    }
                    case ParsingPageType.N1:
                    {
                        var indexOf = string.IsNullOrEmpty(firstNodeInnerHtml) ? -1 : firstNodeInnerHtml.IndexOf(',');
                        if (indexOf != -1)
                        {
                            if (string.IsNullOrEmpty(page.Adress.City))
                                page.Adress.City = CityManager.GetCityNameByUrl(page.Url);
                            var sub = firstNodeInnerHtml.Substring(indexOf + 1, firstNodeInnerHtml.Length - indexOf - 1)
                                ?.Trim();

                            if (!string.IsNullOrEmpty(page.Adress.City)
                                && !sub.ToLower().Contains(page.Adress.City.ToLower()))
                                page.Adress.GeoSearchString = page.Adress.City + ", " + sub;
                            else
                                page.Adress.GeoSearchString = sub;
                        }

                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                if (string.IsNullOrEmpty(page.Adress.GeoSearchString))
                {
                    if (string.IsNullOrEmpty(page.Adress.Street))
                        Log.Warn("[" + page.Type + "] Не определен GeoSearchString: " + page.Url);
                    else
                        page.Adress.GeoSearchString = page.Adress.Street;
                }


                // Во время разбора координат, если потредуется - определится
                //if(String.IsNullOrEmpty(page.Adress.City))
                //page.Adress.City = CityManager.GetCityNameByUrl(page.Url);
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка парсинга параметра: " + PageField + " " + page.Url);
                Log.Exception(ex, LogType.Error, true);
            }
        }

    }
}

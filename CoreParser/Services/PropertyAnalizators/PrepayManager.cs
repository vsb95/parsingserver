﻿using System;
using Core.Enum;
using Core.Extension;
using CoreParser.Enum;
using CoreParser.Interface;
using CoreParser.Pages;
using Logger;

namespace CoreParser.Services.PropertyAnalizators
{
    internal class PrepayManager : IAdvertismentPropertyAnalizator
    {
        public PageField PageField => PageField.Prepay;


        public void Identify(AbstractAdvertisement page)
        {
            if(page.RentType == RentType.Sell)
                return;
            if (!DictionaryStorage.PageParsingSettings.TryGetValue(page.Type, out var selectorDictionary))
                throw new Exception("Неизвестный тип страницы: " + page.Type);
            if (!selectorDictionary.TryGetValue(PageField, out var selector))
                throw new Exception("Не указан селектор [" + PageField + "] для страницы типа: " + page.Type);
            try
            {
                var nodes = page.TrySelectNodes(PageField, selector, out var isShouldContinue);

                if (nodes == null && !isShouldContinue)
                    return;

                var firstNodeInnerHtml = nodes != null && nodes.Count > 0
                    ? nodes[0].InnerHtml.Replace("&nbsp;", " ").Replace("\n", "").Trim()
                    : "";

                string prepayStr = null;
                switch (page.Type)
                {
                    case ParsingPageType.Avito:
                    {
                        if (nodes != null && nodes.Count != 0)
                        {
                            var strings = firstNodeInnerHtml.FindDigits(true);
                            for (var i = 1; i < strings.Count; i++)
                            {
                                if (!strings[i - 1].Contains("залог")) continue;
                                if(strings[i - 1].Contains("без залога"))
                                {
                                    prepayStr = "0";
                                    break;
                                }
                                prepayStr = strings[i];
                                break;
                            }
                        }

                        break;
                    }
                    case ParsingPageType.Mlsn:
                    {
                        if (nodes != null && nodes.Count > 11)
                        {
                            //1-комнатная квартира, 31 м² ул Ишимская, 26 6 000 ₽/мес.+ КУ (3 000 ₽) + оплата эл. энергии + депозит
                            if (nodes[10].InnerText.Contains("+ депозит"))
                            {
                                var t = nodes[10].InnerText.Split(' ');
                                prepayStr = t[4];
                            }
                        }

                        break;
                    }
                    case ParsingPageType.Cian:
                    {
                        if (nodes != null && nodes.Count != 0)
                        {
                            var innerText = nodes[0].InnerText?.Replace("&nbsp;"," ")?.ToLower();
                            if (innerText.Contains("без залога") || !innerText.Contains("залог"))
                                break;
                            var chunks = innerText.Split(',');
                            foreach (var chunk in chunks)
                            {
                                if (!chunk.Contains("залог")) continue;
                                prepayStr = chunk.Replace("залог", "").Replace("₽", "").Replace(" ", "");
                                if (prepayStr == " без а")
                                    prepayStr = null;
                                break;
                            }
                        }
                            break;
                    }
                    case ParsingPageType.Yandex:
                    {
                        if (!string.IsNullOrEmpty(firstNodeInnerHtml))
                        {
                            if (firstNodeInnerHtml.Contains("без залога"))
                                break;

                            if (firstNodeInnerHtml.Contains("залог,"))
                            {
                                var strings = page.Description.FindDigits(true);
                                for (var i = 1; i < strings.Count; i++)
                                {
                                    if (!strings[i - 1].Contains("залог")) continue;
                                    prepayStr = strings[i];
                                    break;
                                }
                            }
                            
                        }

                        break;
                    }
                    case ParsingPageType.N1:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                if (decimal.TryParse(prepayStr, out var prepay))
                    page.Prepay = prepay;
                else if (!string.IsNullOrEmpty(prepayStr))
                    Log.Error("[" + page.Type + "]Не удалось распознать депозит: " + page.Url);
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка парсинга параметра: " + PageField + " " + page.Url);
                Log.Exception(ex, LogType.Error, true);
            }
        }

    }
}

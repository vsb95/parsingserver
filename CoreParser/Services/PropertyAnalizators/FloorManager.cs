﻿using System;
using System.Linq;
using Core.Enum;
using Core.Extension;
using CoreParser.Enum;
using CoreParser.Interface;
using CoreParser.Pages;
using Logger;

namespace CoreParser.Services.PropertyAnalizators
{
    internal class FloorManager : IAdvertismentPropertyAnalizator
    {
        public PageField PageField => PageField.Floor;


        public void Identify(AbstractAdvertisement page)
        {
            if (!DictionaryStorage.PageParsingSettings.TryGetValue(page.Type, out var selectorDictionary))
                throw new Exception("Неизвестный тип страницы: " + page.Type);
            if (!selectorDictionary.TryGetValue(PageField, out var selector))
                throw new Exception("Не указан селектор [" + PageField + "] для страницы типа: " + page.Type);
            try
            {
                var nodes = page.TrySelectNodes(PageField, selector, out var isShouldContinue);

                if (nodes == null && !isShouldContinue)
                    return;

                var firstNodeInnerHtml = nodes != null && nodes.Count > 0
                    ? nodes[0].InnerHtml.Replace("&nbsp;", " ").Replace("\n", "").Trim()
                    : "";

                string floorStr = null;
                switch (page.Type)
                {
                    case ParsingPageType.Avito:
                    {
                        foreach (var node in nodes)
                        {
                            var row = node.InnerText.Replace("&nbsp;", " ").Replace("\n \n ", "\n");
                            if (string.IsNullOrEmpty(row.Trim()))
                                continue;
                            var chunks = row.Trim().Split(':');
                            if (chunks.Length < 2)
                                break;
                            if (chunks[0] != "Этаж") continue;
                            floorStr = chunks[1].Trim();
                            break;
                        }
                        break;
                    }
                    case ParsingPageType.Mlsn:
                    {
                        foreach (var node in nodes)
                        {
                            if (!node.ParentNode.InnerText.Contains("Этаж")) continue;
                            floorStr = node.InnerText.Split('/').FirstOrDefault();
                        }

                        break;
                    }
                    case ParsingPageType.Cian:
                    {
                            foreach (var node in nodes)
                            {
                                if (!node.InnerText.Contains("Этаж")) continue;
                                floorStr = node.InnerText.Replace("Этаж", "");
                                break;
                            }

                        break;
                    }
                    case ParsingPageType.Yandex:
                    {
                        foreach (var node in nodes)
                        {
                            var text = node.InnerText;
                            if(!text.Contains("этаж")) continue;
                            floorStr = text
                                .Split(new string[] { " из " }, StringSplitOptions.RemoveEmptyEntries)
                                .FirstOrDefault()?.Replace("этаж").Trim();
                                break;
                        }

                        break;
                    }
                    case ParsingPageType.N1:
                    {
                        if (nodes != null && nodes.Count == 1)
                        {
                            var floorWithMaxFloor = nodes[0].InnerText.Split(" из ");
                            floorStr = floorWithMaxFloor.FirstOrDefault();
                        }

                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                if (int.TryParse(floorStr, out var floor))
                    page.Floor = floor;
                else if ((page.Floor == null || page.Floor == 0)
                         && page.ObjType.Type != EnumObjType.Dacha
                         && page.ObjType.Type != EnumObjType.Commercial
                         && page.ObjType.Type != EnumObjType.Dom
                         && page.ObjType.Type != EnumObjType.Kottedj
                         && page.ObjType.Type != EnumObjType.Uchastok)
                    Log.Warn("[" + page.Type + "] Не удалось распознать этаж: " + page.Url);

            }
            catch (Exception ex)
            {
                Log.Error("Ошибка парсинга параметра: " + PageField + " " + page.Url);
                Log.Exception(ex, LogType.Error, true);
            }
        }

    }
}

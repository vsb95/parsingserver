﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.Enum;
using CoreParser.Exceptions;
using CoreParser.Interface;
using CoreParser.Pages;
using CoreParser.Services.Proxies;
using Logger;

namespace CoreParser.Services.Downloaders
{
    public class HttpWebRequestDownloader : IDownloader
    {
        private ParsingPageType _parsingPageType;
        private Proxy _proxy;
        private int _countDdos = 0;

        public int TryDownloadCount { get; set; }

        private string Download(string url, out string referedUrl, CookieContainer cookieContainer,
            WebProxy proxy = null)
        {
            referedUrl = url;
            if (TryDownloadCount >= 10)
                return null;
            try
            {
                var time = new Stopwatch();
                time.Start();

                var request = (HttpWebRequest)WebRequest.Create(url);
                request.UserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:10.0) Gecko/20100101 Firefox/10.0";

                request.CookieContainer = cookieContainer;
                request.Method = "GET";
                request.Timeout = 300000; //300 сек
                if (proxy != null)
                    request.Proxy = proxy;

                TryDownloadCount++;
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    string responseString = null;
                    using (var stream = new StreamReader(response.GetResponseStream()))
                    {
                        responseString = stream.ReadToEnd();
                    }

                    referedUrl = response.ResponseUri.ToString();
                    Log.Trace("\tDownload html through HttpWebRequestDownloader at " + Math.Round(time.Elapsed.TotalSeconds, 3) +
                              " sec;\n\tURL = " + url +
                              ";\n\tProxy = " + proxy?.Address);
                    return responseString;
                }
            }
            catch (IOException)
            {
                return Download(url, out referedUrl, cookieContainer, proxy);
            }
        }

        public string Download(string url, out string referedUrl, ParsingPageType parsingPageType)
        {
            try
            {
                _parsingPageType = parsingPageType;
                _proxy = ProxyManager.GetProxy(parsingPageType, true);

                // Если прокси не пришла - значит у нас либо они протухли, либо все задудосили
                if (_proxy == null)
                {
                    Log.Warn("Для " + parsingPageType + " кончились прокси - будем пробовать через браузер");
                    return new WebBrowserDownloader().Download(url, out referedUrl, parsingPageType);
                }

                var result = Download(url, out referedUrl, CookieManager.GetCookieContainer(_proxy.Ip, parsingPageType), _proxy.ToWebProxy());
                AbstractPage.CheckPage(result);
                return result;
            }
            catch (DdosException ddex)
            {
                _countDdos++;
                if (_countDdos <= ProxyManager.ProxyCount)
                {
                    SendDelayProxy();
                    _proxy = null;
                    Log.Error(ddex.Message, true);
                    return Download(url, out referedUrl, parsingPageType);
                }
                else
                {
                    return new WebBrowserDownloader().Download(url, out referedUrl, parsingPageType);
                }
            }
            catch (WebException wex)
            {
                if (_proxy == null || !_proxy.IsFree)
                    throw;

                Log.Warn("Удаляем бесплатный прокси: " + _proxy.Host + " Потому что: " + wex.Status);
                ProxyManager.ExcludeFreeProxy(_proxy.Ip);
                switch (wex.Status)
                {
                    // Битые бесплатные прокси
                    case WebExceptionStatus.ServerProtocolViolation:
                    case WebExceptionStatus.SecureChannelFailure:
                    case WebExceptionStatus.ProtocolError:
                    case WebExceptionStatus.ConnectFailure:
                    case WebExceptionStatus.SendFailure:
                    case WebExceptionStatus.ReceiveFailure:
                        //if(TryDownloadCount <= 20) TryDownloadCount--;
                        return Download(url, out referedUrl, parsingPageType);
                    default:
                        throw;
                }
            }
        }

        public static int DownloadFile(string remoteFilename, string localFilename)
        {
            // Function will return the number of bytes processed
            // to the caller. Initialize to 0 here.
            int bytesProcessed = 0;

            // Assign values to these objects here so that they can
            // be referenced in the finally block
            Stream remoteStream = null;
            Stream localStream = null;
            WebResponse response = null;

            // Use a try/catch/finally block as both the WebRequest and Stream
            // classes throw exceptions upon error
            try
            {
                // Create a request for the specified remote file name
                WebRequest request = WebRequest.Create(remoteFilename);
                request.Timeout = SettingsManager.IsStartAsAPI ? 60000 : 300000; // 60 сек для апи и 300 для остальных
                // Send the request to the server and retrieve the
                // WebResponse object 
                response = request.GetResponse();

                // Once the WebResponse object has been retrieved,
                // get the stream object associated with the response's data
                remoteStream = response.GetResponseStream();

                // Create the local file
                localStream = File.Create(localFilename);

                // Allocate a 1k buffer
                byte[] buffer = new byte[1024];
                int bytesRead;

                // Simple do/while loop to read from stream until
                // no bytes are returned
                do
                {
                    // Read data (up to 1k) from the stream
                    bytesRead = remoteStream.Read(buffer, 0, buffer.Length);

                    // Write the data to the local file
                    localStream.Write(buffer, 0, bytesRead);

                    // Increment total bytes processed
                    bytesProcessed += bytesRead;
                } while (bytesRead > 0);

            }
            finally
            {
                // Close the response and streams objects here 
                // to make sure they're closed even if an exception
                // is thrown at some point
                response?.Close();
                remoteStream?.Close();
                localStream?.Close();
            }

            // Return total bytes processed to caller.
            return bytesProcessed;
        }

        public void SendDelayProxy()
        {
            if (_proxy == null)
                return;
            try
            {
                var requestUriString = SettingsManager.Settings.ServerSettings.ProxyServiceApiAdress + "api/proxydelete";

                var values = new Dictionary<string, string>
                {
                    {"Ip", _proxy.Ip},
                    {"ParsingPageTypeFor", _parsingPageType.ToString()},
                    {"IsBrowser", "false"}
                };
                using (HttpClient client = new HttpClient())
                {
                    using (var content = new FormUrlEncodedContent(values))
                    {
                        using (var response = client.PostAsync(requestUriString, content).Result)
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "HttpWebRequestDownloader::SendDelayProxy: '" + _proxy?.Ip + "'");
            }
        }
    }

}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Core.Enum;
using CoreParser.Interface;
using CoreParser.Services.Proxies;
using Knyaz.Optimus;
using Knyaz.Optimus.ResourceProviders;

namespace CoreParser.Services.Downloaders
{
    /// <summary>
    /// Пока не использовать - утечка памяти. Ждем обновления библиотеки
    /// </summary>
    public class HeadlessBrowserDownloader : IDownloader
    {
        private static int _createdEngineCount = 0;
        private static ConcurrentQueue<Engine> Engines = new ConcurrentQueue<Engine>();
        //https://bitbucket.org/RusKnyaz/optimus/wiki/Home
        private Proxy _proxy;
        private int _countDdos = 0;
        public int TryDownloadCount { get; set; }
        public string Download(string url, out string referedUrl, ParsingPageType parsingPageType)
        {
            referedUrl = null;
            if (string.IsNullOrEmpty(url))
                return null;
#if !DEBUG
            return null;
#endif
            try
            {
                Engine browser;
                if (!Engines.TryDequeue(out browser))
                {
                    if (ProxyManager.ProxyCount <= _createdEngineCount)
                    {
                        while (!Engines.TryDequeue(out browser))
                        {
                            Thread.Sleep(200);
                        }
                    }
                    else
                    {
                        _proxy = ProxyManager.GetProxy(parsingPageType, false);
                        //initialize resource provider with the proxy
                        var resourceProvider = new ResourceProviderBuilder()
                            .Http(http => http.Proxy(_proxy.ToWebProxy()))
                            .Build();
                        browser = new Engine(resourceProvider);
                        browser.Window.InnerWidth = 1920;
                        browser.Window.InnerHeight = 1080;
                        browser.PreHandleResponse += BrowserOnPreHandleResponse;
                        _createdEngineCount++;
                    }
                }

                _proxy = ProxyManager.GetProxy(parsingPageType, false);
                //initialize resource provider with the proxy
                var resourceProvider2 = new ResourceProviderBuilder()
                    .Http(http => http.Proxy(_proxy.ToWebProxy()))
                    .Build();
                using (browser = new Engine(resourceProvider2))
                {
                    browser.Window.InnerWidth = 1920;
                    browser.Window.InnerHeight = 1080;
                    browser.OpenUrl(url).Wait();
                    referedUrl = browser.Uri.ToString();
                    var result = browser.Document.InnerHTML;
                    browser = null;
                    Engines.Enqueue(browser);
                    return result;
                }


            }
            catch (Exception ex)
            {
                Logger.Log.Exception(ex);
            }

            return null;
        }

        private void BrowserOnPreHandleResponse(object sender, ResponseEventArags responseEventArags)
        {
            if (sender is Engine browser)
            {
                Logger.Log.Debug(browser.Uri +" => "+ responseEventArags.Response.Type);
            }
        }

        public void SendDelayProxy()
        {
            return;
        }
    }
}

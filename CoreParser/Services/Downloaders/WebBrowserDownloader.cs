﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.Enum;
using CoreParser.Exceptions;
using CoreParser.Interface;
using CoreParser.Pages;
using CoreParser.Services.Proxies;
using Logger;

namespace CoreParser.Services.Downloaders
{
    public class WebBrowserDownloader : IDownloader
    {
        private string _proxyIp;
        private ParsingPageType _parsingPageType;
        private int _countDdos = 0;

        public int TryDownloadCount { get; set; }

        private string Download(string url, out string referedUrl)
        {
            return Download(url, out referedUrl, ParsingPageTypeIdentifier.Identify(url));
        }

        public string Download(string url, out string referedUrl, ParsingPageType parsingPageType)
        {
            _parsingPageType = parsingPageType;
            referedUrl = null;
            try
            {
                var html = BrowserManager.Download(url);
                while (!AbstractPage.CheckPage(html))
                {
                    html = BrowserManager.Download(url);
                }
                return html;
            }
            catch (DdosException ddex)
            {
                _countDdos++;
                if (_countDdos <= ProxyManager.ProxyCount)
                {
                    SendDelayProxy();
                    Log.Error(ddex.Message, true);
                    return Download(url, out referedUrl, parsingPageType);
                }

                return null;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true);
                return null;
            }
        }

        public void SendDelayProxy()
        {
            if (string.IsNullOrEmpty(_proxyIp))
            {
#if DEBUG
                Log.Error("У WebBrowserDownloader не установлен _proxyIp при вызове SendDelayProxy", true);
#endif
                return;
            }
            try
            {
                var requestUriString = SettingsManager.Settings.ServerSettings.ProxyServiceApiAdress + "api/proxydelete";

                var values = new Dictionary<string, string>
                {
                    {"Ip", _proxyIp},
                    {"ParsingPageTypeFor", _parsingPageType.ToString()},
                    {"IsBrowser", "1"}
                };
                using (HttpClient client = new HttpClient())
                {
                    using (var content = new FormUrlEncodedContent(values))
                    {
                        using (var response = client.PostAsync(requestUriString, content).Result)
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "HttpWebRequestDownloader::SendDelayProxy: '" + _proxyIp + "'");
            }
        }
    }
}

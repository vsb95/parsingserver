﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using Core.Enum;
using CoreParser.Enum;
using CoreParser.Pages;
using CoreParser.Services.Proxies;
using HtmlAgilityPack;
using Logger;

namespace CoreParser.Services.Captcha
{
    public static class CaptchaManager
    {
        private static bool _isLocked = false;
        private static readonly object Locker = new object();
        private static Random Random = new Random();

        private static bool IsLocked
        {
            get => _isLocked;
            set
            {
                lock (Locker)
                    _isLocked = value;
            }
        }

        private static string _apiKey = "9eac0108f817505477f523bef8136894";
        /* public static bool ResolveCian(string originalUrl, string redirectedUrl, out string html, Proxy proxy, string googlekey)
         {
             html = null;
             if (!Monitor.TryEnter(Locker))
             {
                 Monitor.Wait(Locker);
                 return true;
             }

             try
             {

                 var token = SendRecaptchav2Request(googlekey, redirectedUrl, proxy);
                 if (string.IsNullOrEmpty(token))
                     throw new Exception("Сервис распознавания капчи ответил как то неправильно");
                 redirectedUrl = redirectedUrl.Replace("https", "http");
                 var request = (HttpWebRequest) WebRequest.Create(redirectedUrl);
                 request.Proxy = proxy.ToWebProxy(ParsingPageType.Cian);
                 request.CookieContainer = proxy.GetCookieContainer(ParsingPageType.Cian);//HttpWebRequestDownloader.cookieContainer;
                 foreach (Cookie cookie in request.CookieContainer.GetCookies(new Uri(originalUrl)))
                 {
                     Log.Trace("HttpWebRequestDownloader.cookieContainer => " + cookie.ToString());
                 }
                 var postData = "redirect_url=" + originalUrl.Split('&')[0] + "&g-recaptcha-response=" +
                                WaitResolveCaptcha(token);
                 var data = Encoding.ASCII.GetBytes(postData);
                 request.Method = "POST";
                 request.ContentLength = data.Length;
                 using (var stream = request.GetRequestStream())
                     stream.Write(data, 0, data.Length);
                 //request.AllowAutoRedirect = false;

                 using (var response = (HttpWebResponse) request.GetResponse())
                 {
                     //request.FixCookies(response);
                     var responseString = new StreamReader(response.GetResponseStream() ?? throw new InvalidOperationException()).ReadToEnd();
                     foreach (string responseHeader in response.Headers)
                     {
                         var value = response.Headers[responseHeader];
                         Log.Trace(responseHeader+": "+ value);
                     }

                     Log.Trace("\n");
                     foreach (Cookie cookie in response.Cookies)
                     {
                         Log.Trace("response.Cookies => " + cookie.ToString());
                         //HttpWebRequestDownloader.SetCookie(cookie, new Uri(originalUrl));
                     }
                     foreach (Cookie cookie in request.CookieContainer.GetCookies(new Uri(originalUrl)))
                     {
                         Log.Trace("request.CookieContainer => "+cookie.ToString());
                         //HttpWebRequestDownloader.SetCookie(cookie, new Uri(originalUrl));
                     }

                     if (AbstractPage.CheckPage(responseString))
                     {
                         html = responseString;
                         return true;
                     }
                 }
             }
             catch (Exception ex)
             {
                 Log.Exception(ex, LogType.Error, true);
             }
             finally
             {
                 Monitor.PulseAll(Locker);
                 Monitor.Exit(Locker);
             }
             return false;
         }
         */

        public static bool ResolveYandex(string originalUrl, string redirectedUrl, string html, Proxy proxy, out string resultHTML)
        {
            while (IsLocked)
            {
                Thread.Sleep(250 + Random.Next(1500));
            }

            IsLocked = true;
            resultHTML = null;
            var doc = new HtmlDocument();
            doc.LoadHtml(html);
            #region Определим картинку
            var nodes = doc.DocumentNode.SelectNodes("//img[@class='image form__captcha']");
            if (nodes.Count != 1)
            {
                throw new Exception("чета не так с картинками их не одна (мб нет вообще)");
            }
            var node = nodes.FirstOrDefault();
            var src = node?.GetAttributeValue("src", "");
            if (string.IsNullOrEmpty(src))
                throw new Exception("чета не так с картинкой - не найден src");
            #endregion

            #region Определим данные формы
            string formKey = null;
            string formRetpath = null;

            nodes = doc.DocumentNode.SelectNodes("//input[@name='key']");
            if (nodes.Count != 1)
            {
                throw new Exception("чета не так с формой капчи");
            }
            node = nodes.FirstOrDefault();
            formKey = node?.GetAttributeValue("value", "");
            if (string.IsNullOrEmpty(formKey))
                throw new Exception("чета не так с формой капчи - не найден formKey");
            //
            nodes = doc.DocumentNode.SelectNodes("//input[@name='retpath']");
            if (nodes.Count != 1)
            {
                throw new Exception("чета не так с формой капчи");
            }
            node = nodes.FirstOrDefault();
            formRetpath = node?.GetAttributeValue("value", "");
            if (string.IsNullOrEmpty(formRetpath))
                throw new Exception("чета не так с формой капчи - не найден formRetpath");


            #endregion

            /*
            if (!Monitor.TryEnter(Locker))
            {
                Monitor.Wait(Locker);
                return true;
            }
            */
            try
            {
                // Скачаем картинку для распознавания
                using (var webClient = new WebClient())
                {
                    webClient.DownloadFile(src, "ya.captcha.jpg");
                }
                #region Распознаем капчу
                var capthcaclient = new RuCaptchaClient(_apiKey);
                var config = new CaptchaConfig();
                config.SetCharType(CaptchaCharTypeEnum.Any);
                config.SetIsPhrase(true);
                var captchaId = capthcaclient.UploadCaptchaFile("ya.captcha.jpg", config);
                string captchaResolveText = null;
                while (true)
                {
                    Thread.Sleep(5000);
                    try
                    {
                        captchaResolveText = capthcaclient.GetCaptcha(captchaId);
                        break;
                    }
                    catch (RuCaptchaException e)
                    {
                        if (e.ErrorKey != "CAPCHA_NOT_READY")
                        {
                            Log.Exception(e, LogType.Fatal, true);
                            return false;
                        }
                        Log.Warn(e.Message);
                    }
                    catch (Exception e)
                    {
                        Log.Exception(e, LogType.Fatal, true);
                    }
                }
                #endregion

                #region Отправим форму с расшифровкой
                var postData = "?key=" + formKey + "&retpath=" + HttpUtility.UrlEncode(formRetpath) + "&rep=" + captchaResolveText;
                postData = postData.Replace(" ", "+");
                var url = "https://realty.yandex.ru/checkcaptcha" + postData;
                var request = (HttpWebRequest) WebRequest.Create(url);
                request.Proxy = proxy.ToWebProxy();
                request.CookieContainer = CookieManager.GetCookieContainer(proxy.Ip, ParsingPageType.Yandex);
                request.Method = "GET";

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    var responseString = new StreamReader(response.GetResponseStream() ?? throw new InvalidOperationException()).ReadToEnd();
                    foreach (string responseHeader in response.Headers)
                    {
                        var value = response.Headers[responseHeader];
                        Log.Trace(responseHeader + ": " + value);
                    }

                    Log.Trace("\n");
                    foreach (Cookie cookie in response.Cookies)
                    {
                        Log.Trace("response.Cookies => " + cookie.ToString());
                        //HttpWebRequestDownloader.SetCookie(cookie, new Uri(originalUrl));
                    }
                    foreach (Cookie cookie in request.CookieContainer.GetCookies(new Uri(originalUrl)))
                    {
                        Log.Trace("request.CookieContainer => " + cookie.ToString());
                        //HttpWebRequestDownloader.SetCookie(cookie, new Uri(originalUrl));
                    }

                    if (AbstractPage.CheckPage(responseString))
                    {
                        Log.Info("Yandex Капчу разгадали: "+ proxy.Host);
                        resultHTML = responseString;
                        return true;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true);
            }
            finally
            {
                //Monitor.PulseAll(Locker);
                //Monitor.Exit(Locker);
                IsLocked = false;
            }
            return false;
        }

        private static string WaitResolveCaptcha(string token)
        {
            Log.Trace("Ожидаем пока нам разберут капчу...");
            var watch = new Stopwatch();
            watch.Start();
            using (var client = new WebClient())
            {
                while (true)
                {
                    Thread.Sleep(5000);
                    try
                    {
                        var url = "http://2captcha.com/res.php?key=" + _apiKey + "&action=get&id=" + token;
                        var result = client.DownloadString(url);
                        if (result.Contains("CAPCHA_NOT_READY")) continue;
                        Log.Trace("Ожидание капчи составило: " + Math.Round(watch.Elapsed.TotalSeconds, 2) + " сек");
                        return result.Replace("OK|", "");
                    }
                    catch (Exception e)
                    {
                        Log.Exception(e, LogType.Error, true);
                    }
                }
            }
            
        }

        /// <summary>
        /// Получить id запроса на расшифровку
        /// </summary>
        /// <param name="googlekey">key из капчи</param>
        /// <param name="pageurl">страница, на которой решается капча</param>
        /// <returns></returns>
        private static string SendRecaptchav2Request(string googlekey, string pageurl, Proxy proxy)
        {
            string responseString = null;
            try
            {
                System.Net.ServicePointManager.Expect100Continue = false;
                var request = (HttpWebRequest)WebRequest.Create("http://2captcha.com/in.php");

                var postData = "key="+ _apiKey +"&method=userrecaptcha&googlekey="+ googlekey+ "&pageurl="+ pageurl;
                if(proxy!= null)
                {
                    postData += "&proxytype=" + proxy.Type + "&proxy=" + String.Format("{0}:{1}@{2}:{3}",
                                    proxy.UserLogin, proxy.UserPassword, proxy.Ip, proxy.Port);
                }
                var data = Encoding.ASCII.GetBytes(postData);

                request.Method = "POST";

                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                //  GET
                if (responseString.Contains("OK|"))
                {
                    return responseString.Replace("OK|", "");
                }
            }
            catch (Exception e)
            {
                Log.Exception(e, LogType.Error, true);
            }
            throw new Exception("Ответ от рукапчи: "+responseString);
        }
    }
}

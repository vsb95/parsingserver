﻿namespace CoreParser.Services.Captcha
{
    /// <summary>
    /// Язык
    /// </summary>
    internal enum CaptchaLanguageEnum
    {
        /// <summary>
        /// По-умолчанию
        /// </summary>
        Default = 0,
        /// <summary>
        /// Русский
        /// </summary>
        Russian = 1,
        /// <summary>
        /// Английский
        /// </summary>
        English = 2
    }
}

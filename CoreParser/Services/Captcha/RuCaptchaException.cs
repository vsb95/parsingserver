﻿using System;

namespace CoreParser.Services.Captcha
{
    public class RuCaptchaException : Exception
    {
        public string ErrorKey { get; set; }
        public RuCaptchaException()
        {

        }
        public RuCaptchaException(string message)
            : base(message)
        {

        }
        public RuCaptchaException(string message, Exception inner)
            : base(message, inner)
        {

        }

    }
}

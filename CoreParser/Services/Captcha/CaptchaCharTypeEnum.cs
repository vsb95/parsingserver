﻿namespace CoreParser.Services.Captcha
{
    /// <summary>
    /// Из чего состоит капча
    /// </summary>
    internal enum CaptchaCharTypeEnum
    {
        /// <summary>
        /// По-умолчанию
        /// </summary>
        Default = 0,
        /// <summary>
        /// Капча состоит только из цифр
        /// </summary>
        OnlyDigits = 1,
        /// <summary>
        /// капча состоит только из букв
        /// </summary>
        OnlyLetter = 2,
        /// <summary>
        /// Капча состоит либо только из цифр, либо только из букв
        /// </summary>
        OnlyDigitsOrOnlyLetter = 3,

        /// <summary>
        /// в капче могут быть и буквы, и цифры
        /// </summary>
        Any = 4
    }
}

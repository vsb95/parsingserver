﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using Core.Enum;
using Core.Extension;
using Core.Services;
using CoreParser.Enum;
using CoreParser.Exceptions;
using CoreParser.Interface;
using CoreParser.Pages;
using CoreParser.Services.Downloaders;
using CoreParser.Services.Proxies;
using HtmlAgilityPack;
using Logger;

namespace CoreParser.Services
{
    public static class CityManager
    {
        public static CitySourceItem SelectedCity { get; set; }
        public static readonly List<CitySourceItem> CitiesDictionary = new List<CitySourceItem>();

        public static readonly Dictionary<ParsingPageType, Queue<string>> RotateCategoriesUrlQueue =
            new Dictionary<ParsingPageType, Queue<string>>();

        /// <summary>
        /// Список сгенерированных ссылок на доски
        /// </summary>
        public static readonly ConcurrentDictionary<ParsingPageType, List<string>> GeneratedUrlList =
            new ConcurrentDictionary<ParsingPageType, List<string>>();
        static CityManager()
        {
            var queue = new Queue<string>();
            queue.Enqueue("/doma_dachi_kottedzhi/sdam/na_dlitelnyy_srok?s=104&user=1&view=list&p={0}");
            queue.Enqueue("/komnaty/sdam/na_dlitelnyy_srok?s=104&user=1&view=list&p={0}");
            queue.Enqueue("/kvartiry/sdam/na_dlitelnyy_srok?s=104&user=1&view=list&p={0}");
            queue.Enqueue("/doma_dachi_kottedzhi/prodam?s=104&user=1&view=list&p={0}");
            queue.Enqueue("/komnaty/prodam?s=104&user=1&view=list&p={0}");
            queue.Enqueue("/kvartiry/prodam?s=104&user=1&view=list&p={0}");

            queue.Enqueue("/doma_dachi_kottedzhi/sdam/na_dlitelnyy_srok?s=104&user=2&view=list&p={0}");
            queue.Enqueue("/komnaty/sdam/na_dlitelnyy_srok?s=104&user=2&view=list&p={0}");
            queue.Enqueue("/kvartiry/sdam/na_dlitelnyy_srok?s=104&user=2&view=list&p={0}");
            queue.Enqueue("/doma_dachi_kottedzhi/prodam?s=104&user=2&view=list&p={0}");
            queue.Enqueue("/komnaty/prodam?s=104&user=2&view=list&p={0}");
            queue.Enqueue("/kvartiry/prodam?s=104&user=2&view=list&p={0}");
            RotateCategoriesUrlQueue.Add(ParsingPageType.Avito, queue);
            /*
            queue = new Queue<string>();
            queue.Enqueue("/snyat/dolgosrochno/kvartiry/?limit=100&author=owner&sort=-date&page={0}");
            queue.Enqueue("/snyat/dolgosrochno/komnaty/?limit=100&author=owner&sort=-date&page={0}");
            queue.Enqueue("/snyat/dolgosrochno/doma/?limit=100&author=owner&sort=-date&page={0}");
            queue.Enqueue("/kupit/kvartiry/?limit=100&author=owner&sort=-date&page={0}");
            queue.Enqueue("/kupit/komnaty/?limit=100&author=owner&sort=-date&page={0}");
            queue.Enqueue("/kupit/doma/?limit=100&author=owner&sort=-date&page={0}");
            queue.Enqueue("/snyat/kommercheskaya/?limit=100&author=owner&sort=-date&page={0}");
            queue.Enqueue("/kupit/kommercheskaya/?limit=100&author=owner&sort=-date&page={0}");
            queue.Enqueue("/kupit/zemlya/?limit=100&author=owner&sort=-date&page={0}");
            queue.Enqueue("/snyat/zemlya/?limit=100&author=owner&sort=-date&page={0}");
            queue.Enqueue("/kupit/dachi/?limit=100&author=owner&sort=-date&page={0}");
            queue.Enqueue("/snyat/dachi/?limit=100&author=owner&sort=-date&page={0}");

            queue.Enqueue("/snyat/dolgosrochno/kvartiry/?limit=100&sort=-date&page={0}");
            queue.Enqueue("/snyat/dolgosrochno/komnaty/?limit=100&sort=-date&page={0}");
            queue.Enqueue("/snyat/dolgosrochno/doma/?limit=100&sort=-date&page={0}");
            queue.Enqueue("/kupit/kvartiry/?limit=100&sort=-date&page={0}");
            queue.Enqueue("/kupit/komnaty/?limit=100&sort=-date&page={0}");
            queue.Enqueue("/kupit/doma/?limit=100&sort=-date&page={0}");
            queue.Enqueue("/snyat/kommercheskaya/?limit=100&sort=-date&page={0}");
            queue.Enqueue("/kupit/kommercheskaya/?limit=100&sort=-date&page={0}");
            queue.Enqueue("/kupit/zemlya/?limit=100&sort=-date&page={0}");
            queue.Enqueue("/snyat/zemlya/?limit=100&sort=-date&page={0}");
            queue.Enqueue("/kupit/dachi/?limit=100&sort=-date&page={0}");
            queue.Enqueue("/snyat/dachi/?limit=100&sort=-date&page={0}");
            RotateCategoriesUrlQueue.Add(ParsingPageType.N1, queue);
            */
            queue = new Queue<string>();
            queue.Enqueue("/pokupka-nedvizhimost/?sort=-inserted&viewMode=paper&onlyOwners=1&page={0}");
            queue.Enqueue("/pokupka-dachi-i-uchastki/?sort=-inserted&viewMode=paper&onlyOwners=1&page={0}");
            queue.Enqueue("/pokupka-kommercheskaja-nedvizhimost/?sort=-inserted&viewMode=paper&onlyOwners=1&page={0}");
            queue.Enqueue("/arenda-nedvizhimost/?sort=-inserted&viewMode=paper&onlyOwners=1&page={0}");
            queue.Enqueue("/arenda-kommercheskaja-nedvizhimost/?sort=-inserted&viewMode=paper&onlyOwners=1&page={0}");

            queue.Enqueue("/pokupka-nedvizhimost/?sort=-inserted&viewMode=paper&page={0}");
            queue.Enqueue("/pokupka-dachi-i-uchastki/?sort=-inserted&viewMode=paper&page={0}");
            queue.Enqueue("/pokupka-kommercheskaja-nedvizhimost/?sort=-inserted&viewMode=paper&page={0}");
            queue.Enqueue("/arenda-nedvizhimost/?sort=-inserted&viewMode=paper&page={0}");
            queue.Enqueue("/arenda-kommercheskaja-nedvizhimost/?sort=-inserted&viewMode=paper&page={0}");
            RotateCategoriesUrlQueue.Add(ParsingPageType.Mlsn, queue);


            queue = new Queue<string>();
            queue.Enqueue("/snyat/kvartira/?sort=DATE_DESC&hasAgentFee=NO&page={0}");
            queue.Enqueue("/snyat/komnata/?sort=DATE_DESC&hasAgentFee=NO&page={0}");
            queue.Enqueue("/snyat/dom/?sort=DATE_DESC&hasAgentFee=NO&houseType=TOWNHOUSE&houseType=HOUSE&page={0}");
            queue.Enqueue("/kupit/kvartira/?sort=DATE_DESC&agents=NO&page={0}");
            queue.Enqueue("/kupit/komnata/?sort=DATE_DESC&agents=NO&page={0}");
            queue.Enqueue("/kupit/dom/?sort=DATE_DESC&agents=NO&houseType=TOWNHOUSE&houseType=HOUSE&page={0}");
            queue.Enqueue("/kupit/uchastok/?sort=DATE_DESC&agents=NO&page={0}");

            queue.Enqueue("/snyat/kvartira/?sort=DATE_DESC&page={0}");
            queue.Enqueue("/snyat/komnata/?sort=DATE_DESC&page={0}");
            queue.Enqueue("/snyat/dom/?sort=DATE_DESC&houseType=TOWNHOUSE&houseType=HOUSE&page={0}");
            queue.Enqueue("/kupit/kvartira/?sort=DATE_DESC&page={0}");
            queue.Enqueue("/kupit/komnata/?sort=DATE_DESC&page={0}");
            queue.Enqueue("/kupit/dom/?sort=DATE_DESC&houseType=TOWNHOUSE&houseType=HOUSE&page={0}");
            queue.Enqueue("/kupit/uchastok/?sort=DATE_DESC&page={0}");
            RotateCategoriesUrlQueue.Add(ParsingPageType.Yandex, queue);

            queue = new Queue<string>();
            queue.Enqueue("/cat.php?deal_type=rent&engine_version=2&offer_type=flat&p={0}&region=4914&room1=1&type=4");
            RotateCategoriesUrlQueue.Add(ParsingPageType.Cian, queue);

            var omsk = new CitySourceItem
            {
                Id = 0,
                Name = "Омск",
                DomainDictionary = new Dictionary<ParsingPageType, string>()
                {
                    {ParsingPageType.Avito, "://www.avito.ru/omsk"},
                    {ParsingPageType.Mlsn, "://omsk.mlsn.ru"},
                    {ParsingPageType.Cian, "://omsk.cian.ru"},
                    {ParsingPageType.Yandex, "://realty.yandex.ru/omsk"},
                    {ParsingPageType.N1, "://omsk.n1.ru"}
                }
            };
            var novosib = new CitySourceItem
            {
                Id = 1,
                Name = "Новосибирск",
                DomainDictionary = new Dictionary<ParsingPageType, string>()
                {
                    {ParsingPageType.Avito, "://www.avito.ru/novosibirsk"},
                    {ParsingPageType.Mlsn, "://nsk.mlsn.ru"},
                    {ParsingPageType.Cian, "://novosibirsk.cian.ru"},
                    {ParsingPageType.Yandex, "://realty.yandex.ru/novosibirsk"},
                    {ParsingPageType.N1, "://novosibirsk.n1.ru"}
                }
            };

            CitiesDictionary.Add(omsk);
            CitiesDictionary.Add(novosib);

            CitiesDictionary.Sort();

            GeneratedUrlList.TryAdd(ParsingPageType.Avito, new List<string>());
            GeneratedUrlList.TryAdd(ParsingPageType.Mlsn, new List<string>());
            GeneratedUrlList.TryAdd(ParsingPageType.Cian, new List<string>());
            GeneratedUrlList.TryAdd(ParsingPageType.Yandex, new List<string>());
            GeneratedUrlList.TryAdd(ParsingPageType.N1, new List<string>());
        }

        /// <summary>
        /// Выбрать город через интерфейс в консоле. Блокирует поток до ввода
        /// </summary>
        public static CitySourceItem HandSelectCity()
        {
            int idCity = 0;
            while (true)
            {
                Console.WriteLine("Выберите город: ");
                foreach (var item in CityManager.CitiesDictionary)
                {
                    Console.WriteLine(item.ToString());
                }
                var input = Console.ReadLine();
                if (!Int32.TryParse(input, out idCity))
                    continue;
                return CityManager.SelectCity(idCity);
            }
        }

        public static CitySourceItem SelectCity(int id)
        {
            SelectedCity = CitiesDictionary.FirstOrDefault(x => x.Id == id);
            if(SelectedCity == null)
                throw new ArgumentOutOfRangeException(nameof(id));
            Log.Info("Выбран город: "+ SelectedCity.Name);
            return SelectedCity;
        }

        public static ConcurrentQueue<string> GetAllUrlQueue(ParsingPageType parsingPageType, int _maxPageCount = 9999, string args=null, IDownloader downloader = null)
        {
            if (SelectedCity == null)
                throw new Exception("Не выбран город");
            if (!string.IsNullOrEmpty(args) && !args.StartsWith("&"))
            {
                args = "&" + args;
            }
            GeneratedUrlList[parsingPageType].Clear();

            var result = new ConcurrentQueue<string>();
            var domain = SelectedCity.GetDomainForPageType(parsingPageType);
            for (int i = 0; i < RotateCategoriesUrlQueue[parsingPageType].Count; i++)
            {
                var rotateUrl = RotateCategoriesUrlQueue[parsingPageType].Dequeue();
                var baseUrl = domain + String.Format(rotateUrl, "").Replace("&page=", "").Replace("?page=", "").Replace("&p=","") + args;
                result.Enqueue(baseUrl);
                int maxIndex = -1;
                while (!TrySelectMaxIndexPage(parsingPageType, baseUrl, out maxIndex, downloader))
                {
                    Thread.Sleep(100);
                }

                maxIndex = Math.Min(_maxPageCount, maxIndex);
                var subUrl = domain + rotateUrl + args;
                Log.Debug("Определено " + subUrl + " = " + maxIndex);
                if(maxIndex == 0)
                    continue;
                for (int j = 2; j <= maxIndex+2; j++)
                {
                    var url = domain + String.Format(rotateUrl, j)+ args;
                    result.Enqueue(url);
                }
                RotateCategoriesUrlQueue[parsingPageType].Enqueue(rotateUrl);
            }

            GeneratedUrlList[parsingPageType].AddRange(result);
            return result;
        }

        /// <summary>
        /// Вернет список подстраниц для шаблона (принимает шаблон полной ссылки + &p={0})
        /// </summary>
        /// <param name="urlTemplate"></param>
        /// <param name="_maxPageCount"></param>
        /// <param name="downloader"></param>
        /// <returns></returns>
        public static List<string> GetAllUrlByTemplate(string urlTemplate, int _maxPageCount = 9999,
            IDownloader downloader = null)
        {
            var result = new List<string>();
            var baseUrl = String.Format(urlTemplate, "").Replace("&page=", "").Replace("?page=", "").Replace("&p=", "");
            result.Add(baseUrl);
            int maxIndex = -1;
            var pageType = ParsingPageTypeIdentifier.Identify(baseUrl);
            while (!TrySelectMaxIndexPage(pageType, baseUrl, out maxIndex, downloader))
            {
                Log.Info("Не удалось определить макс кол-во объектов на странице: '"+ baseUrl + "'. Возможен бесконечный цикл");
                Thread.Sleep(100);
            }

            maxIndex = Math.Min(_maxPageCount, maxIndex);
            for (int j = 1; j <= maxIndex + 1; j++)
            {
                var url = String.Format(urlTemplate, j);
                result.Add(url);
            }

            return result;
        }


        private static bool TrySelectMaxIndexPage(ParsingPageType parsingPageType, string baseUrl, out int maxIndexPage, IDownloader downloader = null)
        {
            maxIndexPage = -1;
            string selector = null;
            switch (parsingPageType)
            {
                case ParsingPageType.Avito:
                    selector = "//a[@class='pagination-page']";
                    break;
                case ParsingPageType.Mlsn:
                    selector = "//span[@class='count'] | //span[@id='totally'] | //span[@class='long-label']";
                    break;
                case ParsingPageType.Cian:
                    selector = "//title";
                    break;
                case ParsingPageType.Yandex:
                    selector = "//div[contains(@class,'FiltersFormField__counter-submit')]";
                    break;
                case ParsingPageType.N1:
                    selector = "//li[@class='breadcrumbs-list__item']";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(parsingPageType), parsingPageType, null);
            }
            if (String.IsNullOrEmpty(selector))
                throw new Exception("Селектор не указан");

            string maxIndex = "";
            try
            {
                if (downloader == null)
                    downloader = DownloaderFactory.CreateDownloader(parsingPageType);
                var html = "";//downloader.Download(baseUrl, out var referer, parsingPageType);
                bool isGoodPage = false;
                int _countDdos = 0;
                string referer = null;
                while (string.IsNullOrEmpty(html) || !isGoodPage)
                {
                    try
                    {
                        html = downloader.Download(baseUrl, out referer, parsingPageType);
                        isGoodPage = AbstractPage.CheckPage(html);
                    }
                    catch (DdosException ddex)
                    {
                        _countDdos++;
                        if (_countDdos <= ProxyManager.ProxyCount)
                        {
                            downloader.SendDelayProxy();
                            Log.Error("[" + parsingPageType + "] " + ddex.Message, true);
                        }
                        else
                        {
                            if (downloader is HttpWebRequestDownloader)
                            {
                                _countDdos = 0;
                                downloader = new WebBrowserDownloader();
                            }
                        }
                    }
                    catch (Exception exx)
                    {
                        Log.Warn("Проблемесы с определением ссылок на доски: "+exx.Message);
                    }
                }


                var doc = new HtmlDocument();
                doc.LoadHtml(html);
                HtmlNodeCollection nodes = doc.DocumentNode.SelectNodes(selector);
                if (nodes == null || nodes.Count == 0)
                {
                    // Если не найден, то значит у нас всего 1 страница
                    if (parsingPageType == ParsingPageType.Avito)
                    {
                        maxIndexPage = 1;
                        return true;
                    }
                    Log.Warn("не найдено ни одной ноды '"+ selector + "', возможен бесконечный цикл");
                    return false;
                }
                var node = nodes.LastOrDefault();
                if (node == null)
                    throw new Exception("Не удалсь найти значение по селектору '" + selector + "' для " + parsingPageType);
                switch (parsingPageType)
                {
                    case ParsingPageType.Avito:
                    {
                        var href = node.GetAttributeValue("href", "");
                        maxIndex = href.Split("p=").LastOrDefault()?.Split('&')?.FirstOrDefault();
                        break;
                    }
                    case ParsingPageType.Mlsn:
                    {
                        var t = node.InnerText.Replace(" объявления", "").Replace(" объявлений", "").Replace(".", "").Replace(",", "")
                            .Replace(" объявление", "").Replace("Найдено: ", "").Replace("Показать", "").Replace("объектов", "").Replace("объекта", "").Replace("объект", "").Trim();
                        if (Int32.TryParse(t, out var totalCount))
                        {
                            maxIndex = (totalCount / 50 + 1).ToString();
                            break;
                        }

                        nodes = doc.DocumentNode.SelectNodes("//div[@class='pagination-wrp']");
                        var srt = nodes.FirstOrDefault()?.ChildNodes;
                        if (srt != null)
                        {
                            var lastNode = srt[srt.Count - 1];
                                var href = lastNode.GetAttributeValue("href", "");
                            maxIndex = href.Split("page=").LastOrDefault()?.Split('&')?.FirstOrDefault();
                        }

                        break;
                    }
                    case ParsingPageType.Cian:
                    {
                        var tr = node.InnerText.IndexOf("объявле");
                        var s = node.InnerText.Substring(0, tr)?.Trim();
                        if (Int32.TryParse(s, out var totalCount))
                        {
                            maxIndexPage = totalCount / 25 + 1; // Перепроверить количество, тк взял наобум
                            return true;
                        }

                        break;
                    }
                    case ParsingPageType.Yandex:
                    {
                        var t = node.InnerText.Replace("Показать ", "").Replace("&nbsp;", "").Replace(" объявления", "")
                            .Replace(" объявлений", "")
                            .Replace(" объявление", "").Trim();
                        if (Int32.TryParse(t, out var totalCount))
                        {
                            maxIndexPage = totalCount / 20 + 1;
                            return true;
                        }

                        if (t == "Найти")
                        {
                            maxIndexPage = 0;
                            return true;
                        }

                        break;
                    }
                    case ParsingPageType.N1:
                    {
                        var t = node.InnerText.Replace(" объявления", "").Replace(" объявлений", "")
                            .Replace(" объявление", "").Trim();
                        if (Int32.TryParse(t, out var totalCount))
                        {
                            maxIndexPage = totalCount / 100 + 1;
                            return true;
                        }

                        break;
                    }
                }

                // ReSharper disable once AssignNullToNotNullAttribute
                maxIndexPage = Int32.Parse(maxIndex);

                //Если нам отобразили на старой версии Mlsn
                if (parsingPageType == ParsingPageType.Mlsn && maxIndexPage > 80
                    && baseUrl.Contains("pokupka-nedvizhimost") && baseUrl.Contains("onlyOwners=1"))
                {
                    while (true)
                    {
                        Thread.Sleep(10000);
                        Log.Fatal("Ждем проверки форсирования на млсн - подозрительно много ссылок");
                    }
                }
                    
                return true;
            }
            catch (WebException wex)
            {
                Log.Warn("WebException "+wex.Message+"\n Url: "+ baseUrl);
                return TrySelectMaxIndexPage(parsingPageType, baseUrl, out maxIndexPage, downloader);
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "maxIndex = " + maxIndex + "\tbaseUrl: " + baseUrl);
            }

            return false;
        }

        /// <summary>
        /// Получить внутренний id города (не в бд)
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static int GetCityIdByUrl(string url)
        {
            foreach (var citySourceItem in CitiesDictionary)
            {
                foreach (var pair in citySourceItem.DomainDictionary)
                {
                    if (url.Contains(pair.Value))
                        return citySourceItem.Id;
                }
            }

            if (url.Contains("yandex.ru"))
                return -1;
            Log.Error("Не удалось определить внутренний id города по url: " + url);
            return -99;
        }

        public static string GetCityNameByUrl(string url)
        {
            if (SelectedCity != null)
                return SelectedCity.Name;
            foreach (var citySourceItem in CitiesDictionary)
            {
                foreach (var pair in citySourceItem.DomainDictionary)
                {
                    if (url.Contains(pair.Value))
                        return citySourceItem.Name;
                }
            }

            Log.Error(
                "Не удалось определить название города по url: " + url +
                " (Это грозит тем, что возможно неправильно сохранится в БД)", true);
            return null;
        }
    }

    public class CitySourceItem:IComparable<CitySourceItem>
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Dictionary<ParsingPageType, string> DomainDictionary { get; set; }

        public string GetDomainForPageType(ParsingPageType parsingPageType)
        {
            if (DomainDictionary.TryGetValue(parsingPageType, out var domain)) return "https" + domain;
            throw new Exception("Не указана ссылка-домен для типа страниц: "+ parsingPageType);
        }

        public override string ToString()
        {
            return Id + " : " + Name;
        }
        public int CompareTo(CitySourceItem other)
        {
            return Id.CompareTo(other.Id);
        }
    }
}

﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace CoreParser.Services.Proxies
{
    public class Proxy
    {
        [JsonProperty("is-free")]
        private bool _isFree;
        public object Locker = new object();
        private static readonly JsonSerializer Deserializer = new JsonSerializer { NullValueHandling = NullValueHandling.Ignore };

        #region Properties
        [JsonProperty("active")]
        private string _active;

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }

        [JsonProperty("ip")]
        public string Ip { get; set; }

        [JsonProperty("host")]
        public string Host { get; set; }

        [JsonProperty("port")]
        public string Port { get; set; }

        [JsonProperty("user")]
        public string UserLogin { get; set; }

        [JsonProperty("pass")]
        public string UserPassword { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("descr")]
        public string Description { get; set; }

        public bool IsActive => _active == "1" || _isFree;

        public bool IsShared => Version == "3" || _isFree;
        public bool IsFree => _isFree;


        private string FileNameCookies => "Cookies\\" + Ip + ".json";
        #endregion

        public Proxy(string json)
        {
            if (string.IsNullOrEmpty(json) || json == "null")
                throw new ArgumentNullException(nameof(json));
            Deserialize(json);
        }

        /// <summary>
        /// Free Proxy
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        public Proxy(string ip, string port)
        {
            Ip = ip;
            Host = ip;
            Port = port;
            Type = "http";
            _isFree = true;
        }

        private void Deserialize(string _json)
        {
            if (string.IsNullOrEmpty(_json)) return;
            if (!string.IsNullOrEmpty(Id))
            {
                int iPos = _json.IndexOf("{\"" + Id + "\":{", StringComparison.Ordinal);
                if (iPos != -1)
                {
                    _json = _json.Substring(iPos + Id.Length + 4);
                    iPos = _json.LastIndexOf("}}", StringComparison.Ordinal);
                    _json = _json.Substring(0, iPos + 1);
                }
            }
            using (StringReader sr = new StringReader(_json))
            {
                Deserializer.Populate(new JsonTextReader(sr), this);
            }
        }

        public WebProxy ToWebProxy()
        {
            if (string.IsNullOrEmpty(Host))
                return null;
            var result = new WebProxy(Type + "://" + Host + ":" + Port);
            if (!string.IsNullOrEmpty(UserLogin) && !string.IsNullOrEmpty(UserPassword))
            {
                result.Credentials = new NetworkCredential(UserLogin, UserPassword);
            }
            return result;
        }

        #region Cookie management
        /*internal void SerializeCookies()
        {
            try
            {
                lock (Locker)
                {
                    var dictionary = new Dictionary<ParsingPageType, List<JsonCookie>>();
                    foreach (var pair in _cookieDictionary)
                    {
                        var list = new List<JsonCookie>();
                        foreach (Cookie cookie in pair.Value.GetAllCookies())
                        {
                            list.Add(new JsonCookie(cookie));
                        }

                        dictionary.Add(pair.Key, list);
                    }

                    var json = JsonConvert.SerializeObject(dictionary);
                    File.WriteAllText(FileNameCookies, json);
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }

        internal void DeserializeCookies()
        {
            try
            {
                if (!File.Exists(FileNameCookies)) return;
                var json = File.ReadAllText(FileNameCookies);
                var dictionary = JsonConvert.DeserializeObject<Dictionary<ParsingPageType, List<JsonCookie>>>(json);

                foreach (var pair in dictionary)
                {
                    var cookieContainer = new CookieContainer();
                    foreach (var jsonCookie in pair.Value)
                    {
                        var cookie = jsonCookie.ToCookie();
                        cookieContainer.Add(cookie);
                    }

                    _cookieDictionary.TryAdd(pair.Key, cookieContainer);
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }
        */
        [JsonObject(Id = "cookie", MemberSerialization = MemberSerialization.OptIn)]
        private class JsonCookie
        {
            #region Properties
            [JsonProperty("domain")]
            public string Domain { get; set; }

            [JsonProperty("comment")]
            public string Comment { get; set; }

            [JsonProperty("commentUri")]
            public Uri CommentUri { get; set; }

            [JsonProperty("discard")]
            public bool Discard { get; set; }

            [JsonProperty("expired")]
            public bool Expired { get; set; }

            [JsonProperty("expires")]
            public DateTime Expires { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("path")]
            public string Path { get; set; }

            [JsonProperty("port")]
            public string Port { get; set; }

            [JsonProperty("secure")]
            public bool Secure { get; set; }

            [JsonProperty("httponly")]
            public bool HttpOnly { get; set; }

            [JsonProperty("value")]
            public string Value { get; set; }

            [JsonProperty("timestamp")]
            public DateTime TimeStamp { get; set; }

            [JsonProperty("version")]
            public int Version { get; set; }
            #endregion

            public JsonCookie(Cookie cookie)
            {
                if (cookie == null)
                    return;

                Value = cookie.Value;
                Comment = cookie.Comment;
                CommentUri = cookie.CommentUri;
                Discard = cookie.Discard;
                Domain = cookie.Domain;
                Expired = cookie.Expired;
                Expires = cookie.Expires;
                HttpOnly = cookie.HttpOnly;
                Name = cookie.Name;
                Path = cookie.Path;
                Port = cookie.Port;
                Secure = cookie.Secure;
                TimeStamp = cookie.TimeStamp;
                Version = cookie.Version;
            }

            public Cookie ToCookie()
            {
                var cookie = new Cookie
                {
                    Value = Value,
                    Comment = Comment,
                    CommentUri = CommentUri,
                    Discard = Discard,
                    Domain = Domain,
                    Expired = Expired,
                    Expires = Expires,
                    HttpOnly = HttpOnly,
                    Name = Name,
                    Path = Path,
                    Port = Port,
                    Secure = Secure,
                    //TimeStamp = TimeStamp,
                    Version = Version
                };


                return cookie;
            }
        }

        #endregion
    }
}

using System;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Threading;
using Core;
using Core.Enum;
using CoreParser.Enum;
using Logger;

namespace CoreParser.Services.Proxies
{
    public static class ProxyManager
    {
        public static bool IsAliveService { get; private set; }
        private static Thread _healthThread;
        public static int ProxyCount { get; private set; }
        
        public static void Init()
        {
            _healthThread = new Thread(HealthCheckLoop) { IsBackground = true };
            _healthThread.Start();
        }
        
        public static Proxy GetProxy(ParsingPageType typeParsingPageFor, bool isCanFree = false)
        {
            var url = SettingsManager.Settings.ServerSettings.ProxyServiceApiAdress + "api/proxy?type=" + typeParsingPageFor;
            if (!SettingsManager.IsStartAsAPI)
            {
                if(SettingsManager.Settings.ServerSettings.IsParsingServerUsingOnlySharedProxy)
                    url += "&isOnlyPrivate=true";
                else
                    url += "&isOnlyPrivate=false";
                if (SettingsManager.Settings.ServerSettings.IsParsingServerCanUseFreeProxy && isCanFree)
                    url += "&isCanFree=true";
                else
                    url += "&isCanFree=false";
            }

            for (int i = 0; !IsAliveService; i++)
            {
                Thread.Sleep(500);

                // через 90 сек оповестим
                if (i > 90)
                {
                    i = 0;
                    Log.Fatal("Ожидаю запуска ProxyService по адресу: " + SettingsManager.Settings.ServerSettings.ProxyServiceApiAdress);
                }
                Log.Warn("Ожидаю запуска ProxyService по адресу: " + SettingsManager.Settings.ServerSettings.ProxyServiceApiAdress);
            }
            using (var web = new WebClient())
            {
                var json = web.DownloadString(url);
                if (string.IsNullOrEmpty(json) || json == "null")
                    return null;
                var proxy = new Proxy(json);
                return proxy;
            }
        }

        private static void HealthCheckLoop()
        {
            while (true)
            {
                try
                {
                    IsAliveService = HealthCheck();
                    if (!IsAliveService)
                    {
                        Process.Start(SettingsManager.Settings.ServerSettings.ProxyServiceExePath);
                        Thread.Sleep(5000);

                        IsAliveService = HealthCheck();
                        if (!IsAliveService)
                        {
                            Log.Fatal(
                                "Смертельно опух/не запущен прокси сервис на '" +
                                SettingsManager.Settings.ServerSettings.ProxyServiceApiAdress + "'");

                            Thread.Sleep(15 * 1000);
                            continue;
                        }
                    }

                    using (var client = new WebClient())
                    {
                        var result = client.UploadData(SettingsManager.Settings.ServerSettings.ProxyServiceApiAdress + "api/proxy", "POST", new byte[0]);
                        var t = Encoding.UTF8.GetString(result);
                        if (int.TryParse(t, out var count))
                            ProxyCount = count;
                    }

                    Thread.Sleep(15 * 1000);
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, LogType.Fatal, true, "Прокси сервис отвалился");
                }
            }
        }

        private static bool HealthCheck()
        {
            try
            {
                using (var client = new WebClient())
                {
                    var result = client.DownloadString(SettingsManager.Settings.ServerSettings.ProxyServiceApiAdress + "api/check");

                    if (!string.IsNullOrEmpty(result) && result == "\"1\"")
                        return true;
                }
            }
            catch (WebException wex)
            {
                if (wex.Status == WebExceptionStatus.ConnectFailure)
                    return false;
                if (wex.InnerException != null)
                    Log.Exception(wex.InnerException, LogType.Error, true);
                Log.Exception(wex, LogType.Fatal, true);
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

            return false;
        }

        public static void ExcludeFreeProxy(string proxyIp)
        {
            if(string.IsNullOrEmpty(proxyIp))
                return;
            try
            {
                var url = SettingsManager.Settings.ServerSettings.ProxyServiceApiAdress + "api/proxydelete?ip="+ proxyIp;
                using (var web = new WebClient())
                {
                    web.DownloadStringAsync(new Uri(url));
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true);
            }
        }
    }
}

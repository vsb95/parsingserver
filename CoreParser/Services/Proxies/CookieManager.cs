﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Core.Enum;
using CoreParser.Enum;
using Logger;
using Newtonsoft.Json;

namespace CoreParser.Services.Proxies
{
    public static class CookieManager
    {
        private static ConcurrentDictionary<string, Dictionary<ParsingPageType, CookieContainer>> _cookieCollections = 
            new ConcurrentDictionary<string, Dictionary<ParsingPageType, CookieContainer>>();
        
        public static CookieContainer GetCookieContainer(string ip, ParsingPageType typeParsingPageFor)
        {
            var dictionaty = _cookieCollections.GetOrAdd(ip, new Dictionary<ParsingPageType, CookieContainer>());
            CookieContainer result = null;
            // Есл добавили новый
            if (dictionaty.Count == 0)
            {
                result = new CookieContainer();
                if (typeParsingPageFor == ParsingPageType.Mlsn)
                    result.Add(new Cookie("mlsn_experiment_1", "1", "/", ".mlsn.ru"));
                dictionaty.Add(typeParsingPageFor, result);
            }
            else if (dictionaty.TryGetValue(typeParsingPageFor, out result) && result == null)
            {
                result = new CookieContainer();
                if (typeParsingPageFor == ParsingPageType.Mlsn)
                    result.Add(new Cookie("mlsn_experiment_1", "1", "/", ".mlsn.ru"));
                dictionaty[typeParsingPageFor] = result;
            }

            return result;
        }
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Core;
using Core.Enum;
using CoreParser.DB;
using CoreParser.DB.LocalEntities;
using CoreParser.Enum;
using CoreParser.Exceptions;
using CoreParser.Interface;
using CoreParser.Pages.Board;
using CoreParser.Services;
using CoreParser.Services.PropertyAnalizators;
using HtmlAgilityPack;
using Logger;
using Newtonsoft.Json;
using Page = CoreParser.DB.Models.Page;

namespace CoreParser.Pages
{
    [JsonObject(Id = "page", MemberSerialization = MemberSerialization.OptIn)]
    public abstract class AbstractAdvertisement : AbstractPage
    {
        /// <summary>
        /// Список манагеров по определению свойства объявления. Каждый манагер отвечает за определение своего свойства
        /// </summary>
        private static readonly List<IAdvertismentPropertyAnalizator> Analizators =
            new List<IAdvertismentPropertyAnalizator>()
            {
                // Порядок важен
                new IsDeletedManager(),
                new ObjTypeManager(),
                new DescriptionManager(),
                new CostManager(),
                new SizeManager(),
                new ImageManager(),
                new RentTypeManager(),
                new HouseMaterialManager(),
                new AddressManager(),
                new CostTypeManager(),
                new FloorManager(),
                new MaxFloorManager(),
                new SellTypeManager(),
                new PrepayManager(),
                //new PhoneManager(), // Чтобы так определять номер телефона - нужно научить браузер кликать на кнопку
                new DateManager(),
                new FurnitureCheckManager(),
                new GeoCoordinateManager(),
                //new AgencyManager(),
                new Agency2Manager(),
                new DistrictManager()
            };

        private static TimeSpan? _minTimeToReload;

        #region Properties
        [JsonProperty("id")]
        public int? Id { get; set; }

        protected CostType _costType = CostType.Undefined;
        protected string _searchIndex;

        [JsonProperty("url")] private string jsonUrl => Url;

        [JsonProperty("cost-type")]
        protected string CostTypeString
        {
            get
            {
                switch (CostType)
                {
                    case CostType.Undefined:
                        return "";
                    case CostType.ВсеВключено:
                        return "(Все включено)";
                    case CostType.Счетчики:
                        return "+ Счетчики";
                    case CostType.КоммуналкаИСчетчики:
                    case CostType.Коммуналка:
                        return "+ Ком. услуги";
                    default:
                        return CostType.ToString();
                }
            }
        }

        [JsonProperty("debug-info")]
        public string DebugInfo { get; set; }

        [JsonProperty("search-index")]
        public string SearchIndex
        {
            get
            {
                if (string.IsNullOrEmpty(_searchIndex))
                    _searchIndex = GetSearchIndex(Url);
                return _searchIndex;
            }
            set => _searchIndex = value;
        }

        /// <summary>
        /// Страницу достали из БД?
        /// </summary>
        public bool IsCached => Id.HasValue;


        [JsonProperty("page-source")]
        public string PageSource => Type.ToString();

        #region Database Property
        
        /// <summary>
        /// Список путей к картинкам
        /// </summary>
        [JsonProperty("images")]
        public List<string> DownloadedImagesPathList { get; set; }


        /// <summary>
        /// Дата добавления к нам
        /// </summary>
        public DateTime CreateDateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// дата публикации на сайте-источнике объявления
        /// </summary>
        [JsonProperty("date-publish")]
        public DateTime? PublishDateTime { get; set; }

        [JsonProperty("date-last-refresh")]
        public DateTime? LastRefreshDateTime { get; set; }
        
        /// <summary>
        /// Свойство: С мебелью
        /// </summary>
        [JsonProperty("is-with-furniture")]
        public bool IsWithFurniture { get; set; }

        /// <summary>
        /// Свойство: Мультилистинг
        /// </summary>
        [JsonProperty("is-mls")]
        public bool IsMls { get; set; } = false;

        /// <summary>
        /// Признак: значение IsAgency установленно форсированно?
        /// </summary>
        public bool IsForceIsAgency { get; set; } = false;

        /// <summary>
        /// Признак: объявление из раздела "частники\без комиссии"?
        /// </summary>
        public bool? IsFromOwnerSection { get; set; } = null;

        /// <summary>
        /// Тип продажи (новостройка\вторичка)
        /// </summary>
        [JsonProperty("sell-type")]
        public SellType SellType { get; set; } = SellType.Undefined;

        /// <summary>
        /// Объявление егентства?
        /// </summary>
        [JsonProperty("is-agency")]
        public bool IsAgency { get; set; }

        /// <summary>
        /// Посуточная аренда?
        /// </summary>
        [JsonProperty("rent-type")]
        public RentType RentType { get; set; } = RentType.Undefined;
        

        [JsonProperty("size")]
        public double? Size { get; set; }

        [JsonProperty("obj-type")] public string JsonObjType => ObjType.JsonPrepare();
        public ObjType ObjType { get; set; }

        [JsonProperty("floor")]
        public int? Floor { get; set; }
        
        [JsonProperty("adress")]
        public Adress Adress { get; set; } = new Adress();

        [JsonProperty("prepay")]
        public decimal? Prepay { get; set; }

        [JsonProperty("cost")]
        public decimal? Cost { get; set; }
        
        public CostType CostType
        {
            get => _costType;
            set => _costType = value;
        }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("is_deleted")]
        public bool IsDeleted { get; set; }
        public string PhoneNumber { get; set; } = "Undefined";
        #endregion

        /// <summary>
        /// Директория, куда будут загружены фото
        /// </summary>
        public string Directory { get; set; }
        
        /// <summary>
        /// Верхний отступ для обрезки изображения
        /// </summary>
        public abstract int TopIndent { get; }

        /// <summary>
        /// Нижний отступ для обрезки изображения
        /// </summary>
        public abstract int BottomIndent { get; }

        /// <summary>
        /// Это объявление является основным из склеенных?
        /// </summary>
        public bool IsMasterMerge => IdMasterPage == null;

        /// <summary>
        /// Id страницы основной для склейки
        /// </summary>
        [JsonProperty("id-master-page")]
        public int? IdMasterPage { get; set; }
        #endregion

        protected AbstractAdvertisement(string url, string html = null) : base(url, html){}

        public override bool Parse()
        {
            if (!base.Parse() || Url.Contains("office/statistic"))
                return false;

            try
            {
                // Парсим анализаторами
                foreach (var analizator in Analizators)
                {
                    analizator.Identify(this);
                    if (IsDeleted)
                        return true;
                    if (!(analizator is IsDeletedManager) && (ObjType == null || ObjType.Type == EnumObjType.Undefined))
                    {
#if DEBUG
                        new ObjTypeManager().Identify(this);
#endif
                        throw new Exception("Не удалось распознать [ObjType]: " + Url);
                    }
                    if (analizator is CostManager && !Cost.HasValue)
                    {
#if DEBUG
                        new CostManager().Identify(this);
#endif
                        throw new Exception("Не удалось распознать [Cost]: " + Url);
                    }

                    if (analizator is Agency2Manager a2m)
                    {
                        DebugInfo = a2m.DebugInfo;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true);
                return false;
            }
            return true;
        }

        public virtual bool Download()
        {
            IDownloader downloader = DownloaderFactory.CreateDownloader(Type);
            return !string.IsNullOrEmpty(HtmlPage) || base.DownloadAndParse(downloader);
        }

        /// <summary>
        /// Проверка странцы на актуальность
        /// </summary>
        /// <param name="tryDownload"></param>
        /// <exception cref="CheckActuallyException">Вылетит если не получится скачать страницу для </exception>
        /// <returns>Свежий HTML</returns>
        public bool CheckActualy()
        {
            if (_minTimeToReload == null)
            {
                _minTimeToReload = new TimeSpan(0, 0, SettingsManager.Settings.ActualizerSettings.ActualizePageEveryMin, 0);
            }
            LastRefreshDateTime = DateTime.Now;
            /*
            if (DateTime.Now - LastRefreshDateTime < _minTimeToReload)
            {
                Log.Debug("AbstractAdvertisement Нет нужды проверять, тк прошло менее " + _minTimeToReload.Value.ToString("g"));
                LastRefreshDateTime.Value.AddMinutes(30);
                return true;
            }*/

            try
            {   // Проверка на битые ссылки. 
                // Тк может быть ситуация что в БД добавились случайно ссылки, которые мы нераспознаем
                var t = ParsingPageTypeIdentifier.Identify(Url);
            }
            catch (ArgumentOutOfRangeException)
            {
                IsDeleted = true;
                return true;
            }

            try
            {
                if (!base.DownloadAndParse(null))
                {
                    // Попытка номер 2, только для браузеров
                    if (!SettingsManager.Settings.ServerSettings.ParsingSettings.TryGetValue(Type, out var config) || !config.IsDownloadThroughBrowser ||
                        !base.DownloadAndParse(null))
                    {
                        throw new CheckActuallyException("Не удалось скачать/распарсить страницу для актуализации: " + Url);
                    }
                }

                if (string.IsNullOrEmpty(HtmlPage))
                {
                    if (IsDeleted)
                        return true;
                    throw new CheckActuallyException("Не удалось скачать страницу для актуализации: " + Url);
                }
            }
            catch (CheckActuallyException caex)
            {
                Log.Exception(caex, LogType.Error, true, "page.CheckActualy()");
                return false;
            }

            if (AbstractBoardPage.TryParse(HtmlPage, Url, out var boardPage))
            {
                IsDeleted = true;
                return true;
            }

            if (!SettingsManager.Settings.ActualizerSettings.IsActualizerCheckImages 
                || DownloadedImagesPathList == null || DownloadedImagesPathList.Count == 0) return true;
            foreach (var imagePath in DownloadedImagesPathList)
            {
                if (imagePath.Contains("http:/") || imagePath.Contains("https:/"))
                    return true;

                if (File.Exists(imagePath)) continue;
                /*
                var fileInfo = new FileInfo(imagePath);
                if (fileInfo.Directory != null && fileInfo.Directory.Exists)
                    fileInfo.Directory.Delete(true);
                new ImageManager().DownloadImages(this, fileInfo.Directory?.FullName);
                //*/
                break;
            }
            return true;
        }

        /// <summary>
        /// Индекс используемый для поиска
        /// </summary>
        public static string GetSearchIndex(string url)
        {
            try
            {
                url = url.Trim();
                ParsingPageType parsingPageType = ParsingPageTypeIdentifier.Identify(url);
                var pageTypeId = DbObjectManager.GetIdOrAddNewPageSource(parsingPageType);

                var idCity = CityManager.GetCityIdByUrl(url);
                if (url.Contains("/?"))
                {
                    url = url.Substring(0, url.LastIndexOf("?", StringComparison.Ordinal)-1);
                }

                var urlId = "";
                switch (parsingPageType)
                {
                    case ParsingPageType.Avito:
                    {
                        if (url.Contains("?"))
                            url = url.Substring(0, url.LastIndexOf("?", StringComparison.Ordinal) - 1);

                        var chunks = url.Split('_');
                        urlId = chunks.LastOrDefault();
                        break;
                        }
                    case ParsingPageType.Mlsn:
                    {
                        var chunks = url.Split('-');
                        urlId = chunks.LastOrDefault();
                        break;
                        }
                    case ParsingPageType.Cian:
                    {
                        var chunks = url.Split(new char[]{'/'},StringSplitOptions.RemoveEmptyEntries);
                        urlId = chunks.LastOrDefault();
                        break;
                    }
                    case ParsingPageType.Yandex:
                    {
                        var chunks = url.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                        urlId = chunks.LastOrDefault();
                        if (urlId == null || urlId.Contains("?page="))
                            return null;
                        break;
                    }
                    case ParsingPageType.N1:
                    {
                        var chunks = url.Split(new char[] {'/'}, StringSplitOptions.RemoveEmptyEntries);
                        urlId = chunks.LastOrDefault();
                        if (!long.TryParse(urlId, out var id))
                        {
                            Log.Error("Возможно сгенерируется кривой SearchIndex: '" + urlId + "'\t" + url);
                        }
                        if (urlId == null || urlId.Contains("page=") || url.Contains("limit="))
                            return null;
                        break;
                    }
                    default:
                        return null;
                }

                var searchIndex = string.Format("{0}:{1}:{2}", idCity, pageTypeId, urlId.Replace("/", "")).Trim();
                return searchIndex;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error);
            }
            return null;
        }
        
        /// <summary>
        /// Создать БД сущность из абстрактной страницы
        /// </summary>
        public Page ToDbPage()
        {
            try
            {
                var dbPage = new Page
                {
                    Images = new List<CoreParser.DB.Models.Image>()
                };
                var idSource = DbObjectManager.GetIdOrAddNewPageSource(Type);
                if (idSource < 1)
                    throw new ArgumentOutOfRangeException("Неизвестный тип источника в БД: " + Type);

                dbPage.Id_Source = idSource;

                if (Id.HasValue)
                    dbPage.Id = Id.Value;

                dbPage.Date_Create = CreateDateTime;
                dbPage.SearchIndex = SearchIndex;
                dbPage.Date_LastRefresh = DateTime.Now;
                dbPage.Date_Publish = PublishDateTime;
                dbPage.IsDeleted = IsDeleted;
                dbPage.URL = Url;
                dbPage.IsAgency = IsAgency;
                dbPage.RentType = (int)RentType;
                dbPage.PhoneNumber = PhoneNumber;
                dbPage.Area_Size = Size;
                dbPage.IdMasterPage = IdMasterPage;
                dbPage.IsFromOwnerSection = IsFromOwnerSection;
                dbPage.Floor = Floor;
                dbPage.Prepay = Prepay;
                dbPage.Cost = Cost;
                dbPage.Id_Cost_Type = DbObjectManager.GetIdOrAddNewCostType(CostType.ToString());
                dbPage.Id_Obj_Type = DbObjectManager.GetIdOrAddNewObjType(ObjType?.Name);
                dbPage.Date_Publish = PublishDateTime;
                dbPage.Description = Description;
                dbPage.Id_SellType = RentType == RentType.Sell ? DbObjectManager.GetIdSellType(SellType) : null;
                dbPage.IsWithFurniture = IsWithFurniture;
                dbPage.IsMls = IsMls;
                dbPage.IsForceIsAgency = IsForceIsAgency;

                if (Adress != null)
                {
                    if (Adress.Id != null)
                        dbPage.Id_Adress = Adress.Id;
                    if(string.IsNullOrEmpty(Adress.City))
                        Adress.City = CityManager.GetCityNameByUrl(Url);
                    dbPage.Adress = Adress.ToDbAdress();

                    if (!string.IsNullOrEmpty(dbPage.Adress.House) && dbPage.Adress.House.Length > 15)
                    {
                        Log.Error("Error house: Too long; " + Url
                                                            + "\nCutted house: " + Adress.House
                                                            + "\nGeoSeaarchString: " + Adress.GeoSearchString, true);
                        dbPage.Adress.House = null;
                    }
                }

                if (DownloadedImagesPathList == null)
                {
                    Log.Trace("abstractPage.DownloadedImagesPathList == null; Url: " + Url);
                    return dbPage;
                }

                foreach (var path in DownloadedImagesPathList)
                {
                    var image = new DB.Models.Image
                    {
                        Id_Page = dbPage.Id,
                        Path = path,
                        Exctension = path.Substring(path.LastIndexOf(".", StringComparison.Ordinal),
                            path.Length - path.LastIndexOf(".", StringComparison.Ordinal))
                    };
                    if (image.Exctension.Length > 8)
                        image.Exctension = "http";
                    dbPage.Images.Add(image);
                }

                return dbPage;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

            return null;
        }

        /// <summary>
        /// Попытаемся распарсить ноды по селектору
        /// </summary>
        /// <param name="field">Тип поля</param>
        /// <param name="selector">HTML-Селектор</param>
        /// <param name="isShouldContinue">TRUE если стоит продолжить парсинг этого поля</param>
        /// <returns></returns>
        internal HtmlNodeCollection TrySelectNodes(PageField? field, string selector, out bool isShouldContinue)
        {
            // Некоторые поля нет нужны парсить через HtmlAgilityPack, поэтому отсутвие селектора не является ошибкой
            bool isError = true;
            isShouldContinue = false;
            if (string.IsNullOrEmpty(selector) || selector == "//")
            {
                switch (Type)
                {
                    case ParsingPageType.Avito:
                        if (field == PageField.CostType 
                            || field == PageField.IsWithFurniture)
                            isError = false;
                            break;
                    case ParsingPageType.Mlsn:
                        if (field == PageField.PhoneNumber)
                            isError = false;
                        break;
                    case ParsingPageType.Cian:
                        if (field == PageField.GeoMetka
                            || field == PageField.PhoneNumber)
                            isError = false;
                        break;
                    case ParsingPageType.Yandex:
                        if (field == PageField.Address
                        || field == PageField.GeoMetka)
                        {
                            isError = false;
                            isShouldContinue = true;
                        }
                        break;
                    case ParsingPageType.N1:
                        switch (field)
                        {
                            case PageField.Address:
                            case PageField.CostType:
                            case PageField.Prepay:
                                isError = false;
                                break;
                            case PageField.XPathImage:
                            case PageField.SellType:
                            case PageField.GeoMetka:
                                isError = false;
                                isShouldContinue = true;
                                break;
                        }
                        break;
                    case ParsingPageType.Undefined:
                    default:
                        break;
                }
                if(isError)
                    Log.Warn(Type + " DictionaryStorage.PageParsingSettings[" + field + "] не указан xPath");
                return null;
            }

            HtmlNodeCollection nodes = null;
            try
            {
                nodes = HtmlDocument.DocumentNode.SelectNodes(selector);
                if (nodes != null) return nodes;
            }
            catch (Exception ex)
            {
                Log.Fatal("Не удалось распознать ноды: [" + Type + "] " + field + " '" + selector + "':\n" +
                          ex.GetType() + " " + ex.Message + "\n" + ex.StackTrace);
            }
            if (field == PageField.XPathImage || field == PageField.Description)
                isError = false;
            switch (Type)
            {
                case ParsingPageType.Avito:
                    if (field == PageField.CostType
                        || field == PageField.Prepay)
                        isError = false;
                    if (field == PageField.IsAgency 
                        || field == PageField.Cost)
                    {
                        isError = false;
                        isShouldContinue = true;
                    }
                    break;
                case ParsingPageType.Mlsn:
                    if (field == PageField.GeoMetka 
                        || field == PageField.PhoneNumber 
                        || field == PageField.SellType
                        || field == PageField.IsAgency)
                    {
                        isError = false;
                        isShouldContinue = true;
                    }
                    break;
                case ParsingPageType.Cian:
                    switch (field)
                    {
                        case PageField.CostType when RentType != RentType.Undefined:
                            isError = false;
                            break;
                        case PageField.RentType:
                            IsDeleted = true;
                            isError = false;
                            isShouldContinue = true;
                            break;
                    }

                    break;
                case ParsingPageType.Yandex:
                    if (field == PageField.PhoneNumber
                        || field == PageField.SellType
                        || field == PageField.IsAgency
                        || field == PageField.Size
                        )
                    {
                        isError = false;
                        isShouldContinue = true;
                    }

                    if (field == PageField.Cost)
                    {
                        Log.Error("[Yandex] Стоимость не найдена по '"+selector+"', пытаемся определить хардкодом", true);
                        return TrySelectNodes(null, "//div[@class='Price OfferCard__title']", out isShouldContinue);
                    }
                    if (field == PageField.IsWithFurniture)
                        isError = false;
                    if ((field == PageField.Floor || field == PageField.MaxFloor) && ObjType != null && ObjType.Type == EnumObjType.Commercial)
                        isError = false;
                    break;
                case ParsingPageType.N1:
                    // Скорее всего это продажа
                    if (field == PageField.ObjType 
                        || field == PageField.RentType 
                        || field == PageField.IsAgency)
                    {
                        isError = false;
                        isShouldContinue = true;
                    }

                    if (field == PageField.Size || field == PageField.HouseMaterial)
                    {
                        return TrySelectNodes(null, "//li[contains(@class,'card-land-content-params-list__item') or contains(@class,'card-dacha-content-params-list__item') or contains(@class,'card-commercial-content-params-list__item') or contains(@class,'card-living-content-params-list__item')]", out isShouldContinue);
                    }

                    if (ObjType != null && (ObjType.Type == EnumObjType.Uchastok || ObjType.Type == EnumObjType.Commercial || ObjType.Type == EnumObjType.Dacha || ObjType.Type == EnumObjType.Dom || ObjType.Type == EnumObjType.Kottedj))
                    {
                        if (field == PageField.Floor || field == PageField.MaxFloor || field == PageField.IsWithFurniture || field == PageField.HouseMaterial)
                        {
                            isError = false;
                        }
                    }
                    break;
                case ParsingPageType.Undefined:
                default:
                    break;
            }

            if (isError)
                Log.Warn("[TrySelectNodes] Для страницы типа '" + Type + "' = [" +
                         ObjType?.Type + "] не удалось распознать поле '" + field + "'; \tURL = " + Url);
            return nodes;
        }

        public static bool TryParse(string html, string url, out AbstractAdvertisement page)
        {
            page = null;
            try
            {
                switch (ParsingPageTypeIdentifier.Identify(url))
                {
                    case ParsingPageType.Avito:
                        page = new AvitoAdvertisement(url, html);
                        break;
                    case ParsingPageType.Mlsn:
                        page = new MlsnAdvertisement(url, html);
                        break;
                    case ParsingPageType.Cian:
                        page = new CianAdvertisement(url, html);
                        break;
                    case ParsingPageType.Yandex:
                        page = new YandexAdvertisement(url, html);
                        break;
                    case ParsingPageType.N1:
                        page = new N1Advertisement(url, html);
                        break;
                    case ParsingPageType.Undefined:
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "AbstractAdv::TryParse");
            }

            return page != null && (string.IsNullOrEmpty(html) || page.Parse());
        }

        /// <summary>
        /// Записать страницу в файл (заголовок, урл, тип и html)
        /// </summary>
        /// <param name="subFolder"></param>
        public void LogInFile(string subFolder = "Default", string comment = null)
        {
            try
            {
                var index = SearchIndex.Replace(':', '_');
                var folder = "SavedPages\\" + subFolder;
                if (!System.IO.Directory.Exists(folder))
                    System.IO.Directory.CreateDirectory(folder);
                var fileName = folder + "\\" + index + ".txt";
                while (File.Exists(fileName))
                {
                    fileName = folder + "\\" + index + "_t" + DateTime.Now.Ticks + ".txt";
                }

                var listLines = new List<string>
                {
                    ObjType?.JsonPrepare() + "\t", Title, "\n",
                    Url, "\n",
                    "comment = '" + comment + "'", "\n",
                    HtmlPage
                };
                File.WriteAllLines(fileName, listLines);
            }
            catch (Exception e)
            {
                Log.Exception(e, LogType.Error, true, "AbstractPage::LogInFile");
            }
        }

        public override string ToString()
        {
            return string.Format("[{0}] {1} {2}", Type, ObjType?.Name, Adress?.Street ?? SearchIndex);
        }
    }
}

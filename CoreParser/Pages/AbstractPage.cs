using System;
using System.Linq;
using System.Net;
using Core;
using Core.Enum;
using Core.Services;
using CoreParser.Enum;
using CoreParser.Exceptions;
using CoreParser.Interface;
using CoreParser.Pages.Board;
using CoreParser.Services;
using CoreParser.Services.Downloaders;
using CoreParser.Services.Proxies;
using HtmlAgilityPack;
using Logger;

namespace CoreParser.Pages
{
    public class AbstractPage
    {
        #region Fields

        private int _countDdos = 0;
        protected ParsingPageType _type = ParsingPageType.Undefined;
        protected HtmlDocument _htmlDocument;

        #endregion

        #region  Properties

        /// <summary>
        /// Тип страницы
        /// </summary>
        public ParsingPageType Type
        {
            get
            {
                if (_type == ParsingPageType.Undefined)
                    _type = ParsingPageTypeIdentifier.Identify(Url);
                return _type;
            }
            protected set => _type = value;
        }
        
        /// <summary>
        /// URL страницы
        /// </summary>
        public string Url { get; protected set; }

        /// <summary>
        /// HTML Разметка загруженной страницы
        /// </summary>
        public string HtmlPage { get; set; }

        public string Title { get; set; }

        /// <summary>
        /// Используется для парсинга
        /// </summary>
        protected HtmlDocument HtmlDocument
        {
            get
            {
                if (_htmlDocument != null) return _htmlDocument;
                _htmlDocument = new HtmlDocument();
                _htmlDocument.LoadHtml(HtmlPage);
                return _htmlDocument;
            }
        }

        #endregion

        protected AbstractPage(string url, string html = null)
        {
            Url = url;
            HtmlPage = html;
        }

        /// <summary>
        /// Скачать и распарсить 
        /// </summary>
        /// <param name="downloader"></param>
        /// <returns></returns>
        public bool DownloadAndParse(IDownloader downloader)
        {
            // ToDo Сделать возвращаемым типом - перечисление со статусами. такие как 404, ОК и тд
            if (downloader == null)
            {
                downloader = DownloaderFactory.CreateDownloader(Type);
            }

            string redirectedUrl = null;
            try
            {
                var url = Url.Replace("https", "http");
                if (string.IsNullOrEmpty(HtmlPage))
                    HtmlPage = downloader.Download(url, out redirectedUrl, Type);

                if (string.IsNullOrEmpty(HtmlPage)) return false;
                if (Parse())
                {
                    return true;
                }

                Log.Error("Парсинг не удался: " + url, true);
            }

            #region CaptchaException
            // Все такие страницы загружаются через браузер. так что проблема пока не актуальна
            /*catch (CaptchaException ex)
            {
                Log.Warn(Type + " " + ex.Message + "; " + (_proxy?.Host ?? " прокси не использовались"));
                if (downloader.TryDownloadCount > ProxyManager.ProxyCount && downloader.TryDownloadCount > 5)
                {
                    var msg = "Использованы все прокси, а капча все вылазит. У нас тут критическая беда: " + Url +
                              ";\nreferedUrl: " + redirectedUrl;
                    Log.Fatal(msg, false, true);
                    return false;
                    throw new Exception(msg);
                }

                Log.Trace(String.Format(
                    "Наткнулись на капчу [{3}]:\nUrl:\t\t{0}\nRedirectedUrl:\t{1}\nProxy:\t\t{2}",
                    Url, redirectedUrl, _proxy?.Host, Type));
                switch (Type)
                {
                    case ParsingPageType.Cian:
                        var t = HtmlPage.Substring(HtmlPage.IndexOf("sitekey"));
                        var tt = t.Substring(t.IndexOf(": '") + 3, t.IndexOf("',"));
                        var sitekey = tt.Substring(0, tt.IndexOf("',"));
                        // Если не удалось разобрать капчу, то сменим прокси
                        if (CaptchaManager.ResolveCian(Url, redirectedUrl, out var newHtml, sitekey))
                        {
                            HtmlPage = newHtml;
                            return DownloadHtml(downloader);
                            return Parse();
                        }

                        _proxy = null;
                        HtmlPage = null;
                        return DownloadHtml(downloader);
                    case ParsingPageType.Yandex:
                        // Если не удалось разобрать капчу, то сменим прокси
                        if (CaptchaManager.ResolveYandex(Url, redirectedUrl, HtmlPage, out var resultHTML))
                        {
                            HtmlPage = resultHTML;
                            return DownloadHtml(downloader);
                            return Parse();
                        }

                        _proxy = null;
                        HtmlPage = null;
                        return DownloadHtml(downloader);
                    //HtmlPage = newHtml;
                }

                _proxy = null;
                HtmlPage = null;
                return DownloadHtml(downloader);
            }
            */
            #endregion

            catch (DdosException ddex)
            {
                _countDdos++;
                if (SettingsManager.IsStartAsAPI && _countDdos <= ProxyManager.ProxyCount || _countDdos <= 3)
                {
                    downloader.SendDelayProxy();
                    Log.Error(ddex.Message, true);
                }
                else
                {
                    if (downloader is HttpWebRequestDownloader)
                    {
                        downloader = new WebBrowserDownloader();
                        _countDdos = 0;
                    }
                    else
                        return false;
                }
                // Перекачаем 
                HtmlPage = null;
                return DownloadAndParse(downloader);
            }
            catch (WebException wex)
            {
                // Кастомное исключение от браузера
                if (wex.Status == WebExceptionStatus.RequestCanceled && wex.Message == "<404>")
                {
                    Log.Info("Page Not Found (404). URL: " + Url);
                    return false;
                }

                if (wex.Response != null && wex.Response is HttpWebResponse responce)
                {
                    switch (responce.StatusCode)
                    {
                        case HttpStatusCode.NotFound:
                            if (this is AbstractAdvertisement adv)
                            {
                                adv.IsDeleted = true;
                                return true;
                            }

                            Log.Info("Page Not Found (404). URL: " + Url);
                            return !SettingsManager.IsStartAsAPI;
                        case HttpStatusCode.InternalServerError:
                        case HttpStatusCode.BadRequest:
                            Log.Info("Сервер ответил: '" + wex.Message + "' (HttpStatusCode = " +
                                     responce.StatusCode + "). Выходим URL: " + Url);
                            return false;
                        case HttpStatusCode.BadGateway:
                            if (downloader.TryDownloadCount > 2)
                                return false;
                            HtmlPage = null;
                            Log.Error("Прокси перестала отвечать: тк поймали '502 Недопустимый шлюз.' Попробуем еще раз", true);
                            break;
                    }
                }

                if (downloader.TryDownloadCount > 3 && downloader is HttpWebRequestDownloader)
                {
                    Log.Warn("Перепробовали 3 прокси, но мы попробуем еще через браузер");
                    downloader = new WebBrowserDownloader();
                    return DownloadAndParse(downloader);
                }

                if (downloader.TryDownloadCount > ProxyManager.ProxyCount)
                    throw new Exception("Не удалось скачать страницу - уже перепробованы все прокси: " + Url);
                

                switch (wex.Status)
                {
                    case WebExceptionStatus.TrustFailure:
                    case WebExceptionStatus.SecureChannelFailure:
                        if (downloader.TryDownloadCount > 3)
                            throw new Exception("SecureChannelFailure Перепробованы все прокси на: " + Url);
                        HtmlPage = null;
                        ServicePointManager.SecurityProtocol =
                            SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                            SecurityProtocolType.Tls12;
                        return DownloadAndParse(downloader);

                    case WebExceptionStatus.Timeout:
                        if (downloader is WebBrowserDownloader)
                            Log.Error("Отвалились по таймауту браузера", true);
                        else
                            Log.Warn("Отвалились по таймауту прокси");
                        HtmlPage = null;
                        return DownloadAndParse(downloader);
                    case WebExceptionStatus.SendFailure:
                    case WebExceptionStatus.ConnectFailure:
                    case WebExceptionStatus.ConnectionClosed:
                    case WebExceptionStatus.ProtocolError:
                        if (downloader.TryDownloadCount > 3)
                            throw new Exception(wex.Status + " Перепробованы все прокси на: " + Url);

                        Log.Warn("Ошибка запроса: " + wex.Status + "; Сменим прокси и попробуем еще разок");
                        HtmlPage = null;
                        return DownloadAndParse(downloader);
                    default:
                        Log.Fatal("<AbstractPage::DownloadHtml> WebException " + wex.Message + "; Status: " +
                                  wex.Status + "\n\t" + wex.StackTrace
                                  + (wex.InnerException == null
                                      ? ""
                                      : "\n\nInnerException: " + wex.InnerException.Message + "\n" +
                                        wex.InnerException.StackTrace));
                        break;
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "<AbstractPage::Download>\n"+Url);
            }


            return false;
        }

        public virtual bool Parse()
        {
            if (string.IsNullOrEmpty(HtmlPage) || HtmlPage == "<head></head><body></body>")
                throw new Exception("Не удалось загрузить страницу: "+Url);
            Title = HtmlDocument.DocumentNode.Descendants("title").FirstOrDefault(x => !string.IsNullOrEmpty(x.InnerHtml))?.InnerHtml;
            return CheckPage(HtmlPage);
        }

        /// <summary>
        /// Проверка html на дудос или капчу
        /// Если что то будет - выкинет соответвующее исключение
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        public static bool CheckPage(string html)
        {
            if (string.IsNullOrEmpty(html))
                return false;
            var doc = new HtmlDocument();
            doc.LoadHtml(html);
            var title = doc.DocumentNode.Descendants("title").FirstOrDefault(x => !string.IsNullOrEmpty(x.InnerHtml))?.InnerHtml;
            var isDdosBlocked = title != null && (title.Contains("заблокирован") 
                                                  || title.Contains("с вашего IP-адреса временно ограничен"));
            if (isDdosBlocked)
                throw new DdosException("Дудосим");
            if (!string.IsNullOrEmpty(title) && (title.Contains("Captcha") || title.Contains("Ой!")))
                throw new CaptchaException("Наткнулись на капчу");
            return true;
        }


        /// <summary>
        /// Скачает и вернет либо доску либо объявление
        /// </summary>
        /// <param name="url"></param>
        /// <param name="downloader"></param>
        /// <returns></returns>
        public static AbstractPage DownloadPage(string url, IDownloader downloader)
        {
            AbstractPage result = new AbstractPage(url);
            if (!result.DownloadAndParse(downloader))
            {
                return null;
            }

            if (AbstractBoardPage.TryParse(result.HtmlPage, url, out var boardPage))
            {
               return boardPage;
            }
            if (AbstractAdvertisement.TryParse(result.HtmlPage, url, out var page) || page != null)
            {
                return page;
            }

            Log.Error("Не удалось определить тип страницы: " + url);
            return null;
        }
    }
}

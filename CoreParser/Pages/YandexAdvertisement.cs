﻿using System;
using Core;
using Core.Enum;
using CoreParser.Enum;

namespace CoreParser.Pages
{
    public class YandexAdvertisement : AbstractAdvertisement
    {
        public override int TopIndent => 54;
        public override int BottomIndent => 0;

        public YandexAdvertisement(string url, string html = null) : base(url, html)
        {
            Url = url;
            Type = ParsingPageType.Yandex;
            Directory = string.Format("{0}\\{2:yyyy}\\{2:MM}\\{2:dd}\\{1}\\",
                    SettingsManager.Settings.ServerSettings?.ImageFolder, Type, DateTime.Now);

        }
    }
}

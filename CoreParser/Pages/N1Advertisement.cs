using System;
using Core;
using Core.Enum;
using CoreParser.Enum;

namespace CoreParser.Pages
{
    public class N1Advertisement : AbstractAdvertisement
    {
        public override int TopIndent => 50;
        public override int BottomIndent => 100;

        public N1Advertisement(string url, string html = null) : base(url, html)
        {
            Url = url;
            Type = ParsingPageType.N1;
            Directory = string.Format("{0}\\{2:yyyy}\\{2:MM}\\{2:dd}\\{1}\\",
                SettingsManager.Settings.ServerSettings?.ImageFolder, Type, DateTime.Now);

        }
    }
}

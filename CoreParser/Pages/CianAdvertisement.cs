﻿using System;
using Core;
using Core.Enum;
using CoreParser.Enum;

namespace CoreParser.Pages
{
    public class CianAdvertisement : AbstractAdvertisement
    {
        public override int TopIndent => 0;
        public override int BottomIndent => 180;

        public CianAdvertisement(string url, string html = null) : base(url, html)
        {
            Url = url;
            Type = ParsingPageType.Cian;
            Directory = string.Format("{0}\\{2:yyyy}\\{2:MM}\\{2:dd}\\{1}\\",
                SettingsManager.Settings.ServerSettings?.ImageFolder, Type, DateTime.Now);
        }
    }
}

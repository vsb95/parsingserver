using System;
using System.Collections.Generic;
using Core;
using Core.Enum;
using CoreParser.Enum;
using CoreParser.Services;
using HtmlAgilityPack;
using Logger;

namespace CoreParser.Pages.Board
{
    public abstract class AbstractBoardPage : AbstractPage
    {
        #region Properties
        
        /// <summary>
        /// Список страниц. Это результат парсинга
        /// </summary>
        public List<AbstractAdvertisement> AdvertisementsList { get; private set; } = new List<AbstractAdvertisement>();

        public List<string> UrlList { get; private set; } = new List<string>();

        public bool? IsFromOwnerSection { get; set; }
        #endregion

        protected AbstractBoardPage(string url, string html = null) : base(url, html)
        {
            switch (Type)
            {
                case ParsingPageType.Avito:
                    if (Url.Contains("&user=1"))
                    {
                        IsFromOwnerSection = true;
                    }
                    if (Url.Contains("&user=2"))
                    {
                        IsFromOwnerSection = false;
                    }
                    break;
                case ParsingPageType.Mlsn:
                    if (Url.Contains("&onlyOwners=1"))
                    {
                        IsFromOwnerSection = true;
                    }
                    if (Url.Contains("&isMls=1"))
                    {
                        IsFromOwnerSection = false;
                    }
                    break;
                case ParsingPageType.Cian:
                    break;
                case ParsingPageType.Yandex:
                    if (Url.Contains("agents=NO") || Url.Contains("hasAgentFee=NO"))
                    {
                        IsFromOwnerSection = true;
                    }
                    if (Url.Contains("agents=YES") || Url.Contains("hasAgentFee=YES"))
                    {
                        IsFromOwnerSection = false;
                    }
                    break;
                case ParsingPageType.N1:
                    if (Url.Contains("author=owner"))
                    {
                        IsFromOwnerSection = true;
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            //Если доску скачивает сервер, то по умолчанию выставляем в агентства
            if (!SettingsManager.IsStartAsAPI
                && IsFromOwnerSection == null)
            {
                IsFromOwnerSection = false;
            }
        }

        /// <summary>
        /// Парсинг. До этого необходимо скачать страницу
        /// </summary>
        /// <returns></returns>
        public override bool Parse()
        {
            if (string.IsNullOrEmpty(HtmlPage) && !base.DownloadAndParse(null))
            {
                Log.Debug(Type + " Почему то страница не скачалась: " + Url);
                throw new NullReferenceException(Type + " Не удалось распознать HTML. Тк ничего нет");
            }

            try
            {
                var doc = new HtmlDocument();
                doc.LoadHtml(HtmlPage);

                var xPath = DictionaryStorage.BoardParsingSettings[Type];
                var nodes = doc.DocumentNode.SelectNodes(xPath);
                if (nodes == null)
                {
                    //Log.Warn("У доски не найдено не одной ссылки (возможно это не доска): " + Url);
                    return false;
                }

                // режется до первого слеша после первой точки
                var domain = Url.Substring(0,
                    Url.IndexOf("/", Url.IndexOf(".", StringComparison.Ordinal), StringComparison.Ordinal));

                foreach (var node in nodes)
                {
                    var link = node?.Attributes["href"]?.Value;
                    if (string.IsNullOrEmpty(link)
                        || link == "#"
                        || link.Contains("office/statistic")
                        || link.Contains("//li.ru/"))
                        continue;
                    if (!link.StartsWith("http"))
                        link = domain + link;

                    AbstractAdvertisement advertisement = null;
                    switch (Type)
                    {
                        case ParsingPageType.Avito:
                            advertisement = new AvitoAdvertisement(link);
                            break;
                        case ParsingPageType.Mlsn:
                            advertisement = new MlsnAdvertisement(link);
                            break;
                        case ParsingPageType.Cian:
                            advertisement = new CianAdvertisement(link);
                            break;
                        case ParsingPageType.Yandex:
                            advertisement = new YandexAdvertisement(link);
                            break;
                        case ParsingPageType.N1:
                            advertisement = new N1Advertisement(link);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                    advertisement.IsFromOwnerSection = IsFromOwnerSection;
                    UrlList.Add(link);
                    AdvertisementsList.Add(advertisement);
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true,
                    "Ошибка во время парсинга AbstractBoardPage [" + Type + "]: " + Url);
            }

            return AdvertisementsList.Count > 0;
        }


        public static bool TryParse(string html, string url, out AbstractBoardPage boardPage)
        {
            boardPage = null;
            try
            {
                var type = ParsingPageTypeIdentifier.Identify(url);
                boardPage = new BoardPage(type, url, html);
                var isHasInnerAdv = boardPage.Parse();
                var result = url.Contains("page=") || url.Contains("p=") ||
                             CityManager.GeneratedUrlList[type].Contains(url) || isHasInnerAdv;
                return result;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "AbstractAdv::TryParse");
            }

            return false;
        }
    }
}

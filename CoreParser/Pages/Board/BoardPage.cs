﻿using System;
using Core.Enum;
using CoreParser.Enum;

namespace CoreParser.Pages.Board
{
    public class BoardPage : AbstractBoardPage
    {
        public BoardPage(ParsingPageType type, string url, string html = null) : base(url, html)
        {
            if (type == ParsingPageType.Undefined)
                type = ParsingPageTypeIdentifier.Identify(url);
            if (type == ParsingPageType.Undefined)
                throw new ArgumentOutOfRangeException(nameof(type), "Не указан явно тип страницы");
            Url = url;
            HtmlPage = html;
            Type = type;
        }
    }
}

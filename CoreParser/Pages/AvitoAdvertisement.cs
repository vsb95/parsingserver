using System;
using Core;
using Core.Enum;
using CoreParser.Enum;

namespace CoreParser.Pages
{
    public class AvitoAdvertisement : AbstractAdvertisement
    {
        public override int TopIndent => 0;
        public override int BottomIndent => 60;

        public AvitoAdvertisement(string url, string html = null) : base(url, html)
        {
            Url = url;
            Type = ParsingPageType.Avito;
            Directory = string.Format("{0}\\{2:yyyy}\\{2:MM}\\{2:dd}\\{1}\\",
                SettingsManager.Settings.ServerSettings?.ImageFolder, Type, DateTime.Now);
        }
    }
}

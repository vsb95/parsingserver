﻿namespace Yandex.Geocoder
{
    public enum Kind
    {
        None,
        house, street, metro, district, locality,

        /// <summary>
        /// Вход. Используется только в ответе если kind = None
        /// </summary>
        entrance
    }
}

﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Yandex.Geocoder.Raw
{
    [DataContract]
    internal class Envelope
    {
        [DataMember]
        internal string lowerCorner { get; set; }

        [DataMember]
        internal string upperCorner { get; set; }
    }

    [DataContract]
    internal class BoundedBy
    {
        [DataMember]
        internal Envelope Envelope { get; set; }
    }

    [DataContract]
    internal class GeocoderResponseMetaData
    {
        [DataMember]
        internal string request { get; set; }

        [DataMember]
        internal string found { get; set; }

        [DataMember]
        internal string results { get; set; }

        [DataMember]
        internal BoundedBy boundedBy { get; set; }
    }

    [DataContract]
    internal class MetaDataProperty
    {
        [DataMember]
        internal GeocoderResponseMetaData GeocoderResponseMetaData { get; set; }
    }

    [DataContract]
    internal class DependentLocality2
    {
        internal string DependentLocalityName { get; set; }
    }

    [DataContract]
    internal class DependentLocality
    {
        [DataMember]
        internal string DependentLocalityName { get; set; }

        [DataMember]
        internal DependentLocality2 DependentLocality2 { get; set; }
    }

    [DataContract]
    internal class Locality
    {
        [DataMember]
        internal string LocalityName { get; set; }

        [DataMember]
        internal DependentLocality DependentLocality { get; set; }
    }

    [DataContract]
    internal class DependentLocality3
    {
        [DataMember]
        internal string DependentLocalityName { get; set; }
    }

    [DataContract]
    internal class Locality2
    {
        [DataMember]
        internal string LocalityName { get; set; }

        [DataMember]
        internal DependentLocality3 DependentLocality { get; set; }
    }

    [DataContract]
    internal class SubAdministrativeArea
    {
        [DataMember]
        internal string SubAdministrativeAreaName { get; set; }

        [DataMember]
        internal Locality2 Locality { get; set; }
    }

    [DataContract]
    internal class AdministrativeArea
    {
        [DataMember]
        internal string AdministrativeAreaName { get; set; }

        [DataMember]
        internal Locality Locality { get; set; }

        [DataMember]
        internal SubAdministrativeArea SubAdministrativeArea { get; set; }
    }

    [DataContract]
    internal class Country
    {
        [DataMember]
        internal string AddressLine { get; set; }

        [DataMember]
        internal string CountryNameCode { get; set; }

        [DataMember]
        internal string CountryName { get; set; }

        [DataMember]
        internal AdministrativeArea AdministrativeArea { get; set; }
    }

    [DataContract]
    internal class AddressDetails
    {
        [DataMember]
        internal Country Country { get; set; }
    }

    [DataContract]
    internal class GeocoderMetaData
    {
        [DataMember]
        internal string kind { get; set; }

        [DataMember]
        internal string text { get; set; }

        [DataMember]
        internal string precision { get; set; }

        [DataMember]
        internal AddressDetails AddressDetails { get; set; }
    }

    [DataContract]
    internal class MetaDataProperty2
    {
        [DataMember]
        internal GeocoderMetaData GeocoderMetaData { get; set; }
    }

    [DataContract]
    internal class Envelope2
    {
        [DataMember]
        internal string lowerCorner { get; set; }

        [DataMember]
        internal string upperCorner { get; set; }
    }

    [DataContract]
    internal class BoundedBy2
    {
        [DataMember]
        internal Envelope2 Envelope { get; set; }
    }

    [DataContract]
    internal class Point
    {
        [DataMember]
        internal string pos { get; set; }
    }

    [DataContract]
    internal class GeoObject
    {
        [DataMember]
        internal MetaDataProperty2 metaDataProperty { get; set; }

        [DataMember]
        internal string description { get; set; }

        [DataMember]
        internal string name { get; set; }

        [DataMember]
        internal BoundedBy2 boundedBy { get; set; }

        [DataMember]
        internal Point Point { get; set; }
    }

    [DataContract]
    internal class FeatureMember
    {
        [DataMember]
        internal GeoObject GeoObject { get; set; }
    }

    [DataContract]
    internal class GeoObjectCollection
    {
        [DataMember]
        internal MetaDataProperty metaDataProperty { get; set; }

        [DataMember]
        internal List<FeatureMember> featureMember { get; set; }
    }

    [DataContract]
    internal class ApiResponse
    {
        [DataMember]
        internal GeoObjectCollection GeoObjectCollection { get; set; }
    }

    [DataContract]
    internal class RootObject
    {
        [DataMember]
        internal ApiResponse response { get; set; }
    }
}
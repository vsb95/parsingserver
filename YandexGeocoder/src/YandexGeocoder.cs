﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using Logger;
using Yandex.Geocoder.Raw;

namespace Yandex.Geocoder
{
    public class YandexGeocoder
    {
        /// <summary>
        ///     The address you want to geocode, or the geographical coordinates. Coordinates can be set in reverse geocoding:
        /// </summary>
        internal string SearchQuery { get; set; } = "null";

        /// <summary>
        ///     Locale.
        /// </summary>
        internal LanguageCode LanguageCode { get; set; } = LanguageCode.ru_RU;

        internal Kind Kind { get; set; } = Kind.None;
        /// <summary>
        ///     Maximum number of objects to return. Default value: 10. Maximum value: 100.
        /// </summary>
        internal int Results { get; set; } = 10;

        /// <summary>
        ///     The number of objects to skip in the response (starting from the first one). Default value: 0.
        /// </summary>
        internal int Skip { get; set; } = 0;

        /// <summary>
        ///     The key assigned in the Yandex Developer's Dashboard.
        /// </summary>
        internal string Apikey { get; set; } = null;

        public WebProxy Proxy { get; set; }

        /// <summary>
        ///     Builds the URL with all parameters
        /// </summary>
        /// <returns>URL for webRequest</returns>
        private string BuildUrl()
        {
            var sb = new StringBuilder();
            sb.Append("http://geocode-maps.yandex.ru/1.x/?");
            sb.Append($"geocode={SearchQuery}");
            sb.Append($"&lang={LanguageCode}");
            sb.Append($"&results={Results}");
            if (Kind != Kind.None)
                sb.Append($"&kind={Kind}");
            if (Skip != 0) sb.Append($"&skip={Skip}");
            if (Apikey != null) sb.Append($"&apikey={Apikey}");
            sb.Append("&format=json");
            return sb.ToString();
        }

        /// <summary>
        ///     Gets the raw JSON responce from Yandex YandexGeocoder.
        /// </summary>
        /// <returns>JSON string</returns>
        internal string GetJsonResponce()
        {
            var url = BuildUrl();
            var request = WebRequest.Create(url);
            if (Proxy != null)
            {
                request.Proxy = Proxy;
                request.Credentials = CredentialCache.DefaultCredentials;
            }
            using (var response = (HttpWebResponse) request.GetResponse())
            {
                using (var dataStream = response.GetResponseStream())
                {
                    if (dataStream == null) throw new Exception("Response stream is null");
                    using (var reader = new StreamReader(dataStream))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
        }

        /// <summary>
        ///     Gets the Raw data. RootObject
        /// </summary>
        /// <returns>Deserialized RootObject</returns>
        internal RootObject GetRawData()
        {
            using (var stream = new MemoryStream())
            {
                using (var writer = new StreamWriter(stream))
                {
                    writer.Write(GetJsonResponce());
                    writer.Flush();
                    stream.Position = 0;
                    var serializer = new DataContractJsonSerializer(typeof(RootObject));
                    return (RootObject)serializer.ReadObject(stream);
                }
            }
        }

        /// <summary>
        ///     Gets the responses list.
        /// </summary>
        /// <returns>List with YandexResponse objects</returns>
        internal List<YandexResponse> GetResults()
        {
            var ynadexResponsesList = new List<YandexResponse>();
            foreach (var featureMember in GetRawData()?.response?.GeoObjectCollection?.featureMember)
            {
                if(featureMember== null) continue;
                var response = new YandexResponse(featureMember.GeoObject);
                ynadexResponsesList.Add(response);
            }
            return ynadexResponsesList;
        }

        /// <summary>
        /// найти адрес по координате 
        /// </summary>
        /// <param name="geocode">адрес или координаты</param>
        /// <param name="kind">Тип искомого значения. По умолчанию - улица с домом</param>
        /// <returns></returns>
        public static YandexResponse FindAdress(string geocode, Kind kind = Kind.house, WebProxy proxy = null)
        {
            if (string.IsNullOrEmpty(geocode))
                throw new ArgumentNullException(nameof(geocode));
            YandexGeocoder geocoder = null;
            try
            {
                geocoder = new YandexGeocoder
                {
                    SearchQuery = geocode,
                    Results = 1,
                    LanguageCode = LanguageCode.ru_RU,
                    Kind = kind,
                    Proxy = proxy
                };
                var results = geocoder.GetResults();
                return results?.FirstOrDefault();
            }
            catch (Exception ex)
            {
                try
                {
                    if (geocoder != null)
                    {
                        geocoder.Proxy = null;
                        var results = geocoder.GetResults();
                        return results.FirstOrDefault();
                    }
                }
                catch (Exception)
                {
                    //
                }

                Log.Error("[YandexGeocoder] Ошибка при распознавании адреса: " + string.Format("(geocode='{0}', {1}, proxy='{2}')", geocode, kind, proxy?.Address), true);
                Log.Exception(ex, LogType.Fatal, true);
            }

            return null;
        }
    }
}

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Logger;
using Newtonsoft.Json;
using ProxyService.BrowserService;
using ProxyService.Exceptions;

namespace ProxyService.Proxies
{
    public static class ProxyManager
    {
        private static readonly object _locker = new object();
        public static bool IsLoadedFreeProxy = false;
        public static int AllProxyCount => _allProxyDictionary.FirstOrDefault().Value.Count;
        public static int ProxyCount => _privateProxyDictionary.FirstOrDefault().Value.Count;
        private static ConcurrentDictionary<string, DateTime> _badProxyDictionary = new ConcurrentDictionary<string, DateTime>();

        private static string ApiKeyProxy = "eefb89e40a-43ebd9c142-56e113d79e";

        private static ConcurrentDictionary<ParsingPageType, ConcurrentQueue<Proxy>> _allProxyDictionary = new ConcurrentDictionary<ParsingPageType, ConcurrentQueue<Proxy>>();
        private static ConcurrentDictionary<ParsingPageType, ConcurrentQueue<Proxy>> _privateProxyDictionary = new ConcurrentDictionary<ParsingPageType, ConcurrentQueue<Proxy>>();
       

        public static void LoadProxies()
        {
            try
            {
                var privateProxyDictionary = new ConcurrentDictionary<ParsingPageType, ConcurrentQueue<Proxy>>();
                var allProxyDictionary = new ConcurrentDictionary<ParsingPageType, ConcurrentQueue<Proxy>>();
                foreach (ParsingPageType value in System.Enum.GetValues(typeof(ParsingPageType)))
                {
                    privateProxyDictionary.AddOrUpdate(value, new ConcurrentQueue<Proxy>(),
                        (type, queue) => new ConcurrentQueue<Proxy>());
                    allProxyDictionary.AddOrUpdate(value, new ConcurrentQueue<Proxy>(),
                        (type, queue) => new ConcurrentQueue<Proxy>());
                }


                using (WebClient web = new WebClient {Encoding = Encoding.UTF8})
                {
                    var response = web.DownloadString("https://proxy6.net/api/" + ApiKeyProxy + "/getproxy");
                    var proxyList = new ProxyList(response);
                    foreach (var proxy in proxyList.Proxies)
                    {
                        proxy.DeserializeCookies();
                        if (proxy.IsActive && proxy.Country == "ru")
                        {
                            foreach (var pair in privateProxyDictionary)
                                pair.Value.Enqueue(proxy);
                            foreach (var pair in allProxyDictionary)
                                pair.Value.Enqueue(proxy);
#if DEBUG
                            break;
#endif
                        }
                    }
                }

                lock (_locker)
                {
                    _privateProxyDictionary = privateProxyDictionary;
                    _allProxyDictionary = allProxyDictionary;
                }
                LoadFreeProxy(true);

                var json = JsonConvert.SerializeObject(_privateProxyDictionary, Formatting.Indented);
                File.WriteAllText("proxies.txt", json);

                Log.Info("Подключено " + ProxyCount + " прокси");
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
                LoadProxies();
            }
        }

        public static Proxy GetProxy(ParsingPageType typeParsingPageFor, bool isOnlyPrivate = true, bool isCanFree = false)
        {
            lock (_locker)
            {
                var source = isCanFree ? _allProxyDictionary : _privateProxyDictionary;
                Proxy result = null;
                if (!source.TryGetValue(typeParsingPageFor, out var proxyQueue)) return null;
                var i = 0;
                WebProxy webProxy = null;
                do
                {
                    // Достанем и положим в конец
                    if (!proxyQueue.TryDequeue(out result)) return result;
                    proxyQueue.Enqueue(result);
                    i++;
                    webProxy = result.ToWebProxy(typeParsingPageFor);
                    if (webProxy == null && i == proxyQueue.Count-1)
                    {
                        Log.Error("Все прокси для " + typeParsingPageFor + " задудосены");
                        return null;
                    }
                } while (isOnlyPrivate && result.IsShared && i <= proxyQueue.Count
                         && !isCanFree && result.IsFree && webProxy == null);

                return webProxy != null ? result : null;
            }
        }

        public static Proxy GetFreeProxy()
        {
            lock (_locker)
            {
                var source = _allProxyDictionary;
                Proxy result = null;
                if (!source.TryGetValue(ParsingPageType.Undefined, out var proxyQueue)) return null;
                var i = 0;
                do
                {
                    // Достанем и положим в конец
                    if (!proxyQueue.TryDequeue(out result)) return result;
                    proxyQueue.Enqueue(result);
                    i++;
                } while (!result.IsFree && i <= proxyQueue.Count);

                return result;
            }
        }

        public static bool TryLoadCasheProxyList()
        {
            try
            {
                if (!File.Exists("proxies.txt"))
                    return false;
                var fileInfo = new FileInfo("proxies.txt");
                if (fileInfo.LastWriteTime.Add(new TimeSpan(0, 4, 0, 0)) > DateTime.Now)
                {
                    var json = File.ReadAllText("proxies.txt");
                    if (string.IsNullOrEmpty(json)) return false;
                    var result = new ConcurrentDictionary<ParsingPageType, ConcurrentQueue<Proxy>>();
                    using (StringReader sr = new StringReader(json))
                    {
                        JsonSerializer _deserializer = new JsonSerializer { NullValueHandling = NullValueHandling.Ignore };
                        _deserializer.Populate(new JsonTextReader(sr), result);
                    }

                    _privateProxyDictionary = result;
                    foreach (var pair in result)
                    {
                        var queue = _allProxyDictionary.GetOrAdd(pair.Key, new ConcurrentQueue<Proxy>());
                        foreach (var proxy in pair.Value)
                        {
                            queue.Enqueue(proxy);
                        }
                    }
                    LoadFreeProxy(true);

                    return _privateProxyDictionary != null && _privateProxyDictionary.Count > 0 &&
                           _privateProxyDictionary.FirstOrDefault().Value.Count > 0;
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true);
            }
            return false;
            
        }


        /// <summary>
        /// Удалить бесплатный прокси из всех списков
        /// </summary>
        public static void ExcludeFreeProxy(string ip)
        {
            try
            {
                Log.Info("Удаляем free Proxy: "+ip);
                if(string.IsNullOrEmpty(ip))
                    return;
                lock (_locker)
                {
                    _badProxyDictionary.AddOrUpdate(ip, DateTime.Now, (s, time) => DateTime.Now);

                    var backList = new List<Proxy>();
                    #region Уберем во всех Dictionary

                    foreach (var pair in _allProxyDictionary)
                    {
                        while (pair.Value.TryDequeue(out var result))
                        {
                            if (result.IsFree && result.Ip == ip)
                                break;

                            backList.Add(result);
                        }
                        // Вернем на место
                        foreach (var proxy1 in backList)
                            pair.Value.Enqueue(proxy1);
                    }

                    #endregion
                    var countFreeProxy = _allProxyDictionary[ParsingPageType.Undefined].Count(x => x.IsFree);
                    Log.Info("Осталось "+ countFreeProxy+ " бесплатных прокси");
                    if (countFreeProxy <= 3)
                        LoadFreeProxy(true);
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "ExcludeFreeProxy");
            }
        }
        
        private static async void LoadFreeProxy(bool force = false)
        {
            if (File.Exists("free_proxies.txt") && !force)
            {
                var fileInfo = new FileInfo("free_proxies.txt");
                if (fileInfo.LastWriteTime.Add(new TimeSpan(0, 0, 20, 0)) > DateTime.Now)
                {
                    var lines = File.ReadAllLines("free_proxies.txt");
                    foreach (var line in lines)
                    {
                        var uri = new Uri(line);

                        // Если это плохой прокси, то не будем его грузить
                        if(_badProxyDictionary.TryGetValue(uri.Host, out var date) && date.AddMinutes(30) > DateTime.Now)
                            continue;
                        else
                            date = DateTime.MinValue;
                        
                        var proxy = new Proxy(uri.Host, uri.Port.ToString());
                        foreach (var pair in _allProxyDictionary)
                            pair.Value.Enqueue(proxy);
                    }

                    Log.Info("Загружено " + lines.Length + " бесплатных прокси из кэша");
                    IsLoadedFreeProxy = true;
                    return;
                }
            }

            await Task.Factory.StartNew(() =>
            {
                Log.Info("loading free Proxy ");
                var url = "http://www.freeproxylists.net/ru/?c=RU&s=rs";
                //"https://hidemyna.me/ru/proxy-list/?country=RU&maxtime=1000&type=h#list"
                var list = new List<KeyValuePair<string, string>>();
                try
                {
                    Browser browser = null;
                    try
                    {
                        browser = BrowserManager.GetNextFreeBrowser();
                    }
                    catch (AllIsBusyException busyEx)
                    {
                        //
                    }
                    for (int i = 0; browser == null; i++)
                    {
                        // через 90 сек оповестим
                        if (i > 90)
                        {
                            i = 0;
                            Log.Fatal("Ожидаю запуска браузеров");
                        }

                        Log.Warn("Ожидаю запуска браузеров");

                        Thread.Sleep(500);
                        try
                        {
                            browser = BrowserManager.GetNextFreeBrowser();
                        }
                        catch (AllIsBusyException busyEx)
                        {
                            //
                        }
                    }
                    string html = browser.Download(url);
                    if (html.Contains("<title>500 Error - Internal Server Error</title>"))
                    {
                        url = "https://hidemyna.me/ru/proxy-list/?country=RU&maxtime=1000&type=h#list";
                        html = browser.Download(url);
                    }
                    var doc = new HtmlDocument();
                    doc.LoadHtml(html);
                    var table = doc.DocumentNode
                        .SelectNodes("//table[contains(@class,'proxy__t') or contains(@class,'DataGrid')]")
                        ?.FirstOrDefault();
                    if (table == null) return;
                    var rows = table.ChildNodes.Nodes().ToList();
                    for (var i = 1; i < rows.Count; i++)
                    {
                        var row = rows[i];
                        var cells = row.ChildNodes.Nodes().ToList().Select(x => x.InnerText).ToList();
                        if (url.Contains("hidemyna.me"))
                        {
                            if (cells.Count < 5 || !cells[4].ToLower().Contains("http")) continue;
                            if (int.TryParse(cells[3].Replace(" мс", ""), out var ping) && ping > 1000) continue;
                            list.Add(new KeyValuePair<string, string>(cells[0], cells[1]));
                        }
                        else if (url.Contains("freeproxylists.net"))
                        {
                            if (cells.Count < 10 || cells[3].ToLower() != "http") continue;
                            list.Add(new KeyValuePair<string, string>(cells[1], cells[2]));
                        }
                    }

                    var textList = new List<string>();
                    lock (_locker)
                    {
                        foreach (var valuePair in list)
                        {
                            // Если это плохой прокси, то не будем его грузить
                            if (_badProxyDictionary.TryGetValue(valuePair.Key, out var date) && date.AddMinutes(30) > DateTime.Now)
                                continue;
                            else
                                date = DateTime.MinValue;

                            textList.Add($"http://{valuePair.Key}:{valuePair.Value}");
                            var proxy = new Proxy(valuePair.Key,valuePair.Value);
                            if (!proxy.PingHost()) continue;
                            //new WebProxy(new Uri("http://"+pair.Key+":"+pair.Value));
                            foreach (var pair in _allProxyDictionary)
                                pair.Value.Enqueue(proxy);
                        }
                    }

                    Log.Info("Загружено " + list.Count + " бесплатных прокси");
                    if (textList.Count > 10)
                        File.WriteAllLines("free_proxies.txt", textList);
                }
                catch (Exception ex)
                {
                    Log.Exception(ex);
                }
                IsLoadedFreeProxy = true;
            });
        }


        /// <summary>
        /// Используется для временного исключения прокси из очереди на использование тк по ней идет дудос
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="parsingPageTypeFor"></param>
        public static void UpdateDelayProxy(string ip, ParsingPageType parsingPageTypeFor)
        {
            var newDate = DateTime.Now.AddMinutes(5);
            /*switch (parsingPageTypeFor)
            {
                case ParsingPageType.Avito:
                    newDate = newDate.AddMinutes(10);
                    break;
                default:
                    newDate = newDate.AddMinutes(15);
                    break;
            }*/

            lock (_locker)
            {
                foreach (var pair in _privateProxyDictionary)
                {
                    if(pair.Key != parsingPageTypeFor) continue;
                    foreach (var proxy in pair.Value)
                    {
                        if (proxy.Ip != ip)
                            continue;
                        Log.Debug(
                            "Ставим задержку на прокси для [" + parsingPageTypeFor + "] тк через нее дудосим:" + proxy.Ip);
                        proxy.UpdateTime(newDate, parsingPageTypeFor);
                        return;
                    }
                }

                Log.Error("Не смогли установить задержку на прокси для [" + parsingPageTypeFor + "] тк прокси '" + ip + "' не найдена", true);
            }
        }
    }
}

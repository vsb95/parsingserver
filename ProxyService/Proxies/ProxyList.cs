﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace ProxyService.Proxies
{
    internal class ProxyList
    {

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("user_id")]
        public string UserId { get; set; }

        [JsonProperty("balance")]
        public decimal Balance { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }
        
        [JsonProperty("list")]
        public List<Proxy> Proxies { get; set; }

        public ProxyList(string json)
        {
            Deserialize(json);
        }

        public void Deserialize(string json)
        {
            Proxies = new List<Proxy>();
            var split = json.Split(new[] { "\"list\":{"}, StringSplitOptions.None);
            var list = split.Last()?.Split(new[] { "\"},\""}, StringSplitOptions.None);
            if(list == null || list.Length == 0)
                throw new Exception("не удалось инициализировать список прокси");
            for (var i = 0; i < list.Length; i++)
            {
                var proxyJson = list[i].Split('}').First();
                if (i<list.Length-1)
                    proxyJson += "\"}";
                proxyJson=proxyJson.Replace(proxyJson.Split('{').First(), "");
                if (i == list.Length - 1)
                    proxyJson += "}";
                var proxy = new Proxy(proxyJson);
                Proxies.Add(proxy);
            }

            Proxies = Proxies.OrderByDescending(proxy => proxy.Version).ToList();
        }

    }
}

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Logger;
using Newtonsoft.Json;

namespace ProxyService.Proxies
{
    public class Proxy
    {
        // ToDo Сериализовать куки, чтобы передать их клиенту
        //[JsonProperty("serialized-cookies")]
        //public string SerializedCookies => SerializeCookies();

        [JsonProperty("is-free")]
        private bool _isFree = false;

        public object Locker = new object();
        private static TimeSpan? _minTimeToReUseProxy;
        private readonly ConcurrentDictionary<ParsingPageType, object> _lockers = new ConcurrentDictionary<ParsingPageType, object>();
        private readonly ConcurrentDictionary<ParsingPageType, DateTime> _historyUsingDictionary = new ConcurrentDictionary<ParsingPageType, DateTime>();
        private readonly ConcurrentDictionary<ParsingPageType, CookieContainer> _cookieDictionary = new ConcurrentDictionary<ParsingPageType, CookieContainer>();

        #region Properties
        [JsonProperty("active")] private string _active;
        private static readonly JsonSerializer Deserializer = new JsonSerializer { NullValueHandling = NullValueHandling.Ignore };

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }

        [JsonProperty("ip")]
        public string Ip { get; set; }

        [JsonProperty("host")]
        public string Host { get; set; }

        [JsonProperty("port")]
        public string Port { get; set; }

        [JsonProperty("user")]
        public string UserLogin { get; set; }

        [JsonProperty("pass")]
        public string UserPassword { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("descr")]
        public string Description { get; set; }

        public bool IsActive => _active == "1" || _isFree;

        public bool IsShared => Version == "3" || _isFree;
        public bool IsFree => _isFree;


        private string FileNameCookies => "Cookies\\" + Ip + ".json";
        #endregion

        private Proxy()
        {

        }

        public Proxy(string json)
        {
            Deserialize(json);
            _isFree = false;
        }

        /// <summary>
        /// Free Proxy
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        public Proxy(string ip, string port, bool isFree = true)
        {
            Ip = ip;
            Host = ip;
            Port = port;
            Type = "http";
            _isFree = isFree;
        }

        public void Deserialize(string _json)
        {
            if (string.IsNullOrEmpty(_json)) return;
            if (!string.IsNullOrEmpty(Id))
            {
                int iPos = _json.IndexOf("{\"" + Id + "\":{", StringComparison.Ordinal);
                if (iPos != -1)
                {
                    _json = _json.Substring(iPos + Id.Length + 4);
                    iPos = _json.LastIndexOf("}}", StringComparison.Ordinal);
                    _json = _json.Substring(0, iPos + 1);
                }
            }
            using (StringReader sr = new StringReader(_json))
            {
                Deserializer.Populate(new JsonTextReader(sr), this);
            }
        }

        public WebProxy ToWebProxy(ParsingPageType typeParsingPageFor)
        {
            try
            {
                if (_minTimeToReUseProxy == null)
                {
                    _minTimeToReUseProxy = new TimeSpan(0, 0, Settings.ProxyServiceSettings.MinSecToReUseProxy);
                }
                if (_lockers.TryGetValue(typeParsingPageFor, out object locker))
                {
                    lock (locker)
                    {
                        // Высчитываем сколько времени прошло после последнего обращения по данному прокси для данного типа страницы
                        if (!_isFree && typeParsingPageFor != ParsingPageType.Undefined 
                                     && _historyUsingDictionary.TryGetValue(typeParsingPageFor, out var lastUsedTime))
                        {
                            var diffTime = DateTime.Now - lastUsedTime;

                            // Если нам ждать больше 30 сек, то выйдем, пусть сменит прокси на другую.
                            // Такая задержка может быть если у нас в конфигах установлено такое значение или у нас задудосеная прокся
                            Log.Info(Ip+" diffTime.sec = " + diffTime.TotalSeconds);
                            if (diffTime.TotalSeconds < -30)
                            {
                                Log.Trace(Ip + " too big delay");
                                return null;
                            }

                            if (diffTime.TotalSeconds < _minTimeToReUseProxy.Value.Seconds)
                            {
                                var delay = (int) (_minTimeToReUseProxy.Value.TotalMilliseconds - diffTime.TotalMilliseconds);
                                if (delay > 1000)
                                    Log.Trace("Proxy [" + Ip + "] for [" + typeParsingPageFor + "] delay ms= " + delay);
                                Thread.Sleep(delay);
                            }
                        }

                        _historyUsingDictionary.AddOrUpdate(typeParsingPageFor, DateTime.Now, (key, oldValue) => DateTime.Now);
#if DEBUG
                        var result = new WebProxy();
#else
                        var result = new WebProxy(Type + "://" + Host + ":" + Port);
#endif
                        if (!string.IsNullOrEmpty(UserLogin) && !string.IsNullOrEmpty(UserPassword))
                        {
                            result.Credentials = new NetworkCredential(UserLogin, UserPassword);
                        }

                        return result;
                    }
                }

                _lockers.AddOrUpdate(typeParsingPageFor, new object(), (key, oldValue) => oldValue);
                return ToWebProxy(typeParsingPageFor);
            }
            catch (Exception ex)
            {
                Log.Fatal("Не смогли создать проксю: " + ex.GetType() + "\t" + ex.Message + "\n" + ex.StackTrace);
            }
            return null;
        }

        public bool PingHost()
        {
            try
            {
                if (!int.TryParse(Port, out var port)) return false;
                using (var client = new TcpClient(Ip, port))
                    return true;

            }
            catch (SocketException)
            {
                return false;
            }
        }

        /// <summary>
        /// Обновить время последнего использования прокси (для задержки от дудоса)
        /// </summary>
        /// <param name="newTime"></param>
        /// <param name="parsingPageTypeFor"></param>
        public void UpdateTime(DateTime newTime, ParsingPageType parsingPageTypeFor)
        {
            if (!_lockers.TryGetValue(parsingPageTypeFor, out var locker)) return;
            lock (locker)
            {
                if (_historyUsingDictionary.TryGetValue(parsingPageTypeFor, out var oldTime)
                    && _historyUsingDictionary.TryUpdate(parsingPageTypeFor, newTime, oldTime))
                {
                    return;
                }
                Log.Warn("не удалось увеличить время последнего использования прокси при дудосе");
            }

        }

        #region Cookie management
        public CookieContainer GetCookieContainer(ParsingPageType typeParsingPageFor)
        {
            return _cookieDictionary.GetOrAdd(typeParsingPageFor, pageType => new CookieContainer());
        }

        internal void SerializeCookies()
        {
            try
            {
                lock (Locker)
                {
                    var dictionary = new Dictionary<ParsingPageType, List<JsonCookie>>();
                    foreach (var pair in _cookieDictionary)
                    {
                        var list = new List<JsonCookie>();
                        foreach (Cookie cookie in pair.Value.GetAllCookies())
                        {
                            list.Add(new JsonCookie(cookie));
                        }

                        dictionary.Add(pair.Key, list);
                    }

                    var json = JsonConvert.SerializeObject(dictionary);
                    File.WriteAllText(FileNameCookies, json);
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }

        internal void DeserializeCookies()
        {
            try
            {
                if (!File.Exists(FileNameCookies)) return;
                var json = File.ReadAllText(FileNameCookies);
                var dictionary = JsonConvert.DeserializeObject<Dictionary<ParsingPageType, List<JsonCookie>>>(json);

                foreach (var pair in dictionary)
                {
                    var cookieContainer = new CookieContainer();
                    foreach (var jsonCookie in pair.Value)
                    {
                        var cookie = jsonCookie.ToCookie();
                        cookieContainer.Add(cookie);
                    }

                    _cookieDictionary.TryAdd(pair.Key, cookieContainer);
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }
        
        [JsonObject(Id = "cookie", MemberSerialization = MemberSerialization.OptIn)]
        private class JsonCookie
        {
            #region Properties
            [JsonProperty("domain")]
            public string Domain { get; set; }

            [JsonProperty("comment")]
            public string Comment { get; set; }

            [JsonProperty("commentUri")]
            public Uri CommentUri { get; set; }

            [JsonProperty("discard")]
            public bool Discard { get; set; }

            [JsonProperty("expired")]
            public bool Expired { get; set; }

            [JsonProperty("expires")]
            public DateTime Expires { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("path")]
            public string Path { get; set; }

            [JsonProperty("port")]
            public string Port { get; set; }

            [JsonProperty("secure")]
            public bool Secure { get; set; }

            [JsonProperty("httponly")]
            public bool HttpOnly { get; set; }

            [JsonProperty("value")]
            public string Value { get; set; }

            [JsonProperty("timestamp")]
            public DateTime TimeStamp { get; set; }

            [JsonProperty("version")]
            public int Version { get; set; }
            #endregion

            public JsonCookie(Cookie cookie)
            {
                if (cookie == null)
                    return;

                Value = cookie.Value;
                Comment = cookie.Comment;
                CommentUri = cookie.CommentUri;
                Discard = cookie.Discard;
                Domain = cookie.Domain;
                Expired = cookie.Expired;
                Expires = cookie.Expires;
                HttpOnly = cookie.HttpOnly;
                Name = cookie.Name;
                Path = cookie.Path;
                Port = cookie.Port;
                Secure = cookie.Secure;
                TimeStamp = cookie.TimeStamp;
                Version = cookie.Version;
            }

            public Cookie ToCookie()
            {
                var cookie = new Cookie
                {
                    Value = Value,
                    Comment = Comment,
                    CommentUri = CommentUri,
                    Discard = Discard,
                    Domain = Domain,
                    Expired = Expired,
                    Expires = Expires,
                    HttpOnly = HttpOnly,
                    Name = Name,
                    Path = Path,
                    Port = Port,
                    Secure = Secure,
                    //TimeStamp = TimeStamp,
                    Version = Version
                };


                return cookie;
            }
        }

        #endregion
    }
}

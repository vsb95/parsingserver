﻿using System;
using System.Web.Http;
using Logger;
using Microsoft.Owin.Hosting;
using Owin;

namespace ProxyService.WebApi
{
    public class Startup
    {
        // This code configures Web API. The Startup class is specified as a type
        // parameter in the WebApp.Start method.
        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for self-host. 
            HttpConfiguration config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            appBuilder.UseWebApi(config);
        }
    }

    public class Api : IDisposable
    {
        private IDisposable _owinHost;

        public bool Start(string baseAddress)
        {
            try
            {
                Log.Trace("Starting WebApi");
                _owinHost = WebApp.Start<Startup>(url: baseAddress);
                Log.Info("WebApi Started by " + baseAddress);
                return true;
            }
            catch (Exception ex)
            {
                Log.Exception(ex.InnerException ?? ex, LogType.Fatal);
            }

            return false;
        }

        public void Dispose()
        {
            _owinHost?.Dispose();
            _owinHost = null;
        }
    }
}

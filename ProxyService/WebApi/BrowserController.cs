﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Logger;
using ProxyService.BrowserService;
using ProxyService.Exceptions;

namespace ProxyService.WebApi
{
    public class BrowserController : ApiController
    {
        public string Get(bool isOnlyPrivate = true, bool isCanFree = false)
        {
            try
            {
                Log.Trace("Get browser" );
                var browser = BrowserManager.GetNextFreeBrowser(isOnlyPrivate, isCanFree);
                browser.WaitIfNeed(true);
                return browser.Adress;
            }
            catch (Exception ex)
            {
                if (ex.Message != "<AllIsBusy>")
                    Log.Exception(ex, LogType.Fatal, true);
                return ex.Message;
            }
        }

        [HttpPost]
        public async Task<string> Post([FromBody] DownloadBrowserModel model)
        {
            try
            {
                ParsingPageType parsingPageType = ParsingPageType.Undefined;
                try
                {
                    parsingPageType = ParsingPageTypeIdentifier.Identify(model.Url);
                }
                catch (ArgumentOutOfRangeException aor)
                {
                }
                Log.Trace("Browser Download: ["+parsingPageType+"] isOnlyPrivate=" + model.isOnlyPrivate + " isCanFree=" + model.isCanFree);

                string result = null;
                for (int i = 0; i < 5 && (string.IsNullOrEmpty(result) || result == "<Timeout>"); i++)
                {
                    var browser = BrowserManager.GetNextFreeBrowser(model.isOnlyPrivate, model.isCanFree, parsingPageType);
                    if (browser != null)
                        result = await browser.DownloadAsync(model.Url);
                }
                return result;
            }
            catch (AllIsBusyException aibEx)
            {
                Log.Debug(aibEx.Message);
                return "<AllIsBusy>";
            }
            catch (Exception ex)
            {
                if(ex.Message != "<AllIsBusy>")
                    Log.Exception(ex, LogType.Fatal, true);
            }
            return null;
        }

        /// <summary>
        /// Создать временный браузер, не учавствующий в общем ранжировании
        /// </summary>
        /// <returns></returns>
        public string Put()
        {
            try
            {
                var browser = BrowserManager.CreateBrowser(true);
                return browser?.Adress;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal,true, "BrowserController::Put CreateBrowser");
            }

            return null;
        }

        public class DownloadBrowserModel
        {
            public string Url { get; set; }
            public bool isOnlyPrivate { get; set; }
            public bool isCanFree { get; set; }
        }
    }
}

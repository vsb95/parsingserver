﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Logger;
using ProxyService.BrowserService;
using ProxyService.Proxies;

namespace ProxyService.WebApi
{
    public class ProxyDeleteController : ApiController
    {
        public class DelayProxy
        {
            public String Ip { get; set; }
            public ParsingPageType ParsingPageTypeFor { get; set; }
            public int IsBrowser { get; set; }
        }


        public async void Get(string ip)
        {
            try
            {
                await Task.Factory.StartNew(() => ProxyManager.ExcludeFreeProxy(ip));
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }
        }

        /// <summary>
        /// Увеличить задержку на прокси тк через нее проходит дудос
        /// </summary>
        /// <param name="ip"></param>
        public void Post(DelayProxy delay)
        {
            Log.Trace("ProxyDelete: '"+delay.Ip+"' ["+delay.ParsingPageTypeFor+"] "+delay.IsBrowser);
            if (delay.IsBrowser == 1)
            {
                BrowserManager.UpdateDelayProxy(delay.Ip, delay.ParsingPageTypeFor);
            }
            else
            {
                ProxyManager.UpdateDelayProxy(delay.Ip, delay.ParsingPageTypeFor);
            }
        }
    }
}

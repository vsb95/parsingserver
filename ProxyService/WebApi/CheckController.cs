﻿using System.Web.Http;

namespace ProxyService.WebApi
{
    public class CheckController : ApiController
    {
        /// <summary>
        /// Health Check
        /// </summary>
        /// <returns></returns>
        public string Get()
        {
            return "1";
        }
    }
}

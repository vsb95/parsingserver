﻿using System;
using System.Web.Http;
using Logger;
using ProxyService.Proxies;

namespace ProxyService.WebApi
{
    public class ProxyController : ApiController
    {
        /// <summary>
        /// Получить прокси для определенного тпиа страницы
        /// /api/proxy?type=type&isOnlyPrivate=false
        /// </summary>
        /// <returns></returns>
        public Proxy Get(ParsingPageType type, bool isOnlyPrivate = true, bool isCanFree = false)
        {
            try
            {
                Log.Trace("Get proxy for " + type + " isOnlyPrivate = " + isOnlyPrivate + " isCanFree = " + isCanFree);

                var result = ProxyManager.GetProxy(type, isOnlyPrivate, isCanFree);
                Log.Trace("Return proxy for [" + type + "] ip: " + result?.Ip);
                return result;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

            return null;
        }

        public int Post()
        {
            return ProxyManager.ProxyCount;
        }
    }
}

﻿using System;
using System.IO;
using System.Threading;
using Logger;
using Newtonsoft.Json;
using ProxyService.BrowserService;
using ProxyService.Proxies;

namespace ProxyService
{
    public static class Settings
    {
        public static BrowserServiceSettings BrowserServiceSettings = null;
        public static ProxyServiceSettings ProxyServiceSettings = null;

        static Settings()
        {
            var refreshThread = new Thread(RefreshFunc) {IsBackground = true};
            refreshThread.Start();
        }

        public static void ReloadSettings()
        {
            try
            {
                ProxyServiceSettings = new ProxyServiceSettings();
                ProxyServiceSettings.Serialize();
                BrowserServiceSettings = new BrowserServiceSettings();
                BrowserServiceSettings.Serialize();
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error);
            }
        }

        private static void RefreshFunc()
        {
            while (true)
            {
                TimeZoneInfo moscowTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time");
                DateTime moscowDateTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, moscowTimeZone);

                var delayTimeSpan = new TimeSpan(23, 59, 59) - moscowDateTime.TimeOfDay + new TimeSpan(1, 0, 0);
                var delayMin = Math.Min((int) delayTimeSpan.TotalMinutes, 6 * 60);
                Log.Debug("Перезагружаю настройки через " + delayMin + " минут");
                Thread.Sleep(delayMin * 60 * 1000);
                ReloadSettings();
                ProxyManager.LoadProxies();
                BrowserManager.LoadBrowserList();
            }
        }
    }
    
    [JsonObject(Id = "proxy-service-settings", MemberSerialization = MemberSerialization.OptIn)]
    public class ProxyServiceSettings
    {
        private readonly JsonSerializer _deserializer = new JsonSerializer { NullValueHandling = NullValueHandling.Ignore };
        private string _fileName = "config.json";
        #region Properties
        
        [JsonProperty("min-sec-to-reuse-proxy")]
        public int MinSecToReUseProxy { get; set; } = 2;

        [JsonProperty("proxy-service-address")]
        public string ProxyServiceApiAdress { get; set; } = "http://localhost:9005/";
        #endregion

        public ProxyServiceSettings(string fileName = null)
        {
            if (!string.IsNullOrEmpty(fileName))
                _fileName = fileName;

            if (File.Exists(_fileName))
                Deserialize(File.ReadAllText(_fileName));
        }

        public void Serialize()
        {
            var json = JsonConvert.SerializeObject(this, Formatting.Indented);
            File.WriteAllText(_fileName, json);
        }

        public void Deserialize(string json)
        {
            if (string.IsNullOrEmpty(json)) return;
            using (StringReader sr = new StringReader(json))
            {
                _deserializer.Populate(new JsonTextReader(sr), this);
            }
        }
    }
    [JsonObject(Id = "browser-service-settings", MemberSerialization = MemberSerialization.OptIn)]
    public class BrowserServiceSettings
    {
        private readonly JsonSerializer _deserializer = new JsonSerializer { NullValueHandling = NullValueHandling.Ignore };
        private string _fileName = "BrowserServiceSettings.json";
        #region Properties

        /// <summary>
        /// Запускать и использовать BrowserService.exe
        /// </summary>
        [JsonProperty("is-enable-browser-services")]
        public bool IsEnableBrowserServices { get; set; } = true;

        /// <summary>
        /// Максимальное количество экземпляров BrowserService.exe
        /// </summary>
        [JsonProperty("max-count-browser-instance")]
        public int MaxCountBrowserInstance { get; set; } = 10;
        
        /// <summary>
        /// Максимальное количество экземпляров BrowserService.exe на бесплатный проксях
        /// </summary>
        [JsonProperty("max-count-free-browser-instance")]
        public int MaxCountFreeBrowserInstance { get; set; } = 10;

        [JsonProperty("min-port-to-create-browser")]
        public int MinPort { get; set; } = 9050;

        [JsonProperty("health-check-every-sec")]
        public int HealthCheckEverySec { get; set; } = 15;

        [JsonProperty("hostname")]
        public string HostName { get; set; } = "http://localhost";

        [JsonProperty("browser-exe-path")]
        public string ExePath { get; set; } = "BrowserService\\BrowserService.exe";

        [JsonProperty("download-timeout-sec")]
        public int DownloadTimeoutSec { get; set; } = 4;
        #endregion

        public BrowserServiceSettings(string fileName = null)
        {
            if (!string.IsNullOrEmpty(fileName))
                _fileName = fileName;

            if (File.Exists(_fileName))
                Deserialize(File.ReadAllText(_fileName));
        }

        public void Serialize()
        {
            var json = JsonConvert.SerializeObject(this, Formatting.Indented);
            File.WriteAllText(_fileName, json);
        }

        public void Deserialize(string json)
        {
            if (string.IsNullOrEmpty(json)) return;
            using (StringReader sr = new StringReader(json))
            {
                _deserializer.Populate(new JsonTextReader(sr), this);
            }
        }
    }
}

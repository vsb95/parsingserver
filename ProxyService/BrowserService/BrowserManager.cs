﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Logger;
using ProxyService.Exceptions;
using ProxyService.Proxies;

namespace ProxyService.BrowserService
{
    public static class BrowserManager
    {
        private static readonly object Locker = new object();
        private static List<Browser> _browserList = new List<Browser>();
        private static Thread _healthThread;
        
        public static Browser GetNextFreeBrowser(bool isOnlyPrivate = true, bool isCanFree = false,
            ParsingPageType parsingPageType = ParsingPageType.Undefined)
        {
            Browser result = null;
            var recomendedMinSecTried = _browserList.Count * 3;
            lock (Locker)
            {
                if (_browserList.Count == 0)
                {
                    Log.Error("Список браузеров не инициализирован");
                    throw new AllIsBusyException("<AllIsBusy>");
                }

                var browsers = _browserList.OrderByDescending(x => x.IsFree).ThenBy(x => x.IsShared).Where(x =>
                    !x.IsBusy && (!isOnlyPrivate || !x.IsShared) && (isCanFree || !x.IsFree)).ToList();
                foreach (var browser in browsers)
                {
                    if (!browser.IsCanUseFor(parsingPageType) ||
                        !((DateTime.Now - browser.LastTried).TotalSeconds > recomendedMinSecTried)) continue;
                    result = browser;
                    break;
                }
                if(result == null)
                    result = browsers.FirstOrDefault(x => x.IsCanUseFor(parsingPageType));

                if (browsers.Any() && result == null && _browserList.Count < Settings.BrowserServiceSettings.MaxCountBrowserInstance)
                {
                    // Если не осталось браузеров для этого типа страницы - поднимем новый
                    result = CreateBrowser(false);
                }

                if (result == null)
                {
                    result = _browserList.FirstOrDefault(x =>
                        (!isOnlyPrivate || !x.IsShared) && (!isCanFree || x.IsFree));
                    if (result == null)
                        throw new AllIsBusyException("<AllIsBusy>");
                }

                _browserList = _browserList.OrderByDescending(x => x.Priority).ToList();
                result.LastTried = DateTime.Now;
                
                return result;
            }
        }

        internal static Task LoadBrowserList()
        {
            return Task.Factory.StartNew(() =>
            {
                try
                {
                    Log.Trace("макс браузеров: " + Settings.BrowserServiceSettings.MaxCountBrowserInstance);

                    var browserBag = new ConcurrentBag<Browser>();
                    bool isStartingBrowser = false;
                    for (var i = 0;
                        i < ProxyManager.ProxyCount && i < Settings.BrowserServiceSettings.MaxCountBrowserInstance;
                        i++)
                    {
                        var proxy = ProxyManager.GetProxy(ParsingPageType.Undefined, false);
                        var port = Settings.BrowserServiceSettings.MinPort + i;
                        var adress = Settings.BrowserServiceSettings.HostName + ":" + port;
                        var browser = new Browser(proxy, adress);
                        if (!browser.IsAlive())
                        {
                            isStartingBrowser = true;
                            browser.Start();
                        }

                        browserBag.Add(browser);
                    }

                    if (isStartingBrowser)
                        Thread.Sleep(2500);
                    if (_healthThread == null)
                    {
                        _healthThread = new Thread(HealthCheckLoop) {IsBackground = true};
                        _healthThread.Start();
                    }

                    _browserList = browserBag.ToList();
                    Log.Info("Инициализировано " + _browserList.Count + " браузеров");
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, LogType.Fatal, true);
                }

                #region Free Browser

                try
                {
                    while (!ProxyManager.IsLoadedFreeProxy)
                    {
                        Thread.Sleep(500);
                    }

                    Log.Trace("макс бесплатных браузеров: " +
                              Settings.BrowserServiceSettings.MaxCountFreeBrowserInstance);

                    var browserBag = new ConcurrentBag<Browser>();
                    bool isStartingBrowser = false;
                    for (var i = _browserList.Count;
                        i < ProxyManager.AllProxyCount + _browserList.Count
                        && i < Settings.BrowserServiceSettings.MaxCountFreeBrowserInstance + _browserList.Count;
                        i++)
                    {
                        var proxy = ProxyManager.GetFreeProxy();
                        var port = Settings.BrowserServiceSettings.MinPort + i;
                        var adress = Settings.BrowserServiceSettings.HostName + ":" + port;
                        var browser = new Browser(proxy, adress);
                        if (!browser.IsAlive())
                        {
                            isStartingBrowser = true;
                            browser.Start();
                        }

                        browserBag.Add(browser);
                    }

                    if (isStartingBrowser)
                        Thread.Sleep(2500);
                    if (_healthThread == null)
                    {
                        _healthThread = new Thread(HealthCheckLoop) {IsBackground = true};
                        _healthThread.Start();
                    }

                    foreach (var freebrowser in browserBag)
                    {
                        _browserList.Add(freebrowser);
                    }

                    Log.Info("Инициализировано " + _browserList.Count + " браузеров");
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, LogType.Fatal, true);
                }

                #endregion
            });
        }

        /// <summary>
        /// Создать браузер. Если isTemporary=true то на полчаса, используя любой незанятый прокси и не включая в общий список
        /// </summary>
        /// <returns></returns>
        internal static Browser CreateBrowser(bool isTemporary)
        {
            Browser browser = null;
            Proxy proxy;
            string adress;
            int port;
            for (var i = 0; i < ProxyManager.ProxyCount; i++)
            {
                proxy = ProxyManager.GetProxy(ParsingPageType.Undefined, false);

                bool isFound = false;
                foreach (var item in _browserList)
                {
                    if (item.Proxy?.Ip != proxy?.Ip)
                        continue;
                    isFound = true;
                    break;
                }

                if (isFound) continue;

                port = Settings.BrowserServiceSettings.MinPort + i + 50;
                adress = Settings.BrowserServiceSettings.HostName + ":" + port;
                browser = new Browser(proxy, adress);
                if (browser.IsAlive())
                {
                    continue;
                }

                browser.Start();
                Thread.Sleep(10000); // дадим 10 сек стартануть
                if (isTemporary)
                {
                    Task.Factory.StartNew(() =>
                    {
#if DEBUG
                        Thread.Sleep(10 * 60 * 1000); // 10 минут
#else
                        Thread.Sleep(30 * 60 * 1000); // 30 минут
#endif
                        browser.Close();
                    });
                }
                else
                {
                    _browserList.Add(browser);
                }

                return browser;
            }

            if (!isTemporary)
            {
                return null;
            }

            Log.Warn("Нет свободных прокси для запуска приватного браузера. Создадим дубликат");

            proxy = ProxyManager.GetProxy(ParsingPageType.Undefined, false);
            port = Settings.BrowserServiceSettings.MinPort + 100;
            adress = Settings.BrowserServiceSettings.HostName + ":" + port;
            browser = new Browser(proxy, adress);
            if (browser.IsAlive())
            {
                Log.Debug("БРАУЗЕР ЖИВ!!");
                return browser;
            }

            browser.Start();
            Thread.Sleep(10000); // дадим 10 сек стартануть
            Task.Factory.StartNew(() =>
            {
#if DEBUG
                Thread.Sleep(10 * 60 * 1000); // 10 минут
#else
                Thread.Sleep(30 * 60 * 1000); // 30 минут
#endif
                browser.Close();
            });
            return browser;
        }

        /// <summary>
        /// Используется для временного исключения прокси из очереди на использование тк по ней идет дудос
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="pageTypeFor"></param>
        public static void UpdateDelayProxy(string ip, ParsingPageType parsingPageType)
        {
            foreach (var browser in _browserList)
            {
                if (browser.Proxy?.Ip != ip) continue;
                browser.SetBadPageType(parsingPageType);
                return;
            }

            Log.Error("Не найден браузер на увеличение таймаута: " + ip);
        }

        /// <summary>
        /// Закрыть все браузеры
        /// </summary>
        public static void CloseAll()
        {
            foreach(var browser in _browserList)
            {
                browser.Close();
            }
            _browserList = null;
        }
        private static void HealthCheckLoop()
        {
            while (true)
            {
                string adress = null;
                try
                {
                    List<Browser> badBrowsers = null;
                    Thread.Sleep(Settings.BrowserServiceSettings.HealthCheckEverySec * 1000);
                    foreach (var browser in _browserList)
                    {
                        adress = browser.Adress;
                        if (browser.IsAlive()) continue;
                        if (browser.IsFree)
                        {
                            if (badBrowsers == null)
                                badBrowsers = new List<Browser>();
                            Log.Warn("Смертельно опух свободный браузер на '" + adress + "', убиваю");
                            badBrowsers.Add(browser);
                            browser.Close();
                            continue;
                        }

                        Thread.Sleep(5000);
                        if (browser.IsAlive()) continue;
                        Thread.Sleep(2000);
                        if (browser.IsAlive()) continue;
                        Thread.Sleep(2000);
                        if (browser.IsAlive()) continue;
                        Thread.Sleep(2000);
                        if (browser.IsAlive()) continue;
                        Thread.Sleep(2000);
                        if (browser.IsAlive()) continue;

                        Log.Error("Смертельно опух браузер на '" + adress + "', перезапускаю", false);
                        browser.Start();
                        Thread.Sleep(2000);
                    }

                    if (badBrowsers != null)
                    {
                        foreach (var badBrowser in badBrowsers)
                        {
                            var browser = badBrowser;
                            if (!browser.IsFree)
                            {
                                Log.Error("Попытка удалить из списка браузеров - не свободный");
                                continue;
                            }

                            var res = _browserList.Remove(browser);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, LogType.Fatal, true);
                }
            }
        }

    }
}

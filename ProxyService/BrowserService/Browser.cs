﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Logger;
using Newtonsoft.Json;
using ProxyService.Exceptions;
using ProxyService.Proxies;

namespace ProxyService.BrowserService
{
    public class Browser : IComparable<Browser>
    {
        private readonly ConcurrentDictionary<ParsingPageType, DateTime> _badPageTypeDictionary =
            new ConcurrentDictionary<ParsingPageType, DateTime>();

        /// <summary>
        /// Максимальное количесво вкладок в браузере
        /// </summary>
        private const int MaxCountSubProcessesInBrowser = 3;

        /// <summary>
        /// Количесвто текущих процессов загрузок через этот браузер
        /// </summary>
        private int countSubProcesses = 0;

        private DateTime _lastTime = DateTime.Now;
        private Process _process;

        public DateTime LastTried { get; set; }
        /// <summary>
        /// Сгенерированный ID
        /// </summary>
        public Guid Id { get; set; }

        public Proxy Proxy { get; private set; }

        /// <summary>
        /// Адрес, на котором слушает этот браузер (локалхост)
        /// </summary>
        public string Adress { get; private set; }

        public bool IsBusy => countSubProcesses >= MaxCountSubProcessesInBrowser;

        /// <summary>
        /// Приоритет в очереди: чем больше - тем лучше
        /// </summary>
        public int Priority => (int) DateTime.Now.TimeOfDay.TotalSeconds - (int) _lastTime.TimeOfDay.TotalSeconds -
                               (countSubProcesses * 1000) * (IsShared ? 1 : 1000) * (IsFree ? 1 : 1000);

        /// <summary>
        /// Бесплатный прокси?
        /// </summary>
        public bool IsFree => Proxy?.IsFree ?? false;

        public bool IsShared => Proxy?.IsShared ?? false;


        public Browser(Proxy proxy, string adress)
        {
#if !DEBUG
            if(proxy == null)
                throw new NullReferenceException(nameof(proxy));
#endif
            Proxy = proxy;
            Id = Guid.NewGuid();
            Adress = adress;
        }

        /// <summary>
        /// Проверка по таймауту между запросами и степенью занятости браузера
        /// </summary>
        /// <param name="isUseDefaultDelay"></param>
        public void WaitIfNeed(bool isUseDefaultDelay = false)
        {
            var time = DateTime.Now - _lastTime;
            if (time.TotalSeconds > Settings.BrowserServiceSettings.DownloadTimeoutSec)
            {
                _lastTime = DateTime.Now;
                return;
            }

            if (IsBusy)
            {
                Log.Info("Ждем пока освободится браузер по адресу: " + Adress);
                while (IsBusy)
                    Thread.Sleep(100);
            }
            else if (isUseDefaultDelay)
            {
                var delay = Settings.BrowserServiceSettings.DownloadTimeoutSec - (int) time.TotalSeconds;
                Log.Trace("Ожидаем " + delay + " сек, прежде чем продолжить скачивать через браузер: " + Adress);
                Thread.Sleep(delay * 1000);
            }

            _lastTime = DateTime.Now;
        }

        public Task<string> DownloadAsync(string url)
        {
            return Task.Factory.StartNew(() => Download(url));
        }

        public string Download(string url)
        {
            WaitIfNeed();
            countSubProcesses++;
            try
            {
                Log.Trace("Download through browser '" + Adress + "': " + url);
                var requestAdress = Adress + "/api/download?url=" + Uri.EscapeDataString(url);
                var time = new Stopwatch();
                time.Start();
                var request = (HttpWebRequest) WebRequest.Create(requestAdress);

                request.Method = "GET";
                request.Timeout = 100000; //200 сек

                using (var response = (HttpWebResponse) request.GetResponse())
                {
                    string responseString = null;
                    using (var stream = new StreamReader(response.GetResponseStream()))
                    {
                        responseString = stream.ReadToEnd();
                    }

                    var message = JsonConvert.DeserializeObject<string>(responseString);
                    Log.Trace("\tDownload html through Browser '" + Adress + "' at " +
                              Math.Round(time.Elapsed.TotalSeconds, 3) + " sec");
                    if (message.Contains("<head></head><body></body>"))
                    {
                        if (IsFree)
                        {
                            Close();
                            ProxyManager.ExcludeFreeProxy(Proxy.Ip);
                            throw new Exception("Свободный браузер покинул нас. Убиваю процесс");
                        }

                        // не удалось скачать страницу через этот браузер
                        throw new WebException(message, null, WebExceptionStatus.UnknownError, null);
                    }

                    if (message.Contains("<AllIsBuzy>"))
                    {
                        throw new AllIsBusyException("Все вкладки заняты");
                    }
                    if (message.Contains("<Captcha>"))
                    {
                        var pageType = ParsingPageTypeIdentifier.Identify(url);
                        SetBadPageType(pageType, 60);
                    }

                    return message;
                }
            }
            catch (WebException wex)
            {
                if (wex.Status == WebExceptionStatus.Timeout)
                    Log.Warn("Отвалились по таймауту. Похоже браузеру тяжко");
                return "<Timeout>";
            }
            finally
            {
                countSubProcesses--;
            }
        }

        /// <summary>
        /// Health Check
        /// </summary>
        /// <returns></returns>
        public bool IsAlive()
        {
            try
            {
                // Если к этому браузеру 14 часов никто не прикасался - значит от забанен или уже мертв
                if (_lastTime.AddHours(14) < DateTime.Now)
                    return false;
                using (var client = new WebClient())
                {
                    var result = client.DownloadString(Adress + "/api/check");

                    if (!string.IsNullOrEmpty(result) && result == "\"1\"")
                        return true;
                }
            }
            catch (WebException wex)
            {
                if (wex.Status == WebExceptionStatus.ConnectFailure)
                    return false;
                if (wex.InnerException != null)
                    Log.Exception(wex.InnerException, LogType.Error, true);
                Log.Exception(wex, LogType.Fatal, true);
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

            return false;
        }

        /// <summary>
        /// Запустить процесс браузера
        /// </summary>
        public Task Start()
        {
            return Task.Factory.StartNew(() =>
             {
                 Close();
                 try
                 {
                     Log.Trace("Запускаем браузер на '" + Adress + "'");
                     if (Proxy != null)
                         _process = Process.Start(Settings.BrowserServiceSettings.ExePath,
                             $"{Proxy.Ip} {Proxy.Port} {Proxy.UserLogin} {Proxy.UserPassword} {Adress}");
                     else
                         _process = Process.Start(Settings.BrowserServiceSettings.ExePath, Adress);
                 }
                 catch (Exception e)
                 {
                     Log.Exception(e, LogType.Fatal, true, "ProxyService::Browser");
                 }
             });
        }

        /// <summary>
        /// Закрыть процесс браузера
        /// </summary>
        public void Close()
        {
            try
            {
                _process?.Refresh();
                if (_process != null && !_process.HasExited)
                    _process.Kill();
            }
            catch (Exception e)
            {
                Log.Exception(e, LogType.Debug);
            }
        }

        /// <summary>
        /// Проверка, можно ли этому браузеру качать выбранный сайт через свой прокси (а если дудос)
        /// </summary>
        /// <param name="pagetype"></param>
        /// <returns></returns>
        public bool IsCanUseFor(ParsingPageType pagetype)
        {
            if (pagetype == ParsingPageType.Undefined)
                return true;
            // Если плохой записи нет или срок "наказания" истек
            return !_badPageTypeDictionary.TryGetValue(pagetype, out var date) || DateTime.Now > date;
        }

        /// <summary>
        /// Запретить браузеру качать выбранный сайт через свой прокси (задудосили например)
        /// </summary>
        /// <param name="parsingPageType"></param>
        public void SetBadPageType(ParsingPageType parsingPageType, int minuteDelay = 1440)
        {
            Log.Info("Браузер [" + Proxy?.Ip + "] запрещен на сутки для " + parsingPageType);
            _badPageTypeDictionary.AddOrUpdate(parsingPageType, DateTime.Now.AddMinutes(minuteDelay),
                (type, oldTime) => oldTime.AddMinutes(minuteDelay));
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public int CompareTo(Browser other)
        {
            if (!IsBusy && other.IsBusy) return -1;
            if (IsBusy && !other.IsBusy) return 1;
            if (!IsShared && other.IsShared) return -1;
            if (IsShared && !other.IsShared) return 1;
            if (!IsFree && other.IsFree) return -1;
            if (IsFree && !other.IsFree) return 1;
            return -_lastTime.CompareTo(other._lastTime);
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append(Adress);
            builder.Append(" -> "+Proxy?.Ip);
            builder.Append(" LastUsed: "+_lastTime.TimeOfDay.ToString("hh:MM:ss"));
            builder.Append(IsBusy ? " Busy" : " NotBusy");
            if (IsShared)
                builder.Append(" Shared");
            if (IsFree)
                builder.Append(" Free");

            return builder.ToString();

        }
    }

}

﻿using System;

namespace ProxyService.Exceptions
{
    internal class AllIsBusyException : Exception
    {
        public AllIsBusyException(string message) : base(message){}
    }
}

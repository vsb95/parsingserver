﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using Logger;
using ProxyService.BrowserService;
using ProxyService.Proxies;
using ProxyService.WebApi;

namespace ProxyService
{
    class Program
    {
        static void Main(string[] args)
        {
            Api api = null;
            try
            {
                if (string.IsNullOrEmpty(Thread.CurrentThread.Name))
                    Thread.CurrentThread.Name = "ProxyService";
                var version = Assembly.GetExecutingAssembly().GetName().Version;
#if DEBUG
                Console.Title = "[DEBUG] ProxyService v" + version;
#else
                Console.Title = "ProxyService v" + version;
#endif
                AppDomain.CurrentDomain.ProcessExit += new EventHandler(CurrentDomain_ProcessExit);

                Process thisProc = Process.GetCurrentProcess();
                thisProc.PriorityClass = ProcessPriorityClass.High;

                Settings.ReloadSettings();
                Log.Info("Logger Initializing...");
                Log.Init(version, 30, 60*5, true);
                //Log.InitSlackLogger(Settings.CoreSettings.LoggerSettings.SlackToken, Settings.CoreSettings.LoggerSettings.SlackChannel, false);

                if (!ProxyManager.TryLoadCasheProxyList())
                    ProxyManager.LoadProxies();
                Log.Info("Загружено " + ProxyManager.ProxyCount + " прокси");

                BrowserManager.LoadBrowserList();


                api = new Api();
                if (!api.Start(Settings.ProxyServiceSettings.ProxyServiceApiAdress))
                {
                    throw new Exception("Почему то не запустился WebAPI");
                }

                Console.WriteLine("=================================");
                Console.WriteLine("All initialized");

                while (true)
                {
                    Thread.Sleep(200);
                }           
        
            }
            catch (Exception ex)
            {
                //Console.Clear();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("============= FATAL ERROR =============");
                Log.Exception(ex, LogType.Fatal, true);
                Console.WriteLine();
                Console.WriteLine();
                api?.Dispose();
                Console.WriteLine("Press key 'esc' to exit");
                while (true)
                {
                    var keyInfo = Console.ReadKey();
                    if (keyInfo.Key == ConsoleKey.Escape)
                        break;
                }
            }
            finally
            {
                api?.Dispose();
            }
        }

        static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            Console.WriteLine("exit");
            try
            {
                BrowserManager.CloseAll();
            }
            catch(Exception ex)
            {
                Log.Exception(ex, LogType.Error, true);
            }
        }
    }
}

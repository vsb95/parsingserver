﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Core;
using CoreParser.Pages;
using CoreParser.Services.Api;
using Logger;

namespace ParsingServer.Management
{
    public static class EnvManager
    {
        private static bool IsTestEnvSetted { get; set; }
        private static bool IsTestEnvAlive { get; set; }
        private static Thread HealthCheckThread;

        static EnvManager()
        {
            if (!SettingsManager.Settings.EnvironmentSettings.IsTestEnv
                && !string.IsNullOrEmpty(SettingsManager.Settings.EnvironmentSettings.TestApiAdress))
            {
                IsTestEnvSetted = true;
            }
        }

        public static Task SendToTestEnvAsync(AbstractAdvertisement adv)
        {
            return Task.Factory.StartNew(() =>
            {
                try
                {
                    if (IsTestEnvSetted && IsTestEnvAlive)
                    {
                        SendToApi(adv);
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, LogType.Error, true, "");
                }
            });
        }
        public static void SendToApi(AbstractAdvertisement page)
        {
            var requestUriString = SettingsManager.Settings.EnvironmentSettings.TestApiAdress + "api/pageupdate/";
            try
            {
                var request2 = new PageUpdateRequest(page);

                //if (!page.IsDeleted) request2.Html = page.HtmlPage;
                var data = request2.Serialize();
                {
                    WebRequest request = WebRequest.Create(requestUriString);
                    request.Method = "POST";
                    // преобразуем данные в массив байтов
                    byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(data);
                    // устанавливаем тип содержимого - параметр ContentType
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.ContentLength = byteArray.Length;

                    //записываем данные в поток запроса
                    using (Stream dataStream = request.GetRequestStream())
                    {
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Flush();
                        dataStream.Close();
                    }

                    WebResponse response = request.GetResponseAsync().Result;
                    using (Stream stream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            Console.WriteLine(reader.ReadToEnd());
                        }
                    }
                    response.Close();
                    Console.WriteLine("Запрос выполнен...");
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Warn);
            }
        }
    }
}

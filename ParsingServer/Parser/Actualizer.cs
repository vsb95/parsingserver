using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core;
using Core.Enum;
using Core.Extension;
using CoreParser.DB;
using CoreParser.DB.Managers;
using CoreParser.DB.Models;
using CoreParser.Pages;
using CoreParser.Services;
using Logger;

namespace ParsingServer.Parser
{
    /// <summary>
    /// Класс "Актуализатор"
    /// Проверяет все скачанные объявления на актуальность.
    /// </summary>
    public static class Actualizer
    {
        private static object locker = new object();
        private static int _deletedDirCount;
        private static ConcurrentBag<Task> _deleteDirTaskBag = new ConcurrentBag<Task>();
        private static ConcurrentQueue<AbstractAdvertisement> _throughtBrowserAdvQueue = new ConcurrentQueue<AbstractAdvertisement>();
        private static ConcurrentQueue<int> _mergeQueue = new ConcurrentQueue<int>();

        private static bool _isEndedActualize;
        private static int _offset;
        private static readonly Task ActualizeTask;

        private static readonly CancellationTokenSource TokenSource = new CancellationTokenSource();
        private static readonly CancellationToken CancellationToken;

        static Actualizer()
        {
            CancellationToken = TokenSource.Token;
            ActualizeTask = new Task(() =>
            {
                while (true) RefreshOldPages();
            }, CancellationToken, TaskCreationOptions.LongRunning);
        }

        public static void Start()
        {
            if (ActualizeTask.Status == TaskStatus.Running)
            {
                Log.Info("Процесс актуализации уже запущен");
                return;
            }
            _offset = 0;
            ActualizeTask.Start();
            Task.Factory.StartNew(() =>
            {
                var yandexPages = new List<AbstractAdvertisement>(25000);
                Thread.Sleep(5*60000);
                var tasks = new List<Task>();
                bool isAllowableYandexDownload = false;
                while (true)
                {
                    if (!_throughtBrowserAdvQueue.TryDequeue(out var page))
                    {
                        if (yandexPages != null && yandexPages.Count > 0)
                        {
                            Log.Debug("Добавляем в очереди на актуализацию через браузер "+ yandexPages.Count + " яндекс объявлений");
                            foreach (var yandexPage in yandexPages.OrderBy(x=>x.LastRefreshDateTime))
                            {
                                _throughtBrowserAdvQueue.Enqueue(yandexPage);
                            }

                            yandexPages = null;
                            isAllowableYandexDownload = true;
                            continue;
                        }
                        Log.Debug("Нет объявлений в очереди на актуализацию через браузер");
                        Thread.Sleep(10*60*1000);
                        isAllowableYandexDownload = false;
                        continue;
                    }
                    if (yandexPages == null)
                        yandexPages = new List<AbstractAdvertisement>();

                    if (!isAllowableYandexDownload && page.Type == ParsingPageType.Yandex)
                    {
                        yandexPages.Add(page);
                        Thread.Sleep(200);
                        continue;
                    }

                    tasks.Add(Task.Factory.StartNew(() =>
                    {
                        CheckAdv(page);
                        lock (locker)
                            _offset--;
                    }));

                    #region Определяем, сейчас день или ночь и выставляем количество одновременно актуализируемых объявлений
                    // Днем можно 8 объявлений, ночью 20
                    var countByIteration = DateTime.Now.TimeOfDay.Hours > 9 && DateTime.Now.TimeOfDay.Hours < 22 ? 8 : 20;
                    #endregion

                    if (tasks.Count > countByIteration)
                    {
                        Task.WaitAll(tasks.ToArray(), 5*60*1000); // Ждем 5 минут
                        tasks.Clear();
                    }
                }
            }, CancellationToken, TaskCreationOptions.LongRunning, TaskScheduler.Default);
            Task.Factory.StartNew(() =>
            {
                if (string.IsNullOrEmpty(Thread.CurrentThread.Name))
                    Thread.CurrentThread.Name = "MergePagesTask";
                Thread.Sleep(15*60000); // через 15 мин начнем переклейку. тк пока нет нужды пока там с очередями разберутся
                
                int i = 0;
                while (true)
                {
                    while (!_isEndedActualize && DateTime.Now.TimeOfDay.Hours > 10 && DateTime.Now.TimeOfDay.Hours < 22)
                    {
                        Thread.Sleep(60000 * 5);
                        Log.Debug("Процесс склейки ждет окончания актуализации или интервала времени [22;10]");
                    }
                    if (!_mergeQueue.TryDequeue(out var idPage))
                    {
                        Log.Debug("Нет объявлений в очереди на склейку");
                        Thread.Sleep(10 * 60 * 1000);
                        i = 0;
                        continue;
                    }

                    DbMergeManager.MergeAdvertisment(idPage);
                    i++;
                    if (i % 100 == 0)
                    {
                        Log.Debug("Переклеено 100 объявлений. Осталось: "+ _mergeQueue.Count);
                    }
                }
            }, CancellationToken, TaskCreationOptions.LongRunning, TaskScheduler.Default);
        }

        public static void Stop()
        {
            TokenSource.Cancel();
        }


        private static void RefreshOldPages()
        {
            try
            {
                if (string.IsNullOrEmpty(Thread.CurrentThread.Name))
                    Thread.CurrentThread.Name = "Actualizer";
                Stopwatch searchRefreshOldPageWatch = new Stopwatch();
                int delayRefreshOldPage = 4 * 60 * 60 * 1000;
#if DEBUG
                delayRefreshOldPage = 1 * 60 * 60 * 1000; // 1 час
#endif
                _isEndedActualize = false;
                searchRefreshOldPageWatch.Start();
                int countRefreshed = 0;
                var watch = new Stopwatch();
                var actualPageList = DbActualizeManager.GetNotActualPages(out var totalCountNotActualized, _offset, 200);
                while (actualPageList.Count > 0)
                {
                    if (CancellationToken.IsCancellationRequested)
                        return;

                    #region Определяем, сейчас день или ночь и выставляем количество одновременно актуализируемых объявлений
                    int countByIteration = 0;
                    //Если уже день
                    if (DateTime.Now.TimeOfDay.Hours > 9 && DateTime.Now.TimeOfDay.Hours < 22)
                    {
                        countByIteration = SettingsManager.Settings.ActualizerSettings.ActualizePageCountByIterationDay;
                        if (countByIteration == 0)
                        {
                            // Ждем до 22 вечера
                            var d = new TimeSpan(22, 0, 0) - DateTime.Now.TimeOfDay;
                            Log.Info("Ожидаем 22 вечера, чтобы начать актуализацию...");
                            Thread.Sleep((int)d.TotalMilliseconds);
                        }
                    }
                    else
                    {
                        countByIteration = SettingsManager.Settings.ActualizerSettings.ActualizePageCountByIterationNight;
                    }
                    #endregion

                    watch.Restart();
                    for (var i = 0; i < actualPageList.Count; i+= countByIteration)
                    {
                        if (CancellationToken.IsCancellationRequested)
                            return;
                        var pages = actualPageList.GetRange(i, Math.Min(actualPageList.Count - i, countByIteration)).OrderBy(x=>x.Type);
                        var tasks = new List<Task>();
                        foreach (var page in pages)
                        {
                            // Если данное объявление нужн оскачивать через браузер - положим его в отдельную очередь и пойдем дальше
                            if (SettingsManager.Settings.ServerSettings.ParsingSettings.TryGetValue(page.Type, out var config))
                            {
                                if (config.IsDownloadThroughBrowser)
                                {
                                    lock (locker)
                                        _offset++;
                                    if (_throughtBrowserAdvQueue.Any(x => x.Id == page.Id))
                                        continue;
                                    _throughtBrowserAdvQueue.Enqueue(page);
                                    continue;
                                }
                            }

                            var task = Task.Factory.StartNew(() => { CheckAdv(page); });
                            tasks.Add(task);
                        }
                        if (CancellationToken.IsCancellationRequested)
                            return;
                        if (tasks.Count > 0)
                            Task.WaitAll(tasks.ToArray());
                    }

                    countRefreshed += actualPageList.Count;
                    var minToEnd = Math.Floor(searchRefreshOldPageWatch.GetTotalSeconds() / countRefreshed * totalCountNotActualized / 60);
                    Log.Debug("Актуализировано "+ actualPageList.Count+" объявлений за " + watch.GetTotalSeconds() + " сек\nДо окончания актуализации осталось примерно: "+ minToEnd+" мин");
                    actualPageList = DbActualizeManager.GetNotActualPages(out totalCountNotActualized, _offset, 200);
                }
                Log.Debug("Обновление старых страниц заняло: " + searchRefreshOldPageWatch.GetTotalSeconds() + " сек\nНачинаем форсировать IsAgency");
                if (CancellationToken.IsCancellationRequested)
                    return;
                searchRefreshOldPageWatch.Restart();
                CheckCountPagesWitoutSection();
                MlsnSetMls();
                _isEndedActualize = true;
                // if(!Merger.IsStarted) Merger.Start(false);
                Log.Debug("Форсированная установка IsAgency = false заняло: " + searchRefreshOldPageWatch.GetTotalSeconds() + " сек\nТеперь ждем " +
                          Math.Round((decimal)delayRefreshOldPage / 1000 / 60 / 60, 2) + " часов до следующего рефреша");
                Thread.Sleep(delayRefreshOldPage);

                if (CancellationToken.IsCancellationRequested)
                    return;
                searchRefreshOldPageWatch.Restart();
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal,true, "Actualizer::RefreshOldPages");
            }

        }

        /// <summary>
        /// Запускает задачу удаления фото, для которых не сущесвует страниц
        /// </summary>
        public static void DeleteImagesWithoutPage()
        {
            var watch = new Stopwatch();
            watch.Start();
            int pagesCount = 0;
            var goodDirectories = new List<string>();
            using (var context = new RieltorServiceDBEntities())
            {
                var query = context.Pages;
                pagesCount = query.Count();
                Log.Info("Начинаем загружать список актуальных директорий... Всего страниц: "+ pagesCount);
                foreach (var page in query)
                {
                    var image = page.Images.FirstOrDefault();
                    if(image == null)
                        continue;
                    var directory = Path.GetDirectoryName(image.Path);
                    goodDirectories.Add(directory);
                }
            }

            Log.Info("Начинаем обход директорий...  ");
            var folder = Path.Combine(SettingsManager.Settings.ServerSettings.ImageFolder, DateTime.Now.Year.ToString());
            var directories = new DirectoryInfo(folder);
            ReqursiveCheckDirectory(directories, goodDirectories, 0);
            Task.WaitAll(_deleteDirTaskBag.ToArray());
            Log.Info("Закончили. Всего удалили " + _deletedDirCount + " директорий. Заняло " +
                     Math.Round(watch.Elapsed.TotalMinutes, 2) + " минут");
        }

        private static void ReqursiveCheckDirectory(DirectoryInfo root, List<string> goodDirectories, int depth)
        {
            try
            {
                // Now find all the subdirectories under this directory.
                var subDirs = root.GetDirectories();
                if (subDirs.Length == 0)
                {
                    if (goodDirectories.Contains(root.FullName))
                        return;
                    _deletedDirCount++;
                    Log.Debug("Удалена директория: '" + root.FullName + "'");
                    root.Delete(true);
                }
                else
                {
                    foreach (DirectoryInfo dirInfo in subDirs)
                    {
                        if (dirInfo.Name.ToUpper() == "PNG" || dirInfo.Name.ToLower() == "original")
                            continue;

                        // Для каждого дня запустим поток
                        if (depth == 1)
                        {
                           var task = Task.Factory.StartNew(() =>
                                ReqursiveCheckDirectory(dirInfo, goodDirectories, depth + 1));
                            _deleteDirTaskBag.Add(task);
                        }
                        else
                        {
                            ReqursiveCheckDirectory(dirInfo, goodDirectories, depth + 1);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Exception(e, LogType.Error);
            }

        }
        
        public static void CheckCountPagesWitoutSection()
        {
            try
            {
                using (var context = new RieltorServiceDBEntities())
                {
                    var badpages = context.Pages.Where(x => x.IsFromOwnerSection == null 
                                                         && (!x.IsDeleted.HasValue || !x.IsDeleted.Value)).ToList();
                    if (badpages.Count <= 0) return;
                    // это обычно объявления из других городов, тк мы их не проверяем то и разделы мы не знаем
                    Log.Fatal("Найдено " + badpages.Count + " объявлений у которых не определен раздел на источнике");
                    foreach (var page in badpages)
                    {
                        page.IdMasterPage = null;
                        page.IsDeleted = true;
                        context.Pages.AddOrUpdate(page);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex,LogType.Error,true, "CheckCountPagesWitoutSection");
            }
        }

        public static async void MlsnSetMls(int maxIndex = 99999)
        {
            try
            {
                var watch = new Stopwatch();
                watch.Start();
                Log.Debug("[MLSN] начинаем поиск мультилистинга. maxIndex: " + maxIndex);

                var mlsnSelfmansList = await BrowserManager.GetMlsList(maxIndex);
                if (mlsnSelfmansList == null || mlsnSelfmansList.Count == 0)
                {
                    Log.Debug("[MLSN] загружено 0 объявлений. Выходим");
                    return;
                }

                try
                {
                    using (var context = new RieltorServiceDBEntities())
                    {
                        var badPages = context.Pages.Where(x => x.IsMls && !mlsnSelfmansList.Contains(x.URL));
                        foreach (var badPage in badPages)
                        {
                            badPage.IsMls = false;
                        }

                        await context.SaveChangesAsync();
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, LogType.Error, true, "[Actializer::MlsnSetMls] Try Unset IsMls Pages");
                }

                var advList = PageManager.GetAdvertisementsByUrl(mlsnSelfmansList, false).Advertisements;
                Log.Debug(
                    "[MLSN] Найдено " + advList.Count + " объектов млс за " + watch.GetTotalSeconds() + " сек");

                foreach (var adv in advList)
                {
                    if (adv.IsAgency && adv.IsMls && adv.IsForceIsAgency
                        && adv.IsFromOwnerSection.HasValue && !adv.IsFromOwnerSection.Value) continue;
                    adv.IsAgency = true;
                    adv.IsFromOwnerSection = false;
                    adv.IsMls = true;
                    adv.IsForceIsAgency = true;
                    DbManager.AddOrUpdatePage(adv);
                }

                Log.Debug("[MLSN] Обновили список млс объявлений за " + watch.GetTotalSeconds() + " сек");
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

        }


        private static void CheckAdv(AbstractAdvertisement page)
        {
            try
            {
                // Если у нас объявление из неизвестного города - то мы его удалим
                if (page.SearchIndex.StartsWith("-99"))
                {
                    page.IsDeleted = true;
                    if (!DbManager.AddOrUpdatePage(page))
                        throw new Exception("Страница не сохранена");
                    return;
                }

                var correctChecked = page.CheckActualy();
                if (!correctChecked)
                {
                    page.LastRefreshDateTime = page.LastRefreshDateTime?.AddHours(2) ?? DateTime.Now.AddHours(2); // Раз уж не удалось, то перенесем срок обновления на 2 часа позже
                    Log.Error("Почему то не удалось проверить актуальность: " + page.Url, true);
                }

                if (page.IsFromOwnerSection == null && !page.IsDeleted)
                {
                    // если это не из доски или через апи - то все ок
                    Log.Error("Обновляется страница без IsFromOwnerSection: " + page.SearchIndex +
                              " (Возможно это посуточная))\n" + page.Url);
                }

                DbManager.AddOrUpdatePage(page);
                if(page.Id == null)
                    throw new Exception("У страницы нет id во время актуализации. Как так то? "+page.SearchIndex+"\nUrl: "+page.Url);
                if (page.IsDeleted)
                {
                    Log.Debug("Объявление удалено, переклеиваем наследников");
                    DbMergeManager.UnMasterPage(page.Id.Value);
                }
                else
                {
                    _mergeQueue.Enqueue(page.Id.Value);
                }
                
                //EnvManager.SendToTestEnvAsync(page);
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "Actualizer::CheckAdv()");
            }
        }
    }
}

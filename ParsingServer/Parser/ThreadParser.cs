﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core;
using Core.Enum;
using CoreParser.DB;
using CoreParser.Services;
using Logger;

namespace ParsingServer.Parser
{
    public class ThreadParser : IDisposable
    {
        public static bool IsAutoStart = false;
        private readonly Task _parseTask;

        private readonly CancellationTokenSource _tokenSource = new CancellationTokenSource();
        private readonly CancellationToken _cancellationToken;

        private readonly object _locker = new object();
        private int _switcher;
        private int _offset;
        private readonly ParsingPageType _type;

        /// <summary>
        /// Время последней загрузки всех страниц (_maxPageCount = over9999)
        /// </summary>
        private DateTime _lastDownloadAllPages = DateTime.MinValue;
        private int _maxPageCount;
        private int _startDelay;

        public ThreadParser(ParsingPageType type, int maxPageCount, int offset = 1)
        {
            _type = type;
            _maxPageCount = maxPageCount;
            _cancellationToken = _tokenSource.Token;
            _parseTask = new Task(StartLoop, _cancellationToken, TaskCreationOptions.LongRunning);
            _offset = offset;
            if (!IsAutoStart)
            {
                Console.WriteLine("[CanBeNull] [" + _type + "] Задержка перед запуском в минутах: ");
                if (int.TryParse(Console.ReadLine(), out var delay))
                    _startDelay = delay;
            }
            else
            {
                _startDelay = 1;
            }
        }

        /// <summary>
        /// Запустить поток, с постоянным парсингом
        /// </summary>
        public void Start()
        {
            _parseTask.Start();
        }

        private void StartLoop()
        {
            try
            {
                if (string.IsNullOrEmpty(Thread.CurrentThread.Name))
                    Thread.CurrentThread.Name = "Parser_"+ _type;
                while (_startDelay > 0)
                {
                    Console.WriteLine("[" + _type + "] Задержка перед запуском " + _startDelay + " мин");
                    Thread.Sleep(Math.Min(_startDelay, 5) * 60 * 1000);
                    _startDelay -= 5;
                }
                while (true)
                {
                    Stopwatch searchNewPageStopwatch = new Stopwatch();
                    int delaySearchNewPage = 2*60*60*1000; // 2h
#if !DEBUG
                    if (SettingsManager.Settings.ServerSettings.IsEnableDayNightMode && DateTime.Now.TimeOfDay.Hours < 22 &&
                        DateTime.Now.TimeOfDay.Hours > 9)
                    {
                        // Ждем до 22 вечера
                        var d = new TimeSpan(22, 0, 0) - DateTime.Now.TimeOfDay;
                        Log.Info("Ожидаем 22 вечера, чтобы начать скачивать новые объявления...");
                        Thread.Sleep((int) d.TotalMilliseconds);
                    }

                    searchNewPageStopwatch.Start();
                    // Будем парсить до 9 утра
                    while (!SettingsManager.Settings.ServerSettings.IsEnableDayNightMode || DateTime.Now.TimeOfDay.Hours > 22 ||
                           DateTime.Now.TimeOfDay.Hours < 9)
#else
                    delaySearchNewPage = 120000;
                    searchNewPageStopwatch.Start();
#endif
                    {
                        if (_cancellationToken.IsCancellationRequested)
                            return;

                        lock (_locker)
                        {
                            Log.Trace(_type + " Начинаем процесс обновления");
                            DownloadAndParse();
                            Log.Debug("Поиск новых страниц занял: " +
                                      (searchNewPageStopwatch.ElapsedMilliseconds / 1000) + " сек");
                        }

                        switch (_type)
                        {
                            case ParsingPageType.Yandex:
                                _maxPageCount = 2;
                                break;
                            default:
                                _maxPageCount = 1;
                                break;
                        }

                        //ночью можно поставить загрузку всех страниц, так сказать "загрузим базу полностью"
                        if ((DateTime.Now.Hour > 22 || DateTime.Now.Hour < 6) &&
                            (DateTime.Now - _lastDownloadAllPages).TotalHours >= 24)
                        {
                            _maxPageCount = 9999999;
                        }
#if DEBUG
                        Log.Info("["+ _type + "] Закончили обновление");
                        return;
#else
                        var timePassed = searchNewPageStopwatch.ElapsedMilliseconds;
                        var rand = new Random();
                        var delay = delaySearchNewPage - rand.Next(60*60*1000);
                        delay = delay < 0 ? 0 : delay;
                        Log.Trace(_type + " Закончили процесс обновления. Ожидаем " + (delay / 1000) +
                                  " сек до следующего раза");
                        Thread.Sleep(delay);
                        searchNewPageStopwatch.Restart();
#endif
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error);
            }
        }

        private void DownloadAndParse()
        {
            bool isEndBoard = false;
            int downloadedCount = 0;
            int unDownloadedCount = 0;

            var urlQueue = CityManager.GetAllUrlQueue(_type, _maxPageCount);
            Log.Info("[" + _type + "] Загружено " + urlQueue.Count + " ссылкок на доски");
            while (urlQueue.Count > 0 && !_cancellationToken.IsCancellationRequested)
            {
                try
                {
                    if (_cancellationToken.IsCancellationRequested)
                        return;
                    var tasks = new Task[3];
                    for (int i = 0; i < 3; i++)
                    {
                        if (urlQueue.Count == 0 || _cancellationToken.IsCancellationRequested)
                            break;
                        tasks[i] = Task.Factory.StartNew(() =>
                        {
                            if (!urlQueue.TryDequeue(out var boardUrl)) return;
                            Log.Info("Начинаем скачивать " + boardUrl);
                            var result = PageManager.GetAdvertisementsByUrl(boardUrl, false);
                            downloadedCount += result.Advertisements.Count;
                            var deletedAdv = result.Advertisements.Where(x => x.IsDeleted).ToList();
                            if (deletedAdv.Count > 0)
                            {
                                Log.Info("Найдено "+ deletedAdv.Count()+" объявлений считающимися удаленными");

                                foreach (var adv in deletedAdv)
                                {
                                    adv.IsDeleted = false;
                                    DbManager.AddOrUpdatePage(adv);
                                }
                            }

                            if (result.UnParsedAdvertisements.Count <= 0) return;

                            #region #2 попытка
                            result = PageManager.GetAdvertisementsByUrl(result.UnParsedAdvertisements.Select(x=>x.Url), false);
                            downloadedCount += result.Advertisements.Count;
                            deletedAdv = result.Advertisements.Where(x => x.IsDeleted).ToList();
                            if (deletedAdv.Count > 0)
                            {
                                Log.Info("Найдено " + deletedAdv.Count + " объявлений считающимися удаленными");

                                foreach (var adv in deletedAdv)
                                {
                                    adv.IsDeleted = false;
                                    DbManager.AddOrUpdatePage(adv);
                                }
                            }
                            unDownloadedCount += result.UnParsedAdvertisements.Count;
                            if (result.UnParsedAdvertisements.Count > 0)
                            {
                                Log.Fatal(
                                    "После 2й попытки не удалось скачать некоторые объявления при парсинге сайта:\n" +
                                    string.Join("\n\t", result.UnParsedAdvertisements.Select(x => x.Url)));

                            }
                            #endregion

                        }, _cancellationToken);
                    }

                    Task.WaitAll(tasks);
                    if (_cancellationToken.IsCancellationRequested)
                        return;
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, LogType.Error, true);
                }
            }
            Log.Info("Всего загружено [" + _type + "]: " + downloadedCount+" не загружено: "+unDownloadedCount);
        }

        public void Stop()
        {
            _tokenSource.Cancel();
        }

        public void Dispose()
        {
            Stop();
        }
    }
}

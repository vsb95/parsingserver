﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Core.Services.RabbitMQueue;
using CoreParser.DB.Managers;
using Logger;

namespace ParsingServer.Parser
{
    public static class PhoneParser
    {
        private static Task ParseTask;

        private static readonly CancellationTokenSource TokenSource = new CancellationTokenSource();
        private static readonly CancellationToken CancellationToken;

        static PhoneParser()
        {
            CancellationToken = TokenSource.Token;
        }

        public static void Start()
        {
            ParseTask = new Task(PutTasksInQueue, CancellationToken);
            ParseTask.Start();
        }

        public static void Stop()
        {
            TokenSource.Cancel();
        }

        /// <summary>
        /// Метод определит у каких страниц нет номера телефона и поставит их в очередь RabbitMQ, для BrowserService
        /// </summary>
        private static void PutTasksInQueue()
        {
            try
            {
                if (string.IsNullOrEmpty(Thread.CurrentThread.Name))
                    Thread.CurrentThread.Name = "PhoneParser";
                if (CancellationToken.IsCancellationRequested)
                    return;
                var actualPageList = DbActualizeManager.GetPagesWithoutPhone();
                if (actualPageList.Count == 0)
                    return;
                using (var mq = new RabbitMQPublisher())
                {
                    mq.MessageEvent += message => Log.Debug(message);
                    mq.Start();
                    foreach (var page in actualPageList)
                    {
                        if (CancellationToken.IsCancellationRequested)
                            return;
                        mq.Send(string.Format("GETPHONE {0}:{1}", page.Id, page.Url));
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error);
            }
        }
    }
}

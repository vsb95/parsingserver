﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Core.Extension;
using CoreParser.DB.Managers;
using Logger;

namespace ParsingServer.Parser
{
    /// <summary>
    /// Склеиватель объявлений
    /// </summary>
    public static class Merger
    {
        private static readonly Task MergeTask;

        private static readonly CancellationTokenSource TokenSource = new CancellationTokenSource();
        private static readonly CancellationToken CancellationToken;

        public static bool IsStarted => MergeTask.Status == TaskStatus.Running;
        private static bool _isIgnoreTimeOffset = false;

        static Merger()
        {
            CancellationToken = TokenSource.Token;
            MergeTask = new Task(() =>
            {
                while (true)
                {
                    if (_isIgnoreTimeOffset || DateTime.Now.TimeOfDay.Hours < 9 && DateTime.Now.TimeOfDay.Hours > 22)
                    {
                        AnalizeAndMerge();
                        Thread.Sleep(12 * 60 * 60 * 1000); // каждые 12 часов
                    }
                    else
                    {
                        var secToNight = (22 - DateTime.Now.TimeOfDay.Hours) * 60 * 60;
                        Log.Info("Склеиватель ожидает 22 вечера. Запустится через: "+(secToNight/60) +" минут");
                        Thread.Sleep(secToNight*1000);
                    }
                }
            }, CancellationToken);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isIgnoreTimeOffset">Запустить прямо сейчас?</param>
        public static void Start(bool isIgnoreTimeOffset = false)
        {
            if (MergeTask.Status == TaskStatus.Running)
            {
                Log.Info("Процесс склейки уже запущен");
                return;
            }
            _isIgnoreTimeOffset = isIgnoreTimeOffset;
            MergeTask.Start();
        }

        public static void Stop()
        {
            TokenSource.Cancel();
        }


        private static void AnalizeAndMerge()
        {
            try
            {
                if (string.IsNullOrEmpty(Thread.CurrentThread.Name))
                    Thread.CurrentThread.Name = "Merger";
                
                Stopwatch watch = new Stopwatch();
                Stopwatch innerWatch = new Stopwatch();
                watch.Start();
                int counter = 0;
                int offset = 0;
                while (true)
                {
                    var pages = DbMergeManager.GetNotMergedPages(offset);
                    if(pages.Count == 0)
                        break;
                    foreach (var adv in pages)
                    {
                        if (!DbMergeManager.MergeAdvertisment(adv.Id.Value))
                        {
                            offset++;
                            continue;
                        }
                        counter++;
                        if (counter % 100 == 0)
                        {
                            Log.Debug("склеили 100 объявлений за " + innerWatch.GetTotalSeconds() + " сек");
                            innerWatch.Restart();
                        }
                    }
                    // Немного передохнем. а то у нас сервачок так опухнет и апишка туго работает
                    Thread.Sleep(5000);
                }

                Log.Info("Завершен процесс склейки за " + watch.GetTotalMinutes() + " мин");
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "Merger::AnalizeAndMerge");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CoreParser.DB;
using CoreParser.Pages;
using CoreParser.Services;
using Logger;
using Newtonsoft.Json;

namespace ParsingServer.Parser
{
    public static class ExportManager
    {
        public static List<ExportItem> ExportDataForKeywords(string filename = "keywords.csv")
        {
            if (!File.Exists(filename))
                throw new FileNotFoundException();

            var result = new List<ExportItem>();
            try
            {
                var lines = File.ReadAllLines(filename);
                foreach (var line in lines)
                {
                    var columns = line.Replace("&quot;", "_quot_").Replace("quot;", "_quot_").Split(';');
                    var lowerKey = columns.FirstOrDefault()?.ToLower();
                    if (string.IsNullOrEmpty(lowerKey)
                        || columns.Length < 5
                        || lowerKey.Contains("ключевик") || lowerKey.Contains("условия для продажи") ||
                        lowerKey.Contains("плохие продажа") || lowerKey.Contains("хорошие продажа")
                        || lowerKey.Contains("плохие аренда") || lowerKey.Contains("хорошие аренда") ||
                        lowerKey.Contains("слова исключения"))
                        continue;

                    var key = columns.FirstOrDefault().Replace("\"", "").Replace(",", "").Replace("\\", "\"");

                    var exampleDescription = columns[1]?.Replace("_quot_", "quot;").Replace("\"\"", "_$%$_")
                        .Replace("\"", "").Replace("_$%$_", "\"");
                    var exampleUrl = columns[2];
                    var isAgency = columns[3] == "1";
                    AbstractAdvertisement page = null;
                    if (!string.IsNullOrEmpty(exampleUrl))
                    {

                        //ToDo Если страница удалена, то даже описание не будет проверяться
                        page = DbManager.GetAbstractAdvertisementByUrl(exampleUrl) 
                               ?? PageManager.GetAdvertisementsByUrl(exampleUrl, false).Advertisements.FirstOrDefault();
                        if (page != null && string.IsNullOrEmpty(page.Description))
                            page.CheckActualy();
                        if (page == null)
                            Log.Warn("Не удалось скачть HTML страницы: " + exampleUrl);

                    }

                    var item = new ExportItem
                    {
                        Url = exampleUrl,
                        OriginalDescription = page?.Description,
                        Keyword = key,
                        IsAgency = isAgency,
                        TestDescription = exampleDescription
                    };
                    result.Add(item);
                }

                File.WriteAllText("keys.json", string.Join(",\r\n", result.Select(x => x.Serialize())));
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error);
            }

            return result;
        }

    }

    [JsonObject(Id = "export-item", MemberSerialization = MemberSerialization.OptIn)]
    public class ExportItem
    {
        [JsonProperty("Keyword")]
        public string Keyword { get; set; }

        [JsonProperty("IsAgency")]
        public bool IsAgency { get; set; }

        [JsonProperty("Url")]
        public string Url { get; set; }

        [JsonProperty("TestDescription")]
        public string TestDescription { get; set; }

        [JsonProperty("OriginalDescription")]
        public string OriginalDescription { get; set; }

        
        private readonly JsonSerializer _deserializer = new JsonSerializer { NullValueHandling = NullValueHandling.Ignore };

        public string Serialize()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public void Deserialize(string json)
        {
            if (string.IsNullOrEmpty(json)) return;
            using (StringReader sr = new StringReader(json))
            {
                _deserializer.Populate(new JsonTextReader(sr), this);
            }
        }

        public override string ToString()
        {
            return string.Format("{0}:{1} in '{2}'", Keyword, IsAgency, TestDescription ?? (string.IsNullOrEmpty(OriginalDescription) ? OriginalDescription : "null"));
        }
    }
}

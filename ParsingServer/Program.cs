using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using Core;
using Core.Enum;
using Core.Services;
using Core.Services.Geocoding.Sputnik;
using CoreParser;
using CoreParser.DB;
using CoreParser.DB.Managers;
using CoreParser.Pages;
using CoreParser.Services;
using CoreParser.Services.Proxies;
using Logger;
using ParsingServer.Parser;
using Console = System.Console;


namespace ParsingServer
{
    class Program
    {
        static void Main(string[] args)
        {
            ThreadParser avitoThreadParser = null;
            ThreadParser mlsnThreadParser = null;
            ThreadParser cianThreadParser = null;
            ThreadParser yandexThreadParser = null;
            ThreadParser ngsThreadParser = null;
            try
            {
                if(string.IsNullOrEmpty(Thread.CurrentThread.Name))
                    Thread.CurrentThread.Name = "ParsingServer";
                SettingsManager.IsStartAsAPI = false;
                SettingsManager.ReloadSettings();
                if (SettingsManager.Settings.ServerSettings.SelectedCityId >= 0)
                    CityManager.SelectCity(SettingsManager.Settings.ServerSettings.SelectedCityId);
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                var version = Assembly.GetExecutingAssembly().GetName().Version;
#if DEBUG
                Console.Title = "[DEBUG] Parsing Server v" + version;
                Console.ForegroundColor = ConsoleColor.Green;
#else
                Console.Title ="Parsing Server v" + version;
                Console.ForegroundColor = ConsoleColor.White;
#endif

                Process thisProc = Process.GetCurrentProcess();
                thisProc.PriorityClass = ProcessPriorityClass.BelowNormal;

                ProxyManager.Init();
                Log.Info("Logger Initializing...");
                Log.Init(version, SettingsManager.Settings.LoggerSettings.MinCountMessageToSendLog, SettingsManager.Settings.LoggerSettings.MinSecIntervalToSendLog, SettingsManager.Settings.LoggerSettings.IsEnabledConsoleTrace);
                //Log.InitTelegramLogger(Settings.SettingsDictionary[Setting.TelegramBotApiToken], ProxyManager.GetNotRussianProxy());
                Log.InitSlackLogger(SettingsManager.Settings.LoggerSettings.SlackToken, SettingsManager.Settings.LoggerSettings.SlackChannel);
                DictionaryStorage.Init();
                DbManager.Init();

                if (args != null && args.Length > 0)
                {
                    ThreadParser.IsAutoStart = true;
                    Console.WriteLine("Аргументы: {0}", string.Join(", ", args));
                    foreach (var arg in args)
                    {
                        switch (arg)
                        {
                            case "actualize":
                                Actualizer.Start();
                                break;
                            case "omsk":
                                CityManager.SelectCity(0);
                                break;
                            case "nsk":
                                CityManager.SelectCity(1);
                                break;
                            case "autostart":
                                avitoThreadParser?.Dispose();
                                avitoThreadParser = CreateAndStartParsing(ParsingPageType.Avito, 99999);
                                mlsnThreadParser?.Dispose();
                                mlsnThreadParser = CreateAndStartParsing(ParsingPageType.Mlsn, 99999);
                                ngsThreadParser?.Dispose();
                                ngsThreadParser = CreateAndStartParsing(ParsingPageType.N1, 99999);
                                yandexThreadParser?.Dispose();
                                yandexThreadParser = CreateAndStartParsing(ParsingPageType.Yandex, 99999);
                                break;
                        }
                    }
                    ThreadParser.IsAutoStart = false;
                }
#if DEBUG
                
               /* var listt = new List<string>
                {
                    "",
                    //"https://omsk.mlsn.ru/arenda-kommercheskaja-nedvizhimost/ofisnoe-pomeshhenie-ul-70-let-oktyabrya-8-id650441/",
                };
                var rest = PageManager.GetAdvertisementsByUrl(listt,false);
                var pg = rest.Advertisements?.FirstOrDefault();
                var ad = pg?.IsAgency;
                var ttay = pg.SellType;*/
#endif
                while (true)
                {
                    Console.WriteLine("=============================================");
                    Console.WriteLine("Выбери один из пунктов:");
                    Console.WriteLine("1. ");
                    Console.WriteLine("2. Парсинг");
                    Console.WriteLine("3. Привязать микрорайоны к районам");
                    Console.WriteLine("4. Скачать номера телефонов");
                    Console.WriteLine("5. Обновление страниц");
                    Console.WriteLine("6. ");
                    Console.WriteLine("7. Удалить фото, для которых нет страниц");
                    Console.WriteLine("8. ");
                    Console.WriteLine("10. DEBUG");
                    Console.WriteLine("9. Выход");
                    Console.WriteLine("=============================================");
                    var input = Console.ReadLine();
                    try
                    {
                        if (!int.TryParse(input, out var result))
                            continue;
                        switch (result)
                        {
                            case 1:
                                break;
                            case 2:
                            {
                                    if (CityManager.SelectedCity == null) CityManager.HandSelectCity();
                                    Console.WriteLine("Максимальный индекс стараницы: [CanBeNull]");
                                    int maxPageCount = 9999;
                                    input = Console.ReadLine();
                                    if (!int.TryParse(input, out maxPageCount))
                                        maxPageCount = 9999999;
                                    Console.WriteLine("");
                                    Console.WriteLine("Выбери пункты (через пробел):");
                                    Console.WriteLine("1. Яндекс");
                                    Console.WriteLine("2. Авито");
                                    Console.WriteLine("3. МЛСН");
                                    Console.WriteLine("4. ЦИАН");
                                    //Console.WriteLine("5. N1");
                                    Console.WriteLine("9. Выход");
                                    Console.WriteLine("=============================================");

                                    input = Console.ReadLine();
                                    foreach (var task in input.Split(' '))
                                    {
                                        if (!int.TryParse(task, out result))
                                            continue;
                                        switch (result)
                                        {
                                            case 1:
                                                yandexThreadParser?.Dispose();
                                                yandexThreadParser = CreateAndStartParsing(ParsingPageType.Yandex, maxPageCount);
                                                break;
                                            case 2:
                                                avitoThreadParser?.Dispose();
                                                avitoThreadParser = CreateAndStartParsing(ParsingPageType.Avito, maxPageCount);
                                                break;
                                            case 3:
                                                mlsnThreadParser?.Dispose();
                                                mlsnThreadParser = CreateAndStartParsing(ParsingPageType.Mlsn, maxPageCount);
                                                break;
                                            case 4:
                                                cianThreadParser?.Dispose();
                                                cianThreadParser = CreateAndStartParsing(ParsingPageType.Cian, maxPageCount);
                                                break;
                                            case 5:
                                                ngsThreadParser?.Dispose();
                                                //ngsThreadParser = CreateAndStartParsing(ParsingPageType.N1, maxPageCount);
                                                break;
                                            case 9:
                                                break;
                                        }
                                    }
                                }
                                break;
                            case 3:
                                Console.WriteLine("Введите Id города");
                                input = Console.ReadLine();
                                if (!int.TryParse(input, out result))
                                    continue;
                                CoordAnalizer.Analize(result);
                                break;
                            case 4:
                                PhoneParser.Start();
                                break;
                            case 5:
                                {
                                    Console.WriteLine("=============================================");
                                    Console.WriteLine("Выбери один из пунктов:");
                                    Console.WriteLine("1. Запуск актуализатора");
                                    Console.WriteLine("2. Форсированная актуализация частников млсн и авито");
                                    Console.WriteLine("3. Поиск млс объявлений у млсн");
                                    Console.WriteLine("4. Merge (склейка дубликатов)");
                                    Console.WriteLine("9. Выход");
                                    Console.WriteLine("=============================================");
                                    input = Console.ReadLine();
                                    if (!int.TryParse(input, out result))
                                        continue;
                                    switch (result)
                                    {
                                        case 1:
                                            Actualizer.Start();
                                            break;
                                        case 2:
                                            Actualizer.CheckCountPagesWitoutSection();
                                            break;
                                        case 3:
                                            Actualizer.MlsnSetMls();
                                            break;
                                        case 4:
#if DEBUG
                                            input = "y";
#else
                                            Console.WriteLine("Запустить прям сейчас? (y/n)");
                                            input = Console.ReadLine();
#endif
                                            Merger.Start(input.ToLower() == "y");
                                            break;
                                        case 5:
                                            break;
                                        case 6:
                                            break;
                                        default: break;
                                    }
                                    break;
                                }
                            case 6:
                                break;
                            case 7:
                                Actualizer.DeleteImagesWithoutPage();
                                break;
                            case 8:
                                break;
                            case 9:
                                return;
                            case 10:
                            {
                                var badId = DbDebugManager.ForceUpdateSearchindex();
                                File.WriteAllText("badIdSearchIndex_Image.sql", "Delete from Image Where Id_Page = " + string.Join("\r\n OR Id_Page =", badId.Select(x=>x.ToString())));
                                File.WriteAllText("badIdSearchIndex_Page.sql", "Delete from Page Where Id = "+string.Join(";\r\n Delete from Page Where Id =", badId.Select(x => x.ToString()))+";");
                                Log.Fatal("Завершено форсирование. Файлы созданы. Всего "+badId.Count+" дублей");
                                    /*
                                                                    Console.WriteLine("");
                                                                    Console.WriteLine("Выбери пункты:");
                                                                    Console.WriteLine("1. Форсированно обновить SearchIndex");
                                                                    Console.WriteLine("=============================================");

                                                                    input = Console.ReadLine();
                                                                    switch (input)
                                                                    {

                                                                    }*/
                                    break;
                                }
                            default: continue;
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Exception(ex, LogType.Fatal);
                    }
                }
            }
            catch (Exception ex)
            {
                //Console.Clear();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("============= FATAL ERROR =============");
                Log.Exception(ex, LogType.Fatal, true);
                Console.WriteLine();
                Console.WriteLine();
                Console.ReadKey(true);
            }
            finally
            {
                Console.WriteLine("Выходим...");
                Actualizer.Stop();
                ngsThreadParser?.Dispose();
                yandexThreadParser?.Dispose();
                avitoThreadParser?.Dispose();
                mlsnThreadParser?.Dispose();
                cianThreadParser?.Dispose();
                Thread.Sleep(10000);
            }
        }

        private static ThreadParser CreateAndStartParsing(ParsingPageType boardType, int maxPages,int offset=1)
        {
            if (CityManager.SelectedCity == null)
                CityManager.HandSelectCity();
            
            var threadParser = new ThreadParser(boardType, maxPages, offset);
            threadParser.Start();
            return threadParser;
        }
    }
}

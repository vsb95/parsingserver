﻿using System;
using System.Linq;
using Core.Exceptions;
using CoreMls.DB.LocalEntities;
using CoreMls.DB.Model;
using CoreMls.Enum;
using CoreMls.Enum.Commercial;
using CoreMls.Interfaces;
using CoreMls.Validators.Commercial;
using CoreMls.Validators.Living;
using Logger;

namespace CoreMls.Validators
{
    public static class OfferValidatorFactory
    {
        public static IOfferValidator Create(offer offer)
        {
            if (!offer.id_offer_type.HasValue)
                throw new ValidationException("Не указан тип сделки");
            if (!offer.id_offer_yrl_category.HasValue)
                throw new ValidationException("Не указана категория сделки");

            var offerType = (OfferType)offer.id_offer_type.Value;
            var offerCategory = (OfferCategory)offer.id_offer_yrl_category.Value;

            if (offerCategory == OfferCategory.Commercial)
            {
                var commercialProp = offer.property.property_property_commercial_type.FirstOrDefault();
                if (commercialProp?.id_property_commercial_type == null)
                    throw new ValidationException("Не указана категория коммерческого объекта");

                //ToDo переделать на массив валидаторов
                var propertyCategory = (PropertyCommercialType)commercialProp.id_property_commercial_type;

                return CreateCommercialValidator(offerType, offerCategory, propertyCategory);
            }
            else
            {
                if (!offer.property.id_property_category.HasValue)
                    throw new ValidationException("Не указана категория жилого объекта");

                var propertyCategory = (PropertyLivingCategory)offer.property.id_property_category.Value;
                return CreateLivingValidator(offerType, offerCategory, propertyCategory);
            }
        }

        /// <summary>
        /// Создаст валидатор
        /// </summary>
        /// <param name="offerType">Аренда\продажа</param>
        /// <param name="offerCategory">Жилая\коммерция\новостройки</param>
        /// <param name="propertyCategory">Тип объекта (дом, кв, комната и тд)</param>
        /// <returns></returns>
        public static IOfferValidator CreateLivingValidator(OfferType offerType, OfferCategory offerCategory, PropertyLivingCategory propertyCategory)
        {
            try
            {
                switch (offerCategory)
                {
                    case OfferCategory.NewFlat:
                        return offerType == OfferType.Sell
                            ? new SellApartmentNewValidator()
                            : throw new Exception("Новостройка не в продаже? как это получилось. это бага");
                    case OfferCategory.Living:
                        switch (offerType)
                        {
                            case OfferType.Sell:
                                switch (propertyCategory)
                                {
                                    case PropertyLivingCategory.Dacha:
                                    case PropertyLivingCategory.Cottage:
                                    case PropertyLivingCategory.House:
                                    case PropertyLivingCategory.Townhouse:
                                    case PropertyLivingCategory.Duplex:
                                        return new SellHouseValidator();
                                    case PropertyLivingCategory.Lot: return new SellLotValidator();
                                    case PropertyLivingCategory.HouseWithLot: return new SellHouseWithLotValidator();
                                    case PropertyLivingCategory.ApartmentPart: return new SellApartmentPartValidator();
                                    case PropertyLivingCategory.HousePart: return new SellHousePartValidator();
                                    case PropertyLivingCategory.Apartment: return new SellApartmentValidator();
                                    case PropertyLivingCategory.Room: return new SellRoomValidator();
                                    default:
                                        throw new ValidationException("Не верно указан тип объекта");
                                }
                            case OfferType.Rent:
                                switch (propertyCategory)
                                {
                                    case PropertyLivingCategory.Dacha:
                                    case PropertyLivingCategory.Cottage:
                                    case PropertyLivingCategory.House:
                                    case PropertyLivingCategory.Townhouse:
                                    case PropertyLivingCategory.Duplex:
                                        return new RentHouseValidator();
                                    case PropertyLivingCategory.HouseWithLot: return new RentHouseWithLotValidator();
                                    case PropertyLivingCategory.Apartment: return new RentApartmentValidator();
                                    case PropertyLivingCategory.Room: return new RentRoomValidator();
                                    default:
                                        throw new ValidationException("Не верно указан тип объекта");
                                }
                            default:
                                throw new ValidationException("Не верно указан тип сделки");
                        }
                    case OfferCategory.Commercial:
                        throw new ValidationException("Не верно указан тип объекта: с коммерцией пока не работаем");
                    default:
                        throw new ValidationException("Не верно указан тип недвижимости");
                }
            }
            catch (ValidationException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "[OfferValidatorFactory::CreateLivingValidator]");
            }

            return null;
        }


        public static IOfferValidator CreateCommercialValidator(OfferType offerType, OfferCategory offerCategory, PropertyCommercialType propertyType)
        {
            try
            {
                switch (offerCategory)
                {
                    case OfferCategory.Commercial:
                    {
                        switch (offerType)
                        {
                            case OfferType.Sell:
                                switch (propertyType)
                                {
                                    case PropertyCommercialType.Default:
                                        break;
                                    case PropertyCommercialType.Autorepair:
                                        break;
                                    case PropertyCommercialType.Business:
                                        break;
                                    case PropertyCommercialType.FreePurpose:
                                        break;
                                    case PropertyCommercialType.Hotel:
                                        break;
                                    case PropertyCommercialType.Land:
                                        break;
                                    case PropertyCommercialType.LegalAddress:
                                        break;
                                    case PropertyCommercialType.Manufacturing:
                                        break;
                                    case PropertyCommercialType.Office:
                                        break;
                                    case PropertyCommercialType.PublicCatering:
                                        break;
                                    case PropertyCommercialType.Retail:
                                        break;
                                    case PropertyCommercialType.Warehouse:
                                        break;
                                    default:
                                        throw new ArgumentOutOfRangeException(nameof(propertyType), propertyType, null);
                                }
                                break;
                            case OfferType.Rent:
                                switch (propertyType)
                                {
                                    case PropertyCommercialType.Default:
                                        break;
                                    case PropertyCommercialType.Autorepair:
                                        break;
                                    case PropertyCommercialType.Business:
                                        break;
                                    case PropertyCommercialType.FreePurpose:
                                        break;
                                    case PropertyCommercialType.Hotel:
                                        break;
                                    case PropertyCommercialType.Land:
                                        break;
                                    case PropertyCommercialType.LegalAddress:
                                        break;
                                    case PropertyCommercialType.Manufacturing:
                                        break;
                                    case PropertyCommercialType.Office:
                                        break;
                                    case PropertyCommercialType.PublicCatering:
                                        break;
                                    case PropertyCommercialType.Retail:
                                        break;
                                    case PropertyCommercialType.Warehouse:
                                        break;
                                    default:
                                        throw new ArgumentOutOfRangeException(nameof(propertyType), propertyType, null);
                                }
                                break;
                            default:
                                throw new ArgumentOutOfRangeException(nameof(offerType), offerType, null);
                        }
                        break;
                    }
                    case OfferCategory.NewFlat:
                    case OfferCategory.Living:
                        throw new ValidationException("Не верно указан тип объекта");
                    default:
                        throw new ValidationException("Не верно указан тип недвижимости");
                }

                return new AbstractCommercialOfferValidator();
            }
            catch (ValidationException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "[OfferValidatorFactory::CreateLivingValidator]");
            }

            return null;
        }
    }
}

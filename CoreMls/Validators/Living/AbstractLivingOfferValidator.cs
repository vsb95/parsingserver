﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Exceptions;
using CoreMls.DB.Model;
using CoreMls.Enum;
using Logger;

namespace CoreMls.Validators.Living
{
    internal class AbstractLivingOfferValidator : AbstractOfferValidator
    {
        public override bool IsValid(offer offer)
        {
            try
            {
                if (!base.IsValid(offer))
                    return false;

                if (offer.price_value <= 0)
                    throw new ValidationException("Стоимость указана не верно", ValidationErrorType.Error);
                if (!offer.property.id_property_category.HasValue)
                    throw new ValidationException("Не указан тип объекта", ValidationErrorType.Error);

                if ((PropertyLivingCategory)offer.property.id_property_category != PropertyLivingCategory.Room
                    && offer.property.area_value.HasValue && offer.property.area_value <
                    (offer.property.kitchen_space_value ?? 0)
                    + (offer.property.living_space_value ?? 0)
                    + (offer.property.room_space_value ?? 0))
                    throw new ValidationException("Общая площадь не может быть меньше суммы остальных", ValidationErrorType.Error);

                if (offer.property.kitchen_space_value.HasValue && offer.property.kitchen_space_value >= 1000)
                    throw new ValidationException("Слишком большое значение площади кухни", ValidationErrorType.Error);
                if (offer.property.living_space_value.HasValue && offer.property.living_space_value >= 1000)
                    throw new ValidationException("Слишком большое значение жилой площади", ValidationErrorType.Error);
                if (offer.property.room_space_value.HasValue && offer.property.room_space_value >= 1000)
                    throw new ValidationException("Слишком большое значение площади комнаты", ValidationErrorType.Error);

                if (offer.property.area_value.HasValue && offer.property.area_value >= 1000)
                    throw new ValidationException("Слишком большое общей площади", ValidationErrorType.Error);
                if (offer.property.area_value.HasValue && offer.property.lot_area_value >= 10000)
                    throw new ValidationException("Слишком большое значение площади участка, укажите площадь в другой единице измерения", ValidationErrorType.Error);



                return true;
            }
            catch (ValidationException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "[AbstractLivingOfferValidator::IsValid]");
            }

            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Exceptions;
using CoreMls.DB.Model;
using CoreMls.Interfaces;
using Logger;

namespace CoreMls.Validators.Living
{
    internal class SellApartmentValidator: AbstractLivingOfferValidator
    {
        public override bool IsValid(offer offer)
        {
            try
            {
                if (!base.IsValid(offer))
                    return false;

                if (offer.price_value < 250000)
                    throw new ValidationException("Стоимость указана не верно: слишком маленькая сумма для продажи");


                return true;
            }
            catch (ValidationException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "[SellApartmentValidator::IsValid]");
            }

            return false;
        }
    }
}

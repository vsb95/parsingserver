﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Exceptions;
using CoreMls.DB.Model;
using CoreMls.Interfaces;
using CoreMls.Managers;
using Logger;

namespace CoreMls.Validators.Living
{
    internal class RentHouseValidator : AbstractLivingOfferValidator
    {
        public override bool IsValid(offer offer)
        {
            try
            {
                if (!base.IsValid(offer))
                    return false;
                if (offer.price_value > 500000)
                    throw new ValidationException("Стоимость указана не верно: слишком большая сумма для аренды");

                return true;
            }
            catch (ValidationException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "[RentHouseValidator::IsValid]");
            }

            return false;
        }
    }
}

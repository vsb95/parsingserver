﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Exceptions;
using CoreMls.DB.Managers;
using CoreMls.DB.Model;
using CoreMls.Enum;
using CoreMls.Interfaces;

namespace CoreMls.Validators.Living
{
    internal abstract class AbstractOfferValidator :IOfferValidator
    {
        public virtual bool IsValid(offer offer)
        {
            if(offer == null)
                throw new ArgumentNullException(nameof(offer));

            if (offer.property.floor > offer.property.building.floors_total)
                throw new ValidationException("Указанный этаж больше чем указанная этажность здания", ValidationErrorType.Error);

            if (!offer.property.id_property_type.HasValue)
                throw new ValidationException("Не указан тип недвижимости", ValidationErrorType.Error);

            if (string.IsNullOrEmpty(offer.property.building.building_location.address))
                throw new ValidationException("Не указан адрес", ValidationErrorType.Error);

            var agent = offer.sales_agent ?? DbSalesAgentManager.GetAgentById(offer.id_sales_agent.Value)?.ToDb();
            if(string.IsNullOrEmpty(agent?.sales_agent_name))
                throw new ValidationException("Не указано имя агента, задайте в <a href=\"/settings\" target=\"_blank\">настройках профиля</a>", ValidationErrorType.Error);
            if (!agent.sales_agent_phone.Any(x => x.phone_number.HasValue && x.phone_number.Value > 1000))
                throw new ValidationException("Не указан телефон агента, задайте в <a href=\"/settings\" target=\"_blank\">настройках профиля</a>", ValidationErrorType.Error);
            if (agent.id_sales_agent_organization < 1)
                throw new ValidationException("Не указана организация", ValidationErrorType.Error);

            var location = offer.property?.building?.building_location;
            if (location == null || string.IsNullOrEmpty(location.locality_name) && string.IsNullOrEmpty(location.region))
                throw new ValidationException("Не указан нас.пункт", ValidationErrorType.Error);

            if (!string.IsNullOrEmpty(offer.property.cadastral_number) && offer.property.cadastral_number.Length < 14)
                throw new ValidationException("Не верно указан кадастровый номер", ValidationErrorType.Warning);
            return true;
        }
    }
}

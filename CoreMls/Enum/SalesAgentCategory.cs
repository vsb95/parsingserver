﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    public enum SalesAgentCategory
    { 
        // Id связаны с таблицей sales_agent_category
        Agency = 2,
        Owner = 4,
        Developer = 6
    }
}

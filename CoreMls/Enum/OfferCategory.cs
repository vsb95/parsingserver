﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    /// <summary>
    /// Жилая\Коммерция\Нвоостройки
    /// </summary>
    public enum OfferCategory
    {
        //id завязаны на таблице offer_yrl_category
        Default = 0,
        /// <summary>
        /// Жилая
        /// </summary>
        Living = 1,
        /// <summary>
        /// Коммерческая недвижимость
        /// </summary>
        Commercial = 2,
        /// <summary>
        /// Новостройки
        /// </summary>
        NewFlat = 3,
    }
}

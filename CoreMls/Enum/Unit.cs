﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    public enum Unit
    {
        // Номера не трогать - они завязаны напрямую к бд 'unit'
        SqMeter = 1,
        OneHundred = 3,
        Hectare = 4
    }
}

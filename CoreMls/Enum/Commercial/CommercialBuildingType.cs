﻿namespace CoreMls.Enum.Commercial
{
    /// <summary>
    /// Тип здания, в котором находится объект
    /// </summary>
    public enum CommercialBuildingType
    {
        Default = 0,

        /// <summary>
        /// бизнес-центр
        /// </summary>
        BusinessCenter = 2,

        /// <summary>
        /// отдельно стоящее здание
        /// </summary>
        DetachedBuilding = 4,

        /// <summary>
        /// встроенное помещение
        /// </summary>
        ResidentialBuilding = 6,

        /// <summary>
        /// торговый центр
        /// </summary>
        ShoppingCenter = 8,

        /// <summary>
        /// складской комплекс
        /// </summary>
        Warehouse = 10,
    }
}

﻿namespace CoreMls.Enum.Commercial
{
    /// <summary>
    /// Категория коммерческого объекта.
    /// </summary>
    public enum PropertyCommercialType
    {
        Default = 0,

        /// <summary>
        /// автосервис
        /// </summary>
        Autorepair = 2,

        /// <summary>
        /// готовый бизнес
        /// </summary>
        Business = 4,

        /// <summary>
        /// помещения свободного назначения
        /// </summary>
        FreePurpose = 6,

        /// <summary>
        /// гостиница
        /// </summary>
        Hotel = 8,

        /// <summary>
        /// земли коммерческого назначения
        /// </summary>
        Land = 10,

        /// <summary>
        /// юридический адрес
        /// </summary>
        LegalAddress = 12,

        /// <summary>
        /// производственное помещение
        /// </summary>
        Manufacturing = 14,

        /// <summary>
        /// офисные помещения
        /// </summary>
        Office = 16,

        /// <summary>
        /// общепит
        /// </summary>
        PublicCatering = 18,

        /// <summary>
        /// торговые помещения
        /// </summary>
        Retail = 20,

        /// <summary>
        /// склад
        /// </summary>
        Warehouse = 22,
    }
}

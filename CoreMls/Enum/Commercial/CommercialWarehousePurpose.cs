﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum.Commercial
{
    /// <summary>
    /// Назначение склада.
    /// </summary>
    public enum CommercialWarehousePurpose
    {
        Default = 0 ,

        /// <summary>
        /// алкогольный склад
        /// </summary>
        Alcohol = 2,

        /// <summary>
        /// фармацевтический склад
        /// </summary>
        PharmaceuticalStorehouse = 4,

        /// <summary>
        /// овощехранилище
        /// </summary>
        VegetableStorehouse = 6,

    }
}

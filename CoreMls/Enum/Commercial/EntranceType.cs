﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum.Commercial
{
    /// <summary>
    /// Вход в помещение.
    /// </summary>
    public enum EntranceType
    {
        Default = 0,

        /// <summary>
        /// общий
        /// </summary>
        Common = 2,

        /// <summary>
        /// отдельный
        /// </summary>
        Separate = 4,
    }
}

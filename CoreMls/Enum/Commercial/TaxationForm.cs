﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum.Commercial
{
    /// <summary>
    /// Форма налогообложения арендодателя для аренды.
    /// </summary>
    public enum TaxationForm
    {
        Default = 0,

        /// <summary>
        /// «НДС» (арендодатель — плательщик НДС)
        /// </summary>
        Nds = 1,

        /// <summary>
        /// «УСН» (арендодатель работает по упрощенной системе налогообложения)
        /// </summary>
        Usn = 2
    }
}

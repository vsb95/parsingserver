﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum.Commercial
{
    /// <summary>
    /// Класс бизнес-центра.
    /// </summary>
    public enum OfficeClassType
    {
        Default = 0,

        A = 1,
        Aplus = 2,
        B = 3,
        Bplus = 4,
        C = 5,
        Cplus = 6,

    }
}

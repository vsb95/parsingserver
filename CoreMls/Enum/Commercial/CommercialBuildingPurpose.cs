﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum.Commercial
{
    /// <summary>
    /// Рекомендуемое назначение объекта.
    /// </summary>
    public enum CommercialBuildingPurpose
    {
        Default = 0,

        /// <summary>
        /// помещение для банка
        /// </summary>
        Bank = 2,

        /// <summary>
        /// салон красоты
        /// </summary>
        BeautyShop = 4,

        /// <summary>
        /// продуктовый магазин
        /// </summary>
        FoodStore = 6,

        /// <summary>
        /// медицинский центр
        /// </summary>
        MedicalCenter = 8,

        /// <summary>
        /// шоу-рум
        /// </summary>
        ShowRoom = 10,

        /// <summary>
        /// турагентство
        /// </summary>
        Touragency = 12,

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    /// <summary>
    /// Сайт для автовыгрузки
    /// </summary>
    public enum FeedSite
    {
        Default = 0,
        // завязано на таблице feed_site
        Yandex = 1,
        Avito = 2,
        MLSN = 3,
        Cian = 4
    }
}

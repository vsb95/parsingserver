﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    /// <summary>
    /// Вид договора (эксклюзив и тд)
    /// </summary>
    public enum ContractType
    {
        //id Завязаны на таблице contract_type
        Default = 0,
        Exclusive = 1,
        Soft = 2,
        None = 3
    }
}

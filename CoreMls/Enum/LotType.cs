﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    public enum LotType
    {
        // Связь на таблице lot_type
        Default = 0,
        /// <summary>
        /// ИЖС
        /// </summary>
        Person = 1,
        /// <summary>
        /// Садоводчество
        /// </summary>
        Garden = 2
    }
}

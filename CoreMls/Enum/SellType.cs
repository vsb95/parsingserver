﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    /// <summary>
    /// Вид продажи (чистая\альтернатива)
    /// </summary>
    public enum SellType
    {
        // id завязаны на таблице sell_type
        Default = 0,
        Free = 1,
        Alternative = 2
    }
}

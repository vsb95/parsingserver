﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    /// <summary>
    /// Аренда\Продажа
    /// </summary>
    public enum OfferType
    {
        Default = 0,
        // Номера не трогать - они завязаны напрямую к бд offer_type
        Sell =1,
        Rent =2

    }
}

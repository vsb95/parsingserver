﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    /// <summary>
    /// Срок аренды
    /// </summary>
    public enum PricePeriod
    {
        Default = 0,
        // Номера не трогать - они завязаны напрямую к бд 'period'
        Day = 1,
        Month = 3
    }
}

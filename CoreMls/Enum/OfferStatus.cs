﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    /// <summary>
    /// Первичная продажа\Прямая продажа и тд
    /// </summary>
    public enum OfferStatus
    {
        // id связаны с таблицей offer_status
        Default = 0,
        /// <summary>
        /// Первичная продажа
        /// </summary>
        FirstSell =1,

        /// <summary>
        /// прямая продажа
        /// </summary>
        DirectSale = 2,

        /// <summary>
        /// Продажа от застройщика
        /// </summary>
        DeveloperSell = 3,

        /// <summary>
        /// первичная продажа вторички
        /// </summary>
        FirstSecondSale = 5,

        /// <summary>
        /// Встречная продажа
        /// </summary>
        CounterSale = 7,

        /// <summary>
        /// Переуступка
        /// </summary>
        ReAssignment = 9,

        /// <summary>
        /// Прямая Аренда
        /// </summary>
        DirectRent = 11,
        /// <summary>
        /// СубАренда
        /// </summary>
        SubRent = 13,
    }
}

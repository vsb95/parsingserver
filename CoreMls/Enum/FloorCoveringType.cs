﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    public enum FloorCoveringType
    {
        Default = 0,

        Carpet = 1,

        Laminat = 2,

        Linoleum = 3,

        Parket = 4
    }
}

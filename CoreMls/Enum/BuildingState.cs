﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    public enum BuildingState
    {
        //id к таблице building_state
        Default = 0,
        /// <summary>
        /// Построен но не сдан
        /// </summary>
        BuildedButNotDone =2,
        /// <summary>
        /// Сдан
        /// </summary>
        Done = 4,
        /// <summary>
        /// Строится
        /// </summary>
        InBuild =6,
    }
}

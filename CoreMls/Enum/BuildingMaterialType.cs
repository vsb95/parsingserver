﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    public enum BuildingMaterialType
    {
        // id завязаны на тблице bulding_type
        Default = 0,
        Кирпич = 1,
        Железобетонный = 3,
        Металлический = 5,
        Блочный = 7,
        Деревянный = 8,
        КирпичноМонолитный = 9,
        Монолит = 10,
        Панельный = 11,
        КаркасноНасыпной = 12,
    }
}

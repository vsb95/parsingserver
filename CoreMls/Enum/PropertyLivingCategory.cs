﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    public enum PropertyLivingCategory
    {
        // id завязаны на таблице property_category
        Default = 0,
        Dacha = 1,
        Cottage = 2,
        House = 4,
        /// <summary>
        /// Дом с участком
        /// </summary>
        HouseWithLot = 6,
        /// <summary>
        /// Участок
        /// </summary>
        Lot = 8,

        /// <summary>
        /// Часть кв
        /// </summary>
        ApartmentPart = 23,
        /// <summary>
        /// Часть дома
        /// </summary>
        HousePart = 10,

        /// <summary>
        /// Квартира
        /// </summary>
        Apartment = 11,

        Room = 14,

        Townhouse = 15,
        
        Duplex = 17,

        Garage = 20
    }
}

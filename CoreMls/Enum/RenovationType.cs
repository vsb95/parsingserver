﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    public enum RenovationType
    {
        // id завязаны на таблице property_renovation
        Default = 0,
        Дизайнерский = 1,
        Евро = 2,
        СОтделкой = 3,
        ТребуетРемонта = 4,
        Хороший = 5,
        ЧастичныйРемонт = 6,
        ЧистоваяОтделка = 7,
        ПодКлюч = 8,
        ЧерноваяОтделка = 9

    }
}

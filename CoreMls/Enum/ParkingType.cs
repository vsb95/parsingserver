﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    public enum ParkingType
    {
        // id завязаны на тблице parking_type
        Default = 0,
        SubGround = 1,
        Ground = 3,
        Multilevel = 5
    }
}

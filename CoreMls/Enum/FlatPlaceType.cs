﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    public enum FlatPlaceType
    {
        //id завяланы на таблице flat_place_type
        Default = 0,

        /// <summary>
        /// Угловая
        /// </summary>
        Angle = 1,

        /// <summary>
        /// 
        /// </summary>
        NotAngle = 2

    }
}

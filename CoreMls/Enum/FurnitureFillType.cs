﻿namespace CoreMls.Enum
{
    public enum FurnitureFillType
    {
        Default = 0,

        Full = 1,
        Part = 2,
        OnRequest = 3,
    }
}

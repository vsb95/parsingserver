﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    public enum CommissionType
    {
        // id завязаны на таблице commission_type
        Default = 0,
        Percent =1,
        Fixed = 2,
        /// <summary>
        /// Нет комиссии
        /// </summary>
        None = 3,
        /// <summary>
        /// Не указано
        /// </summary>
        Null = 4
    }
}

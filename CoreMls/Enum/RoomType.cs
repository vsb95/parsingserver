﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    public enum RoomType
    {
        //Id завязаны на таблице rooms_type

        /// <summary>
        /// Смежные
        /// </summary>
        Neighbor=1,

        /// <summary>
        /// Раздельные\изолированные
        /// </summary>
        Isolated = 2,

        /// <summary>
        /// Смежно-изолированные
        /// </summary>
        NeighborIsolated = 3
    }
}

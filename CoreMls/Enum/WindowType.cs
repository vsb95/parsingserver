﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    public enum WindowType
    {
        // id привязаны к таблице window_view_type
        Yard = 1,
        Street = 2,
        Both = 3
    }
}

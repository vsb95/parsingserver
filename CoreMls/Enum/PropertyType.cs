﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreMls.Enum
{
    public enum PropertyType
    {
        // таблица property_type
        Default = 0,
        Living = 2,
        Commercial = 4
    }
}

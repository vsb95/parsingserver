﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreMls.Managers;

namespace CoreMls.Interfaces
{
    internal interface IFeedParser
    {
        bool TryParse(string fileUrl, long idOrg, out FeedImportResult result);
    }
}

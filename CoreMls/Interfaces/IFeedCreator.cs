﻿namespace CoreMls.Interfaces
{
    internal interface IFeedCreator
    {
        bool TryCreateFeed(long idOrg, string fileTo);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreMls.DB.Model;

namespace CoreMls.Interfaces
{
    public interface IOfferValidator
    {
        /// <summary>
        /// Выкинет ValidationException если не прошли проверку, вернет true если все ок, и false если произошло внутреннее исключение
        /// </summary>
        /// <param name="offer">объявление</param>
        bool IsValid(offer offer);
    }
}

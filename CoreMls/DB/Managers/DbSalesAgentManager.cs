﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using CoreMls.DB.LocalEntities;
using CoreMls.DB.Model;
using Logger;

namespace CoreMls.DB.Managers
{
    public static class DbSalesAgentManager
    {
        public const int DefaultRetryCount = 5;
        public const int DeadlockErrorNumber = 1205;
        public const int LockingErrorNumber = 1222;
        public const int UpdateConflictErrorNumber = 3960;
        private static object _locker = new object();


        /// <summary>
        /// Получить агента по id
        /// </summary>
        /// <returns></returns>
        public static SalesAgent GetAgentById(long id)
        {
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
                {
                    using (var context = new realtyMLSEntities())
                    {
                        var agents = context.sales_agent.Where(x => x.id_sales_agent == id);

                        if (agents.Count() == 0)
                            return null;

                        var agent = agents.FirstOrDefault();
                        if (agent != null)
                            return new SalesAgent(agent);
                        transaction.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

            return null;
        }
        public static SalesAgent GetAgentByName(string name, long idOrg, string email = null, string photo = null)
        {
            if (string.IsNullOrEmpty(name) || idOrg < 1)
                return null;
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
                {
                    using (var context = new realtyMLSEntities())
                    {
                        var agents = context.sales_agent.Where(x => x.id_sales_agent_organization == idOrg && x.sales_agent_name.ToLower() == name);
                        if (!string.IsNullOrEmpty(email))
                            agents = agents.Where(x => x.sales_agent_email.ToLower() == email.ToLower());
                        if (!string.IsNullOrEmpty(photo))
                            agents = agents.Where(x => x.sales_agent_photo.ToLower() == photo.ToLower());
                        if (agents.Count() == 0)
                            return null;

                        var agent = agents.FirstOrDefault();
                        if (agent != null)
                            return new SalesAgent(agent);
                        transaction.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

            return null;
        }

        public static sales_agent_phone GetSalesAgentPhone(string inputPhone, long agentId, realtyMLSEntities context = null)
        {
            sales_agent_phone result = null;
            if (string.IsNullOrEmpty(inputPhone))
                return null;
            var phone = inputPhone.Replace(" ", "").Replace("+", "").Replace("-", "").Replace("(", "").Replace(")", "").Replace("_", "");
            if (!long.TryParse(phone, out var phoneNum))
                throw new ArgumentException("Не верно указан номер телефона: "+ inputPhone);
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))
                {
                    if (context == null)
                    {
                        using (context = new realtyMLSEntities())
                        {
                            var phones = context.phones.Where(x => x.phone_number == phoneNum);
                            if (phones.Count() == 0)
                            {
                                context.phones.Add(new phone { phone_number = phoneNum });
                                context.SaveChanges();
                            }
                            result = context.sales_agent_phone.FirstOrDefault(x =>
                                x.id_sales_agent == agentId && x.phone_number == phoneNum);
                            transaction.Complete();
                        }
                    }
                    else
                    {
                        var phones = context.phones.Where(x => x.phone_number == phoneNum);
                        if (phones.Count() == 0)
                        {
                            context.phones.Add(new phone { phone_number = phoneNum });
                            context.SaveChanges();
                        }
                        result = context.sales_agent_phone.FirstOrDefault(x =>
                            x.id_sales_agent == agentId && x.phone_number == phoneNum);
                        transaction.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

            return result ?? new sales_agent_phone { phone_number = phoneNum, id_sales_agent = agentId };
        }


        public static long AddOrUpdate(SalesAgent salesAgent)
        {
            return AddOrUpdate(salesAgent.ToDb());
        }
        /// <summary>
        /// Добавит\обновит пользователя или Сгенерирует новое число больше 10млн для нового фиктивного пользователя
        /// и добавит пользователя без имени
        /// </summary>
        /// <returns></returns>
        public static long AddOrUpdate(sales_agent salesAgent)
        {
            if (salesAgent == null || salesAgent.id_sales_agent > 0 && salesAgent.id_sales_agent_organization < 1 && salesAgent.sales_agent_organization1 == null)
                return -1;
            if (!salesAgent.create_dt.HasValue)
                salesAgent.create_dt = DateTime.Now;

            try
            {
                using (var context = new realtyMLSEntities())
                {
                    if (salesAgent.id_sales_agent != 0)
                    {
                        var originSalesAgent =
                            context.sales_agent.FirstOrDefault(x => x.id_sales_agent == salesAgent.id_sales_agent);
                        // Update
                        if (originSalesAgent != null)
                        {
                            bool isModify = false;
                            // если кол-во телефонов изменилось или сами телефоны разные то обновим список телефонов
                            if (originSalesAgent.sales_agent_phone.Count != salesAgent.sales_agent_phone.Count
                                || originSalesAgent.sales_agent_phone.Any(x =>
                                    salesAgent.sales_agent_phone.All(n => n.phone_number != x.phone_number)))
                            {
                                isModify = true;
                                foreach (var phone in originSalesAgent.sales_agent_phone.ToList())
                                {
                                    context.sales_agent_phone.Remove(phone);
                                }

                                context.sales_agent.AddOrUpdate(originSalesAgent);
                                context.SaveChanges();

                                foreach (var phone in salesAgent.sales_agent_phone)
                                {
                                    originSalesAgent.sales_agent_phone.Add(GetSalesAgentPhone(
                                        phone.phone_number.ToString(), originSalesAgent.id_sales_agent, context));
                                }
                            }

                            isModify |= originSalesAgent.sales_agent_name != salesAgent.sales_agent_name;
                            isModify |= originSalesAgent.sales_agent_email != salesAgent.sales_agent_email;
                            isModify |= originSalesAgent.sales_agent_organization !=
                                        salesAgent.sales_agent_organization;
                            isModify |= originSalesAgent.sales_agent_partner != salesAgent.sales_agent_partner;
                            isModify |= originSalesAgent.sales_agent_photo != salesAgent.sales_agent_photo;
                            isModify |= originSalesAgent.sales_agent_url != salesAgent.sales_agent_url;
                            isModify |= originSalesAgent.id_sales_agent_category != salesAgent.id_sales_agent_category;

                            if (!isModify) return originSalesAgent.id_sales_agent;
                            originSalesAgent.sales_agent_name = salesAgent.sales_agent_name;
                            originSalesAgent.sales_agent_email = salesAgent.sales_agent_email;
                            originSalesAgent.sales_agent_organization = salesAgent.sales_agent_organization;
                            originSalesAgent.sales_agent_partner = salesAgent.sales_agent_partner;
                            originSalesAgent.sales_agent_photo = salesAgent.sales_agent_photo;
                            originSalesAgent.sales_agent_url = salesAgent.sales_agent_url;
                            originSalesAgent.id_sales_agent_category = salesAgent.id_sales_agent_category;

                            context.sales_agent.AddOrUpdate(originSalesAgent);
                            context.SaveChanges();
                            Log.Trace("SalesAgent Update: " + salesAgent.id_sales_agent);
                            return salesAgent.id_sales_agent;
                        }

                        // Add
                        context.sales_agent.AddOrUpdate(salesAgent);
                        context.SaveChanges();
                        Log.Trace("SalesAgent Add: " + salesAgent.id_sales_agent);
                        return salesAgent.id_sales_agent;
                    }

                    lock (_locker)
                    {
                        // Add virtual agent
                        var id = context.sales_agent.Max(x => x.id_sales_agent);
                        if (id < 10000000)
                            id += 10000000;
                        id++;
                        salesAgent.id_sales_agent = id;
                        salesAgent.is_virtual = true;
                        if (salesAgent.id_sales_agent_organization != 0)
                            salesAgent.sales_agent_organization1 = null;
                        context.sales_agent.Add(salesAgent);
                        context.SaveChanges();

                        Log.Trace("Создан виртуальный пользователь: " + id + "('" + salesAgent.sales_agent_name + "' " +
                                  salesAgent.sales_agent_phone.FirstOrDefault()?.phone_number + ")");
                        return id;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "[DbSalesAgentManager::AddOrUpdate]");
            }

            return -1;
        }

        public static SalesAgent GetRealAgentByVirtualAgentId(long virtualAgentId)
        {
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))
                {
                    using (var context = new realtyMLSEntities())
                    { 
                        var association = context.sales_agent_feed_user.FirstOrDefault(x => x.id_sales_agent_feed == virtualAgentId);

                        if (association == null)
                            return null;

                        var agent = context.sales_agent.FirstOrDefault(x=>x.id_sales_agent == association.id_sales_agent_user);
                        if (agent != null)
                            return new SalesAgent(agent);
                        transaction.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "[DbSalesAgentManager::GetRealAgentByVirtualAgentId] virtualId= " + virtualAgentId);
            }

            return null;
        }
    }


}

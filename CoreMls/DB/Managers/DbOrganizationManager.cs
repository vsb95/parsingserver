﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using CoreMls.DB.LocalEntities;
using CoreMls.DB.Model;
using Logger;

namespace CoreMls.DB.Managers
{
    public static class DbOrganizationManager
    {

        public static Organization GetOrganizationById(long idOrg)
        {
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))
                {
                    using (var context = new realtyMLSEntities())
                    {
                        var organizations = context.sales_agent_organization.Where(x => x.id_sales_agent_organization == idOrg);

                        if (organizations.Count() == 0)
                            return null;

                        var org = organizations.FirstOrDefault();
                        if (org != null)
                            return new Organization(org);
                        transaction.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

            return null;
        }

        /*
        public static long AddOrUpdate(Organization organization)
        {
            if (organization == null)
                return -1;
            return AddOrUpdate(organization.ToDb());
        }
        */

        public static long AddOrUpdate(sales_agent_organization organization)
        {
            if (organization == null)
                return -1;

            try
            {
                using (var context = new realtyMLSEntities())
                {
                    if (organization.id_sales_agent_organization > 0)
                    {
                        var originOrg =
                            context.sales_agent_organization.FirstOrDefault(x => x.id_sales_agent_organization == organization.id_sales_agent_organization);
                        // Update
                        if (originOrg != null)
                        {
                            bool isModify = false;
                            
                            isModify |= originOrg.sales_agent_organization_name != organization.sales_agent_organization_name;
                            //isModify |= originOrg.feed_yrl_file != organization.feed_yrl_file;
                            isModify |= originOrg.watermark_image != organization.watermark_image;
                            
                            if (!isModify) return originOrg.id_sales_agent_organization;
                            originOrg.sales_agent_organization_name = organization.sales_agent_organization_name;
                            originOrg.watermark_image = organization.watermark_image;
                            //originOrg.feed_yrl_file = organization.feed_yrl_file;

                            context.sales_agent_organization.AddOrUpdate(originOrg);
                            context.SaveChanges();
                            Log.Trace("Organization Update: " + originOrg.id_sales_agent_organization);
                            return originOrg.id_sales_agent_organization;
                        }

                        // Add
                        context.sales_agent_organization.AddOrUpdate(organization);
                        context.SaveChanges();
                        Log.Trace("Organization Add: " + organization.id_sales_agent_organization);
                        return organization.id_sales_agent_organization;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "[DbOrganizationManager::AddOrUpdate]");
            }

            return -1;
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using Core.Exceptions;
using CoreMls.DB.LocalEntities;
using CoreMls.DB.Model;
using CoreMls.Managers;
using Logger;

namespace CoreMls.DB.Managers
{
    public static class DbOfferManager
    {
        public const int DefaultRetryCount = 5;
        public const int DeadlockErrorNumber = 1205;
        public const int LockingErrorNumber = 1222;
        public const int UpdateConflictErrorNumber = 3960;
        /// <summary>
        /// Получить страницу по id
        /// </summary>
        /// <returns></returns>
        public static MlsAdvertisment GetAdvById(long id)
        {
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
                {
                    using (var context = new realtyMLSEntities())
                    {
                        var offers = context.offers.Where(x => x.id_offer == id);

                        if (offers.Count() == 0)
                            return null;

                        var offer = offers.FirstOrDefault();
                        if (offer != null)
                            return new MlsAdvertisment(offer);
                        transaction.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

            return null;
        }

        public static offer GetOfferById(long id, realtyMLSEntities context)
        {
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
                {
                    var offers = context.offers.Where(x => x.id_offer == id);

                    if (offers.Count() == 0)
                        return null;

                    var offer = offers.FirstOrDefault();
                    transaction.Complete();
                    if (offer != null)
                        return offer;
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

            return null;
        }

        /// <summary>
        /// Установить IDшники сущностей, если этот innerId есть в системе для этой организации
        /// </summary>
        /// <param name="innerId"></param>
        /// <param name="orgid"></param>
        /// <param name="inputOffer"></param>
        /// <returns></returns>
        public static bool SetIdOfferByInnerId(long innerId, long orgid, offer inputOffer)
        {
            if (inputOffer == null)
                return false;
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
                {
                    using (var context = new realtyMLSEntities())
                    {
                        var offers = context.offers.Where(x => x.sales_agent.id_sales_agent_organization == orgid && x.feed_internal_id == innerId).Take(1).ToList();

                        if (offers.Count() == 0)
                            return false;

                        var offer = offers.FirstOrDefault();
                        if (offer != null)
                        {
                            inputOffer.create_dt = offer.create_dt;
                            inputOffer.id_offer = offer.id_offer;
                            inputOffer.id_sales_agent = offer.id_sales_agent;
                            inputOffer.sales_agent.id_sales_agent = offer.sales_agent.id_sales_agent;
                            inputOffer.id_property = offer.id_property;
                            inputOffer.property.id_property = offer.property.id_property;
                            inputOffer.property.id_building = offer.property.id_building;
                            inputOffer.property.building.id_building = offer.property.building.id_building;
                            inputOffer.property.building.id_building_location = offer.property.building.id_building_location;
                            inputOffer.property.building.building_location.id_building_location =
                                offer.property.building.building_location.id_building_location;
                        }
                        transaction.Complete();
                        return offer != null;
                    }

                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

            return false;
        }

        public static bool AddOrUpdatePage(MlsAdvertisment adv)
        {
            var trace = new StringBuilder();
            try
            {
                if (adv == null)
                {
                    Log.Error("[DbManager::AddOrUpdatePage] MLS Попытка добавить пустую страницу");
                    return false;
                }

                bool isNeedMoveImages = false;
                bool isNeedUpdateImages = false;
                var entity = adv.ToDb();
                if (adv.Id.HasValue)
                {
                    if (!adv.Property.Id.HasValue)
                        throw new Exception("Не указан id 'Property'");
                    if (!adv.Property.Building.Id.HasValue)
                        throw new Exception("Не указан id 'Building'");
                    if (!adv.SalesAgent.Id.HasValue)
                        throw new Exception("Не указан id 'SalesAgent'");
                    Log.Trace("Update MLS id = " + adv.Id);
                    var originPage = GetAdvById(adv.Id.Value);
                    if (originPage.Property.Images?.Count != adv.Property.Images?.Count)
                    {
                        trace.AppendLine("количество изображений не совпало");
                        isNeedUpdateImages = true;
                    }
                    else if (adv.Property.Images == null || adv.Property.Images.FirstOrDefault() != originPage.Property.Images.FirstOrDefault())
                    {
                        isNeedUpdateImages = true;
                    }
                    else
                    {
                        foreach (var newImage in adv.Property.Images)
                        {
                            if (originPage.Property.Images.Contains(newImage)) continue;
                            trace.AppendLine("нашли другое изображение: " + newImage);
                            isNeedUpdateImages = true;
                            break;
                        }
                    }
                    if (adv.Property?.Images != null)
                        isNeedMoveImages = adv.Property.Images.Any(x => !x.StartsWith("http"));
                    //entity.last_update_date = DateTime.Now;
                }
                else
                {
                    isNeedMoveImages = true;
                    Log.Trace("Add MLS");
                    entity.create_dt = DateTime.Now;
                }

                var attemptNumber = 1;
                while (true)
                {
                    try
                    {
                        using (var context = new realtyMLSEntities())
                        {
                            //context.Database.Log = Log.Trace;
                            if (isNeedUpdateImages)
                            {
                                var origProperty = context.properties.First(x => x.id_property == adv.Property.Id);

                                if (origProperty.property_image != null && origProperty.property_image.Count > 0)
                                {
                                    foreach (var image in origProperty.property_image.ToList())
                                    {
                                        context.property_image.Remove(image);
                                    }
                                }

                                origProperty.property_image = entity.property.property_image;
                                context.properties.AddOrUpdate(origProperty);

                                context.SaveChanges();
                            }
                            trace.AppendLine("AddOrUpdate MLS");
                            context.offers.AddOrUpdate(entity);
                            context.properties.AddOrUpdate(entity.property);
                            if (adv.Id.HasValue)
                            {
                                context.buildings.AddOrUpdate(entity.property.building);
                                context.building_location.AddOrUpdate(entity.property.building.building_location);
                            }
                            context.SaveChanges();

                            trace.AppendLine("end transaction");

                            if (!adv.Id.HasValue)
                            {
                                trace.AppendLine("update id");
                                adv.Id = entity.id_offer;
                                adv.Property.Id = entity?.id_property ?? 0;
                                adv.Property.Building.Id = entity?.property.id_building;
                                adv.Property.Building.Location.Id = entity?.property.building.id_building_location;
                            }

                            break;

                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException is SqlException sqlException)
                        {
                            if (!sqlException.Errors.Cast<SqlError>().Any(error =>
                                (error.Number == DeadlockErrorNumber) ||
                                (error.Number == LockingErrorNumber) ||
                                (error.Number == UpdateConflictErrorNumber)))
                            {
                                Log.Exception(ex, LogType.Fatal, true);
                                return false;
                            }

                            if (attemptNumber == DefaultRetryCount + 1)
                            {
                                Log.Exception(ex, LogType.Fatal, true);
                                return false;
                            }

                            Thread.Sleep(15);
                            attemptNumber++;
                            continue;
                        }

                        if (ex is DbEntityValidationException validationException)
                        {
                            var message = "Ошибка валидации: ";
                            foreach (var error in validationException.EntityValidationErrors)
                            {
                                foreach (var validationError in error.ValidationErrors)
                                {
                                    message += validationError.ErrorMessage + "\n";
                                }
                            }
                            Log.Fatal(message);
                            return false;
                        }

                        Log.Exception(ex, LogType.Fatal, true, "[DbManager::AddOrUpdatePage]");
                        return false;
                    }
                }

                trace.AppendLine("end");
                // ToDO fix reqursive 
                if (isNeedMoveImages && ImageManager.MoveImages(adv))
                {
                    if (!AddOrUpdatePage(adv)) return false;
                    ImageManager.SetWaterMarkIfNeeded(adv.Id.Value);
                    return true;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "[DbManager::AddOrUpdatePage-global]:\n" + trace);
            }

            return false;
        }

        public static bool TryDelete(long idAdv, long idUser)
        {
            try
            {
                using (var context = new realtyMLSEntities())
                {
                    var offer = context.offers.FirstOrDefault(x => x.id_offer == idAdv);
                    if (offer == null)
                        return true;
                    var propId = offer.id_property.Value;
                    if (offer.id_sales_agent != idUser)
                    {
                        var owner = DbSalesAgentManager.GetAgentById(idUser);
                        if (owner == null || owner.Organization.Id != offer.sales_agent.id_sales_agent_organization)
                        {
                            throw new ValidationException("Попытка обновить чужое объявление!!! ",
                                ValidationErrorType.Fatal);
                        }

                        Log.Info("Удаление объявления админом adv: " + idAdv + " user: " + owner.Id + " (" +owner.Name + ")");
                    }

                    foreach (var item in context.offer_feed_site.Where(x=>x.id_offer == idAdv).ToList())
                    {
                        context.offer_feed_site.Remove(item);
                    }
                    foreach (var item in context.property_image.Where(x => x.id_property== propId).ToList())
                    {
                        context.property_image.Remove(item);
                    }
                    foreach (var item in context.properties.Where(x => x.id_property == propId).ToList())
                    {
                        context.properties.Remove(item);
                    }

                    context.offers.Remove(offer);
                    context.SaveChanges();
                    return true;
                }
            }
            catch (ValidationException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "[DbOfferManager::TryDelete] idAdv: " + idAdv+ " idUser: "+ idUser);
            }
            return false;
        }

        /// <summary>
        /// Удалить все объявления организации за исключением некоторых
        /// </summary>
        /// <param name="excludeAdv">Список id объявлений, которые нужно оставить</param>
        /// <param name="idOrg">id Организации</param>
        /// <param name="onlyFromImport">Только те, которые были импортированы из фида</param>
        /// <returns></returns>
        public static bool TryDelete(List<long> excludeAdv, long idOrg, bool onlyFromImport, out int countDeleted)
        {
            countDeleted = 0;
            if (!onlyFromImport)
                throw new NotImplementedException();
            var org = DbOrganizationManager.GetOrganizationById(idOrg);
            if (org == null)
                return true;

            try
            {
                using (var context = new realtyMLSEntities())
                {
                    var offers = context.offers.Where(x => x.sales_agent.id_sales_agent_organization == idOrg && !excludeAdv.Contains(x.id_offer));
                    if (onlyFromImport)
                        offers = offers.Where(x => x.feed_internal_id.HasValue);
                    if (offers.Count() == 0)
                        return true;

                    foreach (var offer in offers.ToList())
                    {
                        var propId = offer.id_property.Value;

                        foreach (var item in offer.offer_feed_site.ToList())
                        {
                            context.offer_feed_site.Remove(item);
                        }
                        foreach (var item in context.property_image.Where(x => x.id_property == propId).ToList())
                        {
                            context.property_image.Remove(item);
                        }
                        foreach (var item in context.properties.Where(x => x.id_property == propId).ToList())
                        {
                            context.properties.Remove(item);
                        }

                        context.offers.Remove(offer);
                        context.SaveChanges();
                        countDeleted++;
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "[DbOfferManager::TryDelete] idOrg: " + idOrg + " onlyFromImport: " + onlyFromImport+ "\nexcludeAdv: {"+string.Join(", ", excludeAdv)+"}");
            }
            return false;
        }
    }
}

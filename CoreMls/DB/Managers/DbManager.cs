using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Transactions;
using Core;
using Core.Services;
using CoreMls.DB.LocalEntities;
using CoreMls.DB.Model;
using CoreMls.Enum;
using CoreMls.Managers;
using Logger;

namespace CoreMls.DB.Managers
{
    public static class DbManager
    {
        public const int DefaultRetryCount = 5;
        public const int DeadlockErrorNumber = 1205;
        public const int LockingErrorNumber = 1222;
        public const int UpdateConflictErrorNumber = 3960;

        public static void Init()
        {
            Sheduler.AddOperation(new TimeSpan(12, 0, 0), true, () =>
            {
                using (var context = new realtyMLSEntities())
                {
                    foreach (var org in context.sales_agent_organization)
                    {
                        Log.Debug("Проверяем фид для организации: " + org.id_sales_agent_organization);
                        if (!FeedManager.TryGetOrCreateFeed(org.id_sales_agent_organization, out var file,
                            out var date))
                        {
                            Log.Fatal("не создан фид для организации: " + org.id_sales_agent_organization);
                        }
                    }
                }
            }, true, "CreateFeed");
            Sheduler.AddOperation(new TimeSpan(6, 0, 0), true, () =>
            {
                try
                {
                    using (var context = new realtyMLSEntities())
                    {
                        foreach (var item in context.sales_agent_organization_import_feed)
                        {
                            var orgName = item.sales_agent_organization.sales_agent_organization_name + "(" + item.id_organization + ")";
                            if (item.last_import_dt.HasValue && (DateTime.Now - item.last_import_dt.Value).TotalHours < 24)
                            {
                                Log.Debug("Не импортируем фид для организации: " + orgName +
                                          " тк времени прошло менее 24 часов");
                                continue;
                            }

                            if (string.IsNullOrEmpty(item.contact_email))
                            {
                                Log.Error("Не импортируем фид для организации: " + orgName + " тк не указан емейл", true);
                                continue;

                            }

                            Log.Debug("Импортируем фид для организации: " + item.sales_agent_organization.sales_agent_organization_name + "(" + item.id_organization + ")");
                            if (FeedManager.TryImport(FeedSite.Yandex, item.file_url, item.id_organization, out var importResult))
                            {
                                item.last_import_dt = DateTime.Now;
                                context.sales_agent_organization_import_feed.AddOrUpdate(item);
                                //context.SaveChanges();

                                EmailManager.TrySendEmail(new SendEmailRequest
                                {
                                    Caption =
                                        "[Rieltor-service.ru] Отчет по загрузке фида от " + DateTime.Now.ToString("g"),
                                    Content = importResult.ToHtml(item.file_url),
                                    EmailTo = item.contact_email,
                                    BccList = new List<string> {"rieltorservice@ya.ru"}
                                });
                            }
                            else
                            {
                                Log.Fatal("не импортирован фид для организации: " + orgName);
                            }
                        }
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, LogType.Fatal, true, "[Sheduler::ImportFeed]");
                }
            }, true, "ImportFeed");
        }

        public static long? GetBuilderIdByName(string name)
        {
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))
                {
                    using (var context = new realtyMLSEntities())
                    {
                        var n = name.ToLower();
                        var builders = context.builders.Where(x => x.name.ToLower() == n);

                        if (builders.Count() == 0)
                            return null;
                        var builderId = builders.FirstOrDefault()?.id;
                        transaction.Complete();
                        return builderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true);
            }

            return null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Transactions;
using Core;
using CoreMls.DB.LocalEntities;
using CoreMls.DB.Model;
using CoreMls.Enum;
using Logger;

namespace CoreMls.DB.Managers
{
    public static class DbSearchManager
    {
        /// <summary>
        /// Поиск объявлений
        /// </summary>
        public static List<MlsAdvertisment> SearchPages(SearchMlsFilter filter, out int totalCountByFilter)
        {
            totalCountByFilter = 0;
            var result = new List<MlsAdvertisment>();
            try
            {
                if (SettingsManager.IsStartAsAPI)
                    Log.Debug(filter.ToString());
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                    {IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted}))
                {
                    using (var context = new realtyMLSEntities())
                    {
                        if (filter.Id.HasValue)
                        {
                            var adv = DbOfferManager.GetAdvById(filter.Id.Value);
                            if (adv == null) return result;
                            totalCountByFilter = 1;
                            result.Add(adv);
                            return result;
                        }

                        IQueryable<offer> offers = null;

                        offers = context.offers.Where(page => (page.delete_dt == null));
                        if (!string.IsNullOrEmpty(filter.AdvType))
                        {
                            switch (filter.AdvType)
                            {
                                case "actialy":
                                    offers = context.offers.Where(x => !x.delete_dt.HasValue);
                                    break;
                                case "archive":
                                    offers = context.offers.Where(x => x.delete_dt.HasValue);
                                    break;
                                case "all":
                                    offers = context.offers;
                                    break;

                                default: throw new Exception("Неизвестный тип фильтрации объявлений");
                            }
                        }
                        if (filter.OfferType != OfferType.Default)
                        {
                            offers = offers.Where(x => x.offer_type.id_offer_type == (int)filter.OfferType);
                            if (filter.OfferType == OfferType.Rent && filter.RentPeriod != PricePeriod.Default)
                                offers = offers.Where(x => x.price_id_period == (int)filter.RentPeriod);
                        }

                        if (filter.SellType != SellType.Default)
                            offers = offers.Where(x => x.id_sell_type == (long)filter.SellType);

                        //
                        if (filter.ObjTypes != null && filter.ObjTypes.Count != 0)
                        {
                            offers = offers.Where(page =>
                                page.property.id_property_category.HasValue && filter.ObjTypes.Contains(page.property.id_property_category.Value));
                        }
                        //
                        if (filter.CountRooms != null && filter.CountRooms.Count > 0)
                        {
                            offers = offers.Where(page =>
                                page.property.rooms.HasValue && filter.CountRooms.Contains(page.property.rooms.Value));
                        }
                        if (filter.IsNewFlat.HasValue)
                            offers = offers.Where(x => x.property.new_flat.HasValue && x.property.new_flat.Value == filter.IsNewFlat.Value);

                        if (filter.CostMin > 0)
                            offers = offers.Where(x => x.price_value >= filter.CostMin);
                        if (filter.CostMax > 0)
                            offers = offers.Where(x => x.price_value <= filter.CostMax);

                        if (filter.OfferCategory.HasValue)
                            offers = offers.Where(x => x.id_offer_yrl_category.HasValue && x.id_offer_yrl_category.Value == filter.OfferCategory.Value);

                        if (filter.ContractType.HasValue)
                            offers = offers.Where(x => x.id_contract_type.HasValue && x.id_contract_type.Value == filter.ContractType.Value);
                        if (filter.IsOnPrepayment)
                            offers = offers.Where(x => x.is_on_prepayment.HasValue && x.is_on_prepayment.Value);
                        if (filter.IsCanMortgage)
                            offers = offers.Where(x => x.mortgage.HasValue && x.mortgage.Value);
                        if(filter.LotAreaState.HasValue)
                            offers = offers.Where(x => x.property.id_lot_type == filter.LotAreaState);
                        if(filter.CountRoomsOffered.HasValue)
                            offers = offers.Where(x => x.property.rooms_offered == filter.CountRoomsOffered);
                        #region Area
                        //
                        if (filter.AreaTotalMin > 0)
                            offers = offers.Where(page => page.property.area_value.HasValue && page.property.area_value >= filter.AreaTotalMin);
                        if (filter.AreaTotalMax > 0)
                            offers = offers.Where(page => page.property.area_value.HasValue && page.property.area_value <= filter.AreaTotalMax);
                        //
                        if (filter.AreaKitchenMin > 0)
                            offers = offers.Where(page => page.property.area_value.HasValue && page.property.kitchen_space_value >= filter.AreaKitchenMin);
                        if (filter.AreaKitchenMax > 0)
                            offers = offers.Where(page => page.property.area_value.HasValue && page.property.kitchen_space_value <= filter.AreaKitchenMax);
                        //
                        if (filter.AreaLivingMin > 0)
                            offers = offers.Where(page => page.property.area_value.HasValue && page.property.living_space_value >= filter.AreaLivingMin);
                        if (filter.AreaLivingMax > 0)
                            offers = offers.Where(page => page.property.area_value.HasValue && page.property.living_space_value <= filter.AreaLivingMax);
                        //
                        if (filter.LotAreaTotalMin > 0)
                            offers = offers.Where(page => page.property.lot_area_value.HasValue && page.property.lot_area_value >= filter.LotAreaTotalMin);
                        if (filter.LotAreaTotalMax > 0)
                            offers = offers.Where(page => page.property.lot_area_value.HasValue && page.property.lot_area_value <= filter.LotAreaTotalMax);
                        //
                        if (filter.AreaRoomMin > 0)
                            offers = offers.Where(page => page.property.room_space_value.HasValue && page.property.room_space_value >= filter.AreaRoomMin);
                        if (filter.AreaRoomMax > 0)
                            offers = offers.Where(page => page.property.room_space_value.HasValue && page.property.room_space_value <= filter.AreaRoomMax);
                        //
                        #endregion

                        #region Floor
                        //
                        if (filter.FloorMin != 0)
                            offers = offers.Where(page => page.property.floor.HasValue && page.property.floor.Value >= filter.FloorMin);
                        if (filter.FloorMax != 0)
                            offers = offers.Where(page => page.property.floor.HasValue && page.property.floor.Value <= filter.FloorMax);
                        //
                        if (filter.FloorTotalMin != 0)
                            offers = offers.Where(page => page.property.building.floors_total.HasValue && page.property.building.floors_total.Value >= filter.FloorTotalMin);
                        if (filter.FloorTotalMax != 0)
                            offers = offers.Where(page => page.property.building.floors_total.HasValue && page.property.building.floors_total.Value <= filter.FloorTotalMax);
                        //
                        if (!string.IsNullOrEmpty(filter.FloorDetail))
                        {
                            switch (filter.FloorDetail)
                            {
                                // не первый
                                case "1+":
                                    offers = offers.Where(page => page.property.floor.HasValue && page.property.floor.Value > 1);
                                    break;
                                // не последний
                                case "-N":
                                    offers = offers.Where(page => page.property.floor.HasValue && page.property.floor.Value < page.property.building.floors_total);
                                    break;
                                // не первый и не последний
                                case "1+-N":
                                    offers = offers.Where(page => page.property.floor.HasValue && page.property.floor.Value > 1
                                                                                               && page.property.floor.HasValue && page.property.floor.Value < page.property.building.floors_total);
                                    break;
                                default: throw new ArgumentOutOfRangeException("", "Указано неправильное уточнение по этажу");
                            }
                        }
                        //
                        #endregion

                        if (filter.DistanceOutCityMin > 0)
                            offers = offers.Where(page => page.property.building.building_location.distance.HasValue && page.property.building.building_location.distance >= filter.DistanceOutCityMin);
                        if (filter.DistanceOutCityMax > 0)
                            offers = offers.Where(page => page.property.building.building_location.distance.HasValue && page.property.building.building_location.distance <= filter.DistanceOutCityMax);
                        //
                        if (filter.HouseMaterials != null && filter.HouseMaterials.Count != 0)
                        {
                            offers = offers.Where(page =>
                                page.property.building.id_building_type.HasValue &&
                                filter.HouseMaterials.Contains(page.property.building.id_building_type.Value));
                        }
                        //
                        if (filter.RenovationType != 0)
                            offers = offers.Where(x => x.property.id_property_renovation.HasValue && x.property.id_property_renovation.Value == filter.RenovationType);
                        //
                        if (filter.CellHeightMin != 0)
                            offers = offers.Where(x=>x.property.ceiling_height >= filter.CellHeightMin);
                        if (filter.CellHeightMax != 0)
                            offers = offers.Where(x => x.property.ceiling_height <= filter.CellHeightMax);
                        //
                        if (filter.BuildYearMin.HasValue)
                            offers = offers.Where(x => x.property.building.built_year.HasValue && x.property.building.built_year.Value <= filter.BuildYearMin);
                        if (filter.BuildYearMax.HasValue)
                            offers = offers.Where(x => x.property.building.built_year.HasValue && x.property.building.built_year.Value >= filter.BuildYearMax);
                        //
                        if (filter.CountLiftMin != 0)
                            offers = offers.Where(x => x.property.lift_passenger_count.HasValue && x.property.lift_weight_count.HasValue && x.property.lift_passenger_count.Value + x.property.lift_weight_count.Value >= filter.CountLiftMin
                                                       || x.property.lift_passenger_count.HasValue && x.property.lift_passenger_count.Value >= filter.CountLiftMin
                                                       || x.property.lift_weight_count.HasValue && x.property.lift_weight_count.Value >= filter.CountLiftMin);
                        //
                        if (!string.IsNullOrEmpty(filter.BalconyType))
                        {
                            switch (filter.BalconyType)
                            {
                                case "":// ToDO BalconyType
                                    break;
                                default: throw new ArgumentOutOfRangeException("","Неизвестный тип балкона\\лоджии");
                            }
                        }

                        if (filter.IsStudia.HasValue && filter.IsStudia.Value)
                            offers = offers.Where(x => x.property.studio.HasValue && x.property.studio.Value);
                        else if (filter.IsOpenPlan)
                            offers = offers.Where(x => x.property.open_plan.HasValue && x.property.open_plan.Value);
                        else if (filter.PlainingType.HasValue)
                            offers = offers.Where(x => x.property.id_rooms_type.HasValue && x.property.id_rooms_type.Value == filter.PlainingType.Value);

                        if (filter.WindowView.HasValue)
                        {
                            offers = offers.Where(x =>
                                x.property.id_window_view_type.HasValue &&
                                x.property.id_window_view_type.Value == filter.WindowView.Value);
                        }

                        if (!string.IsNullOrEmpty(filter.Phone))
                        {
                            var phone = filter.Phone.Replace(" ", "").Replace("+", "").Replace("-", "").Replace("(", "").Replace(")", "").Replace("_", "");
                            if(long.TryParse(phone, out var ph))
                                offers = offers.Where(x => x.sales_agent.sales_agent_phone.Any(y => y.phone_number == ph));
                        }


                        // Если указаны районы
                        if (filter.Districts != null && filter.Districts.Count != 0)
                        {
                            // Если есть еще и мкр
                            if (filter.MicroDistricts != null && filter.MicroDistricts.Count != 0)
                            {
                                offers = offers.Where(x =>
                                    x.property.building.building_location.id_micro_district.HasValue &&
                                    filter.MicroDistricts.Contains(x.property.building.building_location.id_micro_district.Value)
                                    || x.property.building.building_location.id_district.HasValue &&
                                    filter.Districts.Contains(x.property.building.building_location.id_district.Value));
                            }
                            // Если только районы
                            else
                            {
                                offers = offers.Where(x =>
                                    x.property.building.building_location.id_district.HasValue &&
                                    filter.Districts.Contains(x.property.building.building_location.id_district.Value));
                            }
                        }
                        // если указаны только мкр
                        else if (filter.MicroDistricts != null && filter.MicroDistricts.Count != 0)
                        {
                            offers = offers.Where(x =>
                                x.property.building.building_location.id_micro_district.HasValue &&
                                filter.MicroDistricts.Contains(x.property.building.building_location.id_micro_district.Value));
                        }
                        
                        if(filter.HeatingType.HasValue)
                            offers = offers.Where(x => x.property.heating_supply.HasValue && x.property.heating_supply.Value == filter.HeatingType.Value);
                        if (filter.IsWithoutCommission)
                            offers = offers.Where(x => x.id_commission_type == (long)CommissionType.None);

                        if (filter.IsWithFurniture)
                            offers = offers.Where(x => x.property.room_furniture.HasValue && x.property.room_furniture.Value);

                        if (filter.IsOnlyWithPhoto)
                            offers = offers.Where(x => x.property.property_image.Any());


                        // исключаем старые объявления (нужно для перезагрузки отчета по фильтру, чтобы не показывать старее)
                        /*
                         if (filter.DateBegin != DateTime.MinValue)
                            offers = offers.Where(page =>
                                DbFunctions.TruncateTime(page.create_dt) > filter.DateBegin.Date);
                        if (filter.DateEnd != DateTime.MinValue)
                            offers = offers.Where(
                                page => DbFunctions.TruncateTime(page.create_dt) < filter.DateEnd.Date);
                        */
                        /*
                        // исключаем некоторые объявления (нужно для перезагрузки отчета по фильтру, чтобы не показывать опять теже самые)
                        if (filter.ExcludeIdList != null && filter.ExcludeIdList.Count != 0)
                        {
                            pages = pages.Where(page => !filter.ExcludeIdList.Contains(page.Id));
                        }*/

                        #region Поиск по моим объектам
                        if (filter.UserId.HasValue || filter.OrganizationId.HasValue)
                        {
                            if (filter.UserId.HasValue && (filter.FilterBy == "user" || string.IsNullOrEmpty(filter.FilterBy)))
                            {
                                offers = offers.Where(x => x.id_sales_agent == filter.UserId.Value);
                            }
                            else if (filter.OrganizationId.HasValue && filter.FilterBy == "org")
                            {
                                offers = offers.Where(x => x.sales_agent.id_sales_agent_organization == filter.OrganizationId.Value);
                            }

                            offers = offers.OrderByDescending(page => page.create_dt);
                            totalCountByFilter = offers.Count();
                            if (filter.IndexPage != 0)
                                offers = offers.Skip(filter.Count * filter.IndexPage);
                            offers = offers.Take(filter.Count);
                            foreach (var offer in offers)
                            {
                                if (offer == null)
                                    continue;
                                var adv = new MlsAdvertisment(offer);
                                result.Add(adv);
                            }
                            transaction.Complete();
                            return result;
                        }
                        #endregion

                        if (!string.IsNullOrEmpty(filter.Adress))
                        {
                            // Если указана улица, но без дома
                            if (string.IsNullOrEmpty(filter.House) && !string.IsNullOrEmpty(filter.Street))
                            {
                                offers = offers.Where(page => page.property.building.building_location.address.Contains(filter.Street));
                            }
                            else
                            {
                                offers = offers.Where(page => filter.Adress.Contains(page.property.building.building_location.address));
                            }
                        }
                        switch (filter.OrderType)
                        {
                            case 1: // Сначала старые
                                offers = offers.OrderBy(page => page.create_dt);
                                break;
                            case 2: // Сначала дешевые
                                offers = offers.OrderBy(page => page.price_value);
                                break;
                            case 3: // Сначала дорогие
                                offers = offers.OrderByDescending(page => page.price_value);
                                break;
                            case 0:
                            default: // Сначала новые
                                offers = offers.OrderByDescending(page => page.create_dt);
                                break;
                        }

                        totalCountByFilter = offers.Count();
                        if (filter.IndexPage > 0)
                            offers = offers.Skip(filter.Count * filter.IndexPage);
                        offers = offers.Take(filter.Count);
                        foreach (var offer in offers)
                        {
                            if (offer == null)
                                continue;
                            var adv = new MlsAdvertisment(offer);
                            result.Add(adv);
                        }
                        
                        transaction.Complete();
                    }
                }
            }
            catch (AggregateException ae)
            {
                ae.Handle((x) =>
                {
                    Log.Exception(x, LogType.Fatal, true);
                    return false;
                });
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

            return result;
        }
    }
}

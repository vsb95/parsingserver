﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using CoreMls.DB.LocalEntities;
using CoreMls.DB.LocalEntities.Report;
using CoreMls.DB.Model;
using Logger;

namespace CoreMls.DB.Managers
{
    public static class DbReportManager
    {
        public static List<ReportItemCountOfferPerOrg> GetCountOfferPerOrg(DateTime? beginDate, DateTime? endDate)
        {
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew, new
                    TransactionOptions
                    {
                        IsolationLevel = IsolationLevel.ReadUncommitted
                    }))
                {
                    using (var context = new realtyMLSEntities())
                    {
                        var offers = context.offers.Where(x=>!x.delete_dt.HasValue);

                        if (beginDate.HasValue)
                            offers = offers.Where(x => x.create_dt.HasValue && x.create_dt.Value >= beginDate.Value);
                        if (endDate.HasValue)
                            offers = offers.Where(x => x.create_dt.HasValue && x.create_dt.Value <= endDate.Value);

                        var report = offers.GroupBy(x => x.sales_agent.id_sales_agent_organization)
                            .Select(x => new ReportItemCountOfferPerOrg {Count = x.Count(), OrgId = x.Key}).OrderByDescending(x=>x.Count).ToList();
                        
                        transaction.Complete();
                        return report;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "[DbReportManager::GetCountOfferOrg] "+beginDate?.ToString("g")+" - "+ endDate?.ToString("g"));
            }

            return null;
        }
    }
}

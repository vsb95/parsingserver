﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoreMls.Enum;
using Logger;

namespace CoreMls.DB
{
    public class SearchMlsFilter
    {
        public string Adress { get; set; }
        public OfferType OfferType { get; set; } = OfferType.Default;
        public string ObjectType { get; set; }
        public long? OfferCategory { get; set; }
        public PricePeriod RentPeriod { get; set; } = PricePeriod.Default;
        public decimal AreaTotalMin { get; set; }
        public decimal AreaTotalMax { get; set; }
        public decimal AreaLivingMin { get; set; }
        public decimal AreaLivingMax { get; set; }
        public decimal AreaKitchenMin { get; set; }
        public decimal AreaKitchenMax { get; set; }
        public decimal LotAreaTotalMin { get; set; }
        public decimal LotAreaTotalMax { get; set; }
        public decimal AreaRoomMin { get; set; }
        public decimal AreaRoomMax { get; set; }
        public decimal DistanceOutCityMin { get; set; }
        public decimal DistanceOutCityMax { get; set; }
        public int FloorMin { get; set; }
        public int FloorMax { get; set; }
        public string FloorDetail { get; set; }
        public int FloorTotalMin { get; set; }
        public int FloorTotalMax { get; set; }
        public string HouseType { get; set; }
        public long? BuildYearMin { get; set; }
        public long? BuildYearMax { get; set; }
        public decimal CellHeightMin { get; set; }
        public decimal CellHeightMax { get; set; }
        public int CountLiftMin { get; set; }
        public string BalconyType { get; set; } // балкон\лоджия
        public long? WindowView { get; set; }
        public long RenovationType { get; set; }
        public long? PlainingType { get; set; }
        public bool? IsStudia { get; set; } = null;
        public long FlatPlaceType { get; set; }
        public string ToiletType { get; set; }
        public string ToiletCount { get; set; }
        public long? LotAreaState { get; set; }
        public SellType SellType { get; set; } = SellType.Default;
        public long? ContractType { get; set; }
        public string Phone { get; set; }
        public long? Id { get; set; }

        public decimal CostMin { get; set; }
        public decimal CostMax { get; set; }

        public string Street { get; set; }
        public string House { get; set; }

        public bool? HeatingType { get; set; }
        public bool IsWithoutCommission { get; set; }
        public bool IsOnPrepayment { get; set; }
        public bool IsCanMortgage { get; set; }
        public bool IsOnlyWithPhoto { get; set; }
        public bool IsWithFurniture { get; set; }

        public int? CountRoomsOffered { get; set; }
        public List<long> CountRooms { get; set; }

        public List<int> Districts { get; set; } = new List<int>();
        public List<int> MicroDistricts { get; set; } = new List<int>();

        /// <summary>
        /// Количество на странице
        /// </summary>
        public int Count { get; set; } = 50;

        /// <summary>
        /// № страницы
        /// </summary>
        public int IndexPage { get; set; } = 0;

        /// <summary>
        /// Тип сортировки
        /// </summary>
        public int OrderType { get; set; } = 0;

        public List<long> ObjTypes { get; set; } = new List<long>();

        /// <summary>
        /// Тип дома при продаже (кирпич\панель\ итд)
        /// </summary>
        public List<long> HouseMaterials { get; set; } = new List<long>();

        /// <summary>
        /// Новостройки
        /// </summary>
        public bool? IsNewFlat { get; set; }

        public string FilterBy { get; set; }
        public long? OrganizationId { get; set; }
        public long? UserId { get; set; }

        /// <summary>
        /// Тип объявлений - архивное и тд
        /// </summary>
        public string AdvType { get; set; }

        public bool IsOpenPlan { get; set; }
        
        protected bool Equals(SearchMlsFilter other)
        {
            return string.Equals(Adress, other.Adress) && OfferType == other.OfferType && string.Equals(ObjectType, other.ObjectType) && OfferCategory == other.OfferCategory && RentPeriod == other.RentPeriod && AreaTotalMin == other.AreaTotalMin && AreaTotalMax == other.AreaTotalMax && AreaLivingMin == other.AreaLivingMin && AreaLivingMax == other.AreaLivingMax && AreaKitchenMin == other.AreaKitchenMin && AreaKitchenMax == other.AreaKitchenMax && LotAreaTotalMin == other.LotAreaTotalMin && LotAreaTotalMax == other.LotAreaTotalMax && AreaRoomMin == other.AreaRoomMin && AreaRoomMax == other.AreaRoomMax && DistanceOutCityMin == other.DistanceOutCityMin && DistanceOutCityMax == other.DistanceOutCityMax && FloorMin == other.FloorMin && FloorMax == other.FloorMax && string.Equals(FloorDetail, other.FloorDetail) && FloorTotalMin == other.FloorTotalMin && FloorTotalMax == other.FloorTotalMax && string.Equals(HouseType, other.HouseType) && BuildYearMin == other.BuildYearMin && BuildYearMax == other.BuildYearMax && CellHeightMin == other.CellHeightMin && CellHeightMax == other.CellHeightMax && CountLiftMin == other.CountLiftMin && string.Equals(BalconyType, other.BalconyType) && WindowView == other.WindowView && RenovationType == other.RenovationType && PlainingType == other.PlainingType && IsStudia == other.IsStudia && FlatPlaceType == other.FlatPlaceType && string.Equals(ToiletType, other.ToiletType) && string.Equals(ToiletCount, other.ToiletCount) && LotAreaState == other.LotAreaState && SellType == other.SellType && ContractType == other.ContractType && string.Equals(Phone, other.Phone) && Id == other.Id && CostMin == other.CostMin && CostMax == other.CostMax && string.Equals(Street, other.Street) && string.Equals(House, other.House) && HeatingType == other.HeatingType && IsWithoutCommission == other.IsWithoutCommission && IsOnPrepayment == other.IsOnPrepayment && IsCanMortgage == other.IsCanMortgage && IsOnlyWithPhoto == other.IsOnlyWithPhoto && IsWithFurniture == other.IsWithFurniture && CountRoomsOffered == other.CountRoomsOffered && Equals(CountRooms, other.CountRooms) && Equals(Districts, other.Districts) && Equals(MicroDistricts, other.MicroDistricts) && Count == other.Count && IndexPage == other.IndexPage && OrderType == other.OrderType && Equals(ObjTypes, other.ObjTypes) && Equals(HouseMaterials, other.HouseMaterials) && IsNewFlat == other.IsNewFlat && string.Equals(FilterBy, other.FilterBy) && OrganizationId == other.OrganizationId && UserId == other.UserId && string.Equals(AdvType, other.AdvType) && IsOpenPlan == other.IsOpenPlan;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Adress != null ? Adress.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (int) OfferType;
                hashCode = (hashCode * 397) ^ (ObjectType != null ? ObjectType.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ OfferCategory.GetHashCode();
                hashCode = (hashCode * 397) ^ (int) RentPeriod;
                hashCode = (hashCode * 397) ^ AreaTotalMin.GetHashCode();
                hashCode = (hashCode * 397) ^ AreaTotalMax.GetHashCode();
                hashCode = (hashCode * 397) ^ AreaLivingMin.GetHashCode();
                hashCode = (hashCode * 397) ^ AreaLivingMax.GetHashCode();
                hashCode = (hashCode * 397) ^ AreaKitchenMin.GetHashCode();
                hashCode = (hashCode * 397) ^ AreaKitchenMax.GetHashCode();
                hashCode = (hashCode * 397) ^ LotAreaTotalMin.GetHashCode();
                hashCode = (hashCode * 397) ^ LotAreaTotalMax.GetHashCode();
                hashCode = (hashCode * 397) ^ AreaRoomMin.GetHashCode();
                hashCode = (hashCode * 397) ^ AreaRoomMax.GetHashCode();
                hashCode = (hashCode * 397) ^ DistanceOutCityMin.GetHashCode();
                hashCode = (hashCode * 397) ^ DistanceOutCityMax.GetHashCode();
                hashCode = (hashCode * 397) ^ FloorMin;
                hashCode = (hashCode * 397) ^ FloorMax;
                hashCode = (hashCode * 397) ^ (FloorDetail != null ? FloorDetail.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ FloorTotalMin;
                hashCode = (hashCode * 397) ^ FloorTotalMax;
                hashCode = (hashCode * 397) ^ (HouseType != null ? HouseType.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ BuildYearMin.GetHashCode();
                hashCode = (hashCode * 397) ^ BuildYearMax.GetHashCode();
                hashCode = (hashCode * 397) ^ CellHeightMin.GetHashCode();
                hashCode = (hashCode * 397) ^ CellHeightMax.GetHashCode();
                hashCode = (hashCode * 397) ^ CountLiftMin;
                hashCode = (hashCode * 397) ^ (BalconyType != null ? BalconyType.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ WindowView.GetHashCode();
                hashCode = (hashCode * 397) ^ RenovationType.GetHashCode();
                hashCode = (hashCode * 397) ^ PlainingType.GetHashCode();
                hashCode = (hashCode * 397) ^ IsStudia.GetHashCode();
                hashCode = (hashCode * 397) ^ FlatPlaceType.GetHashCode();
                hashCode = (hashCode * 397) ^ (ToiletType != null ? ToiletType.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ToiletCount != null ? ToiletCount.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ LotAreaState.GetHashCode();
                hashCode = (hashCode * 397) ^ (int) SellType;
                hashCode = (hashCode * 397) ^ ContractType.GetHashCode();
                hashCode = (hashCode * 397) ^ (Phone != null ? Phone.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Id.GetHashCode();
                hashCode = (hashCode * 397) ^ CostMin.GetHashCode();
                hashCode = (hashCode * 397) ^ CostMax.GetHashCode();
                hashCode = (hashCode * 397) ^ (Street != null ? Street.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (House != null ? House.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ HeatingType.GetHashCode();
                hashCode = (hashCode * 397) ^ IsWithoutCommission.GetHashCode();
                hashCode = (hashCode * 397) ^ IsOnPrepayment.GetHashCode();
                hashCode = (hashCode * 397) ^ IsCanMortgage.GetHashCode();
                hashCode = (hashCode * 397) ^ IsOnlyWithPhoto.GetHashCode();
                hashCode = (hashCode * 397) ^ IsWithFurniture.GetHashCode();
                hashCode = (hashCode * 397) ^ CountRoomsOffered.GetHashCode();
                hashCode = (hashCode * 397) ^ (CountRooms != null ? CountRooms.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Districts != null ? Districts.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (MicroDistricts != null ? MicroDistricts.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Count;
                hashCode = (hashCode * 397) ^ IndexPage;
                hashCode = (hashCode * 397) ^ OrderType;
                hashCode = (hashCode * 397) ^ (ObjTypes != null ? ObjTypes.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (HouseMaterials != null ? HouseMaterials.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ IsNewFlat.GetHashCode();
                hashCode = (hashCode * 397) ^ (FilterBy != null ? FilterBy.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ OrganizationId.GetHashCode();
                hashCode = (hashCode * 397) ^ UserId.GetHashCode();
                hashCode = (hashCode * 397) ^ (AdvType != null ? AdvType.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ IsOpenPlan.GetHashCode();
                return hashCode;
            }
        }

        public int SerializedValue()
        {
            if (OrganizationId.HasValue || UserId.HasValue || Id.HasValue)
                return GetHashCode();

            var hashCode = Adress?.Length ?? 0;
            hashCode = (hashCode * 397) ^ (int)OfferType;
            hashCode = (hashCode * 397) ^ (ObjectType?.Length ?? 0);
            hashCode = (hashCode * 397) ^ (int)(OfferCategory ?? 2);
            hashCode = (hashCode * 397) ^ (int)RentPeriod;
            hashCode = (hashCode * 397) ^ (int)AreaTotalMin;
            hashCode = (hashCode * 397) ^ (int)AreaTotalMax;
            hashCode = (hashCode * 397) ^ (int)AreaLivingMin;
            hashCode = (hashCode * 397) ^ (int)AreaLivingMax;
            hashCode = (hashCode * 397) ^ (int)AreaKitchenMin;
            hashCode = (hashCode * 397) ^ (int)AreaKitchenMax;
            hashCode = (hashCode * 397) ^ (int)LotAreaTotalMin;
            hashCode = (hashCode * 397) ^ (int)LotAreaTotalMax;
            hashCode = (hashCode * 397) ^ (int)AreaRoomMin;
            hashCode = (hashCode * 397) ^ (int)AreaRoomMax;
            hashCode = (hashCode * 397) ^ (int)DistanceOutCityMin;
            hashCode = (hashCode * 397) ^ (int)DistanceOutCityMax;
            hashCode = (hashCode * 397) ^ FloorMin;
            hashCode = (hashCode * 397) ^ FloorMax;
            hashCode = (hashCode * 397) ^ (FloorDetail?.Length ?? 0);
            hashCode = (hashCode * 397) ^ FloorTotalMin;
            hashCode = (hashCode * 397) ^ FloorTotalMax;
            hashCode = (hashCode * 397) ^ (HouseType?.Length ?? 0);
            hashCode = (hashCode * 397) ^ (int)(BuildYearMin??1);
            hashCode = (hashCode * 397) ^ (int)(BuildYearMax ?? 1);
            hashCode = (hashCode * 397) ^ (int)CellHeightMin;
            hashCode = (hashCode * 397) ^ (int)CellHeightMax;
            hashCode = (hashCode * 397) ^ CountLiftMin;
            hashCode = (hashCode * 397) ^ (BalconyType?.Length ?? 0);
            hashCode = (hashCode * 397) ^ (int)(WindowView ?? 1);
            hashCode = (hashCode * 397) ^ (int)RenovationType;
            hashCode = (hashCode * 397) ^ (int)(PlainingType ?? 1);
            hashCode = (hashCode * 397) ^ (int)(IsStudia.HasValue&& IsStudia.Value ? 1:0);
            hashCode = (hashCode * 397) ^ (int)FlatPlaceType;
            hashCode = (hashCode * 397) ^ (ToiletType?.Length ?? 0);
            hashCode = (hashCode * 397) ^ (ToiletCount?.Length ?? 0);
            hashCode = (hashCode * 397) ^ (int)(LotAreaState ?? 1);
            hashCode = (hashCode * 397) ^ (int)SellType;
            hashCode = (hashCode * 397) ^ (int)(ContractType ?? 1);
            hashCode = (hashCode * 397) ^ (Phone?.Length ?? 0);
            hashCode = (hashCode * 397) ^ (int)CostMin;
            hashCode = (hashCode * 397) ^ (int)CostMax;
            hashCode = (hashCode * 397) ^ (Street?.Length ?? 0);
            hashCode = (hashCode * 397) ^ (House?.Length ?? 0);
            hashCode = (hashCode * 397) ^ (int)(HeatingType.HasValue && HeatingType.Value ? 5 : 1);
            hashCode = (hashCode * 397) ^ (int)(IsWithoutCommission ? 5 : 1);
            hashCode = (hashCode * 397) ^ (int)(IsOnPrepayment ? 5 : 1);
            hashCode = (hashCode * 397) ^ (int)(IsCanMortgage ? 5 : 1);
            hashCode = (hashCode * 397) ^ (int)(IsOnlyWithPhoto ? 5 : 1);
            hashCode = (hashCode * 397) ^ (int)(IsWithFurniture ? 5 : 1);
            hashCode = (hashCode * 397) ^ (int) (CountRoomsOffered ?? 1);
            hashCode = (hashCode * 397) ^ (int)(CountRooms?.Sum() ?? 0);
            hashCode = (hashCode * 397) ^ (Districts?.Sum() ?? 0);
            hashCode = (hashCode * 397) ^ (MicroDistricts?.Sum() ?? 0);
            hashCode = (hashCode * 397) ^ Count;
            hashCode = (hashCode * 397) ^ IndexPage;
            hashCode = (hashCode * 397) ^ OrderType;
            hashCode = (hashCode * 397) ^ (int)(ObjTypes?.Sum() ?? 0);
            hashCode = (hashCode * 397) ^ (int)(HouseMaterials?.Sum() ?? 0);
            hashCode = (hashCode * 397) ^ (int)(IsNewFlat.HasValue && IsNewFlat.Value ? 5 : 1);
            hashCode = (hashCode * 397) ^ (FilterBy?.Length ?? 0);
            hashCode = (hashCode * 397) ^ (AdvType?.Length ?? 0);
            hashCode = (hashCode * 397) ^ (IsOpenPlan ?5:1);
            return hashCode;
        }
    }

   
}

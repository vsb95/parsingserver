//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoreMls.DB.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class sales_agent_phone
    {
        public long id_sales_agent_phone { get; set; }
        public Nullable<long> id_sales_agent { get; set; }
        public Nullable<long> phone_number { get; set; }
        public Nullable<System.DateTime> create_dt { get; set; }
        public Nullable<System.DateTime> delete_dt { get; set; }
    
        public virtual phone phone { get; set; }
        public virtual sales_agent sales_agent { get; set; }
    }
}

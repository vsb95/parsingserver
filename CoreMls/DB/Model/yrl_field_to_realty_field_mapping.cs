//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoreMls.DB.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class yrl_field_to_realty_field_mapping
    {
        public long id_yrl_field_to_realty_field_mapping { get; set; }
        public Nullable<long> id_yrl_field { get; set; }
        public Nullable<long> id_realty_field { get; set; }
        public Nullable<System.DateTime> create_dt { get; set; }
        public Nullable<System.DateTime> delete_dt { get; set; }
    
        public virtual realty_field realty_field { get; set; }
        public virtual yrl_field yrl_field { get; set; }
    }
}

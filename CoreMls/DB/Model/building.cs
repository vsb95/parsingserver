//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoreMls.DB.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class building
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public building()
        {
            this.properties = new HashSet<property>();
        }
    
        public long id_building { get; set; }
        public string building_name { get; set; }
        public Nullable<long> id_building_type { get; set; }
        public Nullable<long> id_building_location { get; set; }
        public Nullable<long> id_building_state { get; set; }
        public Nullable<long> id_builder { get; set; }
        public Nullable<long> id_building_commercial_type { get; set; }
        public Nullable<long> id_commercial_building_purpose { get; set; }
        public Nullable<long> id_office_class { get; set; }
        public Nullable<long> built_year { get; set; }
        public Nullable<long> id_ready_quarter { get; set; }
        public Nullable<long> id_building_phase { get; set; }
        public string building_series { get; set; }
        public string building_section { get; set; }
        public Nullable<long> id_parking_type { get; set; }
        public Nullable<bool> is_has_trash_tube { get; set; }
        public Nullable<int> count_weight_lift { get; set; }
        public Nullable<int> count_passenger_lift { get; set; }
        public Nullable<bool> lift { get; set; }
        public Nullable<long> floors_total { get; set; }
        public Nullable<bool> guarded_building { get; set; }
        public Nullable<bool> parking { get; set; }
        public Nullable<long> parking_places { get; set; }
        public Nullable<long> parking_place_price { get; set; }
        public Nullable<bool> parking_guest { get; set; }
        public Nullable<long> parking_guest_places { get; set; }
        public Nullable<bool> alarm { get; set; }
        public Nullable<bool> security { get; set; }
        public Nullable<bool> is_elite { get; set; }
        public Nullable<bool> twenty_four_seven { get; set; }
        public Nullable<bool> eating_facilities { get; set; }
        public Nullable<long> yandex_building_id { get; set; }
        public string yandex_house_id { get; set; }
        public Nullable<System.DateTime> create_dt { get; set; }
        public Nullable<System.DateTime> delete_dt { get; set; }
    
        public virtual builder builder { get; set; }
        public virtual building_location building_location { get; set; }
        public virtual building_phase building_phase { get; set; }
        public virtual building_state building_state { get; set; }
        public virtual building_type building_type { get; set; }
        public virtual commercial_building_purpose commercial_building_purpose { get; set; }
        public virtual office_class office_class { get; set; }
        public virtual ready_quarter ready_quarter { get; set; }
        public virtual building_commercial_type building_commercial_type { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<property> properties { get; set; }
    }
}

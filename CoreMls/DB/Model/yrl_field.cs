//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoreMls.DB.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class yrl_field
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public yrl_field()
        {
            this.category_yrl_field_valid_state = new HashSet<category_yrl_field_valid_state>();
            this.realty_to_yrl_errors = new HashSet<realty_to_yrl_errors>();
            this.yrl_category_field_hierarchy = new HashSet<yrl_category_field_hierarchy>();
            this.yrl_field_to_realty_field_mapping = new HashSet<yrl_field_to_realty_field_mapping>();
        }
    
        public long id_yrl_field { get; set; }
        public string yrl_field_name { get; set; }
        public string yrl_field_group_name { get; set; }
        public string yrl_field_group_pattern { get; set; }
        public Nullable<System.DateTime> create_dt { get; set; }
        public Nullable<System.DateTime> delete_dt { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<category_yrl_field_valid_state> category_yrl_field_valid_state { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<realty_to_yrl_errors> realty_to_yrl_errors { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<yrl_category_field_hierarchy> yrl_category_field_hierarchy { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<yrl_field_to_realty_field_mapping> yrl_field_to_realty_field_mapping { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoreMls.DB.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class ownership_type
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ownership_type()
        {
            this.garages = new HashSet<garage>();
        }
    
        public long id_ownership_type { get; set; }
        public string ownership_type_name { get; set; }
        public Nullable<long> id_language { get; set; }
        public Nullable<System.DateTime> create_dt { get; set; }
        public Nullable<System.DateTime> delete_dt { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<garage> garages { get; set; }
        public virtual language language { get; set; }
    }
}

﻿using System;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using CoreMls.DB.LocalEntities.Property;
using CoreMls.DB.Managers;
using CoreMls.DB.Model;
using CoreMls.Enum;
using Logger;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CoreMls.DB.LocalEntities
{
    /// <summary>
    /// Объявление Мультилистинга (наше)
    /// </summary>
     [JsonObject(Id = "offer", MemberSerialization = MemberSerialization.OptIn, ItemNullValueHandling = NullValueHandling.Ignore)]
    public class MlsAdvertisment
    {
        #region Properties
        [JsonProperty("id")]
        public long? Id { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("offer-type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public OfferType? Type { get; set; }
        [JsonProperty("property")]
        public AbstractProperty Property { get; set; }
        [JsonProperty("offer-status")]
        [JsonConverter(typeof(StringEnumConverter))]
        public OfferStatus? Status { get; set; }

        [JsonProperty("sell-type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public SellType? SellType { get; set; }

        [JsonProperty("contract-type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ContractType? ContractType { get; set; }

        [JsonProperty("category")]
        [JsonConverter(typeof(StringEnumConverter))]
        public OfferCategory? Category { get; set; }

        /// <summary>
        /// Торг?
        /// </summary>
        [JsonProperty("is-haggle")]
        public bool? IsHaggle { get; set; }

        /// <summary>
        /// Ипотека?
        /// </summary>
        [JsonProperty("is-can-mortgage")]
        public bool? IsMortgage { get; set; }

        /// <summary>
        /// под авансом?
        /// </summary>
        [JsonProperty("is-on-prepayment")]
        public bool? IsOnPrepayment { get; set; }

        /// <summary>
        /// Агент
        /// </summary>
        [JsonProperty("sales-agent")]
        public SalesAgent SalesAgent { get; set; }

        /// <summary>
        /// Комиссия агенту
        /// </summary>
        [JsonProperty("agent_fee")]
        public decimal? AgentFee { get; set; }

        /// <summary>
        /// не для агентств?
        /// </summary>
        public bool? IsNotForAgents { get; set; }

        /// <summary>
        /// К/У включены
        /// </summary>
        [JsonProperty("is-utilities-included")]
        public bool? IsUtilitiesIncluded { get; set; }

        [JsonProperty("utilities-price")]
        public decimal? UtilitiesPrice { get; set; }
        /// <summary>
        /// Уборка включена
        /// </summary>
        [JsonProperty("is-cleaning-included")]
        public bool? IsCleaningIncluded { get; set; }

        /// <summary>
        /// Электричество включено
        /// </summary>
        [JsonProperty("is-electricity-included")]
        public bool? IsElectricityIncluded { get; set; }
        
        /// <summary>
        /// ??
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// - к оплате (к\у и\или счетчики)
        /// </summary>
        [JsonProperty("utilities-type")]
        public string UtilitiesType
        {
            get
            {
                if(!Type.HasValue || Type.Value != OfferType.Rent)
                    return null;
                if (offer.utilities_included.HasValue && offer.utilities_included.Value
                    && offer.electricity_included.HasValue && offer.electricity_included.Value)
                    return "full";
                if (offer.electricity_included.HasValue && offer.electricity_included.Value)
                    return "counter";
                if (offer.utilities_included.HasValue && offer.utilities_included.Value)
                    return "utilities";
                return "none";
            }
        }
    
        /// <summary>
        /// + к оплате (к\у и\или счетчики)
        /// </summary>
        [JsonProperty("utilities-needle")]
        public string UtilitiesNeedle
        {
            get
            {
                if (!Type.HasValue || Type.Value != OfferType.Rent)
                    return null;
                if (offer.utilities_included.HasValue && offer.utilities_included.Value
                                                      && offer.electricity_included.HasValue && offer.electricity_included.Value)
                    return "none";
                if (offer.electricity_included.HasValue && offer.electricity_included.Value)
                    return "utilities";
                if (offer.utilities_included.HasValue && offer.utilities_included.Value)
                    return "counter";
                return "full";
            }
        }

        /// <summary>
        /// ??
        /// </summary>
        [JsonProperty("creation-date")]
        public DateTime? CreationDate { get; set; }

        /// <summary>
        /// Дата последнего обновления
        /// </summary>
        [JsonProperty("last-update-date")]
        public DateTime? LastUpdateDate { get; set; }

        /// <summary>
        /// Дата окончания (когда протухнет)
        /// </summary>
        public DateTime? ExpireDate { get; set; } = DateTime.Now.AddMonths(1);
        
        /// <summary>
        /// Стоимость
        /// </summary>
        [JsonProperty("price")]
        public decimal Price { get; set; }

        //      public Nullable<long> price_id_currency { get; set; } // ??
        //      public Nullable<long> price_id_unit { get; set; } // ??

        [JsonProperty("price-period")]
        [JsonConverter(typeof(StringEnumConverter))]
        public PricePeriod? PricePeriod { get; set; }

        [JsonProperty("commission-type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public CommissionType? CommissionType { get; set; }
        /// <summary>
        /// Комиссия в руб
        /// </summary>
        [JsonProperty("commission")]
        public long Commission { get; set; }

        /// <summary>
        /// Предоплата (в месяцах)
        /// </summary>
        [JsonProperty("prepay")]
        public long? Prepayment { get; set; }

        /// <summary>
        /// Залог
        /// </summary>
        [JsonProperty("rent-pledge")]
        public decimal? RentPledge { get; set; }

        public DateTime? CreateDate { get; set; }

        [JsonProperty("archive-date")]
        public DateTime? DeleteDate
        {
            get => _deleteDate;
            set
            {
                _deleteDate = value;
                if(offer != null)
                    offer.delete_dt = value;
            }
        }

        #endregion
        

        private readonly offer offer;
        private DateTime? _deleteDate;

        public MlsAdvertisment(offer offer)
        {
            if (offer == null)
                throw new ArgumentNullException("dbAdv");
            Id = offer.id_offer == 0 ? null : (long?)offer.id_offer;
            this.offer = offer;
            Property = AbstractProperty.Create(offer.property);
            if (offer.id_sales_agent.HasValue && offer.sales_agent == null)
            {
                SalesAgent = DbSalesAgentManager.GetAgentById(offer.id_sales_agent.Value);
            }
            else
            {
                SalesAgent = new SalesAgent(offer.sales_agent);
            }
            
            if (offer.id_sell_type.HasValue)
                SellType = (SellType)offer.id_sell_type.Value;
            if (offer.id_contract_type.HasValue)
                ContractType = (ContractType)offer.id_contract_type.Value;
            CreateDate = offer.create_dt ?? DateTime.Now;
            CreationDate= offer.creation_date ?? DateTime.Now;
            if (offer.id_offer_type.HasValue)
                Type = (OfferType) offer.id_offer_type.Value;
            if (offer.id_offer_status.HasValue)
                Status = (OfferStatus)offer.id_offer_status.Value;
            if (offer.price_id_period.HasValue)
                PricePeriod = (PricePeriod)offer.price_id_period.Value;
            if (offer.id_offer_yrl_category.HasValue)
                Category = (OfferCategory)offer.id_offer_yrl_category.Value;
            UtilitiesPrice = offer.utilities_price;
            CommissionType = (CommissionType)offer.id_commission_type;
            IsHaggle = offer.haggle;
            IsMortgage = offer.mortgage;
            IsOnPrepayment= offer.is_on_prepayment;
            AgentFee = offer.agent_fee;
            IsUtilitiesIncluded = offer.utilities_included;
            IsCleaningIncluded = offer.cleaning_included;
            IsElectricityIncluded = offer.electricity_included;
            LastUpdateDate = offer.last_update_date ?? DateTime.Now;
            ExpireDate = offer.expire_date ?? DateTime.Now.AddMonths(1);
            RentPledge = offer.rent_pledge_value;
            Price = offer.price_value;
            if(offer.commission.HasValue)
                Commission = offer.commission.Value;
            if (offer.prepayment.HasValue)
                Prepayment = offer.prepayment.Value;
            Title = offer.title;
            Url = offer.offer_url;
            DeleteDate = offer.delete_dt;
        }

        public offer ToDb()
        {
            offer.id_sales_agent = SalesAgent?.Id;
            return offer;
            if (Property == null)
                throw new ArgumentNullException("Не указан Property");
            
            if (SalesAgent == null)
                throw new ArgumentNullException("Не указан SalesAgent");

            var entity = new offer
            {
                id_offer_status = (long)Status.Value,
                haggle = IsHaggle,
                mortgage = IsMortgage
            };
            if (Id.HasValue)
                entity.id_offer = Id.Value;
            //
            #region Property
            entity.property = Property.ToDb();
            if (entity.property.new_flat.HasValue && Property.PropertyLivingCategory ==  PropertyLivingCategory.Apartment)
            {
                entity.id_offer_status = (long) (entity.property.new_flat.Value ? CoreMls.Enum.OfferStatus.FirstSell : CoreMls.Enum.OfferStatus.DirectSale);
            }
            #endregion
            //
            #region SalesAgent
            entity.sales_agent = SalesAgent.ToDb();
            #endregion
            //
            entity.agent_fee = AgentFee;
            entity.not_for_agents = IsNotForAgents;
            entity.utilities_included = IsUtilitiesIncluded;
            entity.cleaning_included = IsCleaningIncluded;
            entity.electricity_included = IsElectricityIncluded;
            entity.offer_url = Url;
            entity.creation_date = CreationDate;
            entity.last_update_date = LastUpdateDate;
            entity.expire_date = ExpireDate;
            //entity.id_vas = IdVas;
            entity.price_value = Price;
            entity.price_id_currency = 2; // захардкоженый RUB
            entity.price_id_period = (int) PricePeriod;
            entity.commission = Commission;
            entity.prepayment = Prepayment;
            /*
            entity.price_id_unit = (int) Unit;
            entity.lot_number = LotNumber;
            entity.security_payment = SecurityPayment;
            entity.rent_pledge = RentPledge;
            entity.id_taxation_form = IdTaxationForm;
            */
            entity.create_dt = CreateDate;
            entity.delete_dt = DeleteDate;
            return null;
        }
    }
}

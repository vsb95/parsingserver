﻿using System;
using CoreMls.DB.Model;

namespace CoreMls.DB.LocalEntities
{
    public class Metro
    {
        public long? Id { get; set; }
        public string MetroName { get; set; }
        public DateTime CreateDt { get; set; }
        public DateTime? DeleteDt { get; set; }

        private metro _metro;

        public Metro(metro metro)
        {
            if (metro == null)
            {
                _metro = new metro();
            }
            else
            {
                Id = metro.id_metro;
                _metro = metro;
            }

            MetroName = metro?.metro_name;
            CreateDt = metro.create_dt.Value;
        }
        public metro ToDb()
        {
            return _metro;
        }
    }
}

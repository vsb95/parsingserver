﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoreMls.DB.Managers;
using CoreMls.DB.Model;
using CoreMls.Enum;
using Logger;
using Newtonsoft.Json;

namespace CoreMls.DB.LocalEntities
{
    /// <summary>
    /// Риэлтор
    /// </summary>
    [JsonObject(Id = "sales-agent", MemberSerialization = MemberSerialization.OptIn)]
    public class SalesAgent
    {
        #region Properties
        [JsonProperty("id")]
        public long? Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        public SalesAgentCategory SalesAgentCategory { get; set; } = SalesAgentCategory.Agency;
        [JsonProperty("org")]
        public Organization Organization { get; set; }
        [JsonProperty("phone")]
        public string Phone { get; set; }
        [JsonProperty("phone-alternative")]
        public string PhoneAlternative { get; set; }
        [JsonProperty("org-url")]
        public string Url { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("photo")]
        public string PhotoUrl { get; set; }
        [JsonProperty("partner")]
        public string Partner { get; set; }
        [JsonProperty("is-virtual")]
        public bool IsVirtual { get; set; }
        [JsonProperty("create-date")]
        public DateTime CreateDate { get; set; } = DateTime.Now;
        public DateTime? DeleteDate { get; set; }


        [JsonProperty("category")]
        public string CategoryName => SalesAgentCategory.ToString();
        #endregion

        public SalesAgent()
        {
        }

        public SalesAgent(sales_agent salesAgent)
        {
            if(salesAgent.id_sales_agent != 0)
                Id = salesAgent.id_sales_agent;
            Name = salesAgent.sales_agent_name;
            if (salesAgent.id_sales_agent_organization != 0)
            {
                var org = DbOrganizationManager.GetOrganizationById(salesAgent.id_sales_agent_organization);
                Organization = org ?? new Organization
                {
                    Id = salesAgent.id_sales_agent_organization,
                    Name = salesAgent.sales_agent_organization
                };
            }
            Phone = salesAgent.sales_agent_phone.FirstOrDefault()?.phone_number?.ToString();
            Phone = GetFormattedPhone(Phone);
            if (salesAgent.sales_agent_phone.Count > 1)
            {
                PhoneAlternative = salesAgent.sales_agent_phone.LastOrDefault()?.phone_number?.ToString();
                PhoneAlternative = GetFormattedPhone(PhoneAlternative);
            }
            Url = salesAgent.sales_agent_url;
            Email = salesAgent.sales_agent_email;
            PhotoUrl = salesAgent.sales_agent_photo;
            Partner = salesAgent.sales_agent_partner;
            CreateDate = salesAgent.create_dt ?? DateTime.Now;
            IsVirtual = salesAgent.is_virtual;
        }

        public sales_agent ToDb()
        {
            var agent = new sales_agent
            {
                id_sales_agent_category = (long) SalesAgentCategory,
                sales_agent_name = Name,
                sales_agent_url = Url,
                sales_agent_email = Email,
                sales_agent_photo = PhotoUrl,
                sales_agent_partner = Partner,
                create_dt = CreateDate,
                delete_dt = DeleteDate,
                sales_agent_phone = new List<sales_agent_phone>(),
                is_virtual = IsVirtual,
                sales_agent_organization1 = null,
                id_sales_agent_organization = Organization.Save()
            };
            if (agent.id_sales_agent_organization == -1)
            {
                Log.Fatal("Не удалось добавить организацию: "+ Organization);
                agent.id_sales_agent_organization = Organization.Id.Value;
            }
            if (Id.HasValue)
                agent.id_sales_agent = Id.Value;
            if (!string.IsNullOrEmpty(Phone))
            {
                var phone = DbSalesAgentManager.GetSalesAgentPhone(Phone, Id ?? 0);
                agent.sales_agent_phone.Add(phone);
            }

            if (!string.IsNullOrEmpty(PhoneAlternative))
            {
                var phone = DbSalesAgentManager.GetSalesAgentPhone(PhoneAlternative, Id ?? 0);
                agent.sales_agent_phone.Add(phone);
            }

            return agent;
        }

        private string GetFormattedPhone(string notFormattedPhone)
        {
            if (string.IsNullOrEmpty(notFormattedPhone))
                return null;
            if(notFormattedPhone.Length<=6)
                return Regex.Replace(notFormattedPhone, @"(\d{1,2})(\d{2})(\d{2})", "$1-$2-$3");
            var result = Regex.Replace(notFormattedPhone, @"(7|8)(\d{3})(\d{3})(\d{4})", "$1 ($2) $3-$4");
            if (result.StartsWith("7"))
                return "+" + result;
            return result;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode()+ (int)Id;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreMls.DB.Managers;
using CoreMls.DB.Model;
using Newtonsoft.Json;

namespace CoreMls.DB.LocalEntities
{
    [JsonObject(Id = "organization", MemberSerialization = MemberSerialization.OptIn)]
    public class Organization
    {
        private long? _id;
        private string _name;
        private string _watermarkImage;
        private readonly sales_agent_organization _entity = new sales_agent_organization {create_dt = DateTime.Now};

        [JsonProperty("id")]
        public long? Id
        {
            get => _id;
            set
            {
                _id = value;
                if(value.HasValue)
                _entity.id_sales_agent_organization = value.Value;
            }
        }

        [JsonProperty("name")]
        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                _entity.sales_agent_organization_name = value;
            }
        }
        [JsonIgnore]
        public string WatermarkImage
        {
            get => _watermarkImage;
            set
            {
                _watermarkImage = value;
                _entity.watermark_image = value;
            }
        }

        public Organization()
        {

        }

        public Organization(sales_agent_organization org)
        {
            Id = org.id_sales_agent_organization;
            Name = org.sales_agent_organization_name;
            WatermarkImage = org.watermark_image;
            _entity = org;
        }

        public long Save()
        {
           return DbOrganizationManager.AddOrUpdate(_entity);
        }

        public override string ToString()
        {
            return Name + "(" + Id + ")";
        }
    }
}

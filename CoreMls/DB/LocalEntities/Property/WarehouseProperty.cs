﻿using System;
using CoreMls.DB.Model;
using Newtonsoft.Json;

namespace CoreMls.DB.LocalEntities.Property
{
    [JsonObject(Id = "warehouse-property", ItemNullValueHandling = NullValueHandling.Ignore)]
    public class WarehouseProperty : AbstractProperty
    {

        public WarehouseProperty(property property) : base(property)
        {
            
        }

        public override property ToDb()
        {
            var property = base.ToDb();
            property.warehouse = new warehouse()
            {

            };
            return property;
        }
    }
}

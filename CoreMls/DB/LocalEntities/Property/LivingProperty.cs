﻿using System.Globalization;
using CoreMls.DB.Model;
using CoreMls.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CoreMls.DB.LocalEntities.Property
{
    [JsonObject(Id = "living-property", ItemNullValueHandling = NullValueHandling.Ignore)]
    public class LivingProperty : AbstractProperty
    {
        public bool? IsNewFlat { get; set; }

        // ToDo Проверить какие свойства относятся к жилой, а какие к комерции
        public bool? AirConditioner { get; set; }
        public bool? Ventilation { get; set; }
        public bool? FireAlarm { get; set; }
        public bool? AccessControlSystem { get; set; }
        public bool? FlatAlarm { get; set; }
        public bool? RubbishChute { get; set; }
        public bool? WaterSupply { get; set; }
        public bool? SewerageSupply { get; set; }
        public bool? ElectricitySupply { get; set; }
        public long? ElectricCapacity { get; set; }
        public bool? GasSupply { get; set; }
        public bool? HeatingSupply { get; set; }
        public bool? Toilet { get; set; }
        public bool? Bath { get; set; }
        public bool? Shower { get; set; }
        public bool? Pool { get; set; }
        public bool? Billiard { get; set; }
        public bool? Sauna { get; set; }
        public bool? Phone { get; set; }
        public long? PhoneLines { get; set; }
        public bool? AddingPhoneOnRequest { get; set; }
        public bool? Internet { get; set; }
        public bool? SelfSelectionTelecom { get; set; }
        public bool? RoomFurniture { get; set; }
        public bool? KitchenFurniture { get; set; }
        public bool? Television { get; set; }
        public bool? WashingMachine { get; set; }
        public bool? Dishwasher { get; set; }
        public bool? Refrigerator { get; set; }
        public bool? BuiltInTech { get; set; }
        public long? IdFloorCoveringType { get; set; }
        public bool? WithChildren { get; set; }
        public bool? WithPets { get; set; }

        public int? CountToilet { get; set; }
        public int? CountBathroom { get; set; }

        [JsonIgnore]
        public long IdBathUnit { get; set; }

        public bool? Foundation { get; set; }
        public bool? Outbuilding { get; set; }
        public bool? Garage { get; set; }

        [JsonProperty("furniture-fill")]
        [JsonConverter(typeof(StringEnumConverter))]
        public FurnitureFillType? FurnitureFillType { get; set; }

        public LivingProperty(property property):base(property)
        {
            if (property == null)
                return;
            AirConditioner = property.air_conditioner;
            Ventilation = property.ventilation;
            FireAlarm = property.fire_alarm;
            AccessControlSystem = property.access_control_system;
            FlatAlarm = property.flat_alarm;
            RubbishChute = property.rubbish_chute;
            WaterSupply = property.water_supply;
            SewerageSupply = property.sewerage_supply;
            ElectricitySupply = property.electricity_supply;
            ElectricCapacity = property.electric_capacity;
            GasSupply = property.gas_supply;
            HeatingSupply = property.heating_supply;
            Toilet = property.toilet;
            Shower = property.shower;
            Pool = property.pool;
            Billiard = property.billiard;
            Sauna = property.sauna;
            Phone = property.phone;
            PhoneLines = property.phone_lines;
            AddingPhoneOnRequest = property.adding_phone_on_request;
            Internet = property.internet;
            SelfSelectionTelecom = property.self_selection_telecom;
            RoomFurniture = property.room_furniture;
            KitchenFurniture = property.kitchen_furniture;
            Television = property.television;
            WashingMachine = property.washing_machine;
            Dishwasher = property.dishwasher;
            Refrigerator = property.refrigerator;
            BuiltInTech = property.built_in_tech;
            IdFloorCoveringType = property.id_floor_covering_type;
            WithChildren = property.with_children;
            WithPets = property.with_pets;
            IsNewFlat = property.new_flat;
            Foundation = property.foundation;
            Bath = property.bath;
            Outbuilding = property.outbuilding;
            if (property.id_furniture_fill.HasValue)
                FurnitureFillType = (FurnitureFillType) property.id_furniture_fill.Value;
            Garage = property.is_has_garage;
            if (property.id_bathroom_unit.HasValue)
                IdBathUnit = property.id_bathroom_unit.Value - 1;
            CountBathroom = property.toilet_with_bath_count;
            CountToilet = property.toilet_count;
        }

        public override property ToDb()
        {
            var property = base.ToDb();
            property.id_property_type = 1;
            property.new_flat = IsNewFlat;
            property.air_conditioner = AirConditioner;
            property.ventilation = Ventilation;
            property.fire_alarm = FireAlarm;
            property.access_control_system = AccessControlSystem;
            property.flat_alarm = FlatAlarm;
            property.rubbish_chute = RubbishChute;
            property.water_supply = WaterSupply;
            property.sewerage_supply = SewerageSupply;
            property.electricity_supply = ElectricitySupply;
            property.electric_capacity = ElectricCapacity;
            property.gas_supply = GasSupply;
            property.heating_supply = HeatingSupply;
            property.toilet = Toilet;
            property.shower = Shower;
            property.pool = Pool;
            property.billiard = Billiard;
            property.sauna = Sauna;
            property.bath = Bath;
            property.phone = Phone;
            property.phone_lines = PhoneLines;
            property.adding_phone_on_request = AddingPhoneOnRequest;
            property.internet = Internet;
            property.self_selection_telecom = SelfSelectionTelecom;
            property.room_furniture = RoomFurniture;
            property.kitchen_furniture = KitchenFurniture;
            property.television = Television;
            property.washing_machine = WashingMachine;
            property.dishwasher = Dishwasher;
            property.refrigerator = Refrigerator;
            property.built_in_tech = BuiltInTech;
            property.id_floor_covering_type = IdFloorCoveringType;
            property.with_children = WithChildren;
            property.with_pets = WithPets;
            property.id_bathroom_unit = IdBathUnit + 1;
            property.toilet_count = CountToilet;
            property.toilet_with_bath_count = CountBathroom;
            return property;
        }
    }
}

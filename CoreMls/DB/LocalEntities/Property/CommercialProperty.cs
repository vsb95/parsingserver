﻿using CoreMls.DB.Model;
using Newtonsoft.Json;

namespace CoreMls.DB.LocalEntities.Property
{
    [JsonObject(Id = "commercial-property", ItemNullValueHandling = NullValueHandling.Ignore)]
    public class CommercialProperty : AbstractProperty
    {
        public bool? IsWarehouseResponsibleStorage { get; set; }
        public decimal? WarehousePalletPrice { get; set; }
        public bool? IsWarehouseFreightElevator { get; set; }
        public bool? IsWarehouseTruckEntrance { get; set; }
        public bool? IsWarehouseRamp { get; set; }
        public bool? IsWarehouseRailway { get; set; }
        public bool? IsWarehouseOfficeWarehouse { get; set; }
        public bool? IsWarehouseOpenArea { get; set; }
        public bool? IsWarehouseServiceThreePl { get; set; }
        public string WarehouseTemperatureComment { get; set; }

        public CommercialProperty(property property) : base(property)
        {
            
        }

        public override property ToDb()
        {
            var property = base.ToDb();
            property.warehouse = new warehouse
            {
                responsible_storage = IsWarehouseResponsibleStorage,
                pallet_price = WarehousePalletPrice,
                freight_elevator = IsWarehouseFreightElevator,
                truck_entrance = IsWarehouseTruckEntrance,
                ramp = IsWarehouseRamp,
                railway = IsWarehouseRailway,
                office_warehouse = IsWarehouseOfficeWarehouse,
                service_three_pl = IsWarehouseServiceThreePl,
                temperature_comment = WarehouseTemperatureComment
            };

            return property;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoreMls.DB.Model;
using CoreMls.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CoreMls.DB.LocalEntities.Property
{
    [JsonObject(Id = "property", MemberSerialization = MemberSerialization.OptIn, ItemNullValueHandling = NullValueHandling.Ignore)]
    public abstract class AbstractProperty
    {
        private List<string> _images;
        [JsonIgnore]
        public long? Id { get; set; }
        [JsonProperty("type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public PropertyType? PropertyType { get; protected set; }
        [JsonProperty("category")]
        [JsonConverter(typeof(StringEnumConverter))]
        public PropertyLivingCategory? PropertyLivingCategory { get; protected set; }
        [JsonProperty("commercial-type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public CommercialType? CommercialType { get; protected set; }
        [JsonProperty("building")]
        public Building Building { get; set; }

        [JsonProperty("cadastral-number")]
        public string CadastralNumber { get; set; }

        [JsonProperty("area-total")]
        public decimal? AreaValue { get; set; }
        public long? AreaIdUnit { get; set; }

        [JsonProperty("area-room")]
        public decimal? RoomSpaceValue { get; set; }
        public long? RoomSpaceIdUnit { get; set; }

        [JsonProperty("lot-area")]
        public decimal? LotAreaValue { get; set; }
        [JsonProperty("lot-area-unit")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Unit? LotAreaUnit { get; set; }
        [JsonProperty("lot-area-type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public LotType? LotType { get; set; }

        [JsonProperty("area-living")]
        public decimal? LivingSpaceValue { get; set; }
        public long? LivingSpaceIdUnit { get; set; }

        [JsonProperty("area-kitchen")]
        public decimal? KitchenSpaceValue { get; set; }
        public long? KitchenSpaceIdUnit { get; set; }
        public long? IdEntranceType { get; set; }
        [JsonProperty("room-type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public RoomType? RoomType { get; set; }
        [JsonProperty("count-rooms")]
        public long? CountRooms { get; set; }
        [JsonProperty("offered-rooms")]
        public long? RoomsOffered { get; set; }
        [JsonProperty("floor")]
        public long? Floor { get; set; }
        public bool? IsApartments { get; set; }
        [JsonProperty("is-studio")]
        public bool? IsStudio { get; set; }
        public bool? IsOpenPlan { get; set; }

        [JsonProperty("balcon-count")]
        public long? BalconCount { get; set; }
        [JsonProperty("loggia-count")]
        public long? LoggiaCount { get; set; }

        [JsonProperty("toilet-type")]
        public string ToiletType { get; set; }
        [JsonProperty("part-size")]
        public string PartSize { get; set; }

        [JsonProperty("window-type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public WindowType? WindowType { get; set; }
        [JsonProperty("window-view")]
        [JsonConverter(typeof(StringEnumConverter))]
        public WindowViewType? WindowViewType { get; set; }
        [JsonProperty("balcony-type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public BalconyType? BalconyType { get; set; }
        [JsonProperty("bathroom-unit")]
        [JsonConverter(typeof(StringEnumConverter))]
        public BathroomUnit? BathroomUnit { get; set; }

        [JsonProperty("ceiling-height")]
        public decimal? CeilingHeight { get; set; }
        public bool? IsPmg { get; set; }

        [JsonProperty("lift-passenger-count")]
        public int? LiftPassengerCount { get; set; }
        [JsonProperty("lift-weight-count")]
        public int? LiftWeightCount { get; set; }

        [JsonProperty("youtube")]
        public string YoutubeUrl { get; set; }

        [JsonProperty("plain-image")]
        public string FlatPlainImage { get; set; }

        [JsonProperty("renovation")]
        public string RenovationJson
        {
            get
            {
                switch (Renovation)
                {
                    case RenovationType.Дизайнерский: return "Дизайнерский";
                    case RenovationType.ТребуетРемонта: return "Требует ремонта";
                    case RenovationType.ЧастичныйРемонт: return "Частичный ремонт";
                    case RenovationType.ЧистоваяОтделка: return "Чистовая отделка";
                    case RenovationType.ЧерноваяОтделка: return "Черновая отделка";
                    case RenovationType.СОтделкой: return "С отделкой";
                    case RenovationType.Хороший: return "Хороший";
                    case RenovationType.ПодКлюч: return "Под ключ";
                    case RenovationType.Евро: return "Евро";
                    case RenovationType.Default:
                    case null:
                        return null;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
        [JsonProperty("renovation-id")]
        public RenovationType? Renovation { get; set; }
        [JsonProperty("quality")]
        [JsonConverter(typeof(StringEnumConverter))]
        public PropertyQuality? PropertyQuality { get; set; }
        [JsonProperty("flat-place-type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public FlatPlaceType? FlatPlaceType { get; set; }
        [JsonProperty("sell-type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public SellType? SellType { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("images")]
        public List<string> Images
        {
            get => _images;
            set
            {
                _images = value;
                var newLList = new List<property_image>();
                foreach (var img in value)
                {
                    newLList.Add(new property_image{image_url = img });
                }

                _property.property_image = newLList;
            }
        }

        [JsonProperty("area-unit")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Unit Unit { get; set; } = Unit.SqMeter;

        public DateTime CreateDt { get; set; } 
        public DateTime? DeleteDt { get; set; }

        private readonly property _property;
        protected AbstractProperty(property property)
        {
            if (property.id_property != 0)
                Id = property.id_property;
            
            _property = property;
            CreateDt = property?.create_dt ?? DateTime.Now;
            Building = new Building(property.building);
            Images = property.property_image.Select(x=>x.image_url).ToList();
            Description = property.description;
            if (property.id_property_category.HasValue)
                PropertyLivingCategory = (PropertyLivingCategory) property.id_property_category.Value;
            AreaValue = property.area_value;
            KitchenSpaceValue = property.kitchen_space_value;
            LivingSpaceValue = property.living_space_value;
            LotAreaValue = property.lot_area_value;
            if(property.lot_area_id_unit.HasValue)
                LotAreaUnit = (Unit)property.lot_area_id_unit;
            if(property.id_lot_type.HasValue)
                LotType = (LotType)property.id_lot_type;
            CadastralNumber = property.cadastral_number;
            PartSize = property.part;
            ToiletType = property.toilet_type;
            RoomSpaceValue = property.room_space_value;
            Floor = property.floor;
            CeilingHeight = property.ceiling_height;
            LiftPassengerCount = property.lift_passenger_count;
            LiftWeightCount = property.lift_weight_count;
            if (property.id_window_type.HasValue)
                WindowType = (WindowType) property.id_window_type.Value;
            if (property.id_window_view_type.HasValue)
                WindowViewType = (WindowViewType)property.id_window_view_type.Value;
            if (property.id_property_renovation.HasValue)
                Renovation =(RenovationType) property.id_property_renovation.Value;
            if (property.id_property_quality.HasValue)
                PropertyQuality = (PropertyQuality)property.id_property_quality.Value;
            if (property.id_flat_place_type.HasValue)
                FlatPlaceType = (FlatPlaceType)property.id_flat_place_type.Value;

            if (property.studio.HasValue && property.studio.Value)
                IsStudio = true;
            if (property.id_rooms_type.HasValue)
                RoomType = (RoomType) property.id_rooms_type.Value;
            BalconCount = property.balcony_count;
            LoggiaCount = property.loggia_count;
            YoutubeUrl = property.youtube_url;
            CountRooms = property.rooms;
            RoomsOffered = property.rooms_offered;
            FlatPlainImage = property.flat_plain_image;
        }

        public virtual property ToDb()
        {
            _property.building = Building?.ToDb();
            return _property;
            var property = new property();

            if (Id.HasValue)
                property.id_property = Id.Value;

            return property;
        }

        public static AbstractProperty Create(property property)
        {
            AbstractProperty result = null;
            if (property.id_garage.HasValue)
            {
                result = new GarageProperty(property);
            }
            else if (property.id_warehouse.HasValue)
            {
                result = new WarehouseProperty(property);
            }
            else if(property.property_property_commercial_type.Any())
            {
                result = new CommercialProperty(property);
            }
            else
            {
                result = new LivingProperty(property);
            }



                return result;
        }
    }
}


﻿using System;
using CoreMls.DB.Model;
using Newtonsoft.Json;

namespace CoreMls.DB.LocalEntities.Property
{
    [JsonObject(Id = "garage-property", ItemNullValueHandling = NullValueHandling.Ignore)]
    public class GarageProperty :AbstractProperty
    {
        public long? Id  { get; set; }
        public long? IdGarageType { get; set; }
        public long? IdGarageOwnershipType { get; set; }
        public string Name { get; set; }
        public long? IdParkingType { get; set; }
        public bool? IsAutomaticGates { get; set; }
        public bool? IsCctv { get; set; }
        public bool? IsInspectionPit { get; set; }
        public bool? IsCellar { get; set; }
        public bool? IsCarWash { get; set; }
        public bool? IsAutoRepair { get; set; }
        public bool? IsNewParking { get; set; }
        public DateTime CreateDt { get; set; }

        public GarageProperty(property property) : base(property)
        {
            Id = property.garage.id_garage;
            IdGarageType = property.garage.id_garage_type;
            IdGarageOwnershipType = property.garage.id_ownership_type;
            Name = property.garage.garage_name;
            IdParkingType = property.garage.id_parking_type;
            IsAutomaticGates = property.garage.automatic_gates;
            IsCctv = property.garage.cctv;
            IsInspectionPit = property.garage.inspection_pit;
            IsCellar = property.garage.cellar;
            IsCarWash = property.garage.car_wash;
            IsAutoRepair = property.garage.auto_repair;
            IsNewParking = property.garage.new_parking;
            CreateDt = property.create_dt ?? DateTime.Now;
        }

        public override property ToDb()
        {
            var property = base.ToDb();
            property.garage = new garage
            {
                id_garage_type = IdGarageType,
                id_ownership_type = IdGarageOwnershipType,
                garage_name = Name,
                id_parking_type = IdParkingType,
                automatic_gates = IsAutomaticGates,
                cctv = IsCctv,
                inspection_pit = IsInspectionPit,
                cellar = IsCellar,
                car_wash = IsCarWash,
                auto_repair = IsAutoRepair,
                new_parking = IsNewParking,
                create_dt = CreateDt
            };
            if (Id.HasValue)
                property.garage.id_garage = Id.Value;
            return property;
        }
    }
}

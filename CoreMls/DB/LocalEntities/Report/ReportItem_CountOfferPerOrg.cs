﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CoreMls.DB.LocalEntities.Report
{
    /// <summary>
    /// Элемент отчета кол-ва поданых объектов по организациям
    /// </summary>
    [JsonObject(Id = "report-item", ItemNullValueHandling = NullValueHandling.Ignore)]
    public class ReportItemCountOfferPerOrg
    {
        [JsonProperty("org-id")]
        public long OrgId { get; set; }
        
        [JsonProperty("offer-count")]
        public long Count { get; set; }

    }
}

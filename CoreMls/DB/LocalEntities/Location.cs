﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.Storage;
using CoreMls.DB.Model;
using Newtonsoft.Json;

namespace CoreMls.DB.LocalEntities
{
    [JsonObject(Id = "location", MemberSerialization = MemberSerialization.OptIn, ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Location
    {
        public long? Id { get; set; }
        public string Country { get; set; }
        [JsonProperty("region")]
        public string Region { get; set; }
        [JsonProperty("district")]
        public string District { get; set; }
        [JsonProperty("locality-name")]
        public string LocalityName { get; set; }
        public string SubLocalityName { get; set; }
        [JsonProperty("address")]
        public string Address { get; set; }
        public string Direction { get; set; }
        [JsonProperty("distance")]
        public decimal? Distance { get; set; }
        [JsonProperty("coord")]
        public GeoCoordinate Coordinate { get; set; }
        public string MetroName { get; set; }
        public decimal? MetroTimeOnTransport { get; set; }
        public decimal? MetroTimeOnFoot { get; set; }
        public string RailwayStation { get; set; }
        public string VillageName { get; set; }
        public string IdYandexVillage { get; set; }
        [JsonProperty("id-district")]
        public int? IdDistrict { get; set; }
        [JsonProperty("id-micro-district")]
        public int? IdMicroDistrict { get; set; }

        [JsonProperty("orientir")]
        public string Orientir { get; set; }

        [JsonProperty("metro")]
        public Metro Metro { get; set; }
        /// <summary>
        /// ЖК "синявки"
        /// </summary>
        [JsonProperty("subdistrict-name")]
        public string SubDistrictName { get; set; }


        [JsonProperty("microdistrict-name")]
        public string MicroDistrictName =>
            IdMicroDistrict.HasValue ? RegionsManager.GetMicroDistrict(IdMicroDistrict.Value) : null;
        [JsonProperty("district-name")]
        public string DistrictName =>
            IdDistrict.HasValue ? RegionsManager.GetDistrict(IdDistrict.Value) : null;
        public DateTime CreateDt { get; set; }
        public DateTime? DeleteDt { get; set; }

        public Location(building_location location)
        {
            CreateDt = location?.create_dt ?? DateTime.Now;
            if (location == null)
                return;
            DeleteDt = location.delete_dt;
            Orientir = location.orientir;
            Id = location.id_building_location;
            Country = location.country;
            Region = location.region;
            District = location.district;
            LocalityName = location.locality_name;
            SubLocalityName = location.sub_locality_name;
            Address = location.address;
            Direction = location.direction;
            Distance = location.distance;
            if(location.latitude.HasValue && location.longitude.HasValue)
                Coordinate = new GeoCoordinate((double)location.latitude.Value, (double)location.longitude.Value);
            if (location.building_location_metro.Any())
            {
                // сгенерировать объекты METRO
            }
            RailwayStation = location.railway_station;
            VillageName = location.village_name;
            IdYandexVillage = location.id_yandex_village;
            IdDistrict = location.id_district;
            IdMicroDistrict = location.id_micro_district;
            SubDistrictName = location.microdistrict_name;
        }

        public building_location ToDb()
        {
            var loc = new building_location
            {
                country = Country,
                region = Region,
                district = District,
                locality_name = LocalityName,
                sub_locality_name = SubLocalityName,
                address = Address,
                direction = Direction,
                distance = Distance,
                latitude = Coordinate?.Lat,
                longitude = Coordinate?.Lon,
                railway_station = RailwayStation,
                village_name = VillageName,
                id_yandex_village = IdYandexVillage,
                id_district = IdDistrict,
                id_micro_district = IdMicroDistrict,
                microdistrict_name = SubDistrictName,
                create_dt = CreateDt,
                delete_dt = DeleteDt,
                orientir = Orientir
            };
            if (Id.HasValue)
                loc.id_building_location = Id.Value;
            if (Metro != null)
            {
                //loc.building_location_metro = new List<building_location_metro>{Metro.ToDb()};
            }
            return loc;
        }
    }
}

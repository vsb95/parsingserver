﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreMls.DB.Model;
using CoreMls.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CoreMls.DB.LocalEntities
{
    [JsonObject(Id = "building", ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Building
    {
        [JsonIgnore] public long? Id { get; set; }
        [JsonProperty("name")] public string BuildingName { get; set; }

        [JsonProperty("material-id")]
        public string BuildingMaterialTypeId
        {
            get
            {
                switch (BuildingMaterialType)
                {
                    case BuildingMaterialType.Кирпич: return "brick";
                    case BuildingMaterialType.Железобетонный: return "concrete";
                    case BuildingMaterialType.Металлический: return "metall";
                    case BuildingMaterialType.Блочный: return "block";
                    case BuildingMaterialType.Деревянный: return "wood";
                    case BuildingMaterialType.КирпичноМонолитный: return "brick-monolite";
                    case BuildingMaterialType.Монолит: return "monolite";
                    case BuildingMaterialType.Панельный: return "panel";
                    case BuildingMaterialType.КаркасноНасыпной: return "skeleton";
                    case BuildingMaterialType.Default: return null;
                    default: return BuildingMaterialType.ToString();
                }
            }
        }

        [JsonProperty("material")]
        [JsonConverter(typeof(StringEnumConverter))]
        public BuildingMaterialType BuildingMaterialType { get; set; }

        [JsonProperty("parking-type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ParkingType? ParkingType { get; set; }

        [JsonProperty("location")] public Location Location { get; set; }

        [JsonProperty("built-year")] public long? BuiltYear { get; set; }
        [JsonProperty("series")] public string BuildingSeries { get; set; }

        /// <summary>
        /// Корпус
        /// </summary>
        [JsonProperty("section")]
        public string BuildingSection { get; set; }

        /// <summary>
        /// Застройщик
        /// </summary>
        [JsonProperty("builder")]
        public string BuilderName { get; set; }

        [JsonProperty("is-has-lift")] public bool? Lift { get; set; }
        [JsonProperty("is-has-trash-tube")] public bool? TrashTube { get; set; }
        [JsonProperty("floors-total")] public long? FloorsTotal { get; set; }

        [JsonProperty("count-passenger-lift")] public int? CountPassengerElevator { get; set; }
        [JsonProperty("count-weight-lift")] public int? CountWeightElevator { get; set; }

        [JsonProperty("ready-quarter")] public long? ReadyQuarter { get; set; }
        public long? IdBuildingPhase { get; set; }

        [JsonProperty("building-state")]
        [JsonConverter(typeof(StringEnumConverter))]
        public BuildingState? BuildingState { get; set; }

        public long? IdCommercialBuildingType { get; set; }
        public long? IdCommercialBuildingPurpose { get; set; }
        public long? IdOfficeClass { get; set; }
        public bool? GuardedBuilding { get; set; }
        public bool? Parking { get; set; }
        public long? ParkingPlaces { get; set; }
        public long? ParkingPlacePrice { get; set; }
        public bool? ParkingGuest { get; set; }
        public long? ParkingGuestPlaces { get; set; }
        public bool? Alarm { get; set; }
        public bool? Security { get; set; }
        public bool? IsElite { get; set; }
        public bool? TwentyFourSeven { get; set; }
        public bool? EatingFacilities { get; set; }
        public long? YandexBuildingId { get; set; }
        public string YandexHouseId { get; set; }

        [JsonIgnore] public DateTime CreateDt { get; set; }
        [JsonIgnore] public DateTime? DeleteDt { get; set; }

        public Building(building building)
        {
            Id = building.id_building;
            Location = new Location(building.building_location);
            if (building.id_building_type.HasValue)
                BuildingMaterialType = (BuildingMaterialType) building.id_building_type.Value;
            if (building.id_parking_type.HasValue)
                ParkingType = (ParkingType) building.id_parking_type.Value;
            BuildingName = building.building_name;
            if (building.id_building_state.HasValue)
                BuildingState = (BuildingState) building.id_building_state.Value;
            IdCommercialBuildingType = building.id_building_commercial_type;
            IdCommercialBuildingPurpose = building.id_commercial_building_purpose;
            IdOfficeClass = building.id_office_class;
            BuiltYear = building.built_year;
            ReadyQuarter = building.id_ready_quarter;
            IdBuildingPhase = building.id_building_phase;
            BuildingSeries = building.building_series;
            BuildingSection = building.building_section;
            Lift = building.lift;
            CountPassengerElevator = building.count_passenger_lift;
            CountWeightElevator = building.count_weight_lift;
            TrashTube = building.is_has_trash_tube;
            FloorsTotal = building.floors_total;
            GuardedBuilding = building.guarded_building;
            Parking = building.parking;
            ParkingPlaces = building.parking_places;
            ParkingPlacePrice = building.parking_place_price;
            ParkingGuest = building.parking_guest;
            ParkingGuestPlaces = building.parking_guest_places;
            Alarm = building.alarm;
            Security = building.security;
            IsElite = building.is_elite;
            TwentyFourSeven = building.twenty_four_seven;
            EatingFacilities = building.eating_facilities;
            YandexBuildingId = building.yandex_building_id;
            YandexHouseId = building.yandex_house_id;
            CreateDt = building.create_dt ?? DateTime.Now;
            DeleteDt = building.delete_dt;

            BuilderName = building.builder?.name;
        }

        public building ToDb()
        {
            var building = new building
            {
                building_name = BuildingName,
                id_building_type = (long?) BuildingMaterialType,
                building_location = Location.ToDb(),
                id_building_state = (long?) BuildingState,
                id_building_commercial_type = IdCommercialBuildingType,
                id_commercial_building_purpose = IdCommercialBuildingPurpose,
                id_office_class = IdOfficeClass,
                built_year = BuiltYear,
                id_ready_quarter = ReadyQuarter,
                id_building_phase = IdBuildingPhase,
                building_series = BuildingSeries,
                building_section = BuildingSection,
                lift = Lift,
                count_passenger_lift = CountPassengerElevator,
                count_weight_lift = CountWeightElevator,
                floors_total = FloorsTotal,
                guarded_building = GuardedBuilding,
                parking = Parking,
                id_parking_type = (long?) ParkingType,
                parking_places = ParkingPlaces,
                parking_place_price = ParkingPlacePrice,
                parking_guest = ParkingGuest,
                parking_guest_places = ParkingGuestPlaces,
                alarm = Alarm,
                security = Security,
                is_elite = IsElite,
                twenty_four_seven = TwentyFourSeven,
                eating_facilities = EatingFacilities,
                yandex_building_id = YandexBuildingId,
                yandex_house_id = YandexHouseId,
                create_dt = CreateDt,
                delete_dt = DeleteDt
            };

            if (Id.HasValue)
                building.id_building = Id.Value;
            return building;
        }
    }
}

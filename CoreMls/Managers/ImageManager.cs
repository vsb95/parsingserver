using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.Extension;
using Core.Services;
using CoreMls.DB.LocalEntities;
using CoreMls.DB.Managers;
using CoreMls.DB.Model;
using CoreMls.Enum;
using Logger;

namespace CoreMls.Managers
{
    public static class ImageManager
    {
        /// <summary>
        /// При добавлении объявления перетащит фотки из tmp в нужную дирректорию и переименует
        /// </summary>
        /// <param name="adv"></param>
        internal static bool MoveImages(MlsAdvertisment adv)
        {
            if (adv?.Id == null)
                return false;
            string truePath = null;
            try
            {
                if (string.IsNullOrEmpty(SettingsManager.Settings.ServerSettings.MlsImageFolder)
                    || string.IsNullOrEmpty(SettingsManager.Settings.ServerSettings.MlsImageTmpFolder))
                    throw new Exception("не указана папка для фото");
                if (!adv.Category.HasValue || !adv.Type.HasValue ||
                    (!adv.Property.PropertyLivingCategory.HasValue && !adv.Property.CommercialType.HasValue))
                    throw new Exception("не прошли валидацию");
                var destinationFolder = Path.Combine(SettingsManager.Settings.ServerSettings.MlsImageFolder,
                    adv.Type.Value.ToString(), adv.Category.Value.ToString(),
                    adv.Property.PropertyLivingCategory?.ToString() ?? adv.Property.CommercialType.Value.ToString(),
                    adv.Id.Value.ToString());

                if (!Directory.Exists(destinationFolder))
                    Directory.CreateDirectory(destinationFolder);

                var newImageList = new List<string>();
                foreach (var image in adv.Property.Images)
                {
                    // Это фото у нас уже есть на нужном месте
                    if (image.StartsWith(SettingsManager.Settings.ServerSettings.Domain)
                        && FileExist(image, out truePath)
                        && !image.ToLower().Contains("tmp"))
                    {
                        if (!newImageList.Contains(image))
                            newImageList.Add(image);
                        continue;
                    }

                    if (image.StartsWith("http:") || image.StartsWith("https:"))
                    {
                        if (!newImageList.Contains(image))
                            newImageList.Add(image);
                        continue;
                    }

                    var oldPath = Path.Combine(SettingsManager.Settings.ServerSettings.MlsImageTmpFolder,
                        image.Replace(SettingsManager.Settings.ServerSettings.Domain, "").TrimStart('/'));
                    if (!FileExist(image, out truePath))
                        continue;
                    var newImagePath = GenerateNewName(destinationFolder, truePath);
                    File.Move(truePath, newImagePath);
                    // Вырезаем локальный путь
                    newImagePath = LocalToGlobalPath(newImagePath);
                    //
                    newImageList.Add(newImagePath);
                }

                adv.Property.Images = newImageList;

                return true;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "[CoreMls.ImageManager::MoveImages] truePath = " + truePath);
            }

            return false;
        }

        public static void SetWaterMarkIfNeeded(long advId)
        {
            string truePath = null;
            try
            {
                using (var context = new realtyMLSEntities())
                {
                    var offer = DbOfferManager.GetOfferById(advId, context);
                    var watermark = offer?.sales_agent?.sales_agent_organization1?.watermark_image;
                    if (string.IsNullOrEmpty(watermark))
                        return;
                    if (!FileExist(watermark, out var watermarkPath))
                        throw new Exception("Не найден водяной знак: " + watermark);
                    foreach (var image in offer.property.property_image)
                    {
                        if (image.is_watermarked) continue;
                        // Если изображение не на нашем сервере
                        if(!image.image_url.StartsWith(SettingsManager.Settings.ServerSettings.Domain) && image.image_url.StartsWith("http"))
                            continue;
                        // Это фото у нас уже есть на нужном месте
                        if (!image.image_url.StartsWith(SettingsManager.Settings.ServerSettings.Domain) ||
                            !FileExist(image.image_url, out truePath) || image.image_url.ToLower().Contains("tmp"))
                        {
                            Log.Error("не удалось найти изображение чтобы поставить ватермарку: " + image, true);
                            continue;
                        }

                        using (var img = Image.FromFile(truePath))
                        {
                            if (!img.TryAddWatermarkImage(watermarkPath))
                            {
                                Log.Error(
                                    "не удалось поставить ватермарку: \nImagePath: " + truePath + "\nWatermarkPath: " +
                                    watermarkPath, true);
                                continue;
                            }

                            var newName = GenerateNewName(null, truePath);
                            img.Save(newName);
                            image.image_url = LocalToGlobalPath(newName);
                        }

                        File.Delete(truePath);

                        image.is_watermarked = true;
                        context.property_image.AddOrUpdate(image);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true,
                    "[CoreMls.ImageManager::SetWaterMarkIfNeeded] truePath = " + truePath);
            }
        }

        private static string GenerateNewName(string directory, string oldPath)
        {
            var oldFile = new FileInfo(oldPath);
            if (string.IsNullOrEmpty(directory))
            {
                directory = oldFile.DirectoryName;
            }

            var newName = Path.Combine(directory, Randomizer.CreateRandomString(10) + oldFile.Extension);
            while (File.Exists(newName))
            {
                newName = Path.Combine(directory, Randomizer.CreateRandomString(10) + oldFile.Extension);
            }

            return newName;
        }

        /// <summary>
        /// Пробует найти изображение. Если путь указан к домену - разберется и все равно найдет
        /// </summary>
        /// <param name="path"></param>
        /// <param name="truePath"></param>
        /// <returns></returns>
        private static bool FileExist(string path, out string truePath)
        {
            truePath = null;
            if (string.IsNullOrEmpty(path))
                return false;
            try
            {
                path = path.Replace(SettingsManager.Settings.ServerSettings.Domain, "").TrimStart('/');
                // ищем во временной папке если решили не добавлять уже при загрузке
                var path2 = Path.Combine(SettingsManager.Settings.ServerSettings.MlsImageTmpFolder, path);
                if (!File.Exists(path2))
                {
                    // ищем в уже загруженной папке
                    var indexOfSetting =
                        SettingsManager.Settings.ServerSettings.MlsImageFolder.LastIndexOf("assets",
                            StringComparison.Ordinal);
                    var indexOfFile = path.IndexOf("assets", StringComparison.Ordinal);
                    if (indexOfFile != -1 && indexOfSetting != -1)
                    {
                        path2 = Path.Combine(
                            SettingsManager.Settings.ServerSettings.MlsImageFolder.Substring(0, indexOfSetting),
                            path.Substring(indexOfFile));
                    }
                }

                truePath = path2.Replace("\\", "/");
                return File.Exists(truePath);
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "ImageManager::DeleteFileIfExist");
            }

            return false;
        }

        /// <summary>
        /// Вырежет домен и если найдет изображение - удалит
        /// </summary>
        /// <param name="badimg"></param>
        public static void DeleteFileIfExist(string badimg)
        {
            if (FileExist(badimg, out var truePath))
                File.Delete(truePath);
        }

        /// <summary>
        /// Преобразовать путь к изображению с локального (D:\images...) в глобальный (http://...)
        /// </summary>
        /// <param name="localPath"></param>
        /// <returns></returns>
        public static string LocalToGlobalPath(string localPath)
        {
            var indexOf = localPath.IndexOf("assets", StringComparison.Ordinal);
            if (indexOf != -1)
            {
                localPath = localPath.Substring(indexOf);
                localPath = Path.Combine(SettingsManager.Settings.ServerSettings.Domain, localPath).Replace("\\", "/");
            }
            else
            {
                Log.Error("Странный путь к изображению: " + localPath, true);
            }

            return localPath;
        }

        /// <summary>
        /// Преобразовать путь к изображению с глобального (http://...) в локальный (D:\images...)
        /// </summary>
        /// <param name="globalPath"></param>
        /// <returns></returns>
        public static string GlobalToLocalPath(string globalPath)
        {
            return FileExist(globalPath, out var truePath) ? truePath : null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Core;
using Core.Exceptions;
using Core.Extension;
using Core.Storage;
using CoreMls.DB.Managers;
using CoreMls.DB.Model;
using CoreMls.Enum;
using CoreMls.Enum.Commercial;
using CoreMls.Interfaces;
using CoreMls.Validators;
using Logger;

namespace CoreMls.Managers.FeedParsers
{
    public class YandexFeedParser : IFeedParser
    {
        private string currentInternalIdStr;
        private string _fileUrl;
        private bool isHasGarage = false;

        public bool TryParse(string fileUrl, long idOrg, out FeedImportResult result)
        {
            _fileUrl = fileUrl;
            result = new FeedImportResult();
            try
            {
                offer offer = null;
                var prevElemName = "";
                var elemName = "";
                var organization = DbOrganizationManager.GetOrganizationById(idOrg);
                if (organization?.Id == null)
                {
                    result.Errors.Add("Указана не верная организация");
                    return false;
                }

                XmlTextReader reader = new XmlTextReader(fileUrl);
                var currentInternalIdStr = "";
                while (reader.Read())
                {
                    try
                    {
                        var nodeType = reader.NodeType;
                        if (offer == null && (nodeType != XmlNodeType.Element && reader.Name == "offer" ||
                                              reader.Name != "offer"))
                            continue;
                        switch (nodeType)
                        {
                            case XmlNodeType.Element:
                            {
                                if (reader.NamespaceURI != "http://webmaster.yandex.ru/schemas/feed/realty/2010-06")
                                    throw new ValidationException("Не верный фид", ValidationErrorType.Fatal);
                                elemName = reader.Name;
                                if (string.IsNullOrEmpty(prevElemName))
                                    prevElemName = reader.Name;

                                switch (elemName)
                                {
                                    case "offer":
                                    {
                                        prevElemName = elemName;
                                        if (!reader.HasAttributes)
                                            throw new ValidationException("Не указан internal-id",
                                                ValidationErrorType.Fatal);
                                        offer = new offer
                                        {
                                            id_commission_type = (long) CommissionType.Null,
                                            property = new property
                                            {
                                                building = new building
                                                {
                                                    building_location = new building_location()
                                                }
                                            },
                                            offer_feed_site = new List<offer_feed_site>
                                            {
                                                new offer_feed_site
                                                {
                                                    id_feed_site = (long) FeedSite.Yandex,
                                                    is_enabled_autoload = true
                                                },
                                                new offer_feed_site
                                                {
                                                    id_feed_site = (long) FeedSite.Avito,
                                                    is_enabled_autoload = true
                                                }
                                            },
                                            sales_agent = new sales_agent
                                            {
                                                id_sales_agent_organization = organization.Id.Value,
                                                id_sales_agent_category = 2,
                                                sales_agent_organization1 = null
                                            }
                                        };
                                        for (int i = 0; i < reader.AttributeCount; i++)
                                        {
                                            reader.MoveToAttribute(i);
                                            if (reader.Name != "internal-id")
                                                throw new ValidationException("Не указан internal-id",
                                                    ValidationErrorType.Fatal);
                                            currentInternalIdStr = reader.Value;
                                            var internalId = ClearInternalId(currentInternalIdStr);
                                            if (internalId <= 0)
                                                throw new ValidationException(
                                                    "Internal-id должен быть целым числом, а указано: '" +
                                                    currentInternalIdStr + "'", ValidationErrorType.Fatal);
                                            offer.feed_internal_id = internalId;

                                            DbOfferManager.SetIdOfferByInnerId(internalId, organization.Id.Value,
                                                offer);
                                        }

                                        break;
                                    }

                                    case "location":
                                    case "sales-agent":
                                    case "area":
                                    case "lot-area":
                                    case "living-space":
                                    case "kitchen-space":
                                    case "price":
                                        prevElemName = elemName;
                                        break;

                                    case "":
                                    case "realty-feed":
                                    case "generation-date":
                                        continue;
                                    default:
                                        //Log.Trace(reader.LineNumber + ":\t" + reader.NodeType +" <"+ elemName + "> ");
                                        break;
                                }

                                break;
                            }

                            case XmlNodeType.EndElement:
                            {
                                if (reader.Name == "offer")
                                {
                                    if (offer == null)
                                        throw new ValidationException(
                                            "Закрывающий тэг </offer> найден раньше открывающегося",
                                            ValidationErrorType.Fatal);

                                    // Если при валидации вылетит исключение - нам нужно чтобы offer был занулен, тк это конечный тэг
                                    var _offer = offer;
                                    offer = null;

                                    var validator = OfferValidatorFactory.Create(_offer);
                                    if (validator == null)
                                    {
                                        Log.Fatal("не создан валидатор для offer'a: " + _offer.feed_internal_id +
                                                  "\nXML: " + fileUrl);
                                        result.Offers.Add(_offer);
                                        continue;
                                    }

                                    //
                                    //Если агент не указан при импорте - мы сгенерируем фиктивный по названию организации
                                    //
                                    if (string.IsNullOrEmpty(_offer.sales_agent.sales_agent_name))
                                        _offer.sales_agent.sales_agent_name =
                                            _offer.sales_agent.sales_agent_organization;
                                    //
                                    if (validator.IsValid(_offer))
                                    {
                                        var salesAgent = DbSalesAgentManager.GetAgentByName(
                                            _offer.sales_agent.sales_agent_name,
                                            organization.Id.Value, _offer.sales_agent.sales_agent_email,
                                            _offer.sales_agent.sales_agent_photo);
                                        if (salesAgent != null)
                                        {
                                            var salesAgentId = salesAgent.Id.Value;
                                            if (salesAgent.IsVirtual)
                                            {
                                                var realAgent =
                                                    DbSalesAgentManager.GetRealAgentByVirtualAgentId(salesAgent.Id
                                                        .Value);
                                                if (realAgent != null)
                                                {
                                                    salesAgentId = realAgent.Id.Value;
                                                }
                                            }

                                            _offer.sales_agent = null;
                                            _offer.id_sales_agent = salesAgentId;
                                        }
                                        else
                                        {
                                            var id = DbSalesAgentManager.AddOrUpdate(_offer.sales_agent);
                                            if (id != -1)
                                            {
                                                _offer.sales_agent = null;
                                                _offer.id_sales_agent = id;
                                            }
                                        }

                                        if (_offer.property.building.building_location.latitude.HasValue &&
                                            _offer.property.building.building_location.longitude.HasValue)
                                        {
                                            var geocoord = new GeoCoordinate(
                                                _offer.property.building.building_location.latitude.Value,
                                                _offer.property.building.building_location.longitude.Value);
                                            if (RegionsManager.DistrictManager.TryIdentifyDistrictAndMicroDistrict(
                                                geocoord, out var district,
                                                out var microDistrict, out var isNeedReverse))
                                            {
                                                _offer.property.building.building_location.latitude =
                                                    isNeedReverse ? geocoord.Lon : geocoord.Lat;
                                                _offer.property.building.building_location.longitude =
                                                    isNeedReverse ? geocoord.Lat : geocoord.Lon;
                                                _offer.property.building.building_location.id_district = district?.Id;
                                                _offer.property.building.building_location.id_micro_district =
                                                    microDistrict?.Id;
                                                if (microDistrict == null)
                                                    Log.Error(
                                                        "Не удалось распознать мкр при добалении фида: '" +
                                                        _offer.property.building.building_location.address + "' " +
                                                        geocoord, true);
                                            }
                                        }

                                        result.Offers.Add(_offer);
                                    }
                                }

                                if (reader.Name == prevElemName)
                                    prevElemName = null;
                                break;
                            }

                            case XmlNodeType.Text:
                                Log.Trace(reader.LineNumber + ":\t\t" + reader.NodeType + " <" + prevElemName + "->" +
                                          elemName + "> = " + reader.Value);
                                if (!TryParseTag(offer, prevElemName, elemName, reader.Value))
                                {
                                    //Log.Trace(reader.LineNumber + ":\t\tIGNORED " + reader.NodeType + " = " +currentValue);
                                    Log.Error("Строка " + reader.LineNumber + ": " +
                                              (offer?.feed_internal_id != null
                                                  ? "internal-id=\"" + currentInternalIdStr + "\" "
                                                  : "") + "IGNORED: <" + prevElemName + "->" + elemName + "> = " +
                                              reader.Value);
                                }

                                break;
                            case XmlNodeType.XmlDeclaration:
                            case XmlNodeType.Whitespace:
                                break;
                            default:
                                if (!string.IsNullOrEmpty(reader.Value) || !string.IsNullOrEmpty(reader.Name))
                                    Log.Trace(reader.LineNumber + ":\t" + reader.NodeType + " = <" + reader.Name +
                                              "> " + reader.Value);
                                break;
                        }
                    }
                    catch (ValidationException vex)
                    {
                        var msg = "[" + vex.ErrorType + "] Строка " + reader.LineNumber + ": " +
                                  (offer?.feed_internal_id != null
                                      ? "internal-id=\"" + currentInternalIdStr + "\" "
                                      : "") + vex.Message;
                        Log.Debug(msg);
                        result.Errors.Add(msg);
                        switch (vex.ErrorType)
                        {
                            case ValidationErrorType.Error:
                                offer = null;
                                break;
                            case ValidationErrorType.Fatal:
                                return false;
                        }
                    }
                }

                if (isHasGarage)
                    result.Errors.Add(
                        "Мы пока не работаем с гаражами. Поэтому объекты из этой категории не были добавлены");
                return true;
            }
            catch (XmlException xe)
            {
                result.Errors.Add(
                    $"Фатальная ошибка при парсинге файла: {xe}\nСтрока: {xe.LineNumber}; Позиция: {xe.LinePosition}");
                Log.Exception(xe, LogType.Error, true, "[FeedManager::TryParseYandex]");
            }
            catch (Exception ex)
            {
                result.Errors.Add("Фатальная внутренняя ошибка. Обратитесь в техподдержку");
                Log.Exception(ex, LogType.Fatal, true, "[FeedManager::TryParseYandex]");
            }

            return false;
        }

        private static bool AsBool(string value)
        {
            switch (value.ToLower())
            {
                case "+":
                case "да":
                case "true":
                case "1":
                    return true;

                case "-":
                case "нет":
                case "false":
                case "0":
                    return false;
            }

            throw new ValidationException("Не верно указано булевое значение", ValidationErrorType.Fatal);
        }

        private static Unit GetUnit(string value)
        {
            switch (value.ToLower())
            {
                case "га":
                case "гектар":
                case "hectare":
                    return Unit.Hectare;
                case "сот":
                case "cот":
                case "cотка":
                case "сотка":
                    return Unit.OneHundred;
                case "кв.м":
                case "sq.m":
                case "кв. м":
                case "sq. m":
                    return Unit.SqMeter;
            }

            throw new ValidationException("Не верно указан тип площади: '" + value + "'", ValidationErrorType.Error);
        }

        /// <summary>
        /// Если в id есть буквы или символы, то они преобразуются в число
        /// </summary>
        /// <param name="internalId"></param>
        /// <returns></returns>
        private static long ClearInternalId(string internalId)
        {
            long result = 0;
            if (string.IsNullOrEmpty(internalId))
                return 0;
            if (long.TryParse(internalId, out result))
                return result;
            var chunks = internalId.FindDigits(true);
            var newInternalId = internalId;
            foreach (var chunk in chunks)
            {
                var charArray = chunk.ToCharArray();
                if (char.IsDigit(charArray[0]))
                    continue;
                foreach (var ch in charArray)
                {
                    var intView = "00000" + (int) ch;
                    newInternalId = newInternalId.Replace(ch.ToString(), intView);
                }
            }

            if (long.TryParse(newInternalId, out result))
                return result;
            return 0;
        }

        private bool TryParseTag(offer offer, string prevElemName, string elemName, string currentValue)
        {
            switch (prevElemName)
            {
                case "location":
                {
                    switch (elemName)
                    {
                        case "country":
                        {
                            offer.property.building.building_location.country = currentValue;
                            break;
                        }

                        case "region":
                        {
                            offer.property.building.building_location.region = currentValue;
                            break;
                        }

                        case "district":
                        {
                            offer.property.building.building_location.district = currentValue;
                            break;
                        }

                        case "locality-name":
                        {
                            offer.property.building.building_location.locality_name = currentValue;
                            break;
                        }

                        case "sub-locality-name":
                        {
                            offer.property.building.building_location.sub_locality_name = currentValue;
                            break;
                        }

                        case "address":
                        {
                            offer.property.building.building_location.address = currentValue;
                            break;
                        }

                        case "latitude":
                        {
                            if (!double.TryParse(currentValue.Replace(".", ","), out var lat))
                                throw new ValidationException("не верно указана широта");
                            offer.property.building.building_location.latitude = lat;
                            break;
                        }

                        case "longitude":
                        {
                            if (!double.TryParse(currentValue.Replace(".", ","), out var lon))
                                throw new ValidationException("не верно указана долгота");
                            offer.property.building.building_location.longitude = lon;
                            break;
                        }

                        case "distance":
                        {
                            if (!decimal.TryParse(currentValue.Replace(".", ","), out var distance))
                                throw new ValidationException("не верно указана удаленность",
                                    ValidationErrorType.Warning);
                            offer.property.building.building_location.distance = distance;
                            break;
                        }

                        case "non-admin-sub-locality": break; // хз вообще откуда
                        default:
                            throw new ValidationException("Неизвестный аргумент: " + elemName);
                    }

                    break;
                }

                case "sales-agent":
                {
                    switch (elemName)
                    {
                        case "category":
                            break;
                        case "organization":
                        {
                            offer.sales_agent.sales_agent_organization = currentValue;
                            break;
                        }

                        case "name":
                        {
                            offer.sales_agent.sales_agent_name = currentValue;
                            break;
                        }

                        case "phone":
                        {
                            var phone = DbSalesAgentManager.GetSalesAgentPhone(currentValue, 0);
                            offer.sales_agent.sales_agent_phone.Add(phone);
                            break;
                        }

                        case "email":
                        {
                            offer.sales_agent.sales_agent_email = currentValue;
                            break;
                        }

                        case "photo":
                        {
                            offer.sales_agent.sales_agent_photo = currentValue;
                            break;
                        }

                        case "url":
                        {
                            offer.sales_agent.sales_agent_url = currentValue;
                            break;
                        }

                        default:
                        {
#if DEBUG
                            //throw new ValidationException("Неизвестный тэг: " + elemName, ValidationErrorType.Warning);
#endif
                            break;
                        }
                    }

                    break;
                }

                case "price":
                {
                    switch (elemName)
                    {
                        case "value":
                        {
                            if (!decimal.TryParse(currentValue.Replace(".", ","), out var price))
                                throw new ValidationException("Не верно указана стоимость", ValidationErrorType.Error);
                            offer.price_value = price;
                            break;
                        }

                        case "currency":
                        {
                            if (currentValue != "RUB" && currentValue != "RUR")
                                throw new ValidationException(
                                    "Не верно указана стоимость: работаем только с рублями", ValidationErrorType.Error);
                            break;
                        }

                        case "period":
                        {
                            switch (currentValue.ToLower())
                            {
                                case "день":
                                case "day":
                                    throw new ValidationException(
                                        "Мы пока не работаем с квартирами с посуточной арендой",
                                        ValidationErrorType.Error);
                                    offer.price_id_period = (long) PricePeriod.Day;
                                    break;
                                case "месяц":
                                case "month":
                                    offer.price_id_period = (long) PricePeriod.Month;
                                    break;
                                default:
                                    throw new ValidationException(
                                        "Не верный срок оплаты: " + currentValue, ValidationErrorType.Error);
                            }

                            break;
                        }

                        default:
                            throw new ValidationException("Неизвестный тэг: " + elemName);
                    }

                    break;
                }

                case "area":
                {
                    switch (elemName)
                    {
                        case "value":
                        {
                            if (!decimal.TryParse(currentValue.Replace(".", ","), out var area))
                                throw new ValidationException("Не верно указана площадь", ValidationErrorType.Error);
                            offer.property.area_value = area;
                            break;
                        }

                        case "unit":
                        {
                            offer.property.area_id_unit = (long) GetUnit(currentValue);
                            break;
                        }

                        default:
                            throw new ValidationException("Неизвестный тэг: " + elemName);
                    }

                    break;
                }

                case "lot-area":
                {
                    switch (elemName)
                    {
                        case "value":
                        {
                            if (!decimal.TryParse(currentValue.Replace(".", ","), out var area))
                                throw new ValidationException("Не верно указана площадь");
                            offer.property.lot_area_value = area;
                            break;
                        }

                        case "unit":
                        {
                            offer.property.lot_area_id_unit = (long) GetUnit(currentValue);
                            break;
                        }

                        default:
                            throw new ValidationException("Неизвестный тэг: " + elemName);
                    }

                    break;
                }

                case "living-space":
                {
                    switch (elemName)
                    {
                        case "value":
                        {
                            if (!decimal.TryParse(currentValue.Replace(".", ","), out var area))
                                throw new ValidationException("Не верно указана площадь");
                            offer.property.living_space_value = area;
                            break;
                        }

                        case "unit":
                        {
                            offer.property.living_space_id_unit = (long) GetUnit(currentValue);
                            break;
                        }

                        default:
                            throw new ValidationException("Неизвестный тэг: " + elemName);
                    }

                    break;
                }

                case "kitchen-space":
                {
                    switch (elemName)
                    {
                        case "value":
                        {
                            if (!decimal.TryParse(currentValue.Replace(".", ","), out var area))
                                throw new ValidationException("Не верно указана площадь");
                            offer.property.kitchen_space_value = area;
                            break;
                        }

                        case "unit":
                        {
                            offer.property.kitchen_space_id_unit = (long) GetUnit(currentValue);
                            break;
                        }

                        default:
                            throw new ValidationException("Неизвестный тэг: " + elemName);
                    }

                    break;
                }

                case "room-space":
                {
                    switch (elemName)
                    {
                        case "value":
                        {
                            if (!decimal.TryParse(currentValue.Replace(".", ","), out var area))
                                throw new ValidationException("Не верно указана площадь");
                            offer.property.room_space_value = area;
                            break;
                        }

                        case "unit":
                        {
                            offer.property.room_space_id_unit = (long) GetUnit(currentValue);
                            break;
                        }

                        default:
                            throw new ValidationException("Неизвестный тэг: " + elemName);
                    }

                    break;
                }

                default:
                    switch (elemName)
                    {
                        case "generationdate":
                        case "generation-date":
                            break;

                        #region offer

                        case "type":
                        {
                            switch (currentValue.ToLower())
                            {
                                case "продажа":
                                    offer.id_offer_type = (long) OfferType.Sell;
                                    break;
                                case "аренда":
                                    offer.id_offer_type = (long) OfferType.Rent;
                                    break;
                                default:
                                    throw new ValidationException(
                                        "Не верно указан тип объявления: " + currentValue, ValidationErrorType.Error);
                            }

                            break;
                        }

                        case "property-type":
                        {
                            switch (currentValue.ToLower())
                            {
                                case "жилая":
                                case "living":
                                    offer.id_offer_yrl_category = (long) OfferCategory.Living;
                                    offer.property.id_property_type = (long) PropertyType.Living;
                                    break;
                                case "коммерческая":
                                case "commercial":
                                    offer.id_offer_yrl_category = (long) OfferCategory.Commercial;
                                    offer.property.id_property_type = (long) PropertyType.Commercial;
                                    break;
                                default:
                                    throw new ValidationException("Не верно указан тип объявления: " + currentValue,
                                        ValidationErrorType.Error);
                            }

                            break;
                        }

                        case "creation-date":
                        {
                            if (!DateTime.TryParse(currentValue, out var createDt))
                                throw new ValidationException("не верно указана дата", ValidationErrorType.Error);
                            offer.creation_date = createDt;
                            offer.create_dt = createDt;
                            break;
                        }

                        case "last-update-date":
                        {
                            if (!DateTime.TryParse(currentValue, out var lastUpdateDt))
                                throw new ValidationException("не верно указана дата", ValidationErrorType.Error);
                            offer.last_update_date = lastUpdateDt;
                            break;
                        }

                        case "mortgage":
                        {
                            offer.mortgage = AsBool(currentValue);
                            break;
                        }

                        case "haggle":
                        {
                            offer.haggle = AsBool(currentValue);
                            break;
                        }

                        case "prepayment":
                        {
                            if (!int.TryParse(currentValue, out var prepayMonth))
                                throw new ValidationException("не верно указано кол-во месяцев предоплаты",
                                    ValidationErrorType.Error);
                            offer.prepayment = prepayMonth;
                            break;
                        }

                        case "rent-pledge":
                        {
                            offer.rent_pledge = AsBool(currentValue);
                            break;
                        }

                        case "url":
                            offer.offer_url = currentValue;
                            break;
                        case "deal-status":
                        {
                            switch (currentValue.ToLower())
                            {
                                case "sale":
                                case "прямая продажа":
                                    offer.id_offer_status = (long) OfferStatus.DirectSale;
                                    break;
                                case "первичная продажа вторички":
                                case "primary sale of secondary":
                                    offer.id_offer_status = (long) OfferStatus.FirstSecondSale;
                                    break;
                                case "встречная продажа":
                                case "countersale":
                                    offer.id_offer_status = (long) OfferStatus.CounterSale;
                                    break;
                                case "первичная продажа":
                                case "продажа от застройщика":
                                    offer.id_offer_status = (long) OfferStatus.FirstSell;
                                    break;
                                case "переуступка":
                                case "reassignment":
                                    offer.id_offer_status = (long) OfferStatus.ReAssignment;
                                    break;
                                case "прямая аренда":
                                    offer.id_offer_status = (long) OfferStatus.DirectRent;
                                    break;
                                default:
                                    throw new ValidationException(
                                        "Не верно указан статус сделки: " + currentValue, ValidationErrorType.Error);
                            }

                            break;
                        }

                        case "agent-fee":
                        {
                            if (!decimal.TryParse(currentValue.Replace(".", ","), out var fee))
                                throw new ValidationException("не верно указана комиссия", ValidationErrorType.Error);
                            offer.agent_fee = fee;
                            if (fee > 0)
                                offer.id_commission_type = (long) CommissionType.Percent;
                            break;
                        }

                        case "utilities-included":
                        {
                            offer.utilities_included = AsBool(currentValue);
                            break;
                        }

                        case "electricity-included":
                        {
                            offer.electricity_included = AsBool(currentValue);
                            break;
                        }

                        #endregion

                        #region property

                        case "balcony":
                        {
                            var val = currentValue.ToLower().Trim();
                            if (int.TryParse(val, out var balcony))
                                offer.property.balcony_count = balcony;
                            else
                            {
                                if (val.Contains("балкон"))
                                {
                                    val = val.Replace("балкона").Replace("балконы")
                                        .Replace("балконов").Replace("балкон").Trim();
                                    if (String.IsNullOrEmpty(val))
                                        val = "1";
                                    if (int.TryParse(val, out balcony))
                                        offer.property.balcony_count = balcony;
                                    else
                                    {
                                        Log.Error(
                                            "Импорт фида '" + _fileUrl +
                                            "': Указан неизвестный параметр 'balcony' со значением: '" +
                                            currentValue + "'", true);
                                    }
                                }
                                else if (val.Contains("лодж"))
                                {
                                    val = val.Replace("лоджия").Replace("лоджии")
                                        .Replace("лоджиев").Replace("лоджий").Trim();
                                    if (String.IsNullOrEmpty(val))
                                        val = "1";

                                    if (int.TryParse(val, out var loggia))
                                        offer.property.loggia_count = loggia;
                                    else
                                    {
                                        Log.Error(
                                            "Импорт фида '" + _fileUrl +
                                            "': Указан неизвестный параметр 'balcony' со значением: '" +
                                            currentValue + "'", true);
                                    }
                                }
                                else
                                {
                                    Log.Error(
                                        "Импорт фида '" + _fileUrl +
                                        "': Указан неизвестный параметр 'balcony' со значением: '" +
                                        currentValue + "'", true);
                                }

                            }

                            break;
                        }

                        case "category":
                        {
                            switch (currentValue.ToLower())
                            {
                                case "коттедж":
                                case "cottage":
                                case "дача":
                                case "таунхаус":
                                case "townhouse":
                                case "дом":
                                case "house":
                                    offer.property.id_property_category = (long) PropertyLivingCategory.House;
                                    break;
                                case "дом с участком":
                                case "house with lot":
                                    offer.property.id_property_category = (long) PropertyLivingCategory.HouseWithLot;
                                    break;
                                case "lot":
                                case "участок":
                                case "земельный участок":
                                    offer.property.id_property_category = (long) PropertyLivingCategory.Lot;
                                    break;
                                case "часть дома":
                                    offer.property.id_property_category = (long) PropertyLivingCategory.HousePart;
                                    break;
                                case "flat":
                                case "квартира":
                                    offer.property.id_property_category = (long) PropertyLivingCategory.Apartment;
                                    break;
                                case "room":
                                case "комната":
                                    offer.property.id_property_category = (long) PropertyLivingCategory.Room;
                                    break;
                                case "дуплекс":
                                case "duplex":
                                    offer.property.id_property_category = (long) PropertyLivingCategory.Duplex;
                                    break;
                                case "гараж":
                                case "garage":
                                    isHasGarage = true;
                                    offer = null;
                                    break;
                                //throw new ValidationException("Мы пока не работаем с гаражами", ValidationErrorType.Error);
                                case "коммерческая":
                                case "commercial":
                                    offer.property.id_property_type = (long) PropertyType.Commercial;
                                    offer.id_offer_yrl_category = (long) OfferCategory.Commercial;
                                    break;
                                default:
                                    throw new ValidationException(
                                        "Не верно указана категория объявления: " + currentValue,
                                        ValidationErrorType.Error);
                            }

                            break;
                        }

                        case "description":
                            offer.property.description = currentValue;
                            break;
                        case "new-flat":
                            if (AsBool(currentValue))
                            {
                                offer.property.new_flat = true;
                                offer.id_offer_yrl_category = (long) OfferCategory.NewFlat;
                            }

                            break;
                        case "image":
                        {
                            offer.property.property_image.Add(
                                new property_image {image_url = currentValue});
                            break;
                        }

                        case "floor":
                        {
                            if (!int.TryParse(currentValue, out var floor))
                                throw new ValidationException(
                                    "Не верно указан этаж: " + currentValue, ValidationErrorType.Error);
                            offer.property.floor = floor;
                            break;
                        }

                        case "rooms":
                        {
                            if (!int.TryParse(currentValue, out var rooms))
                                throw new ValidationException(
                                    "Не верно указано кол-во комнат: " + currentValue, ValidationErrorType.Error);
                            offer.property.rooms = rooms;
                            break;
                        }

                        case "rooms-offered":
                        {
                            if (!int.TryParse(currentValue, out var rooms))
                                throw new ValidationException(
                                    "Не верно указано кол-во комнат в сдаче: " + currentValue,
                                    ValidationErrorType.Error);
                            offer.property.rooms_offered = rooms;
                            break;
                        }

                        case "renovation":
                        {
                            switch (currentValue.ToLower())
                            {
                                case "дизайнерский":
                                    offer.property.id_property_renovation = (long) RenovationType.Дизайнерский;
                                    break;
                                case "евро":
                                    offer.property.id_property_renovation = (long) RenovationType.Евро;
                                    break;
                                case "с отделкой":
                                    offer.property.id_property_renovation = (long) RenovationType.СОтделкой;
                                    break;
                                case "требуется ремонта":
                                case "требует ремонта":
                                    offer.property.id_property_renovation = (long) RenovationType.ТребуетРемонта;
                                    break;
                                case "хороший":
                                    offer.property.id_property_renovation = (long) RenovationType.Хороший;
                                    break;
                                case "частичный ремонт":
                                    offer.property.id_property_renovation = (long) RenovationType.ЧастичныйРемонт;
                                    break;
                                case "чистовая отделка":
                                    offer.property.id_property_renovation = (long) RenovationType.ЧистоваяОтделка;
                                    break;
                                case "черновая отделка":
                                    offer.property.id_property_renovation = (long) RenovationType.ЧерноваяОтделка;
                                    break;
                                case "под ключ":
                                    offer.property.id_property_renovation = (long) RenovationType.ПодКлюч;
                                    break;
                                default:
                                    throw new ValidationException(
                                        "Не верно указан ремонт: " + currentValue, ValidationErrorType.Error);
                            }

                            break;
                        }

                        case "rooms-type":
                        {
                            switch (currentValue.ToLower())
                            {
                                case "изолированные":
                                case "раздельные":
                                    offer.property.id_rooms_type = (long) RoomType.Isolated;
                                    break;
                                case "смежные":
                                    offer.property.id_rooms_type = (long) RoomType.Neighbor;
                                    break;
                                case "смежно-раздельные":
                                    offer.property.id_rooms_type = (long) RoomType.NeighborIsolated;
                                    break;


                                case "студия":
                                    offer.property.studio = true;
                                    break;
                                case "свободная":
                                    offer.property.open_plan = true;
                                    break;
                                default:
                                    throw new ValidationException("Не верно указан тип комнат: " + currentValue,
                                        ValidationErrorType.Error);
                            }

                            break;
                        }

                        case "window-view":
                        {
                            switch (currentValue.ToLower())
                            {
                                case "во двор":
                                    offer.property.id_window_view_type = (long) WindowType.Yard;
                                    break;
                                case "на улицу":
                                    offer.property.id_window_view_type = (long) WindowType.Street;
                                    break;
                                case "во двор и на улицу":
                                    offer.property.id_window_view_type = (long) WindowType.Both;
                                    break;
                                default:
                                    throw new ValidationException(
                                        "Не верно указано куда выходят окна: " + currentValue);
                            }

                            break;
                        }

                        case "ceiling-height":
                        {
                            if (!decimal.TryParse(currentValue.Replace(".", ","), out var height))
                                throw new ValidationException("Не верно указана высота потолков: " + currentValue,
                                    ValidationErrorType.Error);
                            // Если кто то указал в мм
                            if (height > 1000)
                                height = height / 1000;
                            // Если кто то указал в см
                            if (height > 100)
                                height = height / 100;
                            if (height < 1 || height > 5)
                                throw new ValidationException("Не верно указана высота потолков: " + currentValue,
                                    ValidationErrorType.Error);
                            offer.property.ceiling_height = height;
                            break;
                        }

                        case "bathroom-unit":
                        {
                            switch (currentValue)
                            {
                                case "совмещенный":
                                    offer.property.id_bathroom_unit = (long) BathroomUnit.Совмещенный;
                                    break;
                                case "раздельный":
                                    offer.property.id_bathroom_unit = (long) BathroomUnit.Раздельный;
                                    break;
                                default:
                                    if (!int.TryParse(currentValue, out var count) || count > 10)
                                        throw new ValidationException(
                                            "Не верно указано кол-во санузлов: " + currentValue);
                                    offer.property.id_bathroom_unit =
                                        count + 1; // формируется id-шник, там просто числа далее
                                    break;
                            }

                            break;
                        }

                        case "lot-type":
                        {
                            switch (currentValue.ToLower())
                            {
                                case "ижс":
                                    offer.property.id_lot_type = (long) LotType.Person;
                                    break;
                                case "садоводство":
                                    offer.property.id_lot_type = (long) LotType.Garden;
                                    break;
                                default:
                                    throw new ValidationException(
                                        "Неизвестный аргумент: " + currentValue);
                            }

                            break;
                        }

                        case "cadastral-number":
                        {
                            offer.property.cadastral_number = currentValue;
                            break;
                        }

                        case "phone":
                        {
                            offer.property.phone = AsBool(currentValue);
                            break;
                        }

                        case "water-supply":
                        {
                            offer.property.water_supply = AsBool(currentValue);
                            break;
                        }

                        case "gas-supply":
                        {
                            offer.property.gas_supply = AsBool(currentValue);
                            break;
                        }

                        case "sewerage-supply":
                        {
                            offer.property.sewerage_supply = AsBool(currentValue);
                            break;
                        }

                        case "heating-supply":
                        {
                            offer.property.heating_supply = AsBool(currentValue);
                            break;
                        }

                        case "room-furniture":
                        {
                            offer.property.room_furniture = AsBool(currentValue);
                            break;
                        }

                        case "sauna":
                        {
                            offer.property.sauna = AsBool(currentValue);
                            break;
                        }

                        case "refrigerator":
                        {
                            offer.property.refrigerator = AsBool(currentValue);
                            break;
                        }

                        case "with-children":
                        {
                            offer.property.with_children = AsBool(currentValue);
                            break;
                        }

                        case "with-pets":
                        {
                            offer.property.with_pets = AsBool(currentValue);
                            break;
                        }

                        case "television":
                        {
                            offer.property.television = AsBool(currentValue);
                            break;
                        }

                        case "dishwasher":
                        {
                            offer.property.dishwasher = AsBool(currentValue);
                            break;
                        }

                        case "washing-machine":
                        {
                            offer.property.washing_machine = AsBool(currentValue);
                            break;
                        }

                        case "shower":
                        {
                            offer.property.shower = AsBool(currentValue);
                            break;
                        }

                        case "internet":
                        {
                            offer.property.internet = AsBool(currentValue);
                            break;
                        }

                        case "air-conditioner":
                        {
                            offer.property.air_conditioner = AsBool(currentValue);
                            break;
                        }

                        case "electricity-supply":
                        {
                            offer.property.electricity_supply = AsBool(currentValue);
                            break;
                        }

                        case "toilet":
                        {
                            offer.property.toilet = AsBool(currentValue);
                            break;
                        }

                        case "studio":
                        {
                            offer.property.studio = AsBool(currentValue);
                            break;
                        }

                        case "open-plan":
                        {
                            offer.property.open_plan = AsBool(currentValue);
                            break;
                        }

                        case "pool":
                        {
                            offer.property.pool = AsBool(currentValue);
                            break;
                        }

                        case "rubbish-chute":
                        {
                            offer.property.rubbish_chute = AsBool(currentValue);
                            break;
                        }

                        case "kitchen-furniture":
                        {
                            offer.property.kitchen_furniture = AsBool(currentValue);
                            break;
                        }

                        #endregion

                        #region building

                        case "building-state":
                        {
                            switch (currentValue.ToLower())
                            {
                                case "hand-over":
                                    offer.property.building.id_building_state = (long) BuildingState.Done;
                                    break;
                                case "built":
                                    offer.property.building.id_building_state = (long) BuildingState.BuildedButNotDone;
                                    break;
                                case "unfinished":
                                    offer.property.building.id_building_state = (long) BuildingState.InBuild;
                                    break;
                                default:
                                    throw new ValidationException(
                                        "Указано неизвестное состояние здания: " + currentValue);
                            }

                            break;
                        }

                        case "ready-quarter":
                        {
                            if (!int.TryParse(currentValue, out var quarter) || quarter < 1 || quarter > 4)
                                throw new ValidationException("Не верно указан квартал сдачи дома: " + currentValue);

                            offer.property.building.id_ready_quarter = quarter;
                            break;
                        }

                        case "building-series":
                        {
                            offer.property.building.building_series = currentValue;
                            break;
                        }

                        case "floors-total":
                        {
                            if (!int.TryParse(currentValue, out var floors))
                                throw new ValidationException(
                                    "Не верно указана этажность здания: " + currentValue);
                            offer.property.building.floors_total = floors;
                            break;
                        }

                        case "floor-covering":
                        {
                            var type = FloorCoveringType.Default;
                            switch (currentValue.ToLower())
                            {
                                case "ковролин":
                                    type = FloorCoveringType.Carpet;
                                    break;
                                case "ламинат":
                                    type = FloorCoveringType.Laminat;
                                    break;
                                case "линолеум":
                                    type = FloorCoveringType.Linoleum;
                                    break;
                                case "паркет":
                                    type = FloorCoveringType.Parket;
                                    break;
                                default:
                                    throw new ValidationException("Неизвестное покрытие пола: " + currentValue);
                            }

                            offer.property.id_floor_covering_type = (long) type;
                            break;
                        }

                        case "building-type":
                        {
                            switch (currentValue.ToLower())
                            {
                                case "шлакоблок":
                                case "блок":
                                case "блочный":
                                    offer.property.building.id_building_type = (long) BuildingMaterialType.Блочный;
                                    break;
                                case "рубленый":
                                case "брус":
                                case "деревянный":
                                    offer.property.building.id_building_type = (long) BuildingMaterialType.Деревянный;
                                    break;
                                case "кирпичный":
                                case "кирпично-насыпной":
                                    offer.property.building.id_building_type = (long) BuildingMaterialType.Кирпич;
                                    break;
                                case "каркасно-насыпной":
                                    offer.property.building.id_building_type =
                                        (long) BuildingMaterialType.КаркасноНасыпной;
                                    break;
                                case "монолитно-кирпичный":
                                case "кирпично-монолитный":
                                    offer.property.building.id_building_type =
                                        (long) BuildingMaterialType.КирпичноМонолитный;
                                    break;
                                case "монолит":
                                    offer.property.building.id_building_type = (long) BuildingMaterialType.Монолит;
                                    break;
                                case "panel":
                                case "панельный":
                                    offer.property.building.id_building_type = (long) BuildingMaterialType.Панельный;
                                    break;
                                case "железобетонный":
                                    offer.property.building.id_building_type =
                                        (long) BuildingMaterialType.Железобетонный;
                                    break;
                                case "металлический":
                                    offer.property.building.id_building_type =
                                        (long) BuildingMaterialType.Металлический;
                                    break;
                                default:
                                    throw new ValidationException("Не верно указан материал здания: " + currentValue);
                            }

                            break;
                        }

                        case "built-year":
                        {
                            if (!int.TryParse(currentValue, out var year))
                                throw new ValidationException("Не верно указан год постройки: " + currentValue);

                            if (year < 40)
                                year += 2000;
                            if (year < 100)
                                year += 1900;
                            if (year < 1900 || year > DateTime.Now.Year + 3)
                                throw new ValidationException("Не верно указан год постройки: " + currentValue);
                            offer.property.building.built_year = year;
                            break;
                        }

                        case "building-section":
                        {
                            offer.property.building.building_section = currentValue;
                            break;
                        }

                        case "parking":
                        {
                            offer.property.building.parking = AsBool(currentValue);
                            break;
                        }

                        case "lift":
                        {
                            offer.property.building.lift = AsBool(currentValue);
                            break;
                        }

                        case "security":
                        {
                            offer.property.building.security = AsBool(currentValue);
                            break;
                        }

                        case "is-elite":
                        {
                            offer.property.building.is_elite = AsBool(currentValue);
                            break;
                        }

                        case "alarm":
                        {
                            offer.property.building.alarm = AsBool(currentValue);
                            break;
                        }

                        #endregion

                        #region Commercial


                        case "commercial-type":
                        {
                            var type = PropertyCommercialType.Default;
                            switch (currentValue.ToLower())
                            {
                                case "автосервис":
                                case "auto_repair":
                                case "auto repair":
                                case "autorepair":
                                    type = PropertyCommercialType.Autorepair;
                                    break;
                                case "готовый бизнес":
                                case "бизнес":
                                case "business":
                                    type = PropertyCommercialType.Business;
                                    break;
                                case "помещения свободного назначения":
                                case "freepurpose":
                                case "free_purpose":
                                case "free purpose":
                                    type = PropertyCommercialType.FreePurpose;
                                    break;
                                case "гостиница":
                                case "hotel":
                                    type = PropertyCommercialType.Hotel;
                                    break;
                                case "земли коммерческого назначения":
                                case "земля":
                                case "участок":
                                case "land":
                                    type = PropertyCommercialType.Land;
                                    break;
                                case "юридический адрес":
                                case "legal_address":
                                case "legaladdress":
                                case "legal address":
                                    type = PropertyCommercialType.LegalAddress;
                                    break;
                                case "производственное помещение":
                                case "manufacturing":
                                    type = PropertyCommercialType.Manufacturing;
                                    break;
                                case "офисные помещения":
                                case "офис":
                                case "office":
                                    type = PropertyCommercialType.Office;
                                    break;
                                case "общепит":
                                case "public_catering":
                                case "publiccatering":
                                case "public catering":
                                    type = PropertyCommercialType.PublicCatering;
                                    break;
                                case "торговые помещения":
                                case "retail":
                                    type = PropertyCommercialType.Retail;
                                    break;
                                case "склад":
                                case "warehouse":
                                    type = PropertyCommercialType.Warehouse;
                                    offer.property.warehouse = new warehouse();
                                    break;
                                default:
                                    throw new ValidationException(
                                        "Не верно указана категория объявления: " + currentValue,
                                        ValidationErrorType.Error);
                            }

                            offer.property.property_property_commercial_type.Add(new property_property_commercial_type()
                            {
                                id_property_commercial_type = (long) type,
                                create_dt = DateTime.Now
                            });
                            break;
                        }

                        case "commercial-building-type":
                        {
                            var type = CommercialBuildingType.Default;
                            switch (currentValue.ToLower())
                            {
                                case "бизнес-центр":
                                case "businesscenter":
                                case "business center":
                                case "business_center":
                                    type = CommercialBuildingType.BusinessCenter;
                                    break;
                                case "отдельно стоящее здание":
                                case "detachedbuilding":
                                case "detached_building":
                                case "detached building":
                                    type = CommercialBuildingType.DetachedBuilding;
                                    break;
                                case "встроенное помещение":
                                case "residentialbuilding":
                                case "residential_building":
                                case "residential building":
                                    type = CommercialBuildingType.ResidentialBuilding;
                                    break;
                                case "торговый центр":
                                case "shoppingcenter":
                                case "shopping_center":
                                case "shopping center":
                                    type = CommercialBuildingType.ShoppingCenter;
                                    break;
                                case "складской комплекс":
                                case "warehouse":
                                    type = CommercialBuildingType.Warehouse;
                                    break;
                                default:
                                    throw new ValidationException(
                                        "Неизвестный тип коммерческого здания: " + currentValue,
                                        ValidationErrorType.Warning);
                            }

                            offer.property.building.id_building_commercial_type = (long) type;
                            break;
                        }

                        case "purpose":
                        {
                            var type = CommercialBuildingPurpose.Default;
                            switch (currentValue.ToLower())
                            {
                                case "помещение для банка":
                                case "банк":
                                case "bank":
                                    type = CommercialBuildingPurpose.Bank;
                                    break;
                                case "продуктовый магазин":
                                case "food store":
                                case "food_store":
                                case "foodstore":
                                    type = CommercialBuildingPurpose.FoodStore;
                                    break;
                                case "салон красоты":
                                case "beauty shop":
                                case "beauty_shop":
                                case "beautyshop":
                                    type = CommercialBuildingPurpose.BeautyShop;
                                    break;
                                case "медицинский центр":
                                case "medical center":
                                case "medical_center":
                                case "medicalcenter":
                                    type = CommercialBuildingPurpose.MedicalCenter;
                                    break;
                                case "шоу-рум":
                                case "show room":
                                case "show_room":
                                case "showroom":
                                    type = CommercialBuildingPurpose.ShowRoom;
                                    break;
                                case "турагентство":
                                case "touragency":
                                    type = CommercialBuildingPurpose.Touragency;
                                    break;
                                default:
                                    throw new ValidationException(
                                        "Неизвестный тип коммерческого здания: " + currentValue);
                            }

                            offer.property.building.id_commercial_building_purpose = (long) type;
                            break;
                        }

                        case "purpose-warehouse":
                        {
                            if (offer.property.warehouse == null)
                                offer.property.warehouse = new warehouse();
                            var type = CommercialWarehousePurpose.Default;
                            switch (currentValue.ToLower())
                            {
                                case "алкогольный склад":
                                case "alcohol":
                                    type = CommercialWarehousePurpose.Alcohol;
                                    break;
                                case "фармацевтический склад":
                                case "pharmaceutical storehouse":
                                case "pharmaceutical_storehouse":
                                case "pharmaceuticalstorehouse":
                                    type = CommercialWarehousePurpose.PharmaceuticalStorehouse;
                                    break;
                                case "овощехранилище":
                                case "vegetable storehouse":
                                case "vegetable_storehouse":
                                case "vegetablestorehouse":
                                    type = CommercialWarehousePurpose.VegetableStorehouse;
                                    break;
                                default:
                                    throw new ValidationException("Неизвестный тип назначения склада: " + currentValue,
                                        ValidationErrorType.Warning);
                            }

                            offer.property.warehouse.id_warehouse_purpose = (long) type;
                            break;
                        }

                        case "taxation-form":
                        {
                            var type = TaxationForm.Default;
                            switch (currentValue.ToUpper())
                            {
                                case "НДС":
                                    type = TaxationForm.Nds;
                                    break;
                                case "УСН":
                                    type = TaxationForm.Usn;
                                    break;
                            }

                            offer.id_taxation_form = (long) type;
                            break;
                        }

                        case "entrance-type":
                        {
                            var type = EntranceType.Default;
                            switch (currentValue.ToLower())
                            {
                                case "общий":
                                case "common":
                                    type = EntranceType.Common;
                                    break;
                                case "отдельный":
                                case "separate":
                                    type = EntranceType.Separate;
                                    break;
                            }

                            offer.property.id_entrance_type = (long) type;
                            break;
                        }

                        case "office-class":
                        {
                            var type = OfficeClassType.Default;
                            switch (currentValue.ToLower())
                            {
                                case "a":
                                case "а":
                                    type = OfficeClassType.A;
                                    break;
                                case "a+":
                                case "а+":
                                    type = OfficeClassType.Aplus;
                                    break;
                                case "b":
                                case "б":
                                    type = OfficeClassType.B;
                                    break;
                                case "b+":
                                case "б+":
                                    type = OfficeClassType.Bplus;
                                    break;
                                case "с":
                                case "c":
                                    type = OfficeClassType.C;
                                    break;
                                case "с+":
                                case "c+":
                                    type = OfficeClassType.Cplus;
                                    break;
                            }

                            offer.property.building.id_office_class = (long) type;
                            break;
                        }

                        case "phone-lines":
                        {
                            if (!long.TryParse(currentValue.Replace(".", ","), out var count))
                                throw new ValidationException(
                                    "Не удалось распознать количество телефонных линий: " + currentValue);
                            offer.property.phone_lines = count;
                            break;
                        }

                        case "parking-places":
                        {
                            if (!long.TryParse(currentValue.Replace(".", ","), out var count))
                                throw new ValidationException(
                                    "Не удалось распознать количество парковочных мест: " + currentValue);
                            offer.property.building.parking_places = count;
                            break;
                        }

                        case "parking-place-price":
                        {
                            if (!long.TryParse(currentValue.Replace(".", ","), out var price))
                                throw new ValidationException(
                                    "Не удалось распознать стоимость парковочных мест: " + currentValue);
                            offer.property.building.parking_place_price = price;
                            break;
                        }

                        case "guarded-building":
                        {
                            offer.property.building.guarded_building = AsBool(currentValue);
                            break;
                        }

                        case "access-control-system":
                        {
                            offer.property.access_control_system = AsBool(currentValue);
                            break;
                        }

                        case "twenty-four-seven":
                        {
                            offer.property.building.twenty_four_seven = AsBool(currentValue);
                            break;
                        }

                        case "parking-guest":
                        {
                            offer.property.building.parking_guest = AsBool(currentValue);
                            break;
                        }

                        case "parking-guest-places":
                        {
                            if (!long.TryParse(currentValue.Replace(".", ","), out var count))
                                throw new ValidationException(
                                    "Не удалось распознать количество гостевых парковочных мест: " + currentValue);
                            offer.property.building.parking_guest_places = count;
                            break;
                        }

                        case "eating-facilities":
                        {
                            offer.property.building.eating_facilities = AsBool(currentValue);
                            break;
                        }

                        case "adding-phone-on-request":
                        {
                            offer.property.adding_phone_on_request = AsBool(currentValue);
                            break;
                        }

                        case "self-selection-telecom":
                        {
                            offer.property.self_selection_telecom = AsBool(currentValue);
                            break;
                        }

                        case "security-payment":
                        {
                            if (!long.TryParse(currentValue.Replace(".", ","), out var percent))
                                throw new ValidationException(
                                    "Не удалось распознать размер обеспечительного платежа в процентах: " +
                                    currentValue);
                            offer.security_payment = percent;
                            break;
                        }

                        #region warehose

                        case "responsible-storage":
                        {
                            if (offer.property.warehouse == null) offer.property.warehouse = new warehouse();
                            offer.property.warehouse.responsible_storage = AsBool(currentValue);
                            break;
                        }

                        case "pallet-price":
                        {
                            if (offer.property.warehouse == null) offer.property.warehouse = new warehouse();
                            if (!decimal.TryParse(currentValue.Replace(".", ","), out var price))
                                throw new ValidationException(
                                    "Не удалось распознать стоимость палето-мест: " + currentValue);
                            offer.property.warehouse.pallet_price = price;
                            break;
                        }

                        case "freight-elevator":
                        {
                            if (offer.property.warehouse == null) offer.property.warehouse = new warehouse();
                            offer.property.warehouse.freight_elevator = AsBool(currentValue);
                            break;
                        }

                        case "truck-entrance":
                        {
                            if (offer.property.warehouse == null) offer.property.warehouse = new warehouse();
                            offer.property.warehouse.truck_entrance = AsBool(currentValue);
                            break;
                        }

                        case "ramp":
                        {
                            if (offer.property.warehouse == null) offer.property.warehouse = new warehouse();
                            offer.property.warehouse.ramp = AsBool(currentValue);
                            break;
                        }

                        case "railway":
                        {
                            if (offer.property.warehouse == null) offer.property.warehouse = new warehouse();
                            offer.property.warehouse.railway = AsBool(currentValue);
                            break;
                        }

                        case "office-warehouse":
                        {
                            if (offer.property.warehouse == null) offer.property.warehouse = new warehouse();
                            offer.property.warehouse.office_warehouse = AsBool(currentValue);
                            break;
                        }

                        case "open-area":
                        {
                            if (offer.property.warehouse == null) offer.property.warehouse = new warehouse();
                            offer.property.warehouse.open_area = AsBool(currentValue);
                            break;
                        }

                        case "service-three-pl":
                        {
                            if (offer.property.warehouse == null) offer.property.warehouse = new warehouse();
                            offer.property.warehouse.service_three_pl = AsBool(currentValue);
                            break;
                        }

                        case "temperature-comment":
                        {
                            if (offer.property.warehouse == null) offer.property.warehouse = new warehouse();
                            offer.property.warehouse.temperature_comment = currentValue;
                            break;
                        }

                        #endregion

                        #endregion

                        #region Garage

                        case "cellar":
                        case "inspection-pit":
                        case "garage-type":
                        {
                            throw new ValidationException("не работаем с гаражами");
                        }

                        case "garage-name":
                        {
                            throw new ValidationException("не работаем с гаражами");
                        }

                        #endregion

                        default:
                            return false;
                            break;
                    }

                    break;
            }

            return true;
        }
    }
}

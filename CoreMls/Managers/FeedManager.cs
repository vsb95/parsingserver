﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Core;
using Core.Exceptions;
using Core.Extension;
using Core.Services;
using CoreMls.DB.LocalEntities;
using CoreMls.DB.Managers;
using CoreMls.DB.Model;
using CoreMls.Enum;
using CoreMls.Extension;
using CoreMls.Interfaces;
using CoreMls.Managers.FeedCreator;
using CoreMls.Managers.FeedParsers;
using CoreMls.Validators;
using Logger;

namespace CoreMls.Managers
{
    public static class FeedManager
    {
        /// <summary>
        /// Импортировать фид
        /// </summary>
        /// <returns></returns>
        public static bool TryImport(FeedSite site, string fileUrl, long idOrg, out FeedImportResult result)
        {
            result = null;
            if (string.IsNullOrEmpty(fileUrl))
                throw new ArgumentNullException(nameof(fileUrl));
            IFeedParser parser;
            switch (site)
            {
                case FeedSite.Yandex:
                    parser = new YandexFeedParser();
                    break;
                default:
                    throw new NotImplementedException("Пока не принимаем для импорта такой тип фида");
            }
            var watch = new Stopwatch();
            watch.Start();
            if (!parser.TryParse(fileUrl, idOrg, out result)) return false;
            var importedIdList = new List<long>();
            var isDeleteOldOffers = true;
            foreach (var offer in result.Offers)
            {
                if (offer.id_offer != 0)
                    result.CountAdded++;
                else
                    result.CountUpdated++;

                var adv = new MlsAdvertisment(offer);
                if (DbOfferManager.AddOrUpdatePage(adv))
                {
                    if (!adv.Id.HasValue)
                    {
                        isDeleteOldOffers = false;
                        Log.Error("После добавления объявления не проставился Id у internal-id: "+offer.feed_internal_id, true);
                    }
                    else
                    {
                        importedIdList.Add(adv.Id.Value);
                    }
                    result.Messages.Add("Объявление " + offer.feed_internal_id + " успешно добавлено");
                }
                else
                {
                    result.Errors.Add("Ошибка! Не удалось добавить объявление " + offer.feed_internal_id);
                }
            }
#if  DEBUG
            var countDeleted = -1;
            if (!isDeleteOldOffers || importedIdList.Count <= 0 ||
                !DbOfferManager.TryDelete(importedIdList, idOrg, true, out countDeleted))
            {
                Log.Fatal("Не удалось удалить старые объявления при импорте нового "+ site + " фида '"+ fileUrl + "' для организации '"+ idOrg + "'");
            }

            result.CountDeleted = countDeleted;

            Log.Debug("\n\n" + string.Join("\n", result.Errors));
#endif
            Log.Info("Фид '"+ fileUrl + "' парсился: "+watch.GetTotalSeconds()+" сек");
            return true;
        }

        /// <summary>
        /// Сгенерировать фид если необходимо и вернуть дату создания фида
        /// </summary>
        /// <returns></returns>
        public static bool TryGetOrCreateFeed(long idOrg, out string file, out DateTime createDate)
        {
            file = null;
            createDate = DateTime.Now;
            if (idOrg == 0)
                return false;
            try
            {
                using (var context = new realtyMLSEntities())
                {
                    var organization = context.sales_agent_organization.FirstOrDefault(x => x.id_sales_agent_organization == idOrg);
                    if (organization == null)
                        return false;
                    file = organization.feed_yrl_file;
                    if (string.IsNullOrEmpty(file))
                    {
                        file = Path.Combine(SettingsManager.Settings.ServerSettings.MlsFeedFolder,
                            Randomizer.CreateRandomString(10) + ".xml");
                        while (File.Exists(file))
                        {
                            file = Path.Combine(SettingsManager.Settings.ServerSettings.MlsFeedFolder,
                                Randomizer.CreateRandomString(10) + ".xml");
                        }
                        organization.feed_yrl_file = file;
                        context.SaveChanges();
                    }
                    else
                    {
                        var fileInfo = new FileInfo(file);
                        if (fileInfo.Exists && (DateTime.Now - fileInfo.LastWriteTime).TotalHours < 12)
                        {
                            Log.Trace("Фид не будем генерировать тк он был создан: " +
                                      fileInfo.LastWriteTime.ToString("g") + ", А это " +
                                      (DateTime.Now - fileInfo.LastWriteTime).TotalHours + " часов назад");
                            createDate = fileInfo.LastWriteTime;
                            return true;
                        }
                    }
                    //Если нет объявлений - то выходим
                    if (!context.offers.Any(x => !x.delete_dt.HasValue && x.sales_agent.id_sales_agent_organization == idOrg))
                    {
                        Log.Trace("Фид не будем генерировать тк не найдено ниодного объявления");
                        File.WriteAllText(file, "");
                        return true;
                    }

                    createDate = DateTime.Now;
                    var watch = new Stopwatch();
                    watch.Start();
                    IFeedCreator creator = new YandexFeedCreator(); // заменить потом на выбор создателя
                    var res =  creator.TryCreateFeed(idOrg, file);
                    Log.Info("Фид для организации " + idOrg+ " создавался: " + watch.GetTotalSeconds() + " сек");
                    return res;
                }
            }
            catch (Exception e)
            {
                Log.Exception(e, LogType.Fatal, true, "[MLS::DbManager::TryGetOrCreateFeed] idOrg = " + idOrg);
            }
            return false;
        }

        /// <summary>
        /// Вернет список ассоциаций виртуальных агентов и юзеров в системе
        /// </summary>
        /// <returns></returns>
        public static Dictionary<SalesAgent, long?> GetAgentAssociation(long idOrg)
        {
            try
            {
                var result = new Dictionary<SalesAgent, long?>();
                using (var context = new realtyMLSEntities())
                {
                    var virtualAgents =
                        context.sales_agent.Where(x => x.is_virtual && x.id_sales_agent_organization == idOrg);
                    foreach (var virtualAgent in virtualAgents)
                    {
                        var association = context.sales_agent_feed_user.FirstOrDefault(x =>x.id_sales_agent_feed == virtualAgent.id_sales_agent);
                        result.Add(new SalesAgent(virtualAgent), association?.id_sales_agent_user);
                    }
                }
                
                return result;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "[FeedManager::GetAgentAssociation]");
            }

            return null;
        }

        /// <summary>
        /// Обновление связи "виртуальный агент -> реальный пользователь"
        /// </summary>
        /// <param name="idOrg">Организация</param>
        /// <param name="virtualAgentId">Виртуальный агент, если не указан сбросит всех виртуальных с реального пользователя</param>
        /// <param name="realAgent">Реальный пользователь</param>
        /// <param name="newOwherId">Новый реальный пользователь. Указывается если не указан виртуальный агент. На него скинутся все виртуальные агенты со старого реального пользователя</param>
        /// <returns></returns>
        public static bool TryUpdateAgentAssociation(long idOrg, long? virtualAgentId, SalesAgent realAgent, long? newOwherId = null)
        {
            if (realAgent == null)
                return false;
            try
            {
                using (var context = new realtyMLSEntities())
                {
                    if (virtualAgentId.HasValue)
                    {
                        // Сбросить все объявления на виртуального пользователя, чтобы отображалось нормально, а редактировать мог только админ
                        if (realAgent.Id == -1)
                        {
                            var association = context.sales_agent_feed_user.FirstOrDefault(x => x.id_sales_agent_feed == virtualAgentId.Value);
                            if (association != null)
                            {
                                context.sales_agent_feed_user.Remove(association);
                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            var realAgentId = DbSalesAgentManager.AddOrUpdate(realAgent.ToDb());
                            var association = context.sales_agent_feed_user.FirstOrDefault(x => x.id_sales_agent_feed == virtualAgentId.Value) ??
                                              new sales_agent_feed_user { id_sales_agent_feed = virtualAgentId.Value };
                            if (association.id_sales_agent_user == realAgentId)
                                return true;
                            association.id_sales_agent_user = realAgentId;
                            context.sales_agent_feed_user.AddOrUpdate(association);
                            context.SaveChanges();

                            foreach (var offer in context.offers.Where(x => x.id_sales_agent == virtualAgentId).ToList())
                            {
                                offer.id_sales_agent = realAgentId;
                                context.offers.AddOrUpdate(offer);
                                context.SaveChanges();
                            }
                        }
                    }
                    // Поиск виртуальных пользователей привязанных к юзеру и сброс ассоциаций, если есть
                    else
                    {
                        if (realAgent.Id == -1)
                            return true;
                        var associations = context.sales_agent_feed_user.Where(x => x.id_sales_agent_user == realAgent.Id).ToList();
                        if (associations.Count == 0)
                            return true;
                        foreach (var association in associations)
                        {
                            if (newOwherId.HasValue)
                            {
                                association.id_sales_agent_user = newOwherId.Value;
                                context.sales_agent_feed_user.AddOrUpdate(association);
                            }
                            else
                            {
                                context.sales_agent_feed_user.Remove(association);
                            }
                            context.SaveChanges();
                        }
                    }
                }
                
                return true;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "[FeedManager::GetAgentAssociation]");
            }

            return false;
        }

        /// <summary>
        /// Добавить фид к автоимпорту
        /// </summary>
        /// <returns></returns>
        public static bool AddAutoImportFeed(string fileUrl, long idOrg, string email, FeedSite feedType)
        {
            if (string.IsNullOrEmpty(fileUrl))
                return false;
            Log.Info("Добавление Автоимпорта фида " + idOrg + ": " + fileUrl);
            try
            {
                using (var context = new realtyMLSEntities())
                {
                    if (!context.sales_agent_organization.Any(x => x.id_sales_agent_organization == idOrg))
                        throw new ValidationException("Не найдена организация: "+ idOrg, ValidationErrorType.Error);
                    if (context.sales_agent_organization_import_feed.Any(x => x.id_organization == idOrg && x.file_url == fileUrl))
                        throw new ValidationException("Нельзя добавить дублирующий фид к автозагрузке", ValidationErrorType.Error);

                    var item = new sales_agent_organization_import_feed
                    {
                        id_organization = idOrg,
                        contact_email = email,
                        id_feed = (long) feedType,
                        last_import_dt = DateTime.Now,
                        create_dt = DateTime.Now,
                        file_url = fileUrl
                    };
                    context.sales_agent_organization_import_feed.Add(item);
                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "[FeedManager::AddAutoFeed] " + idOrg + ": " + fileUrl);
            }

            return false;
        }

        /// <summary>
        /// Получить список фидов, которые импортируются автоматически
        /// </summary>
        /// <returns></returns>
        public static List<string> GetAutoImportFeeds(int idOrg)
        {
            try
            {
                using (var context = new realtyMLSEntities())
                {
                    return context.sales_agent_organization_import_feed.Where(x => x.id_organization == idOrg)
                        .Select(x => x.file_url).ToList();
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "[FeedManager::GetAutoImportFeeds] " + idOrg);
            }

            return null;
        }
    }
}

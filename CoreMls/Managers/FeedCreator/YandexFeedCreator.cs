﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Core.Exceptions;
using CoreMls.DB.Model;
using CoreMls.Enum;
using CoreMls.Extension;
using CoreMls.Interfaces;
using ValidationException = Core.Exceptions.ValidationException;
using Logger;

namespace CoreMls.Managers.FeedCreator
{
    internal class YandexFeedCreator :IFeedCreator
    {
        private static readonly XNamespace YandexXmlNamespace = "http://webmaster.yandex.ru/schemas/feed/realty/2010-06";
        private const long IdFeedSite = (long) FeedSite.Yandex;

        public bool TryCreateFeed(long idOrg, string fileTo)
        {
            if (string.IsNullOrEmpty(fileTo))
                return false;

            try
            {
                var xRealtyFeed = new XElement("realty-feed");
                xRealtyFeed.Add(new XElement("generation-date", DateTime.Now.ToString("s")));

                using (var context = new realtyMLSEntities())
                {
                    foreach (var enabledOffer in context.offer_feed_site.Where(x =>
                        x.id_feed_site == IdFeedSite && x.is_enabled_autoload &&
                        !x.offer.delete_dt.HasValue && x.offer.sales_agent.id_sales_agent_organization == idOrg))
                    {
                        var offer = enabledOffer.offer;
                        // Костыль для галеона - отдельные фиды с одним номером телефона
                        if (idOrg == 3 && offer.id_offer_status.HasValue &&
                            offer.id_offer_status == (long)OfferStatus.DirectSale)
                        {
                            offer.sales_agent.sales_agent_phone.Clear();
                            offer.sales_agent.sales_agent_phone.Add(new sales_agent_phone() { phone_number = 89620585584 });
                        }
                        var offerXml = CreateXmlElementYandex(offer, context);
                        xRealtyFeed.Add(offerXml);
                    }
                }

                var xdoc = new XDocument(xRealtyFeed);
                xdoc.Root.SetDefaultXmlNamespace(YandexXmlNamespace);
                xdoc.Save(fileTo);


                // Костыль для галеона - отдельные фиды с одним номером телефона
                if (idOrg == 3)
                {
                    var fileinfo = new FileInfo(fileTo);

                    #region Фид для циана
                    {
                        var file = Path.Combine(fileinfo.Directory.FullName, "galleon_cian.xml");
                        var xhardRealtyFeed = new XElement("realty-feed");
                        xhardRealtyFeed.Add(new XElement("generation-date", DateTime.Now.ToString("s")));

                        using (var context = new realtyMLSEntities())
                        {
                            foreach (var enabledOffer in context.offer_feed_site.Where(x =>
                                x.id_feed_site == IdFeedSite && x.is_enabled_autoload &&
                                !x.offer.delete_dt.HasValue &&
                                x.offer.sales_agent.id_sales_agent_organization == idOrg))
                            {
                                var offer = enabledOffer.offer;
                                if (offer.id_offer_status.HasValue &&
                                    offer.id_offer_status == (long) OfferStatus.DirectSale)
                                {
                                    offer.sales_agent.sales_agent_phone.Clear();
                                    offer.sales_agent.sales_agent_phone.Add(new sales_agent_phone() {phone_number = 89620585582 });
                                }

                                var offerXml = CreateXmlElementYandex(offer, context);
                                xhardRealtyFeed.Add(offerXml);
                            }
                        }

                        var xhardDoc = new XDocument(xhardRealtyFeed);
                        xhardDoc.Root.SetDefaultXmlNamespace(YandexXmlNamespace);
                        xhardDoc.Save(file);
                    }

                    #endregion
                }

                return true;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "[YandexFeedCreator::TryCreateFeed] idOrg " + idOrg);
            }


            return false;
        }

        private static XElement CreateXmlElementYandex(offer offer, realtyMLSEntities context)
        { 
            var trace = new StringBuilder();
            try
            {
                var phone = offer.sales_agent.sales_agent_phone.FirstOrDefault();
                if (phone == null)
                    throw new ValidationException("не указан номер телефона для объявления: " + offer.id_offer,ValidationErrorType.Error);
                if (!offer.property.id_property_type.HasValue)
                    throw new ValidationException("Не указана категория объекта", ValidationErrorType.Error);
                //if (string.IsNullOrEmpty(offer.property.building.building_location.locality_name)) throw new ValidationException("не указан locality-name", ValidationErrorType.Error);
                trace.AppendLine("1");
                var xOffer = new XElement(YandexXmlNamespace + "offer", new XAttribute("internal-id", offer.id_offer));
                xOffer.Add(new XElement("type", offer.offer_type.offer_type_name));
                xOffer.Add(new XElement("property-type", offer.property.property_type.property_type_name));
                xOffer.Add(new XElement("category", offer.property.property_category.property_category_name));
                xOffer.Add(new XElement("creation-date", offer.creation_date?.ToString("s") ?? DateTime.Now.ToString("s")));
                xOffer.Add(new XElement("last-update-date", offer.last_update_date?.ToString("s") ?? DateTime.Now.ToString("s")));
                if(offer.id_offer_status.HasValue)
                    xOffer.Add(new XElement("deal-status", offer.offer_status.offer_status_name));
                trace.AppendLine("2");
                if (offer.property.id_property_renovation.HasValue)
                    xOffer.Add(new XElement("renovation", offer.property.property_renovation.property_renovation_name));
                xOffer.Add(new XElement("description", offer.property.description));
                if (!string.IsNullOrEmpty(offer.property.cadastral_number))
                    xOffer.Add(new XElement("cadastral-number", offer.property.cadastral_number));
                if (offer.rent_pledge.HasValue)
                    xOffer.Add(new XElement("rent-pledge", offer.rent_pledge.Value));
                if (offer.mortgage.HasValue)
                    xOffer.Add(new XElement("mortgage", offer.mortgage.Value));
                if (offer.haggle.HasValue)
                    xOffer.Add(new XElement("haggle", offer.haggle.Value));
                //if (offer.prepayment.HasValue)
                //    xOffer.Add(new XElement("prepayment", offer.haggle.Value));
                if (offer.agent_fee.HasValue)
                    xOffer.Add(new XElement("agent-fee", (int)offer.agent_fee.Value));
                if (offer.utilities_included.HasValue)
                    xOffer.Add(new XElement("utilities-included", offer.utilities_included.Value));
                if (offer.property.id_lot_type.HasValue)
                    xOffer.Add(new XElement("lot-type", offer.property.lot_type.lot_type_name));

                trace.AppendLine("3");
                foreach (var image in offer.property.property_image)
                {
                    xOffer.Add(new XElement("image", image.image_url));
                }

                trace.AppendLine("4");
                #region price

                var price = new XElement("price", new XElement("value", offer.price_value),
                    new XElement("currency", "RUB"));
                if (offer.price_id_period.HasValue)
                {
                    price.Add(new XElement("period", offer.period.period_name));
                }
                xOffer.Add(price);
                #endregion

                trace.AppendLine("5");
                #region area

                trace.AppendLine("5.1");
                if (offer.property.area_value.HasValue)
                {
                    var totalArea = new XElement("area",
                        new XElement("value", offer.property.area_value),
                        new XElement("unit",
                            context.units.FirstOrDefault(x => x.id_unit == offer.property.area_id_unit)?.unit_name));
                    xOffer.Add(totalArea);
                }

                trace.AppendLine("5.2");
                if (offer.property.living_space_value.HasValue)
                {
                    var livingArea = new XElement("living-space",
                        new XElement("value", offer.property.living_space_value),
                        new XElement("unit",
                            context.units.FirstOrDefault(x => x.id_unit == offer.property.living_space_id_unit)
                                ?.unit_name));
                    xOffer.Add(livingArea);
                }

                trace.AppendLine("5.3");
                if (offer.property.kitchen_space_value.HasValue)
                {
                    var kitchenArea = new XElement("kitchen-space",
                        new XElement("value", offer.property.kitchen_space_value),
                        new XElement("unit",
                            context.units.FirstOrDefault(x => x.id_unit == offer.property.kitchen_space_id_unit)
                                ?.unit_name));
                    xOffer.Add(kitchenArea);
                }

                trace.AppendLine("5.4");
                if (offer.property.room_space_value.HasValue)
                {
                    var roomArea = new XElement("room-space",
                        new XElement("value", offer.property.room_space_value),
                        new XElement("unit",
                            context.units.FirstOrDefault(x => x.id_unit == offer.property.room_space_id_unit)
                                ?.unit_name));
                    xOffer.Add(roomArea);
                }

                trace.AppendLine("5.5");
                if (offer.property.lot_area_value.HasValue)
                {
                    var roomArea = new XElement("lot-area",
                        new XElement("value", offer.property.lot_area_value),
                        new XElement("unit",
                            context.units.FirstOrDefault(x => x.id_unit == offer.property.lot_area_id_unit)
                                ?.unit_name));
                    xOffer.Add(roomArea);
                }

                #endregion
                trace.AppendLine("6");

                #region

                if (offer.property.rooms.HasValue)
                    xOffer.Add(new XElement("rooms", offer.property.rooms.Value));
                if (offer.property.rooms_offered.HasValue)
                    xOffer.Add(new XElement("rooms-offered", offer.property.rooms_offered.Value));
                if (offer.property.floor.HasValue)
                    xOffer.Add(new XElement("floor", offer.property.floor.Value));
                if (offer.property.apartments.HasValue)
                    xOffer.Add(new XElement("apartments", offer.property.apartments.Value));
                if (offer.property.studio.HasValue)
                    xOffer.Add(new XElement("studio", offer.property.studio.Value));
                if (offer.property.open_plan.HasValue)
                    xOffer.Add(new XElement("open-plan", offer.property.open_plan.Value));
                if (offer.property.id_rooms_type.HasValue)
                    xOffer.Add(new XElement("rooms-type", offer.property.rooms_type.rooms_type_name));
                if (offer.property.id_window_view_type.HasValue)
                    xOffer.Add(new XElement("window-view", offer.property.window_view_type.window_view_type_name));
                if (offer.property.id_balcony_type.HasValue)
                    xOffer.Add(new XElement("balcony", offer.property.balcony_type.balcony_type_name));
                if (offer.property.id_bathroom_unit.HasValue)
                    xOffer.Add(new XElement("bathroom-unit", offer.property.bathroom_unit.bathroom_unit_name));
                //
                trace.AppendLine("7");
                if (offer.property.phone.HasValue)
                    xOffer.Add(new XElement("phone", offer.property.phone.Value));
                if (offer.property.internet.HasValue)
                    xOffer.Add(new XElement("internet", offer.property.internet.Value));
                if (offer.property.room_furniture.HasValue)
                    xOffer.Add(new XElement("room-furniture", offer.property.room_furniture.Value));
                else if(offer.property.id_furniture_fill.HasValue && offer.property.id_furniture_fill.Value == (long)FurnitureFillType.Full)
                    xOffer.Add(new XElement("room-furniture", 1));
                if (offer.property.kitchen_furniture.HasValue)
                    xOffer.Add(new XElement("kitchen-furniture", offer.property.kitchen_furniture.Value));
                if (offer.property.television.HasValue)
                    xOffer.Add(new XElement("television", offer.property.television.Value));
                if (offer.property.washing_machine.HasValue)
                    xOffer.Add(new XElement("washing-machine", offer.property.washing_machine.Value));
                if (offer.property.dishwasher.HasValue)
                    xOffer.Add(new XElement("dishwasher", offer.property.dishwasher.Value));
                if (offer.property.refrigerator.HasValue)
                    xOffer.Add(new XElement("refrigerator", offer.property.refrigerator.Value));
                if (offer.property.built_in_tech.HasValue)
                    xOffer.Add(new XElement("built-in-tech", offer.property.built_in_tech.Value));
                if (offer.property.id_floor_covering_type.HasValue)
                    xOffer.Add(new XElement("floor-covering", offer.property.floor_covering_type.floor_covering_type_name));
                if (offer.property.with_children.HasValue)
                    xOffer.Add(new XElement("with-children", offer.property.with_children.Value));
                if (offer.property.with_pets.HasValue)
                    xOffer.Add(new XElement("with-pets", offer.property.with_pets.Value));
                //
                trace.AppendLine("8");
                if (offer.property.building.floors_total.HasValue)
                    xOffer.Add(new XElement("floors-total", offer.property.building.floors_total.Value));
                if (offer.property.building.id_building_type.HasValue)
                {
                    var material = offer.property.building.building_type.building_type_name;
                   /* if (offer.property.building.id_building_type == (long) BuildingMaterialType.КаркасноНасыпной)
                    {
                        material = context.building_type
                            .FirstOrDefault(x => x.id_building_type == (long) BuildingMaterialType.Блочный)
                            ?.building_type_name;
                    }*/
                    if(!string.IsNullOrEmpty(material))
                        xOffer.Add(new XElement("building-type", material));
                }
                trace.AppendLine("9");
                if (offer.property.building.built_year.HasValue)
                    xOffer.Add(new XElement("built-year", offer.property.building.built_year.Value));
                if (!string.IsNullOrEmpty(offer.property.building.building_section))
                    xOffer.Add(new XElement("building-section", offer.property.building.building_section));
                if (offer.property.ceiling_height.HasValue)
                    xOffer.Add(new XElement("ceiling-height", offer.property.ceiling_height.Value));
                if (offer.property.building.guarded_building.HasValue)
                    xOffer.Add(new XElement("guarded-building", offer.property.building.guarded_building.Value));
                if (offer.property.pmg.HasValue)
                    xOffer.Add(new XElement("pmg", offer.property.pmg.Value));
                if (offer.property.access_control_system.HasValue)
                    xOffer.Add(new XElement("access-control-system", offer.property.access_control_system.Value));
                if (offer.property.building.lift.HasValue)
                    xOffer.Add(new XElement("lift", offer.property.building.lift.Value));
                if (offer.property.rubbish_chute.HasValue)
                    xOffer.Add(new XElement("rubbish-chute", offer.property.rubbish_chute.Value));
                if (offer.property.electricity_supply.HasValue)
                    xOffer.Add(new XElement("electricity-supply", offer.property.electricity_supply.Value));
                if (offer.property.water_supply.HasValue)
                    xOffer.Add(new XElement("water-supply", offer.property.water_supply.Value));
                if (offer.property.gas_supply.HasValue)
                    xOffer.Add(new XElement("gas-supply", offer.property.gas_supply.Value));
                if (offer.property.sewerage_supply.HasValue)
                    xOffer.Add(new XElement("sewerage-supply", offer.property.sewerage_supply.Value));
                if (offer.property.heating_supply.HasValue)
                    xOffer.Add(new XElement("heating-supply", offer.property.heating_supply.Value));
                if (offer.property.toilet.HasValue)
                    xOffer.Add(new XElement("toilet", offer.property.toilet.Value));
                if (offer.property.shower.HasValue)
                    xOffer.Add(new XElement("shower", offer.property.shower.Value));
                if (offer.property.pool.HasValue)
                    xOffer.Add(new XElement("pool", offer.property.pool.Value));
                if (offer.property.billiard.HasValue)
                    xOffer.Add(new XElement("billiard", offer.property.billiard.Value));
                if (offer.property.sauna.HasValue)
                    xOffer.Add(new XElement("sauna", offer.property.sauna.Value));
                if (offer.property.building.parking.HasValue)
                    xOffer.Add(new XElement("parking", offer.property.building.parking.Value));
                if (offer.property.building.parking_places.HasValue)
                    xOffer.Add(new XElement("parking-places", offer.property.building.parking_places.Value));
                if (offer.property.building.parking_place_price.HasValue)
                    xOffer.Add(new XElement("parking-place-price", offer.property.building.parking_place_price.Value));
                if (offer.property.building.parking_guest.HasValue)
                    xOffer.Add(new XElement("parking-guest", offer.property.building.parking_guest.Value));
                if (offer.property.building.parking_guest_places.HasValue)
                    xOffer.Add(new XElement("parking-guest-places", offer.property.building.parking_guest_places.Value));
                if (offer.property.building.alarm.HasValue)
                    xOffer.Add(new XElement("alarm", offer.property.building.alarm.Value));
                if (offer.property.flat_alarm.HasValue)
                    xOffer.Add(new XElement("flat-alarm", offer.property.flat_alarm.Value));
                if (offer.property.new_flat.HasValue)
                    xOffer.Add(new XElement("new-flat", offer.property.new_flat.Value));
                if (offer.property.building.security.HasValue)
                    xOffer.Add(new XElement("security", offer.property.building.security.Value));
                if (offer.property.building.is_elite.HasValue)
                    xOffer.Add(new XElement("is-elite", offer.property.building.is_elite.Value));
                //
                trace.AppendLine("10");
                if (offer.property.building.id_building_state.HasValue)
                    xOffer.Add(new XElement("building-state", offer.property.building.building_state.building_state_name));
                if (offer.property.building.id_ready_quarter.HasValue)
                    xOffer.Add(new XElement("ready-quarter", offer.property.building.ready_quarter.ready_quarter_name));
                if (offer.property.building.id_building_phase.HasValue)
                    xOffer.Add(new XElement("building-phase", offer.property.building.building_phase.building_phase_name));
                if (!string.IsNullOrEmpty(offer.property.building.building_series))
                    xOffer.Add(new XElement("building-series", offer.property.building.building_series));
                if (!string.IsNullOrEmpty(offer.property.building.building_section))
                    xOffer.Add(new XElement("building-section", offer.property.building.building_section));
                if (offer.property.building.alarm.HasValue)
                    xOffer.Add(new XElement("alarm", offer.property.building.alarm.Value));
                #endregion
                trace.AppendLine("11");

                #region sales agent

                var salesAgent = new XElement("sales-agent");
                salesAgent.Add(new XElement("name", offer.sales_agent.sales_agent_name));
                salesAgent.Add(new XElement("phone", phone.phone_number));
                salesAgent.Add(new XElement("category",
                    offer.sales_agent.sales_agent_category.sales_agent_category_name));
                salesAgent.Add(new XElement("organization",
                    offer.sales_agent.sales_agent_organization1.sales_agent_organization_name));
                if (!string.IsNullOrEmpty(offer.sales_agent.sales_agent_photo))
                    salesAgent.Add(new XElement("photo", offer.sales_agent.sales_agent_photo));
                if (!string.IsNullOrEmpty(offer.sales_agent.sales_agent_email))
                    salesAgent.Add(new XElement("email", offer.sales_agent.sales_agent_email));
                xOffer.Add(salesAgent);

                #endregion
                trace.AppendLine("12");

                #region location

                var location = new XElement("location");
                location.Add(new XElement("country", offer.property.building.building_location.country));
                if (!string.IsNullOrEmpty(offer.property.building.building_location.region))
                    location.Add(new XElement("region", offer.property.building.building_location.region));
                if(!string.IsNullOrEmpty(offer.property.building.building_location.district))
                    location.Add(new XElement("region", offer.property.building.building_location.region));
                if (!string.IsNullOrEmpty(offer.property.building.building_location.locality_name))
                    location.Add(new XElement("locality-name", offer.property.building.building_location.locality_name));
                if (!string.IsNullOrEmpty(offer.property.building.building_location.address))
                    location.Add(new XElement("address", offer.property.building.building_location.address));
                location.Add(new XElement("latitude", offer.property.building.building_location.latitude));
                location.Add(new XElement("longitude", offer.property.building.building_location.longitude));
                xOffer.Add(location);

                #endregion
                trace.AppendLine("13");

                return xOffer;
            }
            catch (ValidationException vex)
            {
                Log.Error(vex.ToString());
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "[FeedManager::CreateXmlElementYandex] offerId "+ offer.id_offer+"\nTrace: "+ trace.ToString());
            }

            return null;
        }
    }
}

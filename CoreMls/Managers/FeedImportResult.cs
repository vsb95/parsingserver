﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreMls.DB.Model;

namespace CoreMls.Managers
{
    public class FeedImportResult
    {
        public int CountAdded { get; set; }
        public int CountUpdated { get; set; }
        public int CountDeleted { get; set; }

        public List<offer> Offers { get; } = new List<offer>();

        public List<string> Errors { get; } = new List<string>();
        public List<string> Messages { get; } = new List<string>();

        public override string ToString()
        {
            return "Распознано " + Offers.Count + " объявлений; " + Errors.Count + " ошибок\nДобавлено новых: " + CountAdded + "\nОбновлено: " + CountUpdated;
            //+"\nУдалено: "+CountDeleted;
        }

        /// <summary>
        /// Подготовить отчет по загрузке
        /// </summary>
        /// <param name="src">Ссылка на источник импорта (xml-файл)</param>
        /// <returns></returns>
        public string ToHtml(string src)
        {
            var builder = new StringBuilder();
            builder.AppendLine("Отчет по загрузке фида '" + src + "' от " + DateTime.Now.ToString("g"));
            builder.AppendLine(ToString());
#if DEBUG
            if (Messages.Count > 0)
            {
                builder.AppendLine();
                builder.AppendLine();
                builder.AppendLine("Сообщения: ");
                foreach (var message in Messages)
                {
                    builder.AppendLine(message);
                }
            }
#endif
            if (Errors.Count > 0)
            {
                builder.AppendLine();
                builder.AppendLine();
                builder.AppendLine("Ошибки: ");
                foreach (var error in Errors)
                {
                    builder.AppendLine(error);
                }

                builder.AppendLine(
                    "<br>Проверьте соответствие с <a href=\"https://yandex.ru/support/realty/requirements/requirements-sale-housing.html\">требованиями к XML-фиду Яндекса</a>");
            }

            return builder.ToString().Replace("\r\n", "<br/>").Replace("\n", "<br/>");
        }
    }
}

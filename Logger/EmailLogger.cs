﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Logger
{
    internal class EmailLogger
    {
        private readonly ConcurrentStack<EmailMessageRequest> _messagesStack = new ConcurrentStack<EmailMessageRequest>();

        public EmailLogger(int minCountMsg, TimeSpan minTimeToSend)
        {
            var minTimeToSend1 = new TimeSpan(0, 5, 0);
            var minCountMsg1 = 50;
            var task = Task.Factory.StartNew(() =>
            {
                try
                {
                    while (true)
                    {
                        // Ждем устанволенный дилэй
                        Thread.Sleep((int)minTimeToSend1.TotalMilliseconds);

                        List<EmailMessageRequest> notFatalRequests = new List<EmailMessageRequest>();
                        StringBuilder fatalMessages = new StringBuilder();
                        int fatalMessagesCount = 0;
                        while (_messagesStack.TryPop(out var request))
                        {
                            if (request.IsFatal)
                            {
                                fatalMessagesCount++;
                                fatalMessages.AppendLine(request.Message);
                            }
                            else
                            {
                                notFatalRequests.Add(request);
                            }
                        }

                        // Если есть фатальные - тут же отправляем
                        if (fatalMessages.Length > 0)
                        {
                            SendMail("smtp.yandex.ru", "mr.workbox@ya.ru", "M2zsJGMYWk", "vsb95@mail.ru", Log.ProjectTitle + " FATAL", "Всего сообщений: "+ fatalMessagesCount + "\n\n" + fatalMessages.ToString());
                        }

                        // Если нефатальных ошибок набралось меньше требуемого - вернем обратно в стек
                        if (notFatalRequests.Count >= minCountMsg1)
                        {
                            StringBuilder notFatalMessages = new StringBuilder();
                            foreach (var notFatalRequest in notFatalRequests)
                            {
                                notFatalMessages.AppendLine(notFatalRequest.Message);
                            }
                            SendMail("smtp.yandex.ru", "mr.workbox@ya.ru", "M2zsJGMYWk", "vsb95@mail.ru", Log.ProjectTitle, "Всего сообщений: " + notFatalRequests.Count + "\n\n" + notFatalMessages.ToString());
                        }
                        else if(notFatalRequests.Count > 0)
                        {
                            _messagesStack.PushRange(notFatalRequests.ToArray());
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, LogType.Fatal, true, "Отвалился метод отправки писем на почту");
                }
            });
        }

        public void SendMessage(string message, bool isFatal = false)
        {
#if DEBUG
            return;
#endif
            _messagesStack.Push(new EmailMessageRequest(message, isFatal));
        }

        /// <summary>
        /// Отправка письма на почтовый ящик C# mail send
        /// </summary>
        /// <param name="smtpServer">Имя SMTP-сервера</param>
        /// <param name="from">Адрес отправителя</param>
        /// <param name="password">пароль к почтовому ящику отправителя</param>
        /// <param name="mailto">Адрес получателя</param>
        /// <param name="caption">Тема письма</param>
        /// <param name="message">Сообщение</param>
        /// <param name="attachFile">Присоединенный файл</param>
        private static void SendMail(string smtpServer, string from, string password,
            string mailto, string caption, string message, string attachFile = null)
        {
#if DEBUG
            Log.WriteInConsole("Попытка отправить письмо: " + message, LogType.Debug);
            return;
#endif
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(from);
                    mail.To.Add(new MailAddress(mailto));
                    mail.Subject = caption;
                    mail.Body = message;
                    if (!string.IsNullOrEmpty(attachFile))
                        mail.Attachments.Add(new Attachment(attachFile));
                    using (SmtpClient client = new SmtpClient
                    {
                        Host = smtpServer,
                        Port = 587,
                        EnableSsl = true,
                        Credentials = new NetworkCredential(@from.Split('@')[0], password),
                        DeliveryMethod = SmtpDeliveryMethod.Network
                    })
                    {
                        client.Send(mail);
                    }
                }
            }
            catch (Exception e)
            {
                string msg =
                    "<Log::SendMail> " + e.Message + "\n" + " smtpServer =" + smtpServer + " from =" + from +
                    " psw =" + password + " mailto =" + mailto + " caption =" + caption + " message =" + message +
                    " attachFile =" + attachFile;
                Log.Message(msg, LogType.Error);
            }
        }
    }

    internal class EmailMessageRequest
    {
        public bool IsFatal { get; set; }
        public string Message { get; set; }
        public DateTime CreateDt { get; private set; } = DateTime.Now;

        public EmailMessageRequest(string msg, bool isFatal = false)
        {
            Message = msg;
            IsFatal = isFatal;
        }
    }
}

﻿using System;
using System.Collections.Specialized;
using System.Net;
using System.Text;

namespace Logger
{
    internal class SlackLogger
    {
        private readonly int _minCountMsg = 1;
        private readonly TimeSpan _minTimeToSend;
        private DateTime _lastSendTime;
        private int _counterMessage;
        private string _msg;
        private readonly WebProxy _proxy;
        private readonly string _uri;
        private readonly string _token;
        private readonly string _channelId;
        private readonly bool _isOnlyForFatal;

        public SlackLogger(string token, string channelId, int minCountMsg, TimeSpan minTimeToSend, WebProxy proxy = null, bool isOnlyForFatal = true)
        {
            Log.Info("Инициализируем slack-бота...");
            _proxy = proxy;
            _token = token;
            _channelId = channelId;
            _uri = "https://slack.com/api/chat.postMessage?token="+ token+ "&channel="+ channelId;
            _isOnlyForFatal = isOnlyForFatal;
#if !DEBUG
            SendMessage("Запущен: "+Console.Title);
#endif
            _minTimeToSend = minTimeToSend;
            _minCountMsg = minCountMsg;
            _counterMessage = _minCountMsg;
        }
        
        public void SendMessage(string message, bool isFatal = false)
        {
#if DEBUG
            return;
#endif
            try
            {
                if (isFatal)
                {
                    using (WebClient client = new WebClient())
                    {
                        client.Proxy = _proxy;
                        var values = new NameValueCollection();
                        values["text"] = Log.ProjectTitle + "\n" + message;
                        values["token"] = _token;
                        values["channel"] = _channelId;

                        var response = client.UploadValues(_uri, values);
                    }
                }else if(_isOnlyForFatal)
                    return;
                _msg += (string.IsNullOrEmpty(_msg)? "":"\n\n") + message;
                if (_counterMessage > 0 || DateTime.Now - _lastSendTime < _minTimeToSend)
                {
                    _counterMessage--;
                    return;
                }

                using (WebClient client = new WebClient())
                {
                    client.Proxy = _proxy;
                    var values = new NameValueCollection();
                    values["text"] = Log.ProjectTitle + "\n"+_msg;
                    values["token"] = _token;
                    values["channel"] = _channelId;

                    var response = client.UploadValues(_uri, values);
                }
;
                _counterMessage = _minCountMsg;
                _lastSendTime = DateTime.Now;
                _msg = null;
            }
            catch (Exception e)
            {
                Log.Exception(e, LogType.Error);
            }
        }
        
    }
}

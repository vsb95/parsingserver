﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Text;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;

namespace Logger
{
    internal class TelegramLogger
    {
        private readonly TelegramBotClient _bot;
        private int MinCountMsg = 1;
        private TimeSpan _minTimeToSend;
        private DateTime _lastSendTime;
        private int _counterMessage;
        private string _msg;
        public TelegramLogger(string token, int minCountMsg, TimeSpan minTimeToSend, WebProxy proxy = null)
        {
            if (string.IsNullOrEmpty(token))
                token = "600880424:AAGSw6JDlDlDLLNAFdVgWO78k5PSUMpiXE0";
            _minTimeToSend = minTimeToSend;
            MinCountMsg = minCountMsg;
            _counterMessage = MinCountMsg;
            try
            {
                Log.Info("Инициализируем телеграмбота");
                _bot = new TelegramBotClient(token, proxy);
                var me = _bot.GetMeAsync().Result;
                Log.Info("Телеграм-оповещение запущено: " + me.Username);
            }
            catch (Exception ex)
            {
                Log.Error(
                    "Не удалось инициализировать оповещение в телеграм; " +
                    (proxy == null ? "Прокси не используется" : "Прокси: " + proxy.Address), false);
                //Log.Exception(ex, LogType.Error);
            }
        }

        public void SendMessage(string message, bool isFatal = false)
        {
            var chatId = "328562457"; //имя канала
#if DEBUG
            return;
#endif
            try
            {
                if (isFatal)
                {
                    _bot.SendTextMessageAsync(chatId, "```" + message + "```", ParseMode.Markdown);
                }

                _msg += (string.IsNullOrEmpty(_msg) ? "" : "\n\n") + message;
                if (_counterMessage > 0 || DateTime.Now - _lastSendTime < _minTimeToSend)
                {
                    _counterMessage--;
                    return;
                }

                var task = _bot.SendTextMessageAsync(chatId, "```"+ _msg + "```", ParseMode.Markdown);
                _counterMessage = MinCountMsg;
                _lastSendTime=DateTime.Now;
                _msg = null;
                //Console.WriteLine(string.Format("Message id = {0}, chat id = {1}", msg.MessageId, msg.Chat.Id));
            }
            catch (Exception e)
            {
                Log.Exception(e, LogType.Error);
            }
        }
    }
}

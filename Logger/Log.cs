﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Logger
{
    public enum LogType
    {
        Trace, Debug, Info, Warn, Error, Fatal
    }

    public static class Log // : ILogger
    {
        public static string ProjectTitle { get; private set; }
        private static int _minCountMsg;
        private static TimeSpan _minTimeToSend;

        private static EmailLogger _emailLogger;
        private static TelegramLogger _telegramLog;
        private static SlackLogger _slackLogger;
        private static Version _version = Assembly.GetExecutingAssembly().GetName().Version;
        private static readonly NLog.Logger CurrentLogger;
        private static bool _isEnabledConsoleTrace = true;
        static Log()
        {
            if (!Directory.Exists("Logs"))
                Directory.CreateDirectory("Logs");
            else
            {
                // Если папка уже была, то надо почистить старые логи, перед тем как записывать новые
                // старые - старее чем вчера
                foreach (string file in Directory.GetFiles("Logs"))
                {
                    var fileInfo = new FileInfo(file);
                    if((DateTime.Now - fileInfo.LastWriteTime).TotalDays > 5)
                        fileInfo.Delete();
                }
            }
            CurrentLogger = NLog.LogManager.GetCurrentClassLogger();
        }

        public static void Init(Version version, int minCountMsg, int minSecToSend, bool isEnabledConsoleTrace, string projectTitle = null)
        {
            if(version != null)
                _version = version;
            _minCountMsg = minCountMsg;
            _minTimeToSend = new TimeSpan(0, 0, minSecToSend);
            _emailLogger= new EmailLogger(_minCountMsg, _minTimeToSend);
            _isEnabledConsoleTrace = isEnabledConsoleTrace;
            ProjectTitle = string.IsNullOrEmpty(projectTitle) ? Console.Title : projectTitle;
        }

        public static async void InitTelegramLogger(string apiToken,WebProxy proxy = null)
        {
            var task = Task.Factory.StartNew(() => new TelegramLogger(apiToken, _minCountMsg, _minTimeToSend, proxy));
            _telegramLog = await task;
        }

        public static async void InitSlackLogger(string apiToken, string channel, bool isOnlyForFatal = true, WebProxy proxy = null)
        {
            var task = Task.Factory.StartNew(() => new SlackLogger(apiToken, channel, _minCountMsg, _minTimeToSend, proxy, isOnlyForFatal));
            _slackLogger = await task;
        }

        public static void Trace(string message)
        {
            Message(message, LogType.Trace);
        }

        public static void Debug(string message)
        {
            Message(message,LogType.Debug);
        }

        public static void Info(string message)
        {
            Message(message, LogType.Info);
        }

        public static void Warn(string message)
        {
            Message(message, LogType.Warn);
        }

        public static void Error(string message, bool sendMail = false)
        {
            Message(message, LogType.Error, sendMail);
        }

        public static void Fatal(string message)
        {
            Message(message, LogType.Fatal, true);
        }

        public static void Exception(Exception exception, LogType type = LogType.Warn, bool sendMail = false, string AddMessage = null)
        {
            if(exception == null)
                return;
            if (exception is AggregateException ae)
            {
                ae.Handle((x) =>
                {
                    Exception(x, type, true);
                    return false;
                });
                return;
            }
            if (exception.InnerException != null)
            {
                Exception(exception.InnerException, type, sendMail, $"BASE EXCEPTION\n\t[{exception.GetType()}\t{exception.Message}]\n\t{exception.StackTrace}" + (string.IsNullOrEmpty(AddMessage) ? "" : "\n\n\tДоп.информация: " + AddMessage));
            }
            else
                Message($"EXCEPTION\n[{exception}" + (string.IsNullOrEmpty(AddMessage) ? "" : "\n\nДоп.информация: " + AddMessage), type, sendMail);

            //$"EXCEPTION\n[{exception.GetType()}\t{exception.Message}]\n{exception.StackTrace}"+(string.IsNullOrEmpty(AddMessage)?"":"\n\nДоп.информация: "+ AddMessage), type, sendMail);
        }

        public static void PrintStackTrace(int limit = 10)
        {
            Message(GetStackTrace(limit), LogType.Debug);
        }
        
        public static void Message(string message, LogType type, bool sendMail = false)
        {
            switch (type)
            {
                case LogType.Trace:
                    CurrentLogger.Trace(message);
                    break;
                case LogType.Debug:
                    CurrentLogger.Debug(message);
                    break;
                case LogType.Info:
                    CurrentLogger.Info(message);
                    break;
                case LogType.Warn:
                    CurrentLogger.Warn(message);
                    break;
                case LogType.Error:
                    CurrentLogger.Error(message);
                    break;
                case LogType.Fatal:
                    CurrentLogger.Fatal(message);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }

            message = "[" + DateTime.Now.ToString("HH:mm:ss.ff") + " " + type.ToString().ToUpper() +
                      (string.IsNullOrEmpty(Thread.CurrentThread.Name)
                          ? "-" + Thread.CurrentThread.ManagedThreadId
                          : " (" + Thread.CurrentThread.Name + ")") +
                      "] " + message;
            if (sendMail)
                CallMeMaybe(message, type == LogType.Fatal);
            if(type != LogType.Trace || _isEnabledConsoleTrace)
                WriteInConsole(message, type);
        }

        public static void WriteInConsole(string message, LogType type)
        {
            var oldColor = Console.ForegroundColor;
            switch (type)
            {
                case LogType.Trace:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case LogType.Debug:
                    Console.ForegroundColor = ConsoleColor.Gray;
                    break;
                case LogType.Info:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case LogType.Warn:
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    break;
                case LogType.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case LogType.Fatal:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }

            Console.WriteLine(message);
            Console.ForegroundColor = oldColor;
        }


        /// <summary>
        ///  Получить стек, исключая вызывающего и этот метод
        /// </summary>
        /// <param name="limit"></param>
        /// <returns></returns>
        private static string GetStackTrace(int limit = 10)
        {
            StackTrace stackTrace = new StackTrace(); // get call stack 
            StackFrame[] stackFrames = stackTrace.GetFrames(); // get method calls (frames) 
            string str = null;
            if (stackFrames != null)
            {
                for (int index = 2; index < stackFrames.Length && index < limit+2; index++)
                {
                    StackFrame stackFrame = stackFrames[index];
                    str += "\n\t-> " + stackFrame.GetMethod().Name;
                }
            }
            return str;
        }

        /// <summary>
        /// Оповестить разработчика на почту\смс\еще канибудь
        /// </summary>
        /// <seealso cref="https://www.youtube.com/watch?v=fWNaR-rxAic"/>
        /// <param name="message"></param>
        private static void CallMeMaybe(string message, bool isFatal = false)
        {
            if (string.IsNullOrEmpty(message))
            {
                Info("Пустое Email сообщение");
                return;
            }

            _emailLogger?.SendMessage(message, isFatal);
            _telegramLog?.SendMessage(message, isFatal);
            _slackLogger?.SendMessage(message, isFatal);
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Core;
using CoreMls.DB.LocalEntities;
using CoreParser.Pages;
using Logger;

namespace ApiService.Core
{
    public static class AdvertismentPreparer
    {

        private static readonly List<Regex> BadRegexList = new List<Regex>
        {
            new Regex(@"(аген(т|)с(т|)во\sнедвижимости|(\,|\.|\s|^)(АН|А\.Н\.)|компания)\s(\w+|"".{0,30}""|«.{0,30}»)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"("".+""|«.+»)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"\w+\s(сдаст|представляет)", RegexOptions.Compiled | RegexOptions.IgnoreCase),
            new Regex(@"(аген(т|)с(т|)в(а|ам|о)|аген(т|)с(т|)в(а|ам|о)\sнедвижимости)", RegexOptions.Compiled | RegexOptions.IgnoreCase),

            new Regex(@"/(\+7|8)[- _]*\(?[- _]*(\d{3}[- _]*\)?([- _]*\d){7}|\d\d[- _]*\d\d[- _]*\)?([- _]*\d){6})/g",
                RegexOptions.Compiled) // телефон
        };

        private static readonly List<string> OwnersWord = new List<string>
        {
            @"(\,|\.|\s|)(от\s|я\s|)собствен(н|)и((\w?){1,3}|ками)(\.|\,|)",
            @"хозя(ин.?|йк.?|ева)",
            @"выгодные скидки по комиссии",
            @"агентства недвижимости просьба не беспокоить(\.|\,|)",
            @"\w+\s(не беспокоить|не звонить)(\.|\,|)",
            @"(бесплатно|без комиссии|без первоначального взноса|скидк(и|а)|напрямую|акция)(\.|\,|)",
            @"услуги аген.?ств.? не интересуют(\.|\,|)",
            @"без посредник(ов|а)(\.|\,|)",
            @"в услугах не нуждаюсь(\.|\,|)",
            @"сдается собственником(\.|\,|)",
            @"я владелец",
            @"комис(с|)ии нет",
            @"не аген(т|)с(т|)во(\.|\,|)"
        };

        /// <summary>
        /// Почистит описание по ключевикам, удалит номера телефонов, отдаст нормальные ссылки на изображения
        /// </summary>
        /// <param name="adv"></param>
        /// <param name="isClearingDescription">TRUE - будут чистится описание от собственников и АН</param>
        public static void Prepare(AbstractAdvertisement adv, bool isClearingDescription)
        {
            if (adv == null)
                return;
            try
            {

#if DEBUG
                var origin = adv.Description;
#endif
                if (isClearingDescription && !string.IsNullOrEmpty(adv.Description))
                {
                    adv.Description = adv.Description.Replace("&quot;", "\"");
                    // Очищаем от слова "Собственник" и тп
                    foreach (var pattern in OwnersWord)
                    {
                        adv.Description = Regex.Replace(adv.Description, pattern, "",
                            RegexOptions.IgnoreCase | RegexOptions.Multiline);
                    }

                    foreach (var regex in BadRegexList)
                    {
                        adv.Description = regex.Replace(adv.Description, "");
                    }
                }

#if DEBUG
                adv.Description = origin + "<br>\n<br>=============================\n<br>" + adv.Description;
#endif

                if (adv.DownloadedImagesPathList == null || adv.DownloadedImagesPathList.Count == 0)
                    return;
                for (var i = adv.DownloadedImagesPathList.Count - 1; i >= 0; i--)
                {
                    var originPath = adv.DownloadedImagesPathList[i];
                    var path = adv.DownloadedImagesPathList[i].ToLower();
                    if (path.Contains("http") || !File.Exists(originPath))
                        break;
                    if (!path.Contains(".jpg") && !path.Contains(".png") && !path.Contains(".gif") 
                        && !path.Contains(".webp") && !path.Contains(".svg"))
                    {
                        adv.DownloadedImagesPathList.RemoveAt(i);
                        continue;
                    }

                    adv.DownloadedImagesPathList[i] = RemovePathSymbols(originPath);
                }
            }
            catch (Exception e)
            {
                Log.Exception(e, LogType.Error, true, "AdvertismentPreparer::Prepare " + adv.Url);
            }
        }

        public static void Prepare(MlsAdvertisment adv, bool isClearingDescription)
        {
            if (adv?.Property == null)
                return;
            try
            {

                /*
#if DEBUG
                var origin = adv.Property.Description;
#endif
                if (isClearingDescription && !string.IsNullOrEmpty(adv.Property.Description))
                {
                    adv.Property.Description = adv.Property.Description.Replace("&quot;", "\"");
                    // Очищаем от слова "Собственник" и тп
                    foreach (var pattern in OwnersWord)
                    {
                        adv.Property.Description = Regex.Replace(adv.Property.Description, pattern, "",
                            RegexOptions.IgnoreCase | RegexOptions.Multiline);
                    }

                    foreach (var regex in BadRegexList)
                    {
                        adv.Property.Description = regex.Replace(adv.Property.Description, "");
                    }
                }

#if DEBUG
                adv.Property.Description = origin + "<br>\n<br>=============================\n<br>" + adv.Property.Description;
#endif
                */
                adv.Property.Description = adv.Property.Description?.Replace("\n", "<br>");
            }
            catch (Exception e)
            {
                Log.Exception(e, LogType.Error, true, "AdvertismentPreparer::PrepareMLS " + adv.Id);
            }
        }

        /// <summary>
        /// Удалить из ура изображения локальный путь и оставить только относительный сервера
        /// </summary>
        /// <param name="imagePath"></param>
        /// <returns></returns>
        public static string RemovePathSymbols(string imagePath)
        {
            try
            {
                var indexOf = imagePath.IndexOf("assets", StringComparison.Ordinal);
                if (indexOf == -1)
                    return imagePath;
                var result = imagePath.Substring(indexOf);
                if (!result.StartsWith("/"))
                    result = "/" + result;
                return result;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "PageController::RemovePathSymbols");
            }

            var result2 = imagePath.Replace(SettingsManager.Settings.ServerSettings.ImageFolder, "");

            if (!result2.StartsWith("/"))
                result2 = "/" + result2;
            return result2;
        }
    }
}

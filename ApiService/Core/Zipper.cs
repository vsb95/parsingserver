﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Core;
using Core.Services;
using CoreParser.Pages;
using Ionic.Zip;
using Logger;

namespace ApiService.Core
{
    public static class Zipper
    {
        public static string CreateZip(ConcurrentDictionary<string, List<string>> imagesDictionary)
        {
            string lastStreet = null;
            try
            {
                if (!Directory.Exists(SettingsManager.Settings.ServerSettings.ZipFolder))
                    Directory.CreateDirectory(SettingsManager.Settings.ServerSettings.ZipFolder);
                //var dir = Randomizer.CreateRandomFolder(Settings.ServerSettings.ZipFolder);
                var zipFileName = "Фото из отчета от " + DateTime.Now.ToShortDateString() + "_" +
                                  Randomizer.CreateRandomString(8) + ".zip";
                while (File.Exists(SettingsManager.Settings.ServerSettings.ZipFolder + "\\" + zipFileName))
                {
                    zipFileName = "Фото из отчета от " + DateTime.Now.ToShortDateString() + "_" +
                                  Randomizer.CreateRandomString(8) + ".zip";
                }

                var streets = new List<string>();
                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncoding = Encoding.UTF8;
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                    zip.ZipErrorAction = ZipErrorAction.Skip;
                    foreach (var page in imagesDictionary)
                    {
                        // отсотрируем по длине, тк главное не взять папку с png или оригиналами
                        var imagePath = page.Value?.OrderBy(x => x.Length).FirstOrDefault();
                        if (string.IsNullOrEmpty(imagePath)) continue;
                        var fileInfo = new FileInfo(imagePath);
                        lastStreet = page.Key;
                        if (string.IsNullOrEmpty(lastStreet)) continue;
                        int i = 1;
                        while (streets.Contains(lastStreet))
                        {
                            i++;
                            lastStreet = page.Key+" " + i;
                        }
                        streets.Add(lastStreet);
                        zip.AddDirectory(fileInfo.DirectoryName, lastStreet);
                    }

                    zip.Save(Path.Combine(SettingsManager.Settings.ServerSettings.ZipFolder, zipFileName));
                }

                return zipFileName;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка при формировании архива: " + String.Join("\n", imagesDictionary.Keys)+"\nПоследняя добавленная улица: '"+ lastStreet+"'");
                Log.Exception(ex, LogType.Fatal, true);
            }

            return null;
        }

        public static string CreateZip(IEnumerable<AbstractAdvertisement> pages)
        {
            try
            {
                if (!Directory.Exists(SettingsManager.Settings.ServerSettings.ZipFolder))
                    Directory.CreateDirectory(SettingsManager.Settings.ServerSettings.ZipFolder);
                //var dir = Randomizer.CreateRandomFolder(Settings.ServerSettings.ZipFolder);
                var zipFileName = "Фото из отчета от " + DateTime.Now.ToShortDateString() + "_" +
                                  Randomizer.CreateRandomString(8) + ".zip";
                while (File.Exists(SettingsManager.Settings.ServerSettings.ZipFolder + "\\" + zipFileName))
                {
                    zipFileName = "Фото из отчета от " + DateTime.Now.ToShortDateString() + "_" +
                                  Randomizer.CreateRandomString(8) + ".zip";
                }

                var streets = new List<string>();
                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncoding = Encoding.UTF8;
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                    zip.ZipErrorAction = ZipErrorAction.Skip;
                    foreach (var page in pages)
                    {
                        var imagePath = page.DownloadedImagesPathList?.OrderBy(x => x.Length).FirstOrDefault();
                        if (string.IsNullOrEmpty(imagePath)) continue;
                        var fileInfo = new FileInfo(imagePath);
                        var street = page.Adress.Street;
                        int i = 1;
                        while (streets.Contains(street))
                        {
                            i++;
                            street += page.Adress.Street+" " + i;
                        }
                        streets.Add(street);
                        zip.AddDirectory(fileInfo.DirectoryName, street);
                    }
                    zip.Save(SettingsManager.Settings.ServerSettings.ZipFolder + "\\" + zipFileName);
                }

                return zipFileName;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка при формировании архива: "+String.Join("\n",pages.Select(x=>x.Url)));
                Log.Exception(ex, LogType.Fatal, true);
            }

            return null;
        }
    }
}

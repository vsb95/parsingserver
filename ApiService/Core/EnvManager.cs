﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using CoreParser.DB;
using CoreParser.Pages;
using CoreParser.Services.Api;
using Logger;

namespace ApiService.Core
{
    public static class EnvManager
    {
        private static ConcurrentQueue<PageUpdateRequest> _queue = new ConcurrentQueue<PageUpdateRequest>();

        private static Thread _updateThread = new Thread(UpdatePagesLoop);

        static EnvManager()
        {
            _updateThread.Start();
        }

        private static void UpdatePagesLoop()
        {
            while (true)
            {
                PageUpdateRequest request = null;
                try
                {
                    if (!_queue.TryDequeue(out request))
                    {
                        Thread.Sleep(60000);
                        continue;
                    }

                    AbstractAdvertisement adv = null;
                    if (request.IsDeleted)
                    {
                        adv = DbManager.GetAbstractAdvertisementByUrl(request.Url);
                        if (adv != null)
                        {
                            adv.IsDeleted = true;
                            DbManager.AddOrUpdatePage(adv);
                            continue;
                        }
                    }

                    if (!AbstractAdvertisement.TryParse(request.Html, request.Url, out adv))
                    {
                        throw new Exception("Не удалось распарсить страницу: "+request.Url);
                    }
                    // ToDo проверить актуализацию
                    adv.IsMls = request.IsMls;
                    if (request.IsForceIsAgency)
                    {
                        adv.IsForceIsAgency = true;
                        adv.IsAgency = request.IsAgency;
                    }
                    DbManager.AddOrUpdatePage(adv);
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, LogType.Error, true, "EnvManager::UpdatePagesLoop: "+(request?.Url));
                }
            }
        }

        internal static void AddRequest(PageUpdateRequest request)
        {
            _queue.Enqueue(request);
        }
    }
}

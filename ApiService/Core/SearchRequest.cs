﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoreParser.DB;
using CoreParser.DB.LocalEntities;
using Logger;
using CoreEnum = Core.Enum;

namespace ApiService.Core
{
    public class SearchRequest
    {
        public int ItIsForCreatedReport { get; set; } = 0;
        public string DateFrom { get; set; }
        public string DateEnd { get; set; }
        public string DEBUG { get; set; }

        public string IsOnlyWithPhoto { get; set; }
        public string IsAgencyNeeded { get; set; }
        public string IsSelfmanNeeded { get; set; }
        public string IsNotLastFloor { get; set; }
        public List<int> IsWithFurnitures { get; set; }

        public string IdCity { get; set; }
        public double SizeMin { get; set; } = 0;
        public double SizeMax { get; set; } = 0;

        public int MinFloor { get; set; } = 0;
        public int MaxFloor { get; set; } = 0;

        public string CostMin { get; set; }
        public string CostMax { get; set; }

        public string CostType { get; set; } = CoreParser.Enum.CostType.Undefined.ToString();
        public string SearchPhrase { get; set; }
        public string SearchAdress { get; set; }
        public int RentType { get; set; } = -1;
        public List<int> SellTypes { get; set; }

        /// <summary>
        /// Список источников
        /// </summary>
        public List<int> Sources { get; set; } = new List<int>();
        public int IsMls { get; set; }

        /// <summary>
        /// Тип объявления (дача\коттедж)
        /// </summary>
        public string ObjTypeSelect { get; set; }
        public List<int> CountRooms { get; set; }

        /// <summary>
        /// Тип дома при продаже (кирпич\панель\ итд)
        /// </summary>
        public List<int> HouseTypes { get; set; }

        /// <summary>
        /// id-исключения. они не нужны в результатах
        /// </summary>
        public List<int> ExcludeIdList { get; set; }

        /// <summary>
        /// Список районов\округов
        /// </summary>
        public string Districts { get; set; }

        public List<int> DistrictsArr { get; set; }
        /// <summary>
        /// Список микрорайонов
        /// </summary>
        public string MicroDistricts { get; set; }
        public List<int> MicroDistrictsArr { get; set; }

        /// <summary>
        /// Количество на странице
        /// </summary>
        public int Count { get; set; } = 50;

        /// <summary>
        /// № страницы
        /// </summary>
        public int IndexPage { get; set; } = 0;

        /// <summary>
        /// Тип сортировки
        /// </summary>
        public int OrderType { get; set; } = 0;

        public string UserId { get; set; }
        public int IsCutLogo { get; set; }
        public int IsOnlyMerged { get; set; } = 0;

        public SearchFilter ToSearchFilter()
        {
            var filter = new SearchFilter();
            filter.IdCity = int.TryParse(IdCity, out var idcity)
                ? idcity
                : throw new Exception("не понятный Id города: " + IdCity);
            filter.IsAgencyNeeded = IsAgencyNeeded == "1";
            filter.IsSelfmanNeeded = IsSelfmanNeeded == "1";
            filter.IsOnlyWithPhoto = IsOnlyWithPhoto == "1";
            filter.IsNotLastFloor = IsNotLastFloor == "1";
            if (IsWithFurnitures != null && IsWithFurnitures.Count == 1)
            {
                filter.IsWithFurniture = IsWithFurnitures[0] == 1;
            }

            filter.IsMls = IsMls == 1;
            filter.SizeMin = SizeMin;
            filter.SizeMax = SizeMax;
            if (!string.IsNullOrEmpty(CostMin) 
                && decimal.TryParse(CostMin.Replace(" ", ""), out var costMin))
            {
                filter.CostMin = costMin;
            }
            if (!string.IsNullOrEmpty(CostMax) 
                && decimal.TryParse(CostMax.Replace(" ", ""), out var costMax))
            {
                filter.CostMax = costMax;
            }
            filter.MinFloor = MinFloor;
            filter.MaxFloor = MaxFloor;
            filter.Sources = Sources;
            filter.CostType = System.Enum.TryParse(CostType, out CoreParser.Enum.CostType resultCostType) ? resultCostType : CoreParser.Enum.CostType.Undefined;
            
            filter.SearchPhrase = SearchPhrase?.ToLower();
            if(!string.IsNullOrEmpty(SearchAdress))
            {
                SearchAdress = SearchAdress.Trim();
                string lastChunk = null;
                try
                {
                    // Вырезаем город
                    var cityName = DbObjectManager.GetNameCity(filter.IdCity);
                    filter.SearchAdress = SearchAdress.ToLower().Replace(cityName.ToLower(), "").Replace("россия, ","");

                    var chunks = SearchAdress.Split(new[] {","}, StringSplitOptions.RemoveEmptyEntries);
                    lastChunk = chunks.LastOrDefault()?.ToLower().Replace("к", "").Replace("/","")
                        .Replace("п", "").Replace("а", "").Replace("б", "").Replace("в", "").Replace("г", "").Replace("д", "").Replace("е", "").Trim();

                    if (chunks.Length > 2 && !string.IsNullOrEmpty(lastChunk))
                    {
                        // Если не удалось определить номер дома, то скорее всего поиск только по улице
                        if (int.TryParse(lastChunk, out var result))
                        {
                            filter.House = chunks.LastOrDefault()?.Trim();
                            filter.Street = chunks[chunks.Length - 2]?.Trim();
                        }
                        else
                        {
                            filter.Street = chunks.LastOrDefault()?.Trim();
                            if (lastChunk.Length < 10)
                            {
                                var adr = int.Parse(lastChunk); // выкинем исключение, чтобы понять почему не удалось
                            }
                        }
                    }
                    else if (chunks.Length == 1 && chunks.FirstOrDefault() == cityName)
                    {
                        filter.SearchAdress = null;
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, LogType.Error, true, "Parsig filter.SearchAdress; lastChunk = '"+ lastChunk+"'");
                }
            }
            filter.SellTypes = SellTypes;
            filter.RentType = RentType;
            filter.OrderType = OrderType;
            filter.IsOnlyMerged = IsOnlyMerged != 0;
            if (filter.RentType == (int)CoreParser.Enum.RentType.Sell && ObjTypeSelect != "4") // если это не дачи\участки
            {
                filter.HouseMaterials = HouseTypes;
            }
            filter.ExcludeIdList = ExcludeIdList;

            var objTypes = new List<int>();
            switch (ObjTypeSelect)
            {
                // Квартиры
                case "0":
                    if (CountRooms == null || CountRooms.Count == 0)
                    {
                        var countRoomId = new ObjType(CoreParser.Enum.EnumObjType.Kvartira1).Id;
                        objTypes.Add(countRoomId);
                        countRoomId = new ObjType(CoreParser.Enum.EnumObjType.Studia).Id;
                        objTypes.Add(countRoomId);
                        countRoomId = new ObjType(CoreParser.Enum.EnumObjType.Kvartira2).Id;
                        objTypes.Add(countRoomId);
                        countRoomId = new ObjType(CoreParser.Enum.EnumObjType.Kvartira3).Id;
                        objTypes.Add(countRoomId);
                        countRoomId = new ObjType(CoreParser.Enum.EnumObjType.Kvartira4).Id;
                        objTypes.Add(countRoomId);
                        countRoomId = new ObjType(CoreParser.Enum.EnumObjType.Kvartira5Plus).Id;
                        objTypes.Add(countRoomId);
                    }
                    else
                    {
                        foreach (var room in CountRooms)
                        {
                            int countRoomId = -1;
                            switch (room)
                            {
                                case 1:
                                    countRoomId = new ObjType(CoreParser.Enum.EnumObjType.Kvartira1).Id;
                                    var studiaId = new ObjType(CoreParser.Enum.EnumObjType.Studia).Id;
                                    objTypes.Add(studiaId);
                                    break;
                                case 2:
                                    countRoomId = new ObjType(CoreParser.Enum.EnumObjType.Kvartira2).Id;
                                    break;
                                case 3:
                                    countRoomId = new ObjType(CoreParser.Enum.EnumObjType.Kvartira3).Id;
                                    break;
                                case 4:
                                    countRoomId = new ObjType(CoreParser.Enum.EnumObjType.Kvartira4).Id;
                                    break;
                                case 5:
                                    countRoomId = new ObjType(CoreParser.Enum.EnumObjType.Kvartira5Plus).Id;
                                    break;
                            }
                            if (countRoomId >= 0)
                                objTypes.Add(countRoomId);
                        }
                    }
                    break;
                // комната
                case "1":
                    var roomId = new ObjType(CoreParser.Enum.EnumObjType.Komnata).Id;
                    objTypes.Add(roomId);
                    break;
                //Дом\коттедж
                case "2":
                    var domId = new ObjType(CoreParser.Enum.EnumObjType.Dom).Id;
                    var kottedjId = new ObjType(CoreParser.Enum.EnumObjType.Kottedj).Id;
                    objTypes.Add(domId);
                    objTypes.Add(kottedjId);
                    break;
                // Коммерческая
                case "3":
                    var commercialId = new ObjType(CoreParser.Enum.EnumObjType.Commercial).Id;
                    objTypes.Add(commercialId);
                    break;
                // Земля\участок
                case "4":
                    var uchastokId = new ObjType(CoreParser.Enum.EnumObjType.Uchastok).Id;
                    var dachaId = new ObjType(CoreParser.Enum.EnumObjType.Dacha).Id;
                    objTypes.Add(uchastokId);
                    objTypes.Add(dachaId);
                    break;
            }
            filter.ObjTypes = objTypes;

            if (!string.IsNullOrEmpty(Districts))
            {
                filter.Districts = new List<int>();
                foreach (var id in Districts.Split(','))
                {
                    if (int.TryParse(id, out var result))
                        filter.Districts.Add(result);
                }
            }
            if (DistrictsArr != null)
            {
                filter.Districts.AddRange(DistrictsArr);
            }

            if (!string.IsNullOrEmpty(MicroDistricts))
            {
                filter.MicroDistricts = new List<int>();
                foreach (var id in MicroDistricts.Split(','))
                {
                    if (int.TryParse(id, out var result))
                        filter.MicroDistricts.Add(result);
                }
            }
            if (MicroDistrictsArr != null)
            {
                filter.MicroDistricts.AddRange(MicroDistrictsArr);
            }
#if DEBUG
            filter.Count = 5;
#else
            filter.Count = Count;
#endif
            filter.IndexPage = IndexPage;
            filter.DateBegin = DateTime.TryParse(DateFrom, out var dateBegin) ? dateBegin : DateTime.MinValue;
            filter.DateEnd = DateTime.TryParse(DateEnd, out var dateEnd) ? dateEnd : DateTime.MinValue;
            return filter;
        }
    }
}

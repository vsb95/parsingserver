using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Core;
using Core.Enum;
using Core.Exceptions;
using Core.Extension;
using Core.Storage;
using CoreMls.DB.LocalEntities;
using CoreMls.DB.LocalEntities.Property;
using CoreMls.DB.Managers;
using CoreMls.DB.Model;
using CoreMls.Enum;
using CoreMls.Managers;
using CoreMls.Validators;
using CoreParser.Enum;
using CoreParser.Extension;
using CoreParser.Services;
using CoreParser.Services.PropertyAnalizators;
using CoreParser.Services.Proxies;
using Logger;
using Newtonsoft.Json;
using Yandex.Geocoder;


namespace ApiService.Core.MLS
{
    public class AddMlsLivingOfferRequest
    {
        #region Properties

        public int IdCity { get; set; } = 643;
        public string OfferType { get; set; }
        public string RentPeriod { get; set; }
        public string RealtyType { get; set; } // жилая\коммерция
        public string ObjectCategory { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }
        
        public string KadastrNumber { get; set; }
        public string AreaSizeTotal { get; set; }
        public string AreaSizeLiving { get; set; }
        public string AreaSizeKitchen { get; set; }
        public string Floor { get; set; }
        public string TotalFloor { get; set; }
        public string CountRooms { get; set; }
        public string PlainingRooms { get; set; }

        public int CountLoggia { get; set; }
        public int CountBalcon { get; set; }
        public List<string> Windows { get; set; }
        public string RenovationType { get; set; }
        public List<string> Furnitures { get; set; } = new List<string>();

        public int CountToilet { get; set; }
        public int CountToiletWithBathroom { get; set; }
        public string PlaceTypeInHouse { get; set; }//угловая итд
        
        public string BuildingMicrodistrictName { get; set; } // ЖК "ххх"
        public string BuildingYear { get; set; }
        public string BuildingMaterialType { get; set; }
        public string BuildingSeria { get; set; }
        public string BuildingHeightFloor { get; set; }
        public int BuildingCountPassengerElevator { get; set; }
        public int BuildingCountWeightElevator { get; set; }
        public string BuildingIsHasTrash { get; set; }
        public string BuildingParkingType { get; set; }

        public List<UploadImage> ImagesList { get; set; } = new List<UploadImage>();
        public string MainPhoto { get; set; }
        public string YoutubeUrl { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public string Cost { get; set; }
        public string IsCanMorgage { get; set; } // возможна ипотека?
        public string IsOnPrepayment { get; set; } // под авансом\задатком?
        public string SellType { get; set; }
        public string ContractType { get; set; }
        public string CommissionType { get; set; }
        public string Commission { get; set; }

        public string Phone { get; set; }
        public string PhoneAlternative { get; set; }
        
        public User User { get; set; }
        public long? OwnerId { get; set; }

        #region Новостройки специфика:

        public string BuildingCorpus { get; set; }
        public string BuilderName { get; set; }
        public int? BuildEndYear { get; set; }
        public int? BuildEndQuartal { get; set; }
        public string IsBuildingDone { get; set; }
        public string IsHasSubgroundParking { get; set; }
        public string PlainingImage { get; set; }
        #endregion
        #region Аренда специфика:
        public string IsNewBuilding { get; set; }
        public string IsHasLift { get; set; }
        public string IsCanHaggle { get; set; }
        public string UtilitiesType { get; set; }
        public string UtilitiesCost { get; set; }
        public int PrepayPeriod { get; set; }
        public string FurnitureFill { get; set; }
        public string Prepayment { get; set; }
        public string PledgeCost { get; set; } // залог

        public string CountRentRooms { get; set; }
        public string AreaSizeRoom { get; set; }
        #endregion
        #region Продажа специфика
        /// <summary>
        /// Комнат в продажу
        /// </summary>
        public string SellCountRooms { get; set; }
        /// <summary>
        /// Размер доли
        /// </summary>
        public string PartSize { get; set; }
        #endregion
        #region Дом\дача специфика
        /// <summary>
        /// Тип туалета (на улице\в доме)
        /// </summary>
        public string ToiletType { get; set; }
        public string BuildingOrientir { get; set; }
        public string DistanceOutCity { get; set; }
        /// <summary>
        /// Площадь участка
        /// </summary>
        public string LotAreaSize { get; set; }
        /// <summary>
        /// гектар\м2\сотка
        /// </summary>
        public string LotAreaType { get; set; }
        public string LotAreaState { get; set; }
        public string HeatingType { get; set; }
        #endregion


        #endregion

        public bool TryParseToAdv(offer offer, out MlsAdvertisment adv, out string errorMessage)
        {
            if (offer == null)
                offer = new offer
                {
                    price_id_currency = 2, // захардкоженый RUB
                    property = new property
                    {
                        building = new building {building_location = new building_location()},
                        cadastral_number = KadastrNumber
                    },
                    creation_date = DateTime.Now,
                    last_update_date = DateTime.Now,
                    offer_feed_site = new List<offer_feed_site>
                    {
                        new offer_feed_site {id_feed_site = (long) FeedSite.Yandex, is_enabled_autoload = true},
                        new offer_feed_site {id_feed_site = (long) FeedSite.Avito, is_enabled_autoload = true}
                    }

                };

            
            errorMessage = "";
            var trace = new StringBuilder("0");
            try
            {
                #region Validation
                if (string.IsNullOrEmpty(OfferType))
                    throw new ValidationException("Не указан тип сделки");
                if (string.IsNullOrEmpty(RealtyType))
                    throw new ValidationException("Не указан тип недвижимости");
                if (string.IsNullOrEmpty(ObjectCategory))
                    throw new ValidationException("Не указан тип объекта");
                if (string.IsNullOrEmpty(Address))
                    throw new ValidationException("Не указан адрес");
                if (string.IsNullOrEmpty(AreaSizeTotal) && string.IsNullOrEmpty(AreaSizeRoom) && string.IsNullOrEmpty(LotAreaSize))
                    throw new ValidationException("Не указана площадь");
                if(!string.IsNullOrEmpty(KadastrNumber) && KadastrNumber.Length < 17)
                    throw new ValidationException("Не верно указан кадастровый номер");
                /*
                if (string.IsNullOrEmpty(RenovationType))
                    throw new ValidationException("Не указан ремонт");
                if (Floor == 0)
                    throw new ValidationException("Не указан этаж");                
                if (TotalFloor == 0)
                    throw new ValidationException("Не указана этажность");
                */
                if (string.IsNullOrEmpty(Phone) && string.IsNullOrEmpty(User?.Phone))
                    throw new ValidationException("Не указан телефон");
                if (User?.Id == null)
                    throw new ValidationException("Не указан пользователь. Обратитесь в техподдержку");
                if (string.IsNullOrEmpty(User?.Organization))
                    throw new ValidationException("Не указана организация. Обратитесь в техподдержку");
                if (string.IsNullOrEmpty(Cost))
                    throw new ValidationException("Не указана стоимость");
                if (!decimal.TryParse(ClearMask(Cost), out var cost))
                    throw new ValidationException("Не верно указана стоимость");
                #endregion

                trace.AppendLine("1");
                if (!String.IsNullOrEmpty(ObjectCategory) && ObjectCategory == "apartment_new")
                {
                    // TODO КОСТЫЛЬ. ОЧЕНЬ. ГРЯЗНЫЙ. КОСТЫЛЬ
                    RealtyType = "new_flat";
                    offer.property.new_flat = true;
                }
                OfferType offerType;
                switch (OfferType)
                {
                    case "sell":
                        offerType = CoreMls.Enum.OfferType.Sell;
                        offer.id_offer_status = (long) OfferStatus.DirectSale;
                        break;
                    case "rent":
                        offerType = CoreMls.Enum.OfferType.Rent;
                        offer.id_offer_status = (long)OfferStatus.DirectRent;
                        break;
                    default: throw new ValidationException("Не определен тип сделки: " + OfferType);
                }
                offer.id_offer_type = (long)offerType;

                OfferCategory offerCategory;
                switch (RealtyType)
                {
                    case "living":
                        offerCategory = OfferCategory.Living;
                        offer.property.id_property_type = (long)PropertyType.Living;
                        break;
                    case "commercial":
                        offerCategory = OfferCategory.Commercial;
                        offer.property.id_property_type = (long)PropertyType.Commercial;
                        throw new Exception("Не готов еще метод добавления коммерции");
                    case "new_flat":
                        offerCategory = OfferCategory.NewFlat;
                        offer.property.id_property_type = (long)PropertyType.Living;
                        break;
                    default: throw new ValidationException("Не определен тип сделки: " + OfferType);
                }
                offer.id_offer_yrl_category = (long)offerCategory;

                trace.AppendLine("2");
                var countRooms = 0;
                if (!string.IsNullOrEmpty(CountRooms))
                {
                    if (!int.TryParse(CountRooms, out countRooms))
                    {
                        switch (CountRooms)
                        {
                            case "5+":
                                countRooms = 5;
                                break;
                            case "studio":
                                countRooms = 1;
                                offer.property.studio = true;
                                break;
                            default: throw new ValidationException("Не верно указано количество комнат");
                        }
                    }
                }
                if (!string.IsNullOrEmpty(PlainingRooms))
                {
                    switch (PlainingRooms)
                    {
                        case "studia":
                            offer.property.studio = true;
                            break;
                        case "isolated":
                            offer.property.id_rooms_type = (long)RoomType.Isolated;
                            break;
                        case "neighbor":
                            offer.property.id_rooms_type = (long)RoomType.Neighbor;
                            break;
                        case "neighbor-isolated":
                            offer.property.id_rooms_type = (long)RoomType.NeighborIsolated;
                            break;
                        case "free":
                            offer.property.open_plan = true;
                            break;
                        default:
                            throw new ValidationException("Указан неизвестный тип планировки");
                    }
                }

                trace.AppendLine("3");
                var propertyCategory = PropertyLivingCategory.Default;
                switch (offerCategory)
                {
                    case OfferCategory.NewFlat:
                        if(ObjectCategory!= "apartment_new" && ObjectCategory != "apartmentnew")
                            throw new ValidationException("Не верный тип объекта для новостройки");
                        propertyCategory = PropertyLivingCategory.Apartment;
                        if (countRooms == 0)
                            throw new ValidationException("не указано количество комнат");
                        offer.property.rooms = countRooms;
                        offer.property.rooms_offered = countRooms;
                        offer.price_id_unit = (long)Unit.SqMeter;
                        offer.property.new_flat = true;
                        offer.property.id_property_category = (long)propertyCategory;
                        break;
                    case OfferCategory.Living:
                        switch (ObjectCategory)
                        {
                            case "apartment":
                                propertyCategory =PropertyLivingCategory.Apartment;
                                if(countRooms == 0)
                                    throw new ValidationException("не указано количество комнат");
                                offer.property.rooms = countRooms;
                                offer.property.rooms_offered = countRooms;
                                offer.price_id_unit = (long)Unit.SqMeter;
                                offer.property.new_flat = IsNewBuilding =="1";
                                break;
                            case "room":
                                propertyCategory =PropertyLivingCategory.Room;
                                offer.price_id_unit = (long)Unit.SqMeter;
                                offer.property.rooms = 1;
                                offer.property.rooms_offered = 1;
                                break;
                            case "housewithlot":
                            case "house":
                                propertyCategory =PropertyLivingCategory.House;
                                break;
                            case "cottage":
                            case "kottedj":
                                propertyCategory =PropertyLivingCategory.Cottage;
                                break;
                            case "dacha":
                                propertyCategory =PropertyLivingCategory.Dacha;
                                break;
                            case "apartmentpart":
                            case "apartment_part":
                                propertyCategory = PropertyLivingCategory.ApartmentPart;
                                break;
                            case "housepart":
                            case "house_part":
                                propertyCategory = PropertyLivingCategory.HousePart;
                                break;
                            case "lot":
                                propertyCategory = PropertyLivingCategory.Lot;
                                break;
                            default: throw new ValidationException("Указан неизвестный тип объекта");
                        }
                        offer.property.id_property_category = (long) propertyCategory;
                        break;
                    case OfferCategory.Commercial:
                        throw new Exception("Не реализован метод добавления коммерции");
                }

                foreach (var furniture in Furnitures)
                {
                    switch (furniture)
                    {
                        case "in-room":
                            offer.property.room_furniture = true;
                            break;
                        case "in-kitchen":
                            offer.property.kitchen_furniture = true;
                            break;
                        case "fridge":
                            offer.property.refrigerator = true;
                            break;
                        case "dishwasher":
                            offer.property.dishwasher = true;
                            break;
                        case "washing-machine":
                            offer.property.washing_machine = true;
                            break;
                        case "with-child":
                            offer.property.with_children = true;
                            break;
                        case "without-child":
                            offer.property.with_children = false;
                            break;
                        case "with-animal":
                            offer.property.with_pets = true;
                            break;
                        case "without-animal":
                            offer.property.with_pets = false;
                            break;
                        case "internet":
                            offer.property.internet = true;
                            break;
                        case "phone":
                            offer.property.phone = true;
                            break;
                        case "tv":
                            offer.property.television = true;
                            break;
                        case "conditioner":
                            offer.property.air_conditioner = true;
                            break;
                        case "shower":
                            offer.property.shower = true;
                            break;
                        case "electricity":
                            offer.property.electricity_supply = true;
                            break;
                        case "sewerage":
                            offer.property.sewerage_supply = true;
                            break;
                        case "sauna":
                            offer.property.sauna = true;
                            break;
                        case "water":
                            offer.property.water_supply = true;
                            break;
                        case "gas":
                            offer.property.gas_supply = true;
                            break;
                        case "pool":
                            offer.property.pool = true;
                            break;
                        case "outbuilding":
                            offer.property.outbuilding = true;
                            break;
                        case "foundation":
                            offer.property.foundation = true;
                            break;
                        case "security":
                            offer.property.building.security = true;
                            break;
                        case "garage":
                            offer.property.is_has_garage = true;
                            break;
                        case "bath":
                            offer.property.bath = true;
                            break;
                        case "balcon":
#if !DEBUG
                            // Todo переделать нормально
                            offer.property.balcony_count = 1;
#endif
                            break;
                        case "loggia":
#if !DEBUG
                            // Todo переделать нормально
                            offer.property.loggia_count = 1;
#endif
                            break;
                        default: Log.Fatal("Неизвестное поле '"+ furniture + "'; "+ offerType+", "+ propertyCategory); throw new ValidationException("не верный тип мебели: " + furniture);
                    }
                }

                trace.AppendLine("4");
                switch (offerType)
                {
                    case CoreMls.Enum.OfferType.Rent:
                    {
                        if (string.IsNullOrEmpty(RentPeriod))
                            throw new ValidationException("Не указан срок аренды");

                        switch (RentPeriod)
                        {
                            case "day":
                                offer.price_id_period = (long) PricePeriod.Day;
                                break;
                            case "month":
                                offer.price_id_period = (long) PricePeriod.Month;
                                break;
                            default:
                                throw new ValidationException("Указан неизвестный срок аренды");
                        }

                        if (string.IsNullOrEmpty(FurnitureFill))
                            throw new ValidationException("Не указана мебель");
                        switch (FurnitureFill)
                        {
                            case "full":
                                offer.property.id_furniture_fill = (long?) FurnitureFillType.Full;
                                break;
                            case "part":
                                offer.property.id_furniture_fill = (long?) FurnitureFillType.Part;
                                break;
                            case "on-request":
                                offer.property.id_furniture_fill = (long?) FurnitureFillType.OnRequest;
                                break;
                            default: throw new ValidationException("Не верно указана мебелеровка");
                        }

                        if (propertyCategory == PropertyLivingCategory.Room)
                        {
                            if (string.IsNullOrEmpty(CountRentRooms))
                                throw new ValidationException("не указано количество комнат для сдачи");

                            if (!int.TryParse(CountRentRooms, out var offeredRooms))
                            {
                                if (CountRentRooms != "5+")
                                    throw new ValidationException("не верно указано количество комнат для сдачи");
                                offer.property.rooms_offered = 5;
                            }
                            else
                                offer.property.rooms_offered = offeredRooms;
                            if(offer.property.rooms_offered < offer.property.rooms)
                                throw new ValidationException("Указано комнат в аренду больше чем комнат в квартире");
                        }

                        break;
                    }
                    case CoreMls.Enum.OfferType.Sell:
                    {
                        if (!string.IsNullOrEmpty(ContractType))
                        {
                            switch (ContractType)
                            {
                                case "exclusive":
                                    offer.id_contract_type = (long) CoreMls.Enum.ContractType.Exclusive;
                                    break;
                                case "soft":
                                    offer.id_contract_type = (long) CoreMls.Enum.ContractType.Soft;
                                    break;
                                case "none":
                                    offer.id_contract_type = (long) CoreMls.Enum.ContractType.None;
                                    break;
                                default: throw new ValidationException("Не верный вид договора");
                            }
                        }

                        if (!string.IsNullOrEmpty(SellType))
                        {
                            switch (SellType)
                            {
                                case "free":
                                    offer.id_sell_type = (long) CoreMls.Enum.SellType.Free;
                                    break;
                                case "alternative":
                                    offer.id_sell_type = (long) CoreMls.Enum.SellType.Alternative;
                                    break;
                                default: throw new ValidationException("Не верный тип продажи");
                            }
                        }
                        break;
                    }
                    default: throw new ValidationException("не верно указан тип сделки");
                }

                trace.AppendLine("5");
                if (!string.IsNullOrEmpty(HeatingType))
                {
                    offer.property.heating_supply = HeatingType == "1";
                }

                if (!string.IsNullOrEmpty(Floor))
                {
                    if(!int.TryParse(ClearMask(Floor), out var floor))
                        throw new ValidationException("не верно указан этаж");
                    offer.property.floor = floor;
                }
                if (!string.IsNullOrEmpty(TotalFloor))
                {
                    if (!int.TryParse(ClearMask(TotalFloor), out var totalfloor))
                        throw new ValidationException("не верно указан этаж");
                    offer.property.building.floors_total = totalfloor;
                }

                offer.price_value = cost;
                if (!string.IsNullOrEmpty(CommissionType))
                {
                    switch (CommissionType)
                    {
                        case "null":
                            offer.id_commission_type = (long)CoreMls.Enum.CommissionType.Null;
                            offer.agent_fee = null;
                            offer.commission = null;
                            break;
                        case "none":
                            offer.id_commission_type = (long) CoreMls.Enum.CommissionType.None;
                            offer.agent_fee = 0;
                            offer.commission = 0;
                            break;
                        case "percent":
                        {
                            if (string.IsNullOrEmpty(Commission) || !decimal.TryParse(ClearMask(Commission), out var commission))
                                throw new ValidationException("Не верно указана коммиссия");
                            offer.id_commission_type = (long) CoreMls.Enum.CommissionType.Percent;
                            offer.agent_fee = Math.Round(commission);
                            offer.commission = (long) Math.Round((cost / 100 * commission), 0);
                            break;
                        }
                        case "fixed":
                        {
                            if (string.IsNullOrEmpty(Commission) || !decimal.TryParse(ClearMask(Commission), out var commission))
                                throw new ValidationException("Не верно указана коммиссия");
                            offer.id_commission_type = (long) CoreMls.Enum.CommissionType.Fixed;
                            offer.agent_fee = Math.Round(commission / (cost / 100), 2);
                            offer.commission = (long) Math.Round(commission, 0);
                            break;
                        }
                        default: throw new ValidationException("Не верный тип комиссии");
                    }
                }

                trace.AppendLine("6");
                if (Windows != null && Windows.Count > 0)
                {
                    bool isYard = false, isStreet = false;
                    foreach (var window in Windows)
                    {
                        switch (window)
                        {
                            case "street":
                                isStreet = true;
                                break;
                            case "yard":
                                isYard = true;
                                break;
                            default:
                                throw new ValidationException("Указана неверный тип окон");
                        }
                    }

                    if (isYard && isStreet)
                        offer.property.id_window_view_type = (long)WindowType.Both;
                    else if (isYard)
                        offer.property.id_window_view_type = (long)WindowType.Yard;
                    else if (isStreet)
                        offer.property.id_window_view_type = (long)WindowType.Street;
                }

                if (!string.IsNullOrEmpty(AreaSizeTotal))
                {
                    offer.property.area_id_unit = (long)Unit.SqMeter;
                    if (!decimal.TryParse(ClearMask(AreaSizeTotal), out var area))
                        throw new ValidationException("Указана неверная общая площадь");
                    offer.property.area_value = area;
                }
                if (!string.IsNullOrEmpty(AreaSizeLiving))
                {
                    offer.property.living_space_id_unit = (long)Unit.SqMeter;
                    if (!decimal.TryParse(ClearMask(AreaSizeLiving), out var area))
                        throw new ValidationException("Указана неверная общая площадь");
                    offer.property.living_space_value = area;
                }
                trace.AppendLine("7");
                switch (propertyCategory)
                {
                    case PropertyLivingCategory.ApartmentPart:
                    case PropertyLivingCategory.Apartment:
                    {
                        if (!string.IsNullOrEmpty(AreaSizeKitchen))
                        {
                            offer.property.kitchen_space_id_unit = (long) Unit.SqMeter;
                            if (!decimal.TryParse(ClearMask(AreaSizeKitchen), out var area))
                                throw new ValidationException("Указана неверная общая площадь");
                            offer.property.kitchen_space_value = area;
                        }


                        // ToDo записать id типа балкнов\лоджий
                        var idBalconyType = CountBalcon + CountLoggia;
                        offer.property.balcony_count = CountBalcon;
                        offer.property.loggia_count = CountLoggia;

                        if (propertyCategory == PropertyLivingCategory.ApartmentPart)
                        {
                            if (string.IsNullOrEmpty(PartSize))
                                throw new ValidationException("Не указана доля");
                            offer.property.part = PartSize;
                            foreach (var feedSite in offer.offer_feed_site)
                            {
                                feedSite.is_enabled_autoload = false;
                            }
                        }

                        break;
                    }
                    case PropertyLivingCategory.Room:
                    {
                        if (!string.IsNullOrEmpty(AreaSizeRoom))
                        {
                            offer.property.area_id_unit = (long) Unit.SqMeter;
                            offer.property.living_space_id_unit = (long) Unit.SqMeter;
                            offer.property.room_space_id_unit = (long) Unit.SqMeter;
                            if (!decimal.TryParse(ClearMask(AreaSizeRoom), out var area))
                                throw new ValidationException("не верно указана площадь комнаты");
                            offer.property.room_space_value = area;
                            offer.property.living_space_value = area;
                            offer.property.area_value = area;
                        }

                        if (offerType == CoreMls.Enum.OfferType.Sell)
                        {
                            if (!string.IsNullOrEmpty(SellCountRooms))
                            {
                                if (!int.TryParse(CountRooms, out var saleCountRooms))
                                {
                                    switch (CountRooms)
                                    {
                                        case "5+":
                                            saleCountRooms = 5;
                                            break;
                                        case "studio":
                                            saleCountRooms = 1;
                                            break;
                                        default:
                                            throw new ValidationException(
                                                "Не верно указано количество комнат в продаже");
                                    }
                                }

                                offer.property.rooms_offered = saleCountRooms;
                            }
                        }

                        break;
                        }
                    case PropertyLivingCategory.HouseWithLot:
                    case PropertyLivingCategory.HousePart:
                    case PropertyLivingCategory.Dacha:
                    case PropertyLivingCategory.Cottage:
                    case PropertyLivingCategory.House:
                    {
                        if (string.IsNullOrEmpty(ToiletType)) throw new ValidationException("Не указан санузел");
                        offer.property.toilet = true;
                        offer.property.toilet_type = ToiletType;
                        offer.property.building.building_location.orientir = BuildingOrientir;
                        if (propertyCategory == PropertyLivingCategory.HousePart)
                        {
                            if (string.IsNullOrEmpty(PartSize))
                                throw new ValidationException("Не указана доля");
                            offer.property.part = PartSize;
                        }

                        if (!offer.property.floor.HasValue && offer.property.building.floors_total.HasValue)
                            offer.property.floor = offer.property.building.floors_total;

                        if (!string.IsNullOrEmpty(LotAreaSize))
                        {
                            if (string.IsNullOrEmpty(LotAreaType))
                                throw new ValidationException("не указан тип площади участка");
                            if (string.IsNullOrEmpty(LotAreaState))
                                throw new ValidationException("не указан статус участка");
                            propertyCategory = PropertyLivingCategory.HouseWithLot;
                            offer.property.id_property_category = (long) propertyCategory;
                            if (!decimal.TryParse(ClearMask(LotAreaSize), out var lotArea))
                                throw new ValidationException("не верно указана площадь участка");
                            offer.property.lot_area_value = lotArea;
                            //
                            switch (LotAreaType)
                            {
                                case "hectare":
                                    offer.property.lot_area_id_unit = (long) Unit.Hectare;
                                    break;
                                case "meter":
                                    offer.property.lot_area_id_unit = (long) Unit.SqMeter;
                                    break;
                                case "hundred":
                                    offer.property.lot_area_id_unit = (long) Unit.OneHundred;
                                    break;
                                default: throw new ValidationException("Не верно указан тип площади участка");
                            }

                            //
                            switch (LotAreaState)
                            {
                                case "garden":
                                    offer.property.id_lot_type = (long) LotType.Garden;
                                    break;
                                case "izs":
                                    offer.property.id_lot_type = (long) LotType.Person;
                                    break;
                                default: throw new ValidationException("Не верно указан статус участка");
                            }
                        }

                        break;
                    }
                    case PropertyLivingCategory.Lot:
                    {
                        if (string.IsNullOrEmpty(LotAreaSize))
                            throw new ValidationException("Не указана площадь участка");
                        if (string.IsNullOrEmpty(LotAreaType))
                            throw new ValidationException("не указан тип площади участка");
                        if (string.IsNullOrEmpty(LotAreaState))
                            throw new ValidationException("не указан статус участка");
                        offer.property.id_property_category = (long) propertyCategory;
                        if (!decimal.TryParse(ClearMask(LotAreaSize), out var lotArea))
                            throw new ValidationException("не верно указана площадь участка");
                        offer.property.lot_area_value = lotArea;
                        //
                        switch (LotAreaType)
                        {
                            case "hectare":
                                offer.property.lot_area_id_unit = (long) Unit.Hectare;
                                break;
                            case "meter":
                                offer.property.lot_area_id_unit = (long) Unit.SqMeter;
                                break;
                            case "hundred":
                                offer.property.lot_area_id_unit = (long) Unit.OneHundred;
                                break;
                            default: throw new ValidationException("Не верно указан тип площади участка");
                        }
                        //
                        switch (LotAreaState)
                        {
                            case "garden":
                                offer.property.id_lot_type = (long) LotType.Garden;
                                break;
                            case "izs":
                                offer.property.id_lot_type = (long) LotType.Person;
                                break;
                            default: throw new ValidationException("Не верно указан статус участка");
                        }
                        break;
                    }

                    default: throw new NotImplementedException();
                }

                trace.AppendLine("8");
                if (!string.IsNullOrEmpty(DistanceOutCity))
                {
                    if(!decimal.TryParse(ClearMask(DistanceOutCity), out var distance))
                        throw new ValidationException("Не верно указана удаленность");
                    offer.property.building.building_location.distance = distance;
                }

                if (!string.IsNullOrEmpty(RenovationType))
                {
                    if(!int.TryParse(RenovationType, out var renovation))
                        throw new ValidationException("Не верно указан тип ремонта");

                    var type = (CoreMls.Enum.RenovationType) renovation; // чисто для дебага
                    offer.property.id_property_renovation = (long) type;
                }

                offer.property.toilet_count = CountToilet;
                offer.property.toilet_with_bath_count = CountToiletWithBathroom;
                var idBathroomUnit = CountToiletWithBathroom + CountToilet + 1;
                if (CountToilet != 0 || CountToiletWithBathroom != 0)
                {
                    offer.property.toilet = true;
                }
                if (idBathroomUnit > 2)
                {
                    // Там в таблице после 2го значения идут суммы ванн+туалет
                    offer.property.id_bathroom_unit = idBathroomUnit;
                }
                else if(CountToilet > CountToiletWithBathroom)
                {
                    offer.property.id_bathroom_unit = 2;
                }
                else
                {
                    offer.property.id_bathroom_unit = 1;
                }

                trace.AppendLine("9");
                if (!string.IsNullOrEmpty(PlaceTypeInHouse))
                {
                    switch (PlaceTypeInHouse)
                    {
                        case "angle":
                            offer.property.id_flat_place_type = (long) CoreMls.Enum.FlatPlaceType.Angle;
                            break;
                        case "not_angle":
                            offer.property.id_flat_place_type = (long)CoreMls.Enum.FlatPlaceType.NotAngle;
                            break;
                        default: throw new ValidationException("Неизвестный тип расположения в доме");

                    }
                }
                if (!string.IsNullOrEmpty(Title))
                    offer.title = Title;
                if (!string.IsNullOrEmpty(Description))
                    offer.property.description = Description;
                if (!string.IsNullOrEmpty(YoutubeUrl))
                    offer.property.youtube_url = YoutubeUrl;
                if (!string.IsNullOrEmpty(IsCanMorgage))
                    offer.mortgage = IsCanMorgage == "1";
                if (!string.IsNullOrEmpty(IsCanHaggle))
                    offer.haggle = IsCanHaggle == "1";
                if (!string.IsNullOrEmpty(IsOnPrepayment))
                    offer.is_on_prepayment = IsOnPrepayment == "1";
                if (!string.IsNullOrEmpty(IsHasLift))
                    offer.property.building.lift = IsHasLift == "1";

                trace.AppendLine("10");
                if (!string.IsNullOrEmpty(Prepayment))
                {
                    if(!long.TryParse(Prepayment, out var prepay))
                        throw new ValidationException("Не верно указана предоплата");
                    offer.prepayment = prepay;
                }
                else if (PrepayPeriod != 0)
                {
                    offer.prepayment = PrepayPeriod;
                }
                if (!string.IsNullOrEmpty(PledgeCost))
                {
                    if (!long.TryParse(ClearMask(PledgeCost), out var pledgeCost))
                        throw new ValidationException("Не верно указан залог");
                    offer.rent_pledge_value = pledgeCost;
                    offer.rent_pledge = true;
                }
                if (!string.IsNullOrEmpty(UtilitiesType))
                {
                    switch (UtilitiesType)
                    {
                        case "utilities":
                            offer.utilities_included = true;
                            break;
                        case "counter":
                            offer.electricity_included = true;
                            break;
                        case "full":
                            offer.utilities_included = true;
                            offer.electricity_included = true;
                            break;
                        default: throw new ValidationException("Не верно указан тип коммунальных платежей");
                    }
                }
                if (!string.IsNullOrEmpty(UtilitiesCost))
                {
                    if (!long.TryParse(ClearMask(UtilitiesCost), out var utilitiesCost))
                        throw new ValidationException("Не верно указана стоимость коммунальных услуг");
                    offer.utilities_price = utilitiesCost;
                }

                trace.AppendLine("11");

                #region Building

                if (!string.IsNullOrEmpty(BuildingMicrodistrictName))
                {
                    offer.property.building.building_location.microdistrict_name = BuildingMicrodistrictName;
                }
                if (!string.IsNullOrEmpty(BuildingYear))
                {
                    if (!int.TryParse(BuildingYear, out var year) || year < 1900 || year > 2050)
                        throw new ValidationException("Не верно указан год постройки");
                    offer.property.building.built_year = year;
                }
                if (!string.IsNullOrEmpty(BuildingMaterialType))
                {
                    switch (BuildingMaterialType)
                    {
                        case "brick":
                            offer.property.building.id_building_type = (long)CoreMls.Enum.BuildingMaterialType.Кирпич;
                            break;
                        case "concrete":
                            offer.property.building.id_building_type = (long)CoreMls.Enum.BuildingMaterialType.Железобетонный;
                            break;
                        case "metall":
                            offer.property.building.id_building_type = (long)CoreMls.Enum.BuildingMaterialType.Металлический;
                            break;
                        case "block":
                            offer.property.building.id_building_type = (long)CoreMls.Enum.BuildingMaterialType.Блочный;
                            break;
                        case "wood":
                            offer.property.building.id_building_type = (long)CoreMls.Enum.BuildingMaterialType.Деревянный;
                            break;
                        case "brick-monolite":
                            offer.property.building.id_building_type = (long)CoreMls.Enum.BuildingMaterialType.КирпичноМонолитный;
                            break;
                        case "monolite":
                            offer.property.building.id_building_type = (long)CoreMls.Enum.BuildingMaterialType.Монолит;
                            break;
                        case "panel":
                            offer.property.building.id_building_type = (long)CoreMls.Enum.BuildingMaterialType.Панельный;
                            break;
                        case "skeleton":
                            offer.property.building.id_building_type = (long)CoreMls.Enum.BuildingMaterialType.КаркасноНасыпной;
                            break;
                        default: throw new ValidationException("не верно указан материал дома");
                    }
                }
                if (!string.IsNullOrEmpty(BuildingSeria))
                {
                    offer.property.building.building_series = BuildingSeria;
                }
                if (!string.IsNullOrEmpty(BuildingHeightFloor))
                {
                    if (!decimal.TryParse(ClearMask(BuildingHeightFloor), out var height))
                        throw new ValidationException("Не верно указана высота потолков");
                    offer.property.ceiling_height = height;
                }
                if (BuildingCountPassengerElevator != 0)
                {
                    offer.property.building.count_passenger_lift = BuildingCountPassengerElevator;
                    offer.property.building.lift = true;
                }
                if (BuildingCountWeightElevator != 0)
                {
                    offer.property.building.count_weight_lift = BuildingCountPassengerElevator;
                    offer.property.building.lift = true;
                }
                if (!string.IsNullOrEmpty(BuildingIsHasTrash))
                {
                    offer.property.building.is_has_trash_tube = BuildingIsHasTrash == "1";
                }
                if (!string.IsNullOrEmpty(BuildingParkingType))
                {
                    offer.property.building.parking = true;
                    switch (BuildingParkingType)
                    {
                        case "ground":
                            offer.property.building.id_parking_type = (long)ParkingType.Ground;
                            break;
                        case "multilevel":
                            offer.property.building.id_parking_type = (long)ParkingType.Multilevel;
                            break;
                        case "subground":
                            offer.property.building.id_parking_type = (long)ParkingType.SubGround;
                            break;
                        default: throw new ValidationException("неизвестный тип парковки");
                    }

                }
                //
                #region новостройки
                if (!string.IsNullOrEmpty(BuildingCorpus))
                {
                    offer.property.building.building_section = BuildingCorpus;
                }
                if (!string.IsNullOrEmpty(BuilderName))
                {
                    var builderId = DbManager.GetBuilderIdByName(BuilderName);
                    if (builderId.HasValue)
                        offer.property.building.id_builder = builderId.Value;
                    else
                        offer.property.building.builder = new builder { name = BuilderName };
                }
                if (BuildEndYear.HasValue)
                {
                    //Если после постройки прошло более 10 лет, то я чет не верю что это новостройка
                    if (DateTime.Now.Year - BuildEndYear > 10)
                        throw new ValidationException("Слишком старое здание для новостройки");
                    offer.property.building.built_year = BuildEndYear.Value;
                }
                if (BuildEndQuartal.HasValue)
                {
                    //Если после постройки прошло более 10 лет, то я чет не верю что это новостройка
                    if (BuildEndQuartal.Value > 4 || BuildEndQuartal.Value < 1)
                        throw new ValidationException("Не верно указан квартал");
                    offer.property.building.id_ready_quarter = BuildEndQuartal.Value;
                }
                if (!string.IsNullOrEmpty(IsBuildingDone) && IsBuildingDone == "1")
                {
                    offer.property.building.id_building_state = (long?)BuildingState.Done;
                }
                if (!string.IsNullOrEmpty(IsHasSubgroundParking) && IsHasSubgroundParking == "1")
                {
                    offer.property.building.id_parking_type = (long?)ParkingType.SubGround;
                }
                if (!string.IsNullOrEmpty(PlainingImage))
                {
                    offer.property.flat_plain_image = PlainingImage;
                }
                #endregion
                //
                #endregion

                trace.AppendLine("12");

                #region Address

                try
                {
                    if (offer.id_offer == 0 ||
                        (offer.id_offer > 0 && Address != offer.property.building.building_location.address))
                    {
                        var resp = YandexGeocoder.FindAdress(Address, Kind.house,
                            ProxyManager.GetProxy(ParsingPageType.Yandex, false)?.ToWebProxy());

                        if (resp?.Point != null)
                        {
                            var geocoord = new GeoCoordinate(resp.Point.Latitude, resp.Point.Longitude);
                            if (RegionsManager.DistrictManager.TryIdentifyDistrictAndMicroDistrict(geocoord, out var district,
                                out var microDistrict, out var isNeedReverse))
                            {
                                offer.property.building.building_location.latitude =
                                    isNeedReverse ? geocoord.Lon : geocoord.Lat;
                                offer.property.building.building_location.longitude =
                                    isNeedReverse ? geocoord.Lat : geocoord.Lon;
                                offer.property.building.building_location.id_district = district?.Id;
                                offer.property.building.building_location.id_micro_district = microDistrict?.Id;
                                if (microDistrict == null)
                                    Log.Error(
                                        "Не удалось распознать мкр при добалении млс кв: '" + Address + "' " + geocoord,
                                        true);
                            }
                            else
                            {
                                offer.property.building.building_location.latitude = geocoord.Lat;
                                offer.property.building.building_location.longitude = geocoord.Lon;
                                Log.Error("Не удалось распознать район при добалении млс кв: " + Address, true);
                            }
                        }

                        offer.property.building.building_location.country = "Россия"; //resp.CountryName;
                        offer.property.building.building_location.locality_name =
                            string.IsNullOrEmpty(resp.LocalityName) ? "Омск" : resp.LocalityName;
                        offer.property.building.building_location.region = string.IsNullOrEmpty(resp.AdministrativeAreaName) ? "Омская область" : resp.AdministrativeAreaName;

                        // отформатируем адрес
                        var addressFormat = Address.Replace("Россия,", "").Replace("Омск,", "").Trim();
                        if (string.IsNullOrEmpty(addressFormat) || addressFormat.Length < 2)
                        {
                            addressFormat = "Омск";
                        }
                        else
                        {
                            addressFormat = $"{addressFormat.Substring(0, 1).ToUpper()}{addressFormat.Substring(1)}";
                        }
                        offer.property.building.building_location.address = addressFormat;
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, LogType.Error, true, "Ошибка при указании адреса в млс: '"+ Address+"'");
                    throw new ValidationException("Проблема при распознавании адреса. Проверьте, все ли правильно введено и повторите<br>Если все корректно - обратитесь в техподдержку");
                }
                #endregion

                trace.AppendLine("13");

                #region SalesAgent
                if (string.IsNullOrEmpty(User?.Organization))
                    throw new ValidationException("Не указано название организации у пользователя");

                var salesAgent = User.ToSalesAgent().ToDb();
                /*
                var salesAgent = new sales_agent
                {
                    id_sales_agent = User.Id,
                    sales_agent_name = User.Name,
                    sales_agent_email = User.Email,
                    sales_agent_organization = User.Organization,
                    sales_agent_photo = User.Photo,
                    sales_agent_phone = new List<sales_agent_phone>(),
                    id_sales_agent_category = 2 // агентство
                };
                var phone = (Phone ?? User.Phone);
                var phoneAlternative = (PhoneAlternative ?? User.PhoneAlternative);
                //var origSalesAgent = DbManager.GetAgentById(User.Id);
                salesAgent.sales_agent_phone.Add(DbSalesAgentManager.GetSalesAgentPhone(phone, User.Id));

                if (!string.IsNullOrEmpty(phoneAlternative))
                {
                    salesAgent.sales_agent_phone.Add(DbSalesAgentManager.GetSalesAgentPhone(phoneAlternative, User.Id));
                }
                */
                offer.id_sales_agent = DbSalesAgentManager.AddOrUpdate(salesAgent);
                if (offer.id_sales_agent == -1 || OwnerId.HasValue && OwnerId != offer.id_sales_agent)
                {
                    // throw new Exception("Не удалось создать\\обновить сотрудника: "+offer.id_sales_agent);
                }
                if (offer.id_sales_agent == -1)
                {
                    offer.id_sales_agent = null;
                    offer.sales_agent = salesAgent;
                }
                else
                {
                    offer.sales_agent = null;
                }
                #endregion
                trace.AppendLine("14");

                // Удаление изображений
                foreach (var image in offer.property.property_image.ToList())
                {
                    if (ImagesList != null && ImagesList.Count > 0 &&
                        ImagesList.Any(x => x.Src == image.image_url)) continue;
                    ImageManager.DeleteFileIfExist(image.image_url);
                    offer.property.property_image.Remove(image);
                }

                if (ImagesList != null && ImagesList.Count > 0)
                {
                    var oldListImages = offer.property.property_image.ToList();
                    offer.property.property_image = new List<property_image>();
                    if (!string.IsNullOrEmpty(MainPhoto))
                    {
                        var mainImage = ImagesList.FirstOrDefault(x => x.Src == MainPhoto);
                        if (mainImage != null)
                            mainImage.Order = -1;
                        else 
                            Log.Fatal("[MlsAdv:Save] Указан MainPhoto, а его не нашли в объявлении");
                    }

                    ImagesList = ImagesList.OrderBy(x => x.Order).ToList();
                    foreach (var uploadImage in ImagesList)
                    {
                        if (offer.property.property_image.All(x => x.image_url != uploadImage.Src))
                            offer.property.property_image.Add(
                                new property_image
                                {
                                    create_dt = DateTime.Now,
                                    image_url = uploadImage.Src
                                }
                            );
                    }

                    foreach (var image in oldListImages)
                    {
                        if (offer.property.property_image.All(x => x.image_url != image.image_url))
                            offer.property.property_image.Add(image);
                    }
                    if(offer.property.property_image.Count > ImagesList.Count)
                        Log.Warn("Чет много изображений");
                    
                }
                offer.last_update_date = DateTime.Now;
                trace.AppendLine("15 start last validation");

                #region Last Validation

                try
                {
                    var validator = OfferValidatorFactory.CreateLivingValidator(offerType, offerCategory, propertyCategory);
                    if (validator == null || !validator.IsValid(offer))
                        throw new Exception("Не пройдена проверка объявления");
                }
                catch (ValidationException vex)
                {
                    if (vex.ErrorType != ValidationErrorType.Warning)
                        throw;
                    Log.Warn(vex.ToString());
                }

                #endregion

                trace.AppendLine("16 done");
                adv = new MlsAdvertisment(offer);
                return true;
            }
            catch (Exception ex)
            {
                if (ex is ValidationException)
                    throw;
                Log.Exception(ex, LogType.Fatal,true, "[AddMlsLivingOfferRequest::ToAdv] trace: " + trace);
                errorMessage = ex.Message;
            }

            adv = null;
            return false;
        }

        private string ClearMask(string input)
        {
            Regex rgx = new Regex("[^0-9:,.]");
            return rgx.Replace(input, "").Replace(".", ",");
            //return input?.Replace(" ", "").Replace("_", "").Replace("+", "").Replace("-", "").Replace("м", "").Replace("км", "").Replace(".", ",").Replace("Р", "").Replace("%", "").Replace("руб/мес", "").Replace("р/мес", "").Replace("руб", "");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreMls.DB.LocalEntities;
using Newtonsoft.Json;

namespace ApiService.Core.MLS
{
    public class User
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string PhoneAlternative { get; set; }
        public long IdOrg { get; set; }
        public string Organization { get; set; }
        public string OrganizationWatermark { get; set; }
        public string Email { get; set; }
        public string Photo { get; set; }

        public SalesAgent ToSalesAgent()
        {
            return new SalesAgent
            {
                Id = Id,
                Name = Name,
                Phone = Phone,
                PhoneAlternative = PhoneAlternative,
                Organization = new Organization{Id = IdOrg, Name = Organization, WatermarkImage = OrganizationWatermark},
                Email = Email,
                PhotoUrl = Photo
            };
        }
    }
}

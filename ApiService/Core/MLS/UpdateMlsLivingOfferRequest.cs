﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Core;
using Core.Enum;
using Core.Exceptions;
using Core.Extension;
using CoreMls.DB.LocalEntities;
using CoreMls.DB.LocalEntities.Property;
using CoreMls.DB.Managers;
using CoreMls.DB.Model;
using CoreMls.Enum;
using CoreParser.Enum;
using CoreParser.Extension;
using CoreParser.Services;
using CoreParser.Services.PropertyAnalizators;
using CoreParser.Services.Proxies;
using Logger;
using Newtonsoft.Json;
using Enumerable = System.Linq.Enumerable;


namespace ApiService.Core.MLS
{
    public class UpdateMlsLivingOfferRequest : AddMlsLivingOfferRequest
    {
        #region Properties

        //Id Объявления, используется в updat'e
        public long? Id { get; set; }

        #endregion

        public bool TryUpdate(out string errorMessage)
        {
            errorMessage = "";
            var trace = new StringBuilder("0");
            try
            {
                offer offer = null;
                if (!Id.HasValue)
                {
                    throw new ValidationException("не найдено объявление");
                }

                using (var context = new realtyMLSEntities())
                {
                    offer = DbOfferManager.GetOfferById(Id.Value, context);
                    if (offer == null) throw new ValidationException("не найдено объявление");
                    //
                    OfferType = ((CoreMls.Enum.OfferType)offer.id_offer_type).ToString().ToLower();
                    var offerCategory = (OfferCategory) offer.id_offer_yrl_category;
                    if (offerCategory == OfferCategory.NewFlat)
                    {
                        ObjectCategory = "apartment_new";
                        RealtyType = "new_flat";
                    }
                    else
                    {
                        var propertyCategory = (PropertyLivingCategory)offer.property.id_property_category;
                        ObjectCategory = propertyCategory.ToString().ToLower();
                        RealtyType = offerCategory.ToString().ToLower();
                    }

                    var propType = (PropertyType) offer.property.id_property_type;
                    //
                    offer.utilities_included = null;
                    offer.electricity_included = null;
                    offer.rent_pledge = null;
                    offer.property.studio = null;
                    offer.property.open_plan = null;
                    //
                    offer.property.room_furniture = null;
                    offer.property.kitchen_furniture = null;
                    offer.property.refrigerator = null;
                    offer.property.dishwasher = null;
                    offer.property.washing_machine = null;
                    offer.property.with_children = null;
                    offer.property.with_pets = null;
                    offer.property.internet = null;
                    offer.property.phone = null;
                    offer.property.television = null;
                    offer.property.air_conditioner = null;
                    offer.property.bath = null;
                    offer.property.shower = null;
                    offer.property.electricity_supply = null;
                    offer.property.sewerage_supply = null;
                    offer.property.sauna = null;
                    offer.property.water_supply = null;
                    offer.property.gas_supply = null;
                    offer.property.pool = null;
                    offer.property.outbuilding = null;
                    offer.property.foundation = null;
                    offer.property.building.security = null;
                    offer.property.is_has_garage = null;
                    //
                    return TryParseToAdv(offer, out var adv, out errorMessage) && DbOfferManager.AddOrUpdatePage(adv);
                }
            }
            catch (Exception ex)
            {
                if (ex is ValidationException)
                    throw;
                Log.Exception(ex, LogType.Fatal, true, "[AddMlsLivingOfferRequest::ToAdv] trace: " + trace);
                errorMessage = ex.Message;
            }

            return false;
        }
    }
}

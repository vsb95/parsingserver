﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Exceptions;
using CoreMls.DB;
using CoreMls.Enum;
using Logger;

namespace ApiService.Core.MLS
{
    public class SearchMlsRequest
    {
        /// <summary>
        /// Id пользователя который ищет
        /// </summary>
        public long RequestedUserId { get; set; } = -1;

        public string Address { get; set; }
        public string OfferType { get; set; }
        public string ObjectTypeSelect { get; set; }
        public string OfferCategory { get; set; }
        public string RentPeriod { get; set; }
        public string LotAreaTotalMin { get; set; }
        public string LotAreaTotalMax { get; set; }
        public string AreaTotalMin { get; set; }
        public string AreaTotalMax { get; set; }
        public string AreaLivingMin { get; set; }
        public string AreaLivingMax { get; set; }
        public string AreaKitchenMin { get; set; }
        public string AreaKitchenMax { get; set; }
        public string AreaRoomMin { get; set; }
        public string AreaRoomMax { get; set; }
        public string DistanceOutCityMin { get; set; }
        public string DistanceOutCityMax { get; set; }
        public string LotAreaState { get; set; }
        public int FloorMin { get; set; }
        public int FloorMax { get; set; }
        public string FloorDetail { get; set; }
        public int FloorTotalMin { get; set; }
        public int FloorTotalMax { get; set; }
        public int BuildYear { get; set; }
        public int BuildYearMin { get; set; }
        public int BuildYearMax { get; set; }
        public string CellHeightMin { get; set; }
        public string CellHeightMax { get; set; }
        public string HeatingType { get; set; }
        public string CountRoomsOffered { get; set; }
        public int CountLiftMin { get; set; }
        public string BalconyType { get; set; } // не используется
        public string WindowType { get; set; }
        public string RenovationType { get; set; }
        public string PlainingType { get; set; }
        public string FlatPlaceType { get; set; }
        public string Districts { get; set; }
        public string MicroDistricts { get; set; }
        public string ToiletType { get; set; } // В доме\на улице
        public int ToiletCount { get; set; } // не используется
        public string SellType { get; set; }
        public string ContractType { get; set; }
        public string Phone { get; set; }
        public long? Id { get; set; }

        public string CostMin { get; set; }
        public string CostMax { get; set; }

        public string IsCanMortgage { get; set; }
        public string IsOnPrepayment { get; set; }
        public string IsOnlyWithPhoto { get; set; }
        public string IsWithFurniture { get; set; }
        public string IsWithoutCommission { get; set; }

        public string AdvType { get; set; } // архивное или нет

        public List<int> DistrictsArr { get; set; }
        public List<int> MicroDistrictsArr { get; set; }
        public List<long> CountRooms { get; set; }

        /// <summary>
        /// Тип дома при продаже (кирпич\панель\ итд)
        /// </summary>
        public List<string> HouseMaterials { get; set; }
        
        /// <summary>
        /// Количество на странице
        /// </summary>
        public int Count { get; set; } = 50;

        /// <summary>
        /// № страницы
        /// </summary>
        public int IndexPage { get; set; } = 0;

        /// <summary>
        /// Тип сортировки
        /// </summary>
        public int OrderType { get; set; } = 0;

        /// <summary>
        /// поиск по сотруднику
        /// </summary>
        public long? UserId { get; set; }
        /// <summary>
        /// поиск по организации
        /// </summary>
        public long? OrganizationId { get; set; }
        /// <summary>
        /// Поиск по организации или пользователю?
        /// </summary>
        public string FilterBy { get; set; }

        public SearchMlsFilter ToSearchFilter()
        {
            var filter = new SearchMlsFilter
            {
                IsOnPrepayment = IsOnPrepayment == "1",
                IsCanMortgage = IsCanMortgage == "1",
                IsOnlyWithPhoto = IsOnlyWithPhoto == "1",
                IsWithFurniture = IsWithFurniture == "1",
                OrderType = OrderType,
                OfferType = OfferType == "sell" ? CoreMls.Enum.OfferType.Sell :
                    OfferType == "rent" ? CoreMls.Enum.OfferType.Rent : CoreMls.Enum.OfferType.Default,// throw new Exception("Не указан тип сделки"),
                FloorDetail = FloorDetail,
                FloorMin = FloorMin,
                FloorMax = FloorMax,
                FloorTotalMin = FloorTotalMin,
                FloorTotalMax = FloorTotalMax,
                CountLiftMin = CountLiftMin,
                BalconyType = BalconyType,
                Phone = Phone,
                Id = Id,
                Count = Count,
                UserId = UserId,
                OrganizationId = OrganizationId,
                IndexPage = IndexPage,
                FilterBy = FilterBy,
                AdvType = AdvType
            };
            if (Id.HasValue)
                return filter;
            if (HouseMaterials != null && HouseMaterials.Count >0)
            {
                foreach (var material in HouseMaterials)
                {
                    if (string.IsNullOrEmpty(material)) continue;
                    long res = -1;
                    switch (material)
                    {
                        case "brick":
                            res = (long)BuildingMaterialType.Кирпич;
                            break;
                        case "concrete":
                            res = (long)BuildingMaterialType.Железобетонный;
                            break;
                        case "metall":
                            res = (long)BuildingMaterialType.Металлический;
                            break;
                        case "block":
                            res = (long)BuildingMaterialType.Блочный;
                            break;
                        case "wood":
                            res = (long)BuildingMaterialType.Деревянный;
                            break;
                        case "brick-monolite":
                            res = (long)BuildingMaterialType.КирпичноМонолитный;
                            break;
                        case "monolite":
                            res = (long)BuildingMaterialType.Монолит;
                            break;
                        case "panel":
                            res = (long)BuildingMaterialType.Панельный;
                            break;
                        default: throw new ValidationException("не верно указан материал дома", material);
                    }
                    if(res!= -1)
                        filter.HouseMaterials.Add(res);
                }
            }
            if (!string.IsNullOrEmpty(AreaTotalMin) && decimal.TryParse(AreaTotalMin.Replace(".",","), out var parseRes))
                filter.AreaTotalMin = parseRes;
            if (!string.IsNullOrEmpty(AreaTotalMax) && decimal.TryParse(AreaTotalMax.Replace(".", ","), out parseRes))
                filter.AreaTotalMax = parseRes;
            //
            if (!string.IsNullOrEmpty(AreaLivingMin) && decimal.TryParse(AreaLivingMin.Replace(".", ","), out parseRes))
                filter.AreaLivingMin = parseRes;
            if (!string.IsNullOrEmpty(AreaLivingMax) && decimal.TryParse(AreaLivingMax.Replace(".", ","), out parseRes))
                filter.AreaLivingMax = parseRes;
            //
            if (!string.IsNullOrEmpty(AreaKitchenMin) && decimal.TryParse(AreaKitchenMin.Replace(".", ","), out parseRes))
                filter.AreaKitchenMin = parseRes;
            if (!string.IsNullOrEmpty(AreaKitchenMax) && decimal.TryParse(AreaKitchenMax.Replace(".", ","), out parseRes))
                filter.AreaKitchenMax = parseRes;
            //
            if (!string.IsNullOrEmpty(LotAreaTotalMin) && decimal.TryParse(LotAreaTotalMin.Replace(".", ","), out parseRes))
                filter.LotAreaTotalMin = parseRes;
            if (!string.IsNullOrEmpty(LotAreaTotalMax) && decimal.TryParse(LotAreaTotalMax.Replace(".", ","), out parseRes))
                filter.LotAreaTotalMax = parseRes;
            //
            if (!string.IsNullOrEmpty(AreaRoomMin) && decimal.TryParse(AreaRoomMin.Replace(".", ","), out parseRes))
                filter.AreaRoomMin = parseRes;
            if (!string.IsNullOrEmpty(AreaRoomMax) && decimal.TryParse(AreaRoomMax.Replace(".", ","), out parseRes))
                filter.AreaRoomMax = parseRes;
            //
            if (!string.IsNullOrEmpty(DistanceOutCityMin) && decimal.TryParse(DistanceOutCityMin.Replace(".", ","), out parseRes))
                filter.DistanceOutCityMin = parseRes;
            if (!string.IsNullOrEmpty(DistanceOutCityMax) && decimal.TryParse(DistanceOutCityMax.Replace(".", ","), out parseRes))
                filter.DistanceOutCityMax = parseRes;
            //
            if (!string.IsNullOrEmpty(CellHeightMin) && decimal.TryParse(CellHeightMin.Replace(".", ","), out parseRes))
                filter.CellHeightMin = parseRes;
            if (!string.IsNullOrEmpty(CellHeightMax) && decimal.TryParse(CellHeightMax.Replace(".", ","), out parseRes))
                filter.CellHeightMax = parseRes;
            //
            if (!string.IsNullOrEmpty(CostMin) && decimal.TryParse(CostMin.Replace(" ", "").Replace(".", ","), out parseRes))
                filter.CostMin = parseRes;
            if (!string.IsNullOrEmpty(CostMax) && decimal.TryParse(CostMax.Replace(" ", "").Replace(".", ","), out parseRes))
                filter.CostMax = parseRes;
            //
            if (!string.IsNullOrEmpty(RenovationType) && long.TryParse(RenovationType.Replace(" ", ""), out var renovation))
                filter.RenovationType = renovation;

            if (!string.IsNullOrEmpty(FlatPlaceType))
            {
                switch (FlatPlaceType)
                {
                    case "angle":
                        filter.FlatPlaceType = (long) CoreMls.Enum.FlatPlaceType.Angle;
                        break;
                    case "not_angle":
                        filter.FlatPlaceType = (long)CoreMls.Enum.FlatPlaceType.Angle;
                        break;
                    default: throw new ValidationException("не верный тип расположения квартиры", FlatPlaceType);
                }
            }

            if (!string.IsNullOrEmpty(LotAreaState))
            {
                switch (LotAreaState)
                {
                    case "garden":
                        filter.LotAreaState = (long)LotType.Garden;
                        break;
                    case "izs":
                        filter.LotAreaState = (long)LotType.Person;
                        break;
                    default: throw new ValidationException("не верный тип участка", LotAreaState);
                }
            }

            if (!string.IsNullOrEmpty(CountRoomsOffered))
            {
                if (!int.TryParse(CountRoomsOffered, out var offered))
                {
                    if (CountRoomsOffered != "5+")
                        throw new ValidationException("Не верно указано количество комнат в аренду", CountRoomsOffered);
                    offered = 5;
                }

                filter.CountRoomsOffered = offered;
            }
            if (!string.IsNullOrEmpty(OfferCategory))
            {
                switch (OfferCategory)
                {
                    case "living":
                        filter.OfferCategory = (long) CoreMls.Enum.OfferCategory.Living;
                        break;
                    case "commercial":
                        filter.OfferCategory = (long) CoreMls.Enum.OfferCategory.Commercial;
                        break;
                    default: throw new ValidationException("Не определен тип сделки", OfferCategory);
                }
            }

            if (!string.IsNullOrEmpty(ToiletType))
            {
                switch (ToiletType)
                {
                    case "outer":
                    case "inner":
                        filter.ToiletType = ToiletType;
                        break;
                    default: throw new ValidationException("Не определен тип санузла", ToiletType);
                }
            }
            if (!string.IsNullOrEmpty(IsWithoutCommission))
                filter.IsWithoutCommission = IsWithoutCommission == "1";
            if (!string.IsNullOrEmpty(HeatingType))
                filter.HeatingType = HeatingType == "1";

            if (!string.IsNullOrEmpty(WindowType))
            {
                switch (WindowType)
                {
                    case "street":
                        filter.WindowView = (long) CoreMls.Enum.WindowType.Street;
                        break;
                    case "yard":
                        filter.WindowView = (long) CoreMls.Enum.WindowType.Street;
                        break;
                    case "streetANDyard":
                        filter.WindowView = (long) CoreMls.Enum.WindowType.Both;
                        break;
                    default:
                        throw new ValidationException("Указана неверный тип окон", WindowType);
                }
            }

            if (!string.IsNullOrEmpty(ContractType))
            {
                switch (ContractType)
                {
                    case "exclusive":
                        filter.ContractType = (long)CoreMls.Enum.ContractType.Exclusive;
                        break;
                    case "soft":
                        filter.ContractType = (long)CoreMls.Enum.ContractType.Soft;
                        break;
                    case "none":
                        filter.ContractType = (long)CoreMls.Enum.ContractType.None;
                        break;
                    default: throw new ValidationException("Не верный вид договора", ContractType);
                }
            }
            if (!string.IsNullOrEmpty(PlainingType))
            {
                switch (PlainingType)
                {
                    case "studia":
                        filter.IsStudia = true;
                        break;
                    case "isolated":
                        filter.PlainingType = (long)RoomType.Isolated;
                        break;
                    case "neighbor":
                        filter.PlainingType = (long)RoomType.Neighbor;
                        break;
                    case "free":
                        filter.IsOpenPlan = true;
                        break;
                    default:
                        throw new ValidationException("Указан неизвестный тип планировки", PlainingType);
                }
            }

            if (!string.IsNullOrEmpty(SellType))
            {
                switch (SellType)
                {
                    case "free":
                        filter.SellType = CoreMls.Enum.SellType.Free;
                        break;
                    case "alternative":
                        filter.SellType = CoreMls.Enum.SellType.Alternative;
                        break;
                    default:throw new ValidationException("не верный тип продажи", SellType);
                }
            }
            if (!string.IsNullOrEmpty(Address))
            {
                Address = Address.Trim();
                string lastChunk = null;
                try
                {
                    // Вырезаем город
                    var cityName = CoreParser.DB.DbObjectManager.GetNameCity(643); // #OMSK
                    filter.Adress =
                        Address.ToLower().Replace(cityName.ToLower(), "").Replace("россия, ", "");

                    var chunks = Address.Split(new[] {","}, StringSplitOptions.RemoveEmptyEntries);
                    lastChunk = chunks.LastOrDefault()?.ToLower().Replace("к", "").Replace("/", "")
                        .Replace("п", "").Replace("а", "").Replace("б", "").Replace("в", "").Replace("г", "")
                        .Replace("д", "").Replace("е", "").Trim();

                    if (chunks.Length > 2 && !string.IsNullOrEmpty(lastChunk))
                    {
                        // Если не удалось определить номер дома, то скорее всего поиск только по улице
                        if (int.TryParse(lastChunk, out var result))
                        {
                            filter.House = chunks.LastOrDefault()?.Trim();
                            filter.Street = chunks[chunks.Length - 2]?.Trim();
                        }
                        else
                        {
                            filter.Street = chunks.LastOrDefault()?.Trim();
                            if (lastChunk.Length < 10)
                            {
                                var adr = int.Parse(lastChunk); // выкинем исключение, чтобы понять почему не удалось
                            }
                        }
                    }
                    else if (chunks.Length == 1 && chunks.FirstOrDefault() == cityName)
                    {
                        filter.Adress = null;
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, LogType.Error, true,
                        "Parsig filter.SearchAdress; '"+Address+"' lastChunk = '" + lastChunk + "'");
                }
            }

            if (filter.OfferType == CoreMls.Enum.OfferType.Rent)
            {
                switch (RentPeriod)
                {
                    case "day":
                        filter.RentPeriod = PricePeriod.Day;
                        break;
                    case "month":
                        filter.RentPeriod = PricePeriod.Month;
                        break;
                    default:
                        if (filter.UserId.HasValue || filter.OrganizationId.HasValue)
                            break;
                        throw new ValidationException("Неизвестный срок аренды: ", RentPeriod);
                }
            }
            
            if (ObjectTypeSelect == "apartment_new")
            {
                filter.OfferCategory = (long)CoreMls.Enum.OfferCategory.NewFlat;
                filter.IsNewFlat = true;
                ObjectTypeSelect = "apartment";
            }
            switch (ObjectTypeSelect)
            {
                // Квартиры
                case "apartment":
                    filter.ObjTypes.Add((long)PropertyLivingCategory.Apartment);
                    if (CountRooms != null && CountRooms.Count != 0)
                    {
                        filter.CountRooms = CountRooms;
                        if (CountRooms.Contains(-1))
                        {
                            filter.CountRooms = null;
                        }
                        else if (CountRooms.Contains(999))
                        {
                            for (var i = 5; i < 20; i++)
                            {
                                filter.CountRooms.Add(i);
                            }
                        }
                    }
                    break;
                // комната
                case "room":
                    filter.ObjTypes.Add((long)PropertyLivingCategory.Room);
                    break;
                //Дом\коттедж
                case "house":
                    filter.ObjTypes.Add((long)PropertyLivingCategory.House);
                    filter.ObjTypes.Add((long)PropertyLivingCategory.HouseWithLot);
                    filter.ObjTypes.Add((long)PropertyLivingCategory.Cottage);
                    filter.ObjTypes.Add((long)PropertyLivingCategory.Townhouse);
                    filter.ObjTypes.Add((long)PropertyLivingCategory.Duplex);
                    break;
                case "house_part":
                    filter.ObjTypes.Add((long)PropertyLivingCategory.HousePart);
                    break;
                case "apartment_part":
                    filter.ObjTypes.Add((long)PropertyLivingCategory.ApartmentPart);
                    break;
                // Земля\участок
                case "lot":
                    filter.ObjTypes.Add((long)PropertyLivingCategory.Lot);
                    break;
                case "all":
                    if (filter.OfferCategory == (long) CoreMls.Enum.OfferCategory.Living)
                    {
                        foreach (var value in System.Enum.GetValues(typeof(PropertyLivingCategory)))
                        {
                            filter.ObjTypes.Add((long)(PropertyLivingCategory)value);
                            filter.CountRooms = null;
                        }
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException("ObjectTypeSelect", "С коммерцией еще не работаем");
                    }
                    break;
                default:
                    if (filter.UserId.HasValue || filter.OrganizationId.HasValue)
                        break;
                    throw new ValidationException("Указан не верный тип объекта", ObjectTypeSelect);
            }

            if (BuildYear != 0)
            {
                filter.BuildYearMin = BuildYear;
                filter.BuildYearMax = BuildYear;
            }
            else
            {
                if (BuildYearMin != 0)
                    filter.BuildYearMin = BuildYearMin;
                if (BuildYearMax != 0)
                    filter.BuildYearMax = BuildYearMax;
            }

            if (!string.IsNullOrEmpty(Districts))
            {
                filter.Districts = new List<int>();
                foreach (var id in Districts.Split(','))
                {
                    if (int.TryParse(id, out var result))
                        filter.Districts.Add(result);
                }
            }
            if (DistrictsArr != null)
            {
                filter.Districts.AddRange(DistrictsArr);
            }

            if (!string.IsNullOrEmpty(MicroDistricts))
            {
                filter.MicroDistricts = new List<int>();
                foreach (var id in MicroDistricts.Split(','))
                {
                    if (int.TryParse(id, out var result))
                        filter.MicroDistricts.Add(result);
                }
            }
            if (MicroDistrictsArr != null)
            {
                filter.MicroDistricts.AddRange(MicroDistrictsArr);
            }
#if DEBUG
            filter.Count = 5;
#else
            filter.Count = Count;
#endif
            //filter.DateBegin = DateTime.TryParse(DateFrom, out var dateBegin) ? dateBegin : DateTime.MinValue;
            //filter.DateEnd = DateTime.TryParse(DateEnd, out var dateEnd) ? dateEnd : DateTime.MinValue;
            return filter;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreMls.DB;
using CoreMls.DB.LocalEntities;
using CoreParser.DB;
using CoreParser.Pages;

namespace ApiService.Core.Cache
{
    public class SearchParserCache
    {
        public SearchFilter Filter { get; }
        public List<AbstractAdvertisement> Advertisments { get; }
        public int TotalCountByFilter { get; }

        public DateTime CreateDate { get; } = DateTime.Now;

        /// <summary>
        /// Устаревший ли кэш
        /// </summary>
        public bool IsOld => (DateTime.Now - CreateDate).TotalMinutes > 15;

        public SearchParserCache(SearchFilter filter, List<AbstractAdvertisement> advertisments, int totalCountByFilter)
        {
            Filter = filter;
            Advertisments = advertisments;
            TotalCountByFilter = totalCountByFilter;
        }
        

        protected bool Equals(SearchParserCache other)
        {
            return Equals(Filter, other.Filter);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Filter != null ? Filter.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Advertisments != null ? Advertisments.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ CreateDate.GetHashCode();
                return hashCode;
            }
        }
    }
}

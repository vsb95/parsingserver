﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using CoreMls.DB;
using CoreMls.DB.LocalEntities;
using CoreParser.DB;
using CoreParser.Pages;
using Logger;

namespace ApiService.Core.Cache
{
    public static class SearchCacheManager
    {
        private static readonly ConcurrentDictionary<int, SearchMlsCache> SearchMlsCacheDictionary = new ConcurrentDictionary<int, SearchMlsCache>();
        private static readonly ConcurrentDictionary<int, SearchParserCache> SearchParserCacheDictionary = new ConcurrentDictionary<int, SearchParserCache>();

        public static void AddOrUpdate(SearchMlsCache cache)
        {
            if(cache.MaxMinutesToDecay == 0)
                return;
            if (SearchMlsCacheDictionary.Count > 100)
            {
                Task.Factory.StartNew(() =>
                {
                    try
                    {
                        var oldKeys = new List<int>();
                        foreach (var pair in SearchMlsCacheDictionary)
                        {
                            if (pair.Value.IsOld)
                            {
                                oldKeys.Add(pair.Key);
                            }
                        }

                        foreach (var key in oldKeys)
                        {
                            SearchMlsCacheDictionary.TryRemove(key, out var val);
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Exception(ex, LogType.Error, true,
                            "ошибка при очистки кэша SearchMlsCacheDictionary. На текущий момент количество: " +
                            SearchMlsCacheDictionary.Count);
                    }
                });
            }
            if (SearchMlsCacheDictionary.TryAdd(cache.Filter.SerializedValue(), cache))
                return;
            if (!SearchMlsCacheDictionary.TryUpdate(cache.Filter.SerializedValue(), cache, cache))
            {
                Logger.Log.Error("Не удалось обновить кэш для поиска по mls: " + cache.Filter.GetHashCode());
            }
        }

        public static void AddOrUpdate(SearchParserCache cache)
        {
            if (SearchParserCacheDictionary.Count > 100)
            {
                Task.Factory.StartNew(() =>
                {
                    try
                    {
                        var oldKeys = new List<int>();
                        foreach (var pair in SearchParserCacheDictionary)
                        {
                            if (pair.Value.IsOld)
                            {
                                oldKeys.Add(pair.Key);
                            }
                        }

                        foreach (var key in oldKeys)
                        {
                            SearchParserCacheDictionary.TryRemove(key, out var val);
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Exception(ex, LogType.Error, true,
                            "ошибка при очистки кэша SearchParserCacheDictionary. На текущий момент количество: " +
                            SearchParserCacheDictionary.Count);
                    }
                });
            }
            if (SearchParserCacheDictionary.TryAdd(cache.Filter.SerializedValue(), cache))
                return;
            if (!SearchParserCacheDictionary.TryUpdate(cache.Filter.SerializedValue(), cache, cache))
            {
                Logger.Log.Error("Не удалось обновить кэш для поиска по парсеру: " + cache.Filter.GetHashCode());
            }
        }

        public static SearchParserCache GetCache(SearchFilter filter)
        {
            if (SearchParserCacheDictionary.TryGetValue(filter.SerializedValue(), out var result))
                return result.IsOld ? null : result;
            return null;
        }
        public static SearchMlsCache GetCache(SearchMlsFilter filter)
        {
            if (SearchMlsCacheDictionary.TryGetValue(filter.SerializedValue(), out var result))
                return result.IsOld ? null : result;
            return null;
        }

    }
}

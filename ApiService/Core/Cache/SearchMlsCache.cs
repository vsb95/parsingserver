﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreMls.DB;
using CoreMls.DB.LocalEntities;

namespace ApiService.Core.Cache
{
    public class SearchMlsCache
    {
        public int MaxMinutesToDecay { get; }
        public SearchMlsFilter Filter { get; }
        public List<MlsAdvertisment> Advertisments { get; }
        public int TotalCountByFilter { get; }
        public DateTime CreateDate { get; } = DateTime.Now;

        /// <summary>
        /// Устаревший ли кэш
        /// </summary>
        public bool IsOld => (DateTime.Now - CreateDate).TotalMinutes > MaxMinutesToDecay;

        public SearchMlsCache(SearchMlsFilter filter, List<MlsAdvertisment> advertisments, int totalCountByFilter,  int maxMinutesToDecay = 15)
        {
            Filter = filter;
            Advertisments = advertisments;
            TotalCountByFilter = totalCountByFilter;
            MaxMinutesToDecay = maxMinutesToDecay;
            if (filter.OrganizationId.HasValue || filter.UserId.HasValue || filter.Id.HasValue)
                MaxMinutesToDecay = 0;
        }
        
        protected bool Equals(SearchMlsCache other)
        {
            return Equals(Filter, other.Filter);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Filter != null ? Filter.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Advertisments != null ? Advertisments.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ CreateDate.GetHashCode();
                return hashCode;
            }
        }
    }
}

﻿using System;
using System.Web.Http;
using ApiService.Core;
using Core;
using CoreParser.Services.Api;
using Logger;

namespace ApiService.WebApi
{
    /// <summary>
    /// Контроллер для связи прода и тестового окружений
    /// </summary>
    public class PageupdateController : ApiController
    {
        /// <summary>
        /// Положить запрос в очередь на обновление
        /// </summary>
        /// <returns></returns>
        public void Post([FromBody] PageUpdateRequest request)
        {
            if (!SettingsManager.Settings.EnvironmentSettings.IsTestEnv)
            {
                Log.Warn("отправлен запрос на PageupdateController, но мы запущены как прод апи");
            }
            Log.Trace("Прислали на актуализацию: '" + request?.Url+"'");
            try
            {
                var pageUpdaterequest = request;// new PageUpdateRequest(request);
                if (string.IsNullOrEmpty(pageUpdaterequest?.Url))
                    return;
                Log.Trace("Прислали на актуализацию: "+ pageUpdaterequest.Url);
                EnvManager.AddRequest(pageUpdaterequest);
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "PageUpdateController::Post: ");
            }
        }

    }
}

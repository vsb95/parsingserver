﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using Core.Services;
using CoreMls.Managers;
using Logger;

namespace ApiService.WebApi
{
    public class EmailController : ApiController
    {
        //Отправка сообщения на почту риэлтору
        public string Post([FromBody]SendEmailRequest sendEmailRequest)
        {
            if (sendEmailRequest == null)
            {
                return "Неверные параметры";
            }
            // Подготовим пути к файлам
            if (sendEmailRequest.Attachements != null && sendEmailRequest.Attachements.Count > 0)
            {
                var newList = new List<string>();
                foreach (var attachement in sendEmailRequest.Attachements)
                {
                    var localPath = ImageManager.GlobalToLocalPath(attachement);
                    if (!string.IsNullOrEmpty(localPath))
                    {
                        newList.Add(localPath);
                    }
                    else
                    {
                        Log.Error("Файл '" + attachement + "' не найден", true);
                    }
                }
                sendEmailRequest.Attachements = newList;
            }
            // Подготовим пути к файлам
            if (sendEmailRequest.Images != null && sendEmailRequest.Images.Count > 0)
            {
                foreach (var image in sendEmailRequest.Images)
                {
                    var localPath = ImageManager.GlobalToLocalPath(image.Src);
                    if (!string.IsNullOrEmpty(localPath))
                    {
                        image.Src = localPath;
                    }
                    else
                    {
                        Log.Error("Файл '" + image + "' не найден", true);
                    }
                }
            }
            if (EmailManager.TrySendEmail(sendEmailRequest))
                return "Письмо успешно отправлено!";

            return "Не удалось отправить письмо по адресу '"+ sendEmailRequest.EmailTo + "'";
        }

    }
}

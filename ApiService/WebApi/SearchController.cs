using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using ApiService.Core;
using ApiService.Core.Cache;
using Core;
using Core.Enteties;
using Core.Enum;
using CoreParser.DB;
using CoreParser.DB.Managers;
using CoreParser.Pages;
using CoreParser.Services;
using CoreParser.Services.PropertyAnalizators;
using Logger;
using Newtonsoft.Json;
using City = CoreParser.DB.LocalEntities.City;
using District = Core.Enteties.District;
using MicroDistrict = Core.Enteties.MicroDistrict;

namespace ApiService.WebApi
{
   public class SearchController: ApiController
    {
        private class SearchResult : DefaultResult
        {
            [JsonProperty("total-count-by-filter")]
            public int TotalCountByFilter { get; set; }

            [JsonProperty("pages")]
            public List<AbstractAdvertisement> Pages { get; set; }
        }
        public class DetailsResult : DefaultResult
        {
            [JsonProperty("page")]
            public AbstractAdvertisement Page { get; set; }


            [JsonProperty("merged-pages")]
            public List<AbstractAdvertisement> MergedPages{ get; set; } = new List<AbstractAdvertisement>();
        }

        private class SearchInitResult : DefaultResult
        {
            [JsonProperty("sources")]
            public List<KeyValue> Sources { get; set; }

            [JsonProperty("cities")]
            public List<City> Cities { get; set; }

            [JsonProperty("adv-types")]
            public List<KeyValue> AdvTypes { get; set; }

            [JsonProperty("districts")]
            public List<District> Districts { get; set; }

            [JsonProperty("micro-districts")]
            public List<MicroDistrict> MicroDistricts { get; set; }

            [JsonProperty("sell-types")]
            public List<KeyValue> SellTypes { get; set; }
            [JsonProperty("house-types")]
            public List<KeyValue> HouseTypes { get; set; }
        }
        
        /// <summary>
        /// Поисковый запрос
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public string Post([FromBody] SearchRequest args)
        {
            var result = new SearchResult();
            if (string.IsNullOrWhiteSpace(args?.IdCity) || !int.TryParse(args?.IdCity, out var idcity))
            {
                result.Status = "Не выбран город в настройках организации";
                result.StatusTypeResult = StatusTypeResult.EMPTY;

                return JsonConvert.SerializeObject(result);
            }
            if (string.IsNullOrWhiteSpace(args?.ObjTypeSelect) || !int.TryParse(args?.ObjTypeSelect, out var objType))
            {
                result.Status = "Не выбран тип объявлений";
                result.StatusTypeResult = StatusTypeResult.EMPTY;

                return JsonConvert.SerializeObject(result);
            }
            if (string.IsNullOrEmpty(args.UserId))
            {
                Log.Warn("Не залогиненый пользователь!!! ");

                result.Status = "Вы не авторизованы";
                result.StatusTypeResult = StatusTypeResult.ERROR;

                return JsonConvert.SerializeObject(result);
            }

            try
            {
                Log.Debug("Search: " + args.DEBUG + "\n\tObjType: " + args.ObjTypeSelect);
                var filter = args.ToSearchFilter();
                var cache = SearchCacheManager.GetCache(filter);
                if (cache != null)
                {
                    result.Pages = cache.Advertisments;
                    result.TotalCountByFilter = cache.TotalCountByFilter;
                }
                else
                {
                    var res = DbSearchManager.SearchPages(filter, out var totalCountByFilter);
                    result.Pages = res;
                    result.TotalCountByFilter = totalCountByFilter;
                    cache = new SearchParserCache(filter, res, totalCountByFilter);
                    SearchCacheManager.AddOrUpdate(cache);
                }
                if (result.Pages.Count != 0)
                {
                    if (args.ItIsForCreatedReport != 0)
                    {
                        Log.Debug("Создание отчета по фильтру (recreate)");
                        var pageManagerResult = PageManager.GetAdvertisementsById(result.Pages.Where(x => x.Id.HasValue).Select(x => x.Id.Value), args.IsCutLogo != 0);
                        foreach (var abstractPage in pageManagerResult.Advertisements)
                        {
                            AdvertismentPreparer.Prepare(abstractPage, true);
                        }

                        result.Pages = pageManagerResult.Advertisements.ToList();
                    }
                    else
                    {
                        foreach (var abstractPage in result.Pages)
                        {
                            if (abstractPage?.DownloadedImagesPathList == null)
                                continue;
                            AdvertismentPreparer.Prepare(abstractPage,false);
                        }
                    }
                }
            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
                result.Status = "Ошибка API: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
                result.Status = "Ошибка API: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }

            Log.Info(string.Format("[User: {0}] Search end; Total: {1} Sec: {2}", args.UserId, result.Pages?.Count, result.TotalSec));
            var answer = JsonConvert.SerializeObject(result);
            return answer;
        }

        /// <summary>
        /// Инициализация формы поиска
        /// </summary>
        /// <param name="idCity"></param>
        /// <returns></returns>
        public string Get(int? idCity = null)
        {
            var result = new SearchInitResult();
            try
            {
                Log.Trace("Init Search: GetObjTypes" +
                          (idCity != null ? " & GetDistricts & GetMicroDistricts" : " город не указан..."));
                result.AdvTypes = DbSearchManager.GetObjTypes();
                result.Sources = DbSearchManager.GetSources();
                result.SellTypes = DbSearchManager.GetSellTypes();
                result.HouseTypes = DbSearchManager.GetHouseTypes();
                if (idCity != null)
                {
                    result.Districts = DbSearchManager.GetDistricts(idCity.Value);
                    result.MicroDistricts = DbSearchManager.GetMicroDistricts(idCity.Value);
                }
                result.StatusTypeResult = StatusTypeResult.OK;
            }
            catch (Exception ex)
            {
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = ex.Message;
                Log.Exception(ex, LogType.Error, true);
            }
            var answer = JsonConvert.SerializeObject(result);
            return answer;
        }

        /// <summary>
        /// Детализация 1 объявления
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string Get(int id, string userId)
        {
            var result = new DetailsResult();
            if (string.IsNullOrEmpty(userId))
            {
                Log.Warn("Не залогиненый пользователь!!! ");

                result.Status = "Вы не авторизованы";
                result.StatusTypeResult = StatusTypeResult.ERROR;
                return JsonConvert.SerializeObject(result);
            }
            try
            {
                var abstractPage = DbManager.GetAbstractAdvertisementById(id);
                if (abstractPage == null)
                {
                    result.StatusTypeResult = StatusTypeResult.EMPTY;
                    result.Status = "Объявление не найдено";
                }
                else
                {
                    //if (userId == "1")
                    {
                        var oldIsAgency = abstractPage.IsAgency;
                        var agencyManager = new Agency2Manager();
                        agencyManager.Identify(abstractPage);
                        abstractPage.IsAgency = oldIsAgency;
                        abstractPage.DebugInfo = agencyManager.DebugInfo.Replace("\n","<br>");
                        var mergedPages = DbMergeManager.GetMergedPages(abstractPage);
                        if (mergedPages.Count > 0)
                        {
                            foreach (var mergedPage in mergedPages)
                            {
                                AdvertismentPreparer.Prepare(mergedPage, false);
                                result.MergedPages.Add(mergedPage);
                            }
                        }
                    }
                    AdvertismentPreparer.Prepare(abstractPage, false);
                    result.Page = abstractPage;
                    result.StatusTypeResult = StatusTypeResult.OK;
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
                result.Status = "Ошибка API: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }
            
            Log.Info(string.Format("[User: {0}] Get detail view ({2}) end. Sec: {1}", userId, result.TotalSec, id));
            var answer = JsonConvert.SerializeObject(result);
            return answer;
        }
    }
}

﻿using System.Collections.Generic;
using System.Web.Http;
using Core;
using CoreParser.DB.Managers;

namespace ApiService.WebApi
{
    public class ObjTypesController : ApiController
    {
        public List<KeyValue> Get()
        {
            return DbSearchManager.GetObjTypes();
        }

    }
}

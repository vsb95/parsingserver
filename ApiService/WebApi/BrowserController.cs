﻿using System;
using System.Web.Http;
using ApiService.Core;
using Core.Enteties;
using Core.Enum;
using CoreParser.DB.Managers;
using Logger;
using Newtonsoft.Json;

namespace ApiService.WebApi
{
    public class BrowserController : ApiController
    {
        /// <summary>
        /// запрос на сохранение определенного номера телефона
        /// </summary>
        public class SavePhoneRequest
        {
            public int? Id { get; set; }
            public string Phone { get; set; }
            public string Url { get; set; }
        }

        /// <summary>
        /// Сохранение номера телефона для конкретного объявления
        /// </summary>
        /// <param name="savePhoneRequest"></param>
        /// <returns></returns>
        public string Post([FromBody] SavePhoneRequest savePhoneRequest)
        {
            DefaultResult result = new DefaultResult();
            if (savePhoneRequest == null)
            {
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = "Неверные параметры";
                return JsonConvert.SerializeObject(result);
            }

            try
            {
                Log.Trace("Сохранение телефона '" + savePhoneRequest.Phone + "' для " + savePhoneRequest.Id);
                if (savePhoneRequest.Id.HasValue)
                    DbActualizeManager.SavePhone(savePhoneRequest.Id.Value, savePhoneRequest.Phone);
                else
                    DbActualizeManager.SavePhone(savePhoneRequest.Url, savePhoneRequest.Phone);
                result.StatusTypeResult = StatusTypeResult.OK;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = ex.Message;
            }

            var answer = JsonConvert.SerializeObject(result);
            return answer;
        }

        /// <summary>
        /// Загрузить страницу через браузер (ЦИАН например)
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string Get(string url)
        {
            // Нужен BrowserDiscovery, чтобы по очереди их запросить скачать страницы.
            // Для этого им нужно реализовать WebApi
            throw new NotImplementedException();
        }
    }
}

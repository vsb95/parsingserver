﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using ApiService.Core;
using Core.Enteties;
using Core.Enum;
using CoreParser.DB;
using CoreParser.DB.LocalEntities;
using CoreParser.DB.Managers;
using Logger;
using Newtonsoft.Json;

namespace ApiService.WebApi
{
    public class CitiesController : ApiController
    {
        internal class CitiesResult : DefaultResult
        {
            [JsonProperty("cities")]
            internal List<City> Cities { get; set; } = new List<City>();
        }
        /// <summary>
        /// Получить список доступных городов в системе
        /// </summary>
        /// <returns></returns>
        public string Get()
        {
            var result = new CitiesResult();
            try
            {
                Log.Trace("GetCities");
                result.Cities = DbSearchManager.GetCities();
                result.StatusTypeResult = StatusTypeResult.OK;
            }
            catch (Exception ex)
            {
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = ex.Message;
                Log.Exception(ex, LogType.Error, true);
            }
            var answer = JsonConvert.SerializeObject(result);
            return answer;
        }


        /// <summary>
        /// Получить название города по его id
        /// </summary>
        /// <returns></returns>
        public DefaultResult Get(int id)
        {
            var result = new DefaultResult();
            try
            {
                Log.Trace("GetCities Name = "+id);
                result.Status = DbObjectManager.GetNameCity(id);
                result.StatusTypeResult = StatusTypeResult.OK;
            }
            catch (Exception ex)
            {
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = ex.Message;
                Log.Exception(ex, LogType.Error, true);
            }
            return result;
        }
    }
}

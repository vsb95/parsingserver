﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using ApiService.Core;
using Core;
using Core.Enteties;
using Core.Enum;
using CoreParser.DB.LocalEntities;
using CoreParser.DB.Managers;
using Logger;
using Newtonsoft.Json;

namespace ApiService.WebApi
{
    public class MicrodistrictController : ApiController
    {
        public class WithoutDistrictRequest
        {
            public int IsOnlyWithoutRegion { get; set; } = 1;
            public int IsOnlyWithoutMicroRegion { get; set; } = 1;
            public int idCity { get; set; } = -1;

            /// <summary>
            /// Тип объявления (дача\коттедж)
            /// </summary>
            public List<int> ObjTypes { get; set; }
        }
        private class DistrictResult : DefaultResult
        {
            [JsonProperty("districts")]
            public List<District> Districts { get; set; }

            [JsonProperty("microdistricts")]
            public List<MicroDistrict> MicroDistricts { get; set; }
        }

        public class WithoutDistrictResult : DefaultResult
        {
            [JsonProperty("coordinates")]
            public List<KeyValuePair<GeoCoordinate, string>> Coordinates { get; set; } = new List<KeyValuePair<GeoCoordinate, string>>();
        }

        /// <summary>
        /// Получить список районов и микрорайонов для города, если указан
        /// </summary>
        /// <param name="idCity"></param>
        /// <returns></returns>
        public string Get(int idCity = -1)
        {
            var result = new DistrictResult();
            try
            {
                Log.Trace("Get Microdistrict idCity = "+ idCity);

                result.Districts = DbSearchManager.GetDistricts(idCity);
                result.MicroDistricts = DbSearchManager.GetMicroDistricts(idCity);
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true);
            }
            var answer = JsonConvert.SerializeObject(result);
            return answer;
        }

        /// <summary>
        /// Получить список координат без мкр\района 
        /// </summary>
        /// <returns>коллекция координат без мкр\района</returns>
        public async Task<WithoutDistrictResult> Post(WithoutDistrictRequest request)
        {
            var result = new WithoutDistrictResult();
            try
            {
                int top = 250;
#if DEBUG
                top = 10;
#endif
                var advList =
                    await Task.Factory.StartNew(() =>
                        DbActualizeManager.GetPagesWithoutDistrict(request.idCity, request.IsOnlyWithoutRegion != 0, request.IsOnlyWithoutMicroRegion != 0, top,
                            request.ObjTypes));
                
                foreach (var adv in advList)
                {
                    if(adv?.Adress?.GeoCoordinate == null || adv.Adress.GeoCoordinate.IsEmpty) continue;
                    var description = string.Format("<a href=\"{2}\" target=\"_blank\">{4}<br/>[{0}] {1}</a><br/>Обновлено: {3}", adv.Adress.GeoCoordinate,
                        adv.Adress.Street,adv.Url, adv.LastRefreshDateTime,adv.JsonObjType);
                    result.Coordinates.Add(new KeyValuePair<GeoCoordinate, string>(adv.Adress.GeoCoordinate, description));
                }

                result.StatusTypeResult = StatusTypeResult.OK;
            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
                result.Status = "Ошибка API: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
                result.Status = "Ошибка API: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }
            return result;
        }
    }
}

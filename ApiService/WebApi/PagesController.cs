﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using ApiService.Core;
using Core.Enteties;
using Core.Enum;
using CoreParser.DB;
using CoreParser.DB.Managers;
using CoreParser.Pages;
using CoreParser.Services;
using Logger;
using Newtonsoft.Json;

namespace ApiService.WebApi
{
   public class PagesController: ApiController
    {

        public class ParseRequest
        {
            public List<int> IdList { get; set; }
            public string Data { get; set; }
            public string UserId { get; set; }
            public int IsCreateZip { get; set; }
            public int IsActualize { get; set; } = 0;
            public int IsCutLogo { get; set; } = 0;
        }
        
        public class ParseResult : DefaultResult
        {
            [JsonProperty("pages")]
            public List<AbstractAdvertisement> Pages { get; set; } = new List<AbstractAdvertisement>();
            [JsonProperty("unparsed-urls")]
            public ConcurrentBag<string> UnParsedUrList { get; set; } = new ConcurrentBag<string>();

            [JsonProperty("zip-file")]
            public string ZipFile { get; set; }
        }

        /// <summary>
        /// Work page формирование отчета\архива с фото
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public string Post([FromBody] ParseRequest args)
        {
            var result = new ParseResult();
            if (string.IsNullOrWhiteSpace(args?.Data))
            {
                result.Status = "Ссылок нет";
                result.StatusTypeResult = StatusTypeResult.EMPTY;

                return JsonConvert.SerializeObject(result);
            }

            if (string.IsNullOrEmpty(args.UserId))
            {
                Log.Warn("Не залогиненый пользователь!!! ");

                result.Status = "Вы не авторизованы";
                result.StatusTypeResult = StatusTypeResult.ERROR;

                return JsonConvert.SerializeObject(result);
            }
            
            try
            {
                var listUrl = args.Data.Split('\n').Where(link => !string.IsNullOrEmpty(link)).ToList();
                Log.Info(string.Format("[User: {0}] {2} start: \n{1}", args?.UserId, string.Join("\n", listUrl),
                    (args.IsCreateZip == 1 ? "Zip" : "Parsing")));
                if (args.IsCreateZip == 1)
                {
                    var res = PageManager.GetImages(listUrl);
                    result.ZipFile = Zipper.CreateZip(res);
                }
                else
                {
                    var res = PageManager.GetAdvertisementsByUrl(listUrl, args.IsActualize != 1 && args.IsCutLogo != 0);

                    #region Сортировка по пришедшему порядку

                    var advertismentsList = res.Advertisements.ToList();
                    foreach (var originalUrl in listUrl)
                    {
                        var searchIndexOrigin = AbstractAdvertisement.GetSearchIndex(originalUrl);
                        if(string.IsNullOrEmpty(searchIndexOrigin))
                            continue;
                        AbstractAdvertisement advertisement = null;
                        for (var i = 0; i < advertismentsList.Count; i++)
                        {
                            var adv = advertismentsList[i];
                            if (adv.SearchIndex != searchIndexOrigin) continue;
                            advertisement = adv;
                            advertismentsList.RemoveAt(i);
                            break;
                        }

                        if (advertisement != null)
                        {
                            result.Pages.Add(advertisement);
                        }
                        else
                        {
                            Log.Warn("Не определен порядок для страницы (если доска, то окей): " + originalUrl);
                        }
                    }
                    // все беспорядочные страницы добавим в конец
                    result.Pages.AddRange(advertismentsList);
                    
                    #endregion

                    result.UnParsedUrList = res.UnParsedUrList;
                }

                if (args.IsActualize == 1)
                {
                    Log.Debug("Актуализация");
                    for (var i = 0; i < result.Pages.Count; i++)
                    {
                        var adv = result.Pages[i];
                        if (adv.CheckActualy())
                        {
                            DbManager.AddOrUpdatePage(adv);
                            if (!DbMergeManager.MergeAdvertisment(adv.Id.Value))
                            {
                                Log.Debug("Объявление не склеено: "+ adv.Id.Value);
                            }
                        }
                        else
                            result.UnParsedUrList.Add("Не удалось актуализировать: " + adv.Url);
                    }
                }
                if (result.Pages.Count != 0)
                {
                    foreach (var adv in result.Pages)
                    {
                        AdvertismentPreparer.Prepare(adv, true);
                    }
                }

                result.StatusTypeResult = StatusTypeResult.OK;
            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
                result.Status = "Ошибка API: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
                result.Status = "Ошибка API: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }
            
            if (args.IsCreateZip == 1)
            {
                Log.Info(string.Format("[User: {0}] Zip end. Sec: {1}", args?.UserId, result.TotalSec));
            }
            else
            {
                Log.Info(string.Format("[User: {0}] Parsing end. Parsed = {1}/{2}; Unparsed = {3}/{2}; Sec: {4}",
                    args?.UserId,
                    result.Pages.Count, result.Pages.Count + result.UnParsedUrList.Count, result.UnParsedUrList.Count,
                    result.TotalSec));
            }
            var answer = JsonConvert.SerializeObject(result);
            Log.Trace("Отправили юзеру [" + args?.UserId + "] json: \n" + answer);
            return answer;
        }
        
        /// <summary>
        /// Формиование отчета из списка ID объявлений с доски объявлений
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public string Put([FromBody]ParseRequest args)
        {
            var result = new ParseResult();
            if (args?.IdList == null || args.IdList.Count == 0)
            {
                result.Status = "Нет выбранных объявлений";
                result.StatusTypeResult = StatusTypeResult.EMPTY;

                return JsonConvert.SerializeObject(result);
            }

            if (string.IsNullOrEmpty(args?.UserId))
            {
                Log.Warn("Не залогиненый пользователь!!! ");

                result.Status = "Вы не авторизованы";
                result.StatusTypeResult = StatusTypeResult.ERROR;

                return JsonConvert.SerializeObject(result);
            }
            
            try
            {
                var res = PageManager.GetAdvertisementsById(args.IdList, args.IsCutLogo != 0);
                foreach (var badId in res.UnParsedUrList)
                    result.UnParsedUrList.Add(badId);
                result.Pages = res.Advertisements.ToList();
                
                if (result.Pages.Count != 0)
                {
                    foreach (var abstractPage in result.Pages)
                    {
                        AdvertismentPreparer.Prepare(abstractPage, true);
                    }
                    result.StatusTypeResult = StatusTypeResult.OK;
                }
                else
                {
                    result.StatusTypeResult = StatusTypeResult.EMPTY;
                    result.Status = "не удалось распознать ниодного объявления";
                }

            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
                result.Status = "Ошибка API: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
                result.Status = "Ошибка API: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }
            
            Log.Info(string.Format("[User: {0}] Put end. Founded = {1}/{2}; UnFounded = {3}/{2}; Sec: {4}",
                args?.UserId,
                result.Pages.Count, result.Pages.Count + result.UnParsedUrList.Count, result.UnParsedUrList.Count,
                result.TotalSec));
            var answer = JsonConvert.SerializeObject(result);
            return answer;
        }
    }
}

﻿using System;
using System.Web.Http;
using ApiService.Core;
using ApiService.Core.MLS;
using Core.Enteties;
using Core.Enum;
using Core.Exceptions;
using CoreMls.DB.Managers;
using Logger;

namespace ApiService.WebApi.MLS
{
    public class UpdateMlsOfferController : ApiController
    {
        /// <summary>
        /// Сделать пометку (В архив и тп)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="advId"></param>
        /// <returns></returns>
        public DefaultResult Get(long userId, long advId, string action)
        {
            var result = new DefaultResult();
            if (userId == 0)
            {
                Log.Warn("Не залогиненый пользователь!!! ");

                result.Status = "Вы не авторизованы";
                result.StatusTypeResult = StatusTypeResult.ERROR;
                return result;
            }
            if (advId == 0)
            {
                Log.Warn("Не выбрано объявление для метода '"+action+"' !!! ");

                result.Status = "Неизвестное объявление";
                result.StatusTypeResult = StatusTypeResult.ERROR;
                return result;
            }

            if (string.IsNullOrEmpty(action))
            {
                Log.Warn("Не выбрано действие для объявления !!! ");

                result.Status = "Неизвестное действие";
                result.StatusTypeResult = StatusTypeResult.ERROR;
                return result;
            }

            try
            {
                Log.Debug("Update MLS adv : " + advId);
                var adv = DbOfferManager.GetAdvById(advId);
                if (adv == null)
                {
                    Log.Warn("Не найдено объявление для апдейта!!! ");
                    result.Status = "Неизвестное объявление";
                    result.StatusTypeResult = StatusTypeResult.ERROR;
                    return result;
                }

                if (adv.SalesAgent.Id != userId)
                {
                    var owner = DbSalesAgentManager.GetAgentById(userId);
                    if (owner == null || owner.Organization.Id != adv.SalesAgent.Organization.Id)
                    {
                        Log.Error("Попытка обновить чужое объявление!!! advId: "+ advId+ " userId: "+ userId+ " owner.Organization.Id: "+ owner?.Organization.Id+ " adv.SalesAgent.Organization.Id: "+ adv.SalesAgent.Organization.Id, true);
                        result.Status = "Это чужое объявление. Либо вы не привязаны к данной организации. Заполните свой профиль в настройках";
                        result.StatusTypeResult = StatusTypeResult.ERROR;
                        return result;
                    }

                    Log.Info("Редактирование объявления админом adv: " + advId + " action: " + action);
                }

                switch (action)
                {
                    case "in-archive":
                        adv.DeleteDate = DateTime.Now;
                        break;
                    case "out-archive":
                        adv.DeleteDate = null;
                        break;
                    case "delete":
                    {
                        if (DbOfferManager.TryDelete(adv.Id.Value, userId))
                        {
                            result.Status = "Объявление успешно удалено!";
                            result.StatusTypeResult = StatusTypeResult.OK;
                        }
                        else
                        {
                            result.Status = "Не удалось удалить объявление. Попробуйте еще раз";
                            result.StatusTypeResult = StatusTypeResult.ERROR;
                        }

                        return result;
                    }
                    default:
                        Log.Warn("Неизвестное действие '" + action + "' !!! ");
                        result.Status = "Неизвестное действие";
                        result.StatusTypeResult = StatusTypeResult.ERROR;
                        return result;
                }

                if (DbOfferManager.AddOrUpdatePage(adv))
                {
                    result.Status = "Объявление успешно обновлено!";
                    result.StatusTypeResult = StatusTypeResult.OK;
                }
                else
                {
                    result.Status = "Не удалось обновить объявление. Попробуйте еще раз";
                    result.StatusTypeResult = StatusTypeResult.ERROR;
                }
            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
                result.Status = "Ошибка API: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
                result.Status = "Ошибка API: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }
            finally
            {
                Log.Info($"[User: {userId}] Update '{action}' MLS adv end; Sec: {result.TotalSec}");
            }

            return result;
        }
        
        public DefaultResult Post([FromBody] UpdateMlsCommercialOfferRequest args)
        {
            var result = new DefaultResult();
            
            if (args?.User?.Id == null)
            {
                Log.Warn("Не залогиненый пользователь!!! ");

                result.Status = "Вы не авторизованы";
                result.StatusTypeResult = StatusTypeResult.ERROR;
                return result;
            }
            if (args.Id == null)
            {
                Log.Warn("Не выбрано объявление для апдейта!!! ");

                result.Status = "Неизвестное объявление";
                result.StatusTypeResult = StatusTypeResult.ERROR;
                return result;
            }

            try
            {
                Log.Debug("Update MLS : "+args.Id);
                var adv = DbOfferManager.GetAdvById(args.Id.Value);
                if (adv == null)
                {
                    Log.Warn("Не найдено объявление для апдейта!!! ");
                    result.Status = "Неизвестное объявление";
                    result.StatusTypeResult = StatusTypeResult.ERROR;
                    return result;
                }

                if (adv.SalesAgent.Id != args.User.Id)
                {
                    var agent = DbSalesAgentManager.GetAgentById(args.User.Id);
                    if (agent == null || agent.Organization.Id != adv.SalesAgent.Organization.Id)
                    {
                        Log.Error("Попытка обновить чужое объявление!!! ", true);
                        result.Status = "Это чужое объявление. Как вы посмели??!";
                        result.StatusTypeResult = StatusTypeResult.ERROR;
                        return result;
                    }
                    Log.Info("Редактирование объявления админом adv: " + adv.Id);
                }

                if (args.TryUpdate(out var error))
                {
                    result.Status = "Объявление успешно обновлено!";
                    result.StatusTypeResult = StatusTypeResult.OK;
                }
                else
                {
                    result.Status = !string.IsNullOrEmpty(error)? error : "Не удалось обновить объявление. Попробуйте еще раз";
                    result.StatusTypeResult = StatusTypeResult.ERROR;
                }
            }
            catch (ValidationException ex)
            {
                result.Status = "Ошибка в объявлении: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
                result.Status = "Ошибка API: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
                result.Status = "Ошибка API: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }

            Log.Info($"[User: {args.User?.Id}] Update MLS adv end; Sec: {result.TotalSec}");
            return result;
        }
    }
}

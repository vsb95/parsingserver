﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using ApiService.Core;
using CoreMls.DB.Managers;
using Logger;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using Core.Enteties;
using Core.Enum;
using CoreMls.DB.LocalEntities.Report;

namespace ApiService.WebApi.MLS
{
    public class ReportController :ApiController
    {
        public ReportResult Get(string type, string beginDate = null, string endDate = null)
        {
            DateTime? beginDt = null;
            DateTime? endDt = null;
            var result = new ReportResult();
            try
            {
                if (!string.IsNullOrEmpty(beginDate))
                {
                    if (DateTime.TryParse(beginDate, out var date))
                        beginDt = date;
                    else
                        throw new Exception("не верно указана дата начала: " + beginDate);
                }

                if (!string.IsNullOrEmpty(endDate))
                {
                    if (DateTime.TryParse(endDate, out var date))
                        endDt = date;
                    else
                        throw new Exception("не верно указана дата окончания: " + endDate);
                }

                Log.Info("Start create report: '" + type + "' (" + beginDt?.ToString("g") + "; " + endDt?.ToString("g") + ")");
                List<object> reportResult = null;
                switch (type?.ToLower())
                {
                    case "count-offer-per-org":
                    {
                        reportResult = DbReportManager.GetCountOfferPerOrg(beginDt, endDt)?.Select(x=>(object)x).ToList();
                        break;
                    }
                    default: throw new Exception("Указан неизвестный тип отчета");
                }

                result.Rows = reportResult ?? throw new Exception("Ошибка при формировании отчета. Нужен разраб");
                result.Status = "OK";
                result.StatusTypeResult = StatusTypeResult.OK;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "[ReportController::Get] '"+type+"' ("+beginDt?.ToString("g")+"; "+endDt?.ToString("g")+")");
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = ex.Message;
            }

            Log.Info("End create report '" + type + "': " + result.Status);
            return result;
        }
    }
    
    public class ReportResult:DefaultResult
    {
        [JsonProperty("rows")]
        public List<object> Rows { get; set; }
    }
}

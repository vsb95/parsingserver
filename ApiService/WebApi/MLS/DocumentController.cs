﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using ApiService.Core;
using Core.Enteties;
using Core.Enum;
using Core.Services;
using HiQPdf;
using Logger;
using Exception = System.Exception;

namespace ApiService.WebApi.MLS
{
    public class DocumentController : ApiController
    {
        /// <summary>
        /// Конвертация html в файл
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public DefaultResult Post([FromBody]DocumetCreateRequest request)
        {
            var result = new DefaultResult();
            result.StatusTypeResult = StatusTypeResult.ERROR;
            if (string.IsNullOrEmpty(request?.Html))
            {
                result.StatusTypeResult = StatusTypeResult.EMPTY;
                result.Status = "Не указан html";
                return result;
            }
            if (string.IsNullOrEmpty(request?.Type))
            {
                result.StatusTypeResult = StatusTypeResult.EMPTY;
                result.Status = "Не указан Type";
                return result;
            }

            try
            {
                request.Destination = Randomizer.CreateEmptyFilename(request.Type.ToLower(), request.Wanted, request.Destination).FullName;
                switch (request.Type.ToLower())
                {
                    case "pdf":
                    {
                        if (!request.Destination.ToLower().EndsWith(".pdf"))
                        {
                            result.StatusTypeResult = StatusTypeResult.ERROR;
                            result.Status = "Не указан тип файла в Destination или Wanted";
                            return result;
                        }

                        HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
                            // set PDF page margins
                        htmlToPdfConverter.Document.PageSize = PdfPageSize.A4;
                            htmlToPdfConverter.Document.Margins = new PdfMargins(5, 5, 10, 5);
                        htmlToPdfConverter.ConvertHtmlToFile(request.Html,"", request.Destination);
                        break;
                    }
                    default:
                    {
                        result.StatusTypeResult = StatusTypeResult.EMPTY;
                        result.Status = "Указан неизвестный тип файла";
                        return result;
                    }
                }
                result.StatusTypeResult = StatusTypeResult.OK;
                result.Status = request.Destination;
            }
            catch (Exception ex)
            {
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = ex.Message;
                Log.Exception(ex, LogType.Error, true);
            }

            return result;
        }
    }

    public class DocumetCreateRequest
    {
        public string Html { get; set; }
        public string Type { get; set; }
        public string Wanted { get; set; }
        public string Destination { get; set; }
    }
}

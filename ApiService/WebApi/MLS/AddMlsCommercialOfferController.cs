﻿using System;
using System.Web.Http;
using ApiService.Core;
using ApiService.Core.MLS;
using Core.Enteties;
using Core.Enum;
using Core.Exceptions;
using CoreMls.DB.Managers;
using Logger;

namespace ApiService.WebApi.MLS
{
    public class AddMlsCommercialOfferController : ApiController
    {
        public DefaultResult Post([FromBody] AddMlsCommercialOfferRequest args)
        {
            var result = new DefaultResult();
            /*if (args == null || args.IdCity == 0)
            {
                result.Status = "Не выбран город в настройках организации";
                result.StatusType = StatusType.EMPTY;
                return result;
            }*/
            
            if (args.User?.Id == null)
            {
                Log.Warn("Не залогиненый пользователь!!! ");

                result.Status = "Вы не авторизованы";
                result.StatusTypeResult = StatusTypeResult.ERROR;
                return result;
            }

            try
            {
                Log.Debug("Start Add MLS : "+args.Address);
                if (args.TryParseToAdv(null, out var adv, out var error) && DbOfferManager.AddOrUpdatePage(adv))
                {
                    result.Status = "Объявление успешно добавлено!";
                    result.StatusTypeResult = StatusTypeResult.OK;
                }
                else
                {
                    result.Status = !string.IsNullOrEmpty(error)? error : "Внутренная ошибка: Не удалось добавить объявление. Попробуйте еще раз или обратитесь в техподдержку";
                    result.StatusTypeResult = StatusTypeResult.ERROR;
                }
            }
            catch (ValidationException ex)
            {
                result.Status = "Ошибка в объявлении: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
                result.Status = "Ошибка API: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
                result.Status = "Ошибка API: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }

            Log.Info($"[User: {args.User?.Id}] Add MLS adv end; Sec: {result.TotalSec}");
            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;
using ApiService.Core;
using ApiService.Core.MLS;
using Core;
using Core.Enteties;
using Core.Enum;
using Core.Services;
using CoreMls.DB.LocalEntities;
using CoreMls.DB.Managers;
using CoreMls.DB.Model;
using CoreMls.Enum;
using CoreMls.Managers;
using CoreParser.DB.LocalEntities;
using Logger;
using Newtonsoft.Json;

namespace ApiService.WebApi.MLS
{
    public class FeedController : ApiController
    {
        public class FeedResult : DefaultResult
        {
            [JsonProperty("file")]
            public string File { get; set; }
            [JsonProperty("create-date")]
            public string CreateDate { get; set; }
            [JsonProperty("agent-associations")]
            public Dictionary<SalesAgent, long?> Associations { get; set; }

            [JsonProperty("auto-import-feeds")]
            public List<string> AutoImportFeeds { get; set; }
        }
        public class FeedImport 
        {
            public string Src { get; set; }
            public long? IdOrg { get; set; }
            public string FeedbackEmailTo { get; set; }
            public int IsAuto { get; set; }
        }
        public class FeedAssociation
        {
            public long? IdOrg { get; set; }
            public List<KeyValuePair<long,long>> Associations { get; set; } = new List<KeyValuePair<long, long>>();
            public List<User> Users { get; set; } = new List<User>();
        }

        public FeedResult Get(int idOrg)
        {
            if (idOrg == 0)
                return new FeedResult { Status = "Не указана организация", StatusTypeResult = StatusTypeResult.EMPTY};

            var result = new FeedResult();
            try
            {
                if (!FeedManager.TryGetOrCreateFeed(idOrg, out var file, out var createDate))
                    return new FeedResult { Status = "Не удалось найти/сформировать фид", StatusTypeResult = StatusTypeResult.ERROR };
                result.Associations = FeedManager.GetAgentAssociation(idOrg);
                result.File = file.Replace(SettingsManager.Settings.ServerSettings.MlsFeedFolder,"").Replace("\\","");
                result.AutoImportFeeds = FeedManager.GetAutoImportFeeds(idOrg);
                result.CreateDate = createDate.ToString("dd.MM.yyyy HH:mm");
                result.StatusTypeResult = StatusTypeResult.OK;
            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
                result.Status = "Ошибка API: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
                result.Status = "Ошибка API: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }

            Log.Info(string.Format("[org: {0}] Get Feed MLS end; Sec: {1}", idOrg,  result.TotalSec));
            return result;
        }

        public DefaultResult Post([FromBody] FeedImport feedImport)
        {
            DefaultResult result = new DefaultResult();
            if (string.IsNullOrEmpty(feedImport?.Src))
            {
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = "Не указан источник";
                return result;
            }
            if (string.IsNullOrEmpty(feedImport?.FeedbackEmailTo))
            {
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = "Не указан email на который прислать результат";
                return result;
            }
            if (!feedImport.IdOrg.HasValue)
            {
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = "Не указана организация";
                return result;
            }

            var org = DbOrganizationManager.GetOrganizationById(feedImport.IdOrg.Value);
            if (org == null)
            {
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = "Укажите данные организации в настройках прежде чем продолжить";
                return result;
            }

#if !DEBUG
            if (feedImport.Src.Contains("rieltor-service.ru"))
            {
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = "<p style=\"font-size: 24px;\"><br>Вы пытаетесь импортировать свой фид с нашего сервиса.<br><br>Когда вы так делаете где то в мире грустит один котик<br>Не надо так<br><br></p><img src=\"/assets/img/sad_cat.jpg\" style=\"width:300px\"/>";
                return result;
            }
#endif

            try
            {
                TimeSpan delay = new TimeSpan(0, 30, 0);
#if DEBUG
                delay = new TimeSpan(0, 0, 10);
#endif
                var startDt = DateTime.Now;
                Sheduler.AddOperation(delay, false, () =>
                {
                    if (FeedManager.TryImport(FeedSite.Yandex, feedImport.Src, feedImport.IdOrg.Value, out var importResult))
                    {
                        Log.Info("Загружено " + importResult.Offers.Count + " объявлений; " + importResult.Errors.Count + " ошибок");
                        if (feedImport.IsAuto != 0)
                        {
                            FeedManager.AddAutoImportFeed(feedImport.Src, feedImport.IdOrg.Value, feedImport.FeedbackEmailTo, FeedSite.Yandex);
                        }
                    }

                    EmailManager.TrySendEmail(new SendEmailRequest
                    {
                        Caption = "[Rieltor-service.ru] Отчет по загрузке фида от " + startDt.ToString("g"),
                        Content = importResult.ToHtml(feedImport.Src),
                        EmailTo = feedImport.FeedbackEmailTo,
                        BccList = new List<string> { "rieltorservice@ya.ru" }
                    });

                }, false, "FeedImport: "+ org);
                result.StatusTypeResult = StatusTypeResult.OK;
                result.Status = "Ваш фид поставлен в очередь на импорт. Импорт будет запущен через " + delay+".<br>Отчет придет на вашу почту: "+ feedImport.FeedbackEmailTo;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "[FeedController::Post]");
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = ex.Message;
            }

            return result;
        }

        public DefaultResult Put([FromBody] FeedAssociation feedAssociation)
        {
            DefaultResult result = new DefaultResult();
            if (feedAssociation == null || feedAssociation.Associations.Count <1)
            {
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = "Не указан список ассоциаций";
                return result;
            }
            if (!feedAssociation.IdOrg.HasValue)
            {
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = "Не указана организация";
                return result;
            }

            try
            {
                Log.Debug("Start update feed association: org= " + feedAssociation.IdOrg);
                foreach (var pair in feedAssociation.Associations)
                {
                    SalesAgent salesAgnet = null;
                    if (pair.Value == -1)
                    {
                        salesAgnet = new SalesAgent {Id = -1};
                    }
                    else
                    {
                        var user = feedAssociation.Users.FirstOrDefault(x => x.Id == pair.Value);
                        if (user == null)
                            throw new Exception("Неизвестный пользователь: " + pair.Value);
                        salesAgnet = user.ToSalesAgent();
                    }

                    if (!FeedManager.TryUpdateAgentAssociation(feedAssociation.IdOrg.Value, pair.Key, salesAgnet))
                    {
                        throw new Exception("Ошибка при привязке: " + pair.Key+" -> "+ pair.Value);
                    }
                }
                result.StatusTypeResult = StatusTypeResult.OK;
                result.Status = "Все пользователи успешно связаны";
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "[FeedController::Post]");
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = ex.Message;
            }
            Log.Debug("End update feed association: org= "+feedAssociation.IdOrg);

            return result;
        }
    }
}

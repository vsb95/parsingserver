﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using ApiService.Core;
using ApiService.Core.Cache;
using ApiService.Core.MLS;
using Core.Enteties;
using Core.Enum;
using Core.Exceptions;
using CoreMls.DB.LocalEntities;
using CoreMls.DB.Managers;
using CoreParser.DB.LocalEntities;
using Logger;
using Newtonsoft.Json;
using DbSearchManager = CoreParser.DB.Managers.DbSearchManager;

namespace ApiService.WebApi.MLS
{
    public class SearchMlsController : ApiController
    {
        public class SearchMlsResult : DefaultResult
        {
            [JsonProperty("total-count-by-filter")]
            public int TotalCountByFilter { get; set; }

            [JsonProperty("advertisments")]
            public List<MlsAdvertisment> Advertisments { get; set; }
        }
        public class DetailsResult : DefaultResult
        {
            [JsonProperty("advertisment")]
            public MlsAdvertisment Advertisment{ get; set; }
        }

        public class SearchMlsInitResult : DefaultResult
        {
            [JsonProperty("districts")]
            public List<District> Districts { get; set; }

            [JsonProperty("micro-districts")]
            public List<MicroDistrict> MicroDistricts { get; set; }
        }


        /// <summary>
        /// Поисковый запрос
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public SearchMlsResult Post([FromBody] SearchMlsRequest args)
        {
            var result = new SearchMlsResult();
            if (args == null)
                return new SearchMlsResult {Status = "Не указан фильтр", StatusTypeResult = StatusTypeResult.EMPTY};
           /*
            // пока только по омску #OMSK
            if (string.IsNullOrWhiteSpace(args?.IdCity) || !int.TryParse(args?.IdCity, out var idcity))
            {
                result.Status = "Не выбран город в настройках организации";
                result.StatusType = StatusType.EMPTY;
                return result;
            }
            
            if (string.IsNullOrWhiteSpace(args?.ObjTypeSelect) || !int.TryParse(args?.ObjTypeSelect, out var objType))
            {
                result.Status = "Не выбран тип объявлений";
                result.StatusType = StatusType.EMPTY;
                return result;
            }
            */

            try
            {
                Log.Debug("Search MLS. ObjType: " + args.ObjectTypeSelect);
                var filter = args.ToSearchFilter();
                var cache = SearchCacheManager.GetCache(filter);
                if (cache != null)
                {
                    result.Advertisments = cache.Advertisments;
                    result.TotalCountByFilter = cache.TotalCountByFilter;
                }
                else
                {
                    var res = CoreMls.DB.Managers.DbSearchManager.SearchPages(filter, out var totalCountByFilter);
                    result.Advertisments = res;
                    result.TotalCountByFilter = totalCountByFilter;
                    cache = new SearchMlsCache(filter, res, totalCountByFilter, 30);
                    SearchCacheManager.AddOrUpdate(cache);
                }
                var preparedAdvertisment = new List<MlsAdvertisment>();
                foreach (var adv in result.Advertisments)
                {
                    AdvertismentPreparer.Prepare(adv,false);
                    preparedAdvertisment.Add(adv);
                }

                result.Advertisments = preparedAdvertisment;
                result.StatusTypeResult = StatusTypeResult.OK;
            }
            catch (ValidationException ex)
            {
                var msg = "Заметка: '" + ex.InnerMessage + "'; Пользователь: " + args.RequestedUserId;
                if (args.RequestedUserId > 1)
                {
                    var agent = DbSalesAgentManager.GetAgentById(args.RequestedUserId);
                    msg += " (" + agent?.Name + " из организации '" + agent?.Organization?.Name + "')";
                }
                Log.Exception(ex, LogType.Fatal, true, msg);
                result.Status = "Ошибка в вашем запросе: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }
            catch (AggregateException ex)
            {
                ex.Handle((innerException) =>
                {
                    Log.Exception(innerException, LogType.Fatal, true);
                    return true;
                });
                result.Status = "Error API: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }
            catch (Exception ex)
            {
                var msg = "Пользователь: " + args.RequestedUserId;
                if (args.RequestedUserId > 1)
                {
                    var agent = DbSalesAgentManager.GetAgentById(args.RequestedUserId);
                    msg += " (" + agent?.Name + " из организации '" + agent?.Organization?.Name + "')";
                }
                Log.Exception(ex, LogType.Fatal, true, msg);
                result.Status = "Error API: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }

            Log.Info(string.Format("[User: {0}] SearchMLS end; Total: {1} Sec: {2}", args.RequestedUserId, result.Advertisments?.Count, result.TotalSec));
            return result;
        }

        /// <summary>
        /// Инициализация формы поиска
        /// </summary>
        /// <param name="idCity"></param>
        /// <returns></returns>
        public SearchMlsInitResult Get(int? idCity = null)
        {
            var result = new SearchMlsInitResult();
            try
            {
                Log.Trace("Init SearchMLS: GetObjTypes" +(idCity != null ? " & GetDistricts & GetMicroDistricts" : " город не указан..."));
                
                if (idCity != null)
                {
                    result.Districts = CoreParser.DB.Managers.DbSearchManager.GetDistricts(idCity.Value);
                    result.MicroDistricts = CoreParser.DB.Managers.DbSearchManager.GetMicroDistricts(idCity.Value);
                }
                result.StatusTypeResult = StatusTypeResult.OK;
            }
            catch (Exception ex)
            {
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = ex.Message;
                Log.Exception(ex, LogType.Error, true);
            }
            return result;
        }

        /// <summary>
        /// Детализация 1 объявления
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public DetailsResult Get(int id, string userId)
        {
            var result = new DetailsResult();
            if (string.IsNullOrEmpty(userId))
            {
                Log.Warn("Не залогиненый пользователь!!! ");
                userId = "Клиент?";
                // детализацию можно
                /*
                result.Status = "Вы не авторизованы";
                result.StatusType = StatusType.ERROR;
                return result;
                */
            }
            try
            {
                var mlsAdvertisment = DbOfferManager.GetAdvById(id);
                if (mlsAdvertisment == null)
                {
                    result.StatusTypeResult = StatusTypeResult.EMPTY;
                    result.Status = "Объявление не найдено";
                }
                else
                {
                    AdvertismentPreparer.Prepare(mlsAdvertisment, false);
                    result.Advertisment = mlsAdvertisment;
                    result.StatusTypeResult = StatusTypeResult.OK;
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
                result.Status = "Ошибка API: " + ex.Message;
                result.StatusTypeResult = StatusTypeResult.ERROR;
            }

            Log.Info(string.Format("[User: {0}] Get detail MLS view ({2}) end. Sec: {1}", userId, result.TotalSec, id));
            return result;
        }
    }
}

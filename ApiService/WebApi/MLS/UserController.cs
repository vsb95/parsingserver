﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using ApiService.Core;
using ApiService.Core.MLS;
using Core.Enteties;
using Core.Enum;
using CoreMls.DB;
using CoreMls.DB.Managers;
using CoreMls.Managers;
using Logger;

namespace ApiService.WebApi.MLS
{
    public class UserController : ApiController
    {
        /// <summary>
        /// Получить колво объектов у пользователя
        /// </summary>
        /// <param name="idUser"></param>
        /// <returns></returns>
        public long Get(long idUser)
        {
            try
            {
                var advertisments = DbSearchManager.SearchPages(
                    new SearchMlsFilter() { UserId = idUser, AdvType = "all", Count = 1 },
                    out var totalCountByFilter);
                return totalCountByFilter;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "[UserController::Get]");
            }

            return -1;
        }

        /// <summary>
        /// Добавить/обновить пользователя
        /// </summary>
        /// <returns></returns>
        public DefaultResult Post([FromBody] User user)
        {
            DefaultResult result = new DefaultResult();
            if (user == null || user.Id == 0 || user.IdOrg == 0)
            {
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = "Ошибка: не указан пользователь";
                return result;
            }

            try
            {
                var res = DbSalesAgentManager.AddOrUpdate(user.ToSalesAgent().ToDb());
                if (res == -1)
                {
                    result.Status = "Не удалось обновить пользователя";
                    result.StatusTypeResult = StatusTypeResult.ERROR;
                    return result;
                }

                result.Status = "Пользователь успешно обновлен";
                result.StatusTypeResult = StatusTypeResult.OK;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "[UserController::Post]");
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = ex.Message;
            }

            return result;

        }

        public DefaultResult Put()
        {
            DefaultResult result = new DefaultResult();

            try
            {

            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "[UserController::Put]");
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// Уволить пользователя, передав объекты другому
        /// </summary>
        /// <param name="userDeleteRequest"></param>
        /// <returns></returns>
        public DefaultResult Delete([FromBody] UserDeleteRequest userDeleteRequest)
        {
            DefaultResult result = new DefaultResult();
            if (userDeleteRequest?.IdOrg == null)
            {
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = "Не указана организация";
                return result;
            }
            if (userDeleteRequest?.IdUser == null)
            {
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = "Не указан уволенный пользователь";
                return result;
            }

            try
            {
                Log.Debug("Start удаления пользователя [" + userDeleteRequest.IdUser + "]");
                var advertisments = DbSearchManager.SearchPages(
                    new SearchMlsFilter() { UserId = userDeleteRequest.IdUser.Value, AdvType = "all", Count = 999999 },
                    out var totalCountByFilter);
                if (totalCountByFilter != 0 && userDeleteRequest.NewOwner == null)
                {
                    result.StatusTypeResult = StatusTypeResult.ERROR;
                    result.Status = "Не указан сотрудник которому будет передано " + advertisments.Count+" объявлений увольняемого сотрудника";
                    return result;
                }

                result.StatusTypeResult = StatusTypeResult.OK;
                var agent = DbSalesAgentManager.GetAgentById(userDeleteRequest.IdUser.Value);
                // Если у нас такой пользователь не заведен, то и делать ничего не надо, тк он ни разу не добавлял объекты
                if (agent == null)
                    return result;
                // поставить Delete_dt
                agent.DeleteDate = DateTime.Now;
                DbSalesAgentManager.AddOrUpdate(agent.ToDb());
                // удалить ассоциацию если есть
                if (!FeedManager.TryUpdateAgentAssociation(userDeleteRequest.IdOrg.Value, null, agent,
                    userDeleteRequest.NewOwner?.Id))
                {
                    throw new Exception(
                        "Не удалось сбросить ассоциацию виртуальных агентов из фида с данным сотрудником");
                }
                if (totalCountByFilter == 0)
                    return result;
                var newAgent = DbSalesAgentManager.GetAgentById(userDeleteRequest.NewOwner.Id);
                if (newAgent == null)
                {
                    newAgent = userDeleteRequest.NewOwner.ToSalesAgent();
                    DbSalesAgentManager.AddOrUpdate(newAgent.ToDb());
                }
                // объекты передать другому
                foreach (var adv in advertisments)
                {
                    adv.SalesAgent = newAgent;
                    DbOfferManager.AddOrUpdatePage(adv);
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "[UserController::Delete]");
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = ex.Message;
            }
            finally
            {
                Log.Debug("Завершили удаление пользователя ["+ userDeleteRequest.IdUser + "] со статусом: "+result.StatusTypeResult);
            }
            return result;
        }

    }

    public class UserDeleteRequest 
    {
        public long? IdUser { get; set; }
        public long? IdOrg { get; set; }

        /// <summary>
        /// Пользователь, на кого скинуть объекты удаленного сотрудника
        /// </summary>
        public User NewOwner { get; set; }
    }
}

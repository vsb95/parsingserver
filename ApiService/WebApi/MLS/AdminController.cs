﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using ApiService.Core;
using Core.Enteties;
using Core.Enum;
using CoreMls.DB.Model;
using CoreMls.Managers;
using Logger;

namespace ApiService.WebApi.MLS
{
    public class AdminController : ApiController
    {
        /// <summary>
        /// наклеить ватермарки для всех объектов организации
        /// </summary>
        /// <param name="idUser"></param>
        /// <param name="idOrg"></param>
        /// <returns></returns>
        public DefaultResult Get(long idUser, long idOrg)
        {
            var result = new DefaultResult();
            try
            {
                if (idUser != 1)
                        throw new Exception("Неизвестный пользователь");
                if (idOrg < 1)
                    throw new Exception("Неизвестная организация");
                
                Log.Info("Start markering images for org: '" + idOrg);

                using (var context = new realtyMLSEntities())
                {
                    var offers = context.offers.Where(x => x.sales_agent.id_sales_agent_organization == idOrg).Select(x=>x.id_offer);
                    foreach (var idOffer in offers)
                    {
                        ImageManager.SetWaterMarkIfNeeded(idOffer);
                    }
                }
                
                result.Status = "OK";
                result.StatusTypeResult = StatusTypeResult.OK;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "[AdminController::Get] '" + idUser + "'; org: " + idOrg);
                result.StatusTypeResult = StatusTypeResult.ERROR;
                result.Status = ex.Message;
            }

            Log.Info("End markering images for org '" + idOrg+"': " + result.Status);
            return result;
        }
    }
}

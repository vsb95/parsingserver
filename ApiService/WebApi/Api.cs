﻿using System;
using Logger;
using Microsoft.Owin.Hosting;

namespace ApiService.WebApi
{
    public class Api : IDisposable
    {
        private IDisposable _owinHost;

        public bool Start(string baseAddress)
        {
            try
            {
                Log.Trace("Starting WebApi");
                
                _owinHost = WebApp.Start<Startup>(url: baseAddress);

                Log.Info("WebApi Started by " + baseAddress);
                return true;
            }
            catch (Exception ex)
            {
                Log.Exception(ex.InnerException ?? ex, LogType.Fatal);
            }

            return false;
        }

        public void Dispose()
        {
            _owinHost?.Dispose();
            _owinHost = null;
        }
    }
}

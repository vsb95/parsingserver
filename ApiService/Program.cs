using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using ApiService.WebApi;
using Core;
using Core.Services;
using CoreMls.Enum;
using CoreMls.Managers;
using CoreParser;
using CoreParser.DB;
using CoreParser.Services;
using CoreParser.Services.Proxies;
using Logger;

namespace ApiService
{
    class Program
    {
        static void Main(string[] args)
        {
            Api api = null;
            try
            {
                if (string.IsNullOrEmpty(Thread.CurrentThread.Name))
                    Thread.CurrentThread.Name = "API";
                SettingsManager.IsStartAsAPI = true;
                SettingsManager.ReloadSettings();
                var version = Assembly.GetExecutingAssembly().GetName().Version;
#if DEBUG
                Console.Title = "[DEBUG] API v" + version;
#else
                Console.Title = "API v" + version;
#endif

                Process thisProc = Process.GetCurrentProcess();
                thisProc.PriorityClass = ProcessPriorityClass.RealTime;

                Log.Info("Logger Initializing...");
                Log.Init(version, SettingsManager.Settings.LoggerSettings.MinCountMessageToSendLog, SettingsManager.Settings.LoggerSettings.MinSecIntervalToSendLog, SettingsManager.Settings.LoggerSettings.IsEnabledConsoleTrace);
                Log.InitSlackLogger(SettingsManager.Settings.LoggerSettings.SlackToken, SettingsManager.Settings.LoggerSettings.SlackChannel);
                ProxyManager.Init();
                DictionaryStorage.Init();
                CoreParser.DB.DbManager.Init();
#if !DEBUG
                InitSheduleTasks();
#endif
                api = new Api();
                if (!api.Start(SettingsManager.Settings.ServerSettings.StartupApiAdress))
                {
                    throw new Exception("Почему то не запустился WebAPI");
                }
                Console.WriteLine("=================================");
                Console.WriteLine("All initialized");

                while (true)
                {
                    Thread.Sleep(200);
                }
            }
            catch (Exception ex)
            {
                //Console.Clear();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("============= FATAL ERROR =============");
                Log.Exception(ex, LogType.Fatal, true);
                Console.WriteLine();
                Console.WriteLine();
                api?.Dispose();
                Console.WriteLine("Press key 'esc' to exit");
                while (true)
                {
                    var keyInfo = Console.ReadKey();
                    if (keyInfo.Key == ConsoleKey.Escape)
                        break;
                }
            }
            finally
            {
                api?.Dispose();
            }
        }

        static void InitSheduleTasks()
        {
            Log.Info("Shedule Tasks Initializing...");
            CoreMls.DB.Managers.DbManager.Init();
            Sheduler.AddOperation(new TimeSpan(1, 0, 0, 0), true, () =>
            {
                var deletedCount = 0;
                if(string.IsNullOrEmpty(SettingsManager.Settings.ServerSettings.MlsImageTmpFolder))
                {
                    Log.Fatal("Не указана папка для хранения временных файлов");
                    return;
                }
                foreach (var file in Directory.GetFiles(SettingsManager.Settings.ServerSettings.MlsImageTmpFolder))
                {
                    var fileInfo = new FileInfo(file);
                    if (fileInfo.Exists && (DateTime.Now - fileInfo.LastWriteTime).TotalHours > 24)
                    {
                        deletedCount++;
                        fileInfo.Delete();
                    }
                }
                Log.Info("Удалено "+ deletedCount+" файлов из временной папки");
            }, false, "Cleaner tmp folder");
        }
    }
}

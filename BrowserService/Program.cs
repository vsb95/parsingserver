﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BrowserService.Browser;
using BrowserService.WebApi;
using Logger;

namespace BrowserService
{
    class Program
    {
        static void Main(string[] args)
        {
            Api api = null;
            try
            {
                if (string.IsNullOrEmpty(Thread.CurrentThread.Name))
                    Thread.CurrentThread.Name = "BrowserService";
                var version = Assembly.GetExecutingAssembly().GetName().Version;
#if DEBUG
                Console.Title = "[DEBUG] BrowserService v" + version;
#else
                Console.Title = "BrowserService v" + version;
#endif
                Settings.ReloadSettings();
                BrowserManager.LoadBrowserList();

                Log.Info("Logger Initializing...");
                Log.Init(version, Settings.ServiceSettings.MinCountMessageToSendLog, Settings.ServiceSettings.MinSecIntervalToSendLog, true);
                Log.InitSlackLogger(Settings.ServiceSettings.SlackToken, Settings.ServiceSettings.SlackChannel);

                api = new Api();
                if (!api.Start(Settings.ServiceSettings.StartupApiAdress))
                    throw new Exception("Почему то не запустился WebAPI");
                

                Console.WriteLine("=================================");
                Console.WriteLine("All initialized");

                while (true)
                {
                    Thread.Sleep(200);
                }
            }
            catch (Exception ex)
            {
                //Console.Clear();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("============= FATAL ERROR =============");
                Log.Exception(ex, LogType.Fatal, true);
                Console.WriteLine();
                Console.WriteLine();
                api?.Dispose();
                Console.WriteLine("Press key 'esc' to exit");
                while (true)
                {
                    var keyInfo = Console.ReadKey();
                    if (keyInfo.Key == ConsoleKey.Escape)
                        break;
                }
            }
            finally
            {
                api?.Dispose();
            }
        }
    }
}

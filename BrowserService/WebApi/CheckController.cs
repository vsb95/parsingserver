﻿using System.Threading.Tasks;
using System.Web.Http;
using Logger;
using BrowserService.Proxies;

namespace BrowserService.WebApi
{
    public class CheckController : ApiController
    {
        /// <summary>
        /// Health Check
        /// </summary>
        /// <returns></returns>
        public string Get()
        {
            return "1";
        }
    }
}

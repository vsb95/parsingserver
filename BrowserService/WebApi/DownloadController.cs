﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using BrowserService.Proxies;
using Logger;
using BrowserService;
using BrowserService.Browser;

namespace BrowserService.WebApi
{
    public class DownloadController : ApiController
    {
        public string Get(PageType type, string url)
        {
            try
            {
                Log.Trace("Get browser for " + type);

                var browser = BrowserManager.GetNextFreeBrowser(type);
                return browser.Download(url);
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

            return null;
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrowserService.Proxies;

namespace BrowserService.Browser
{
    public class Browser
    {
        /// <summary>
        /// Сгенерированный ID
        /// </summary>
        public string Id { get; set; }

        public PageType PageTypeFor { get; set; }

        public Proxy Proxy { get; set; }

        public string Adress { get; set; }

        public bool IsLocked { get; private set; }

        public Browser()
        {
            
        }

        public string Download(string url)
        {
            return null;
        }
    }
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Core.Services.Proxies;
using Logger;

namespace BrowserService.Browser
{
    public static class BrowserManager
    {
        private static ConcurrentDictionary<PageType, ConcurrentQueue<Browser>> BrowserDictionaryTimeout = new ConcurrentDictionary<PageType, ConcurrentQueue<Browser>>();
        
        public static Browser GetNextFreeBrowser(PageType pageType)
        {
            Browser result = null;
            if (BrowserDictionaryTimeout.TryGetValue(pageType, out var list))
            {
                for (var i = 0; i < list.Count; i++)
                {
                    
                }
            }

            return result;
        }

        internal static async void LoadBrowserList()
        {
            await Task.Factory.StartNew(() =>
            {
                for (int i = 0; !ProxyManager.IsAliveService; i++)
                {
                    Thread.Sleep(1000);

                    // через 90 сек оповестим
                    if (i > 90)
                    {
                        i = 0;
                        Log.Fatal("Ожидаю запуска ProxyService по адресу: " + Settings.ServerSettings.ProxyServiceApiAdress);
                    }
                    Log.Warn("Ожидаю запуска ProxyService по адресу: " + Settings.ServerSettings.ProxyServiceApiAdress);
                }
                try
                {
                    for (var i = 0; i < ProxyManager.ProxyCount && i < Settings.BrowserServiceSettings.MaxCountBrowserInstance; i++)
                    {
                        var proxy = ProxyManager.GetProxy(PageType.Undefined);
                        var port = Settings.BrowserServiceSettings.MinPort + i;
                        var adress = Settings.BrowserServiceSettings.HostName + ":" + port;
                        if (!HealthCheck(adress))
                        {
                            isStartingBrowser = true;
                            Log.Trace("Запускаем браузер на '" + adress + "'");
                            if (proxy != null)
                                Process.Start(Settings.BrowserServiceSettings.ExePath,
                                    $"{proxy.Ip} {proxy.Port} {proxy.UserLogin} {proxy.UserPassword} {adress}");
                            else
                                Process.Start(Settings.BrowserServiceSettings.ExePath);
                        }

                        BrowserAdressQueue.Add(adress);

                        // Init Time Dictionary
                        var timeDictionary = new Dictionary<PageType, DateTime>();
                        foreach (PageType value in System.Enum.GetValues(typeof(PageType)))
                            timeDictionary.Add(value, DateTime.MinValue);
                        BrowserTimeDictionary.TryAdd(adress, timeDictionary);
                    }

                    if (isStartingBrowser)
                        Thread.Sleep(2500);
                    _healthThread = new Thread(HealthCheckLoop) { IsBackground = true };
                    _healthThread.Start();
                    isInited = true;
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, LogType.Fatal, true);
                }
            });
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using Logger;
using Newtonsoft.Json;
using BrowserService.Proxies;

namespace BrowserService
{
    public static class Settings
    {
        private static Thread _refreshThread;

        public static ServiceSettings ServiceSettings = null;

        static Settings()
        {
            _refreshThread = new Thread(RefreshFunc) {IsBackground = true};
            _refreshThread.Start();
        }

        public static void ReloadSettings()
        {
            try
            {
                ServiceSettings = new ServiceSettings("proxy-service-config.json");
                ServiceSettings.Serialize();
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error);
            }
        }

        private static void RefreshFunc()
        {
            while (true)
            {
                TimeZoneInfo moscowTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time");
                DateTime moscowDateTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, moscowTimeZone);

                var delayTimeSpan = new TimeSpan(23, 59, 59) - moscowDateTime.TimeOfDay + new TimeSpan(1, 0, 0);
                var delayMin = Math.Min((int) delayTimeSpan.TotalMinutes, 6 * 60);
                Log.Debug("Перезагружаю настройки через " + delayMin + " минут");
                Thread.Sleep(delayMin * 60 * 1000);
                ReloadSettings();
                ProxyManager.LoadProxies();
            }
        }
    }

    [JsonObject(Id = "service-settings", MemberSerialization = MemberSerialization.OptIn)]
    public class ServiceSettings
    {
        private readonly JsonSerializer _deserializer =
            new JsonSerializer {NullValueHandling = NullValueHandling.Ignore};

        private string _fileName;

        #region Properties

        /// <summary>
        /// Адрес на котором будет работать API
        /// </summary>
        [JsonProperty("startup-api-adress")]
        public string StartupApiAdress { get; set; } = "http://localhost:9005/";

        /// <summary>
        /// Время в секундах, которое должно пройти после последнего вызова экземпляра прокси 
        /// </summary>
        [JsonProperty("min-sec-to-reuse-proxy")]
        public int MinSecToReUseProxy { get; set; } = 2;

        /// <summary>
        /// Минимальное количество сообщений, которое необходимо отправлять в оповещении
        /// </summary>
        [JsonProperty("min-count-message-to-send-log")]
        public int MinCountMessageToSendLog { get; set; } = 10;

        /// <summary>
        /// интервал в секундах между отправкой оповещения 
        /// </summary>
        [JsonProperty("min-sec-interval-to-send-log")]
        public int MinSecIntervalToSendLog { get; set; } = 30;

        /// <summary>
        /// Токен к Slack-боту 
        /// </summary>
        [JsonProperty("slack-token")]
        public string SlackToken { get; set; } =
            "xoxp-391737888054-390731541555-409234792485-d7d68007943c8676f5794efdd3cf5b34";

        /// <summary>
        /// Канал, в который будет оповещять Slack бот
        /// </summary>
        [JsonProperty("slack-channel")]
        public string SlackChannel { get; set; } = "DC0LUGEBC";

        [JsonProperty("browser-adress")]
        public string BrowserAdress { get; set; } = "http://localhost:9050";

        #endregion

        public ServiceSettings(string filename)
        {
            _fileName = filename;
            if (File.Exists(filename))
                Deserialize(File.ReadAllText(filename));
        }

        public void Serialize()
        {
            var json = JsonConvert.SerializeObject(this, Formatting.Indented);
            File.WriteAllText(_fileName, json);
        }

        public void Deserialize(string json)
        {
            if (string.IsNullOrEmpty(json)) return;
            using (StringReader sr = new StringReader(json))
            {
                _deserializer.Populate(new JsonTextReader(sr), this);
            }
        }
    }
}

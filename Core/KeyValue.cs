﻿using Newtonsoft.Json;

namespace Core
{
    public class KeyValue
    {
        [JsonProperty("id")]
        public int Key { get; set; }

        [JsonProperty("name")]
        public string Value { get; set; }

        public KeyValue(int key, string value)
        {
            Key = key;
            Value = value;
        }
    }
}

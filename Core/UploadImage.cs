﻿using System;
using Newtonsoft.Json;

namespace Core
{
    public class UploadImage :IComparable<UploadImage>
    {
        [JsonProperty("src")]
        public string Src { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        public int Angle { get; set; }
        [JsonProperty("order")]
        public int Order { get; set; }


        public int CompareTo(UploadImage other)
        {
            var order = Order.CompareTo(other.Order);
            if (order != 0) return order;
            return string.Compare(Src, other.Src, StringComparison.Ordinal);
        }

        public override string ToString()
        {
            return Order + ": " + Name + " [" + Src + "]";
        }
    }
}

﻿using System;
using System.Collections.Concurrent;
using Logger;

namespace Core.Enum
{
    public enum ParsingPageType
    {
        Undefined = 0,
        Mlsn = 1,
        N1 = 2,
        //IsRukVRuki = 3,

        Avito = 20,
        Cian = 21,
        Yandex = 22,

    }

    public static class ParsingPageTypeIdentifier
    {
        public static ConcurrentDictionary<string, ParsingPageType> BoardPrefixes =
            new ConcurrentDictionary<string, ParsingPageType>();

        static ParsingPageTypeIdentifier()
        {
            BoardPrefixes.TryAdd("freeproxylists.net", ParsingPageType.Undefined);
            BoardPrefixes.TryAdd("hidemyna.me", ParsingPageType.Undefined);

            BoardPrefixes.TryAdd("avito.ru", ParsingPageType.Avito);
            BoardPrefixes.TryAdd("cian.ru", ParsingPageType.Cian);
            BoardPrefixes.TryAdd("mlsn.ru", ParsingPageType.Mlsn);
            BoardPrefixes.TryAdd("realty.yandex.ru", ParsingPageType.Yandex);
            BoardPrefixes.TryAdd("n1.ru", ParsingPageType.N1);
        }

        /// <summary>
        /// Определить домен страницы по урлу, Доски и объявления не делятся (т.е одно и то же)
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static ParsingPageType Identify(string url)
        {
            foreach (var boardPrefix in BoardPrefixes)
            {
                if (!url.Contains(boardPrefix.Key)) continue;
                return boardPrefix.Value;
            }

            Log.Error("[GetPageTypeByUrl] Не определен тип страницы: " + url, true);
            throw new ArgumentOutOfRangeException(nameof(url), url);
        }
    }
}

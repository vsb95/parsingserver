﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using Logger;

namespace Core.Extension
{
    public static class ImageExtension
    {
        /// <summary>
        /// Обрезка Image по Прямоугольнику
        /// </summary>
        /// <param name="image">Изображение</param>
        /// <param name="selection">Обрезка</param>
        /// <returns></returns>
        [Obsolete("Иногда вызывает OutOfMemoryException, Будет заменено на CropImage")]
        public static Image Crop(this Image image, Rectangle selection)
        {
            Bitmap bmp = image as Bitmap;

            // Check if it is a bitmap:
            if (bmp == null)
                throw new ArgumentException("No valid bitmap");
            if (bmp.Size.Height < selection.Height || bmp.Size.Width < selection.Width)
                throw new ArgumentException("Too small bitmap (" + bmp.Size.Width + "x" + bmp.Size.Height + ")");
            Bitmap cropBmp = null;
            try
            {
                // Crop the image:
                cropBmp = bmp.Clone(selection, bmp.PixelFormat);
            }
            catch (Exception ex)
            {
                Logger.Log.Exception(ex, LogType.Error);
                selection.Height -= selection.Y;
                cropBmp = bmp.Clone(selection, bmp.PixelFormat);
            }

            // Release the resources:
            //image.Dispose();

            return cropBmp;
        }

        public static Bitmap CropImage(this Image img, Rectangle cropArea)
        {
            Bitmap bmp = new Bitmap(cropArea.Width - cropArea.X, cropArea.Height - cropArea.Y);
            using (Graphics gph = Graphics.FromImage(bmp))
            {
                gph.DrawImage(img, new Rectangle(0, 0, cropArea.Width, cropArea.Height), cropArea, GraphicsUnit.Pixel);
            }

            return bmp;
        }

        public static bool TryAddWatermarkImage(this Image sourceImg, string watermarkPngPath)
        {
            try
            {
                using (var watermarkBmp = Image.FromFile(watermarkPngPath))
                {
                    using (Graphics g = Graphics.FromImage(sourceImg))
                    {
                        g.CompositingMode = CompositingMode.SourceOver;
                        var sourceWidthCenter = sourceImg.Width / 2;
                        var sourceHeightCenter = sourceImg.Height / 2;
                        var watermarkWidthCenter = watermarkBmp.Width / 2;
                        var watermarkHeightCenter = watermarkBmp.Height / 2;

                        int x = sourceWidthCenter - watermarkWidthCenter;
                        int y = sourceHeightCenter - watermarkHeightCenter;
                        g.DrawImage(watermarkBmp, new Point(x, y));
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error, true, "TryAddWatermarkImage");
            }

            return false;
        }
    }
}

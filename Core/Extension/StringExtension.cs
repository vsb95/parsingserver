﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Extension
{
    public static class StringExtension
    {
        /// <summary>
        /// найти число в строке (вернутся все числа как одно)
        /// </summary>
        /// <param name="originString"></param>
        /// <returns></returns>
        public static string FindDigit(this string originString)
        {
            var stringBuilder = new StringBuilder();

            foreach (var ch in originString)
            {
                if (char.IsDigit(ch))
                    stringBuilder.Append(ch);
            }

            return stringBuilder.ToString();
        }

        /// <summary>
        /// найти все числа в строке
        /// </summary>
        /// <param name="originString"></param>
        /// <param name="isReturnLetters">True - вернет еще и строки не являющиеся числами</param>
        /// <returns></returns>
        public static List<string> FindDigits(this string originString, bool isReturnLetters = false)
        {
            var result = new List<string>();
            if (string.IsNullOrEmpty(originString))
                return result;
            var stringBuilderLetter = new StringBuilder();
            var stringBuilderDigit = new StringBuilder();

            foreach (var ch in originString)
            {
                if (char.IsDigit(ch))
                {
                    if (isReturnLetters && stringBuilderLetter.Length > 0)
                    {
                        result.Add(stringBuilderLetter.ToString());
                        stringBuilderLetter.Clear();
                    }
                    stringBuilderDigit.Append(ch);
                }
                else
                {
                    if (stringBuilderDigit.Length > 0)
                    {
                        if(ch ==' ')
                            continue;
                        result.Add(stringBuilderDigit.ToString());
                        stringBuilderDigit.Clear();
                    }
                    if(isReturnLetters)
                        stringBuilderLetter.Append(ch);
                }
            }

            return result;
        }

        /// <summary>
        /// Удалить число из строки
        /// </summary>
        /// <param name="originString"></param>
        /// <param name="newValue"></param>
        /// <returns></returns>
        public static string RemoveDigit(this string originString, string newValue="")
        {
            var digit = originString.FindDigit();
            var result = originString.Replace(digit, newValue);
            return result;
        }

        /// <summary>
        /// Удалить число из строки
        /// </summary>
        /// <param name="originString"></param>
        /// <param name="newValue"></param>
        /// <returns></returns>
        public static string RemoveAllDigits(this string originString, string newValue = " ")
        {
            var result = originString;
            var digits = originString.FindDigits();
            foreach (var digit in digits)
            {
                result = result.Replace(digit, newValue);
            }
            return result;
        }

        public static List<string> Split(this string originString, string separator)
        {
            return originString.Split(new[] { separator }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public static string Replace(this string originString, string value)
        {
            return originString.Replace(value, "");
        }
    }
}

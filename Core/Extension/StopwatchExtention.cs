﻿using System;
using System.Diagnostics;

namespace Core.Extension
{
    public static class StopwatchExtention
    {
        public static double GetTotalSeconds(this Stopwatch watch)
        {
            var res = Math.Floor(watch.Elapsed.TotalSeconds);
            return res;
        }

        public static double GetTotalMinutes(this Stopwatch watch)
        {
            var res = Math.Round(watch.Elapsed.TotalMinutes, 2);
            return res;
        }

        public static double GetTotalHours(this Stopwatch watch)
        {
            var res = Math.Round(watch.Elapsed.TotalHours, 2);
            return res;
        }
    }
}

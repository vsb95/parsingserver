﻿using System;
using System.Globalization;
using Logger;
using Newtonsoft.Json;

namespace Core
{
    public class GeoCoordinate
    {
        [JsonIgnore]
        public bool IsEmpty => Math.Abs(Lat) <= 0.000001 && Math.Abs(Lon) <= 0.000001;

        [JsonIgnore]
        public string MapLink => string.Format("https://static-maps.yandex.ru/1.x/?l=map&pt={0},{1}&spn=0.5,0.5", Lat.ToString().Replace(',','.'), Lon.ToString().Replace(',', '.'));

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return IsEqualsPlace((GeoCoordinate) obj);
        }

        public bool IsEqualsPlace(GeoCoordinate other)
        {
            return Math.Abs(Lat - other.Lat) < 0.00001
                   && Math.Abs(Lon - other.Lon) < 0.00001;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Lat.GetHashCode() * 397) ^ Lon.GetHashCode();
            }
        }

        /// <summary>
        /// Широта (X)
        /// </summary>
        [JsonProperty("lat")]
        public double Lat { get; set; }

        /// <summary>
        /// Долгота (Y)
        /// </summary>
        [JsonProperty("lon")]
        public double Lon { get; set; }

        public GeoCoordinate()
        {

        }

        public GeoCoordinate(double lat, double lon)
        {
            Lat = lat;
            Lon = lon;
        }
        /*
        public string ToStringLatLon()
        {
            return Lat.ToString(CultureInfo.InvariantCulture).Replace(",",".") + "," + Lon.ToString(CultureInfo.InvariantCulture).Replace(",", ".");
        }*/

        /// <summary>
        /// Формат [Lon,Lat]
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Lon.ToString(CultureInfo.InvariantCulture).Replace(",", ".") + "," + Lat.ToString(CultureInfo.InvariantCulture).Replace(",", ".");
        }

        public static GeoCoordinate Deserialize(string point)
        {
            if (string.IsNullOrEmpty(point) || point == "0,0")
                return null;
            var chunks = point.Split(',');
            if (chunks.Length == 2 && double.TryParse(chunks[0].Replace(".",","), out var lon) && double.TryParse(chunks[1].Replace(".", ","), out var lat))
                return new GeoCoordinate(lat, lon);
            Log.Error("Не смогли распарсить координату из бд: " + point, true);
            return new GeoCoordinate();
        }

        public static GeoCoordinate operator - (GeoCoordinate left, GeoCoordinate right)
        {
            var result = new GeoCoordinate();
            result.Lat = left.Lat - right.Lat;
            result.Lon = left.Lon - right.Lon;
            return result;
        }

        /// <summary>
        /// Вернет дистанцию в метрах до точки
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public double GetDistanceTo(GeoCoordinate other)
        {
            System.Device.Location.GeoCoordinate thisCoordinate = new System.Device.Location.GeoCoordinate(Lat,Lon);
            System.Device.Location.GeoCoordinate otherCoordinate = new System.Device.Location.GeoCoordinate(other.Lat, other.Lon);
            return Math.Floor(thisCoordinate.GetDistanceTo(otherCoordinate));
        }
    }
}

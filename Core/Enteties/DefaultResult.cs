﻿using System.Diagnostics;
using System.IO;
using Core.Enum;
using Core.Extension;
using Logger;
using Newtonsoft.Json;

namespace Core.Enteties
{
    [JsonObject(Id = "result", MemberSerialization = MemberSerialization.OptIn)]
    public class DefaultResult
    {
        private readonly Stopwatch _timer = new Stopwatch();

        public DefaultResult()
        {
            _timer.Start();
        }

        [JsonProperty("status-type")]
        private string _statusType => StatusTypeResult.ToString();

        public StatusTypeResult StatusTypeResult { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("total-sec")]
        public double TotalSec => _timer.GetTotalSeconds();


        public DefaultResult(string input)
        {
            Deserialize(input);
        }

        protected virtual void Deserialize(string input)
        {
            if (string.IsNullOrEmpty(input)) return;
            JsonSerializer _deserializer = new JsonSerializer { NullValueHandling = NullValueHandling.Ignore };
            try
            {
                using (StringReader sr = new StringReader(input))
                {
                    using (var jsonReader = new JsonTextReader(sr))
                    {
                        _deserializer.Populate(jsonReader, this);
                    }
                }
            }
            catch (JsonReaderException e)
            {
                Log.Exception(e, LogType.Fatal, true);
            }
        }
    }
}

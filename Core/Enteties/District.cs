﻿using System;
using System.Collections.Generic;
using System.Linq;
using Logger;
using Core.Extension;
using Core.Storage;
using Newtonsoft.Json;

namespace Core.Enteties
{
    [JsonObject(Id = "district", MemberSerialization = MemberSerialization.OptIn)]
    public class District
    {
        private List<MicroDistrict> _microDistricts;

        [JsonProperty("id")]
        public int Id { get; set; }
        public int IdCity { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        public string City { get; set; }

        [JsonProperty("coords")]
        public List<GeoCoordinate> GeoCoordinates { get; set; } = new List<GeoCoordinate>();

        public District(int id,  string name, int idCity, string cityName, string serializedCoords)
        {
            Id = id;
            Name = name;

            IdCity = idCity;
            City = cityName;

            if(string.IsNullOrEmpty(serializedCoords))
                return;

            try
            {
                var split = serializedCoords.Remove(serializedCoords.Length - 1).Split("],");
                foreach (var str in split)
                {
                    var points = str.Replace("[", "").Replace("]", "").Split(',');
                    if (points.Length != 2
                        || !double.TryParse(points.LastOrDefault()?.Replace('.', ','), out var lon)
                        || !double.TryParse(points.FirstOrDefault()?.Replace('.', ','), out var lat))
                    {
                        Log.Error("Обрати внимание - Не корректные данные в геокоординатах района id= " + Id + ": " + serializedCoords);
                        continue;
                    }

                    GeoCoordinates.Add(new GeoCoordinate(lat, lon));
                }
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка во время загрузки координат района: "+ ToString());
                Log.Exception(ex, LogType.Error);
            }
        }

        public List<MicroDistrict> GetMicroDistricts()
        {
            return _microDistricts ?? (_microDistricts = RegionsManager.GetMicroDistricts(Id));
        }


        public GeoCoordinate GetCenter()
        {
            return RegionsManager.GetCenter(GeoCoordinates);
        }

        public override string ToString()
        {
            return string.Format("{1} ({0}, {2})", Id, Name, City);
        }
    }
}

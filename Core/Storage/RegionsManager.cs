﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Enteties;
using Core.Interface;
using Logger;

namespace Core.Storage
{
    public static class RegionsManager
    {
        public static IDistrictManager DistrictManager;

        private static List<District> _districts;
        private static List<MicroDistrict> _microdistricts;
        private static Dictionary<int, string> _districtDictionary = new Dictionary<int, string>();
        private static Dictionary<int, string> _microDistrictDictionary = new Dictionary<int, string>();

        public static string GetDistrict(int id)
        {
            return _districtDictionary.TryGetValue(id, out var name) ? name : null;
        }
        public static string GetMicroDistrict(int id)
        {
            return _microDistrictDictionary.TryGetValue(id, out var name) ? name : null;
        }

        public static void Init(List<District> districts, List<MicroDistrict> microdistricts)
        {
            _districts = districts;
            _microdistricts = microdistricts;

            // Отдельно инициализирую словари для того чтобы O(1) была у Get методов
            _districtDictionary = districts.ToDictionary(x => x.Id, y => y.Name);
            _microDistrictDictionary = microdistricts.ToDictionary(x => x.Id, y => y.Name);
        }

        public static GeoCoordinate GetCenter(List<GeoCoordinate> coords)
        {
            if (coords == null || coords.Count < 2)
                return coords?.FirstOrDefault();
            try
            {
                double lat = 0;
                double lon = 0;
                foreach (var coord in coords)
                {
                    lat += coord.Lat;
                    lon += coord.Lon;
                }

                lat = lat / coords.Count;
                lon = lon / coords.Count;
                return new GeoCoordinate(lat, lon);
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error);
            }

            return null;
        }

        public static bool InPoligon(List<GeoCoordinate> polygon, GeoCoordinate point)
        {
            int cn = 0;    // the crossing number counter
            // loop through all edges of the polygon
            for (int i = 0; i < polygon.Count; i++)
            {    // edge from V[i] to V[i+1]
                if (((polygon[i].Lon <= point.Lon) && (polygon[(i + 1) % polygon.Count].Lon > point.Lon))    // an upward crossing
                    || ((polygon[i].Lon > point.Lon) && (polygon[(i + 1) % polygon.Count].Lon <= point.Lon)))
                { // a downward crossing
                    // compute the actual edge-ray intersect x-coordinate
                    var vt = (point.Lon - polygon[i].Lon) / (polygon[(i + 1) % polygon.Count].Lon - polygon[i].Lon);
                    if (point.Lat < polygon[i].Lat + vt * (polygon[(i + 1) % polygon.Count].Lat - polygon[i].Lat)) // P.x < intersect
                        ++cn;   // a valid crossing of y=P.y right of P.x
                }
            }
            return (cn % 2 == 1);    // 0 if even (out), and 1 if odd (in)
        }

        public static List<MicroDistrict> GetMicroDistricts(int idDistrict)
        {
            return _microdistricts.Where(x => x.IdDistrict == idDistrict).ToList();
        }
    }
}

﻿namespace Core.Exceptions
{
    public class ValidationException : System.Exception
    {
        public string InnerMessage { get; set; }
        public ValidationErrorType ErrorType { get; private set; }
        public ValidationException(string msg, ValidationErrorType errorType = ValidationErrorType.Warning) :base(msg)
        {
            ErrorType = errorType;
        }

        public ValidationException(string msg, string innerMessage, ValidationErrorType errorType = ValidationErrorType.Warning) : base(msg)
        {
            InnerMessage = innerMessage;
            ErrorType = errorType;
        }

        public override string ToString()
        {
            return $"[{ErrorType}] {Message}"+(!string.IsNullOrEmpty(InnerMessage)?" Расшифровка: "+InnerMessage:"");
        }
    }

    public enum ValidationErrorType
    {
        /// <summary>
        /// Просто сообщить 
        /// </summary>
        Warning,
        /// <summary>
        /// Валидация не пройдена, объект не должен быть добавлен
        /// </summary>
        Error,
        /// <summary>
        /// Все остановить, это фатальная ошибка. Дальше ничего проверяться не должно
        /// </summary>
        Fatal
    }
}

﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using Core.Enum;
using Core.Services;
using Logger;
using Newtonsoft.Json;

namespace Core
{
    public static class SettingsManager
    {
        /// <summary>
        /// Признак: Запустили как АПИ? 
        /// Если TRUE, то будут использоваться только IPv4 прокси, иначе только IPv3 (shared)
        /// </summary>
        public static bool IsStartAsAPI { get; set; } = false;

        public static Settings Settings = null;

        static SettingsManager()
        {
            var random = new Random();

            var startDelay = new TimeSpan(12, 0, 0);
            Sheduler.AddOperation(startDelay, true, () =>
            {
                try
                {
                    TimeZoneInfo moscowTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time");
                    DateTime moscowDateTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, moscowTimeZone);
                    var delay = new TimeSpan(23, 59, 59) - moscowDateTime.TimeOfDay + new TimeSpan(1, 0, random.Next(15000));
                    Log.Debug("Перезагружаю настройки через " + (int)delay.TotalMinutes + " минут");
                    Thread.Sleep((int)delay.TotalMilliseconds);

                    Log.Info("Перезагружаю настройки...");
                    ReloadSettings();
                    //DbObjectManager.Init();
                    Log.Init(null, Settings.LoggerSettings.MinCountMessageToSendLog,
                        Settings.LoggerSettings.MinSecIntervalToSendLog,
                        Settings.LoggerSettings.IsEnabledConsoleTrace);
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, LogType.Error, true);
                }
            },true, "Reloader");
        }

        public static void ReloadSettings()
        {
            try
            {
                Settings = new Settings();
                if (string.IsNullOrEmpty(Settings.ServerSettings.ImageFolder))
                    throw new Exception("не указан путь до папки хранения распарсенных фото");
                if (string.IsNullOrEmpty(Settings.ServerSettings.MlsImageFolder))
                    throw new Exception("не указан путь до папки хранения фото млс");
                if (string.IsNullOrEmpty(Settings.ServerSettings.MlsImageTmpFolder))
                    throw new Exception("не указан путь до временной папки фото млс");
                if (string.IsNullOrEmpty(Settings.ServerSettings.MlsFeedFolder))
                    throw new Exception("не указан путь до папки хранения фидов");

                if (!Directory.Exists(Settings.ServerSettings.ImageFolder))
                    Directory.CreateDirectory(Settings.ServerSettings.ImageFolder);
                if (!Directory.Exists(Settings.ServerSettings.MlsImageFolder))
                    Directory.CreateDirectory(Settings.ServerSettings.MlsImageFolder);
                if (!Directory.Exists(Settings.ServerSettings.MlsImageTmpFolder))
                    Directory.CreateDirectory(Settings.ServerSettings.MlsImageTmpFolder);
                if (!Directory.Exists(Settings.ServerSettings.MlsFeedFolder))
                    Directory.CreateDirectory(Settings.ServerSettings.MlsFeedFolder);
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error);
            }
        }
    }

    [JsonObject(Id = "config", MemberSerialization = MemberSerialization.OptIn)]
    public class Settings
    {
        private readonly JsonSerializer _deserializer = new JsonSerializer { NullValueHandling = NullValueHandling.Ignore };
        private const string _fileName = "config.json";

        [JsonProperty("server-settings")]
        public ServerSettings ServerSettings { get; private set; } = new ServerSettings();

        [JsonProperty("actualizer-settings")]
        public ActualizerSettings ActualizerSettings { get; private set; } = new ActualizerSettings();

        [JsonProperty("logger-settings")]
        public LoggerSettings LoggerSettings { get; private set; } = new LoggerSettings();

        [JsonProperty("rabbitmq-settings")]
        public RabbitMQSettings RabbitMQSettings { get; private set; } = new RabbitMQSettings();
        
        [JsonProperty("env-settings")]
        public EnvironmentSettings EnvironmentSettings { get; private set; } = new EnvironmentSettings();

        public Settings()
        {
            try
            {
                if (File.Exists(_fileName))
                    Deserialize(File.ReadAllText(_fileName));

                if (!EnvironmentSettings.IsTestEnv && string.IsNullOrEmpty(EnvironmentSettings.TestApiAdress))
                {
                    Log.Error("Не указан адрес для тестовой апишки в конфигах");
                }

                // Инициализируем список настроек для парсинга, если их нет в конфиге
                if (ServerSettings.ParsingSettings == null || ServerSettings.ParsingSettings.Count == 0)
                {
                    ServerSettings.ParsingSettings = new ConcurrentDictionary<ParsingPageType, ParsingSetting>();

                    var item = new ParsingSetting(ParsingPageType.Avito, true);
                    ServerSettings.ParsingSettings.TryAdd(item.ParsingPageType, item);
                    item = new ParsingSetting(ParsingPageType.Cian, true);
                    ServerSettings.ParsingSettings.TryAdd(item.ParsingPageType, item);
                    item = new ParsingSetting(ParsingPageType.Yandex, true);
                    ServerSettings.ParsingSettings.TryAdd(item.ParsingPageType, item);
                    item = new ParsingSetting(ParsingPageType.Mlsn, false);
                    ServerSettings.ParsingSettings.TryAdd(item.ParsingPageType, item);
                    item = new ParsingSetting(ParsingPageType.N1, false);
                    ServerSettings.ParsingSettings.TryAdd(item.ParsingPageType, item);
                }


                Serialize();
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Error);
            }
        }

        public void Serialize()
        {
            var json = JsonConvert.SerializeObject(this, Formatting.Indented);
            File.WriteAllText(_fileName, json);
        }

        public void Deserialize(string json)
        {
            if (string.IsNullOrEmpty(json)) return;
            using (StringReader sr = new StringReader(json))
            {
                _deserializer.Populate(new JsonTextReader(sr), this);
            }
        }
    }

    [JsonObject(Id = "server-settings", MemberSerialization = MemberSerialization.OptIn)]
    public class ServerSettings
    {
        #region Properties

        [JsonProperty("domain")]
        public string Domain { get; set; } = "https://rieltor-service.ru/";

        /// <summary>
        /// Путь к папке где будут храниться фото
        /// </summary>
        [JsonProperty("image-folder")]
        public string ImageFolder { get; set; } = $"{Environment.CurrentDirectory}\\Images";

        /// <summary>
        /// Путь к папке где будут храниться фото MLS
        /// </summary>
        [JsonProperty("image-mls-folder")]
        public string MlsImageFolder { get; set; } = $"{Environment.CurrentDirectory}\\Images\\Mls";

        /// <summary>
        /// Путь к папке где будут храниться фото MLS на сервере /assets/img/tmp
        /// </summary>
        [JsonProperty("image-mls-tmp-folder")]
        public string MlsImageTmpFolder { get; set; }

        /// <summary>
        /// Путь к папке где будут храниться xml фиды на сервере /assets/img/tmp
        /// </summary>
        [JsonProperty("mls-feed-folder")]
        public string MlsFeedFolder { get; set; }

        /// <summary>
        /// Адрес на котором будет работать API
        /// </summary>
        [JsonProperty("startup-api-adress")]
        public string StartupApiAdress { get; set; }= "http://localhost:9000/";

        /// <summary>
        /// Адрес на котором ожидаемо работает ProxyService
        /// </summary>
        [JsonProperty("proxy-service-api-adress")]
        public string ProxyServiceApiAdress { get; set; } = "http://localhost:9005/";

        /// <summary>
        /// Адрес на котором ожидаемо работает ProxyService
        /// </summary>
        [JsonProperty("proxy-service-exe-path")]
        public string ProxyServiceExePath { get; set; } = $"{Environment.CurrentDirectory}\\ProxyService\\ProxyService.exe";

        /// <summary>
        /// Конвертировать изображения в PNG?
        /// </summary>
        [JsonProperty("is-convert-images-to-png")]
        public bool IsConvertImagesToPng { get; set; } = false;

        /// <summary>
        /// Сохранять оригинальные изображения?
        /// </summary>
        [JsonProperty("is-save-origin-images")]
        public bool IsSaveOriginImages { get; set; } = false;

        /// <summary>
        /// Время в секундах, которое должно пройти после последнего вызова экземпляра прокси 
        /// </summary>
        [JsonProperty("min-sec-to-reuse-proxy")]
        public int MinSecToReUseProxy { get; set; } = 2;

        /// <summary>
        /// Parsing Server использует только Shared прокси?
        /// </summary>
        [JsonProperty("is-parsing-server-using-only-shared-proxy")]
        public bool IsParsingServerUsingOnlySharedProxy { get; set; } = true;

        /// <summary>
        /// Parsing Server может использовать бесплатные прокси?
        /// </summary>
        [JsonProperty("is-parsing-server-can-use-free-proxy")]
        public bool IsParsingServerCanUseFreeProxy { get; set; } = true;

        /// <summary>
        /// Если включен, то ParsingServer будет качать только с 22 вечера до 9 утра
        /// </summary>
        [JsonProperty("is-enable-day-night-mode")]
        public bool IsEnableDayNightMode { get; set; } = true;

        [JsonProperty("zip-folder")]
        public string ZipFolder { get; set; } = AppDomain.CurrentDomain.BaseDirectory+"zip";
        
        /// <summary>
        /// Список настроект париснга тех или иных объявлений
        /// </summary>
        [JsonProperty("parsing-settings-list")]
        public ConcurrentDictionary<ParsingPageType,ParsingSetting> ParsingSettings;

        /// <summary>
        /// Город, для которого будет работать данный ParsingServer
        /// </summary>
        [JsonProperty("default-cityid-for-parsing-server")]
        public int SelectedCityId { get; set; } = -1;


        #endregion
    }

    [JsonObject(Id = "actualizer-settings", MemberSerialization = MemberSerialization.OptIn)]
    public class ActualizerSettings
    {
        #region Properties
        /// <summary>
        /// Перескачивание страниц каждые N минут
        /// </summary>
        [JsonProperty("actualize-page-every-minutes")]
        public int ActualizePageEveryMin { get; set; } = 1440; // 1 раз в сутки

        /// <summary>
        /// Количество страниц, которые будут актуализироваться одновременно днем (0 - будет выключен до ночи)
        /// </summary>
        [JsonProperty("actualize-page-count-by-iteration-on-day")]
        public int ActualizePageCountByIterationDay { get; set; } = 10;

        /// <summary>
        /// Количество страниц, которые будут актуализироваться одновременно ночью (когда пользователей нет)
        /// </summary>
        [JsonProperty("actualize-page-count-by-iteration-on-night")]
        public int ActualizePageCountByIterationNight { get; set; } = 50;
        
        /// <summary>
        /// Если включен, то ParsingServer будет актуализировать с органичениями в день\ночь
        /// </summary>
        [JsonProperty("is-enable-day-night-mode")]
        public bool IsEnableDayNightMode { get; set; } = true;
        
        [JsonProperty("is-actualizer-check-images")]
        public bool IsActualizerCheckImages { get; set; } = true;

        #endregion
    }

    [JsonObject(Id = "logger-settings", MemberSerialization = MemberSerialization.OptIn)]
    public class LoggerSettings
    {
        #region Properties

        /// <summary>
        /// Минимальное количество сообщений, которое необходимо отправлять в оповещении
        /// </summary>
        [JsonProperty("min-count-message-to-send-log")]
        public int MinCountMessageToSendLog { get; set; } = 10;

        /// <summary>
        /// интервал в секундах между отправкой оповещения 
        /// </summary>
        [JsonProperty("min-sec-interval-to-send-log")]
        public int MinSecIntervalToSendLog { get; set; } = 30;

        /// <summary>
        /// Токен к Slack-боту 
        /// </summary>
        public string SlackToken { get; set; } = "xoxp-391737888054-390731541555-409234792485-d7d68007943c8676f5794efdd3cf5b34";

        /// <summary>
        /// Канал, в который будет оповещять Slack бот
        /// </summary>
        public string SlackChannel { get; set; } = "DC0LUGEBC";

        /// <summary>
        /// Выводить Trace\Debug в консоль?
        /// </summary>
        [JsonProperty("is-enable-console-trace")]
        public bool IsEnabledConsoleTrace { get; set; } = true;

        #endregion
    }

    [JsonObject(Id = "env-settings", MemberSerialization = MemberSerialization.OptIn)]
    public class EnvironmentSettings
    {
        #region Properties

        [JsonProperty("is-test-env")]
        public bool IsTestEnv { get; set; } = false;

        /// <summary>
        /// Адрес тестовой апишки
        /// </summary>
        [JsonProperty("test-api-adress")]
        public string TestApiAdress { get; set; }

        #endregion
    }

    [JsonObject(Id = "startup-settings-rabbitmq", MemberSerialization = MemberSerialization.OptIn)]
    public class RabbitMQSettings
    {
        #region Properties

        [JsonProperty("port")]
        public int Port { get; set; } = 1234;

        [JsonProperty("routing-key")]
        public string RoutingKey { get; set; } = "browser";

        [JsonProperty("hostname")]
        public string HostName { get; set; } = "localhost";

        #endregion
    }

    [JsonObject(Id = "parsing-setting", MemberSerialization = MemberSerialization.OptIn)]
    public class ParsingSetting
    {
        /// <summary>
        /// Тип страницы
        /// </summary>
        [JsonProperty("page-type")]
        public ParsingPageType ParsingPageType { get; set; }

        /// <summary>
        /// Скачивать через браузер? (иначе через httprequest)
        /// </summary>
        [JsonProperty("is-download-through-browser")]
        public bool IsDownloadThroughBrowser { get; set; }


        public ParsingSetting(ParsingPageType parsingPageType, bool isDownloadThroughBrowser)
        {
            ParsingPageType = parsingPageType;
            IsDownloadThroughBrowser = isDownloadThroughBrowser;
        }
    }
}

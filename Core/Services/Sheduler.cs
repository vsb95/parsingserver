﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Logger;

namespace Core.Services
{
    public static class Sheduler
    {
        public delegate void Operation();


        /// <summary>
        /// Добавить операцию которая будет выполнятся по расписанию.
        /// Все задачи имеют низший приоритет
        /// </summary>
        /// <param name="delay">Задержка перед запуском каждой итерации</param>
        /// <param name="isRepeat">Бесконечный цикл?</param>
        /// <param name="operation">Какое действие выполнить</param>
        /// <param name="isStartImmediately">Если в бесконечном цикле, то начать прямо сейчас?</param>
        /// <param name="nameOperation">Название операции (чисто для визуального опознавания)</param>
        public static void AddOperation(TimeSpan delay, bool isRepeat, Operation operation, bool isStartImmediately = false, string nameOperation = null)
        {
            Log.Info("Стартуем задачу по расписанию: '"+ nameOperation + "' "+(isRepeat ? (isStartImmediately?"стартуем сейчас, ":"")+"с повторением каждые ":"запуск через ") + delay.ToString("g"));
            Thread th = new Thread(() =>
            {
                try
                {
                    if (isRepeat)
                    {
                        while (true)
                        {
                            Thread.Sleep(isStartImmediately ? 2000 : (int) delay.TotalMilliseconds);
                            Log.Info("Запускаем задачу: '" + nameOperation + "' ");
                            operation();
                            Log.Info("Задача '" + nameOperation + "' завершена. Повтор через " + delay.ToString("g"));
                            isStartImmediately = false;
                        }
                    }
                    else
                    {
                        Thread.Sleep((int)delay.TotalMilliseconds);
                        Log.Info("Запускаем задачу: '"+ nameOperation + "' ");
                        operation();
                        Log.Info("Задача '" + nameOperation + "' завершена");
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, LogType.Fatal, true, "Sheduler::" + nameOperation);
                }
            });
            th.Priority = ThreadPriority.Lowest;
            th.IsBackground = true;
            if (!string.IsNullOrEmpty(nameOperation))
                th.Name = "ShedulerThread::"+nameOperation;
            th.Start();
        }
    }
}

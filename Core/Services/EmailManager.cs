﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Logger;

namespace Core.Services
{
    public class SendEmailRequest
    {
        /// <summary>
        /// Кому отправить
        /// </summary>
        public string EmailTo { get; set; }
        /// <summary>
        /// Тема
        /// </summary>
        public string Caption { get; set; }
        /// <summary>
        /// Тело письма. Можно HTML
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Список ящиков для скрытых копий
        /// </summary>
        public IEnumerable<string> BccList { get; set; }

        /// <summary>
        /// Список файлов для вложения
        /// </summary>
        public List<string> Attachements { get; set; } = new List<string>();

        /// <summary>
        /// Тоже самое что и Attachements, только картинки архивируются и архив аттачится
        /// </summary>
        public List<UploadImage> Images { get; set; } 
    }

    public static class EmailManager
    {
        private static readonly ConcurrentQueue<SendEmailRequest> MailQueue = new ConcurrentQueue<SendEmailRequest>();
        private static readonly Task SendEmailTask = new Task(() =>
        {
            while (true)
            {
                SendEmailRequest request = null;
                if (MailQueue.TryDequeue(out request))
                {
                    if (!TrySendEmail("smtp.yandex.ru", "rieltorservice@ya.ru", "QqroOb6N", request))
                    {
                        Log.Info("Не удалось отправить письмо: " + request.Caption + " на адрес " + request.EmailTo+"; Ставим в очередь");
                        MailQueue.Enqueue(request);
                    }
                }
                Thread.Sleep(5000);
            }
        });

        public static bool TrySendEmail(SendEmailRequest request)
        {
            if (TrySendEmail("smtp.yandex.ru", "rieltorservice@ya.ru", "QqroOb6N", request))
            {
                Log.Info("Отправлено письмо: " + request.Caption + " на адрес " + request.EmailTo);
                return true;
            }

            try
            {
                MailQueue.Enqueue(request);
                Log.Info("Ставим письмо в очередь: " + request.Caption);
                if (SendEmailTask.Status != TaskStatus.Running)
                    SendEmailTask.Start();
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true);
            }

            return false;
        }

        /// <summary>
        /// Отправка письма на почтовый ящик 
        /// </summary>
        /// <param name="smtpServer">Имя SMTP-сервера</param>
        /// <param name="from">Адрес отправителя</param>
        /// <param name="password">пароль к почтовому ящику отправителя</param>
        /// <param name="request"></param>
        private static bool TrySendEmail(string smtpServer, string from, string password, SendEmailRequest request)
        {
            Log.Trace("Отправка письма '" + request.Caption + "' на: " + request.EmailTo);
#if DEBUG
            /*
            request.EmailTo = "rokira2341@mail.ru";// тестовый ящик
            Log.Info("DEBUG переадресация отправки письма на "+ request.EmailTo);
            return true;
            */
#endif
            try
            {
                using (MailMessage mail = new MailMessage())
                {

                    mail.From = new MailAddress(from);
                    if(!string.IsNullOrEmpty(request.EmailTo))
                        mail.To.Add(new MailAddress(request.EmailTo));
                    mail.Subject = request.Caption;
                    mail.IsBodyHtml = true;
                    mail.Body = request.Content;
                    if (request.BccList != null)
                    {
                        foreach (var copyTo in request.BccList)
                        {
                            if(string.IsNullOrEmpty(copyTo)) continue;
                            mail.Bcc.Add(copyTo);
                        }
                    }
                    //mail.Headers.Add("MIME-Version", "1.0");
                    //mail.Headers.Add("Content-type", "text/html; charset=utf-8");
                    //mail.Headers.Add("From", "NoReplyRieltor-Service <rieltorservice@ya.ru>");
                    if (request.Attachements != null && request.Attachements.Count > 0)
                    {
                        foreach (var file in request.Attachements)
                        {
                            var fileInfo = new FileInfo(file);
                            var attach = new Attachment(file) {Name = fileInfo.Name};
                            mail.Attachments.Add(attach);
                        }
                    }
                    if (request.Images != null && request.Images.Count > 0)
                    {
                        var archivename = "Вложение " + DateTime.Now.Date.ToString("dd.MM") + ".zip";
                        if (Core.Services.ZipManager.TryCreateZip(request.Images, archivename, out var filePath))
                        {
                            var attach = new Attachment(filePath) { Name = archivename };
                            mail.Attachments.Add(attach);
                        }
                        else
                        {
                            foreach (var image in request.Images)
                            {
                                var attach = new Attachment(image.Src) { Name = image.Name };
                                mail.Attachments.Add(attach);
                            }
                        }
                    }
                    using (SmtpClient client = new SmtpClient
                    {
                        Host = smtpServer,
                        Port = 587,
                        EnableSsl = true,
                        Credentials = new NetworkCredential(@from.Split('@')[0], password),
                        DeliveryMethod = SmtpDeliveryMethod.Network
                    })
                    {
                        client.Send(mail);
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                string msg =
                    "<EmailManager::TrySendEmail> " + e.Message + "\n" + " smtpServer =" + smtpServer + " from =" + from +
                    " psw =" + password + " mailto =" + request.EmailTo + " caption =" + request.Caption  +
                    " attachFiles =" + string.Join("\n", request.Attachements ?? new List<string>()) + "\n" + e.StackTrace + "\n\nmessage =\n\n" + request.Content;
                Log.Message(msg, LogType.Error, true);
            }

            return false;
        }
    }
}

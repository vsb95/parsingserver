﻿using System;
using System.IO;
using System.Text;

namespace Core.Services
{
    public static class Randomizer
    {
        private static readonly string AllowableChars =
            "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890_";

        private static readonly Random Rnd = new Random();

        /// <summary>
        /// Сгенерировать уникальный файл в папке
        /// </summary>
        /// <param name="extention">расширение начиная с точки</param>
        /// <param name="wantedName">Желаемое имя файла. К нему будет добавляться постфикс</param>
        /// <param name="folder">папка куда генерируем, если не указана то в текущей</param>
        /// <returns></returns>
        public static FileInfo CreateEmptyFilename(string extention, string wantedName, string folder = "")
        {
            if (string.IsNullOrEmpty(extention))
                throw new ArgumentException("не указано расширение");
            if (!extention.StartsWith("."))
                extention = "." + extention;
            if (string.IsNullOrEmpty(folder))
                folder = "tmp_files";
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            FileInfo fileInfo = null;
            do
            {
                var filename = wantedName + CreateRandomString(10) + extention;
                fileInfo = new FileInfo(Path.Combine(folder, filename));
            } while (fileInfo.Exists);

            return fileInfo;
        }

        /// <summary>
        /// Вернет рандомную строку
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static string CreateRandomString(int size = 16)
        {
            var strBuilder = new StringBuilder();
            for (var i = 0; i < size; i++)
            {
                strBuilder.Append(AllowableChars[Rnd.Next(AllowableChars.Length - 2)]);
            }

            return strBuilder.ToString().ToLower();
        }

        /// <summary>
        /// Создать новый уникальный каталог в папке
        /// </summary>
        /// <param name="baseFolder"></param>
        /// <returns></returns>
        public static string CreateRandomFolder(string baseFolder)
        {
            DirectoryInfo dirInfo;
            do
            {
                var strBuilder = new StringBuilder(baseFolder);
                strBuilder.Append("\\");

                for (var i = 0; i < 16; i++)
                {
                    strBuilder.Append(AllowableChars[Rnd.Next(AllowableChars.Length - 1)]);
                }

                dirInfo = new DirectoryInfo(strBuilder.ToString());
            } while (dirInfo.Exists);

            return dirInfo.FullName;
        }
    }
}

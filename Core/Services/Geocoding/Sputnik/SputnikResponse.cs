﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Logger;
using Newtonsoft.Json;

namespace Core.Services.Geocoding.Sputnik
{
    public class SputnikResponse
    {
        private string city;
        private string street;
        private string house;
        private List<GeoCoordinate> _coordinates;

        private static readonly JsonSerializer Deserializer = new JsonSerializer { NullValueHandling = NullValueHandling.Ignore };

        [JsonProperty("meta")]
        internal SputnikResponseMeta Meta { get; set; }
        [JsonProperty("result")]
        public SputnikResponseResult Result { get; set; }
        [JsonProperty("typo")]
        internal SputnikResponseTypo Typo { get; set; }

        public static SputnikResponse Deserialize(string json)
        {
            if (string.IsNullOrEmpty(json)) throw new ArgumentNullException(nameof(json));
            var result = new SputnikResponse();
            using (StringReader sr = new StringReader(json))
            {
                Deserializer.Populate(new JsonTextReader(sr), result);
            }

           /*var res =  result.Result.Addresses.FirstOrDefault(x => x.Features.Any(y => y.Properties.AddressComponents.Any(z=>z.Value.ToLower().Contains("омск"))));
            if (res == null)
            {
                result.Result.Addresses = null;
            }*/
            return result;
        }

        public string City
        {
            get
            {
                if (!string.IsNullOrEmpty(city))
                    return city;
                if (Result?.Addresses == null)
                    return null;
                try
                {
                    foreach (var address in Result.Addresses)
                    {
                        if (address?.Features == null) continue;
                        foreach (var feature in address.Features)
                        {
                            var addressComponent =
                                feature?.Properties?.AddressComponents?.FirstOrDefault(x => x.Type == "place");
                            if (addressComponent != null)
                            {
                                city = addressComponent.Value;
                                return city;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, LogType.Error, true, "[SputnikResponse::City_get]");
                }

                return null;
            }
        }
        public string Street
        {
            get
            {
                if (!string.IsNullOrEmpty(street))
                    return street;
                if (Result?.Addresses == null)
                    return null;
                try
                {
                    foreach (var address in Result.Addresses)
                    {
                        if(address?.Features == null) continue;
                        foreach (var feature in address.Features)
                        {
                            var addressComponent =
                                feature?.Properties?.AddressComponents?.FirstOrDefault(x => x.Type == "street");
                            if (addressComponent != null)
                            {
                                street = addressComponent.Value;
                                return street;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, LogType.Error, true, "[SputnikResponse::Street_get]");
                }

                return null;
            }
        }
        public string House
        {
            get
            {
                if (!string.IsNullOrEmpty(house))
                    return house;
                if (Result?.Addresses == null)
                    return null;
                try
                {
                    foreach (var address in Result.Addresses)
                    {
                        if (address?.Features == null) continue;
                        foreach (var feature in address.Features)
                        {
                            var addressComponent =
                                feature?.Properties?.AddressComponents?.FirstOrDefault(x => x.Type == "house");
                            if (addressComponent != null)
                            {
                                house = addressComponent.Value;
                                return house;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, LogType.Error, true, "[SputnikResponse::Street_get]");
                }

                return null;
            }
        }

        public List<GeoCoordinate> Coordinates
        {
            get
            {
                if (_coordinates != null) return _coordinates;
                _coordinates = Result?.Addresses?.FirstOrDefault()?.Features?.FirstOrDefault()?.Geometry.Coordinates;
                return _coordinates;
            }
        }

    }

    public class SputnikResponseMeta
    {
        [JsonProperty("version")]
        public string Version { get; set; }
        [JsonProperty("format")]
        public string Format { get; set; }

        public override string ToString()
        {
            return Format+" v"+Version;
        }
    }
    public class SputnikResponseResult
    {
        [JsonProperty("priority")]
        public string Priority { get; set; }
        //[JsonProperty("viewport")]
        //public string Viewport { get; set; }
        [JsonProperty("address")]
        public List<SputnikResponseResultAddress> Addresses { get; set; }

    }
    public class SputnikResponseTypo
    {
        [JsonProperty("OriginalQuery")]
        public string OriginalQuery { get; set; }
        [JsonProperty("FixedQuery")]
        public string FixedQuery { get; set; }
        [JsonProperty("Rank")]
        public string Rank { get; set; }

        public override string ToString()
        {
            return String.Format("Rank:{2} OriginalQuery: '{0}' FixedQuery:'{1}' ", OriginalQuery, FixedQuery, Rank);
        }
    }
    public class SputnikResponseResultAddress
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("features")]
        public List<SputnikResponseResultAddressFeature> Features { get; set; }

        public override string ToString()
        {
            return Type+ " FeaturesCount: " + Features?.Count;
        }
    }
    public class SputnikResponseResultAddressFeature
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("properties")]
        public SputnikResponseResultAddressFeatureProperties Properties { get; set; }
        [JsonProperty("geometry")]
        public SputnikResponseResultAddressFeatureGeometry Geometry { get; set; }

        public override string ToString()
        {
            return Type + " "+ Properties;
        }
    }

    public class SputnikResponseResultAddressFeatureProperties
    {
        [JsonProperty("full_match")]
        private string _fullMatch;

        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("display_name")]
        public string DisplayName { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("address_components")]
        public List<SputnikResponseResultAddressFeatureAddressComponent> AddressComponents { get; set; }
        [JsonProperty("fias_id")]
        public string FiasId { get; set; }

        [JsonProperty("poi_types")]
        public string PoiTypes { get; set; }

        public bool IsFullMatch => _fullMatch == "true";

        public override string ToString()
        {
            return string.Format("{1} IsFullMatch: {0} Title: '{2}' DisplayName: '{3}'", IsFullMatch, Type, Title, DisplayName);
        }
    }

    public class SputnikResponseResultAddressFeatureAddressComponent
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }

        public override string ToString()
        {
            return Type + " => " + Value;
        }
    }
    public class SputnikResponseResultAddressFeatureGeometry
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("geometries")]
        public List<SputnikResponseResultAddressFeatureGeometryPoint> Geometries { get; set; }
        public List<GeoCoordinate> Coordinates
        {
            get { return Geometries.Select(x => x.Coord).ToList(); }
        }
    }

    public class SputnikResponseResultAddressFeatureGeometryPoint
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("coordinates")]
        public List<string> Points { get; set; }

        public GeoCoordinate Coord
        {
            get
            {
                if (Points.Count == 2)
                {
                    if (double.TryParse(Points[0].Replace(".",","), out var lat) && double.TryParse(Points[1].Replace(".", ","), out var lon))
                    {
                        //var res = new GeoCoordinate(lat, lon);
                        //var s = res.MapLink;
                        return new GeoCoordinate(lat, lon);
                    }
                }

                return null;

            }
        }
    }
}

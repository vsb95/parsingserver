﻿using System;
using System.Net;
using System.Text;
using Logger;

namespace Core.Services.Geocoding.Sputnik
{
    public static class SputnikGeocoder
    {
        //http://api.sputnik.ru/maps/geocoder/

        private const string baseUrlByStreet = "http://search.maps.sputnik.ru/search/addr?apikey=123&strict=true&blat=58.29075303&blon=70.36688437&tlat=54.31616593&tlon=76.07967726&format=json&addr_limit=5&q=";
        private const string baseUrlByCoord = "http://whatsthere.maps.sputnik.ru/point?apikey=123&houses=true&";

        public static SputnikResponse FindCoord(string street, WebProxy proxy = null)
        {
            if (string.IsNullOrEmpty(street))
                throw new ArgumentNullException(nameof(street));

            var json =
                "{\"meta\":{\"version\":\"0.197.0\",\"format\":\"geojson\"},\"result\":{\"priority\":\"address\",\"viewport\":{\"TopLat\":54.98438,\"TopLon\":73.28418,\"BotLat\":54.98438,\"BotLon\":73.28418},\"address\":[{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{\"id\":129788,\"type\":\"street\",\"description\":\"Омск, Россия\",\"display_name\":\"улица Ватутина, , Омск, Россия\",\"title\":\"улица Ватутина\",\"address_components\":[{\"type\":\"country\",\"value\":\"Россия\"},{\"type\":\"region\",\"value\":\"Омская область\"},{\"type\":\"place\",\"value\":\"Омск\"},{\"type\":\"street\",\"value\":\"улица Ватутина\"}],\"fias_id\":\"24d6bdc7-fdf6-4834-8129-1c43e64ede50\",\"full_match\":false,\"poi_types\":null},\"geometry\":{\"type\":\"GeometryCollection\",\"geometries\":[{\"type\":\"Coordinates\",\"coordinates\":[73.28418,54.98438]}]}}]}]},\"typo\":{\"OriginalQuery\":\"Омск, ватутина 12\",\"FixedQuery\":\"\",\"Rank\":0}}";
            //string json = null;
            var url = baseUrlByStreet + street;
            try
            {
                using (var client = new WebClient())
                {
                    client.Encoding = Encoding.UTF8;
                    json = client.DownloadString(url);
                    var response = SputnikResponse.Deserialize(json);
                    return response;
                }
                
            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "[SputnikGeocoder] Ошибка при распознавании адреса: " + string.Format("(request='{0}', proxy='{1}')", url, proxy?.Address));
                Log.Debug(json);
            }

            return null;
        }
        public static SputnikResponse FindAddress(double lat, double lon, WebProxy proxy = null)
        {
            string json = null;
            var url = baseUrlByCoord + "lat=" + lat.ToString().Replace(",", ".") + "&lon=" + lon.ToString().Replace(",", ".");
            try
            {
                using (var client = new WebClient())
                {
                    client.Encoding = Encoding.UTF8;
                    json = client.DownloadString(url);
                    var response = SputnikResponse.Deserialize(json);
                    return response;
                }

            }
            catch (Exception ex)
            {
                Log.Exception(ex, LogType.Fatal, true, "[SputnikGeocoder] Ошибка при распознавании адреса: " + string.Format("(request='{0}', proxy='{1}')", url, proxy?.Address));
                Log.Debug(json);
            }

            return null;
        }
    }
}

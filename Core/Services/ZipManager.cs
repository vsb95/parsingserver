﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ionic.Zip;
using Logger;

namespace Core.Services
{
    public static class ZipManager
    {

        public static bool TryCreateZip(IEnumerable<UploadImage> images, string zipFileName, out string path)
        {
            path = Path.Combine(SettingsManager.Settings.ServerSettings.ZipFolder, zipFileName);
            if (string.IsNullOrEmpty(zipFileName))
                return false;
            if (!zipFileName.EndsWith(".zip"))
                zipFileName += ".zip";
            while (File.Exists(path))
            {
                zipFileName = zipFileName.Replace(".zip", "")+"_" + Randomizer.CreateRandomString(2) + ".zip";
                path = Path.Combine(SettingsManager.Settings.ServerSettings.ZipFolder, zipFileName);
            }

            try
            {
                if (!Directory.Exists(SettingsManager.Settings.ServerSettings.ZipFolder))
                    Directory.CreateDirectory(SettingsManager.Settings.ServerSettings.ZipFolder);
                
                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncoding = Encoding.UTF8;
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                    zip.ZipErrorAction = ZipErrorAction.Skip;
                    foreach (var image in images)
                    {
                        if (string.IsNullOrEmpty(image.Src) || !File.Exists(image.Src)) continue;
                        var innerFolder = image.Name.Replace("_", "/").Replace(".jpeg","").Replace(".png", "").Replace(".jpg", "");
                        zip.AddFile(image.Src, innerFolder);
                    }

                    zip.Save(path);
                }

                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка при формировании архива: " + String.Join("\n", images), true);
                Log.Exception(ex, LogType.Fatal, true);
            }

            return false;
        }

    }
}

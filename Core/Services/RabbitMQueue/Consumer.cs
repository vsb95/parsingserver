﻿using System;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Core.Services.RabbitMQueue
{
    public class RabbitMQConsumer : IDisposable
    {
        public ConnectionFactory ConnectionFactory { get; set; }
        public IConnection Connect { get; set; }
        public IModel Channel { get; set; }
        public bool IsConnectToBus { get; set; }
        public bool IsSubscriberNotified { get; set; } // Подписчики оповещенны

        public delegate void ReceivedMessageDelegate(string message);
        public event ReceivedMessageDelegate ReceivedMessageEvent;

        public void Start()
        {
            try
            {
                ConnectionFactory = new ConnectionFactory
                {
                    HostName = SettingsManager.Settings.RabbitMQSettings.HostName/*,
                    UserName = "user",
                    Password = "password",
                    VirtualHost = "/",
                    AutomaticRecoveryEnabled = true,
                    TopologyRecoveryEnabled = true,
                    NetworkRecoveryInterval = TimeSpan.FromSeconds(3)*/
                };

                //Подключение
                Connect = ConnectionFactory.CreateConnection();

                //Создание канала обмена
                Channel = Connect.CreateModel();
                Channel.ExchangeDeclare(exchange: "dataExchange", type: "direct");

                var queueName = Channel.QueueDeclare().QueueName;
                Channel.QueueBind(queue: queueName, exchange: "dataExchange", routingKey: SettingsManager.Settings.RabbitMQSettings.RoutingKey);

                //Подписка на событие получения данных
                var consumer = new EventingBasicConsumer(Channel);
                consumer.Received += Consumer_Received;
                Channel.BasicConsume(queue: queueName, autoAck: true, consumer: consumer);

                IsConnectToBus = Connect.IsOpen;
                ReceivedMessageEvent?.Invoke(IsConnectToBus.ToString());
            }
            catch (RabbitMQ.Client.Exceptions.BrokerUnreachableException ex)
            {
                ReceivedMessageEvent?.Invoke($"Соединение прерванно {ex.ToString()}");
            }
            catch (Exception ex)
            {
                ReceivedMessageEvent?.Invoke($"ИСКЛЮЧЕНИЕ {ex.ToString()}");
            }
        }

        private void Consumer_Received(object model, BasicDeliverEventArgs ea)
        {
            var body = ea.Body;
            var message = Encoding.UTF8.GetString(body);
            var routingKey = ea.RoutingKey;
            ReceivedMessageEvent?.Invoke(message);
        }

        public void Dispose()
        {
            Channel?.Close(200, "Goodbye");
            Connect?.Close();
            Channel = null;
            Connect = null;
        }
    }
}

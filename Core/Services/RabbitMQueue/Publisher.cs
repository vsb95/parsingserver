﻿using System;
using System.Text;
using RabbitMQ.Client;

namespace Core.Services.RabbitMQueue
{
    public class RabbitMQPublisher : IDisposable
    {
        public ConnectionFactory ConnectionFactory { get; set; }
        public IConnection Connect { get; set; }
        public IModel Channel { get; set; }
        public bool IsConnectToBus { get; set; }
        public bool IsSubscriberNotified { get; set; } // Подписчики оповещенны

        public delegate void ReceivedMessageDelegate(string message);
        public event RabbitMQConsumer.ReceivedMessageDelegate MessageEvent;

        public void Start()
        {
            try
            {
                ConnectionFactory = new ConnectionFactory
                {
                    HostName = SettingsManager.Settings.RabbitMQSettings.HostName
                    /*,
                    UserName = "user",
                    Password = "password",
                    VirtualHost = "/",
                    AutomaticRecoveryEnabled = true,
                    TopologyRecoveryEnabled = true,
                    NetworkRecoveryInterval = TimeSpan.FromSeconds(3)*/
                };

                //Подключение
                Connect = ConnectionFactory.CreateConnection();

                //Создание канала обмена
                Channel = Connect.CreateModel();
                Channel.QueueDeclare(queue: SettingsManager.Settings.RabbitMQSettings.RoutingKey, durable: false, exclusive: false, autoDelete: false, arguments: null);
                //Channel.QueueDeclare(queue: Settings.RabbitMQSettings.RoutingKey);
                //Channel.ExchangeDeclare(exchange: "dataExchange", type: "direct");

                IsConnectToBus = Connect.IsOpen;
                MessageEvent?.Invoke(IsConnectToBus.ToString());
            }
            catch (RabbitMQ.Client.Exceptions.BrokerUnreachableException ex)
            {
                MessageEvent?.Invoke($"Соединение прерванно {ex.ToString()}");
            }
            catch (Exception ex)
            {
                MessageEvent?.Invoke($"ИСКЛЮЧЕНИЕ {ex.ToString()}");
            }
        }

        public void Send(string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                MessageEvent?.Invoke("Message = NULL");
                return;
            }

            var routingKey = SettingsManager.Settings.RabbitMQSettings.RoutingKey;
            if (string.IsNullOrEmpty(routingKey))
            {
                MessageEvent?.Invoke("routingKey = NULL");
                return;
            }

            try
            {
                var properties = Channel.CreateBasicProperties();
                properties.Persistent = true;
                var body = Encoding.UTF8.GetBytes(message);
                Channel.BasicPublish(exchange: "", 
                    routingKey: routingKey, basicProperties: properties, body: body);
            }
            catch (RabbitMQ.Client.Exceptions.BrokerUnreachableException ex)
            {
                MessageEvent?.Invoke($"Соединение прерванно {ex.ToString()}");
            }
            catch (Exception ex)
            {
                MessageEvent?.Invoke($"ИСКЛЮЧЕНИЕ {ex.ToString()}");
            }
        }

        public void Dispose()
        {
            Channel?.Close(200, "Goodbye");
            Connect?.Close();
            Channel = null;
            Connect = null;
        }
    }
}

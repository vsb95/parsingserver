﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Enteties;

namespace Core.Interface
{
    public interface IDistrictManager
    {
        bool TryIdentifyDistrictAndMicroDistrict(GeoCoordinate geoCoordinate, out District district,
            out MicroDistrict microDistrict, out bool isNeedReverse);

    }
}
